﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

public class ValidateModelStateAttribute : ActionFilterAttribute
{
    public override void OnActionExecuting(HttpActionContext actionContext)
    {
        if (!actionContext.ModelState.IsValid)
        {
            actionContext.Response = actionContext
                                     .Request
                                     .CreateResponse(HttpStatusCode.BadRequest,
                                                    new ResponseCommon()
                                                    {
                                                        StatusCode = ApiStatus.InvalidInput,
                                                        Message = GetModelStateErrorMessages(actionContext)
                                                    }
                                                    );
        }
    }
    private IEnumerable<string> GetModelStateErrorMessages(HttpActionContext actionContext)
    {
        foreach (var x in actionContext.ModelState.Values)
        {
            foreach (var s in x.Errors)
            {
                if (!string.IsNullOrWhiteSpace(s.ErrorMessage))
                { yield return s.ErrorMessage; }
            }
        }
    }
}
