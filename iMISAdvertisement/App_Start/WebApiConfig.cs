﻿using Autofac;
using Autofac.Integration.WebApi;
using Asi.Controllers.API;
using Asi.DAL.ModelContext;
using Asi.Repository;
using System.Web.Http;
namespace iMISAdvertisement
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApiWithAction",
            //    routeTemplate: "api/{controller}/{action}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            var builder = new ContainerBuilder();
            builder.RegisterType<AdAdjustmentRepository>().As<IAdAdjustmentRepository>();
            builder.RegisterType<AdAdjustmentController>();

            builder.RegisterType<AdAdjustmentTargetRepository>().As<IAdvertiserAgencyMappingRepository>();
            builder.RegisterType<AdAdjustmentTargetController>();

            builder.RegisterType<AdColorRepository>().As<IAdColorRepository>();
            builder.RegisterType<AdColorController>();

            builder.RegisterType<AdSizeRepository>().As<IAdSizeRepository>();
            builder.RegisterType<AdSizeController>();

            builder.RegisterType<AdTypeRepository>().As<IAdTypeRepository>();
            builder.RegisterType<AdTypeController>();

            builder.RegisterType<IssueDateRepository>().As<IIssueDateRepository>();
            builder.RegisterType<IssueDateController>();

            builder.RegisterType<MediaAssetRepository>().As<IMediaAssetRepository>();
            builder.RegisterType<MediaAssetController>();

            builder.RegisterType<MediaAssetGroupRepository>().As<IMediaAssetGroupRepository>();
            builder.RegisterType<MediaAssetGroupController>();

            builder.RegisterType<MediaBillingMethodRepository>().As<IMediaBillingMethodRepository>();
            builder.RegisterType<MediaBillingMethodController>();

            builder.RegisterType<MediaTypeRepository>().As<IMediaTypeRepository>();
            builder.RegisterType<MediaTypeController>();

            builder.RegisterType<PositionRepository>().As<IPositionRepository>();
            builder.RegisterType<PositionController>();

            builder.RegisterType<ProductionStageRepository>().As<IProductionStageRepository>();
            builder.RegisterType<ProductionStageController>();

            builder.RegisterType<ProductionStatusRepository>().As<IProductionStatusRepository>();
            builder.RegisterType<ProductionStatusController>();

            builder.RegisterType<RateCardRepository>().As<IRateCardRepository>();
            builder.RegisterType<RateCardController>();

            builder.RegisterType<RateCardDetailRepository>().As<IRateCardDetailRepository>();
            builder.RegisterType<RateCardDetailController>();

            builder.RegisterType<RepsRepository>().As<IRepsRepository>();
            builder.RegisterType<RepsController>();

            builder.RegisterType<RepTerritoriesRepository>().As<IRepTerritoriesRepository>();
            builder.RegisterType<RepTerritoriesController>();

            builder.RegisterType<Territoriesrepository>().As<ITerritoriesRepository>();
            builder.RegisterType<TerritoriesController>();

            builder.RegisterType<SeparationRepository>().As<ISeparationRepository>();
            builder.RegisterType<SeparationController>();

            builder.RegisterType<FrequencyRepository>().As<IFrequencyRepository>();
            builder.RegisterType<FrequencyController>();

            builder.RegisterType<MediaOrdersRepository>().As<IMediaOrdersRepository>();
            builder.RegisterType<MediaOrderController>();

            builder.RegisterType<MediaOrderLinesRepository>().As<IMediaOrderLinesRepository>();
            builder.RegisterType<MediaOrderLinesController>();

            builder.RegisterType<MediaOrderProductionDetailsRepository>().As<IMediaOrderProductionDetailsRepository>();
            builder.RegisterType<MediaOrderProductionDetailsController>();

            builder.RegisterType<DashBoardRepository>().As<IDashBoardRepository>();
            builder.RegisterType<DashBoardController>();

            builder.RegisterType<InCompleteMediaOrderRepository>().As<IInCompleteMediaOrderRepository>();
            builder.RegisterType<InCompleteMediaOrderController>();

            builder.RegisterType<CompleteMediaOrderRepository>().As<ICompleteMediaOrderRepository>();
            builder.RegisterType<CompleteMediaOrderController>();

            builder.RegisterType<PickUpFilesMediaOrderRepository>().As<IPickUpFilesMediaOrderRepository>();
            builder.RegisterType<PickUpFilesMediaOrderController>();

            builder.RegisterType<MediaOrderSignedDocumentsRepository>().As<IMediaOrderSignedDocumentsRepository>();
            builder.RegisterType<MediaOrderSignedDocumentsController>();

            //builder.RegisterType<AdvertiserRepository>().As<IAdvertiserRepository>();
            //builder.RegisterType<AdvertiserController>();

            //builder.RegisterType<ContactRepository>().As<IContactRepository>();
            //builder.RegisterType<ContactController>();

            builder.RegisterType<InventoryRepository>().As<IInventoryRepository>();
            builder.RegisterType<InventoryController>();

            builder.RegisterType<AdvertiserRepTerritoryMapRepository>().As<IAdvertiserRepTerritoryMapRepository>();
            builder.RegisterType<AdvertiserRepTerritoryMappingController>();

            builder.RegisterType<AdvertiserAgencyMappingRepository>().As<IAdvAgencyMappingRepository>();
            builder.RegisterType<AdvertiserAgencyMappingController>();
            builder.RegisterType<ASI_iMISContext>().As<IDBEntities>();
            var container = builder.Build();
            var resolver = new AutofacWebApiDependencyResolver(container);
            config.DependencyResolver = resolver;
        }
    }
}
