﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Asi.Controllers.API
{
    [RoutePrefix("api/advertiserrepterritorymapping")]
    public class AdvertiserRepTerritoryMappingController : ApiController
    {
        #region Private Fields
        IAdvertiserRepTerritoryMapRepository _IRepository;
        #endregion

        #region Constructor
        public AdvertiserRepTerritoryMappingController(IAdvertiserRepTerritoryMapRepository typeRepository)
        {
            this._IRepository = typeRepository;
        }
        #endregion

        [HttpPost]
        [CheckModelForNull]
        [ValidateModelState]
        [Route("AddUpdate")]
        public HttpResponseMessage AddEditAdvertiserAgencyMapping(AdvertiserRepTerritoryMappingModel model)
        {
            var response = _IRepository.PostMapping(model);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("Get/{AdvertiserId}")]
        public HttpResponseMessage GetAdvertiserAgencyMapping_ByAdvertiserId(string AdvertiserId)
        {
            var response = _IRepository.GetRepTerritoryMapping(AdvertiserId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        //--------------------------------------------------------------------------------------

        [HttpGet]
        [Route("List/{AdvertiserId}")]
        public HttpResponseMessage GetAdvertiserAgencyMappingListByAdvertiserId(string AdvertiserId)
        {
            var response = _IRepository.GetRepTerritoryMappingListByAdvertiserId(AdvertiserId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        public HttpResponseMessage GetAdvertiserRepTerritoryMappingList()
        {
            var response = _IRepository.GetAdvertiserRepTerritoryMappingList();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpDelete]
        public HttpResponseMessage DeleteAdvertiserRepTerritoryMapping(int id)
        {
            var response = _IRepository.DeleteAdvertiserRepTerritoryMapping(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
    }
}
