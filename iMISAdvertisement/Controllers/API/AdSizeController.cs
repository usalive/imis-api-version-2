﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Asi.Controllers.API
{
    [RoutePrefix("api/AdSize")]
    //[Authorize(Roles = "Admin")]
    public class AdSizeController : ApiController
    {
        #region Private Fields
        IAdSizeRepository _IRepository;
        #endregion

        #region Constructor
        public AdSizeController(IAdSizeRepository adSizeRepository)
        {
            this._IRepository = adSizeRepository;
        }
        #endregion

        #region Authenticated API's
        [HttpGet]
        public HttpResponseMessage Get()
        {
            ResponseCommon response = _IRepository.GetAdSizes();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            ResponseCommon response = _IRepository.GetAdSizesById(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("GetByRateCardAndColor/{rateCardId:int}/{adColorId:int}")]
        public HttpResponseMessage GetAdSizesByRateCardIdAndColordId(int rateCardId, int adColorId)
        {
            ResponseCommon response = _IRepository.GetAdSizesByRateCardIdAndColordId(rateCardId, adColorId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("GetByRateCard/{rateCardId:int}")]
        public HttpResponseMessage GetAdSizesByRateCardId(int rateCardId)
        {
            ResponseCommon response = _IRepository.GetAdSizesByRateCardId(rateCardId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("GetByMediaAsset/{mediaAssetId:int}")]
        public HttpResponseMessage ByMediaAsset(int mediaAssetId)
        {
            var response = _IRepository.GetAdSizesByMediaAsset(mediaAssetId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Post([FromBody]PostAdSizeModel adSizeModel)
        {
            ResponseCommon response = _IRepository.PostAdSize(adSizeModel);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPut]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Put(GetAdSizeModel adSizeModel)
        {
            ResponseCommon response = _IRepository.PutAdSize(adSizeModel);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            ResponseCommon response = _IRepository.DeleteAdSize(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        #endregion
    }
}
