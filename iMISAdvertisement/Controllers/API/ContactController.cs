﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Web.Http;
//using Asi.DAL.Model;
//using Asi.Repository;

//namespace Asi.Controllers.API
//{
//    //[Authorize(Roles = "Admin")]
//    [RoutePrefix("api/contact")]
//    public class ContactController : ApiController
//    {
//        #region Private Fields
//       IContactRepository _IRepository;
//        #endregion

//        #region Constructor

//        public ContactController(IContactRepository Irepository)
//        {
//            this._IRepository = Irepository;
//        }
//        #endregion


//        #region Authenticated API's
//        [HttpGet]
//        public HttpResponseMessage Get()
//        {
//            var response = _IRepository.GetContacts();
//            return Request.CreateResponse(HttpStatusCode.OK, response);
//        }
//        [HttpGet]
//        public HttpResponseMessage Get(int id)
//        {
//            var response = _IRepository.GetContactById(id);
//            return Request.CreateResponse(HttpStatusCode.OK, response);
//        }

//        [CheckModelForNull]
//        [ValidateModelState]
//        [HttpPost]
//        public HttpResponseMessage Post(PostContactModel model)
//        {
//            var response = _IRepository.PostContact(model);
//            return Request.CreateResponse(HttpStatusCode.OK, response);
//        }

//        //[CheckModelForNull]
//        //[ValidateModelState]
//        //public HttpResponseMessage Put(GetAdAdjustmentModel model)
//        //{
//        //    var response = _IRepository.PutAdAdjustment(model);
//        //    return Request.CreateResponse(HttpStatusCode.OK, response);
//        //}

//        //public HttpResponseMessage Delete(int id)
//        //{
//        //    var response = _IRepository.DeleteAdAdjustment(id);
//        //    return Request.CreateResponse(HttpStatusCode.OK, response);
//        //}
//        #endregion
//    }
//}
