﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Asi.Controllers.API
{
    //[Authorize(Roles = "Admin")]
    [RoutePrefix("api/IssueDate")]
    public class IssueDateController : ApiController
    {
        #region Private Fields
        IIssueDateRepository _IRepository;
        #endregion

        #region Constructor
        public IssueDateController(IIssueDateRepository issueDateRepository)
        {
            this._IRepository = issueDateRepository;
        }
        #endregion

        #region Authenticated API's
        public HttpResponseMessage Get()
        {
            var response = _IRepository.GetIssueDates();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        public HttpResponseMessage Get(int id)
        {
            var response = _IRepository.GetIssueDatesById(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        [HttpGet]
        [Route("GetByMediaAssetForDropdown/{mediaAssetId:int}")]
        public HttpResponseMessage ByMediaAsset(int mediaAssetId)
        {
            var response = _IRepository.GetIssueDatesByMediaAsset(mediaAssetId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        [HttpPost]
        [Route("GetByMediaAsset")]
        public HttpResponseMessage ByMediaAsset()
        {
            var response = _IRepository.GetIssueDatesByMediaAsset();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [CheckModelForNull]
        [ValidateModelState]
        [Route("GetByMediaAssetAndDateRange")]
        public HttpResponseMessage ByMediaAssetAndDateRange(GetIssueDatesByMediaAssetAndDateRangeModel model)
        {
            var response = _IRepository.GetIssueDatesByMediaAssetAndDateRange(model);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Post(PostIssueDateModel model)
        {
            var response = _IRepository.PostIssueDate(model);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Put(PutIssueDateModel model)
        {
            var response = _IRepository.PutIssueDate(model);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        public HttpResponseMessage Delete(int id)
        {
            var response = _IRepository.DeleteIssueDate(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        #endregion
    }
}
