﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Asi.Controllers.API
{
    //[Authorize(Roles = "Admin")]
    public class ProductionStatusController : ApiController
    {
        #region Private Fields
        IProductionStatusRepository _IRepository;
        #endregion

        #region Constructor
        public ProductionStatusController(IProductionStatusRepository productionStatusRepository)
        {
            this._IRepository = productionStatusRepository;
        }
        #endregion

        #region Authenticated API's
        [HttpGet]
        public HttpResponseMessage Get()
        {
            var response = _IRepository.GetProductionStatuses();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            var response = _IRepository.GetProductionStatusById(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Post(ProductionStatusModel model)
        {
            var response = _IRepository.PostProductionStatus(model);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPut]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage PutProductionStatus(ProductionStatusModel model)
        {
            var response = _IRepository.PutProductionStatus(model);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            var response = _IRepository.DeleteProductionStatus(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        #endregion
    }
}
