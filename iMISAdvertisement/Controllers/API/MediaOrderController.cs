﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using ClosedXML.Excel;
using System;
using System.Configuration;
using Microsoft.Owin;

namespace Asi.Controllers.API
{
    [RoutePrefix("api/MediaOrder")]
    public class MediaOrderController : ApiController
    {
        #region Private Fields
        IMediaOrdersRepository _IRepository;
        #endregion

        #region Constructor
        public MediaOrderController(IMediaOrdersRepository mediaOrdersRepository)
        {
            this._IRepository = mediaOrdersRepository;
        }
        #endregion

        #region Private Methods


        #endregion

        #region API's
        [Route("GenerateBuyId")]
        [HttpGet]
        public HttpResponseMessage GenerateBuyId()
        {
            var response = _IRepository.GenerateBuyId();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [CheckModelForNull]
        [ValidateModelState]
        [HttpPost]
        public HttpResponseMessage PostOrder(PostMediaOrderModel model)
        {
            var response = _IRepository.AddMediaOrder(model);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [CheckModelForNull]
        [ValidateModelState]
        [HttpPost]
        [Route("PostOrderForCopy")]
        public HttpResponseMessage PostOrdersForCopy([FromBody]MediaOrderCopyModel[] model)
        {
            var response = _IRepository.PostOrdersForCopy(model);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("GetByBuyId/{buyid:int}")]
        public HttpResponseMessage GetOrdersByBuyId(int buyId)
        {
            var response = _IRepository.GetOrdersByBuyId(buyId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [Route("GetOrdersNotInBuyer/{buyid:int}/{advertiserId}")]
        [HttpGet]
        public HttpResponseMessage GetOrdersNotInBuyer(int buyId, string advertiserId)
        {
            var response = _IRepository.GetOrdersNotInBuyer(buyId, advertiserId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        //[ArrayInput("advertiserId")]
        //[Route("GetOrdersByAdvertisor/{draw}/{skip}/{take}/{advertiserId}")]
        //[HttpGet]
        //public HttpResponseMessage GetOrdersByAdvertisor(int draw, int skip, int take, string[] advertiserId)
        //{
        //    var response = _IRepository.GetOrdersByAdvertisor(draw, skip, take, advertiserId);
        //    return Request.CreateResponse(HttpStatusCode.OK, response);
        //}

        [Route("GetOrdersByAdvertiser")]
        [HttpPost]
        public HttpResponseMessage GetOrdersByAdvertisor([FromBody]GetOrdersByAdvertisorModel model)
        {
            var response = _IRepository.GetOrdersByAdvertisor(model);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        //[HttpPost]
        //[Route("getordersbuyidwise")]
        //public HttpResponseMessage GetOrdersBuyIdWise([FromBody]GetOrdersBuyIdWiseModal[] modal)
        //{
        //    var response = _IRepository.GetOrdersBuyIdWise(modal);
        //    return Request.CreateResponse(HttpStatusCode.OK, response);
        //}

        [HttpPost]
        [Route("GetOrdersBuyIdWise")]
        public HttpResponseMessage GetOrdersBuyIdWise()
        {
            var response = _IRepository.GetOrdersBuyIdWise();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("GetAllAdvertiserForPostOrder")]
        public HttpResponseMessage GetAllAdvertiserForPostOrder()
        {
            var response = _IRepository.GetAllAdvertiserForPostOrder();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        //[HttpPost]
        //[Route("getordersforpostorder")]
        //public HttpResponseMessage GetOrdersForPostOrder([FromBody]GetOrdersForPostOrderbyFilterOption modal)
        //{
        //    var response = _IRepository.GetOrdersForPostOrder(modal);
        //    return Request.CreateResponse(HttpStatusCode.OK, response);
        //}

        [HttpPost]
        [Route("GetOrdersForPostOrder")]
        public HttpResponseMessage GetOrdersForPostOrder()
        {
            var response = _IRepository.GetOrdersForPostOrder();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("GetOrdersBuyIdWiseForCopy/{buyid:int}")]
        public HttpResponseMessage GetOrdersBuyIdWiseForCopy(int buyId)
        {
            var response = _IRepository.GetOrdersBuyIdWiseForCopy(buyId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [Route("GetOrdersByProductionStatus")]
        public HttpResponseMessage GetOrdersbyProductionStatus()
        {
            var response = _IRepository.GetOrdersbyProductionStatus();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [Route("FillDropdownForPostOrder")]
        public HttpResponseMessage FillDropdownForPostOrder([FromBody]GetOrdersForPostOrderbyFilterOption filterOption)
        {
            var response = _IRepository.FillDropdownForPostOrder(filterOption);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpDelete]
        public HttpResponseMessage DeleteMediaOrder(int id)
        {
            var response = _IRepository.DeleteMediaOrder(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [CheckModelForNull]
        [ValidateModelState]
        [HttpPut]
        [Route("ChangeStatus")]
        public HttpResponseMessage ChangeStatus(MediaOrderChangeStatusModel model)
        {
            var response = _IRepository.ChangeStatus(model);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            var response = _IRepository.GetOrderByMediaOrderId(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [CheckModelForNull]
        [ValidateModelState]
        [HttpPut]
        public HttpResponseMessage PutOrder(PutMediaOrderModel model)
        {
            var response = _IRepository.UpdateMediaOrder(model);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        
        [HttpPost]
        [Route("ImportExcelSheet")]
        public HttpResponseMessage ImportExcelSheet()
        {
            var response = _IRepository.ImportExcelSheet();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        
        [HttpPost]
        [Route("UpdateIsFrozenTrue")]
        public HttpResponseMessage UpdateIsFrozenTrue(List<int> mediaOrderIds)
        {
            var response = _IRepository.UpdateIsFrozenTrue(mediaOrderIds);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        #endregion
    }
}
