﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Asi.Controllers.API
{
    //[Authorize(Roles = "Admin")]
      [RoutePrefix("api/Reps")]
    public class RepsController : ApiController
    {
        #region Private Fields
        IRepsRepository _IRepository;
        #endregion

        #region Constructor
        public RepsController(IRepsRepository repsRepository)
        {
            this._IRepository = repsRepository;
        }
        #endregion

        #region Authenticated API's
        [HttpGet]
        public HttpResponseMessage Get()
        {
            ResponseCommon response = _IRepository.GetReps();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
         [HttpGet]
         [Route("GetRepsForOrderCreation")]
        public HttpResponseMessage GetRepsForOrderCreation()
        {
            ResponseCommon response = _IRepository.GetRepsForOrderCreation();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            ResponseCommon response = _IRepository.GetRepById(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Post([FromBody]RepModel model)
        {
            ResponseCommon response = _IRepository.PostRep(model);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPut]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Put(RepModel model)
        {
            ResponseCommon response = _IRepository.PutRep(model);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            ResponseCommon response = _IRepository.DeleteRep(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        #endregion
    }
}
