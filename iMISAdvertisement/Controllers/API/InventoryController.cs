﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Asi.Controllers.API
{
    //[Authorize(Roles = "Admin")]
    [RoutePrefix("api/Inventory")]
    public class InventoryController : ApiController
    {
        #region Private Fields
        IInventoryRepository _IRepository;
        #endregion

        #region Constructor
        public InventoryController(IInventoryRepository inventoryRepository)
        {
            this._IRepository = inventoryRepository;
        }
        #endregion


        #region Authenticated API's
        [Route("GetByMediaAsset/{mediaAssetId:int}")]
        public HttpResponseMessage GetByMediaAsset(int mediaAssetId)
        {
            var response = _IRepository.GetInventoryDetails(mediaAssetId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        //[Route("GetByRateCardAndColor/{rateCardId:int}/{adColorId:int}")]
        //public HttpResponseMessage Get(int rateCardId, int adColorId)
        //{
        //    var response = _IRepository.GetRateCardDetailsByColor(rateCardId, adColorId);
        //    return Request.CreateResponse(HttpStatusCode.OK, response);
        //}

        //[Route("GetRateCardCost/{rateCardId:int}/{adColorId:int}/{adSizeId:int}/{frequencyId:int}")]
        //public HttpResponseMessage Get(int rateCardId, int adColorId, int adSizeId, int frequencyId)
        //{
        //    var response = _IRepository.GetRateCardsCost(rateCardId, adColorId, adSizeId, frequencyId);
        //    return Request.CreateResponse(HttpStatusCode.OK, response);
        //}

        //[Route("GetRateCardsCostForPerWord/{rateCardId:int}/{adColorId:int}")]
        //public HttpResponseMessage GetRateCardsCostForPerWord(int rateCardId, int adColorId)
        //{
        //    var response = _IRepository.GetRateCardsCostForPerWord(rateCardId, adColorId);
        //    return Request.CreateResponse(HttpStatusCode.OK, response);
        //}

        //public HttpResponseMessage Get(int id)
        //{
        //    var response = _IRepository.GetRateCardDetailById(id);
        //    return Request.CreateResponse(HttpStatusCode.OK, response);
        //}

        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Post(PostInventoryModel inventoryModel)
        {
            var response = _IRepository.PostInventory(inventoryModel);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        #endregion
    }
}
