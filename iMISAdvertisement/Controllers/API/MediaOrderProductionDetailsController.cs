﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Asi.Controllers.API
{
    [RoutePrefix("api/MediaOrderProductionDetails")]
    public class MediaOrderProductionDetailsController : ApiController
    {
        #region Private Fields
        IMediaOrderProductionDetailsRepository _IRepository;
        #endregion

        #region Constructor
        public MediaOrderProductionDetailsController(IMediaOrderProductionDetailsRepository Irepository)
        {
            this._IRepository = Irepository;
        }
        #endregion

        #region API's
        [Route("GetByMediaOrder/{id:int}")]
        [HttpGet]
        public HttpResponseMessage GetByMediaOrderId(int id)
        {
            var response = _IRepository.GetMediaOrderProductionDetailByMediaOrderId(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage PostMediaOrderLine(PostMediaOrderProductionDetailModel model)
        {
            var response = _IRepository.AddUpdateMediaOrderProductionDetail(model);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpDelete]
        [Route("ByMediaOrderId/{mediaOrderId:int}")]
        public HttpResponseMessage DeleteMediaOrderLine([FromUri]int mediaOrderId)
        {
            var response = _IRepository.DeleteMediaOrderProductionDetailByMediaOrderId(mediaOrderId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("UpdateMediaOrderProductionStatusComplete/{mediaOrderId:int}")]
        public HttpResponseMessage UpdateMediaOrderProductionStatusComplete(int mediaOrderId)
        {
            var response = _IRepository.UpdateMediaOrderProductionStatusComplete(mediaOrderId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("UpdateMediaOrderProductionStatusInComplete/{mediaOrderId:int}")]
        public HttpResponseMessage UpdateMediaOrderProductionStatusInComplete(int mediaOrderId)
        {
            var response = _IRepository.UpdateMediaOrderProductionStatusInComplete(mediaOrderId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        #endregion
    }
}
