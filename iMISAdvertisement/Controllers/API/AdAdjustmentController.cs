﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Asi.Controllers.API
{
    //[Authorize(Roles = "Admin")]
    [RoutePrefix("api/AdAdjustment")]
    public class AdAdjustmentController : ApiController
    {
        #region Private Fields
        private readonly IAdAdjustmentRepository _IRepository;
        #endregion

        #region Constructor
        public AdAdjustmentController(IAdAdjustmentRepository adjustmentRepository)
        {
            this._IRepository = adjustmentRepository;
        }
        #endregion

        #region Authenticated API's
        public HttpResponseMessage Get()
        {
            var response = _IRepository.GetAdAdjustments();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        public HttpResponseMessage Get(int id)
        {
            var response = _IRepository.GetAdAdjustmentById(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("GetByMediaAsset/{id:int}")]
        public HttpResponseMessage GetAdAdjustmentByMediaAsset(int id)
        {
            var response = _IRepository.GetAdAdjustmentsByMediaAsset(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("GetByMediaAssetIds")]
        public HttpResponseMessage GetAdAdjustmentByMediaAsset([FromUri]int[] id)
        {
            GetAdjustmentsByMediaAssetIdsModel objRequest = new GetAdjustmentsByMediaAssetIdsModel();
            objRequest.MediaAssetIds = id;
            ResponseCommon response = _IRepository.GetAdAdjustmentsByMediaAssets(objRequest);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Post(PostAdAdjustmentModel model)
        {
            var response = _IRepository.PostAdAdjustment(model);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Put(GetAdAdjustmentModel model)
        {
            var response = _IRepository.PutAdAdjustment(model);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        public HttpResponseMessage Delete(int id)
        {
            var response = _IRepository.DeleteAdAdjustment(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        #endregion
    }
}
