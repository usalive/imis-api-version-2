﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Asi.DAL.Model;
using Asi.Repository;

namespace Asi.Controllers.API
{
    [RoutePrefix("api/CompleteMediaOrder")]
    public class CompleteMediaOrderController : ApiController
    {
        #region Private Fields
        ICompleteMediaOrderRepository _IRepository;
        #endregion

        #region Constructor
        public CompleteMediaOrderController(ICompleteMediaOrderRepository completeMediaOrderRepository)
        {
            this._IRepository = completeMediaOrderRepository;
        }
        #endregion

        #region Authenticated API's
        [Route("GetByMediaOrder/{id:int}")]
        [HttpGet]
        public HttpResponseMessage GetByMediaOrderId(int id)
        {
            var response = _IRepository.GetCompleteMediaOrderByMediaOrderId(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }


        [HttpPost]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Post(PostInCompleteMediaOrderModel model)
        {
            var response = _IRepository.PostCompleteMediaOrder(model);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [Route("GetCompleteOrders")]
        public HttpResponseMessage GetCompleteOrders([FromBody]GetInCompleteOrdersbyFilterOption modal)
        {
            var response = _IRepository.GetCompleteOrdersByFilterOptions(modal);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [Route("FillDropDownForCompleteOrder")]
        public HttpResponseMessage FillDropdownForCompleteOrder([FromBody]GetInCompleteOrdersbyFilterOption modal)
        {
            var response = _IRepository.FillDropdownForCompleteOrder(modal);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        #endregion
    }
}
