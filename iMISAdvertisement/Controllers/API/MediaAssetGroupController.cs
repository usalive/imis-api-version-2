﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Asi.Controllers.API
{
    //[Authorize(Roles = "Admin")]
    public class MediaAssetGroupController : ApiController
    {
        #region Private Fields
        IMediaAssetGroupRepository _IRepository;
        #endregion

        #region Constructor
        public MediaAssetGroupController(IMediaAssetGroupRepository assetGroupRepository)
        {
            this._IRepository = assetGroupRepository;
        }
        #endregion

        #region Authenticated API's
        [HttpGet]
        public HttpResponseMessage Get()
        {
            ResponseCommon response = _IRepository.GetMediaAssetGroups();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            ResponseCommon response = _IRepository.GetMediaAssetGroupsById(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Post([FromBody]PostMediaAssetGroupModel mediaAssetGroupModel)
        {
            ResponseCommon response = _IRepository.PostMediaAssetGroup(mediaAssetGroupModel);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPut]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Put(GetMediaAssetGroupModel mediaAssetGroupModel)
        {
            ResponseCommon response = _IRepository.PutMediaAssetGroup(mediaAssetGroupModel);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            ResponseCommon response = _IRepository.DeleteMediaAssetGroup(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        #endregion
    }
}
