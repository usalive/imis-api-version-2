﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Asi.Controllers.API
{
    //[Authorize(Roles = "Admin")]
    [RoutePrefix("api/MediaAsset")]
    public class MediaAssetController : ApiController
    {
        #region Private Fields
        IMediaAssetRepository _IRepository;
        #endregion

        #region Constructor
        public MediaAssetController(IMediaAssetRepository mediaAssetRepository)
        {
            this._IRepository = mediaAssetRepository;
        }
        #endregion

        #region Authenticated API's

        [HttpPost]
        [Route("GetMediaAssets")]
        public HttpResponseMessage GetAllMediaAsset()
        {
            var response = _IRepository.GetMediaAssets();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }


        [HttpGet]
        public HttpResponseMessage Get()
        {
            var response = _IRepository.GetAllMediaAssets();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            var response = _IRepository.GetMediaAssetsById(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Post(PostMediaAssetModel postMediaAssetModel)
        {
            var response = _IRepository.PostMediaAsset(postMediaAssetModel);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPut]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Put(PutMediaAssetModel putMediaAssetModel)
        {
            var response = _IRepository.PutMediaAsset(putMediaAssetModel);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            var response = _IRepository.DeleteMediaAsset(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        #endregion
    }
}
