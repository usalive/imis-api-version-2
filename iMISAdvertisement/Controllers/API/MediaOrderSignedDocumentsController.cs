﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Asi.Controllers.API
{
    [RoutePrefix("api/MediaOrderSignedDocuments")]
    public class MediaOrderSignedDocumentsController : ApiController
    {
        // GET: MediaOrderSignedDocuments
        #region Private Fields
        IMediaOrderSignedDocumentsRepository _IRepository;
        #endregion

        #region Constructor
        public MediaOrderSignedDocumentsController(IMediaOrderSignedDocumentsRepository ImediaOrderSignedDocumentsRepository)
        {
            this._IRepository = ImediaOrderSignedDocumentsRepository;
        }
        #endregion

        #region API's

        [HttpPost]
        public HttpResponseMessage Post(MediaOrderSignedDocumentsModel model)
        {
            var response = _IRepository.saveMediaOrderSignedDocumentsByBuyId(model);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [Route("getByBuyerId/{buyId:int}")]
        [HttpGet]
        public HttpResponseMessage GetMediaOrderSignedDocumentsByBuyId(int buyId)
        {
            var response = _IRepository.getMediaOrderSignedDocumentsByBuyId(buyId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpDelete]
        public HttpResponseMessage deleteMediaOrderSignedDocumentById(long id)
        {
            var response = _IRepository.deleteMediaOrderSignedDocumentById(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        #endregion
    }
}