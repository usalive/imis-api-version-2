﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace Asi.Controllers.API
{
    [RoutePrefix("api/advertiseragencymapping")]
    public class AdvertiserAgencyMappingController : ApiController
    {
        #region Private Fields
        IAdvAgencyMappingRepository _IRepository;
        #endregion

        #region Constructor
        public AdvertiserAgencyMappingController(IAdvAgencyMappingRepository typeRepository)
        {
            this._IRepository = typeRepository;
        }
        #endregion

        [HttpPost]
        [CheckModelForNull]
        [ValidateModelState]
        [Route("AddUpdate")]
        public HttpResponseMessage AddEditAdvertiserAgencyMapping(AdvertiserAgencyMappingModel model)
        {
            var response = _IRepository.PostMapping(model);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("Get/{AdvertiserId}")]
        public HttpResponseMessage GetAdvertiserAgencyMapping_ByAdvertiserId(string AdvertiserId)
        {
            var response = _IRepository.GetAdvertiserAgencyMapping(AdvertiserId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        //--------------------------------------------------------------------------------
        [HttpGet]
        [Route("List/{AdvertiserId}")]
        public HttpResponseMessage getAdvertiserAgencyMappingListbyId(string AdvertiserId)
        {
            var response = _IRepository.GetAdvertiserAgencyMappingListByAdvertiserId(AdvertiserId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        public HttpResponseMessage AdvertiserAgencyMappingList()
        {
            var response = _IRepository.GetAdvertiserAgencyMappingList();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpDelete]
        public HttpResponseMessage DeleteAdvertiserAgencyMapping(int id)
        {
            var response = _IRepository.DeleteAdvertiserAgencyMappingById(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
    }
}