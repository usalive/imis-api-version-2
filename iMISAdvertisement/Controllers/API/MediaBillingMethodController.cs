﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Asi.Controllers.API
{
    //[Authorize(Roles = "Admin")]
    public class MediaBillingMethodController : ApiController
    {
        #region Private Fields
        IMediaBillingMethodRepository _IRepository;
        #endregion

        #region Constructor
        public MediaBillingMethodController(IMediaBillingMethodRepository methodRepository)
        {
            this._IRepository = methodRepository;
        }
        #endregion

        #region Authenticated API's
        [HttpGet]
        public HttpResponseMessage Get()
        {
            var response = _IRepository.GetMediaBillingMethods();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            var response = _IRepository.GetMediaBillingMethodById(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        //* 2020-11-10 Change : MediaBillingMethod Table Does not exists Now
        //[HttpPost]
        //[CheckModelForNull]
        //[ValidateModelState]
        //public HttpResponseMessage Post(MediaBillingMethodModel billingMethodModel)
        //{
        //    var response = _IRepository.PostMediaBillingMethod(billingMethodModel);
        //    return Request.CreateResponse(HttpStatusCode.OK, response);
        //}

        //[HttpPut]
        //[CheckModelForNull]
        //[ValidateModelState]
        //public HttpResponseMessage PutMediaBillingMethod(MediaBillingMethodModel billingMethodModel)
        //{
        //    var response = _IRepository.PutMediaBillingMethod(billingMethodModel);
        //    return Request.CreateResponse(HttpStatusCode.OK, response);
        //}

        //[HttpDelete]
        //public HttpResponseMessage Delete(int id)
        //{
        //    var response = _IRepository.DeleteMediaBillingMethod(id);
        //    return Request.CreateResponse(HttpStatusCode.OK, response);
        //}
        #endregion
    }
}
