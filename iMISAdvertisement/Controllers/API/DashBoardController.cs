﻿using Asi.DAL.Model;
using Asi.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Asi.Controllers.API
{
    [RoutePrefix("api/Dashboard")]
    public class DashBoardController : ApiController
    {
        #region Private Fields
        IDashBoardRepository _IRepository;
        #endregion

        #region Constructor
        public DashBoardController(IDashBoardRepository dashBoardRepository)
        {
            this._IRepository = dashBoardRepository;
        }
        #endregion

        #region Authenticated API's

        [HttpPost]
        [Route("GetInsertionOrderCountByOrderStatus")]
        public HttpResponseMessage GetInserationOrderCountbyOrderStatusForCurrentYear(GetOrdersWithProposalStatusbyFilterOption model)
        {
            var response = _IRepository.GetInsertionOrderCountbyOrderStatusForCurrentYear(model);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("GetOrderCountByMonthlyForCurrentYear")]
        public HttpResponseMessage GetOrderCountbyMonthlyForCurrentYear()
        {
            var response = _IRepository.GetOrderCountbyMonthlyForCurrentYear();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("GetInsertionOrderCount")]
        public HttpResponseMessage GetInserationOrderCount()
        {
            var response = _IRepository.GetInsertionOrderCount();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("GetTotalNetAmount")]
        public HttpResponseMessage GetTotalNetAmount()
        {
            var response = _IRepository.GetTotalNetAmount();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("GetMediaAssetUsedInOrder")]
        public HttpResponseMessage GetMediaAssetUsedInOrder()
        {
            var response = _IRepository.GetMediaAssetUsedInOrder();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("GetAdvertiserUsedInOrder")]
        public HttpResponseMessage GetAdvertiserUsedInOrder()
        {
            var response = _IRepository.GetAdvertiserUsedInOrder();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [Route("GetOrderByAdvertisers")]
        public HttpResponseMessage GetOrderByAdvertisers()
        {
            var response = _IRepository.GetOrderByAdvertisers();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [Route("GetOrderDetailsByMediaAssets")]
        public HttpResponseMessage GetOrderDetailsByMediaAssets()
        {
            var response = _IRepository.GetOrderDetailsByMediaAssets();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [Route("GetOrdersByProposalStatus")]
        public HttpResponseMessage GetOrdersByProposalStatus()
        {
            var response = _IRepository.GetOrdersByProposalStatus();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }


        //[HttpPost]
        //[Route("getordersbyproposalstatus")]
        //public HttpResponseMessage GetOrdersByProposalStatus()
        //{
        //    var response = _IRepository.GetOrdersByProposalStatus();
        //    return Request.CreateResponse(HttpStatusCode.OK, response);
        //}


        [HttpGet]
        [Route("GetOrderCountByMonthlyForYearly")]
        public HttpResponseMessage GetOrderCountbyMonthlyForYearly()
        {
            var response = _IRepository.GetOrderCountbyMonthlyForYearly();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("GetDashboardData")]
        public HttpResponseMessage GetDashboardData()
        {
            var response = _IRepository.GetDashboardData();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        #endregion
    }
}
