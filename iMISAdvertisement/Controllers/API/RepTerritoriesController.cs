﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Asi.Controllers.API
{
    //[Authorize(Roles = "Admin")]
    [RoutePrefix("api/RepTerritories")]
    public class RepTerritoriesController : ApiController
    {
        #region Private Fields
        IRepTerritoriesRepository _IRepository;
        #endregion

        #region Constructor
        public RepTerritoriesController(IRepTerritoriesRepository repTerritories)
        {
            this._IRepository = repTerritories;
        }
        #endregion

        #region Authenticated API's
        [HttpGet]
        public HttpResponseMessage Get()
        {
            ResponseCommon response = _IRepository.GetRepTerritories();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("GetByRep/{id:int}")]
        public HttpResponseMessage GetByRep(int id)
        {
            ResponseCommon response = _IRepository.GetRepTerritoriesByRepId(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("GetByTerritory/{id:int}")]
        public HttpResponseMessage GetByTerritory(int id)
        {
            ResponseCommon response = _IRepository.GetRepTerritoriesByTerritoryId(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Post([FromBody]RepTerritoryModel repTerritoryModel)
        {
            ResponseCommon response = _IRepository.PostRepTerritory(repTerritoryModel);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPut]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Put(RepTerritoryModel repTerritoryModel)
        {
            ResponseCommon response = _IRepository.PutRepTerritory(repTerritoryModel);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpDelete]
        [Route("{repId:int}/{territoryId:int}")]
        public HttpResponseMessage Delete(int repId, int territoryId)
        {
            ResponseCommon response = _IRepository.DeleteRepTerritoryByRepIdTerritoryId(repId, territoryId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        #endregion
    }
}
