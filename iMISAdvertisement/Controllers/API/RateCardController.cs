﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Asi.Controllers.API
{
    //[Authorize(Roles = "Admin")]
    [RoutePrefix("api/RateCard")]
    public class RateCardController : ApiController
    {
        #region Private Fields
        IRateCardRepository _IRepository;
        #endregion

        #region Constructor
        public RateCardController(IRateCardRepository rateCardRepository)
        {
            this._IRepository = rateCardRepository;
        }
        #endregion

        #region Authenticated API's
        public HttpResponseMessage Get()
        {
            var response = _IRepository.GetRateCards();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        public HttpResponseMessage Get(int id)
        {
            var response = _IRepository.GetRateCardById(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("GetByMediaAsset/{id:int}")]
        public HttpResponseMessage GetRateCardByMediaAsset(int id)
        {
            var response = _IRepository.GetRateCardsByMediaAsset(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Post(PostRateCardModel rateCardModel)
        {
            var response = _IRepository.PostRateCard(rateCardModel);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Put(PostRateCardModel rateCardModel)
        {
            var response = _IRepository.PutRateCard(rateCardModel);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        public HttpResponseMessage Delete(int id)
        {
            var response = _IRepository.DeleteRateCard(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        #endregion
    }
}
