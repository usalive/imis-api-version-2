﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Asi.DAL.Model;
using Asi.Repository;
using System.Web;

namespace Asi.Controllers.API
{
    [RoutePrefix("api/IncompleteMediaOrder")]
    public class InCompleteMediaOrderController : ApiController
    {
        #region Private Fields
        IInCompleteMediaOrderRepository _IRepository;
        #endregion

        #region Constructor
        public InCompleteMediaOrderController(IInCompleteMediaOrderRepository inCompleteMediaOrderRepository)
        {
            this._IRepository = inCompleteMediaOrderRepository;
        }
        #endregion

        #region Authenticated API's

        [Route("GetByMediaOrder/{id:int}")]
        [HttpGet]
        public HttpResponseMessage GetByMediaOrderId(int id)
        {
            var response = _IRepository.GetInCompleteMediaOrderByMediaOrderId(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }


        [HttpPost]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Post(PostInCompleteMediaOrderModel model)
        {
            var response = _IRepository.PostInCompleteMediaOrder(model);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [Route("GetIncompleteOrders")]
        public HttpResponseMessage GetInCompleteOrders([FromBody]GetInCompleteOrdersbyFilterOption getInCompleteOrdersbyFilterOption)
        {
            var response = _IRepository.GetInCompleteOrdersByFilterOptions(getInCompleteOrdersbyFilterOption);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [Route("FillDropdownForIncompleteOrder")]
        public HttpResponseMessage FillDropdownForInCompleteOrder([FromBody]GetInCompleteOrdersbyFilterOption getInCompleteOrdersbyFilterOption)
        {
            var response = _IRepository.FillDropdownForInCompleteOrder(getInCompleteOrdersbyFilterOption);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }


        [Route("FillFilterDropdownInitially")]
        [HttpGet]
        public HttpResponseMessage FillFilterDropdownInitially()
        {
            var response = _IRepository.FillFilterDropdownInitially();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [Route("FillProductionDropdownInitially")]
        [HttpGet]
        public HttpResponseMessage FillProductionDropdownInitially()
        {
            var response = _IRepository.FillProductionDropdownInitially();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }


        #endregion
    }
}
