﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Asi.Controllers.API
{
    //[Authorize(Roles = "Admin")]
    [RoutePrefix("api/RateCardDetail")]
    public class RateCardDetailController : ApiController
    {
        #region Private Fields
        IRateCardDetailRepository _IRepository;
        #endregion

        #region Constructor
        public RateCardDetailController(IRateCardDetailRepository detailRepository)
        {
            this._IRepository = detailRepository;
        }
        #endregion


        #region Authenticated API's
        [Route("GetByRateCard/{rateCardId:int}")]
        public HttpResponseMessage GetByRateCard(int rateCardId)
        {
            var response = _IRepository.GetRateCardDetails(rateCardId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [Route("GetByRateCardAndColor/{rateCardId:int}/{adColorId:int}")]
        public HttpResponseMessage Get(int rateCardId, int adColorId)
        {
            var response = _IRepository.GetRateCardDetailsByColor(rateCardId, adColorId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [Route("GetRateCardCost/{rateCardId:int}/{adColorId:int}/{adSizeId:int}/{frequencyId:int}")]
        public HttpResponseMessage Get(int rateCardId, int adColorId, int adSizeId, int frequencyId)
        {
            var response = _IRepository.GetRateCardsCost(rateCardId, adColorId, adSizeId, frequencyId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [Route("GetRateCardsCostForPerWord/{rateCardId:int}/{adColorId:int}")]
        public HttpResponseMessage GetRateCardsCostForPerWord(int rateCardId, int adColorId)
        {
            var response = _IRepository.GetRateCardsCostForPerWord(rateCardId, adColorId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        public HttpResponseMessage Get(int id)
        {
            var response = _IRepository.GetRateCardDetailById(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Post(PostRateCardDetailModel cardDetailModel)
        {
            var response = _IRepository.PostRateCardDetail(cardDetailModel);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        #endregion
    }
}
