﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Asi.Controllers.API
{
    //[Authorize(Roles = "Admin")]
    public class ProductionStageController : ApiController
    {
        #region Private Fields
        IProductionStageRepository _IRepository;
        #endregion

        #region Constructor
        public ProductionStageController(IProductionStageRepository productionStageRepository)
        {
            this._IRepository = productionStageRepository;
        }
        #endregion

        #region Authenticated API's
        [HttpGet]
        public HttpResponseMessage Get()
        {
            var response = _IRepository.GetProductionStages();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            var response = _IRepository.GetProductionStageById(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Post(ProductionStageModel productionStageModel)
        {
            var response = _IRepository.PostProductionStage(productionStageModel);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPut]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage PutProductionStage(ProductionStageModel productionStageModel)
        {
            var response = _IRepository.PutProductionStage(productionStageModel);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            var response = _IRepository.DeleteProductionStage(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        #endregion
    }
}
