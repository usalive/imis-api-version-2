﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Asi.Controllers.API
{
    //[Authorize(Roles = "Admin")]
    public class MediaTypeController : ApiController
    {
        #region Private Fields
        IMediaTypeRepository _IRepository;
        #endregion

        #region Constructor
        public MediaTypeController(IMediaTypeRepository mediaTypeRepository)
        {
            this._IRepository = mediaTypeRepository;
        }
        #endregion

        #region Authenticated API's
        [HttpGet]
        public HttpResponseMessage Get()
        {
            ResponseCommon response = _IRepository.GetMediaTypes();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            ResponseCommon response = _IRepository.GetMediaTypesById(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Post([FromBody]PostMediaTypeModel mediaTypeModel)
        {
            ResponseCommon response = _IRepository.PostMediaType(mediaTypeModel);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPut]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Put(GetMediaTypeModel mediaTypeModel)
        {
            ResponseCommon response = _IRepository.PutMediaType(mediaTypeModel);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            ResponseCommon response = _IRepository.DeleteMediaType(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        #endregion
    }
}
