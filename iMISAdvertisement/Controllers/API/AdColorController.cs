﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Asi.Controllers.API
{
    //[Authorize(Roles = "Admin")]
    [RoutePrefix("api/AdColor")]
    public class AdColorController : ApiController
    {
        #region Private Fields
        IAdColorRepository _IRepository;
        #endregion

        #region Constructor
        public AdColorController(IAdColorRepository colorRepository)
        {
            this._IRepository = colorRepository;
        }
        #endregion

        #region Authenticated API's
        [HttpGet]
        public HttpResponseMessage Get()
        {
            var response = _IRepository.GetAdColors();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("GetByRateCard/{rateCardId:int}")]
        public HttpResponseMessage GetByRateCard(int rateCardId)
        {
            var response = _IRepository.GetAdColorsByRateCardId(rateCardId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("GetByRateCardAndAdSize/{rateCardId:int}/{adSizeId:int}")]
        public HttpResponseMessage GetByRateCard(int rateCardId, int adSizeId)
        {
            var response = _IRepository.GetAdColorsByRateCardIdAndAdSizeId(rateCardId, adSizeId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            var response = _IRepository.GetAdColorById(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Post(AdColorModel colorModel)
        {
            var response = _IRepository.PostAdColor(colorModel);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPut]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage PutAdColor(AdColorModel colorModel)
        {
            var response = _IRepository.PutAdColor(colorModel);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            var response = _IRepository.DeleteAdColor(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        #endregion
    }
}
