﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Asi.Controllers.API
{
    //[Authorize(Roles = "Admin")]
    public class AdAdjustmentTargetController : ApiController
    {
        #region Private Fields
        IAdvertiserAgencyMappingRepository _IRepository;
        #endregion

        #region Constructor
        public AdAdjustmentTargetController(IAdvertiserAgencyMappingRepository targetRepository)
        {
            this._IRepository = targetRepository;
        }
        #endregion


        #region Authenticated API's
        [HttpGet]
        public HttpResponseMessage Get()
        {
            ResponseCommon response = _IRepository.GetAdAdjustmentTargets();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            ResponseCommon response = _IRepository.GetAdAdjustmentTargetById(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        //* 2020-11-10 Change : AdAdjustmentTarget Table does not exists now
        //[CheckModelForNull]
        //[ValidateModelState]
        //public HttpResponseMessage Post([FromBody]GetAdAdjustmentTargetModel model)
        //{
        //    ResponseCommon response = _IRepository.PostAdAdjustmentTarget(model);
        //    return Request.CreateResponse(HttpStatusCode.OK, response);
        //}

        //[HttpPut]
        //[CheckModelForNull]
        //[ValidateModelState]
        //public HttpResponseMessage Put(GetAdAdjustmentTargetModel model)
        //{
        //    ResponseCommon response = _IRepository.PutAdAdjustmentTarget(model);
        //    return Request.CreateResponse(HttpStatusCode.OK, response);
        //}

        //[HttpDelete]
        //public HttpResponseMessage Delete(int id)
        //{
        //    ResponseCommon response = _IRepository.DeleteAdAdjustmentTarget(id);
        //    return Request.CreateResponse(HttpStatusCode.OK, response);
        //}
        #endregion
    }
}
