﻿using Asi.DAL.Model;
using Asi.Repository;
using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Asi.Controllers.API
{
    [RoutePrefix("api/MediaOrderLines")]
    public class MediaOrderLinesController : ApiController
    {
        #region Private Fields
        IMediaOrderLinesRepository _IRepository;
        #endregion

        #region Constructor
        public MediaOrderLinesController(IMediaOrderLinesRepository mediaOrderLinesRepository)
        {
            this._IRepository = mediaOrderLinesRepository;
        }
        #endregion

        #region API's
        [Route("GetByMediaOrder/{id:int}")]
        [HttpGet]
        public HttpResponseMessage GetByMediaOrderId(int id)
        {
            var response = _IRepository.GetMediaOrderLinesByMediaOrderId(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [Route("GetLogoByBuyId/{id:int}")]
        [HttpGet]
        public HttpResponseMessage GetLogoByBuyId(int id)
        {
            var response = _IRepository.GetLogoByBuyId(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage PostMediaOrderLine(PostMediaOrderLineModel lineModel)
        {
            var response = _IRepository.AddMediaOrderLine(lineModel);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpDelete]
        [Route("ByMediaOrderId/{mediaOrderId:int}")]
        public HttpResponseMessage DeleteMediaOrderLine([FromUri]int mediaOrderId)
        {
            var response = _IRepository.DeleteMediaOrderLineByMediaOrderId(mediaOrderId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [Route("RemoveByMediaLine")]
        public HttpResponseMessage DeleteMediaOrderLines(DeleteMediaOrderLinesModel lineModel)
        {
            var response = _IRepository.DeleteMediaOrderLineByMediaOrderLineIds(lineModel);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPut]
        [Route("UpdateMediaOrderLines")]
        public HttpResponseMessage UpdateMediaOrderLines(MediaOrderLineUpdateModel lineModel)
        {
            var response = _IRepository.UpdateMediaOrderLines(lineModel);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        #endregion
    }

    
}
