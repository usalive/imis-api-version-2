﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Asi.Controllers.API
{
    //[Authorize(Roles = "Admin")]
    [RoutePrefix("api/Frequency")]
    public class FrequencyController : ApiController
    {
        #region Private Fields
        IFrequencyRepository _IRepository;
        #endregion

        #region Constructor
        public FrequencyController(IFrequencyRepository frequencyRepository)
        {
            this._IRepository = frequencyRepository;
        }
        #endregion

        #region Authenticated API's
        [HttpGet]
        public HttpResponseMessage Get()
        {
            var response = _IRepository.GetFrequencies();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            var response = _IRepository.GetFrequencyById(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("getbyratecardandcolor/{rateCardId:int}/{adColorId:int}")]
        public HttpResponseMessage Get(int rateCardId, int adColorId)
        {
            var response = _IRepository.GetFrequencyByRateCardIdAndColordId(rateCardId, adColorId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("getbyratecardcolorandadsize/{rateCardId:int}/{adColorId:int}/{adSizeId:int}")]
        public HttpResponseMessage Get(int rateCardId, int adColorId, int adSizeId)
        {
            var response = _IRepository.GetFrequencyByRateCardIdColordIdAndAdSizeId(rateCardId, adColorId, adSizeId);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }


        [HttpPost]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Post(FrequencyModel frequency)
        {
            var response = _IRepository.PostFrequency(frequency);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPut]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage PutFrequency(FrequencyModel frequency)
        {
            var response = _IRepository.PutFrequency(frequency);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            var response = _IRepository.DeleteFrequency(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        #endregion
    }
}
