﻿using Asi.DAL.Model;
using Asi.Repository;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Asi.Controllers.API
{
    //[Authorize(Roles = "Admin")]
    [RoutePrefix("api/AdType")]
    public class AdTypeController : ApiController
    {
        #region Private Fields
        IAdTypeRepository _IRepository;
        #endregion

        #region Constructor
        public AdTypeController(IAdTypeRepository typeRepository)
        {
            this._IRepository = typeRepository;
        }
        #endregion

        #region Authenticated API's
        [HttpGet]
        public HttpResponseMessage Get()
        {
            var response = _IRepository.GetAdTypes();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            var response = _IRepository.GetAdTypeById(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpGet]
        [Route("GetAllAdTypeForPostOrder")]
        public HttpResponseMessage GetAllAdTypeForPostOrder()
        {
            var response = _IRepository.GetAllAdTypeForPostOrder();
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPost]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage Post(PostAdTypeModel adTypeModel)
        {
            var response = _IRepository.PostAdType(adTypeModel);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpPut]
        [CheckModelForNull]
        [ValidateModelState]
        public HttpResponseMessage PutAdType(GetAdTypeModel adTypeModel)
        {
            var response = _IRepository.PutAdType(adTypeModel);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            var response = _IRepository.DeleteAdType(id);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        #endregion
    }
}
