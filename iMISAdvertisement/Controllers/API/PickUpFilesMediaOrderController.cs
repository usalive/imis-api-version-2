﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Asi.DAL.Model;
using Asi.Repository;

namespace Asi.Controllers.API
{
    [RoutePrefix("api/PickUpFilesMediaOrder")]
    public class PickUpFilesMediaOrderController : ApiController
    {
        #region Private Fields
        IPickUpFilesMediaOrderRepository _IRepository;
        #endregion

        #region Constructor
        public PickUpFilesMediaOrderController(IPickUpFilesMediaOrderRepository pickUpFilesMediaOrderRepository)
        {
            this._IRepository = pickUpFilesMediaOrderRepository;
        }
        #endregion

        #region Authenticated API's
       
        [HttpPost]
        [Route("GetPickUpFilesOrders")]
        public HttpResponseMessage GetPickUpFilesOrders([FromBody]GetInCompleteOrdersbyFilterOption getInCompleteOrdersbyFilterOption)
        {
            var response = _IRepository.GetPickUpFilesOrdersByFilterOptions(getInCompleteOrdersbyFilterOption);
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        #endregion
    }
}
