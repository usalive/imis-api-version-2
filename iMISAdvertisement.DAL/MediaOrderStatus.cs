﻿namespace Asi.DAL
{
    public enum MediaOrderStatus
    {
        Proposal,
        Run,
        Cancel
    }

    public static class APIMediaOrderStatus
    {
        public static string Proposal = "Proposal";
        public static string Run = "Run";
        public static string Cancel = "Cancel";
    }

}
