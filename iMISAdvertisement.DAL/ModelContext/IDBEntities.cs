﻿using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;


namespace Asi.DAL.ModelContext
{
    public interface IDBEntities
    {
        DbSet<AdAdjustment> AdAdjustments { get; set; }
        DbSet<AdvertiserAgencyMap> AdvertiserAgencyMappings { get; set; }
        DbSet<AdvertiserRepTerritoryMap> AdvertiserRepTerritoryMappings { get; set; }

        DbSet<AdColor> AdColors { get; set; }
        DbSet<AdSize> AdSizes { get; set; }
        DbSet<AdType> AdTypes { get; set; }
        DbSet<AdIssueDate> AdIssueDates { get; set; }
        DbSet<MediaAsset> MediaAssets { get; set; }
        DbSet<MediaAssetGroup> MediaAssetGroups { get; set; }
        DbSet<MediaTypeRef> MediaTypes { get; set; }
        //DbSet<AdAdjustmentTarget> AdAdjustmentTargets { get; set; }
        DbSet<Position> Positions { get; set; }
        DbSet<ProductionStage> ProductionStages { get; set; }
        DbSet<ProductionStatu> ProductionStatuses { get; set; }
        DbSet<Separation> Separations { get; set; }
        DbSet<Rep> Reps { get; set; }
        DbSet<RepTerritory> RepTerritories { get; set; }
        DbSet<Territory> Territories { get; set; }
        //DbSet<MediaBillingMethod> MediaBillingMethods { get; set; }
        DbSet<RateCard> RateCards { get; set; }
        DbSet<RateCardDetail> RateCardDetails { get; set; }
        DbSet<Frequency> Frequencies { get; set; }
        DbSet<MediaOrderLine> MediaOrderLines { get; set; }
        DbSet<MediaOrderProductionDetail> MediaOrderProductionDetails { get; set; }
        DbSet<MediaOrderProductionStage> MediaOrderProductionStages { get; set; }
        DbSet<MediaOrderRep> MediaOrderReps { get; set; }
        DbSet<MediaOrder> MediaOrders { get; set; }
        DbSet<MediaOrderBuy> MediaOrdersBuys { get; set; }

        DbSet<MediaOrderSignedDocument> MediaOrderSignedDocuments { get; set; }
        //DbSet<MediaOrganisationMaster> MediaOrganisationMasters { get; set; }
        //DbSet<MediaContactMaster> MediaContactMasters { get; set; }
        DbSet<MediaInventoryMaster> MediaInventoryMasters { get; set; }
        DbSet<MediaInventoryDetail> MediaInventoryDetails { get; set; }
        ObjectResult<asi_GetOrdersByAdvertiserIds_Result> GetOrdersByAdvertiserIds(string advertisorId, string skipRecords, string takeRecords);

        ObjectResult<asi_GetOrdersbyFilterOption_Result> GetOrdersbyFilterOption(string buyIdFieldName, string buyIdType, string buyIdValue, string advertiserIdFieldName, string advertiserIds, string stIdFieldName, string stIds, string flightStartDateFieldName, string startFromDate, string startToDate, string flightEndDateFieldName, string endFromDate, string endToDate, string grossCostFieldName, string grossCostType, string grossCostValue, string netCostFieldName, string netCostType, string netCostValue, string mediaAssetFieldName, string mediaAssetType, string mediaAssetValue, string orderStatusFieldName, string orderStatusValue, string skipRecords, string takeRecords);
        ObjectResult<asi_GetSPOrdersForPostOrder_Result> GetSPOrdersForPostOrder(Nullable<int> mediaAssetId, Nullable<int> issueDateId, Nullable<int> adTypeId, Nullable<int> advertiserId, Nullable<int> productionStatusId, string status, string skipRecords, string takeRecords);
        ObjectResult<asi_GetDataForPostOrder_Result> GetDataForPostOrder(Nullable<int> mediaAssetId, Nullable<int> issueDateId, Nullable<int> adTypeId, Nullable<int> advertiserId,string stataus,  string skipRecords, string takeRecords);

        int SaveChanges();
        DbEntityEntry Entry(object entity);
        Database Databases { get; }
    }
}