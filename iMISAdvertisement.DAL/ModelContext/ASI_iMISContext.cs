﻿using System;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;

namespace Asi.DAL.ModelContext
{
    public class ASI_iMISContext : ASI_iMISEntities, IDBEntities
    {
        public ASI_iMISContext()
        //: base("name=key_ArtOFLivingEntities")
        {
        }

        public virtual DbSet<AdAdjustment> AdAdjustments { get; set; }
        public virtual DbSet<AdvertiserAgencyMap> AdvertiserAgencyMappings { get; set; }
        public virtual DbSet<AdvertiserRepTerritoryMap> AdvertiserRepTerritoryMappings { get; set; }
        public virtual DbSet<AdColor> AdColors { get; set; }
        public virtual DbSet<AdSize> AdSizes { get; set; }
        public virtual DbSet<AdType> AdTypes { get; set; }
        public virtual DbSet<AdIssueDate> AdIssueDates { get; set; }
        public virtual DbSet<MediaAsset> MediaAssets { get; set; }
        public virtual DbSet<MediaAssetGroup> MediaAssetGroups { get; set; }
        public virtual DbSet<MediaTypeRef> MediaTypes { get; set; }
        //public virtual DbSet<AdAdjustmentTarget> AdAdjustmentTargets { get; set; }
        public virtual DbSet<Position> Positions { get; set; }
        public virtual DbSet<ProductionStage> ProductionStages { get; set; }
        public virtual DbSet<ProductionStatu> ProductionStatuses { get; set; }
        public virtual DbSet<Separation> Separations { get; set; }
        public virtual DbSet<Rep> Reps { get; set; }
        public virtual DbSet<RepTerritory> RepTerritories { get; set; }
        public virtual DbSet<Territory> Territories { get; set; }
        //public virtual DbSet<MediaBillingMethod> MediaBillingMethods { get; set; }
        public virtual DbSet<RateCard> RateCards { get; set; }
        public virtual DbSet<RateCardDetail> RateCardDetails { get; set; }
        public virtual DbSet<Frequency> Frequencies { get; set; }
        public virtual DbSet<MediaOrderLine> MediaOrderLines { get; set; }
        public virtual DbSet<MediaOrderProductionDetail> MediaOrderProductionDetails { get; set; }
        public virtual DbSet<MediaOrderProductionStage> MediaOrderProductionStages { get; set; }
        public virtual DbSet<MediaOrderRep> MediaOrderReps { get; set; }
        public virtual DbSet<MediaOrder> MediaOrders { get; set; }
        public virtual DbSet<MediaOrderSignedDocument> MediaOrderSignedDocuments { get; set; }
        public virtual DbSet<MediaOrderBuy> MediaOrdersBuys { get; set; }
        public virtual DbSet<AdvertiserAgencyMap> AdvertiserAgencyMap { get; set; }

        public virtual DbSet<AdvertiserRepTerritoryMap> AdvertiserRepTerritoryMap { get; set; }
        //public virtual DbSet<MediaOrganisationMaster> MediaOrganisationMasters { get; set; }
        //public virtual DbSet<MediaContactMaster> MediaContactMasters { get; set; }

        public virtual ObjectResult<asi_GetOrdersByAdvertiserIds_Result> GetOrdersByAdvertiserIds(string advertisorId, string skipRecords, string takeRecords)
        {

            var advertisorIdParameter = advertisorId != null ?
                new ObjectParameter("AdvertisorId", advertisorId) :
                new ObjectParameter("AdvertisorId", typeof(string));


            var skipRecordsParameter = skipRecords != null ?
                new ObjectParameter("SkipRecords", skipRecords) :
                new ObjectParameter("SkipRecords", typeof(string));


            var takeRecordsParameter = takeRecords != null ?
                new ObjectParameter("TakeRecords", takeRecords) :
                new ObjectParameter("TakeRecords", typeof(string));


            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<asi_GetOrdersByAdvertiserIds_Result>("asi_GetOrdersByAdvertiserIds", advertisorIdParameter, skipRecordsParameter, takeRecordsParameter);
        }

        public virtual ObjectResult<asi_GetOrdersbyFilterOption_Result> GetOrdersbyFilterOption(string buyIdFieldName, string buyIdType, string buyIdValue, string advertiserIdFieldName, string advertiserIds, string stIdFieldName, string stIds, string flightStartDateFieldName, string startFromDate, string startToDate, string flightEndDateFieldName, string endFromDate, string endToDate, string grossCostFieldName, string grossCostType, string grossCostValue, string netCostFieldName, string netCostType, string netCostValue, string mediaAssetFieldName, string mediaAssetType, string mediaAssetValue, string orderStatusFieldName, string orderStatusValue, string skipRecords, string takeRecords)
        {

            var buyIdFieldNameParameter = buyIdFieldName != null ?
                new ObjectParameter("BuyIdFieldName", buyIdFieldName) :
                new ObjectParameter("BuyIdFieldName", typeof(string));


            var buyIdTypeParameter = buyIdType != null ?
                new ObjectParameter("BuyIdType", buyIdType) :
                new ObjectParameter("BuyIdType", typeof(string));


            var buyIdValueParameter = buyIdValue != null ?
                new ObjectParameter("BuyIdValue", buyIdValue) :
                new ObjectParameter("BuyIdValue", typeof(string));


            var advertiserIdFieldNameParameter = advertiserIdFieldName != null ?
                new ObjectParameter("AdvertiserIdFieldName", advertiserIdFieldName) :
                new ObjectParameter("AdvertiserIdFieldName", typeof(string));


            var advertiserIdsParameter = advertiserIds != null ?
                new ObjectParameter("AdvertiserIds", advertiserIds) :
                new ObjectParameter("AdvertiserIds", typeof(string));


            var stIdFieldNameParameter = stIdFieldName != null ?
                new ObjectParameter("StIdFieldName", stIdFieldName) :
                new ObjectParameter("StIdFieldName", typeof(string));


            var stIdsParameter = stIds != null ?
                new ObjectParameter("StIds", stIds) :
                new ObjectParameter("StIds", typeof(string));


            var flightStartDateFieldNameParameter = flightStartDateFieldName != null ?
                new ObjectParameter("FlightStartDateFieldName", flightStartDateFieldName) :
                new ObjectParameter("FlightStartDateFieldName", typeof(string));


            var startFromDateParameter = startFromDate != null ?
                new ObjectParameter("StartFromDate", startFromDate) :
                new ObjectParameter("StartFromDate", typeof(string));


            var startToDateParameter = startToDate != null ?
                new ObjectParameter("StartToDate", startToDate) :
                new ObjectParameter("StartToDate", typeof(string));


            var flightEndDateFieldNameParameter = flightEndDateFieldName != null ?
                new ObjectParameter("FlightEndDateFieldName", flightEndDateFieldName) :
                new ObjectParameter("FlightEndDateFieldName", typeof(string));


            var endFromDateParameter = endFromDate != null ?
                new ObjectParameter("EndFromDate", endFromDate) :
                new ObjectParameter("EndFromDate", typeof(string));


            var endToDateParameter = endToDate != null ?
                new ObjectParameter("EndToDate", endToDate) :
                new ObjectParameter("EndToDate", typeof(string));


            var grossCostFieldNameParameter = grossCostFieldName != null ?
                new ObjectParameter("GrossCostFieldName", grossCostFieldName) :
                new ObjectParameter("GrossCostFieldName", typeof(string));


            var grossCostTypeParameter = grossCostType != null ?
                new ObjectParameter("GrossCostType", grossCostType) :
                new ObjectParameter("GrossCostType", typeof(string));


            var grossCostValueParameter = grossCostValue != null ?
                new ObjectParameter("GrossCostValue", grossCostValue) :
                new ObjectParameter("GrossCostValue", typeof(string));


            var netCostFieldNameParameter = netCostFieldName != null ?
                new ObjectParameter("NetCostFieldName", netCostFieldName) :
                new ObjectParameter("NetCostFieldName", typeof(string));


            var netCostTypeParameter = netCostType != null ?
                new ObjectParameter("NetCostType", netCostType) :
                new ObjectParameter("NetCostType", typeof(string));


            var netCostValueParameter = netCostValue != null ?
                new ObjectParameter("NetCostValue", netCostValue) :
                new ObjectParameter("NetCostValue", typeof(string));

            var mediaAssetFieldNameParameter = mediaAssetFieldName != null ?
                new ObjectParameter("MediaAssetFieldName", mediaAssetFieldName) :
                new ObjectParameter("MediaAssetFieldName", typeof(string));


            var mediaAssetTypeParameter = mediaAssetType != null ?
                new ObjectParameter("MediaAssetType", mediaAssetType) :
                new ObjectParameter("MediaAssetType", typeof(string));


            var mediaAssetValueParameter = mediaAssetValue != null ?
                new ObjectParameter("MediaAssetValue", mediaAssetValue) :
                new ObjectParameter("MediaAssetValue", typeof(string));


            var orderStatusFieldNameParameter = orderStatusFieldName != null ?
                new ObjectParameter("OrderStatusFieldName", orderStatusFieldName) :
                new ObjectParameter("OrderStatusFieldName", typeof(string));


            var orderStatusValueParameter = orderStatusValue != null ?
                new ObjectParameter("OrderStatusValue", orderStatusValue) :
                new ObjectParameter("OrderStatusValue", typeof(string));


            var skipRecordsParameter = skipRecords != null ?
                new ObjectParameter("SkipRecords", skipRecords) :
                new ObjectParameter("SkipRecords", typeof(string));


            var takeRecordsParameter = takeRecords != null ?
                new ObjectParameter("TakeRecords", takeRecords) :
                new ObjectParameter("TakeRecords", typeof(string));


            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<asi_GetOrdersbyFilterOption_Result>("asi_GetOrdersbyFilterOption", buyIdFieldNameParameter, buyIdTypeParameter, buyIdValueParameter, advertiserIdFieldNameParameter, advertiserIdsParameter, stIdFieldNameParameter, stIdsParameter, flightStartDateFieldNameParameter, startFromDateParameter, startToDateParameter, flightEndDateFieldNameParameter, endFromDateParameter, endToDateParameter, grossCostFieldNameParameter, grossCostTypeParameter, grossCostValueParameter, netCostFieldNameParameter, netCostTypeParameter, netCostValueParameter, mediaAssetFieldNameParameter, mediaAssetTypeParameter, mediaAssetValueParameter, orderStatusFieldNameParameter, orderStatusValueParameter, skipRecordsParameter, takeRecordsParameter);
        }

        public virtual ObjectResult<asi_GetSPOrdersForPostOrder_Result> GetSPOrdersForPostOrder(Nullable<int> mediaAssetId, Nullable<int> issueDateId, Nullable<int> adTypeId, Nullable<int> advertiserId, Nullable<int> productionStatusId, string status, string skipRecords, string takeRecords)
        {

            var mediaAssetIdParameter = mediaAssetId.HasValue ?
                new ObjectParameter("mediaAssetId", mediaAssetId) :
                new ObjectParameter("mediaAssetId", typeof(int));


            var issueDateIdParameter = issueDateId.HasValue ?
                new ObjectParameter("issueDateId", issueDateId) :
                new ObjectParameter("issueDateId", typeof(int));


            var adTypeIdParameter = adTypeId.HasValue ?
                new ObjectParameter("adTypeId", adTypeId) :
                new ObjectParameter("adTypeId", typeof(int));


            var advertiserIdParameter = advertiserId.HasValue ?
                new ObjectParameter("advertiserId", advertiserId) :
                new ObjectParameter("advertiserId", typeof(int));


            var productionStatusIdParameter = productionStatusId.HasValue ?
                new ObjectParameter("productionStatusId", productionStatusId) :
                new ObjectParameter("productionStatusId", typeof(int));


            var statusParameter = status != null ?
                new ObjectParameter("status", status) :
                new ObjectParameter("status", typeof(string));


            var skipRecordsParameter = skipRecords != null ?
                new ObjectParameter("SkipRecords", skipRecords) :
                new ObjectParameter("SkipRecords", typeof(string));


            var takeRecordsParameter = takeRecords != null ?
                new ObjectParameter("TakeRecords", takeRecords) :
                new ObjectParameter("TakeRecords", typeof(string));


            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<asi_GetSPOrdersForPostOrder_Result>("asi_GetSPOrdersForPostOrder", mediaAssetIdParameter, issueDateIdParameter, adTypeIdParameter, advertiserIdParameter, productionStatusIdParameter, statusParameter, skipRecordsParameter, takeRecordsParameter);
        }
        public virtual ObjectResult<asi_GetDataForPostOrder_Result> GetDataForPostOrder(Nullable<int> mediaAssetId, Nullable<int> issueDateId, Nullable<int> adTypeId, Nullable<int> advertiserId, string status, string skipRecords, string takeRecords)
        {

            var mediaAssetIdParameter = mediaAssetId.HasValue ?
                new ObjectParameter("mediaAssetId", mediaAssetId) :
                new ObjectParameter("mediaAssetId", typeof(int));


            var issueDateIdParameter = issueDateId.HasValue ?
                new ObjectParameter("issueDateId", issueDateId) :
                new ObjectParameter("issueDateId", typeof(int));


            var adTypeIdParameter = adTypeId.HasValue ?
                new ObjectParameter("adTypeId", adTypeId) :
                new ObjectParameter("adTypeId", typeof(int));


            var advertiserIdParameter = advertiserId.HasValue ?
                new ObjectParameter("advertiserId", advertiserId) :
                new ObjectParameter("advertiserId", typeof(int));



            var statusParameter = status != null ?
                new ObjectParameter("status", status) :
                new ObjectParameter("status", typeof(string));


            var skipRecordsParameter = skipRecords != null ?
                new ObjectParameter("SkipRecords", skipRecords) :
                new ObjectParameter("SkipRecords", typeof(string));


            var takeRecordsParameter = takeRecords != null ?
                new ObjectParameter("TakeRecords", takeRecords) :
                new ObjectParameter("TakeRecords", typeof(string));


            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<asi_GetDataForPostOrder_Result>("asi_GetDataForPostOrder", mediaAssetIdParameter, issueDateIdParameter, adTypeIdParameter, advertiserIdParameter,statusParameter, skipRecordsParameter, takeRecordsParameter);
        }

        public virtual Database Databases
        {
            get { return base.Database; }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }

        public virtual DbEntityEntry Entry(object entity)
        {
            return base.Entry(entity);
        }        
    }
}