﻿using System.ComponentModel.DataAnnotations;

namespace Asi.DAL
{
    public enum BillingMethod
    {
        [Display(Name = "CPM")]
        CPM = 1,

        [Display(Name = "Flat Rate")]
        Flat_Rate = 2,

        [Display(Name = "PCI")]
        PCI = 3,

        [Display(Name = "Per Word")]
        Per_Word = 4,

        [Display(Name = "Sponsorship")]
        Sponsorship = 5,

        [Display(Name = "Web CPM")]
        Web_CPM = 6,
    }

    public enum AdAdjustmentTarget
    {
        [Display(Name = "Running Total")]
        Running_Total = 1
    }
}