//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Asi.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class MediaOrderRep
    {
        public int MediaOrderRepId { get; set; }
        public int MediaOrderId { get; set; }
        public int RepId { get; set; }
        public int TerritoryId { get; set; }
        public double Commission { get; set; }
        public double Split { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public System.Guid CreatedByUserKey { get; set; }
        public System.DateTime UpdatedOn { get; set; }
        public System.Guid UpdatedByUserKey { get; set; }
    
        public virtual MediaOrder MediaOrder { get; set; }
        public virtual UserMain UserMain { get; set; }
        public virtual UserMain UserMain1 { get; set; }
    }
}
