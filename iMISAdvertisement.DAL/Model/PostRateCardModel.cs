﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Asi.DAL.Model
{
    public class PostRateCardModel
    {
        public int RateCardId { get; set; }
        public int MediaAssetId { get; set; }
        public int MediaTypeId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "RateCardName is required")]
        [StringLength(255)]
        public string Name { get; set; }
        public int AdTypeId { get; set; }
        public int MediaBillingMethodId { get; set; }

        [DataType(DataType.Date)]
        public DateTime ValidFromDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime ValidToDate { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "ProductCode is required")]
        [StringLength(50)]
        public string ProductCode { get; set; }
        public int SourceRateCardId { get; set; }
        public bool IsCopy { get; set; }
    }
}