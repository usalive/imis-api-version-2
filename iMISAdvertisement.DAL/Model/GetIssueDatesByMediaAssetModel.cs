﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class GetIssueDatesByMediaAssetModel
    {
        public int MediaAssetId { get; set; }
        public string MediaAssetName { get; set; }
        public IEnumerable<GetIssueDateModel> IssueDates { get; set; }
    }
}
