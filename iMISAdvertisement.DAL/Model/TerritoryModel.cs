﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class TerritoryModel
    {
        public int TerritoryId { get; set; }

        [Required(AllowEmptyStrings =false)]
        [StringLength(100)]
        public string Name { get; set; }
    }
}
