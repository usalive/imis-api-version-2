﻿using System;
using System.Collections.Generic;

namespace Asi.DAL.Model
{
    public class FillProductionTabFilterDropdownInitiallyModel
    {
        public List<MediaAssetsModel> MediaAssets { get; set; }
        public List<AdIssueDatesModel> IssueDates { get; set; }
        public List<AdTypesModel> AdTypes { get; set; }
        public List<RepsModel> Reps { get; set; }
    }

    public class MediaAssetsModel
    {
        public int MediaAssetId { get; set; }
        public string MediaAssetName { get; set; }
    }

    public class AdIssueDatesModel
    {
        public int IssueDateId { get; set; }
        public DateTime CoverDate { get; set; }
    }

    public class AdTypesModel
    {
        public int AdTypeId { get; set; }
        public string AdTypeName { get; set; }
    }

    public class RepsModel
    {
        public int RepId { get; set; }
        public string RepName { get; set; }
    }


}
