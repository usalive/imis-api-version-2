﻿namespace Asi.DAL.Model
{
    public class MediaOrderCostModel
    {
        public int BuyId { get; set; }
        public decimal GrossCost { get; set; }
        public decimal NetCost { get; set; }
    }
}
