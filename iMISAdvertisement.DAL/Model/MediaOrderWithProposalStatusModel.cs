﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class MediaOrderWithProposalStatusModel
    {
        public int BuyId { get; set; }
        public int MediaOrderId { get; set; }
        public int MediaAssetId { get; set; }
        public string MediaAssetName { get; set; }
        public int IssueDateId { get; set; }
        public DateTime IssueDate { get; set; }
        public int AdTypeId { get; set; }
        public string AdTypeName { get; set; }
        public int AdcolorId { get; set; }
        public string AdcolorName { get; set; }
        public int AdSizeId { get; set; }
        public string AdSizeName { get; set; }
        public int FrequencyId { get; set; }
        public string FrequencyName { get; set; }
        public string AdvertiserId { get; set; }
        public string AdvertiserName { get; set; }
        public string BT_ID { get; set; }
        public string ST_ID { get; set; }
        public string BillToContactName { get; set; }
        public string OrderStatus { get; set; }
        public decimal GrossCost { get; set; }
        public decimal NetCost { get; set; }
        public DateTime? FlightStartDate { get; set; }
        public DateTime? FlightEndDate { get; set; }
    }

    public class MediaOrderWithProposalStatusModel1
    {
        public int BuyId { get; set; }
        // public int MediaOrderId { get; set; }
        // public int MediaAssetId { get; set; }
        // public string MediaAssetName { get; set; }
        // public int IssueDateId { get; set; }
        // public DateTime IssueDate { get; set; }
        // public int AdTypeId { get; set; }
        // public string AdTypeName { get; set; }
        // public int AdcolorId { get; set; }
        // public string AdcolorName { get; set; }
        // public int AdSizeId { get; set; }
        // public string AdSizeName { get; set; }
        // public int FrequencyId { get; set; }
        // public string FrequencyName { get; set; }
        public string AdvertiserId { get; set; }
        public string BT_ID { get; set; }
        public string ST_ID { get; set; }
        public string OrderStatus { get; set; }
        public decimal GrossCost { get; set; }
        public decimal NetCost { get; set; }
        //public DateTime? FlightStartDate { get; set; }
        //public DateTime? FlightEndDate { get; set; }
    }

}
