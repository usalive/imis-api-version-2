﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class GetOrdersByAdvertiserModel
    {
        public string AdvertiserId { get; set; }
        public int NoOfOrders { get; set; }
        public int BuyId { get; set; }
        public decimal NetCostAmount { get; set; }
        public string AdvertiserName { get; set; }
    }
}
