﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
   public class MediaOrdersBuyIdWiseStatusDetail
    {
        public string Status { get; set; }
        public int? NoOfOrders { get; set; }
        public int MediaAssetId { get; set; }
        public int IssueDateId { get; set; }
    }
 
}
