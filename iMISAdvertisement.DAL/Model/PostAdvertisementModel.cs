﻿using System.ComponentModel.DataAnnotations;

namespace Asi.DAL.Model
{
    public class PostAdvertisementModel
    {
        
        [Required(AllowEmptyStrings = false, ErrorMessage = "Advertiser required")]
        public string OrganisationName { get; set; }
        public string Email { get; set; }
        [MaxLength(200, ErrorMessage = "Maximum 200 characters allowed in Address")]
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string MemberType { get; set; }
        public string Home { get; set; }
        public string Mobile { get; set; }
        public string Work { get; set; }

    }
}
