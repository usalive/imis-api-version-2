﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{

    public class GetOrdersForMissingMaterialsbyFilterOption
    {
        public int MediaAssetId { get; set; }
        public int IssueDateId { get; set; }
        public int productionstatusId { get; set; }
    }
}
