﻿using System.Collections.Generic;

namespace Asi.DAL.Model
{
    public class DeleteMediaOrderLinesModel
    {
        public IEnumerable<int> MediaOrderLineIds { get; set; }
    }
}
