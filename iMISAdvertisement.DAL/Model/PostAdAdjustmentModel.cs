﻿using System.ComponentModel.DataAnnotations;

namespace Asi.DAL.Model
{
    public class PostAdAdjustmentModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "AdAdjustmentName required")]
        [MaxLength(255, ErrorMessage = "Maximum 255 characters allowed in AdAdjustmentName")]
        public string Name { get; set; }

        public int MediaAssetId { get; set; }

        public short AmountPercent { get; set; }

        public short SurchargeDiscount { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "AdjustmentTarget required")]
        public string AdjustmentTarget { get; set; }

        public double AdjustmentValue { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "ProductCode Required")]
        [MaxLength(50, ErrorMessage = "Maximum 50 characters allowed in ProductCode ")]
        public string ProductCode { get; set; }
    }
}
