﻿namespace Asi.DAL.Model
{
    public class GetAdAdjustmentModel : PostAdAdjustmentModel
    {
        public int AdAdjustmentId { get; set; }
        public string MediaAssetName { get; set; }
    }
}
