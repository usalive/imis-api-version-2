﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class GetTotalNetCostModel
    {
        public decimal NetCost { get; set; }
    }

    public class GetTotalNetCostYearlyModel
    {
        public List<GetTotalNetCostModel> PreviousYear { get; set; }
        public List<GetTotalNetCostModel> CurrentYear { get; set; }
    }
}
