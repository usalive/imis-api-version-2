﻿using System;

namespace Asi.DAL.Model
{
    public class MediaOrderModel
    {
        public int MediaOrderId { get; set; }
        public int BuyId { get; set; }

        public string MediaAssetLogo { get; set; }
        public int MediaAssetId { get; set; }
        public string MediaAssetName { get; set; }
        public int IssueDateId { get; set; }
        public DateTime IssueDate { get; set; }
        public int? AdSizeId { get; set; }
        public string AdSizeName { get; set; }
        public int? AdTypeId { get; set; }
        public string AdTypeName { get; set; }
        public int? AdColorId { get; set; }
        public string AdColorName { get; set; }
        public int? MediaBillingMethodId { get; set; }
        public string MediaBillingMethodName { get; set; }
        public int? FrequencyId { get; set; }
        public string FrequencyName { get; set; }
        public string OrderStatus { get; set; }

        public int? Likelihood { get; set; }
        public string BoothLocation { get; set; }
        public int? RateCardId { get; set; }
        public int? RateCardDetailId { get; set; }
        public string ProductCode { get; set; }
        public Nullable<decimal> Units { get; set; }
        public decimal BaseRateCardCost { get; set; }
        public decimal BaseCalculationCost { get; set; }
        public decimal RateCardCost { get; set; }
        public decimal GrossCost { get; set; }
        public decimal NetCost { get; set; }
        public string AdvertiserId { get; set; }
        public string AgencyId { get; set; }
        public string PageNumber { get; set; }
        public bool IsFrozen { get; set; }
        public bool BillToInd { get; set; }
        public System.Collections.Generic.IEnumerable<MediaOrderRepsModel> MediaOrderReps { get; set; }
        public string ST_ID { get; set; }
        public string BT_ID { get; set; }
        public string HeadLine { get; set; }
        public string CampaignName { get; set; }
        public string PositionName { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
