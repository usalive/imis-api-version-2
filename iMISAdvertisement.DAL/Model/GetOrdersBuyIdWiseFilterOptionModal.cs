﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class GetOrdersBuyIdWiseFilterOptionModal
    {
        public bool FilterIsDate { get; set; }
        public bool FilterIsNumber { get; set; }
        public bool FilterIsArray { get; set; }
        public string[] Ids { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
}
