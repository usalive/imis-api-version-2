﻿using System.ComponentModel.DataAnnotations;

namespace Asi.DAL.Model
{
    public class PostContactModel
    {
        public int? OrganisationId { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Advertiser required")]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Work { get; set; }
    }
}
