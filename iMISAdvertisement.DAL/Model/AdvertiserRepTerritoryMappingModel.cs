﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Asi.DAL.Model
{
    public class AdvertiserRepTerritoryMappingGetModel
    {
        public string AdvertiserID { get; set; }
        public string RepID { get; set; }
        public string TerritoryID { get; set; }
        public decimal Percentage { get; set; }

        public string AdvertiserName { get; set; }
        public string RepName { get; set; }
        public string TerritoryName { get; set; }
        public float Commission { get; set; }
        //Rep Name
        public string Name { get; set; }
    }
    public class AdvertiserRepTerritoryMappingModel
    {
        [Required]
        public string AdvertiserID { get; set; }

        public string AdvertiserName { get; set; }

        public List<AdvertiserRepTerritoryModel> ListRepTerritoryPercentage { get; set; }
    }

    public class AdvertiserRepTerritoryModel
    {
        [Required]
        public string RepID { get; set; }

        public string RepName { get; set; }

        [Required]
        public string TerritoryID { get; set; }

        public string TerritoryName { get; set; }

        [Required]
        public decimal Percentage { get; set; }
    }
}