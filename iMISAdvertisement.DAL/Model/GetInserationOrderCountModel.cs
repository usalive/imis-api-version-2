﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class GetInserationOrderCountModel
    {
        public int InserationOrderCount { get; set; }
    }

    public class GetInserationOrderCountYearlyModel
    {
        public List<GetInserationOrderCountModel> PreviousYear { get; set; }
        public List<GetInserationOrderCountModel> CurrentYear { get; set; }
    }
}
