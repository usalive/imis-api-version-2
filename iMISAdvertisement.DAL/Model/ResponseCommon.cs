﻿namespace Asi.DAL.Model
{
    public class ResponseCommon
    {
        public int StatusCode { get; set; }
        public object Data { get; set; }
        public object Message { get; set; }
    }

    public class ResponseCommonForDatatable
    {
        public int Draw { get; set; }
        public int RecordsFiltered { get; set; }
        public int RecordsTotal { get; set; }
        public dynamic Data { get; set; }
    }
}
