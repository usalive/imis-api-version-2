﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Asi.DAL.Model
{
    public class MediaOrderChangeStatusModel
    {
        public List<int> MediaOrderIds { get; set; }
        public string OrderStatus { get; set; }
        public int? Likelihood { get; set; }
    }
}
