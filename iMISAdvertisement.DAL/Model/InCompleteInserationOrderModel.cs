﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class InCompleteInsertionOrderModel
    {
        public int BuyId { get; set; }
        public int MediaOrderId { get; set; }
        public int MediaAssetId { get; set; }
        public int IssueDateId { get; set; }
        public int AdTypeId { get; set; }
        public string AdTypeName { get; set; }
        public int RepId { get; set; }
        public string RepName { get; set; }
        public int AdcolorId { get; set; }
        public string AdcolorName { get; set; }
        public int AdSizeId { get; set; }
        public string AdSizeName { get; set; }
        public int FrequencyId { get; set; }
        public string FrequencyName { get; set; }
        public Nullable<int> PositionId { get; set; }
        public string PositionName { get; set; }
        public string AdvertiserId { get; set; }
        public string BT_ID { get; set; }
        public string ST_ID { get; set; }
        public decimal GrossCost { get; set; }
        public decimal NetCost { get; set; }
        public double PageFraction { get; set; }
        public DateTime? FlightStartDate { get; set; }
        public DateTime? FlightEndDate { get; set; }
        public string TrackingNumber { get; set; }
        public Nullable<DateTime> MaterialExpectedDate { get; set; }
        public Nullable<int> PickupMediaOrderId { get; set; }
        public Nullable<bool> OnHandInd { get; set; }
        public Nullable<bool> Completed { get; set; }
        public Nullable<bool> NewPickupInd { get; set; }
        public Nullable<bool> CreateExpectedInd { get; set; }
        public Nullable<bool> ChangesInd { get; set; }
        public string PickupIndFromMediaAsset { get; set; }
        public DateTime PickupIndFromIssueDate { get; set; }
        public string PickupIndFromPg { get; set; }
    }
}
