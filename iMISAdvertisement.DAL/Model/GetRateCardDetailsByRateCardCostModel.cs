﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class GetRateCardDetailsByRateCardCostModel
    {
        public int RateCardDetailId { get; set; }

        public int AdSizeId { get; set; }

        public string AdSizeName { get; set; }

        public int FrequencyId { get; set; }

        public string FrequencyName { get; set; }

        public decimal RateCardCost { get; set; }
    }
}
