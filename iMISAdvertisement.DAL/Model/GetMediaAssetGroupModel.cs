﻿namespace Asi.DAL.Model
{
    public class GetMediaAssetGroupModel : PostMediaAssetGroupModel
    {
        public int MediaAssetGroupId { get; set; }
    }
}
