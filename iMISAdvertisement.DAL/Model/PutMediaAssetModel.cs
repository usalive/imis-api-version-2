﻿namespace Asi.DAL.Model
{
    public class PutMediaAssetModel : PostMediaAssetModel
    {
        public int MediaAssetId { get; set; }
    }
}
