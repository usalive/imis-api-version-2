﻿using System.Collections.Generic;

namespace Asi.DAL.Model
{
    public class GetDashboardModel
    {
        public List<GetTotalNetCostYearlyModel> TotalNetCostModel { get; set; }
        public List<GetUsedMediaAssetYearlyModel> UsedMediaAssetCountModel { get; set; }
        public List<GetUsedAdvertiserYearlyModel> UsedAdvertiserCountModel { get; set; }
        public List<GetInserationOrderCountYearlyModel> InserationOrderCountModel { get; set; }
        public List<GetOrderCountByMonthWise> OrderCountAndRevenueByMonthWise { get; set; }
        public List<GetOrderCountByYearlyModel> OrderCountByMonthAndYear { get; set; }
        public List<MediaOrdersBuyIdWiseStatusDetail> OrderCountByStatus { get; set; }
        //public List<GetOrdersByAdvertiserModel> OrdersByAdvertiser { get; set; }
        //public List<GetOrdersDetailsByMediaAssetsModel> OrdersByMediaAssets { get; set; }
        //public List<MediaOrderWithProposalStatusModel> OrderWithProposalStatus { get; set; }
    }
}
