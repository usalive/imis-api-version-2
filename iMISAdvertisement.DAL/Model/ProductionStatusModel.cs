﻿using System.ComponentModel.DataAnnotations;

namespace Asi.DAL.Model
{
    public class ProductionStatusModel
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string Name { get; set; }

        public int ProductionStatusId { get; set; }
    }
}