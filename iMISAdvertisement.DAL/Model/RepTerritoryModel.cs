﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class RepTerritoryModel
    {
        public int RepId { get; set; }
        public int TerritoryId { get; set; }
        public double Commission { get; set; }
    }
}
