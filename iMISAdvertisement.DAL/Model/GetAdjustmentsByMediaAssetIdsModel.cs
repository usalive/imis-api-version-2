﻿using System.Collections.Generic;

namespace Asi.DAL.Model
{
    public class GetAdjustmentsByMediaAssetIdsModel
    {
        public IEnumerable<int> MediaAssetIds { get; set; }
    }
}
