﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class MediaOrdersBuyIdWise
    {
        public int BuyId { get; set; }
        public string AdvertiserId { get; set; }
        public string AgencyId { get; set; }
        public string BT_ID { get; set; }
        public string ST_ID { get; set; }
        public decimal? GrossCost { get; set; }
        public decimal? NetCost { get; set; }
        public DateTime? FlightStartDate { get; set; }
        public DateTime? FlightEndDate { get; set; }
        public IEnumerable<MediaOrdersBuyIdWiseStatusDetail> OrderStatus { get; set; }
        public string MediaAssets { get; set; }
    }
}
