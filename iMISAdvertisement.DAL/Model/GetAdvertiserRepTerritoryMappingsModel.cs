﻿namespace Asi.DAL.Model
{
    public class GetAdvertiserRepTerritoryMappingsModel
    {
        public int AdvertiserRepTerritoryMapId { get; set; }
        public string AdvertiserId { get; set; }
        public string AdvertiserName { get; set; }
        public string RepId { get; set; }
        public string RepName { get; set; }
        public string TerritoryId { get; set; }
        public string TerritoryName { get; set; }
        public decimal Percentage { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public System.Guid CreatedByUserKey { get; set; }
        public System.DateTime UpdatedOn { get; set; }
        public System.Guid UpdatedByUserKey { get; set; }
    }
}