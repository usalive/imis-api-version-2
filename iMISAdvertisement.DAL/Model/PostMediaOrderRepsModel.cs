﻿using System;
using System.ComponentModel.DataAnnotations;
namespace Asi.DAL.Model
{
    public class PostMediaOrderRepsModel
    {
        public int RepId { get; set; }
        public int TerritoryId { get; set; }
        public double Commission { get; set; }
        public double Split { get; set; }
    }


    public class PostImportMediaOrderRepsModel
    {
        public int RepId { get; set; }
        public int TerritoryId { get; set; }
        public double Commission { get; set; }
        public double Split { get; set; }
        public string CreatedBy { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CreatedDate { get; set; }
        public string CreatedTime { get; set; }
    }
}
