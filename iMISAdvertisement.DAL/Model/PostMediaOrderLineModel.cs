﻿using System;
using System.Collections.Generic;

namespace Asi.DAL.Model
{

    public class PostMediaOrderLineModel
    {
        public List<int> MediaOrderIds { get; set; }
        public string AdAdjustmentName { get; set; }
        public short AmountPercent { get; set; }
        public short SurchargeDiscount { get; set; }
        public double AdjustmentValue { get; set; }
        public double AdjustmentAmount { get; set; }
        public string ProductCode { get; set; }
        public bool GrossInd { get; set; }
    }


    public class PostImportMediaOrderLineModel
    {
        public List<int> MediaOrderIds { get; set; }
        public string AdAdjustmentName { get; set; }
        public short AmountPercent { get; set; }
        public short SurchargeDiscount { get; set; }
        public double AdjustmentValue { get; set; }
        public double AdjustmentAmount { get; set; }
        public string ProductCode { get; set; }
        public bool GrossInd { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedTime { get; set; }
    }

}
