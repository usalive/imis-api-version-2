﻿using System.ComponentModel.DataAnnotations;

namespace Asi.DAL.Model
{
    public class GetAdAdjustmentTargetModel
    {
        public int AdAdjustmentTargetId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "AdAdjustmentTargetName is required")]
        [MaxLength(50, ErrorMessage = "AdAdjustmentTargetName maxmimum 50 characters allowed")]
        public string Name { get; set; }
    }
}
