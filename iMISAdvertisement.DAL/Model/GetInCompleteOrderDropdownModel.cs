﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class FillInCompleteDropdownsModel
    {
        public int BuyId { get; set; }
        public List<InCompleteMediaAsset> MediaAssets { get; set; }
        public List<InCompleteIssueDate> IssueDates { get; set; }
        public List<InCompleteAdType> AdTypes { get; set; }
        public List<InCompleteRep> Reps { get; set; }
    }

    public class TempInCompleteModel
    {
        public int BuyId { get; set; }
        public int MediaAssetId { get; set; }
        public string MediaAssetName { get; set; }
        public int IssueDateId { get; set; }
        public DateTime CoverDate { get; set; }
        public int AdTypeId { get; set; }
        public string AdTypeName { get; set; }
        public int RepId { get; set; }
        public string RepName { get; set; }
    }

    public class InCompleteMediaAsset
    {
        public int MediaAssetId { get; set; }
        public string MediaAssetName { get; set; }
    }

    public class InCompleteIssueDate
    {
        public int IssueDateId { get; set; }
        public DateTime CoverDate { get; set; }
    }

    public class InCompleteAdType
    {
        public int AdTypeId { get; set; }
        public string AdTypeName { get; set; }
    }

    public class InCompleteRep
    {
        public int RepId { get; set; }
        public string RepName { get; set; }
    }

}
