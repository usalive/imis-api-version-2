﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class GetInventoryDetailModel
    {
        public int InventoryId { get; set; }

        public int? AdSizeId { get; set; }

        public string AdSizeName { get; set; }

        public int? IssueDateId { get; set; }

        public DateTime IssueDate { get; set; }

        public string TotalInventory { get; set; }
    }
}
