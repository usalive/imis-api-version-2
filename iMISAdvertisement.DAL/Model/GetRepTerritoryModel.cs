﻿namespace Asi.DAL.Model
{
    public class GetRepTerritoryModel : RepTerritoryModel
    {
        public int RepTerretoryId { get; set; }
        public string RepName { get; set; }
        public string TerritoryName { get; set; }
    }
}
