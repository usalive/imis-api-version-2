﻿using System.Collections.Generic;

namespace Asi.DAL.Model.StaticList
{
    public class MediaBillingMethodNameValues
    {
        public int MediaBillingMethodId { get; set; }
        public string MediaBillingMethodName { get; set; }
    }

    //public static class Get_MediaBillingMethodName_Values
    //{
    //    public static List<MediaBillingMethodNameValues> GetMediaBillingMethodNameValues()
    //    {
    //        List<MediaBillingMethodNameValues> LstMediaBillingMethodNameValues = new List<MediaBillingMethodNameValues>
    //        {
    //            new MediaBillingMethodNameValues { MediaBillingMethodId = 1, MediaBillingMethodName = BillingMethod.CPM.ToString().Replace("_", " ") },
    //            new MediaBillingMethodNameValues { MediaBillingMethodId = 2, MediaBillingMethodName = BillingMethod.Flat_Rate.ToString().Replace("_", " ") },
    //            new MediaBillingMethodNameValues { MediaBillingMethodId = 3, MediaBillingMethodName = BillingMethod.PCI.ToString().Replace("_", " ") },
    //            new MediaBillingMethodNameValues { MediaBillingMethodId = 4, MediaBillingMethodName = BillingMethod.Per_Word.ToString().Replace("_", " ") },
    //            new MediaBillingMethodNameValues { MediaBillingMethodId = 5, MediaBillingMethodName = BillingMethod.Sponsorship.ToString().Replace("_", " ") },
    //            new MediaBillingMethodNameValues { MediaBillingMethodId = 6, MediaBillingMethodName = BillingMethod.Web_CPM.ToString().Replace("_", " ") }
    //        };
    //        return LstMediaBillingMethodNameValues;
    //    }
    //}
}