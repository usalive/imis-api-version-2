﻿using System.Collections.Generic;

namespace Asi.DAL.Model.StaticList
{
    public class AdAdjustmentTargetValues
    {
        public int AdAdjustmentTargetId { get; set; }
        public string Name { get; set; }
    }

    //public static class Get_AdAdjustmentTarget_Values
    //{
    //    public static List<AdAdjustmentTargetValues> GetAdAdjustmentTargetValues()
    //    {
    //        List<AdAdjustmentTargetValues> LstadAdjustmentTargetValues = new List<AdAdjustmentTargetValues>();
    //        LstadAdjustmentTargetValues.Add(new AdAdjustmentTargetValues { AdAdjustmentTargetId = 1, Name = "Running Total" });
    //        return LstadAdjustmentTargetValues;
    //    }
    //}
}