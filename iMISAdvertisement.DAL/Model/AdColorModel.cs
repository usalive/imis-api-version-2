﻿using System.ComponentModel.DataAnnotations;

namespace Asi.DAL.Model
{
    public class AdColorModel
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(255)]
        public string Name { get; set; }

        public int AdColorId { get; set; }
    }
}