﻿namespace Asi.DAL.Model
{
    public class GetMediaTypeModel : PostMediaTypeModel
    {
        public int MediaTypeId { get; set; }
    }
}
