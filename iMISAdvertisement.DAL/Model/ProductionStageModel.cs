﻿using System.ComponentModel.DataAnnotations;

namespace Asi.DAL.Model
{
    public class ProductionStageModel
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string Name { get; set; }

        public int ProductionStageId { get; set; }
    }
}