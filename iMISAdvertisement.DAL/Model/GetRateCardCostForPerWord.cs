﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class GetRateCardCostForPerWord
    {
        public int RateCardDetailId { get; set; }
        public string NoOfWord { get; set; }
        public string RateCardCost { get; set; }
        public string AdditionalWord { get; set; }
        public string PerWordCost { get; set; }
    }

    public class GetBasicRateCardCost
    {
        public int RateCardDetailId { get; set; }
        public decimal RateCardCost { get; set; }
    }

    public class AddMediaOrderResponse
    {
        public decimal GrossCost { get; set; }
        public decimal NetCost { get; set; }
        public List<int> MediaOrderIds { get; set; }
    }

}
