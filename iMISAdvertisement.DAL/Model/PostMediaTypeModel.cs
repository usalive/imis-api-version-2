﻿using System.ComponentModel.DataAnnotations;

namespace Asi.DAL.Model
{
    public class PostMediaTypeModel
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(255)]
        public string Name { get; set; }
    }
}
