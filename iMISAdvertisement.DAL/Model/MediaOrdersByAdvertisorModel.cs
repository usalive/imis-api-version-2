﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class MediaOrdersByAdvertisorModel
    {
        public int TotalCount { get; set; }
        public string AdvertiserId { get; set; }
        public string ST_ID { get; set; }
        public int? NoOfOrders { get; set; }
        public decimal? GrossCost { get; set; }
        public decimal? NetCost { get; set; }
    }
}
