﻿using System.ComponentModel.DataAnnotations;

namespace Asi.DAL.Model
{
    public class PositionModel
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string Name { get; set; }

        public int PositionId { get; set; }
    }
}