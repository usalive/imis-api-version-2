﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class AdvertiserAgencyMappingModel
    {
        //public int AdvertiserAgencyMappingId { get; set; }
        [Required]
        public string AdvertiserID { get; set; }
        [Required]
        public string AgencyID { get; set; }
        [Required]
        public string Relation { get; set; }
        public bool IsUpdate { get; set; }
        public string AdvertiserName { get; set; }
        public string AgencyName { get; set; }
    }
    public class AdvertiserAgencyMappingGetModel
    {
        public string AdvertiserID { get; set; }
        public string AgencyID { get; set; }
        public string Relation { get; set; }

        public string OrganizationName { get; set; }
        public string Id { get; set; }
    }

}