﻿using System.ComponentModel.DataAnnotations;

namespace Asi.DAL.Model
{
    public class FrequencyModel
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(255)]
        public string Name { get; set; }
        public int FrequencyId { get; set; }

    }
}
