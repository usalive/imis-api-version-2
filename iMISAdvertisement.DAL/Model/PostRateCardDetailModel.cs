﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace Asi.DAL.Model
{
    public class PostRateCardDetailModel
    {
        [Required(ErrorMessage = "RateCardId Required")]
        public int RateCardId { get; set; }
        [Required(ErrorMessage = "AdTypeId Required")]
        public int AdTypeId { get; set; }
        [Required(ErrorMessage = "AdColorId Required")]
        public int AdColorId { get; set; }
        [Required(ErrorMessage = "RateCardFrequency Required")]
        public List<PostRateCardDetailCostModel> RateCardDetailCost { get; set; }
    }
}