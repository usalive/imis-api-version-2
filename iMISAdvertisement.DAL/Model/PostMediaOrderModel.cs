﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Asi.DAL.Model
{
    public class PostMediaOrderModel
    {
        public int BuyId { get; set; }
        public string AdvertiserId { get; set; }
        public string AgencyId { get; set; }
        public string BT_ID { get; set; }
        public string ST_ID { get; set; }
        public bool BillToInd { get; set; }
        public int MediaAssetId { get; set; }
        public int RateCardId { get; set; }
        public int RateCardDetailId { get; set;} 
        public int AdSizeId { get; set; }
        public string ProductCode { get; set; }
        public string CampaignName { get; set; }
        public string BoothLocation { get; set; }
        public Nullable<int> Column { get; set; }
        public Nullable<double> Inches { get; set; }
        public Nullable<decimal> Units { get; set; }
        public string ClassifiedText { get; set; }
        public Nullable<decimal> PerWordCost { get; set; }
        public decimal BaseCalculationCost { get; set; }
        public decimal BaseRateCardCost { get; set; }
        public decimal RateCardCost { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> FlightStartDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> FlightEndDate { get; set; }
        public IEnumerable<PostMediaOrderRepsModel> MediaOrderReps { get; set; }
        public IEnumerable<int> MediaOrderIssueDates { get; set; }
        public bool IsImportOrder { get; set; }
    }


    public class PostImportMediaOrderModel
    {
        public int BuyId { get; set; }
        public string AdvertiserId { get; set; }
        public string AgencyId { get; set; }
        public string BT_ID { get; set; }
        public string ST_ID { get; set; }
        public bool BillToInd { get; set; }
        public int MediaAssetId { get; set; }
        public int RateCardId { get; set; }
        public int RateCardDetailId { get; set; }
        public string ProductCode { get; set; }
        public string CampaignName { get; set; }
        public Nullable<int> Column { get; set; }
        public Nullable<double> Inches { get; set; }
        public Nullable<decimal> Units { get; set; }
        public string ClassifiedText { get; set; }
        public Nullable<decimal> PerWordCost { get; set; }
        public decimal BaseCalculationCost { get; set; }
        public decimal BaseRateCardCost { get; set; }
        public decimal RateCardCost { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> FlightStartDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> FlightEndDate { get; set; }
        public IEnumerable<PostMediaOrderRepsModel> MediaOrderReps { get; set; }
        public IEnumerable<int> MediaOrderIssueDates { get; set; }
        public bool IsImportOrder { get; set; }
        public string CreatedBy { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime CreatedDate { get; set; }
        public string CreatedTime { get; set; }
        public int ImportBuyId { get; set; }
        public int ImportMediaOrderId { get; set; }
    }
}
