﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class GetInCompleteMediaOrderModel
    {
        public int MediaOrderProductionDetailId { get; set; }
        public int MediaOrderId { get; set; }
        public Nullable<bool> NewPickupInd { get; set; }
        public Nullable<int> PickupMediaOrderId { get; set; }
        public Nullable<bool> CreateExpectedInd { get; set; }
        public Nullable<System.DateTime> MaterialExpectedDate { get; set; }
        public string TrackingNumber { get; set; }
        public Nullable<bool> ChangesInd { get; set; }
        public Nullable<bool> OnHandInd { get; set; }
        public Nullable<bool> AdvertiserAgencyInd { get; set; }
        public Nullable<int> ProductionStatusId { get; set; }
        public string ProductionStatusName { get; set; }
        public Nullable<int> MaterialContactId { get; set; }
        public string OriginalFile { get; set; }
        public string ProofFile { get; set; }
        public string FinalFile { get; set; }
        public string WebAdUrl { get; set; }
        public Nullable<int> TearSheets { get; set; }
        public string HeadLine { get; set; }
        public Nullable<int> PositionId { get; set; }
        public Nullable<int> SeparationId { get; set; }
        public string PageNumber { get; set; }
        public string ProductionComment { get; set; }
        public string ClassifiedText { get; set; }
        public Nullable<bool> Completed { get; set; }
        public List<GetProductionStagesModel> ProductionStages { get; set; }
    }

    public class GetProductionStagesModel
    {
        public int MediaOrderProductionStagesId { get; set; }
        public int MediaOrderId { get; set; }
        public int ProductionStageId { get; set; }
        public string ProductionStagesName { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }

}


