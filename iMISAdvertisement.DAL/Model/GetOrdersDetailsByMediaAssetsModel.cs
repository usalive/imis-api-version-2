﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class GetOrdersDetailsByMediaAssetsModel
    {
        public int MediaAssetId { get; set; }
        public string MediaAssetName { get; set; }
        public DateTime LastOrderDate { get; set; }
        public int TotalQuantity { get; set; }
        public decimal Revenue { get; set; }
    }
}
