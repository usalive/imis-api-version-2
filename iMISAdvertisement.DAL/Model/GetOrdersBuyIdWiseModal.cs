﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class GetOrdersBuyIdWiseModal
    {
        public string FieldName { get; set; }
        public virtual GetOrdersBuyIdWiseFilterOptionModal FilterOption { get; set; }
    }
}
