﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class GetUsedAdvertiserModel
    {
        public int AdvertiserCount { get; set; }
    }

    public class GetUsedAdvertiserYearlyModel
    {
        public List<GetUsedAdvertiserModel> PreviousYear { get; set; }
        public List<GetUsedAdvertiserModel> CurrentYear { get; set; }
    }
}
