﻿namespace Asi.DAL.Model
{
    public class MediaOrderLineModel
    {
        public int MediaOrderLineId { get; set; }
        public int MediaOrderId { get; set; }
        public string ProductCode { get; set; }
        public int SequenceNumber { get; set; }
        public string AdAdjustmentName { get; set; }
        public int AdAdjustmentId { get; set; }
        public short AmountPercent { get; set; }
        public short SurchargeDiscount { get; set; }
        public double AdjustmentValue { get; set; }
        public bool GrossInd { get; set; }
    }

}
