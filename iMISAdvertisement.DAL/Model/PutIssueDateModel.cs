﻿using System.ComponentModel.DataAnnotations;

namespace Asi.DAL.Model
{
   public class PutIssueDateModel
    {
        public int IssueDateId { get; set; }
        public int MediaAssetId { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime CoverDate { get; set; }

        [StringLength(25)]
        public string IssueCode { get; set; }

        [StringLength(50)]
        public string IssueName { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime? AdClosingDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public System.DateTime? MaterialDueDate { get; set; }
    }
}
