﻿using System.ComponentModel.DataAnnotations;

namespace Asi.DAL.Model
{
    public class PostAdSizeModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "AdSizeName is required")]
        [MaxLength(255, ErrorMessage = "Maximum 255 characters allowed in AdSizeName")]
        public string Name { get; set; }
        public double PageFraction { get; set; }
        public int MediaAssetId { get; set; }
    }
}
