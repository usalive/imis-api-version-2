namespace Asi.DAL.Model
{
    public class GetIssueDateModel// : PostIssueDateModel
    {
        public int IssueDateId { get; set; }
        public string MediaAssetName { get; set; }
        public int MediaAssetId { get; set; }
        public System.DateTime CoverDate { get; set; }
        public string IssueCode { get; set; }
        public string IssueName { get; set; }
        public System.DateTime? MaterialDueDate { get; set; }
        public System.DateTime? AdClosingDate { get; set; }
    }
}
