﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class GetOrdersForPostOrderbyFilterOption
    {
        public int MediaAssetId { get; set; }
        public int IssueDateId { get; set; }
        public int AdTypeId { get; set; }
        public int AdvertiserId { get; set; }
        public string Status { get; set; }
        public int ProductionStatusId { get; set; }
        public bool IsCurrentYear { get; set; }
    }
}
