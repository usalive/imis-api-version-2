﻿using System.Collections.Generic;

namespace Asi.DAL.Model
{
    public class GetInventoryByMediaAssetModel
    {
        public int? MediaAssetId { get; set; }

        public string MediaAssetName { get; set; }

        public IEnumerable<GetInventoryDetailModel> Inventory { get; set; }
    }
}
