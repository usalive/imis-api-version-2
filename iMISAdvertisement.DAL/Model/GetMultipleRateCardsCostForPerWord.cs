﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class GetMultipleRateCardsCostForPerWord
    {
        public int RateCardDetailId { get; set; }
        public string NoOfWord { get; set; }
        public string RateCardCost { get; set; }
        public string AdditionalWord { get; set; }
        public string PerWordCost { get; set; }
    }
}
