﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Asi.DAL.Model
{
    public class PostMediaAssetGroupModel
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(255)]
        public string Name { get; set; }
        public Nullable<int> MediaAssetGroupParentGroupId { get; set; }
    }
}
