﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class GetMediaAssetModel
    {
        public int MediaAssetId { get; set; }
        public string MediaAssetName { get; set; }
        public int MediaAssetGroupId { get; set; }
        public string MediaAssetGroupName { get; set; }
        public int MediaTypeId { get; set; }
        public string MediaTypeName { get; set; }
        public string Logo { get; set; }
        public string ThumbLogo { get; set; }
        public string MediaCode { get; set; }
        public string ProductCode { get; set; }
        public int NumberOfActiveRateCards { get; set; }
    }
}
