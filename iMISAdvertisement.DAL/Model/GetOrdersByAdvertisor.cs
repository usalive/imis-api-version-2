﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class GetOrdersByAdvertisorModel
    {
        public int Draw { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
        public List<string> AdvertisorId { get; set; }
    }
}
