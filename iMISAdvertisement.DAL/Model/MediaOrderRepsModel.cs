﻿namespace Asi.DAL.Model
{
    public class MediaOrderRepsModel : PostMediaOrderRepsModel
    {
        public int MediaOrderRepId { get; set; }
        public int MediaOrderId { get; set; }
        public string RepsName { get; set; }
        public string TerritoryName { get; set; }
    }
}
