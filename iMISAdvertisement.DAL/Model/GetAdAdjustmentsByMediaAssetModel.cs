﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class GetAdAdjustmentsByMediaAssetModel
    {
        public int MediaAssetId { get; set; }
        public string MediaAssetName { get; set; }
        public IEnumerable<GetAdAdjustmentModel> AdAdjustments { get; set; }
    }
}
