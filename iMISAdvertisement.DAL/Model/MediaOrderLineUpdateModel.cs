﻿namespace Asi.DAL.Model
{
    public class MediaOrderLineUpdateModel
    {
        public int MediaOrderId { get; set; }
        public int MediaOrderLineId { get; set; }
        public bool MovedUp { get; set; }
    }
}
