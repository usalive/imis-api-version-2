﻿using System.Collections.Generic;
namespace Asi.DAL.Model
{
    public class GetRateCardDetailModel 
    {
        public int RateCardId { get; set; }
        public int AdTypeId { get; set; }
        public int MediaAssetId { get; set; }
        public string RateCardName { get; set; }
        public string AdTypeName { get; set; }
        public string MediaAssetName { get; set; }
        public IEnumerable<GetRateCardDetailCostModel> RateCardDetailCost { get; set; }
    }
}