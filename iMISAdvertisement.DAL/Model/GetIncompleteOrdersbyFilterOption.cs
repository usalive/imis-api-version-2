﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{

    public class GetInCompleteOrdersbyFilterOption
    {
        public int MediaAssetId { get; set; }
        public int IssueDateId { get; set; }
        public int AdTypeId { get; set; }
        public int RepId { get; set; }
    }

}
