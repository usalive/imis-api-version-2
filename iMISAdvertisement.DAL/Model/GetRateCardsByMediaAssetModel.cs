﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class GetRateCardsByMediaAssetModel
    {
        public int MediaAssetId { get; set; }
        public string MediaAssetName { get; set; }
        public IEnumerable<GetRateCardModel> RateCards { get; set; }
    }
}
