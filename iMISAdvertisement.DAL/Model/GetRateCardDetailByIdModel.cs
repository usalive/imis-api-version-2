﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Asi.DAL.Model
{
    public class GetRateCardDetailByIdModel
    {
        public int RateCardDetailId { get; set; }
        public int RateCardId { get; set; }
        public int AdTypeId { get; set; }
        public int AdSizeId { get; set; }
        public int FrequencyId { get; set; }
        public int AdColorId { get; set; }
        public int MediaAssetId { get; set; }
        public string RateCardName { get; set; }
        public string AdTypeName { get; set; }
        public string FrequencyName { get; set; }
        public string AdSizeName { get; set; }
        public string AdColorName { get; set; }
        public string MediaAssetName { get; set; }
        public decimal RateCardCost { get; set; }
    }
}