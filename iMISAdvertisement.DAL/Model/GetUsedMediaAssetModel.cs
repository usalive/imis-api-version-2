﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class GetUsedMediaAssetModel
    {
        public int MediaAssetCount { get; set; }
    }

    public class GetUsedMediaAssetYearlyModel
    {
        public List<GetUsedMediaAssetModel> PreviousYear { get; set; }
        public List<GetUsedMediaAssetModel> CurrentYear { get; set; }
    }
}
