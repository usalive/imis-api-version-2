﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class PostInCompleteMediaOrderModel
    {

        public int MediaOrderId { get; set; }

        public Nullable<bool> NewPickupInd { get; set; }
        public Nullable<int> PickupMediaOrderId { get; set; }
        public Nullable<bool> CreateExpectedInd { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> MaterialExpectedDate { get; set; }
        public string TrackingNumber { get; set; }
        public Nullable<bool> OnHandInd { get; set; }
        public Nullable<bool> ChangesInd { get; set; }
        public Nullable<bool> AdvertiserAgencyInd { get; set; }
        public Nullable<int> ProductionStatusId { get; set; }
        public Nullable<int> MaterialContactId { get; set; }
        public string OriginalFile { get; set; }
        public string OriginalFileExtension { get; set; }
        public string ProofFile { get; set; }
        public string ProofFileExtension { get; set; }
        public string FinalFile { get; set; }
        public string FinalFileExtension { get; set; }
        public string WebAdUrl { get; set; }
        public Nullable<int> TearSheets { get; set; }
        public string HeadLine { get; set; }
        public Nullable<int> PositionId { get; set; }
        public Nullable<int> SeparationId { get; set; }
        public string PageNumber { get; set; }
        public string ProductionComment { get; set; }
        public Nullable<bool> Completed { get; set; }

        public List<PostProductionStagesModel> ProductionStages { get; set; }
    }


    public class PostProductionStagesModel
    {
        public string MediaOrderProductionStagesId { get; set; }
        public int ProductionStageId { get; set; }
        public string UpdateBy { get; set; }
    }
}
