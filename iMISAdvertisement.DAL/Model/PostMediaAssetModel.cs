﻿using System.ComponentModel.DataAnnotations;

namespace Asi.DAL.Model
{
    public class PostMediaAssetModel
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(255)]
        public string Name { get; set; }
        public int MediaAssetGroupId { get; set; }
        public int MediaTypeId { get; set; }
        public string Logo { get; set; }

        [Required(AllowEmptyStrings = false)]
        [StringLength(25)]
        public string MediaCode { get; set; }

        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string ProductCode { get; set; }
    }
}
