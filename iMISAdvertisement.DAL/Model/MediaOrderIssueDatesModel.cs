﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class MediaOrderIssueDatesModel
    {
        public int MediaOrderId { get; set; }
        public int BuyId { get; set; }

        public int MediaAssetId { get; set; }
        public string MediaAssetName { get; set; }

        public string AdvertiserId { get; set; }

        public int IssueDateId { get; set; }
        public DateTime IssueDate { get; set; }

        public List<PostOrderIssueDate> IssueDates { get; set; }

        public int AdSizeId { get; set; }
        public string AdSizeName { get; set; }

        public int AdTypeId { get; set; }
        public string AdTypeName { get; set; }

        public int AdColorId { get; set; }
        public string AdColorName { get; set; }

        public int FrequencyId { get; set; }
        public string FrequencyName { get; set; }
    }
}
