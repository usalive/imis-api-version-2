﻿using System.ComponentModel.DataAnnotations;
namespace Asi.DAL.Model
{
    public class PostRateCardDetailCostModel
    {
        public int RateCardDetailId { get; set; }
        [Required(ErrorMessage = "AdSizeId Required")]
        public int AdSizeId { get; set; }
        [Required(ErrorMessage = "FrequencyId Required")]
        public int FrequencyId { get; set; }
        [Required(ErrorMessage = "RateCardCost Required")]
        public decimal RateCardCost { get; set; }
    }
}