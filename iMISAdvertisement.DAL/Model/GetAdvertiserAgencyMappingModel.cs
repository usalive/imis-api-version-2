﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class GetAdvertiserAgencyMappingModel
    {
        public int AdvertiserAgencyMapId { get; set; }
        public string AdvertiserId { get; set; }
        public string AdvertiserName { get; set; }
        public string AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string Relation { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public System.Guid CreatedByUserKey { get; set; }
        public System.DateTime UpdatedOn { get; set; }
        public System.Guid UpdatedByUserKey { get; set; }
    }
}