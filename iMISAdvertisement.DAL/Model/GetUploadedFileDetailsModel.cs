﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class GetUploadedFileDetailsModel
    {
        public string filePath { get; set; }
        public string fileExtension { get; set; }
        public string fileName { get; set; }
    }
}
