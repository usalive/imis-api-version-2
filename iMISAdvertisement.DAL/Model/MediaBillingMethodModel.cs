﻿using System.ComponentModel.DataAnnotations;

namespace Asi.DAL.Model
{
    public class MediaBillingMethodModel
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(255)]
        public string Name { get; set; }
        public int MediaBillingMethodId { get; set; }
    }
}