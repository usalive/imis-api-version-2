﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class FillInCompleteDropdownsInitiallyModel
    {
        public List<ProductionStatusModel> ProductionStatus { get; set; }
        public List<ProductionStageModel> ProductionStages { get; set; }
        public List<PositionModel> Positions { get; set; }
        public List<SeparationModel> Separations { get; set; }
    }
}
