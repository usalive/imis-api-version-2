﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Asi.DAL.Model
{
    public class GetIssueDatesByMediaAssetAndDateRangeModel
    {
        public int MediaAssetId { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }
    }
}
