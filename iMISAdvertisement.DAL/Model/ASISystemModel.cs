﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{

    public class PersonName
    {
        public string CustomTypeVariable { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
    }

    public class PrimaryOrganization
    {
        public string CustomOrgTypeVariable { get; set; }
        public string OrganizationPartyId { get; set; }
    }

    public class PostAdvertiserModel
    {
        public string SomeVars1 { get; set; }
        public string OrganizationName { get; set; }
     
        public PersonName PersonName { get; set; }
        public PrimaryOrganization PrimaryOrganization { get; set; }
        public Addresses Addresses { get; set; }
        public Emails Emails { get; set; }
        public Phones Phones { get; set; }
    }
    public class TokenAPIResponse
    {
        public string Access_token { get; set; }
        public string Token_type { get; set; }
        public string Expires_in { get; set; }
        public string UserName { get; set; }
        public string Issued { get; set; }
        public string Expires { get; set; }
    }

    public class PartyModel
    {
        public string PartyId { get; set; }
        public string PartyName { get; set; }
        public string OrganizationPartyId { get; set; }
        public string OrganizationPartyName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string Work { get; set; }
    }
    //13-2-2019 
    public class Emails
    {
        public string CustomEmailCVariable { get; set; }
        public List<EmailData> EmailDetail { get; set; }
    }   

    public class EmailData
    {
        public string CustomEmailTypevariable { get; set; }
        public string Address { get; set; }
        public string EmailType { get; set; }
        public bool IsPrimary { get; set; }
    }

    public class Phones
    {
        public string SomeVar { get; set; }
        public List<PhoneData> PhoneDetail { get; set; }
    }
    public class PhoneData
    {
        public string CustomPhoneTypevariable { get; set; }
        public string Number { get; set; }
        public string PhoneType { get; set; }
       
    }

    public class Addresses
    {
        public string CustomVariable { get; set; }
        public List<FullAddressData> FullAddressdata { get; set; }
    }
    public class FullAddressData
    {
        public string CustomFullAddressDataVariable { get; set; }
        public AddressData Address { get; set; }
        public string AddresseeText { get; set; }
        public string AddressPurpose { get; set; }
        public CommunicationPreferencesData CommunicationPreferences { get; set; }
        public string Phone { get; set; }
    }

    public class AddressData
    {
        public string CustomAddressDataVariable { get; set; }
        public AddressLines AddressLines { get; set; }
        public string CityName { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string CountrySubEntityCode { get; set; }
        public string CountrySubEntityName { get; set; }
        public string FullAddress { get; set; }
        public string PostalCode { get; set; }

    }

    public class AddressLines
    {
        public string CustomAddressLinesVariable { get; set; }
        public List<string> AddLines { get; set; }
    }

    public class CommunicationPreferencesData
    {
        public string CustomCommunicationVariable { get; set; }
        public List<CommunicationPreferencesDetails> CommunicationPreferencesDetails { get; set; }
    }

    public class CommunicationPreferencesDetails
    {
        public string CustomCommunicationPreferenceDataVariable { get; set; }
        public string Reason { get; set; }
    }
}
