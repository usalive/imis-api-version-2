﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class MediaOrderSignedDocumentsModel
    {
        public long MediaOrderSignedDocumentsId { get; set; }
        public int BuyId { get; set; }
        public string FilePath { get; set; }
        public string FilePathExtension { get; set; }

        public string FilePathName { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
    }

    public class MediaOrdersSignedDocumentsVM
    {
        public List<MediaOrderSignedDocumentsModel> objMediaOrderSignedDocumentsModelList { get; set; }
    }
}
