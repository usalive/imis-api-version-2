﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class MediaOrderCopyModel
    {
        public int MediaOrderId { get; set; }
        public int IssueDateId { get; set; }
    }
}
