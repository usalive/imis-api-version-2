﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace Asi.DAL.Model
{
    public class PostInventoryDetailModel
    {
        public int InventoryId { get; set; }
        [Required(ErrorMessage = "AdSizeId Required")]
        public int AdSizeId { get; set; }
        [Required(ErrorMessage = "IssueDateId Required")]
        public int IssueDateId { get; set; }
        public int TotalInventory { get; set; }
       
    }
}