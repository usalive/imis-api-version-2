﻿using System.Collections.Generic;

namespace Asi.DAL.Model
{
    public class GetRateCardDetailsByRateCardModel
    {
        public int RateCardId { get; set; }

        public string RateCardName { get; set; }

        public int AdTypeId { get; set; }

        public string AdTypeName { get; set; }

        public string MediaAssetName { get; set; }

        public int MediaAssetId { get; set; }

        public IEnumerable<GetRateCardDetailsByRateCardColorDetails> RateCardDetails { get; set; }
    }
}
