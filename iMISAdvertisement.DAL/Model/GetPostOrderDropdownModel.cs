﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{


    public class FillPostOrderDropdownsModel
    {
        public int BuyId { get; set; }
        public List<PostOrderMediaAsset> MediaAssets { get; set; }
        public List<PostOrderIssueDate> IssueDates { get; set; }
        public List<PostOrderAdType> AdTypes { get; set; }
        public List<PostOrderAdvertiser> Advertisers { get; set; }
    }

    public class TempPostOrderModel
    {
        public int BuyId { get; set; }
        public int MediaAssetId { get; set; }
        public string MediaAssetName { get; set; }
        public int IssueDateId { get; set; }
        public DateTime CoverDate { get; set; }
        public int AdTypeId { get; set; }
        public string AdTypeName { get; set; }
        public string AdvertiserId { get; set; }
        public string OrderStatus { get; set; }
    }

    public class PostOrderMediaAsset
    {
        public int MediaAssetId { get; set; }
        public string MediaAssetName { get; set; }
    }

    public class PostOrderIssueDate
    {
        public int IssueDateId { get; set; }
        public DateTime CoverDate { get; set; }
    }

    public class PostOrderAdType
    {
        public int AdTypeId { get; set; }
        public string AdTypeName { get; set; }
    }

    public class PostOrderAdvertiser
    {
        public string AdvertiserId { get; set; }
    }

}
