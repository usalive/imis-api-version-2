﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace Asi.DAL.Model
{
    public class PostInventoryModel
    {
        
        [Required(ErrorMessage = "MediaAssetId Required")]
        public int MediaAssetId { get; set; }
        public List<PostInventoryDetailModel> Inventory { get; set; }

    }
}