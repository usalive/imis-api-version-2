﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class GetRateCardDetailsByRateCardColorDetails
    {
        public int AdColorId { get; set; }

        public string AdColorName { get; set; }

        public IEnumerable<GetRateCardDetailsByRateCardCostModel> RateCardDetailCost { get; set; }
    }
}
