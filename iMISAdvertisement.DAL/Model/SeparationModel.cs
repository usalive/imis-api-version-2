﻿using System.ComponentModel.DataAnnotations;

namespace Asi.DAL.Model
{
    public class SeparationModel
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(50)]
        public string Name { get; set; }

        public int SeparationId { get; set; }
    }
}