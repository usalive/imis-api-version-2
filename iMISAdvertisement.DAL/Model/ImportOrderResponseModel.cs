﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class ImportOrderResponseModel
    {
        public int TotalImportOrder { get; set; }
        public int SuccessImportOrder { get; set; }
        public int FailImportOrder { get; set; }
        public string ExcelSheetPath { get; set; }
    }

}
