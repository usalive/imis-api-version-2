﻿namespace Asi.DAL.Model
{
    public class GetRateCardModel : PostRateCardModel
    {
        public string MediaAssetName { get; set; }
        public string MediaTypeName { get; set; }
        public string AdTypeName { get; set; }
        public string MediaBillingMethodName { get; set; }
    }
}