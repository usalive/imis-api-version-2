﻿using System.ComponentModel.DataAnnotations;

namespace Asi.DAL.Model
{
    public class PostAdTypeModel
    {
        [Required(AllowEmptyStrings = false)]
        [StringLength(255)]
        public string Name { get; set; }
    }
}
