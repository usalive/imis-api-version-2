﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.DAL.Model
{
    public class GetOrderCountByMonthWise
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public string MonthName { get; set; }
        public int NoOfOrders { get; set; }
        public decimal Revenue { get; set; }
    }

    public class GetOrderCountByYearlyModel
    {
        public List<GetOrderCountByMonthWise> PreviousYear { get; set; }
        public List<GetOrderCountByMonthWise> CurrentYear { get; set; }
    }

}
