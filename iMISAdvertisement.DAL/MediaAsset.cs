//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Asi.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class MediaAsset
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MediaAsset()
        {
            this.AdAdjustments = new HashSet<AdAdjustment>();
            this.AdIssueDates = new HashSet<AdIssueDate>();
            this.AdSizes = new HashSet<AdSize>();
            this.MediaInventoryMasters = new HashSet<MediaInventoryMaster>();
            this.MediaOrders = new HashSet<MediaOrder>();
            this.RateCards = new HashSet<RateCard>();
        }
    
        public int MediaAssetId { get; set; }
        public string MediaAssetName { get; set; }
        public int MediaAssetGroupId { get; set; }
        public int MediaTypeId { get; set; }
        public string Logo { get; set; }
        public string MediaCode { get; set; }
        public string ProductCode { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public System.Guid CreatedByUserKey { get; set; }
        public System.DateTime UpdatedOn { get; set; }
        public System.Guid UpdatedByUserKey { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdAdjustment> AdAdjustments { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdIssueDate> AdIssueDates { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdSize> AdSizes { get; set; }
        public virtual MediaAssetGroup MediaAssetGroup { get; set; }
        public virtual UserMain UserMain { get; set; }
        public virtual UserMain UserMain1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MediaInventoryMaster> MediaInventoryMasters { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MediaOrder> MediaOrders { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RateCard> RateCards { get; set; }
        public virtual MediaTypeRef MediaTypeRef { get; set; }
    }
}
