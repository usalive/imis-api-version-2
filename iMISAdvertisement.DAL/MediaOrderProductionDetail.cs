//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Asi.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class MediaOrderProductionDetail
    {
        public int MediaOrderProductionDetailId { get; set; }
        public int MediaOrderId { get; set; }
        public Nullable<bool> NewPickupInd { get; set; }
        public Nullable<int> PickupMediaOrderId { get; set; }
        public Nullable<bool> CreateExpectedInd { get; set; }
        public Nullable<bool> ChangesInd { get; set; }
        public Nullable<System.DateTime> MaterialExpectedDate { get; set; }
        public string TrackingNumber { get; set; }
        public Nullable<bool> OnHandInd { get; set; }
        public Nullable<bool> AdvertiserAgencyInd { get; set; }
        public Nullable<int> ProductionStatusId { get; set; }
        public Nullable<int> MaterialContactId { get; set; }
        public string OriginalFile { get; set; }
        public string ProofFile { get; set; }
        public string FinalFile { get; set; }
        public string WebAdUrl { get; set; }
        public Nullable<int> TearSheets { get; set; }
        public string HeadLine { get; set; }
        public Nullable<int> PositionId { get; set; }
        public Nullable<int> SeparationId { get; set; }
        public string PageNumber { get; set; }
        public string ProductionComment { get; set; }
        public Nullable<bool> Completed { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public System.Guid CreatedByUserKey { get; set; }
        public System.DateTime UpdatedOn { get; set; }
        public System.Guid UpdatedByUserKey { get; set; }
    
        public virtual MediaOrder MediaOrder { get; set; }
        public virtual UserMain UserMain { get; set; }
        public virtual UserMain UserMain1 { get; set; }
    }
}
