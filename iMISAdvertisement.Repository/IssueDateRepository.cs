﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace Asi.Repository
{
    public class IssueDateRepository : IIssueDateRepository
    {
        private readonly IDBEntities DB;
        public IssueDateRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        #region Public Methods
        public ResponseCommon GetIssueDates()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<GetIssueDateModel> lstIssueDates = new List<GetIssueDateModel>();
                lstIssueDates = DB.AdIssueDates.
                                        //Where(x => x.DeletedInd != true).
                                        Select(x => new GetIssueDateModel
                                        {
                                            IssueDateId = x.IssueDateId,
                                            MediaAssetId = x.MediaAssetId,
                                            CoverDate = x.CoverDate,
                                            IssueCode = x.IssueCode,
                                            IssueName = x.IssueName,
                                            AdClosingDate = x.AdClosingDate,
                                            MaterialDueDate = x.MaterialDueDate,
                                            MediaAssetName = x.MediaAsset.MediaAssetName
                                        }).ToList();
                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstIssueDates;
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
            }
            return objResponse;
        }
        public ResponseCommon GetIssueDatesById(int issueDateId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                GetIssueDateModel objIssueDate = DB.AdIssueDates
                                                .Where(x => /*x.DeletedInd != true &&*/ x.IssueDateId == issueDateId)
                                                .Select(x => new GetIssueDateModel
                                                {
                                                    IssueDateId = x.IssueDateId,
                                                    MediaAssetId = x.MediaAssetId,
                                                    CoverDate = x.CoverDate,
                                                    IssueCode = x.IssueCode,
                                                    IssueName = x.IssueName,
                                                    AdClosingDate = x.AdClosingDate,
                                                    MaterialDueDate = x.MaterialDueDate,
                                                    MediaAssetName = x.MediaAsset.MediaAssetName
                                                }).FirstOrDefault();
                response.Data = objIssueDate;
                response.StatusCode = ApiStatus.Ok;
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon GetIssueDatesByMediaAsset(int mediaAssetId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                List<GetIssueDatesByMediaAssetModel> objIssueDate = new List<GetIssueDatesByMediaAssetModel>();
                objIssueDate = DB.MediaAssets.Where(x => /*x.DeletedInd != true &&*/ x.MediaAssetId == mediaAssetId)
                       .Select(x => new GetIssueDatesByMediaAssetModel
                       {
                           MediaAssetId = x.MediaAssetId,
                           MediaAssetName = x.MediaAssetName,
                           IssueDates = x.AdIssueDates
                                                    //.Where(y => y.DeletedInd != true)
                                                    .Select(y => new GetIssueDateModel
                                                    {
                                                        IssueDateId = y.IssueDateId,
                                                        MediaAssetId = y.MediaAssetId,
                                                        MediaAssetName = y.MediaAsset.MediaAssetName,
                                                        CoverDate = y.CoverDate,
                                                        IssueCode = y.IssueCode,
                                                        IssueName = y.IssueName,
                                                        AdClosingDate = y.AdClosingDate,
                                                        MaterialDueDate = y.MaterialDueDate
                                                    })
                       }).ToList();
                response.Data = objIssueDate;
                response.StatusCode = ApiStatus.Ok;
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommonForDatatable GetIssueDatesByMediaAsset()
        {
            ResponseCommonForDatatable response = new ResponseCommonForDatatable();
            List<GetIssueDatesByMediaAssetModel> objIssueDate = new List<GetIssueDatesByMediaAssetModel>();
            int recordsTotal = 0;
            try
            {
                var context = HttpContext.Current;
                var currentRequest = HttpContext.Current.Request;

                string draw = currentRequest["draw"];
                string start = currentRequest["start"];
                string length = currentRequest["length"];
                int mediaAssetId = Convert.ToInt32(currentRequest["mediaAssetId"]);
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;

                string domainUrl = CommonUtility.CurrentHostDomain;
                objIssueDate = DB.MediaAssets.Where(x => /*x.DeletedInd != true &&*/ x.MediaAssetId == mediaAssetId)
                       .Select(x => new GetIssueDatesByMediaAssetModel
                       {
                           // MediaAssetId = x.MediaAssetId,
                           // MediaAssetName = x.MediaAssetName,
                           IssueDates = x.AdIssueDates
                                                    //.Where(y => y.DeletedInd != true)
                                                    .Select(y => new GetIssueDateModel
                                                    {
                                                        IssueDateId = y.IssueDateId,
                                                        MediaAssetId = y.MediaAssetId,
                                                        MediaAssetName = y.MediaAsset.MediaAssetName,
                                                        CoverDate = y.CoverDate,
                                                        IssueCode = y.IssueCode,
                                                        IssueName = y.IssueName,
                                                        AdClosingDate = y.AdClosingDate,
                                                        MaterialDueDate = y.MaterialDueDate
                                                    })
                       }).ToList();
                recordsTotal = objIssueDate[0].IssueDates.Count();
                objIssueDate[0].IssueDates = objIssueDate[0].IssueDates.Skip(skip).Take(pageSize).ToList();

                if (objIssueDate.Count > 0)
                {

                    response.Draw = Convert.ToInt32(draw);
                    response.RecordsTotal = recordsTotal;
                    response.RecordsFiltered = recordsTotal;
                    response.Data = objIssueDate[0].IssueDates;
                }
                else
                {

                    response.Draw = Convert.ToInt32(draw);
                    response.RecordsTotal = recordsTotal;
                    response.RecordsFiltered = recordsTotal;
                    response.Data = objIssueDate[0].IssueDates;
                }
                //response.data = objIssueDate;
                //response.statusCode = ApiStatus.Ok;
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);

                    }
                }
            }
            catch (Exception ex)
            {
                //LoggerRepsitory.WriteErrorLog(ex);
                //response.statusCode = ApiStatus.Exception;
                //response.message = ApiStatus.ExceptionMessge;

                var context = HttpContext.Current;
                var currentRequest = HttpContext.Current.Request;
                string draw = currentRequest["draw"];

                recordsTotal = objIssueDate.Count();
                response.Draw = Convert.ToInt32(draw);
                response.RecordsTotal = recordsTotal;
                response.RecordsFiltered = recordsTotal;
                response.Data = objIssueDate;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return response;
        }
        public ResponseCommon GetIssueDatesByMediaAssetAndDateRange(GetIssueDatesByMediaAssetAndDateRangeModel model)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                if (model.ToDate < model.FromDate)
                {
                    response.Message = ApiStatus.InvalidInputMessge;
                    response.StatusCode = ApiStatus.InvalidInput;
                    return response;
                }
                List<GetIssueDateModel> objIssueDate = DB.AdIssueDates
                                                 .Where(x => /*x.DeletedInd != true &&*/ x.MediaAssetId == model.MediaAssetId
                                                        && x.CoverDate >= model.FromDate && x.CoverDate <= model.ToDate)
                                                 .Select(x => new GetIssueDateModel
                                                 {
                                                     IssueDateId = x.IssueDateId,
                                                     MediaAssetId = x.MediaAssetId,
                                                     MediaAssetName = x.MediaAsset.MediaAssetName,
                                                     CoverDate = x.CoverDate,
                                                     IssueCode = x.IssueCode,
                                                     IssueName = x.IssueName,
                                                     AdClosingDate = x.AdClosingDate,
                                                     MaterialDueDate = x.MaterialDueDate
                                                 }).ToList();
                response.Data = objIssueDate;
                response.StatusCode = ApiStatus.Ok;
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon PostIssueDate(PostIssueDateModel objIssueDate)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                DateTime date = new DateTime();
                if ((objIssueDate.AdClosingDate != null && objIssueDate.AdClosingDate != date && objIssueDate.AdClosingDate > objIssueDate.CoverDate)
                   || (objIssueDate.MaterialDueDate != null && objIssueDate.MaterialDueDate != date && objIssueDate.MaterialDueDate > objIssueDate.CoverDate))
                {
                    response.StatusCode = ApiStatus.InvalidInput;
                    response.Message = "AdClosing Date & MaterialDue Date should not greater than Issue Date";
                    return response;
                }


                AdIssueDate objIssueDateEdit = new AdIssueDate();
                objIssueDateEdit.MediaAssetId = objIssueDate.MediaAssetId;
                objIssueDateEdit.CoverDate = objIssueDate.CoverDate;
                objIssueDateEdit.IssueCode = objIssueDate.IssueCode;
                objIssueDateEdit.IssueName = objIssueDate.IssueName;

                objIssueDateEdit.AdClosingDate = objIssueDate.AdClosingDate == date || objIssueDate.AdClosingDate == null ? null : (DateTime?)objIssueDate.AdClosingDate;
                objIssueDateEdit.MaterialDueDate = objIssueDate.MaterialDueDate == date || objIssueDate.MaterialDueDate == null ? null : (DateTime?)objIssueDate.MaterialDueDate;

                objIssueDateEdit.CreatedOn = DateTime.Now;
                //objIssueDateEdit.DeletedInd = false;

                //*
                objIssueDateEdit.UpdatedOn = DateTime.Now;
                objIssueDateEdit.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                objIssueDateEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                DB.AdIssueDates.Add(objIssueDateEdit);
                DB.SaveChanges();

                response.Data = objIssueDateEdit.IssueDateId;
                response.StatusCode = ApiStatus.Ok;

            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon PutIssueDate(PutIssueDateModel objIssueDate)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                DateTime date = new DateTime();
                if ((objIssueDate.AdClosingDate != null && objIssueDate.AdClosingDate != date && objIssueDate.AdClosingDate > objIssueDate.CoverDate)
                    || (objIssueDate.MaterialDueDate != null && objIssueDate.MaterialDueDate != date && objIssueDate.MaterialDueDate > objIssueDate.CoverDate))
                {
                    response.StatusCode = ApiStatus.InvalidInput;
                    response.Message = "AdClosing Date & MaterialDue Date should not greater than Issue Date";
                    return response;
                }



                AdIssueDate objIssueDateEdit = DB.AdIssueDates.Where(x => /*x.DeletedInd != true && */
                                                                    x.IssueDateId == objIssueDate.IssueDateId).FirstOrDefault();
                if (objIssueDateEdit != null)
                {
                    objIssueDateEdit.MediaAssetId = objIssueDate.MediaAssetId;
                    objIssueDateEdit.CoverDate = objIssueDate.CoverDate;
                    objIssueDateEdit.IssueCode = objIssueDate.IssueCode;
                    objIssueDateEdit.IssueName = objIssueDate.IssueName;

                    objIssueDateEdit.AdClosingDate = objIssueDate.AdClosingDate == date || objIssueDate.AdClosingDate == null ? null : (DateTime?)objIssueDate.AdClosingDate;
                    objIssueDateEdit.MaterialDueDate = objIssueDate.MaterialDueDate == date || objIssueDate.MaterialDueDate == null ? null : (DateTime?)objIssueDate.MaterialDueDate;

                    objIssueDateEdit.AdClosingDate = objIssueDate.AdClosingDate;
                    objIssueDateEdit.MaterialDueDate = objIssueDate.MaterialDueDate;
                    objIssueDateEdit.UpdatedOn = DateTime.Now;

                    //*
                    objIssueDateEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objIssueDateEdit).State = EntityState.Modified;
                    DB.SaveChanges();

                    response.Data = objIssueDate.IssueDateId;
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    response.StatusCode = ApiStatus.NotFound;
                    response.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon DeleteIssueDate(int issueDateId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                AdIssueDate objIssueDateEdit = null;
                int status = 0;
                objIssueDateEdit = DB.AdIssueDates
                                     .Where(x => /*x.DeletedInd != true &&*/ x.IssueDateId == issueDateId)
                                     .FirstOrDefault();
                if (objIssueDateEdit != null)
                {
                    //objIssueDateEdit.DeletedInd = true;

                    //*
                    objIssueDateEdit.UpdatedOn = DateTime.Now;
                    objIssueDateEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objIssueDateEdit).State = EntityState.Modified;
                    status = DB.SaveChanges();

                    response.Data = status <= 0 ? 0 : 1;
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    response.StatusCode = ApiStatus.NotFound;
                    response.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        #endregion
    }
}
