﻿//using Asi.DAL;
//using Asi.DAL.Model;
//using Asi.DAL.ModelContext;
//using System;
//using System.Collections.Generic;
//using System.Data.Entity;
//using System.Linq;

//namespace Asi.Repository
//{
//    public class AdvertiserRepository : IAdvertiserRepository
//    {
//        private readonly IDBEntities db;
//        public AdvertiserRepository(IDBEntities Context)
//        {
//            db = Context;
//        }

//        #region Public Methods
//        public ResponseCommon GetAdvertisers()
//        {
//            ResponseCommon objResponse = new ResponseCommon();
//            try
//            {
//                List<GetAdvertiserModel> lstAdvertisers = new List<GetAdvertiserModel>();
//                lstAdvertisers = db.MediaOrganisationMasters.Where(x => x.DeletedInd != true).
//                                        Select(x => new GetAdvertiserModel
//                                        {
//                                            OrganisationId = x.OrganisationId,
//                                            OrganisationName = x.OrganisationName,
//                                            Email = x.Email,
//                                            Address = x.Address,
//                                            City = x.City,
//                                            State  = x.State,
//                                            Country= x.Country,
//                                            PostalCode = x.PostalCode,
//                                            MemberType = x.MemberType,
//                                            Home = x.Home,
//                                            Mobile = x.Mobile,
//                                            Work = x.Work,
//                                        }).ToList();
//                objResponse.StatusCode = ApiStatus.Ok;
//                objResponse.Data = lstAdvertisers;
//            }
//            catch (Exception ex)
//            {
//                LoggerRepsitory.WriteErrorLog(ex);
//                objResponse.StatusCode = ApiStatus.Exception;
//                objResponse.Message = ApiStatus.ExceptionMessge;
//            }
//            return objResponse;
//        }
//        public ResponseCommon GetAdvertiserById(int advertiserId)
//        {
//            ResponseCommon response = new ResponseCommon();
//            try
//            {
//                GetAdvertiserModel objAdvertiser = db.MediaOrganisationMasters
//                                                .Where(x => x.DeletedInd != true && x.OrganisationId == advertiserId)
//                                                .Select(x => new GetAdvertiserModel
//                                                {
//                                                    OrganisationId = x.OrganisationId,
//                                                    OrganisationName = x.OrganisationName,
//                                                    Email = x.Email,
//                                                    Address = x.Address,
//                                                    City = x.City,
//                                                    State = x.State,
//                                                    Country = x.Country,
//                                                    PostalCode = x.PostalCode,
//                                                    MemberType = x.MemberType,
//                                                    Home = x.Home,
//                                                    Mobile = x.Mobile,
//                                                    Work = x.Work,
//                                                }).FirstOrDefault();
//                if (objAdvertiser != null)
//                {
//                    response.Data = objAdvertiser;
//                    response.StatusCode = ApiStatus.Ok;
//                }
//                else
//                {
//                    response.Message = ApiStatus.NotFoundMessage;
//                    response.StatusCode = ApiStatus.NotFound;
//                }
//            }
//            catch (Exception ex)
//            {
//                LoggerRepsitory.WriteErrorLog(ex);
//                response.StatusCode = ApiStatus.Exception;
//                response.Message = ApiStatus.ExceptionMessge;
//            }
//            return response;
//        }
//        //public ResponseCommon GetAdAdjustmentsByMediaAsset(int mediaAssetId)
//        //{
//        //    ResponseCommon response = new ResponseCommon();
//        //    try
//        //    {
//        //        var lstAdAdjustments = db.MediaAssets.Where(x => x.DeletedInd != true && x.MediaAssetId == mediaAssetId)
//        //            .Select(x => new GetAdAdjustmentsByMediaAssetModel
//        //            {
//        //                MediaAssetId = x.MediaAssetId,
//        //                MediaAssetName = x.MediaAssetName,
//        //                AdAdjustments = x.AdAdjustments.Where(y => y.DeletedInd != true).Select(y => new GetAdAdjustmentModel
//        //                {
//        //                    AdAdjustmentId = y.AdAdjustmentId,
//        //                    AdAdjustmentName = y.AdAdjustmentName,
//        //                    MediaAssetId = y.MediaAssetId,
//        //                    MediaAssetName = y.MediaAsset.MediaAssetName,
//        //                    AmountPercent = y.AmountPercent,
//        //                    SurchargeDiscount = y.SurchargeDiscount,
//        //                    AdjustmentTarget = y.AdjustmentTarget,
//        //                    AdjustmentValue = y.AdjustmentValue,
//        //                    ProductCode = y.ProductCode
//        //                })
//        //            }).ToList();
//        //        response.data = lstAdAdjustments;
//        //        response.statusCode = ApiStatus.Ok;
//        //    }
//        //    catch (Exception ex)
//        //    {
//        //        LoggerRepsitory.WriteErrorLog(ex);
//        //        response.statusCode = ApiStatus.Exception;
//        //        response.message = ApiStatus.ExceptionMessge;
//        //    }
//        //    return response;
//        //}
//        //public ResponseCommon GetAdAdjustmentsByMediaAssets(GetAdjustmentsByMediaAssetIdsModel model)
//        //{
//        //    ResponseCommon response = new ResponseCommon();
//        //    try
//        //    {
//        //        // Base Query
//        //        //SELECT aj.AdAdjustmentName,aj.AmountPercent,aj.SurchargeDiscount,aj.AdjustmentValue,COUNT(*) AS RecordCount
//        //        //FROM dbo.MediaAsset ma
//        //        //JOIN dbo.AdAdjustment aj ON aj.MediaAssetId = ma.MediaAssetId
//        //        //GROUP BY aj.AmountPercent,aj.SurchargeDiscount,aj.AdjustmentValue,aj.AdAdjustmentName

//        //        List<int> mediaAssetIds = model.MediaAssetIds.Distinct().ToList();
//        //        var lstAdAdjustments = (from adj in db.AdAdjustments
//        //                                join ma in db.MediaAssets on adj.MediaAssetId equals ma.MediaAssetId
//        //                                where adj.DeletedInd != true && ma.DeletedInd != true && mediaAssetIds.Contains(ma.MediaAssetId)
//        //                                group adj by new { adj.AmountPercent, adj.SurchargeDiscount, adj.AdjustmentValue, adj.AdAdjustmentName, adj.ProductCode } into g
//        //                                let RecordCount = g.Count()
//        //                                select new
//        //                                {
//        //                                    AdAdjustmentName = g.Key.AdAdjustmentName,
//        //                                    AmountPercent = g.Key.AmountPercent,
//        //                                    SurchargeDiscount = g.Key.SurchargeDiscount,
//        //                                    AdjustmentValue = g.Key.AdjustmentValue,
//        //                                    ProductCode = g.Key.ProductCode,
//        //                                    RecordCount = RecordCount
//        //                                }).ToList();
//        //        int i = 1;
//        //        response.data = lstAdAdjustments.Where(x => x.RecordCount == mediaAssetIds.Count)
//        //                        .Select(x => new
//        //                        {
//        //                            SrNo = i++,
//        //                            AdAdjustmentName = x.AdAdjustmentName,
//        //                            AmountPercent = x.AmountPercent,
//        //                            SurchargeDiscount = x.SurchargeDiscount,
//        //                            AdjustmentValue = x.AdjustmentValue,
//        //                            ProductCode = x.ProductCode
//        //                        }).ToList();
//        //        response.statusCode = ApiStatus.Ok;
//        //    }
//        //    catch (Exception ex)
//        //    {
//        //        LoggerRepsitory.WriteErrorLog(ex);
//        //        response.statusCode = ApiStatus.Exception;
//        //        response.message = ApiStatus.ExceptionMessge;
//        //    }
//        //    return response;
//        //}
//        public ResponseCommon PostAdvertiser(PostAdvertisementModel objAdvertiser)
//        {
//            ResponseCommon response = new ResponseCommon();
//            try
//            {

//                MediaOrganisationMaster objAdv = new MediaOrganisationMaster();
//                objAdv.OrganisationName = objAdvertiser.OrganisationName;
//                objAdv.Email = objAdvertiser.Email;
//                objAdv.Address = objAdvertiser.Address;
//                objAdv.City = objAdvertiser.City;
//                objAdv.State = objAdvertiser.State;
//                objAdv.Country = objAdvertiser.Country;
//                objAdv.PostalCode = objAdvertiser.PostalCode;
//                objAdv.Home = objAdvertiser.Home;
//                objAdv.Mobile = objAdvertiser.Mobile;
//                objAdv.Work = objAdvertiser.Work;
//                objAdv.City = objAdvertiser.City;
//                objAdv.State = objAdvertiser.State;
//                objAdv.Country = objAdvertiser.Country;
//                objAdv.CreatedOn = DateTime.Now;
//                objAdv.DeletedInd = false;

//                db.MediaOrganisationMasters.Add(objAdv);
//                    db.SaveChanges();

//                    response.Data = objAdv.OrganisationId;
//                    response.StatusCode = ApiStatus.Ok;
              
//            }
//            catch (Exception ex)
//            {
//                LoggerRepsitory.WriteErrorLog(ex);
//                response.StatusCode = ApiStatus.Exception;
//                response.Message = ApiStatus.ExceptionMessge;
//            }
//            return response;
//        }
//        //public ResponseCommon PutAdAdjustment(GetAdAdjustmentModel objAdAdjustment)
//        //{
//        //    ResponseCommon response = new ResponseCommon();
//        //    try
//        //    {
//        //        int isNameExist = db.AdAdjustments
//        //                            .Where(x => x.AdAdjustmentName.ToLower() == objAdAdjustment.AdAdjustmentName.ToLower()
//        //                                    && x.AdAdjustmentId != objAdAdjustment.AdAdjustmentId
//        //                                    && x.DeletedInd == false).Count();
//        //        if (isNameExist > 0)
//        //        {
//        //            response.statusCode = ApiStatus.AlreadyExist;
//        //            response.message = ApiStatus.AlreadyExistCustomMessge("Ad Adjustment Name");
//        //            return response;
//        //        }
//        //        AdAdjustment objAdAdjustmentEdit = db.AdAdjustments.Where(x => x.DeletedInd != true
//        //                                                && x.AdAdjustmentId == objAdAdjustment.AdAdjustmentId).FirstOrDefault();
//        //        if (objAdAdjustmentEdit != null)
//        //        {
//        //            objAdAdjustmentEdit.AdAdjustmentName = objAdAdjustment.AdAdjustmentName;
//        //            objAdAdjustmentEdit.MediaAssetId = objAdAdjustment.MediaAssetId;
//        //            objAdAdjustmentEdit.AmountPercent = objAdAdjustment.AmountPercent;
//        //            objAdAdjustmentEdit.SurchargeDiscount = objAdAdjustment.SurchargeDiscount;
//        //            objAdAdjustmentEdit.AdjustmentTarget = objAdAdjustment.AdjustmentTarget;
//        //            objAdAdjustmentEdit.AdjustmentValue = objAdAdjustment.AdjustmentValue;
//        //            objAdAdjustmentEdit.ProductCode = objAdAdjustment.ProductCode;
//        //            objAdAdjustmentEdit.ModifiedDate = DateTime.Now;
//        //            db.Entry(objAdAdjustmentEdit).State = EntityState.Modified;
//        //            db.SaveChanges();

//        //            response.data = objAdAdjustment.AdAdjustmentId;
//        //            response.statusCode = ApiStatus.Ok;
//        //        }
//        //        else
//        //        {
//        //            response.statusCode = ApiStatus.NotFound;
//        //            response.message = ApiStatus.NotFoundMessage;
//        //        }
//        //    }
//        //    catch (Exception ex)
//        //    {
//        //        LoggerRepsitory.WriteErrorLog(ex);
//        //        response.statusCode = ApiStatus.Exception;
//        //        response.message = ApiStatus.ExceptionMessge;
//        //    }
//        //    return response;
//        //}
//        //public ResponseCommon DeleteAdAdjustment(int AdAdjustmentId)
//        //{
//        //    ResponseCommon response = new ResponseCommon();
//        //    try
//        //    {
//        //        AdAdjustment objAdAdjustmentEdit = null;
//        //        int status = 0;
//        //        objAdAdjustmentEdit = db.AdAdjustments
//        //                             .Where(x => x.DeletedInd != true && x.AdAdjustmentId == AdAdjustmentId)
//        //                             .FirstOrDefault();
//        //        if (objAdAdjustmentEdit != null)
//        //        {
//        //            objAdAdjustmentEdit.DeletedInd = true;
//        //            db.Entry(objAdAdjustmentEdit).State = EntityState.Modified;
//        //            status = db.SaveChanges();

//        //            response.data = status <= 0 ? 0 : 1;
//        //            response.statusCode = ApiStatus.Ok;
//        //        }
//        //        else
//        //        {
//        //            response.statusCode = ApiStatus.NotFound;
//        //            response.message = ApiStatus.NotFoundMessage;
//        //        }
//        //    }
//        //    catch (Exception ex)
//        //    {
//        //        LoggerRepsitory.WriteErrorLog(ex);
//        //        response.statusCode = ApiStatus.Exception;
//        //        response.message = ApiStatus.ExceptionMessge;
//        //    }
//        //    return response;
//        //}
//        #endregion
//    }
//}
