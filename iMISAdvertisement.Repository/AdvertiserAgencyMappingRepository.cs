﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.Repository
{
    public class AdvertiserAgencyMappingRepository : IAdvAgencyMappingRepository
    {
        private readonly IDBEntities DB;
        public AdvertiserAgencyMappingRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }
        public ResponseCommon GetAdvertiserAgencyMappingList()
        {
            //ResponseCommon response = new ResponseCommon();
            ////response.Data = DB.AdvertiserAgencyMappings.ToList();
            //response.StatusCode = ApiStatus.Ok;
            //return response;

            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<GetAdvertiserAgencyMappingModel> lstRep = new List<GetAdvertiserAgencyMappingModel>();
                lstRep = DB.AdvertiserAgencyMappings
                                .Select(x => new GetAdvertiserAgencyMappingModel
                                {
                                    AdvertiserAgencyMapId = x.AdvertiserAgencyMapId,
                                    AdvertiserId = x.AdvertiserId,
                                    AdvertiserName = x.AdvertiserName,
                                    AgencyId = x.AgencyId,
                                    AgencyName = x.AgencyName,
                                    Relation = x.Relation,
                                    CreatedOn = x.CreatedOn,
                                    CreatedByUserKey = x.CreatedByUserKey,
                                    UpdatedOn = x.UpdatedOn,
                                    UpdatedByUserKey = x.UpdatedByUserKey
                                })
                                .OrderBy(x => x.AdvertiserAgencyMapId)
                                .ToList();
                if (lstRep.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }
                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstRep;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetAdvertiserAgencyMappingListByAdvertiserId(string advertiserId)
        {
            ResponseCommon response = new ResponseCommon();
            var advertiserAgencyMappingListIbDb = DB.AdvertiserAgencyMappings.Where(x => x.AdvertiserId == advertiserId).ToList();
            if (advertiserAgencyMappingListIbDb.Count > 0)
            {
                List<AdvertiserAgencyMappingGetModel> advertiserAgencyMappingGetModelList = new List<AdvertiserAgencyMappingGetModel>();
                foreach (AdvertiserAgencyMap objData in advertiserAgencyMappingListIbDb)
                {
                    advertiserAgencyMappingGetModelList.Add(new AdvertiserAgencyMappingGetModel
                    {
                        AdvertiserID = objData.AdvertiserId,
                        AgencyID = objData.AgencyId,
                        Relation = objData.Relation,
                        OrganizationName = objData.AgencyName,
                        Id = objData.AgencyId
                    });
                }
                response.Data = advertiserAgencyMappingGetModelList;
            }
            else
            {
                response.Data = null;
            }
            response.StatusCode = ApiStatus.Ok;
            return response;
        }

        public ResponseCommon GetAdvertiserAgencyMapping(string AdvertiserId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                var data = DB.AdvertiserAgencyMappings.FirstOrDefault(h => h.AdvertiserId == AdvertiserId);

                if (data != null)
                {
                    AdvertiserAgencyMappingGetModel advertiserAgencyMappingGetModel = new AdvertiserAgencyMappingGetModel
                    {
                        AdvertiserID = data.AdvertiserId,
                        AgencyID = data.AgencyId,
                        Relation = data.Relation
                    };
                    response.Data = advertiserAgencyMappingGetModel;
                    response.StatusCode = ApiStatus.Ok;
                    return response;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }

        public ResponseCommon PostMapping(AdvertiserAgencyMappingModel mappingModel)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                AdvertiserAgencyMap advertiserAgencyMapping = new AdvertiserAgencyMap();

                if (mappingModel.IsUpdate == false)
                {
                    advertiserAgencyMapping.AdvertiserId = mappingModel.AdvertiserID;
                    advertiserAgencyMapping.AgencyId = mappingModel.AgencyID;
                    advertiserAgencyMapping.Relation = mappingModel.Relation;
                    advertiserAgencyMapping.AdvertiserName = mappingModel.AdvertiserName;
                    advertiserAgencyMapping.AgencyName = mappingModel.AgencyName;

                    //*
                    advertiserAgencyMapping.CreatedOn = DateTime.Now;
                    advertiserAgencyMapping.UpdatedOn = DateTime.Now;
                    advertiserAgencyMapping.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                    advertiserAgencyMapping.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.AdvertiserAgencyMappings.Add(advertiserAgencyMapping);
                    DB.SaveChanges();
                    advertiserAgencyMapping.AdvertiserAgencyMapId = advertiserAgencyMapping.AdvertiserAgencyMapId;
                    response.Data = advertiserAgencyMapping.AdvertiserAgencyMapId;
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    advertiserAgencyMapping = DB.AdvertiserAgencyMappings.FirstOrDefault(x => x.AdvertiserId == mappingModel.AdvertiserID);

                    if (advertiserAgencyMapping != null)
                    {
                        advertiserAgencyMapping.AgencyId = mappingModel.AgencyID;
                        advertiserAgencyMapping.AgencyName = mappingModel.AgencyName;
                        advertiserAgencyMapping.Relation = mappingModel.Relation;

                        //*
                        advertiserAgencyMapping.UpdatedOn = DateTime.Now;
                        advertiserAgencyMapping.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                        DB.SaveChanges();
                        response.Data = advertiserAgencyMapping.AdvertiserAgencyMapId;
                        response.StatusCode = ApiStatus.Ok;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }

        public ResponseCommon DeleteAdvertiserAgencyMappingById(int id)
        {
            ResponseCommon response = new ResponseCommon();
            AdvertiserAgencyMap advertiserAgencyMapping = DB.AdvertiserAgencyMappings.Find(id);
            if (advertiserAgencyMapping == null)
            {
                response.StatusCode = ApiStatus.NoRecords;
                response.Message = ApiStatus.NotFoundMessage;
            }
            else
            {
                DB.AdvertiserAgencyMappings.Remove(advertiserAgencyMapping);
                DB.SaveChanges();
                response.StatusCode = ApiStatus.Ok;
                response.Message = ApiStatus.OkMessge;
            }
            return response;
        }
    }
}