﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Asi.Repository
{
    public class ProductionStatusRepository : IProductionStatusRepository
    {
        private readonly IDBEntities DB;
        public ProductionStatusRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        #region Public Methods
        public ResponseCommon GetProductionStatuses()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<ProductionStatusModel> lstProductionStatus = new List<ProductionStatusModel>();
                lstProductionStatus = DB.ProductionStatuses/*.Where(x => x.DeletedInd != true)*/
                    .Select(x => new ProductionStatusModel
                    {
                        ProductionStatusId = x.ProductionStatusId,
                        Name = x.ProductionStatusName
                    })
                    .OrderBy(x => x.Name)
                    .ToList();

                if (lstProductionStatus.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }

                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstProductionStatus;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetProductionStatusById(int productionStatusId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                ProductionStatusModel objProductionStatus = null;
                objProductionStatus = DB.ProductionStatuses.Where(x => /*x.DeletedInd != true &&*/ x.ProductionStatusId == productionStatusId)
                                        .Select(x => new ProductionStatusModel
                                        {
                                            ProductionStatusId = x.ProductionStatusId,
                                            Name = x.ProductionStatusName
                                        }).FirstOrDefault();
                if (objProductionStatus == null)
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                    return objResponse;
                }
                objResponse.Data = objProductionStatus;
                objResponse.StatusCode = ApiStatus.Ok;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon PostProductionStatus(ProductionStatusModel productionStatusModel)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                int isRecordExist = DB.ProductionStatuses.Where(x => x.ProductionStatusName.ToLower() == productionStatusModel.Name.ToLower()
                                                        /*&& x.DeletedInd == false*/).Count();
                if (isRecordExist <= 0)
                {
                    ProductionStatu objProductionStatusAdd = new ProductionStatu();
                    objProductionStatusAdd.ProductionStatusName = productionStatusModel.Name;
                    objProductionStatusAdd.CreatedOn = DateTime.Now;
                    //objProductionStatusAdd.DeletedInd = false;

                    //*
                    objProductionStatusAdd.UpdatedOn = DateTime.Now;
                    objProductionStatusAdd.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                    objProductionStatusAdd.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.ProductionStatuses.Add(objProductionStatusAdd);
                    DB.SaveChanges();

                    objResponse.Data = objProductionStatusAdd.ProductionStatusId;
                    objResponse.StatusCode = ApiStatus.Ok;

                    return objResponse;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.AlreadyExist;
                    objResponse.Message = ApiStatus.AlreadyExistCustomMessge("Production Status Name");
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon PutProductionStatus(ProductionStatusModel productionStatusModel)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                ProductionStatu objProductionStatusEdit = null;
                int isNameExist = DB.ProductionStatuses
                                    .Where(x => x.ProductionStatusName.ToLower() == productionStatusModel.Name.ToLower()
                                            && x.ProductionStatusId != productionStatusModel.ProductionStatusId
                                            /*&& x.DeletedInd == false*/).Count();
                if (isNameExist > 0)
                {
                    objResponse.StatusCode = ApiStatus.AlreadyExist;
                    objResponse.Message = ApiStatus.AlreadyExistCustomMessge("Production Status Name");
                    return objResponse;
                }

                objProductionStatusEdit = DB.ProductionStatuses.Where(x => /*x.DeletedInd != true &&*/ x.ProductionStatusId == productionStatusModel.ProductionStatusId).FirstOrDefault();
                if (objProductionStatusEdit != null)
                {
                    objProductionStatusEdit.ProductionStatusName = productionStatusModel.Name;
                    objProductionStatusEdit.UpdatedOn = DateTime.Now;

                    //*
                    objProductionStatusEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objProductionStatusEdit).State = EntityState.Modified;
                    DB.SaveChanges();

                    objResponse.Data = objProductionStatusEdit.ProductionStatusId;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon DeleteProductionStatus(int productionStatusId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                ProductionStatu objProductionStatusEdit = null;
                int status = 0;
                objProductionStatusEdit = DB.ProductionStatuses.Where(x => /*x.DeletedInd != true &&*/ x.ProductionStatusId == productionStatusId).FirstOrDefault();
                if (objProductionStatusEdit != null)
                {
                    //objProductionStatusEdit.DeletedInd = true;

                    //*
                    objProductionStatusEdit.UpdatedOn = DateTime.Now;
                    objProductionStatusEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objProductionStatusEdit).State = EntityState.Modified;
                    status = DB.SaveChanges();
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }
        #endregion
    }
}
