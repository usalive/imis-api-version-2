﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using Asi.Repository;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.Repository
{
    public class MediaOrderSignedDocumentsRepository : IMediaOrderSignedDocumentsRepository
    {
        private readonly IDBEntities DB;

        public MediaOrderSignedDocumentsRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        #region Public Methods

        public ResponseCommon getMediaOrderSignedDocumentsByBuyId(int buyId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                var mediaOrdersSignedDocuments = DB.MediaOrderSignedDocuments.Where(x => x.BuyId == buyId).ToList();
                if (mediaOrdersSignedDocuments != null)
                {
                    response.Data = mediaOrdersSignedDocuments;
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    response.Message = ApiStatus.NotFoundMessage;
                    response.StatusCode = ApiStatus.NotFound;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }

        public ResponseCommon saveMediaOrderSignedDocumentsByBuyId(MediaOrderSignedDocumentsModel model)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                string originamFileName = "";
                if (!string.IsNullOrEmpty(model.FilePath))
                    originamFileName = CommonUtility.SaveBase64ToFile4MediaOrderSignedDocuments(model.FilePath, model.FilePathExtension, model.FilePathName);

                MediaOrderSignedDocument objMediaOrderSignedDocuments = new MediaOrderSignedDocument();
                objMediaOrderSignedDocuments.BuyId = model.BuyId;
                objMediaOrderSignedDocuments.FilePath = originamFileName;
                objMediaOrderSignedDocuments.CreatedOn = DateTime.Now;
                //objMediaOrderSignedDocuments.CreatedByUserKey = Guid.Parse(model.CreatedBy);
                objMediaOrderSignedDocuments.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                DB.MediaOrderSignedDocuments.Add(objMediaOrderSignedDocuments);
                DB.SaveChanges();

                objResponse.Data = model.BuyId;
                objResponse.StatusCode = ApiStatus.Ok;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon deleteMediaOrderSignedDocumentById(long signedDocumentId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            Asi.DAL.MediaOrderSignedDocument objMediaOrderSignedDocuments = DB.MediaOrderSignedDocuments.Find(signedDocumentId);
            if (objMediaOrderSignedDocuments == null)
            {
                objResponse.StatusCode = ApiStatus.NoRecords;
                objResponse.Message = ApiStatus.NotFoundMessage;
            }
            else
            {
                DB.MediaOrderSignedDocuments.Remove(objMediaOrderSignedDocuments);
                DB.SaveChanges();
                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Message = ApiStatus.OkMessge;
            }
            return objResponse;
        }

        #endregion

    }
}
