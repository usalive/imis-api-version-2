﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;


namespace Asi.Repository
{
    public class PickUpFilesMediaOrderRepository : IPickUpFilesMediaOrderRepository
    {
        private readonly IDBEntities DB;
        public PickUpFilesMediaOrderRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        #region Public Methods
        public ResponseCommon GetPickUpFilesOrdersByFilterOptions(GetInCompleteOrdersbyFilterOption getInCompleteOrdersbyFilterOption)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                if (getInCompleteOrdersbyFilterOption != null)
                {
                    string filterQuery = getInCompleteOrdersbyFilterOption.MediaAssetId > 0 ? "MediaAssetId == " + getInCompleteOrdersbyFilterOption.MediaAssetId : "1=1";
                    filterQuery += getInCompleteOrdersbyFilterOption.IssueDateId > 0 ? " && IssueDateId == " + getInCompleteOrdersbyFilterOption.IssueDateId : " && 1=1";
                    filterQuery += getInCompleteOrdersbyFilterOption.AdTypeId > 0 ? " && AdTypeId == " + getInCompleteOrdersbyFilterOption.AdTypeId : " && 1=1";
                    filterQuery += getInCompleteOrdersbyFilterOption.RepId > 0 ? " && RepId == " + getInCompleteOrdersbyFilterOption.RepId : " && 1=1";

                    IQueryable<InCompleteInsertionOrderModel> inCompleteInsertionOrderModels;
                    inCompleteInsertionOrderModels = (from ord in DB.MediaOrders
                                           where /*ord.DeletedInd != true &&*/ ord.IsFrozen == false && (ord.MediaOrderProductionDetails.Select(y => y.NewPickupInd).FirstOrDefault() == false)
                                           orderby ord.MediaOrderId descending
                                           select new InCompleteInsertionOrderModel
                                           {
                                               BuyId = ord.BuyId,
                                               MediaOrderId = ord.MediaOrderId,
                                               MediaAssetId = ord.MediaAssetId,
                                               IssueDateId = ord.IssueDateId,
                                               AdTypeId = ord.RateCardDetail.AdType.AdTypeId,
                                               AdTypeName = ord.RateCardDetail.AdType.AdTypeName,
                                               AdcolorId = ord.RateCardDetail.AdColor.AdColorId,
                                               AdcolorName = ord.RateCardDetail.AdColor.AdColorName,
                                               AdSizeId = ord.RateCardDetail.AdSize.AdSizeId,
                                               AdSizeName = ord.RateCardDetail.AdSize.AdSizeName,
                                               FrequencyId = ord.RateCardDetail.Frequency.FrequencyId,
                                               FrequencyName = ord.RateCardDetail.Frequency.FrequencyName,
                                               AdvertiserId = ord.AdvertiserId,
                                               BT_ID = ord.BtId,
                                               ST_ID = ord.StId,
                                               GrossCost = ord.GrossCost,
                                               NetCost = ord.NetCost,
                                               FlightStartDate = ord.FlightEndDate,
                                               FlightEndDate = ord.FlightEndDate,
                                               RepId = ord.MediaOrderReps.Where(x => x.MediaOrderId == ord.MediaOrderId).Select(y => y.RepId).FirstOrDefault(),
                                               RepName = DB.Reps.Where(x => x.RepId == (ord.MediaOrderReps.Where(y => y.MediaOrderId == ord.MediaOrderId).Select(z => z.RepId))
                                                         .FirstOrDefault()).Select(t => t.RepName).FirstOrDefault(),
                                               Completed = ord.MediaOrderProductionDetails.Where(x => x.MediaOrderId == ord.MediaOrderId).Select(y => y.Completed).FirstOrDefault(),
                                               NewPickupInd = ord.MediaOrderProductionDetails.Where(x => x.MediaOrderId == ord.MediaOrderId).Select(y => y.NewPickupInd).FirstOrDefault(),
                                               CreateExpectedInd = ord.MediaOrderProductionDetails.Where(x => x.MediaOrderId == ord.MediaOrderId).Select(y => y.CreateExpectedInd).FirstOrDefault(),
                                               ChangesInd = ord.MediaOrderProductionDetails.Where(x => x.MediaOrderId == ord.MediaOrderId).Select(y => y.ChangesInd).FirstOrDefault(),
                                               PositionId = ord.MediaOrderProductionDetails.Where(x => x.MediaOrderId == ord.MediaOrderId).Select(y => y.PositionId).FirstOrDefault(),
                                               PositionName = DB.Positions.Where(x => x.PositionId == (ord.MediaOrderProductionDetails.Where(y => y.MediaOrderId == ord.MediaOrderId)
                                                       .Select(y => y.PositionId).FirstOrDefault())).Select(t => t.PositionName).FirstOrDefault(),
                                               PageFraction = ord.RateCardDetail.AdSize.PageFraction,
                                               OnHandInd = ord.MediaOrderProductionDetails.Where(x => x.MediaOrderId == ord.MediaOrderId).Select(y => y.OnHandInd).FirstOrDefault(),
                                               TrackingNumber = ord.MediaOrderProductionDetails.Where(x => x.MediaOrderId == ord.MediaOrderId).Select(y => y.TrackingNumber).FirstOrDefault(),
                                               MaterialExpectedDate = ord.MediaOrderProductionDetails.Where(x => x.MediaOrderId == ord.MediaOrderId).Select(y => y.MaterialExpectedDate).FirstOrDefault(),
                                               PickupMediaOrderId = ord.MediaOrderProductionDetails.Where(x => x.MediaOrderId == ord.MediaOrderId).Select(y => y.PickupMediaOrderId).FirstOrDefault(),

                                               PickupIndFromMediaAsset = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/ x.MediaOrderId == ord.MediaOrderProductionDetails.Where(q => q.MediaOrderId == ord.MediaOrderId).Select(u => u.PickupMediaOrderId).FirstOrDefault())
                                                .Select(z => z.MediaAsset.MediaAssetName).FirstOrDefault(),
                                               PickupIndFromIssueDate = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/ x.MediaOrderId == ord.MediaOrderProductionDetails.Where(q => q.MediaOrderId == ord.MediaOrderId).Select(u => u.PickupMediaOrderId).FirstOrDefault())
                                               .Select(z => z.AdIssueDate.CoverDate).FirstOrDefault(),
                                               PickupIndFromPg = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/ x.MediaOrderId == ord.MediaOrderProductionDetails.Where(q => q.MediaOrderId == ord.MediaOrderId).Select(u => u.PickupMediaOrderId).FirstOrDefault())
                                                  .Select(z => z.MediaOrderProductionDetails.Where(y => y.MediaOrderId == z.MediaOrderId).Select(t => t.PageNumber).FirstOrDefault()).FirstOrDefault(),
                                           }).Where(filterQuery);

                    List<InCompleteInsertionOrderModel> lstIcMediaOrderModel = null;
                    lstIcMediaOrderModel = inCompleteInsertionOrderModels.ToList();
                    if (lstIcMediaOrderModel.Count > 0)
                    {
                        response.StatusCode = ApiStatus.Ok;
                        response.Data = lstIcMediaOrderModel;
                    }
                    else
                    {
                        response.StatusCode = ApiStatus.NoRecords;
                        response.Message = ApiStatus.NoRecordsMessge;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        #endregion
    }
}
