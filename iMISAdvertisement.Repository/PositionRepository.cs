﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Asi.Repository
{
    public class PositionRepository : IPositionRepository
    {
        private readonly IDBEntities DB;
        public PositionRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        #region Public Methods
        public ResponseCommon GetPositions()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<PositionModel> lstPosition = new List<PositionModel>();
                lstPosition = DB.Positions/*.Where(x => x.DeletedInd != true)*/
                    .Select(x => new PositionModel
                    {
                        PositionId = x.PositionId,
                        Name = x.PositionName
                    })
                    .OrderBy(x => x.Name)
                    .ToList();

                if (lstPosition.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }

                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstPosition;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetPositionById(int positionId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                PositionModel objPosition = null;
                objPosition = DB.Positions.Where(x => /*x.DeletedInd != true &&*/ x.PositionId == positionId)
                                        .Select(x => new PositionModel
                                        {
                                            PositionId = x.PositionId,
                                            Name = x.PositionName
                                        }).FirstOrDefault();
                if (objPosition == null)
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                    return objResponse;
                }
                objResponse.Data = objPosition;
                objResponse.StatusCode = ApiStatus.Ok;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon PostPosition(PositionModel model)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                int isRecordExist = DB.Positions.Where(x => x.PositionName.ToLower() == model.Name.ToLower()
                                                        /*&& x.DeletedInd == false*/).Count();
                if (isRecordExist <= 0)
                {
                    Position objPositionAdd = new Position();
                    objPositionAdd.PositionName = model.Name;
                    objPositionAdd.CreatedOn = DateTime.Now;
                    //objPositionAdd.DeletedInd = false;

                    // this is just for testing 2020-11-02
                    objPositionAdd.UpdatedOn = DateTime.Now;
                    objPositionAdd.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                    objPositionAdd.UpdatedByUserKey = Constants.Created_Update_ByUserKey;
                    
                    DB.Positions.Add(objPositionAdd);
                    DB.SaveChanges();

                    objResponse.Data = objPositionAdd.PositionId;
                    objResponse.StatusCode = ApiStatus.Ok;

                    return objResponse;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.AlreadyExist;
                    objResponse.Message = ApiStatus.AlreadyExistCustomMessge("Position Name");

                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon PutPosition(PositionModel model)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                Position objPositionEdit = null;
                int isNameExist = DB.Positions
                                    .Where(x => x.PositionName.ToLower() == model.Name.ToLower()
                                            && x.PositionId != model.PositionId
                                            /*&& x.DeletedInd == false*/).Count();
                if (isNameExist > 0)
                {
                    objResponse.StatusCode = ApiStatus.AlreadyExist;
                    objResponse.Message = ApiStatus.AlreadyExistCustomMessge("Position Name");
                    return objResponse;
                }

                objPositionEdit = DB.Positions.Where(x => /*x.DeletedInd != true &&*/ x.PositionId == model.PositionId).FirstOrDefault();
                if (objPositionEdit != null)
                {
                    objPositionEdit.PositionName = model.Name;
                    objPositionEdit.UpdatedOn = DateTime.Now;

                    //*
                    objPositionEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objPositionEdit).State = EntityState.Modified;
                    DB.SaveChanges();

                    objResponse.Data = objPositionEdit.PositionId;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon DeletePosition(int positionId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                Position objPositionEdit = null;
                int status = 0;
                objPositionEdit = DB.Positions.Where(x => /*x.DeletedInd != true &&*/ x.PositionId == positionId).FirstOrDefault();
                if (objPositionEdit != null)
                {
                    //objPositionEdit.DeletedInd = true;

                    //*
                    objPositionEdit.UpdatedOn = DateTime.Now;
                    objPositionEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objPositionEdit).State = EntityState.Modified;
                    status = DB.SaveChanges();
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }
        #endregion
    }
}
