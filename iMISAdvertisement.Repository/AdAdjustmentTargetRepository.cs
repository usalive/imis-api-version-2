﻿using Asi.DAL;
using Asi.DAL.Model;
//using Asi.DAL.Model.StaticList;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Asi.Repository
{
    public class AdAdjustmentTargetRepository : IAdvertiserAgencyMappingRepository
    {
        private readonly IDBEntities DB;
        public AdAdjustmentTargetRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        #region Public Methods
        public ResponseCommon GetAdAdjustmentTargets()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<GetAdAdjustmentTargetModel> lstAdAdjustmentTarget = new List<GetAdAdjustmentTargetModel>();
                //lstAdAdjustmentTarget = DB.AdAdjustmentTargets.
                //                        //Where(x => x.DeletedInd != true).
                //                        Select(x => new GetAdAdjustmentTargetModel
                //                        {
                //                            AdAdjustmentTargetId = x.AdAdjustmentTargetId,
                //                            Name = x.AdAdjustmentTargetName
                //                        }).ToList();

                //* 2020-11-10 Change : Get HardCoded List
                //lstAdAdjustmentTarget = Get_AdAdjustmentTarget_Values.GetAdAdjustmentTargetValues()
                //                                                     .Select(x => new GetAdAdjustmentTargetModel
                //                                                     {
                //                                                         AdAdjustmentTargetId = x.AdAdjustmentTargetId,
                //                                                         Name = x.Name
                //                                                     }).ToList();

                //* 2020-11-25 Change : Get HardCoded List From Enum
                lstAdAdjustmentTarget = Enum.GetValues(typeof(AdAdjustmentTarget)).Cast<AdAdjustmentTarget>()
                    .Select(x => new GetAdAdjustmentTargetModel
                    {
                        AdAdjustmentTargetId = (int)x,
                        Name = x.ToString().Replace("_", " ")
                    }).ToList();

                if (lstAdAdjustmentTarget.Count <= 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.Ok;
                    objResponse.Data = lstAdAdjustmentTarget;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
            }
            return objResponse;
        }
        public ResponseCommon GetAdAdjustmentTargetById(int adAdjustmentTargetId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                //GetAdAdjustmentTargetModel objAdAdjustmentTarget = DB.AdAdjustmentTargets
                //                        .Where(x =>
                //                        //x.DeletedInd != true && 
                //                        x.AdAdjustmentTargetId == adAdjustmentTargetId)
                //                        .Select(x => new GetAdAdjustmentTargetModel
                //                        {
                //                            AdAdjustmentTargetId = x.AdAdjustmentTargetId,
                //                            Name = x.AdAdjustmentTargetName
                //                        }).FirstOrDefault();

                //* 2020-11-10 Change: Get Values From Hardcoded List
                //GetAdAdjustmentTargetModel objAdAdjustmentTarget = Get_AdAdjustmentTarget_Values
                //                                                  .GetAdAdjustmentTargetValues()
                //                                                  .Where(x => x.AdAdjustmentTargetId == adAdjustmentTargetId)
                //                                                  .Select(x => new GetAdAdjustmentTargetModel
                //                                                  {
                //                                                      AdAdjustmentTargetId = x.AdAdjustmentTargetId,
                //                                                      Name = x.Name
                //                                                  }).FirstOrDefault();

                //* 2020-11-25 Change : Get Values From Enum
                GetAdAdjustmentTargetModel objAdAdjustmentTarget = Enum.GetValues(typeof(AdAdjustmentTarget))
                                                                        .Cast<AdAdjustmentTarget>()
                                                                        .Where(x => (int)x == adAdjustmentTargetId)
                                                                  .Select(x => new GetAdAdjustmentTargetModel
                                                                  {
                                                                      AdAdjustmentTargetId = (int)x,
                                                                      Name = x.ToString().Replace("_", " ")
                                                                  }).FirstOrDefault();
                response.Data = objAdAdjustmentTarget;
                response.StatusCode = ApiStatus.Ok;
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }

        //* 2020-11-10 Change :Comment the below methods. because now list is getting from hardcoded list
        //public ResponseCommon PostAdAdjustmentTarget(GetAdAdjustmentTargetModel model)
        //{
        //    ResponseCommon response = new ResponseCommon();
        //    try
        //    {
        //        int isRecordExist = DB.AdAdjustmentTargets
        //                                    .Where(x => x.AdAdjustmentTargetName.ToLower() == model.Name.ToLower()
        //                                            /*&& x.DeletedInd == false*/)
        //                                    .Count();
        //        if (isRecordExist <= 0)
        //        {
        //            AdAdjustmentTarget objAdAdjustmentTargetAdd = new AdAdjustmentTarget();
        //            objAdAdjustmentTargetAdd.AdAdjustmentTargetName = model.Name.Trim();
        //            objAdAdjustmentTargetAdd.CreatedOn = DateTime.Now;
        //            //objAdAdjustmentTargetAdd.DeletedInd = false;

        //            //*
        //            objAdAdjustmentTargetAdd.UpdatedOn = DateTime.Now;
        //            objAdAdjustmentTargetAdd.CreatedByUserKey = Constants.Created_Update_ByUserKey;
        //            objAdAdjustmentTargetAdd.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

        //            DB.AdAdjustmentTargets.Add(objAdAdjustmentTargetAdd);
        //            DB.SaveChanges();

        //            response.Data = objAdAdjustmentTargetAdd.AdAdjustmentTargetId;
        //            response.StatusCode = ApiStatus.Ok;
        //        }
        //        else
        //        {
        //            response.StatusCode = ApiStatus.AlreadyExist;
        //            response.Message = ApiStatus.AlreadyExistCustomMessge("Ad Adjustment Target Name");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerRepsitory.WriteErrorLog(ex);
        //        response.StatusCode = ApiStatus.Exception;
        //        response.Message = ApiStatus.ExceptionMessge;
        //    }
        //    return response;
        //}
        //public ResponseCommon PutAdAdjustmentTarget(GetAdAdjustmentTargetModel adjustmentTargetModel)
        //{
        //    ResponseCommon response = new ResponseCommon();
        //    try
        //    {
        //        int isNameExist = DB.AdAdjustmentTargets
        //                            .Where(x => x.AdAdjustmentTargetName.ToLower() == adjustmentTargetModel.Name.ToLower()
        //                                    && x.AdAdjustmentTargetId != adjustmentTargetModel.AdAdjustmentTargetId
        //                                    /*&& x.DeletedInd == false*/).Count();
        //        if (isNameExist > 0)
        //        {
        //            response.StatusCode = ApiStatus.AlreadyExist;
        //            response.Message = ApiStatus.AlreadyExistCustomMessge("Ad Adjustment Target Name");
        //            return response;
        //        }

        //        AdAdjustmentTarget objAdtypeEdit = DB.AdAdjustmentTargets
        //                                            .Where(x => /*x.DeletedInd != true &&*/
        //                                                        x.AdAdjustmentTargetId == adjustmentTargetModel.AdAdjustmentTargetId)
        //                                            .FirstOrDefault();
        //        if (objAdtypeEdit != null)
        //        {
        //            objAdtypeEdit.AdAdjustmentTargetName = adjustmentTargetModel.Name;
        //            objAdtypeEdit.UpdatedOn = DateTime.Now;

        //            //*
        //            objAdtypeEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

        //            DB.Entry(objAdtypeEdit).State = EntityState.Modified;
        //            DB.SaveChanges();
        //            response.Data = adjustmentTargetModel.AdAdjustmentTargetId;
        //            response.StatusCode = ApiStatus.Ok;
        //        }
        //        else
        //        {
        //            response.StatusCode = ApiStatus.NotFound;
        //            response.Message = ApiStatus.NotFoundMessage;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerRepsitory.WriteErrorLog(ex);
        //        response.StatusCode = ApiStatus.Exception;
        //        response.Message = ApiStatus.ExceptionMessge;
        //    }
        //    return response;
        //}
        //public ResponseCommon DeleteAdAdjustmentTarget(int adAdjustmentTargetId)
        //{
        //    ResponseCommon response = new ResponseCommon();
        //    try
        //    {
        //        AdAdjustmentTarget objAdt = DB.AdAdjustmentTargets.Where(x => /*x.DeletedInd != true && */
        //        x.AdAdjustmentTargetId == adAdjustmentTargetId).FirstOrDefault();
        //        if (objAdt != null)
        //        {
        //            //objAdt.DeletedInd = true;

        //            //*
        //            objAdt.UpdatedOn = DateTime.Now;
        //            objAdt.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

        //            DB.Entry(objAdt).State = EntityState.Modified;
        //            int status = DB.SaveChanges();

        //            response.Data = status <= 0 ? 0 : 1;
        //            response.StatusCode = ApiStatus.Ok;
        //        }
        //        else
        //        {
        //            response.StatusCode = ApiStatus.NotFound;
        //            response.Message = ApiStatus.NotFoundMessage;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerRepsitory.WriteErrorLog(ex);
        //        response.StatusCode = ApiStatus.Exception;
        //        response.Message = ApiStatus.ExceptionMessge;
        //    }
        //    return response;
        //}
        #endregion
    }
}
