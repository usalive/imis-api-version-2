﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Asi.Repository
{
    public class AdTypeRepository : IAdTypeRepository
    {
        private readonly IDBEntities DB;

        public AdTypeRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        #region Public Methods
        public ResponseCommon GetAdTypes()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<GetAdTypeModel> lstAdType = new List<GetAdTypeModel>();
                lstAdType = DB.AdTypes
                    //.Where(x => x.DeletedInd != true)
                    .Select(x => new GetAdTypeModel
                    {
                        AdTypeId = x.AdTypeId,
                        Name = x.AdTypeName
                    })
                    .OrderBy(x => x.Name)
                    .ToList();

                if (lstAdType.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }

                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstAdType;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetAdTypeById(int adTypeId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                GetAdTypeModel objAdType = null;
                objAdType = DB.AdTypes.Where(x => /*x.DeletedInd != true &&*/ x.AdTypeId == adTypeId)
                                        .Select(x => new GetAdTypeModel
                                        {
                                            AdTypeId = x.AdTypeId,
                                            Name = x.AdTypeName
                                        }).FirstOrDefault();
                if (objAdType == null)
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                    return objResponse;
                }
                objResponse.Data = objAdType;
                objResponse.StatusCode = ApiStatus.Ok;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetAllAdTypeForPostOrder()
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                var lstAdTypes = (from ord in DB.MediaOrders
                                  where /* ord.DeletedInd != true &&*/ ord.IsFrozen == false
                                  orderby ord.MediaOrderId descending
                                  group ord by new { ord.RateCardId } into g
                                  select new
                                  {
                                      AdTypeId = g.Select(y => y.RateCard.AdType.AdTypeId).FirstOrDefault(),
                                      AdTypeName = g.Select(y => y.RateCard.AdType.AdTypeName).FirstOrDefault(),
                                  }).GroupBy(x => x.AdTypeId).Select(t => t.FirstOrDefault()).ToList();
                if (lstAdTypes.Count > 0)
                {
                    response.StatusCode = ApiStatus.Ok;
                    response.Data = lstAdTypes;
                }
                else
                {
                    response.StatusCode = ApiStatus.NoRecords;
                    response.Message = ApiStatus.NoRecordsMessge;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }

        public ResponseCommon PostAdType(PostAdTypeModel adTypeModel)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                int isRecordExist = DB.AdTypes.Where(x => x.AdTypeName.ToLower() == adTypeModel.Name.ToLower()
                                                        /*&& x.DeletedInd == false*/).Count();
                if (isRecordExist <= 0)
                {
                    AdType objAdtypeAdd = new AdType();
                    objAdtypeAdd.AdTypeName = adTypeModel.Name;
                    objAdtypeAdd.CreatedOn = DateTime.Now;
                    //objAdtypeAdd.DeletedInd = false;

                    //*
                    objAdtypeAdd.UpdatedOn = DateTime.Now;
                    objAdtypeAdd.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                    objAdtypeAdd.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.AdTypes.Add(objAdtypeAdd);
                    DB.SaveChanges();

                    objResponse.Data = objAdtypeAdd.AdTypeId;
                    objResponse.StatusCode = ApiStatus.Ok;

                    return objResponse;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.AlreadyExist;
                    objResponse.Message = ApiStatus.AlreadyExistCustomMessge("Ad Type Name");

                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon PutAdType(GetAdTypeModel adTypeModel)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                AdType objAdtypeEdit = null;
                int isNameExist = DB.AdTypes
                                    .Where(x => x.AdTypeName.ToLower() == adTypeModel.Name.ToLower()
                                            && x.AdTypeId != adTypeModel.AdTypeId
                                            /*&& x.DeletedInd == false*/).Count();
                if (isNameExist > 0)
                {
                    objResponse.StatusCode = ApiStatus.AlreadyExist;
                    objResponse.Message = ApiStatus.AlreadyExistCustomMessge("Ad Type Name");
                    return objResponse;
                }

                objAdtypeEdit = DB.AdTypes.Where(x => /*x.DeletedInd != true &&*/ x.AdTypeId == adTypeModel.AdTypeId).FirstOrDefault();
                if (objAdtypeEdit != null)
                {
                    objAdtypeEdit.AdTypeName = adTypeModel.Name;
                    objAdtypeEdit.UpdatedOn = DateTime.Now;

                    //*
                    objAdtypeEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objAdtypeEdit).State = EntityState.Modified;
                    DB.SaveChanges();

                    objResponse.Data = objAdtypeEdit.AdTypeId;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon DeleteAdType(int adTypeId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                AdType objAdtypeEdit = null;
                int status = 0;
                objAdtypeEdit = DB.AdTypes.Where(x => /*x.DeletedInd != true &&*/ x.AdTypeId == adTypeId).FirstOrDefault();
                if (objAdtypeEdit != null)
                {
                    //objAdtypeEdit.DeletedInd = true;

                    //*
                    objAdtypeEdit.UpdatedOn = DateTime.Now;
                    objAdtypeEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objAdtypeEdit).State = EntityState.Modified;
                    status = DB.SaveChanges();
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }
        #endregion
    }
}
