﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Asi.Repository
{
    public class MediaTypeRepository : IMediaTypeRepository
    {
        private readonly IDBEntities DB;
        public MediaTypeRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        #region Public Methods
        public ResponseCommon GetMediaTypes()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<GetMediaTypeModel> lstMediaType = new List<GetMediaTypeModel>();
                lstMediaType = DB.MediaTypes/*.Where(x => x.DeletedInd != true)*/
                                            .Select(x => new GetMediaTypeModel
                                            {
                                                MediaTypeId = x.MediaTypeId,
                                                Name = x.MediaTypeName
                                            })
                                            .OrderBy(x => x.Name)
                                            .ToList();
                if (lstMediaType.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }

                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstMediaType;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetMediaTypesById(int mediaTypeId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                GetMediaTypeModel objMediaType = null;
                objMediaType = DB.MediaTypes.Where(x => /*x.DeletedInd != true &&*/ x.MediaTypeId == mediaTypeId)
                                            .Select(x => new GetMediaTypeModel
                                            {
                                                MediaTypeId = x.MediaTypeId,
                                                Name = x.MediaTypeName
                                            }).FirstOrDefault();
                if (objMediaType == null)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                }
                else
                {
                    objResponse.Data = objMediaType;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon PostMediaType(PostMediaTypeModel objMediaType)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                int isMediaTypeExist = DB.MediaTypes.Where(x => /*x.DeletedInd == false && */
                                                        x.MediaTypeName.ToLower() == objMediaType.Name.ToLower())
                                                        .Count();
                if (isMediaTypeExist <= 0)
                {
                    MediaTypeRef objMediatypeMediad = new MediaTypeRef();
                    objMediatypeMediad.MediaTypeName = objMediaType.Name;                                        

                    DB.MediaTypes.Add(objMediatypeMediad);
                    DB.SaveChanges();

                    objResponse.Data = objMediatypeMediad.MediaTypeId;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.AlreadyExist;
                    objResponse.Message = ApiStatus.AlreadyExistCustomMessge("Media Type Name");
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Data = null;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon PutMediaType(GetMediaTypeModel objMediaType)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                int isNameExist = DB.MediaTypes
                                    .Where(x => x.MediaTypeName.ToLower() == objMediaType.Name.ToLower()
                                            && x.MediaTypeId != objMediaType.MediaTypeId
                                            /*&& x.DeletedInd == false*/).Count();
                if (isNameExist > 0)
                {
                    objResponse.StatusCode = ApiStatus.AlreadyExist;
                    objResponse.Message = ApiStatus.AlreadyExistCustomMessge("Media Type Name");
                    return objResponse;
                }

                MediaTypeRef objMediaTypeEdit = DB.MediaTypes.Where(x => /*x.DeletedInd != true &&*/ x.MediaTypeId == objMediaType.MediaTypeId).FirstOrDefault();
                if (objMediaTypeEdit != null)
                {
                    objMediaTypeEdit.MediaTypeName = objMediaType.Name;
                    
                    DB.Entry(objMediaTypeEdit).State = EntityState.Modified;
                    DB.SaveChanges();
                    objResponse.Data = objMediaTypeEdit.MediaTypeId;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.Data = objMediaType.MediaTypeId;
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon DeleteMediaType(int mediaTypeId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                MediaTypeRef objMediaTypeEdit = null;
                int status = 0;
                objMediaTypeEdit = DB.MediaTypes.Where(x => /*x.DeletedInd != true &&*/ x.MediaTypeId == mediaTypeId).FirstOrDefault();
                if (objMediaTypeEdit != null)
                {                                                            
                    DB.Entry(objMediaTypeEdit).State = EntityState.Modified;
                    status = DB.SaveChanges();
                    objResponse.Data = status <= 0 ? 0 : 1;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.Message = ApiStatus.NotFoundMessage;
                    objResponse.StatusCode = ApiStatus.NotFound;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }
        #endregion
    }
}
