﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Asi.Repository
{
    public class MediaAssetGroupRepository : IMediaAssetGroupRepository
    {
        private readonly IDBEntities DB;
        public MediaAssetGroupRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        #region Public Methods
        public ResponseCommon GetMediaAssetGroups()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<GetMediaAssetGroupModel> lstMediaAssetGroups = new List<GetMediaAssetGroupModel>();
                lstMediaAssetGroups = DB.MediaAssetGroups./*Where(x => x.DeletedInd != true).*/
                                        Select(x => new GetMediaAssetGroupModel
                                        {
                                            MediaAssetGroupId = x.MediaAssetGroupId,
                                            Name = x.MediaAssetGroupName,
                                            MediaAssetGroupParentGroupId = x.MediaAssetGroupParentGroupId
                                        }).ToList();

                objResponse.Data = lstMediaAssetGroups;
                if (lstMediaAssetGroups.Count <= 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.Ok;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
            }
            return objResponse;
        }
        public ResponseCommon GetMediaAssetGroupsById(int mediaAssetGroupId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                GetMediaAssetGroupModel objMediaAssetGroup = DB.MediaAssetGroups
                                                .Where(x => /*x.DeletedInd != true &&*/ x.MediaAssetGroupId == mediaAssetGroupId)
                                                .Select(x => new GetMediaAssetGroupModel
                                                {
                                                    MediaAssetGroupId = x.MediaAssetGroupId,
                                                    Name = x.MediaAssetGroupName,
                                                    MediaAssetGroupParentGroupId = x.MediaAssetGroupParentGroupId
                                                }).FirstOrDefault();
                if (objMediaAssetGroup != null)
                {
                    response.Data = objMediaAssetGroup;
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    response.StatusCode = ApiStatus.NotFound;
                    response.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon PostMediaAssetGroup(PostMediaAssetGroupModel objMediaAssetGroup)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                int recordCount = DB.MediaAssetGroups
                                    .Where(x => x.MediaAssetGroupName.ToLower() == objMediaAssetGroup.Name.ToLower()
                                            /*&& x.DeletedInd == false*/)
                                    .Count();
                if (recordCount <= 0)
                {
                    MediaAssetGroup objMediaAssetGroupAdd = new MediaAssetGroup();
                    objMediaAssetGroupAdd.MediaAssetGroupName = objMediaAssetGroup.Name;
                    objMediaAssetGroupAdd.MediaAssetGroupParentGroupId = objMediaAssetGroup.MediaAssetGroupParentGroupId;
                    objMediaAssetGroupAdd.CreatedOn = DateTime.Now;
                    //objMediaAssetGroupAdd.DeletedInd = false;

                    //*
                    objMediaAssetGroupAdd.UpdatedOn = DateTime.Now;
                    objMediaAssetGroupAdd.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                    objMediaAssetGroupAdd.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.MediaAssetGroups.Add(objMediaAssetGroupAdd);
                    DB.SaveChanges();

                    response.Data = objMediaAssetGroupAdd.MediaAssetGroupId;
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    response.StatusCode = ApiStatus.AlreadyExist;
                    response.Message = ApiStatus.AlreadyExistCustomMessge("Media Asset Group Name");
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon PutMediaAssetGroup(GetMediaAssetGroupModel objMediaAssetGroup)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                int isNameExist = DB.MediaAssetGroups
                                    .Where(x => x.MediaAssetGroupName.ToLower() == objMediaAssetGroup.Name.ToLower()
                                            && x.MediaAssetGroupId != objMediaAssetGroup.MediaAssetGroupId
                                            /*&& x.DeletedInd == false*/).Count();
                if (isNameExist > 0)
                {
                    response.StatusCode = ApiStatus.AlreadyExist;
                    response.Message = ApiStatus.AlreadyExistCustomMessge("Media Asset Group Name");
                    return response;
                }

                MediaAssetGroup objMediaAssetGroupEdit = DB.MediaAssetGroups
                    .Where(x => /*x.DeletedInd != true &&*/ x.MediaAssetGroupId == objMediaAssetGroup.MediaAssetGroupId)
                    .FirstOrDefault();
                if (objMediaAssetGroupEdit != null)
                {
                    objMediaAssetGroupEdit.MediaAssetGroupName = objMediaAssetGroup.Name;
                    objMediaAssetGroupEdit.MediaAssetGroupParentGroupId = objMediaAssetGroup.MediaAssetGroupParentGroupId;
                    objMediaAssetGroupEdit.UpdatedOn = DateTime.Now;

                    //*
                    objMediaAssetGroupEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objMediaAssetGroupEdit).State = EntityState.Modified;
                    DB.SaveChanges();
                    response.Data = objMediaAssetGroup.MediaAssetGroupId;
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    response.StatusCode = ApiStatus.NotFound;
                    response.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon DeleteMediaAssetGroup(int mediaAssetGroupId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                MediaAssetGroup objMediaAssetGroupEdit = null;
                int status = 0;
                objMediaAssetGroupEdit = DB.MediaAssetGroups.Where(x => /*x.DeletedInd != true && */
                                                                        x.MediaAssetGroupId == mediaAssetGroupId).FirstOrDefault();
                if (objMediaAssetGroupEdit != null)
                {
                    //objMediaAssetGroupEdit.DeletedInd = true;

                    //*
                    objMediaAssetGroupEdit.UpdatedOn = DateTime.Now;
                    objMediaAssetGroupEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objMediaAssetGroupEdit).State = EntityState.Modified;
                    status = DB.SaveChanges();

                    response.Data = status <= 0 ? 0 : 1;
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    response.StatusCode = ApiStatus.NotFound;
                    response.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        #endregion
    }
}
