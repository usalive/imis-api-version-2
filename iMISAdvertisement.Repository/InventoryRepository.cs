﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Asi.Repository
{
    public class InventoryRepository : IInventoryRepository
    {
        private readonly IDBEntities DB;
        public InventoryRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        #region Public Methods
        public ResponseCommon GetInventoryDetails(int mediaAssetId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                var lstInventoryDetails = (from i in DB.MediaInventoryMasters
                                           where /*i.DeletedInd != true &&*/ i.MediaAssetId == mediaAssetId
                                           select new
                                           {
                                               InventoryId = i.InventoryId,
                                               MediaAssetId = i.MediaAssetId,
                                               MediaAssetName = i.MediaAsset == null ? "" : i.MediaAsset.MediaAssetName,
                                               AdSizeId = i.AdSizeId,
                                               AdSizeName = i.AdSize.AdSizeName,
                                               IssueDateId = i.IssueDateId,
                                               IssueDate = i.AdIssueDate.CoverDate,
                                               TotalInventory = i.TotalInventory
                                           }).ToList()
                                          .GroupBy(x => new { x.MediaAssetId, x.MediaAssetName })
                                          .Select(x => new GetInventoryByMediaAssetModel
                                          {
                                              MediaAssetName = x.Key.MediaAssetName,
                                              MediaAssetId = x.Key.MediaAssetId,
                                              Inventory = x.Select(y => new GetInventoryDetailModel
                                              {
                                                  InventoryId = y.InventoryId,
                                                  AdSizeId = y.AdSizeId,
                                                  AdSizeName = y.AdSizeName,
                                                  IssueDateId = y.IssueDateId,
                                                  IssueDate = y.IssueDate,
                                                  TotalInventory = y.TotalInventory
                                              })
                                          }).ToList();

                response.Data = lstInventoryDetails;
                response.StatusCode = ApiStatus.Ok;
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        //public ResponseCommon GetRateCardDetailById(int RateCardDetailId)
        //{
        //    ResponseCommon response = new ResponseCommon();
        //    try
        //    {
        //        GetRateCardDetailByIdModel objRateCardDetail = db.RateCardDetails.Where(x => x.DeletedInd != true && x.RateCardDetailId == RateCardDetailId)
        //                                        .Select(x => new GetRateCardDetailByIdModel
        //                                        {
        //                                            RateCardDetailId = x.RateCardDetailId,

        //                                            RateCardId = x.RateCardId,
        //                                            RateCardName = x.RateCard.RateCardName,

        //                                            AdTypeId = x.AdTypeId,
        //                                            AdTypeName = x.AdType.AdTypeName,

        //                                            AdSizeId = x.AdSizeId,
        //                                            AdSizeName = x.AdSize.AdSizeName,

        //                                            AdColorId = x.AdColorId,
        //                                            AdColorName = x.AdColor.AdColorName,

        //                                            FrequencyId = x.FrequencyId,
        //                                            FrequencyName = x.Frequency.FrequencyName,

        //                                            RateCardCost = x.RateCardCost,

        //                                            MediaAssetName = x.RateCard.MediaAsset.MediaAssetName,
        //                                            MediaAssetId = x.RateCard.MediaAssetId
        //                                        }).FirstOrDefault();
        //        if (objRateCardDetail != null)
        //        {
        //            response.data = objRateCardDetail;
        //            response.statusCode = ApiStatus.Ok;
        //        }
        //        else
        //        {
        //            response.message = ApiStatus.NotFoundMessage;
        //            response.statusCode = ApiStatus.NotFound;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerRepsitory.WriteErrorLog(ex);
        //        response.statusCode = ApiStatus.Exception;
        //        response.message = ApiStatus.ExceptionMessge;
        //    }
        //    return response;
        //}
        //public ResponseCommon GetRateCardDetailsByColor(int RateCardId, int AdColorId)
        //{
        //    ResponseCommon response = new ResponseCommon();
        //    try
        //    {
        //        List<GetRateCardDetailModel> lstRateCardDetails = db.RateCards
        //                                         .Where(x => x.DeletedInd != true && x.RateCardId == RateCardId)
        //                                         .Select(x => new GetRateCardDetailModel
        //                                         {
        //                                             RateCardId = x.RateCardId,
        //                                             RateCardName = x.RateCardName,

        //                                             AdTypeId = x.AdTypeId,
        //                                             AdTypeName = x.AdType.AdTypeName,

        //                                             MediaAssetName = x.MediaAsset.MediaAssetName,
        //                                             MediaAssetId = x.MediaAssetId,

        //                                             RateCardDetailCost = x.RateCardDetails.Where(y => y.DeletedInd != true)
        //                                             .Select(y => new GetRateCardDetailCostModel
        //                                             {
        //                                                 AdColorId = y.AdColorId,
        //                                                 AdColorName = y.AdColor.AdColorName,

        //                                                 RateCardDetailId = y.RateCardDetailId,
        //                                                 AdSizeId = y.AdSizeId,
        //                                                 AdSizeName = y.AdSize.AdSizeName,

        //                                                 FrequencyId = y.FrequencyId,
        //                                                 FrequencyName = y.Frequency.FrequencyName,

        //                                                 RateCardCost = y.RateCardCost
        //                                             }).Where(y => y.AdColorId == AdColorId)
        //                                         }).ToList();
        //        response.data = lstRateCardDetails;
        //        response.statusCode = ApiStatus.Ok;
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerRepsitory.WriteErrorLog(ex);
        //        response.statusCode = ApiStatus.Exception;
        //        response.message = ApiStatus.ExceptionMessge;
        //    }
        //    return response;
        //}
        //public ResponseCommon GetRateCardsCost(int RateCardId, int AdColorId, int AdSizeId, int FrequencyId)
        //{
        //    ResponseCommon response = new ResponseCommon();
        //    try
        //    {
        //        var objRateCardCost = db.RateCardDetails
        //            .Where(x => x.DeletedInd != true && x.RateCard.DeletedInd != true && x.RateCardId == RateCardId && x.AdColorId == AdColorId && x.AdSizeId == AdSizeId && x.FrequencyId == FrequencyId)
        //            .Select(x => new { x.RateCardCost, x.RateCardDetailId }).FirstOrDefault();


        //        response.data = objRateCardCost;
        //        response.statusCode = ApiStatus.Ok;
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerRepsitory.WriteErrorLog(ex);
        //        response.statusCode = ApiStatus.Exception;
        //        response.message = ApiStatus.ExceptionMessge;
        //    }
        //    return response;
        //}
        //public ResponseCommon GetRateCardsCostForPerWord(int RateCardId, int AdColorId)
        //{
        //    ResponseCommon response = new ResponseCommon();
        //    try
        //    {
        //        var RateCardCost = db.RateCardDetails
        //            .Where(x => x.DeletedInd != true && x.RateCard.DeletedInd != true && x.RateCardId == RateCardId && x.AdColorId == AdColorId)
        //            .OrderBy(x => x.RateCardDetailId).Select(x => new { x.AdSize.AdSizeName, x.Frequency.FrequencyName, x.RateCardCost, x.RateCardDetailId }).Take(2).ToList();

        //        if (RateCardCost.Count < 2)
        //        {
        //            response.statusCode = ApiStatus.InvalidInput;
        //            response.message = ApiStatus.InvalidInputMessge;
        //        }
        //        else
        //        {
        //            var objCostForWord = new
        //            {
        //                RateCardDetailId = RateCardCost.FirstOrDefault().RateCardDetailId,
        //                NoOfWord = RateCardCost.FirstOrDefault().AdSizeName,
        //                RateCardCost = RateCardCost.FirstOrDefault().RateCardCost,
        //                AdditionalWord = RateCardCost.FirstOrDefault().FrequencyName,
        //                PerWordCost = RateCardCost.Skip(1).FirstOrDefault().RateCardCost
        //            };

        //            response.data = objCostForWord;
        //            response.statusCode = ApiStatus.Ok;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerRepsitory.WriteErrorLog(ex);
        //        response.statusCode = ApiStatus.Exception;
        //        response.message = ApiStatus.ExceptionMessge;
        //    }
        //    return response;
        //}
        //public ResponseCommon GetMultipleRateCardsCost(int RateCardId, int AdColorId, int AdSizeId, int FrequencyId)
        //{
        //    ResponseCommon response = new ResponseCommon();
        //    try
        //    {
        //        List<GetBasicRateCardCost> objRateCardCost = new List<GetBasicRateCardCost>();
        //        objRateCardCost = db.RateCardDetails
        //           .Where(x => x.DeletedInd != true && x.RateCard.DeletedInd != true && x.RateCardId == RateCardId && x.AdColorId == AdColorId && x.AdSizeId == AdSizeId && x.FrequencyId == FrequencyId)
        //           .Select(x => new GetBasicRateCardCost { RateCardCost = x.RateCardCost, RateCardDetailId = x.RateCardDetailId }).ToList();


        //        response.data = objRateCardCost;
        //        response.statusCode = ApiStatus.Ok;
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerRepsitory.WriteErrorLog(ex);
        //        response.statusCode = ApiStatus.Exception;
        //        response.message = ApiStatus.ExceptionMessge;
        //    }
        //    return response;
        //}
        //public ResponseCommon GetMultipleRateCardsCostForPerWord(int RateCardId, int AdColorId)
        //{
        //    ResponseCommon response = new ResponseCommon();
        //    try
        //    {
        //        List<GetMultipleRateCardsCostForPerWord> lstRateCardCost = new List<DAL.Model.GetMultipleRateCardsCostForPerWord>();
        //        var RateCardCost = db.RateCardDetails
        //            .Where(x => x.DeletedInd != true && x.RateCard.DeletedInd != true && x.RateCardId == RateCardId && x.AdColorId == AdColorId)
        //            .OrderBy(x => x.RateCardDetailId).Select(x => new { x.AdSize.AdSizeName, x.Frequency.FrequencyName, x.RateCardCost, x.RateCardDetailId }).ToList();

        //        if (RateCardCost.Count < 2)
        //        {
        //            response.statusCode = ApiStatus.InvalidInput;
        //            response.message = ApiStatus.InvalidInputMessge;
        //        }
        //        else
        //        {
        //            for (int i = 0; i <= RateCardCost.Count() - 1; i = i + 2)
        //            {
        //                var objCostForWord = new GetMultipleRateCardsCostForPerWord
        //                {
        //                    RateCardDetailId = RateCardCost[i].RateCardDetailId,
        //                    NoOfWord = RateCardCost[i].AdSizeName.ToString(),
        //                    RateCardCost = RateCardCost[i].RateCardCost.ToString(),
        //                    AdditionalWord = RateCardCost[i].FrequencyName.ToString(),
        //                    PerWordCost = RateCardCost[i + 1].RateCardCost.ToString()
        //                };
        //                lstRateCardCost.Add(objCostForWord);
        //            }

        //            response.data = lstRateCardCost;
        //            response.statusCode = ApiStatus.Ok;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LoggerRepsitory.WriteErrorLog(ex);
        //        response.statusCode = ApiStatus.Exception;
        //        response.message = ApiStatus.ExceptionMessge;
        //    }
        //    return response;
        //}

        public ResponseCommon PostInventory(PostInventoryModel postInventory)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                List<MediaInventoryMaster> lstInventoryData = DB.MediaInventoryMasters.Where(x => x.MediaAssetId == postInventory.MediaAssetId /*&& x.DeletedInd != true*/).ToList();
                List<MediaInventoryMaster> lstInventory = new List<MediaInventoryMaster>();
                MediaInventoryMaster inventoryMaster = null;

                //New rate card details added to db
                if (lstInventoryData.Count <= 0)
                {
                    foreach (var item in postInventory.Inventory)
                    {
                        inventoryMaster = new MediaInventoryMaster();
                        inventoryMaster.MediaAssetId = postInventory.MediaAssetId;
                        inventoryMaster.AdSizeId = item.AdSizeId;
                        inventoryMaster.IssueDateId = item.IssueDateId;
                        inventoryMaster.TotalInventory = Convert.ToString(item.TotalInventory);
                        inventoryMaster.CreatedOn = DateTime.Now;
                        //inventoryMaster.DeletedInd = false;

                        //*
                        inventoryMaster.UpdatedOn = DateTime.Now;
                        inventoryMaster.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                        inventoryMaster.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                        lstInventory.Add(inventoryMaster);
                    }
                    DB.MediaInventoryMasters.AddRange(lstInventory);
                    DB.SaveChanges();
                    //response.data = objRateCardDetail.RateCardDetailId;
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    //Remove Existing rate card which is not in request details added to db
                    var lstInventoryDetailDelete = lstInventoryData.Where(x => !(postInventory.Inventory.Any(y => y.AdSizeId == x.AdSizeId && y.IssueDateId == x.IssueDateId && y.InventoryId != 0)) /*&& x.DeletedInd == false*/).ToList();
                    foreach (var item in lstInventoryDetailDelete)
                    {
                        //item.DeletedInd = true;
                        item.UpdatedOn = DateTime.Now;

                        //*
                        item.UpdatedByUserKey = Constants.Created_Update_ByUserKey;
                    }
                    foreach (var item in postInventory.Inventory)
                    {
                        var objInventoryDetailEdit = lstInventoryData.Where(x => x.AdSizeId == item.AdSizeId && x.IssueDateId == item.IssueDateId /*&& x.DeletedInd != true*/).FirstOrDefault();
                        //New rate card details added to db
                        if (item.InventoryId == 0 && objInventoryDetailEdit == null)
                        {
                            inventoryMaster = new MediaInventoryMaster();
                            inventoryMaster.MediaAssetId = postInventory.MediaAssetId;
                            inventoryMaster.AdSizeId = item.AdSizeId;
                            inventoryMaster.IssueDateId = item.IssueDateId;
                            inventoryMaster.TotalInventory = Convert.ToString(item.TotalInventory);
                            inventoryMaster.CreatedOn = DateTime.Now;
                            //inventoryMaster.DeletedInd = false;

                            //*
                            inventoryMaster.UpdatedOn = DateTime.Now;
                            inventoryMaster.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                            inventoryMaster.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                            lstInventory.Add(inventoryMaster);
                        }
                        else
                        {
                            //New inventory added to db
                            if (objInventoryDetailEdit == null)
                            {
                                inventoryMaster = new MediaInventoryMaster();
                                inventoryMaster.MediaAssetId = postInventory.MediaAssetId;
                                inventoryMaster.AdSizeId = item.AdSizeId;
                                inventoryMaster.IssueDateId = item.IssueDateId;
                                inventoryMaster.TotalInventory = Convert.ToString(item.TotalInventory);
                                inventoryMaster.CreatedOn = DateTime.Now;
                                //inventoryMaster.DeletedInd = false;

                                //*
                                inventoryMaster.UpdatedOn = DateTime.Now;
                                inventoryMaster.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                                inventoryMaster.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                lstInventory.Add(inventoryMaster);
                            }
                            else
                            {
                                //Edit Existing inventory added to db
                                var blockedInventory = (from mid in DB.MediaInventoryDetails
                                                        where mid.InventoryId == item.InventoryId
                                                        group mid by mid.InventoryId into tmp
                                                        select new { total = tmp.Sum(s => s.BlockedInventory) }).FirstOrDefault();
                                //  var remainInventory = Convert.ToInt32(item.TotalInventory) - Convert.ToInt32(blockedInventory.total);

                                if (blockedInventory == null)
                                {
                                    objInventoryDetailEdit.MediaAssetId = postInventory.MediaAssetId;
                                    objInventoryDetailEdit.AdSizeId = item.AdSizeId;
                                    objInventoryDetailEdit.IssueDateId = item.IssueDateId;
                                    //objInventoryDetailEdit.DeletedInd = false;
                                    objInventoryDetailEdit.UpdatedOn = DateTime.Now;
                                    objInventoryDetailEdit.TotalInventory = Convert.ToString(item.TotalInventory);

                                    //*
                                    objInventoryDetailEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                    DB.Entry(objInventoryDetailEdit).State = EntityState.Modified;
                                }
                                else if (item.TotalInventory >= Convert.ToInt32(blockedInventory.total))
                                {
                                    objInventoryDetailEdit.MediaAssetId = postInventory.MediaAssetId;
                                    objInventoryDetailEdit.AdSizeId = item.AdSizeId;
                                    objInventoryDetailEdit.IssueDateId = item.IssueDateId;
                                    objInventoryDetailEdit.UpdatedOn = DateTime.Now;
                                    //objInventoryDetailEdit.DeletedInd = false;
                                    objInventoryDetailEdit.TotalInventory = Convert.ToString(item.TotalInventory);

                                    //*
                                    objInventoryDetailEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                    DB.Entry(objInventoryDetailEdit).State = EntityState.Modified;
                                }
                                else
                                {
                                    var coverDate = (from aid in DB.AdIssueDates
                                                     where aid.IssueDateId == item.IssueDateId
                                                     select aid.CoverDate).FirstOrDefault();
                                    var adSize = (from ads in DB.AdSizes
                                                  where ads.AdSizeId == item.AdSizeId
                                                  select ads.AdSizeName).FirstOrDefault();
                                    response.Message = "Total inventory of " + Convert.ToString(coverDate.Month + "/" + coverDate.Day + "/" + coverDate.Year) + " issue date and " + adSize + " ad size is less than blocked inventory";
                                }
                            }
                        }
                    }
                    if (lstInventory.Count > 0)
                        DB.MediaInventoryMasters.AddRange(lstInventory);
                    DB.SaveChanges();
                    response.StatusCode = ApiStatus.Ok;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        #endregion
    }
}
