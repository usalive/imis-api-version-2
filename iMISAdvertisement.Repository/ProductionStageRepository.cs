﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Asi.Repository
{
    public class ProductionStageRepository : IProductionStageRepository
    {
        private readonly IDBEntities DB;
        public ProductionStageRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        #region Public Methods
        public ResponseCommon GetProductionStages()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<ProductionStageModel> lstProductionStage = new List<ProductionStageModel>();
                lstProductionStage = DB.ProductionStages/*.Where(x => x.DeletedInd != true)*/
                    .Select(x => new ProductionStageModel
                    {
                        ProductionStageId = x.ProductionStageId,
                        Name = x.ProductionStageName
                    })
                    .OrderBy(x => x.Name)
                    .ToList();

                if (lstProductionStage.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }

                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstProductionStage;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetProductionStageById(int productionStageId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                ProductionStageModel objProductionStage = null;
                objProductionStage = DB.ProductionStages.Where(x => /*x.DeletedInd != true &&*/ x.ProductionStageId == productionStageId)
                                        .Select(x => new ProductionStageModel
                                        {
                                            ProductionStageId = x.ProductionStageId,
                                            Name = x.ProductionStageName
                                        }).FirstOrDefault();
                if (objProductionStage == null)
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                    return objResponse;
                }
                objResponse.Data = objProductionStage;
                objResponse.StatusCode = ApiStatus.Ok;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon PostProductionStage(ProductionStageModel productionStageModel)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                int isRecordExist = DB.ProductionStages.Where(x => x.ProductionStageName.ToLower() == productionStageModel.Name.ToLower()
                                                        /*&& x.DeletedInd == false*/).Count();
                if (isRecordExist <= 0)
                {
                    ProductionStage objProductionStageAdd = new ProductionStage();
                    objProductionStageAdd.ProductionStageName = productionStageModel.Name;
                    objProductionStageAdd.CreatedOn = DateTime.Now;
                    //objProductionStageAdd.DeletedInd = false;

                    //*
                    objProductionStageAdd.UpdatedOn = DateTime.Now;
                    objProductionStageAdd.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                    objProductionStageAdd.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.ProductionStages.Add(objProductionStageAdd);
                    DB.SaveChanges();

                    objResponse.Data = objProductionStageAdd.ProductionStageId;
                    objResponse.StatusCode = ApiStatus.Ok;

                    return objResponse;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.AlreadyExist;
                    objResponse.Message = ApiStatus.AlreadyExistCustomMessge("Production Stage Name");
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon PutProductionStage(ProductionStageModel productionStageModel)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                ProductionStage objProductionStageEdit = null;
                int isNameExist = DB.ProductionStages
                                    .Where(x => x.ProductionStageName.ToLower() == productionStageModel.Name.ToLower()
                                            && x.ProductionStageId != productionStageModel.ProductionStageId
                                            /*&& x.DeletedInd == false*/).Count();
                if (isNameExist > 0)
                {
                    objResponse.StatusCode = ApiStatus.AlreadyExist;
                    objResponse.Message = ApiStatus.AlreadyExistCustomMessge("Production Stage Name");
                    return objResponse;
                }

                objProductionStageEdit = DB.ProductionStages.Where(x =>/* x.DeletedInd != true &&*/ x.ProductionStageId == productionStageModel.ProductionStageId).FirstOrDefault();
                if (objProductionStageEdit != null)
                {
                    objProductionStageEdit.ProductionStageName = productionStageModel.Name;
                    objProductionStageEdit.UpdatedOn = DateTime.Now;

                    //*
                    objProductionStageEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objProductionStageEdit).State = EntityState.Modified;
                    DB.SaveChanges();

                    objResponse.Data = objProductionStageEdit.ProductionStageId;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon DeleteProductionStage(int productionStageId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                ProductionStage objProductionStageEdit = null;
                int status = 0;
                objProductionStageEdit = DB.ProductionStages.Where(x => /*x.DeletedInd != true &&*/ x.ProductionStageId == productionStageId).FirstOrDefault();
                if (objProductionStageEdit != null)
                {
                    //objProductionStageEdit.DeletedInd = true;

                    //*
                    objProductionStageEdit.UpdatedOn = DateTime.Now;
                    objProductionStageEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objProductionStageEdit).State = EntityState.Modified;
                    status = DB.SaveChanges();
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }
        #endregion
    }
}
