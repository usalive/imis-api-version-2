﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Asi.Repository
{
    public class AdSizeRepository : IAdSizeRepository
    {
        private readonly IDBEntities DB;
        public AdSizeRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        #region Public Methods
        public ResponseCommon GetAdSizes()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<GetAdSizeModel> lstAdSize = new List<GetAdSizeModel>();
                lstAdSize = DB.AdSizes.Select(x => new GetAdSizeModel
                {
                    AdSizeId = x.AdSizeId,
                    Name = x.AdSizeName,
                    MediaAssetId = x.MediaAssetId,
                    PageFraction = x.PageFraction
                }).ToList();
                if (lstAdSize.Count <= 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.Ok;
                    objResponse.Data = lstAdSize;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
            }
            return objResponse;
        }
        public ResponseCommon GetAdSizesById(int adSizeId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                GetAdSizeModel objAdSize = DB.AdSizes.Where(x => /*x.DeletedInd != true &&*/ x.AdSizeId == adSizeId)
                                                .Select(x => new GetAdSizeModel
                                                {
                                                    AdSizeId = x.AdSizeId,
                                                    Name = x.AdSizeName,
                                                    MediaAssetId = x.MediaAssetId,
                                                    PageFraction = x.PageFraction
                                                }).FirstOrDefault();
                response.Data = objAdSize;
                response.StatusCode = ApiStatus.Ok;
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon PostAdSize(PostAdSizeModel adSizeModel)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                int isRecordExist = DB.AdSizes.Where(x => x.MediaAssetId == adSizeModel.MediaAssetId
                                                    && x.AdSizeName.ToLower() == adSizeModel.Name.ToLower()
                                                    /*&& x.DeletedInd == false*/)
                                              .Count();
                if (isRecordExist <= 0)
                {
                    AdSize objAdSizeAdd = new AdSize();
                    objAdSizeAdd.AdSizeName = adSizeModel.Name;
                    objAdSizeAdd.MediaAssetId = adSizeModel.MediaAssetId;
                    objAdSizeAdd.PageFraction = adSizeModel.PageFraction;
                    objAdSizeAdd.CreatedOn = DateTime.Now;
                    //objAdSizeAdd.DeletedInd = false;

                    //*
                    objAdSizeAdd.UpdatedOn = DateTime.Now;
                    objAdSizeAdd.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                    objAdSizeAdd.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.AdSizes.Add(objAdSizeAdd);
                    DB.SaveChanges();

                    response.Data = objAdSizeAdd.AdSizeId;
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    response.StatusCode = ApiStatus.AlreadyExist;
                    response.Message = ApiStatus.AlreadyExistCustomMessge("Ad Size");
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon PutAdSize(GetAdSizeModel adSizeModel)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                int isNameExist = DB.AdSizes
                                    .Where(x => x.AdSizeName.ToLower() == adSizeModel.Name.ToLower()
                                            && x.AdSizeId != adSizeModel.AdSizeId
                                            /*&& x.DeletedInd == false*/).Count();
                if (isNameExist > 0)
                {
                    response.StatusCode = ApiStatus.AlreadyExist;
                    response.Message = ApiStatus.AlreadyExistCustomMessge("Ad Size Name");
                    return response;
                }

                AdSize objAdtypeEdit = DB.AdSizes.Where(x =>/* x.DeletedInd != true && */
                x.AdSizeId == adSizeModel.AdSizeId)
                                                 .FirstOrDefault();
                if (objAdtypeEdit != null)
                {
                    objAdtypeEdit.AdSizeName = adSizeModel.Name;
                    objAdtypeEdit.MediaAssetId = adSizeModel.MediaAssetId;
                    objAdtypeEdit.PageFraction = adSizeModel.PageFraction;
                    objAdtypeEdit.UpdatedOn = DateTime.Now;

                    //*
                    objAdtypeEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objAdtypeEdit).State = EntityState.Modified;
                    DB.SaveChanges();
                    response.Data = adSizeModel.AdSizeId;
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    response.StatusCode = ApiStatus.NotFound;
                    response.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon DeleteAdSize(int adSizeId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                AdSize objAdSize = DB.AdSizes.Where(x => x.AdSizeId == adSizeId).FirstOrDefault();
                if (objAdSize != null)
                {
                    //objAdSize.DeletedInd = true;

                    //*
                    objAdSize.UpdatedOn = DateTime.Now;
                    objAdSize.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objAdSize).State = EntityState.Modified;
                    int status = DB.SaveChanges();

                    response.Data = status <= 0 ? 0 : 1;
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    response.StatusCode = ApiStatus.NotFound;
                    response.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }

        public ResponseCommon GetAdSizesByRateCardIdAndColordId(int rateCardId, int adColorId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<GetAdSizeModel> lstAdSizes = (from rc in DB.RateCards
                                                   join rd in DB.RateCardDetails on rc.RateCardId equals rd.RateCardId
                                                   join s in DB.AdSizes on rd.AdSizeId equals s.AdSizeId
                                                   where rd.RateCardId == rateCardId && rd.AdColorId == adColorId
                                                          //&& rc.DeletedInd == false
                                                          //&& rd.DeletedInd == false
                                                          //&& s.DeletedInd == false
                                                   select new GetAdSizeModel()
                                                   {
                                                       AdSizeId = s.AdSizeId,
                                                       Name = s.AdSizeName,
                                                   }).Distinct().ToList();
                if (lstAdSizes.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }

                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstAdSizes;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }
        public ResponseCommon GetAdSizesByRateCardId(int rateCardId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<GetAdSizeModel> lstAdSizes = (from rc in DB.RateCards
                                                   join rd in DB.RateCardDetails on rc.RateCardId equals rd.RateCardId
                                                   join s in DB.AdSizes on rd.AdSizeId equals s.AdSizeId
                                                   where rd.RateCardId == rateCardId
                                                          //&& rc.DeletedInd == false
                                                          //&& rd.DeletedInd == false
                                                          //&& s.DeletedInd == false
                                                   select new GetAdSizeModel()
                                                   {
                                                       AdSizeId = s.AdSizeId,
                                                       Name = s.AdSizeName,
                                                   }).Distinct().ToList();
                if (lstAdSizes.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }

                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstAdSizes;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }
        public ResponseCommon GetAdSizesByMediaAsset(int mediaAssetId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<GetAdSizeModel> lstAdSizes = (from a in DB.AdSizes
                                                   where 
                                                   //a.DeletedInd == false &&
                                                   a.MediaAssetId == mediaAssetId
                                                   select new GetAdSizeModel()
                                                   {
                                                       AdSizeId = a.AdSizeId,
                                                       Name = a.AdSizeName,
                                                   }).Distinct().ToList();
                // 
                //List<GetAdSizeModel> lstAdSizes = (from rc in DB.RateCards
                //                                   join rd in DB.RateCardDetails on rc.RateCardId equals rd.RateCardId
                //                                   join s in DB.AdSizes on rd.AdSizeId equals s.AdSizeId
                //                                   where rc.MediaAssetId == mediaAssetId
                //                                          && rc.DeletedInd == false
                //                                          && rd.DeletedInd == false
                //                                          && s.DeletedInd == false
                //                                   select new GetAdSizeModel()
                //                                   {
                //                                       AdSizeId = s.AdSizeId,
                //                                       Name = s.AdSizeName,
                //                                   }).Distinct().ToList();
                if (lstAdSizes.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }

                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstAdSizes;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }
        #endregion
    }
}
