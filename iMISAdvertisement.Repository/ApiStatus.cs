﻿namespace Asi.Repository
{
    public static class ApiStatus 
    {
        public static int Ok = 1;
        public static string OkMessge = "";

        public static int Exception = 2;
        public static string ExceptionMessge = "Something went wrong";

        public static int NoRecords = 3;
        public static string NoRecordsMessge = "No such a record";

        public static int AlreadyExist = 4;
        public static string AlreadyExistMessge = "Data with same name already exists";
        public static string AlreadyExistCustomMessge(string msg = "Record ")
        {
            return string.Format("{0} already exists", msg);
        }

        public static int NotFound = 5;
        public static string NotFoundMessage = "Record not found";

        public static int InvalidInput = 6;
        public static string InvalidInputMessge = "Invalid input";

        public static int OutOfInventory = 7;
        public static string OutOfInventoryMessge = "Out of inventory";

    }
}
