﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.Model.StaticList;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Asi.Repository
{
    public class RateCardRepository : IRateCardRepository
    {
        private readonly IDBEntities DB;
        public RateCardRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        #region Public Methods
        public ResponseCommon GetRateCards()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<GetRateCardModel> lstRateCards = new List<GetRateCardModel>();
                lstRateCards = DB.RateCards
                                        //.Where(x => x.DeletedInd != true)
                                        .Select(x => new GetRateCardModel
                                        {
                                            RateCardId = x.RateCardId,
                                            MediaAssetId = x.MediaAssetId,
                                            MediaAssetName = x.MediaAsset.MediaAssetName,
                                            MediaTypeId = x.MediaTypeId,
                                            MediaTypeName = x.MediaTypeRef.MediaTypeName,
                                            Name = x.RateCardName,
                                            AdTypeId = x.AdTypeId,
                                            AdTypeName = x.AdType.AdTypeName,

                                            //* 2020-11-10 Change
                                            //MediaBillingMethodId = x.MediaBillingMethodId,
                                            //MediaBillingMethodName = x.MediaBillingMethod.MediaBillingMethodName,

                                            MediaBillingMethodName = x.MediaBillingMethodName,

                                            ValidFromDate = x.ValidFromDate,
                                            ValidToDate = x.ValidToDate,
                                            ProductCode = x.ProductCode
                                        }).AsEnumerable() // database query ends here, the rest is a query in memory
                                        .Select(x => new GetRateCardModel
                                        {
                                            RateCardId = x.RateCardId,
                                            MediaAssetId = x.MediaAssetId,
                                            MediaAssetName = x.MediaAssetName,
                                            MediaTypeId = x.MediaTypeId,
                                            MediaTypeName = x.MediaTypeName,
                                            Name = x.Name,
                                            AdTypeId = x.AdTypeId,
                                            AdTypeName = x.AdTypeName,
                                            //MediaBillingMethodId = Get_MediaBillingMethodName_Values.GetMediaBillingMethodNameValues()
                                            //                                                        .Where(x1 => x1.MediaBillingMethodName == x.MediaBillingMethodName)
                                            //                                                        .Select(x1 => x1.MediaBillingMethodId).FirstOrDefault(),                                                                                                                

                                            MediaBillingMethodId = Enum.GetValues(typeof(BillingMethod)).Cast<BillingMethod>()
                                                                    .Where(x1 => x1.ToString() == x.MediaBillingMethodName.Replace(" ", "_"))
                                                                    .Select(x1 => (int)x1).FirstOrDefault(),
                                            MediaBillingMethodName = x.MediaBillingMethodName,
                                            ValidFromDate = x.ValidFromDate,
                                            ValidToDate = x.ValidToDate,
                                            ProductCode = x.ProductCode
                                        }).ToList();

                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstRateCards;
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
            }
            return objResponse;
        }
        public ResponseCommon GetRateCardById(int rateCardId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                GetRateCardModel objRateCard = DB.RateCards
                                                .Where(x => /*x.DeletedInd != true &&*/ x.RateCardId == rateCardId)
                                                .Select(x => new GetRateCardModel
                                                {
                                                    RateCardId = x.RateCardId,
                                                    MediaAssetId = x.MediaAssetId,
                                                    MediaAssetName = x.MediaAsset.MediaAssetName,
                                                    MediaTypeId = x.MediaTypeId,
                                                    MediaTypeName = x.MediaTypeRef.MediaTypeName,
                                                    Name = x.RateCardName,
                                                    AdTypeId = x.AdTypeId,
                                                    AdTypeName = x.AdType.AdTypeName,

                                                    //* 2020-11-10 Change :
                                                    //MediaBillingMethodId = x.MediaBillingMethodId,
                                                    //MediaBillingMethodName = x.MediaBillingMethod.MediaBillingMethodName,                                                  
                                                    MediaBillingMethodName = x.MediaBillingMethodName,

                                                    ValidFromDate = x.ValidFromDate,
                                                    ValidToDate = x.ValidToDate,
                                                    ProductCode = x.ProductCode
                                                }).AsEnumerable()
                                                .Select(x => new GetRateCardModel
                                                {
                                                    RateCardId = x.RateCardId,
                                                    MediaAssetId = x.MediaAssetId,
                                                    MediaAssetName = x.MediaAssetName,
                                                    MediaTypeId = x.MediaTypeId,
                                                    MediaTypeName = x.MediaTypeName,
                                                    Name = x.Name,
                                                    AdTypeId = x.AdTypeId,
                                                    AdTypeName = x.AdTypeName,

                                                    //MediaBillingMethodId = Get_MediaBillingMethodName_Values.GetMediaBillingMethodNameValues()
                                                    //                                                .Where(x1 => x1.MediaBillingMethodName == x.MediaBillingMethodName)
                                                    //                                                .Select(x1 => x1.MediaBillingMethodId).FirstOrDefault(),

                                                    MediaBillingMethodId = Enum.GetValues(typeof(BillingMethod)).Cast<BillingMethod>()
                                                                            .Where(x1 => x1.ToString() == x.MediaBillingMethodName.Replace(" ", "_"))
                                                                            .Select(x1 => (int)x1).FirstOrDefault(),
                                                    MediaBillingMethodName = x.MediaBillingMethodName,

                                                    ValidFromDate = x.ValidFromDate,
                                                    ValidToDate = x.ValidToDate,
                                                    ProductCode = x.ProductCode
                                                }).FirstOrDefault();
                if (objRateCard != null)
                {
                    response.Data = objRateCard;
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    response.Message = ApiStatus.NotFoundMessage;
                    response.StatusCode = ApiStatus.NotFound;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon GetRateCardsByMediaAsset(int mediaAssetId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                List<GetRateCardsByMediaAssetModel> lstRateCards = DB.MediaAssets.Where(x => /*x.DeletedInd != true &&*/ x.MediaAssetId == mediaAssetId)
                    .Select(x => new GetRateCardsByMediaAssetModel
                    {
                        MediaAssetId = x.MediaAssetId,
                        MediaAssetName = x.MediaAssetName,
                        RateCards = x.RateCards
                        //.Where(y => y.DeletedInd != true)
                        .Select(y => new GetRateCardModel
                        {
                            RateCardId = y.RateCardId,
                            MediaAssetId = y.MediaAssetId,
                            MediaAssetName = y.MediaAsset.MediaAssetName,
                            MediaTypeId = y.MediaTypeId,
                            MediaTypeName = y.MediaTypeRef.MediaTypeName,
                            Name = y.RateCardName,
                            AdTypeId = y.AdTypeId,
                            AdTypeName = y.AdType.AdTypeName,

                            //* 2020-11-10 Change :
                            //MediaBillingMethodId = y.MediaBillingMethodId,
                            //MediaBillingMethodName = y.MediaBillingMethod.MediaBillingMethodName,

                            MediaBillingMethodName = y.MediaBillingMethodName,

                            ValidFromDate = y.ValidFromDate,
                            ValidToDate = y.ValidToDate,
                            ProductCode = y.ProductCode
                        })
                    }).AsEnumerable()
                    .Select(x => new GetRateCardsByMediaAssetModel
                    {
                        MediaAssetId = x.MediaAssetId,
                        MediaAssetName = x.MediaAssetName,
                        RateCards = x.RateCards
                            .Select(y => new GetRateCardModel
                            {
                                RateCardId = y.RateCardId,
                                MediaAssetId = y.MediaAssetId,
                                MediaAssetName = y.MediaAssetName,
                                MediaTypeId = y.MediaTypeId,
                                MediaTypeName = y.MediaTypeName,
                                Name = y.Name,
                                AdTypeId = y.AdTypeId,
                                AdTypeName = y.AdTypeName,

                                //MediaBillingMethodId = Get_MediaBillingMethodName_Values.GetMediaBillingMethodNameValues()
                                //                        .Where(x1 => x1.MediaBillingMethodName == y.MediaBillingMethodName)
                                //                        .Select(x1 => x1.MediaBillingMethodId).FirstOrDefault(),

                                MediaBillingMethodId = Enum.GetValues(typeof(BillingMethod)).Cast<BillingMethod>()
                                                        .Where(x1 => x1.ToString() == y.MediaBillingMethodName.Replace(" ", "_"))
                                                        .Select(x1 => (int)x1).FirstOrDefault(),

                                MediaBillingMethodName = y.MediaBillingMethodName,

                                ValidFromDate = y.ValidFromDate,
                                ValidToDate = y.ValidToDate,
                                ProductCode = y.ProductCode
                            })
                    }).ToList();
                response.Data = lstRateCards;
                response.StatusCode = ApiStatus.Ok;
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon PostRateCard(PostRateCardModel rateCardModel)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                int isRecordExist = DB.RateCards.Where(x => x.RateCardName.ToLower() == rateCardModel.Name.ToLower()
                                                    /*&& x.DeletedInd == false*/)
                                                     .Count();
                if (isRecordExist <= 0)
                {
                    if (rateCardModel.IsCopy && rateCardModel.SourceRateCardId > 0)
                    {
                        var sourceRateCard = DB.RateCards.Where(x => x.RateCardId == rateCardModel.SourceRateCardId /*&& x.DeletedInd != true*/).FirstOrDefault();
                        if (sourceRateCard != null)
                        {
                            RateCard objNewRateCard = new RateCard()
                            {
                                MediaAssetId = rateCardModel.MediaAssetId,
                                MediaTypeId = rateCardModel.MediaTypeId,
                                RateCardName = rateCardModel.Name,
                                AdTypeId = rateCardModel.AdTypeId,

                                //* 2020-11-10 Change
                                //MediaBillingMethodId = rateCardModel.MediaBillingMethodId,

                                //MediaBillingMethodName = Get_MediaBillingMethodName_Values.GetMediaBillingMethodNameValues()
                                //                                                    .Where(x1 => x1.MediaBillingMethodId == rateCardModel.MediaBillingMethodId)
                                //                                                    .Select(x1 => x1.MediaBillingMethodName).FirstOrDefault(),

                                MediaBillingMethodName = Enum.GetValues(typeof(BillingMethod)).Cast<BillingMethod>()
                                                            .Where(x1 => (int)x1 == rateCardModel.MediaBillingMethodId)
                                                            .Select(x1 => x1.ToString().Replace("_", " ")).FirstOrDefault(),
                                ValidFromDate = rateCardModel.ValidFromDate,
                                ValidToDate = rateCardModel.ValidToDate,
                                ProductCode = rateCardModel.ProductCode,
                                CreatedOn = DateTime.Now,
                                //DeletedInd = false,

                                //*
                                UpdatedOn = DateTime.Now,
                                CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                UpdatedByUserKey = Constants.Created_Update_ByUserKey
                            };
                            DB.RateCards.Add(objNewRateCard);
                            DB.SaveChanges();
                            rateCardModel.RateCardId = objNewRateCard.RateCardId;

                            var lstsourceRateCardDetails = DB.RateCardDetails.Where(x => x.RateCardId == sourceRateCard.RateCardId /*&& x.DeletedInd != true*/).ToList();
                            List<RateCardDetail> lstNewRateCardDetails = new List<RateCardDetail>();
                            if (lstsourceRateCardDetails != null)
                            {
                                foreach (var rateCardDetail in lstsourceRateCardDetails)
                                {
                                    RateCardDetail detail = new RateCardDetail()
                                    {
                                        RateCardId = objNewRateCard.RateCardId,
                                        AdColorId = rateCardDetail.AdColorId,
                                        AdSizeId = rateCardDetail.AdSizeId,
                                        AdTypeId = rateCardDetail.AdTypeId,
                                        FrequencyId = rateCardDetail.FrequencyId,
                                        RateCardCost = rateCardDetail.RateCardCost,
                                        CreatedOn = DateTime.Now,
                                        //DeletedInd = false,

                                        //*
                                        UpdatedOn = DateTime.Now,
                                        CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                        UpdatedByUserKey = Constants.Created_Update_ByUserKey
                                    };
                                    lstNewRateCardDetails.Add(detail);
                                }
                            }
                            if (lstNewRateCardDetails.Count > 0)
                            {
                                DB.RateCardDetails.AddRange(lstNewRateCardDetails);
                            }
                        }
                    }
                    else
                    {
                        RateCard objCard = new RateCard();
                        objCard.MediaAssetId = rateCardModel.MediaAssetId;
                        objCard.MediaTypeId = rateCardModel.MediaTypeId;
                        objCard.RateCardName = rateCardModel.Name;
                        objCard.AdTypeId = rateCardModel.AdTypeId;

                        //* 2020-11-10 Change
                        //objCard.MediaBillingMethodId = rateCardModel.MediaBillingMethodId;

                        //objCard.MediaBillingMethodName = Get_MediaBillingMethodName_Values.GetMediaBillingMethodNameValues()
                        //                                                            .Where(x1 => x1.MediaBillingMethodId == rateCardModel.MediaBillingMethodId)
                        //                                                            .Select(x1 => x1.MediaBillingMethodName).FirstOrDefault();

                        objCard.MediaBillingMethodName = Enum.GetValues(typeof(BillingMethod)).Cast<BillingMethod>()
                                                        .Where(x1 => (int)x1 == rateCardModel.MediaBillingMethodId)
                                                        .Select(x1 => x1.ToString().Replace("_", " ")).FirstOrDefault();

                        objCard.ValidFromDate = rateCardModel.ValidFromDate;
                        objCard.ValidToDate = rateCardModel.ValidToDate;
                        objCard.ProductCode = rateCardModel.ProductCode;

                        objCard.CreatedOn = DateTime.Now;
                        //objCard.DeletedInd = false;

                        //*
                        objCard.UpdatedOn = DateTime.Now;
                        objCard.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                        objCard.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                        DB.RateCards.Add(objCard);
                    }

                    DB.SaveChanges();

                    response.Data = rateCardModel.RateCardId;
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    response.StatusCode = ApiStatus.AlreadyExist;
                    response.Message = ApiStatus.AlreadyExistMessge;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon PutRateCard(PostRateCardModel rateCardModel)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                int isNameExist = DB.RateCards
                                    .Where(x => x.RateCardName.ToLower() == rateCardModel.Name.ToLower()
                                            && x.RateCardId != rateCardModel.RateCardId
                                            /*&& x.DeletedInd == false*/).Count();
                if (isNameExist > 0)
                {
                    response.StatusCode = ApiStatus.AlreadyExist;
                    response.Message = ApiStatus.AlreadyExistCustomMessge("Rate Card Name");
                    return response;
                }
                RateCard objCard = DB.RateCards.Where(x => /*x.DeletedInd != true &&*/ x.RateCardId == rateCardModel.RateCardId).FirstOrDefault();
                if (objCard != null)
                {
                    objCard.MediaAssetId = rateCardModel.MediaAssetId;
                    objCard.MediaTypeId = rateCardModel.MediaTypeId;
                    objCard.RateCardName = rateCardModel.Name;
                    objCard.AdTypeId = rateCardModel.AdTypeId;

                    //* 2020-11-10 Change
                    //objCard.MediaBillingMethodId = rateCardModel.MediaBillingMethodId;

                    //objCard.MediaBillingMethodName = Get_MediaBillingMethodName_Values.GetMediaBillingMethodNameValues()
                    //                                                                .Where(x1 => x1.MediaBillingMethodId == rateCardModel.MediaBillingMethodId)
                    //                                                                .Select(x1 => x1.MediaBillingMethodName).FirstOrDefault();

                    objCard.MediaBillingMethodName = Enum.GetValues(typeof(BillingMethod)).Cast<BillingMethod>()
                                                    .Where(x1 => (int)x1 == rateCardModel.MediaBillingMethodId)
                                                    .Select(x1 => x1.ToString().Replace("_", " ")).FirstOrDefault();

                    objCard.ValidFromDate = rateCardModel.ValidFromDate;
                    objCard.ValidToDate = rateCardModel.ValidToDate;
                    objCard.ProductCode = rateCardModel.ProductCode;

                    objCard.UpdatedOn = DateTime.Now;

                    //*
                    objCard.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objCard).State = EntityState.Modified;
                    DB.SaveChanges();

                    response.Data = rateCardModel.RateCardId;
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    response.StatusCode = ApiStatus.NotFound;
                    response.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon DeleteRateCard(int rateCardId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                int status = 0;
                RateCard objRateCardEdit = DB.RateCards
                                     .Where(x => /*x.DeletedInd != true &&*/ x.RateCardId == rateCardId)
                                     .FirstOrDefault();
                if (objRateCardEdit != null)
                {
                    //objRateCardEdit.DeletedInd = true;
                    DB.Entry(objRateCardEdit).State = EntityState.Modified;

                    // Remove RateCardDetails of given RateCard
                    List<RateCardDetail> lstRateCardDetails = DB.RateCardDetails.Where(x => x.RateCardId == objRateCardEdit.RateCardId /*&& x.DeletedInd == false*/).ToList();
                    foreach (var rateCardDetail in lstRateCardDetails)
                    {
                        //rateCardDetail.DeletedInd = true;
                        rateCardDetail.UpdatedOn = DateTime.Now;

                        //*
                        rateCardDetail.UpdatedByUserKey = Constants.Created_Update_ByUserKey;
                        DB.Entry(rateCardDetail).State = EntityState.Modified;
                    }

                    status = DB.SaveChanges();
                    response.Data = status <= 0 ? 0 : 1;
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    response.StatusCode = ApiStatus.NotFound;
                    response.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        #endregion
    }
}
