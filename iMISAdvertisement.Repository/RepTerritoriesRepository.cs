﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Asi.Repository
{
    public class RepTerritoriesRepository : IRepTerritoriesRepository
    {
        private readonly IDBEntities DB;
        public RepTerritoriesRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        public ResponseCommon GetRepTerritories()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<GetRepTerritoryModel> lstRep = new List<GetRepTerritoryModel>();
                lstRep = DB.RepTerritories
                                //.Where(x => x.DeletedInd != true)
                                .Select(x => new GetRepTerritoryModel
                                {
                                    RepTerretoryId = x.RepTerritoryId,

                                    RepId = x.RepId,
                                    RepName = x.Rep.RepName,

                                    TerritoryId = x.TerritoryId,
                                    TerritoryName = x.Territory.TerritoryName,

                                    Commission = x.Commission
                                })
                                .OrderBy(x => x.RepId)
                                .ThenBy(x => x.TerritoryId)
                                .ToList();
                if (lstRep.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }
                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstRep;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetRepTerritoriesByRepId(int repId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<GetRepTerritoryModel> objRep = null;
                objRep = DB.RepTerritories.Where(x => /*x.DeletedInd != true &&*/ x.RepId == repId)
                                .Select(x => new GetRepTerritoryModel
                                {
                                    RepTerretoryId = x.RepTerritoryId,

                                    RepId = x.RepId,
                                    RepName = x.Rep.RepName,

                                    TerritoryId = x.TerritoryId,
                                    TerritoryName = x.Territory.TerritoryName,

                                    Commission = x.Commission
                                }).ToList();
                if (objRep == null)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Data = null;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                }
                else
                {
                    objResponse.Data = objRep;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetRepTerritoriesByTerritoryId(int territoryId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<GetRepTerritoryModel> objRep = null;
                objRep = DB.RepTerritories.Where(x => /*x.DeletedInd != true && */
                                                    x.TerritoryId == territoryId)
                                .Select(x => new GetRepTerritoryModel
                                {
                                    RepTerretoryId = x.RepTerritoryId,

                                    RepId = x.RepId,
                                    RepName = x.Rep.RepName,

                                    TerritoryId = x.TerritoryId,
                                    TerritoryName = x.Territory.TerritoryName,

                                    Commission = x.Commission
                                })
                                .ToList();
                if (objRep == null)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                }
                else
                {
                    objResponse.Data = objRep;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon PostRepTerritory(RepTerritoryModel repTerritoryModel)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                int isRepExist = DB.RepTerritories
                                    .Where(x => /*x.DeletedInd == false && */
                                                    x.RepId == repTerritoryModel.RepId
                                                && x.TerritoryId == repTerritoryModel.TerritoryId).Count();
                if (isRepExist <= 0)
                {
                    RepTerritory obj = new RepTerritory();
                    obj.RepId = repTerritoryModel.RepId;
                    obj.TerritoryId = repTerritoryModel.TerritoryId;
                    obj.Commission = repTerritoryModel.Commission;

                    obj.CreatedOn = DateTime.Now;
                    //obj.DeletedInd = false;

                    //*
                    obj.UpdatedOn = DateTime.Now;
                    obj.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                    obj.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.RepTerritories.Add(obj);
                    DB.SaveChanges();

                    objResponse.Data = obj.RepTerritoryId;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.AlreadyExist;
                    objResponse.Message = ApiStatus.AlreadyExistMessge;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon PutRepTerritory(RepTerritoryModel repTerritoryModel)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                RepTerritory objUpdateRep = DB.RepTerritories.Where(x => /*x.DeletedInd != true && */
                                                                        x.RepId == repTerritoryModel.RepId
                                                                    && x.TerritoryId == repTerritoryModel.TerritoryId)
                                                             .FirstOrDefault();
                if (objUpdateRep != null)
                {
                    objUpdateRep.Commission = repTerritoryModel.Commission;
                    objUpdateRep.UpdatedOn = DateTime.Now;

                    //*
                    objUpdateRep.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objUpdateRep).State = EntityState.Modified;
                    DB.SaveChanges();

                    objResponse.Data = objUpdateRep.RepTerritoryId;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon DeleteRepTerritoryByRepIdTerritoryId(int repId, int territoryId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                RepTerritory objRep = DB.RepTerritories.Where(x => /*x.DeletedInd != true && */
                                                                    x.RepId == repId
                                                                    && x.TerritoryId == territoryId)
                                                        .FirstOrDefault();
                if (objRep != null)
                {
                    //objRep.DeletedInd = true;

                    //*
                    objRep.UpdatedOn = DateTime.Now;
                    objRep.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objRep).State = EntityState.Modified;
                    int status = DB.SaveChanges();
                    objResponse.Data = status <= 0 ? 0 : 1;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }
    }
}
