﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Asi.Repository
{
    public class FrequencyRepository : IFrequencyRepository
    {
        private readonly IDBEntities DB;
        public FrequencyRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        #region Public Methods
        public ResponseCommon GetFrequencies()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<FrequencyModel> lstFrequency = new List<FrequencyModel>();
                lstFrequency = DB.Frequencies/*.Where(x => x.DeletedInd != true)*/
                    .Select(x => new FrequencyModel
                    {
                        FrequencyId = x.FrequencyId,
                        Name = x.FrequencyName
                    })
                    .OrderBy(x => x.Name)
                    .ToList();

                if (lstFrequency.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }

                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstFrequency;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetFrequencyById(int frequencyId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                FrequencyModel objFrequency = null;
                objFrequency = DB.Frequencies.Where(x => /*x.DeletedInd != true &&*/ x.FrequencyId == frequencyId)
                                        .Select(x => new FrequencyModel
                                        {
                                            FrequencyId = x.FrequencyId,
                                            Name = x.FrequencyName
                                        }).FirstOrDefault();
                if (objFrequency == null)
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                    return objResponse;
                }
                objResponse.Data = objFrequency;
                objResponse.StatusCode = ApiStatus.Ok;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetFrequencyByRateCardIdAndColordId(int rateCardId, int adColorId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<FrequencyModel> lstFrequency = (from rc in DB.RateCards
                                                     join rd in DB.RateCardDetails on rc.RateCardId equals rd.RateCardId
                                                     join s in DB.Frequencies on rd.FrequencyId equals s.FrequencyId
                                                     where rd.RateCardId == rateCardId && rd.AdColorId == adColorId
                                                            //&& rc.DeletedInd == false
                                                            //&& rd.DeletedInd == false
                                                            //&& s.DeletedInd == false
                                                     select new FrequencyModel()
                                                     {
                                                         FrequencyId = s.FrequencyId,
                                                         Name = s.FrequencyName,
                                                     }).Distinct().ToList();
                if (lstFrequency.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }

                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstFrequency;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetFrequencyByRateCardIdColordIdAndAdSizeId(int rateCardId, int adColorId, int adSizeId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<FrequencyModel> lstFrequency = (from rc in DB.RateCards
                                                     join rd in DB.RateCardDetails on rc.RateCardId equals rd.RateCardId
                                                     join s in DB.Frequencies on rd.FrequencyId equals s.FrequencyId
                                                     where rd.RateCardId == rateCardId && rd.AdColorId == adColorId && rd.AdSizeId == adSizeId
                                                            //&& rc.DeletedInd == false
                                                            //&& rd.DeletedInd == false
                                                            //&& s.DeletedInd == false
                                                     select new FrequencyModel()
                                                     {
                                                         FrequencyId = s.FrequencyId,
                                                         Name = s.FrequencyName,
                                                     }).Distinct().ToList();
                if (lstFrequency.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }

                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstFrequency;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon PostFrequency(FrequencyModel frequency)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                int isRecordExist = DB.Frequencies.Where(x => x.FrequencyName.ToLower() == frequency.Name.ToLower()
                                                        /*&& x.DeletedInd == false*/).Count();
                if (isRecordExist <= 0)
                {
                    Frequency objFrequencyAdd = new Frequency();
                    objFrequencyAdd.FrequencyName = frequency.Name;
                    objFrequencyAdd.CreatedOn = DateTime.Now;
                    //objFrequencyAdd.DeletedInd = false;

                    //*
                    objFrequencyAdd.UpdatedOn = DateTime.Now;
                    objFrequencyAdd.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                    objFrequencyAdd.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Frequencies.Add(objFrequencyAdd);
                    DB.SaveChanges();

                    objResponse.Data = objFrequencyAdd.FrequencyId;
                    objResponse.StatusCode = ApiStatus.Ok;

                    return objResponse;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.AlreadyExist;
                    objResponse.Message = ApiStatus.AlreadyExistCustomMessge("Frequency Name");

                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon PutFrequency(FrequencyModel frequency)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                Frequency objFrequencyEdit = null;
                int isNameExist = DB.Frequencies
                                    .Where(x => x.FrequencyName.ToLower() == frequency.Name.ToLower()
                                            && x.FrequencyId != frequency.FrequencyId
                                            /*&& x.DeletedInd == false*/).Count();
                if (isNameExist > 0)
                {
                    objResponse.StatusCode = ApiStatus.AlreadyExist;
                    objResponse.Message = ApiStatus.AlreadyExistCustomMessge("Frequency Name");
                    return objResponse;
                }

                objFrequencyEdit = DB.Frequencies.Where(x => /*x.DeletedInd != true &&*/ x.FrequencyId == frequency.FrequencyId).FirstOrDefault();
                if (objFrequencyEdit != null)
                {
                    objFrequencyEdit.FrequencyName = frequency.Name;
                    objFrequencyEdit.UpdatedOn = DateTime.Now;

                    //*
                    objFrequencyEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objFrequencyEdit).State = EntityState.Modified;
                    DB.SaveChanges();

                    objResponse.Data = objFrequencyEdit.FrequencyId;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon DeleteFrequency(int frequencyId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                Frequency objFrequencyEdit = null;
                int status = 0;
                objFrequencyEdit = DB.Frequencies.Where(x => /*x.DeletedInd != true &&*/ x.FrequencyId == frequencyId).FirstOrDefault();
                if (objFrequencyEdit != null)
                {
                    //objFrequencyEdit.DeletedInd = true;

                    //*
                    objFrequencyEdit.UpdatedOn = DateTime.Now;
                    objFrequencyEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objFrequencyEdit).State = EntityState.Modified;
                    status = DB.SaveChanges();
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }
        #endregion
    }
}
