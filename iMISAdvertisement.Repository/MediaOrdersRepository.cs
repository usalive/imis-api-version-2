﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web;
using System.ComponentModel;
using System.Net;
using System.Net.Security;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Diagnostics;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
//using Asi.DAL.Model.StaticList;

namespace Asi.Repository
{
    public class MediaOrdersRepository : IMediaOrdersRepository
    {
        private readonly IDBEntities DB;
        IRateCardDetailRepository _IRateCardDetailRepository = null;
        IMediaOrderLinesRepository _IMediaOrderLinesRepository = null;
        IMediaOrderProductionDetailsRepository _IMediaOrderProductionDetailsRepository = null;
        IAdAdjustmentRepository _IAdAdjustmentRepository = null;

        #region Constructor
        public MediaOrdersRepository(IDBEntities dBEntities, IRateCardDetailRepository cardDetailRepository, IMediaOrderLinesRepository orderLinesRepository, IMediaOrderProductionDetailsRepository productionDetailsRepository, IAdAdjustmentRepository adjustmentRepository)
        {
            DB = dBEntities;
            _IRateCardDetailRepository = cardDetailRepository;
            _IMediaOrderLinesRepository = orderLinesRepository;
            _IMediaOrderProductionDetailsRepository = productionDetailsRepository;
            _IAdAdjustmentRepository = adjustmentRepository;
        }
        #endregion

        #region Private Methods

        private long GetGenerateBuyId()
        {
            MediaOrderCostModel model = new MediaOrderCostModel();
            long buyId = 0;
            try
            {
                MediaOrderBuy mediaOrdersBuy = DB.MediaOrdersBuys.FirstOrDefault();
                if (mediaOrdersBuy == null)
                {
                    mediaOrdersBuy = new MediaOrderBuy()
                    {
                        UpdatedOn = DateTime.Now,
                        BuyId = 1000
                    };
                    DB.MediaOrdersBuys.Add(mediaOrdersBuy);
                }
                else
                {
                    mediaOrdersBuy.BuyId = mediaOrdersBuy.BuyId + 1;
                    DB.Entry(mediaOrdersBuy).State = EntityState.Modified;
                }
                DB.SaveChanges();
                buyId = mediaOrdersBuy.BuyId;
            }

            catch (DbUpdateException ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                var entries = ex.Entries;
                foreach (var entry in entries)
                {
                    //change state to remove it from context 
                    switch (entry.State)
                    {
                        case EntityState.Added:
                            entry.State = EntityState.Detached;
                            break;
                        case EntityState.Modified:
                            entry.CurrentValues.SetValues(entry.OriginalValues);
                            entry.State = EntityState.Unchanged;
                            break;
                        case EntityState.Deleted:
                            entry.State = EntityState.Unchanged;
                            break;
                    }
                    // entry.State = System.Data.Entity.EntityState.Detached;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.LogError(ex);
            }
            return buyId;
        }

        private List<PartyModel> GetAllASIParty(ref DataTable dtError, string asiPartApiUrl, string asiPartyAuthToken, string oName)
        {
            List<PartyModel> lstAsiPart = new List<PartyModel>();
            string returnValue = "0";
            try
            {
                string asiUserName = ConfigurationManager.AppSettings["ASIusername"].ToString();
                string asiPassword = ConfigurationManager.AppSettings["ASIpassword"].ToString();
                string asiGrantType = ConfigurationManager.AppSettings["ASIgrant_type"].ToString();

                //ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });

                string basePath = asiPartApiUrl.Replace("api/Party", "");

                var client2 = new HttpClient();
                //client2.BaseAddress = new Uri("https://imistest.com/");
                client2.BaseAddress = new Uri(basePath);
                var request = new HttpRequestMessage(HttpMethod.Post, "token");

                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("username", asiUserName));
                keyValues.Add(new KeyValuePair<string, string>("password", asiPassword));
                keyValues.Add(new KeyValuePair<string, string>("grant_type", asiGrantType));

                request.Content = new FormUrlEncodedContent(keyValues);
                var responseMessage = client2.SendAsync(request);
                var responseJSONData = responseMessage.Result.Content.ReadAsStringAsync().Result;
                var responseData = JsonConvert.DeserializeObject(responseJSONData);
                int issuedIndexOf = responseJSONData.LastIndexOf(".issued");
                int expiresIndexOf = responseJSONData.LastIndexOf(".expires");

                var textAfterissuedIndexOf = responseJSONData.Substring(issuedIndexOf);
                //var textAfterexpiresIndexOf = responseJSONData.Substring(expiresIndexOf);

                var textAfterReplaceissuedIndexOf = textAfterissuedIndexOf.Replace(".issued", "issued").Replace(".expires", "expires");
                //var textAfterReplaceexpiresIndexOf = textAfterexpiresIndexOf.Replace(".expires", "expires");
                var responseJSONDataAfterChanged = responseJSONData.Replace(textAfterissuedIndexOf, textAfterReplaceissuedIndexOf);
                TokenAPIResponse tokenAPIResponse = JsonConvert.DeserializeObject<TokenAPIResponse>(responseJSONDataAfterChanged);


                if (!string.IsNullOrEmpty(tokenAPIResponse.Access_token))
                {
                    int Offset = 0;
                    List<PartyModel> lstParty = new List<PartyModel>();
                    lstAsiPart = GetPartyData(ref dtError, ref lstParty, tokenAPIResponse, Offset, basePath);
                    // CommonUtility.writeError(lstAsiPart.Count().ToString());
                }
            }

            catch (Exception ex)
            {
                string cError = "";
                CreateErrorOfDatatableForImportOrder(ref dtError, "", ex.Message, cError);
            }
            return lstAsiPart;
        }

        private List<PartyModel> GetPartyData(ref DataTable dtError, ref List<PartyModel> lstParty, TokenAPIResponse tokenAPIResponse, int offset, string basePath)
        {
            dynamic data = null;

            try
            {
                if (!string.IsNullOrEmpty(tokenAPIResponse.Access_token))
                {
                    string getPartyPath = string.Empty;
                    getPartyPath = "api/Party?limit=500&Offset=" + offset;
                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri(basePath);
                    // Add an Accept header for JSON format.  
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenAPIResponse.Access_token);
                    // List all Names.  
                    HttpResponseMessage response = client.GetAsync(getPartyPath).Result;  // Blocking call!  
                    string text = string.Empty;
                    if (response.IsSuccessStatusCode)
                    {
                        text = response.Content.ReadAsStringAsync().Result.ToString();
                        data = JObject.Parse(text);

                        if (data != null)
                        {
                            var partyItemsData = data["Items"];
                            if (partyItemsData != null)
                            {

                                var partyValuesData = data["Items"]["$values"];
                                if (partyValuesData != null)
                                {
                                    var partyItemsDataFirstIndex = partyValuesData[0];
                                    JArray partyItemsDataInArray = (JArray)data["Items"]["$values"];
                                    int dataCount = partyItemsDataInArray.Count;
                                    if (dataCount > 0)
                                    {
                                        for (int i = 0; i < dataCount; i++)
                                        {
                                            try
                                            {
                                                PartyModel objParty = new PartyModel();
                                                var singleParty = (JObject)partyItemsDataInArray[i];
                                                var singlePartyType = singleParty["$type"].ToString();
                                                if (singlePartyType != null)
                                                {
                                                    if (singlePartyType.Contains("Asi.Soa.Membership.DataContracts.PersonData, Asi.Contracts"))
                                                    {
                                                        string partyId = "0";
                                                        string partyName = string.Empty;


                                                        object singlePartyID = singleParty["Id"];
                                                        if (singlePartyID != null)
                                                        {
                                                            partyId = singleParty["Id"].ToString();
                                                        }

                                                        object singlePartyName = singleParty["Name"];
                                                        if (singlePartyName != null)
                                                        {
                                                            partyName = singleParty["Name"].ToString();
                                                        }

                                                        objParty.PartyId = partyId;
                                                        objParty.PartyName = partyName;

                                                        string organizationId = "0";
                                                        string organizationName = string.Empty;
                                                        var partyOrganization = singleParty["PrimaryOrganization"];
                                                        if (partyOrganization != null)
                                                        {
                                                            object organizationPartyID = partyOrganization["OrganizationPartyId"];
                                                            if (organizationPartyID != null)
                                                            {
                                                                organizationId = partyOrganization["OrganizationPartyId"].ToString();
                                                            }

                                                            object organizationPartyName = partyOrganization["Name"];
                                                            if (organizationPartyName != null)
                                                            {
                                                                organizationName = partyOrganization["Name"].ToString();
                                                            }

                                                            objParty.OrganizationPartyId = organizationId;
                                                            objParty.OrganizationPartyName = organizationName;
                                                            lstParty.Add(objParty);
                                                        }
                                                        else
                                                        {
                                                            organizationId = "0";
                                                            organizationName = string.Empty;

                                                            objParty.OrganizationPartyId = organizationId;
                                                            objParty.OrganizationPartyName = organizationName;
                                                            lstParty.Add(objParty);
                                                        }
                                                    }
                                                    else if (singlePartyType.Contains("Asi.Soa.Membership.DataContracts.OrganizationData, Asi.Contracts"))
                                                    {
                                                        string organizationId = "0";
                                                        string organizationName = string.Empty;

                                                        object organizationPartyID = singleParty["Id"];
                                                        if (organizationPartyID != null)
                                                        {
                                                            organizationId = singleParty["Id"].ToString();
                                                        }

                                                        object organizationPartyName = singleParty["OrganizationName"];
                                                        if (organizationPartyName != null)
                                                        {
                                                            organizationName = singleParty["OrganizationName"].ToString();
                                                        }

                                                        objParty.OrganizationPartyId = organizationId;
                                                        objParty.OrganizationPartyName = organizationName;
                                                        objParty.PartyId = "0";
                                                        objParty.PartyName = string.Empty;
                                                        lstParty.Add(objParty);
                                                    }
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        string limit = (string)data.SelectToken("Limit");
                        string count = (string)data.SelectToken("Count");
                        string totalCount = (string)data.SelectToken("TotalCount");
                        string cOffset = (string)data.SelectToken("Offset");
                        int nextOffset = Convert.ToInt32(cOffset) + 500;
                        if (Convert.ToInt32(count) == 500)
                        {
                            // Now called for next record
                            offset = nextOffset;
                            List<PartyModel> nextResponseData = GetPartyData(ref dtError, ref lstParty, tokenAPIResponse, offset, basePath);
                        }
                        else
                        {
                            // API Call Completed
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string error = "";
                CreateErrorOfDatatableForImportOrder(ref dtError, "", ex.Message, error);
            }
            return lstParty;
        }

        private string SaveAdvertiserAsParty(ref DataTable dtError, string asiPartApiUrl, string asiPartyAuthToken, string oName, string street1, string street2, string city, string state, string zip, string country, string workPhone)
        {
            string returnValue = "0";
            try
            {
                string asiUserName = ConfigurationManager.AppSettings["ASIusername"].ToString();
                string asiPassword = ConfigurationManager.AppSettings["ASIpassword"].ToString();
                string asiGrantType = ConfigurationManager.AppSettings["ASIgrant_type"].ToString();

                //ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });

                string basePath = asiPartApiUrl.Replace("api/Party", "");

                var client2 = new HttpClient();
                //client2.BaseAddress = new Uri("https://imistest.com/");
                client2.BaseAddress = new Uri(basePath);
                var request = new HttpRequestMessage(HttpMethod.Post, "token");

                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("username", asiUserName));
                keyValues.Add(new KeyValuePair<string, string>("password", asiPassword));
                keyValues.Add(new KeyValuePair<string, string>("grant_type", asiGrantType));

                request.Content = new FormUrlEncodedContent(keyValues);
                var responseMessage = client2.SendAsync(request);
                var responseJSONData = responseMessage.Result.Content.ReadAsStringAsync().Result;
                var responseData = JsonConvert.DeserializeObject(responseJSONData);
                int issuedIndexOf = responseJSONData.LastIndexOf(".issued");
                int expiresIndexOf = responseJSONData.LastIndexOf(".expires");

                var textAfterissuedIndexOf = responseJSONData.Substring(issuedIndexOf);
                //var textAfterexpiresIndexOf = responseJSONData.Substring(expiresIndexOf);

                var textAfterReplaceissuedIndexOf = textAfterissuedIndexOf.Replace(".issued", "issued").Replace(".expires", "expires");
                //var textAfterReplaceexpiresIndexOf = textAfterexpiresIndexOf.Replace(".expires", "expires");
                var responseJSONDataAfterChanged = responseJSONData.Replace(textAfterissuedIndexOf, textAfterReplaceissuedIndexOf);
                TokenAPIResponse tokenAPIResponse = JsonConvert.DeserializeObject<TokenAPIResponse>(responseJSONDataAfterChanged);

                if (!string.IsNullOrEmpty(tokenAPIResponse.Access_token))
                {
                    PostAdvertiserModel postAdvertiser = new PostAdvertiserModel();
                    postAdvertiser.SomeVars1 = "Asi.Soa.Membership.DataContracts.OrganizationData, Asi.Contracts";
                    postAdvertiser.OrganizationName = oName;

                    // FOCCUSS47 CODE STARTS

                    #region Address
                    //Address
                    Addresses objAddressChild = new Addresses();
                    List<FullAddressData> lstFAddData = new List<FullAddressData>();
                    FullAddressData objFAddData = new FullAddressData();
                    AddressData objAddressData = new AddressData();
                    AddressLines objALines = new AddressLines();
                    CommunicationPreferencesData objCData = new CommunicationPreferencesData();
                    List<CommunicationPreferencesDetails> lstCData = new List<CommunicationPreferencesDetails>();
                    CommunicationPreferencesDetails details = new CommunicationPreferencesDetails();

                    List<AddressLines> lstLines = new List<AddressLines>();
                    List<string> lstString = new List<string>();
                    postAdvertiser.Addresses = objAddressChild;
                    objAddressChild.CustomVariable = "Asi.Soa.Membership.DataContracts.FullAddressDataCollection, Asi.Contracts";
                    postAdvertiser.Addresses.FullAddressdata = lstFAddData;

                    objFAddData.CustomFullAddressDataVariable = "Asi.Soa.Membership.DataContracts.FullAddressData, Asi.Contracts";
                    objFAddData.Address = objAddressData;
                    lstFAddData.Add(objFAddData);


                    objAddressData.CustomAddressDataVariable = "Asi.Soa.Membership.DataContracts.AddressData, Asi.Contracts";
                    objAddressData.CityName = city;
                    objAddressData.CountryName = country;
                    objAddressData.CountrySubEntityName = state;
                    objAddressData.AddressLines = objALines;
                    objAddressData.CountryCode = "";
                    objAddressData.CountrySubEntityCode = "";
                    objAddressData.FullAddress = street1 + " " + street2 + " " + city + " " + state + " " + country;
                    objAddressData.PostalCode = zip;

                    objFAddData.AddresseeText = oName + " " + street1 + " " + city + " " + state + " " + zip;
                    objFAddData.AddressPurpose = "Address";


                    objFAddData.Phone = workPhone;


                    objALines.CustomAddressLinesVariable = "Asi.Soa.Membership.DataContracts.AddressLineDataCollection, Asi.Contracts";
                    objALines.AddLines = lstString;
                    lstLines.Add(objALines);
                    if (street1.Length > 40)
                    {
                        lstString.Add(street1.Substring(0, 39));
                    }
                    else
                    {
                        lstString.Add(street1);
                    }
                    if (street2.Length > 40)
                    {
                        lstString.Add(street2.Substring(0, 39));
                    }
                    else
                    {
                        lstString.Add(street2);
                    }

                    objCData.CustomCommunicationVariable = "Asi.Soa.Membership.DataContracts.CommunicationPreferenceDataCollection, Asi.Contracts";


                    details.CustomCommunicationPreferenceDataVariable = "Asi.Soa.Membership.DataContracts.CommunicationPreferenceData, Asi.Contracts";
                    details.Reason = "bill";
                    lstCData.Add(details);

                    details = new CommunicationPreferencesDetails();
                    details.CustomCommunicationPreferenceDataVariable = "Asi.Soa.Membership.DataContracts.CommunicationPreferenceData, Asi.Contracts";
                    details.Reason = "ship";
                    lstCData.Add(details);

                    objCData.CommunicationPreferencesDetails = lstCData;
                    objFAddData.CommunicationPreferences = objCData;
                    #endregion Address

                    // CODE ENDS

                    string json = JsonConvert.SerializeObject(postAdvertiser);
                    json = json.Replace("SomeVars1", "$type");
                    // FOCCUSS47 CODE STARTS
                    json = json.Replace("CustomVariable", "$type");
                    json = json.Replace("AddLines", "$values");

                    json = json.Replace("CustomCommunicationVariable", "$type");
                    json = json.Replace("CommunicationPreferencesDetails", "$values");
                    json = json.Replace("CustomCommunicationPreferenceDataVariable", "$type");
                    json = json.Replace("FullAddressdata", "$values");
                    json = json.Replace("CustomFullAddressDataVariable", "$type");
                    json = json.Replace("CustomAddressDataVariable", "$type");
                    json = json.Replace("CustomAddressLinesVariable", "$type");
                    json = json.Replace("SomeVar", "$type");
                    // CODE ENDS
                    string postData = json;
                    var buffer = System.Text.Encoding.UTF8.GetBytes(postData);
                    var byteContent = new ByteArrayContent(buffer);
                    byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    //byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                    HttpClient client = new HttpClient();
                    //asiPartApiUrl = "https://imistest.com/";
                    //client.BaseAddress = new Uri(asiPartApiUrl);

                    client.BaseAddress = new Uri(basePath);
                    client.DefaultRequestHeaders.Accept.Add(
                       new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenAPIResponse.Access_token);
                    //var stringContent = new StringContent(postData, Encoding.UTF8, "application/json");
                    var response = client.PostAsync("api/Party", byteContent).Result;
                    var text = response.Content.ReadAsStringAsync().Result.ToString();
                    dynamic data = JObject.Parse(text);
                    string displayName = (string)data.SelectToken("OrganizationName");
                    string reponseId = (string)data.SelectToken("Id");
                    string partyId = (string)data.SelectToken("PartyId");

                    returnValue = partyId;
                }
            }

            catch (Exception ex)
            {
                string error = "";
                CreateErrorOfDatatableForImportOrder(ref dtError, "", ex.Message, error);
            }
            return returnValue;
        }

        private string SaveBillToContactAsParty(ref DataTable dtError, string asiPartApiUrl, string asiPartyAuthToken, string oName, string email, string mobile, string workP, string street1, string street2, string city, string state, string zip, string country, string orgId)
        {
            string returnValue = "0";
            try
            {
                string asiUserName = ConfigurationManager.AppSettings["ASIusername"].ToString();
                string asiPassword = ConfigurationManager.AppSettings["ASIpassword"].ToString();
                string asiGrantType = ConfigurationManager.AppSettings["ASIgrant_type"].ToString();

                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });

                string basePath = asiPartApiUrl.Replace("api/Party", "");

                var client2 = new HttpClient();
                client2.BaseAddress = new Uri(basePath);
                var request = new HttpRequestMessage(HttpMethod.Post, "token");

                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("username", asiUserName));
                keyValues.Add(new KeyValuePair<string, string>("password", asiPassword));
                keyValues.Add(new KeyValuePair<string, string>("grant_type", asiGrantType));

                request.Content = new FormUrlEncodedContent(keyValues);
                var responseMessage = client2.SendAsync(request);
                var responseJSONData = responseMessage.Result.Content.ReadAsStringAsync().Result;
                var responseData = JsonConvert.DeserializeObject(responseJSONData);
                int issuedIndexOf = responseJSONData.LastIndexOf(".issued");
                int expiresIndexOf = responseJSONData.LastIndexOf(".expires");

                var textAfterissuedIndexOf = responseJSONData.Substring(issuedIndexOf);

                var textAfterReplaceissuedIndexOf = textAfterissuedIndexOf.Replace(".issued", "issued").Replace(".expires", "expires");
                var responseJSONDataAfterChanged = responseJSONData.Replace(textAfterissuedIndexOf, textAfterReplaceissuedIndexOf);
                TokenAPIResponse tokenAPIResponse = JsonConvert.DeserializeObject<TokenAPIResponse>(responseJSONDataAfterChanged);

                if (!string.IsNullOrEmpty(tokenAPIResponse.Access_token))
                {
                    string firstName = string.Empty;
                    string lastName = string.Empty;
                    string middleName = string.Empty;

                    string[] names = oName.Split(' ');
                    if (names.Length == 1)
                    {
                        firstName = names[0];
                    }
                    else if (names.Length == 2)
                    {
                        firstName = names[0];
                        lastName = names[1];
                    }
                    else
                    {
                        firstName = names[0];
                        middleName = names[1];
                        int index = 0;
                        string tmpName = "";
                        foreach (var item in names)
                        {
                            if (index > 1)
                            {
                                tmpName += " " + item;
                            }
                            index++;
                        }
                        lastName = tmpName;
                    }

                    // firstName and lastName validation need to put always for validation
                    PersonName objChild = new PersonName();
                    PrimaryOrganization objOrgChild = new PrimaryOrganization();


                    // FOCUS47 CODE BEGIN

                    List<EmailData> objEmailData = new List<EmailData>();
                    EmailData objData = new EmailData();
                    // FOCUS47 CODE END 

                    PostAdvertiserModel obj = new PostAdvertiserModel();
                    obj.PersonName = objChild;
                    obj.PrimaryOrganization = objOrgChild;


                    obj.SomeVars1 = "Asi.Soa.Membership.DataContracts.PersonData, Asi.Contracts";
                    obj.OrganizationName = oName;

                    obj.PersonName.CustomTypeVariable = "Asi.Soa.Membership.DataContracts.PersonNameData, Asi.Soa.Membership.Contracts";
                    obj.PersonName.FirstName = firstName;
                    obj.PersonName.MiddleName = middleName;
                    obj.PersonName.LastName = lastName;

                    obj.PrimaryOrganization.CustomOrgTypeVariable = "Asi.Soa.Membership.DataContracts.PrimaryOrganizationInformationData, Asi.Contracts";
                    obj.PrimaryOrganization.OrganizationPartyId = orgId.ToString();

                    #region Email FOCUS47 CODE BEGIN
                    obj.Emails = new Emails();
                    obj.Emails.CustomEmailCVariable = "Asi.Soa.Membership.DataContracts.EmailDataCollection, Asi.Contracts";
                    obj.Emails.EmailDetail = objEmailData;

                    objData.CustomEmailTypevariable = "Asi.Soa.Membership.DataContracts.EmailData, Asi.Contracts";
                    objData.Address = email;
                    objData.EmailType = "_Primary";
                    objData.IsPrimary = true;
                    objEmailData.Add(objData);

                    #endregion Email

                    #region Phone

                    List<PhoneData> lstPhoneData = new List<PhoneData>();
                    PhoneData objPData = new PhoneData();

                    obj.Phones = new Phones();
                    obj.Phones.SomeVar = "Asi.Soa.Membership.DataContracts.PhoneDataCollection, Asi.Contracts";
                    obj.Phones.PhoneDetail = lstPhoneData;

                    objPData.CustomPhoneTypevariable = "Asi.Soa.Membership.DataContracts.PhoneData, Asi.Contracts";
                    objPData.Number = mobile;
                    objPData.PhoneType = "Mobile";
                    lstPhoneData.Add(objPData);

                    objPData = new PhoneData();
                    objPData.CustomPhoneTypevariable = "Asi.Soa.Membership.DataContracts.PhoneData, Asi.Contracts";
                    objPData.Number = workP;
                    objPData.PhoneType = "_Work Phone";
                    lstPhoneData.Add(objPData);

                    #endregion Phone

                    string json = JsonConvert.SerializeObject(obj);
                    json = json.Replace("SomeVars1", "$type");
                    json = json.Replace("CustomTypeVariable", "$type");
                    json = json.Replace("CustomOrgTypeVariable", "$type");
                    json = json.Replace("CustomEmailTypevariable", "$type");
                    json = json.Replace("EmailDetail", "$values");
                    json = json.Replace("CustomPhoneTypevariable", "$type");
                    json = json.Replace("PhoneDetail", "$values");
                    json = json.Replace("SomeVar", "$type");
                    json = json.Replace("CustomEmailCVariable", "$type");
                    string postData = json;
                    var buffer = System.Text.Encoding.UTF8.GetBytes(postData);
                    var byteContent = new ByteArrayContent(buffer);
                    byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                    HttpClient client = new HttpClient
                    {
                        BaseAddress = new Uri(basePath)
                    };
                    client.DefaultRequestHeaders.Accept.Add(
                       new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenAPIResponse.Access_token);
                    //var stringContent = new StringContent(postData, Encoding.UTF8, "application/json");
                    var response = client.PostAsync("api/Party", byteContent).Result;
                    var text = response.Content.ReadAsStringAsync().Result.ToString();
                    dynamic data = JObject.Parse(text);
                    string displayName = (string)data.SelectToken("Name");
                    string responseId = (string)data.SelectToken("Id");
                    string partyId = (string)data.SelectToken("PartyId");

                    returnValue = partyId;
                }
            }
            catch (Exception ex)
            {
                string error = "";
                CreateErrorOfDatatableForImportOrder(ref dtError, "", ex.Message, error);
            }
            return returnValue;
        }

        private MediaAsset GetAllDatabyMediaAsset(ref DataTable dtError, string mediaAssetName)
        {
            MediaAsset responseData = null;
            try
            {
                if (!string.IsNullOrEmpty(mediaAssetName))
                {
                    MediaAsset objDbMediaAssets = DB.MediaAssets.Where(x => x.MediaAssetName.Trim().ToLower() == mediaAssetName.Trim().ToLower()).FirstOrDefault();
                    if (objDbMediaAssets != null)
                    {
                        responseData = objDbMediaAssets;
                    }
                    else
                    {
                        responseData = new MediaAsset();
                        string cError = "Media Asset Name is not Present in our system";
                        CreateErrorOfDatatableForImportOrder(ref dtError, "Publication", "", cError);
                    }
                }
            }
            catch (Exception ex)
            {
                string error = "";
                CreateErrorOfDatatableForImportOrder(ref dtError, "Publication", ex.Message.ToString(), error);
            }
            return responseData;
        }

        private ResponseCommon ValidationForOrderDetailsAndMediaScheduleTab(ref DataTable dtError, MediaAsset mResponseData, DataRow excelData, List<PartyModel> lstAsiPart)
        {
            bool isError = false;
            ResponseCommon objResponse = new ResponseCommon();
            ResponseCommon addOrderResponse = new ResponseCommon();
            int mediaOrderId = 0;
            try
            {
                bool isValid = true;
                bool isRepsAndTerritoryValid = true;
                // PostMediaOrderModel objPostMOM = new PostMediaOrderModel();
                PostImportMediaOrderModel objPostMOM = new PostImportMediaOrderModel();

                if (mResponseData != null)
                {
                    string pageHistoryId = excelData["Page History ID"].ToString();
                    string contractNum = excelData["ContractNum"].ToString();
                    string advertiserName = excelData["Account Name"].ToString();
                    string agencyName = excelData["Agency Name"].ToString();
                    string billToContactName = excelData["BillName"].ToString();
                    string advertiserId = excelData["AdvertiserId"].ToString();
                    string agencyId = excelData["AgencyId"].ToString();
                    string billToContactId = excelData["BillToContactId"].ToString();
                    string billTo = excelData["Bill"].ToString();
                    string rep1 = excelData["Rep1"].ToString();
                    string territory1 = excelData["Territory1"].ToString();
                    string rep2 = excelData["Rep2"].ToString();
                    string territory2 = excelData["Territory2"].ToString();
                    string rep3 = excelData["Rep3"].ToString();
                    string territory3 = excelData["Territory3"].ToString();
                    string rep4 = excelData["Rep4"].ToString();
                    string territory4 = excelData["Territory4"].ToString();
                    string split1 = excelData["Split1"].ToString();
                    string split2 = excelData["Split2"].ToString();
                    string split3 = excelData["Split3"].ToString();
                    string split4 = excelData["Split4"].ToString();
                    string comm1 = excelData["Comm1"].ToString();
                    string comm2 = excelData["Comm2"].ToString();
                    string comm3 = excelData["Comm3"].ToString();
                    string comm4 = excelData["Comm4"].ToString();

                    string columns = excelData["Columns"].ToString();
                    string inches = excelData["Inches"].ToString();
                    string rateCardNum = excelData["Rate Card Num"].ToString();
                    string adTypeName = excelData["TypeAd"].ToString();
                    string flightStartDate = excelData["Start Date"].ToString();
                    string flightEndDate = excelData["End Date"].ToString();
                    string issueDate = excelData["Issue Date"].ToString();
                    string adSizeName = excelData["Ad Size"].ToString();
                    string adColorName = excelData["Color"].ToString();
                    string baseRateCardCost = excelData["RC Ad Cost"].ToString();
                    string adCost = excelData["Ad Cost"].ToString();
                    string netCost = excelData["Net Cost"].ToString();
                    string grossCost = excelData["Gross Cost"].ToString();
                    string finalBillCost = excelData["Bill Cost"].ToString();
                    string rate = excelData["Rate"].ToString();
                    string units = excelData["IMPRESSIONS"].ToString();
                    string classifiedText = excelData["ClassifiedText"].ToString();

                    string createdBy = excelData["_User"].ToString();
                    string createdDate = excelData["_Date"].ToString();
                    string createdTime = excelData["_Time"].ToString();

                    string buyId = excelData["BuyId"].ToString();
                    int mId = 0;
                    if (!string.IsNullOrEmpty(buyId) && buyId != "0" && buyId != "0.00")
                    {
                        mId = Convert.ToInt32(excelData["BuyId"]);

                    }
                    else
                    {
                        mId = Convert.ToInt32(GetGenerateBuyId());
                        excelData["BuyId"] = mId;
                    }



                    objPostMOM.BuyId = mId;
                    objPostMOM.MediaAssetId = mResponseData.MediaAssetId;
                    objPostMOM.ImportBuyId = Convert.ToInt32(contractNum);
                    objPostMOM.ImportMediaOrderId = Convert.ToInt32(pageHistoryId);
                    #region  object for OrderDetails(step = 1)
                    if (!string.IsNullOrEmpty(advertiserId))
                    {
                        objPostMOM.AdvertiserId = advertiserId;
                    }
                    if (!string.IsNullOrEmpty(agencyId))
                    {
                        objPostMOM.AgencyId = agencyId;
                    }

                    if (!string.IsNullOrEmpty(createdTime))
                    {
                        DateTime? timeTo = string.IsNullOrEmpty(createdTime.ToString()) ? (DateTime?)null : DateTime.Parse(createdTime.ToString());
                        DateTime createdDt = DateTime.ParseExact(createdDate + " " + timeTo, "dd/MM/yy h:mm:ss tt", CultureInfo.InvariantCulture);
                        objPostMOM.CreatedBy = createdBy;
                        objPostMOM.CreatedDate = Convert.ToDateTime(createdDt);
                        objPostMOM.CreatedTime = createdTime;
                    }
                    else
                    {
                        objPostMOM.CreatedBy = createdBy;
                        objPostMOM.CreatedDate = Convert.ToDateTime(createdDate);
                        objPostMOM.CreatedTime = createdTime;
                    }


                    #region check BillToContact is Exits into Advertiser Or Agency
                    if (billTo == "0")
                    {
                        objPostMOM.BillToInd = true;
                        objPostMOM.BT_ID = agencyId;

                        List<PartyModel> lstAsiPartyAsPerson = (from s in lstAsiPart
                                                                where (s.OrganizationPartyName ?? "").Trim().ToLower() == agencyName.Trim().ToLower()
                                                                select new PartyModel
                                                                {
                                                                    OrganizationPartyId = s.OrganizationPartyId,
                                                                    OrganizationPartyName = s.OrganizationPartyName,
                                                                    PartyId = s.PartyId,
                                                                    PartyName = s.PartyName,
                                                                }).ToList();

                        if (lstAsiPartyAsPerson.Count > 0)
                        {
                            // var isPersonExits = lstAsiPartyAsPerson.Where(y => y.PartyId == BillToContactId).ToList();
                            var isPersonExists = (from s in lstAsiPartyAsPerson
                                                  where (s.PartyId ?? "").Trim().ToLower() == billToContactId.Trim().ToLower()
                                                  select new PartyModel
                                                  {
                                                      OrganizationPartyId = s.OrganizationPartyId,
                                                      OrganizationPartyName = s.OrganizationPartyName,
                                                      PartyId = s.PartyId,
                                                      PartyName = s.PartyName,
                                                  }).ToList();



                            if (isPersonExists.Count <= 0)
                            {
                                string cError = "Bill To Contact is not present to this Agency";
                                CreateErrorOfDatatableForImportOrder(ref dtError, "BillName", "", cError);
                                isValid = false;
                            }
                            else
                            {
                                objPostMOM.ST_ID = billToContactId;
                            }
                        }
                    }
                    else if (billTo == "-1")
                    {
                        objPostMOM.BillToInd = false;
                        objPostMOM.BT_ID = advertiserId;

                        List<PartyModel> lstAsiPartyAsPerson = (from s in lstAsiPart
                                                                where (s.OrganizationPartyName ?? "").Trim().ToLower() == advertiserName.Trim().ToLower()
                                                                select new PartyModel
                                                                {
                                                                    OrganizationPartyId = s.OrganizationPartyId,
                                                                    OrganizationPartyName = s.OrganizationPartyName,
                                                                    PartyId = s.PartyId,
                                                                    PartyName = s.PartyName,
                                                                }).ToList();



                        if (lstAsiPartyAsPerson.Count > 0)
                        {
                            // var isPersonExits = lstAsiPartyAsPerson.Where(y => y.PartyId == BillToContactId).ToList();
                            var isPersonExits = (from s in lstAsiPartyAsPerson
                                                 where (s.PartyId ?? "").Trim().ToLower() == billToContactId.Trim().ToLower()
                                                 select new PartyModel
                                                 {
                                                     OrganizationPartyId = s.OrganizationPartyId,
                                                     OrganizationPartyName = s.OrganizationPartyName,
                                                     PartyId = s.PartyId,
                                                     PartyName = s.PartyName,
                                                 }).ToList();

                            //var isPersonExits = (from s in lstAsiPartyAsPerson
                            //                     where s.PartyId.Trim() == BillToContactId.Trim()
                            //                     select new PartyModel
                            //                     {
                            //                         OrganizationPartyId = s.OrganizationPartyId,
                            //                         OrganizationPartyName = s.OrganizationPartyName,
                            //                         PartyId = s.PartyId,
                            //                         PartyName = s.PartyName,
                            //                     }).ToList();

                            if (isPersonExits.Count <= 0)
                            {
                                string cError = "Bill To Contact is not present to this Advertiser";
                                CreateErrorOfDatatableForImportOrder(ref dtError, "BillName", "", cError);
                                isValid = false;
                            }
                            else
                            {
                                objPostMOM.ST_ID = billToContactId;
                                // objPostMOM.ST_ID = "23230";
                            }
                        }
                    }
                    #endregion

                    bool isSpiltChecked = false;
                    List<PostMediaOrderRepsModel> lstRepsComm = new List<PostMediaOrderRepsModel>();

                    #region Rep1 and Territory1 Wise Commission

                    string notUsed = "N/A";
                    if (!string.IsNullOrEmpty(rep1))
                    {
                        if (!string.IsNullOrEmpty(territory1))
                        {
                            if (!string.IsNullOrEmpty(comm1))
                            {
                                var repExits = DB.Reps.Where(y => y.RepName.Trim().ToLower() == rep1.Trim().ToLower()).ToList();
                                //Temp Code Starts
                                bool isError1 = false;
                                int repId = 0;
                                if (repExits.Count == 0)
                                {
                                    Rep newRep = new Rep();
                                    newRep.RepName = rep1.Trim();
                                    newRep.CreatedOn = DateTime.Now;
                                    //newRep.DeletedInd = false;

                                    //*
                                    newRep.UpdatedOn = DateTime.Now;
                                    newRep.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                                    newRep.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                    DB.Reps.Add(newRep);
                                    DB.SaveChanges();
                                    if (newRep.RepId == 0)
                                    {
                                        isError1 = true;
                                    }
                                    else
                                    {
                                        repId = newRep.RepId; ;
                                    }
                                }
                                else
                                {
                                    repId = repExits.Select(x => x.RepId).FirstOrDefault();
                                }

                                if (!isError1)
                                {
                                    var territoryExists = DB.Territories.Where(y => y.TerritoryName.Trim().ToLower() == territory1.Trim().ToLower()).ToList();
                                    //Temp Code Starts
                                    int territoryId = 0;
                                    if (territoryExists.Count == 0)
                                    {
                                        Territory newTerritory = new Territory();
                                        newTerritory.TerritoryName = territory1.Trim();
                                        newTerritory.CreatedOn = DateTime.Now;
                                        //newTerritory.DeletedInd = false;

                                        //*
                                        newTerritory.UpdatedOn = DateTime.Now;
                                        newTerritory.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                                        newTerritory.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                        DB.Territories.Add(newTerritory);
                                        DB.SaveChanges();
                                        if (newTerritory.TerritoryId == 0)
                                        {
                                            isError1 = true;
                                        }
                                        else
                                        {
                                            territoryId = newTerritory.TerritoryId; ;
                                        }
                                    }
                                    else
                                    {
                                        territoryId = territoryExists.Select(x => x.TerritoryId).FirstOrDefault();
                                    }

                                    if (!isError1)
                                    {
                                        float commission = float.Parse(comm1, CultureInfo.InvariantCulture.NumberFormat);
                                        var isRepAndTerritoryExits = DB.RepTerritories.AsEnumerable().Where(x => (x.RepId == repId
                                        && x.TerritoryId == territoryId
                                        /*&& x.DeletedInd != true*/)).FirstOrDefault();

                                        if (isRepAndTerritoryExits == null)
                                        {
                                            //Insert
                                            isRepAndTerritoryExits = new RepTerritory();
                                            isRepAndTerritoryExits.RepId = repId;
                                            isRepAndTerritoryExits.TerritoryId = territoryId;
                                            isRepAndTerritoryExits.Commission = double.Parse(commission.ToString());
                                            isRepAndTerritoryExits.CreatedOn = DateTime.Now;
                                            //isRepAndTerritoryExits.DeletedInd = false;

                                            //*
                                            isRepAndTerritoryExits.UpdatedOn = DateTime.Now;
                                            isRepAndTerritoryExits.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                                            isRepAndTerritoryExits.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                            DB.RepTerritories.Add(isRepAndTerritoryExits);
                                            DB.SaveChanges();
                                            if (isRepAndTerritoryExits.RepTerritoryId == 0)
                                            {
                                                isError1 = true;
                                            }
                                        }
                                        else if (isRepAndTerritoryExits.Commission.ToString() != commission.ToString())
                                        {
                                            //Update or throw error
                                            isRepAndTerritoryExits.Commission = commission;

                                            //*
                                            isRepAndTerritoryExits.UpdatedOn = DateTime.Now;
                                            isRepAndTerritoryExits.UpdatedByUserKey = Constants.Created_Update_ByUserKey;
                                            DB.SaveChanges();
                                        }

                                        if (!isError1)
                                        {

                                            if (!string.IsNullOrEmpty(split1) || !string.IsNullOrEmpty(split2) || !string.IsNullOrEmpty(split3) || !string.IsNullOrEmpty(split4))
                                            {
                                                decimal sp1 = 0;
                                                decimal sp2 = 0;
                                                decimal sp3 = 0;
                                                decimal sp4 = 0;
                                                if (!string.IsNullOrEmpty(split1))
                                                {
                                                    sp1 = Convert.ToDecimal(split1) * 100;
                                                }
                                                if (!string.IsNullOrEmpty(split2))
                                                {
                                                    sp2 = Convert.ToDecimal(split2) * 100;
                                                }
                                                if (!string.IsNullOrEmpty(split3))
                                                {
                                                    sp3 = Convert.ToDecimal(split3) * 100;
                                                }
                                                if (!string.IsNullOrEmpty(split4))
                                                {
                                                    sp4 = Convert.ToDecimal(split4) * 100;
                                                }

                                                var totalSpilt = Convert.ToDecimal(sp1 + sp2 + sp3 + sp4);
                                                //var totalSpilt = Convert.ToDecimal(sp1);
                                                if (totalSpilt > 100)
                                                {
                                                    string cError = "spilt sum is equal to 100";
                                                    CreateErrorOfDatatableForImportOrder(ref dtError, "Split1,Split2,Split3,Split4", "", cError);
                                                    isRepsAndTerritoryValid = false;
                                                }
                                                else if (totalSpilt < 100)
                                                {
                                                    string cError = "spilt sum is equal to 100";
                                                    CreateErrorOfDatatableForImportOrder(ref dtError, "Split1,Split2,Split3,Split4", "", cError);
                                                    isRepsAndTerritoryValid = false;
                                                }
                                                else if (totalSpilt == 100)
                                                {
                                                    if (Convert.ToDecimal(split1) <= 100)
                                                    {
                                                        PostMediaOrderRepsModel objRepsComm = new PostMediaOrderRepsModel();
                                                        objRepsComm.RepId = repId;
                                                        objRepsComm.TerritoryId = territoryId;
                                                        objRepsComm.Split = Convert.ToDouble(split1) * 100;
                                                        objRepsComm.Commission = double.Parse(commission.ToString());
                                                        lstRepsComm.Add(objRepsComm);
                                                    }
                                                }
                                                if (sp1 == 100)
                                                {
                                                    isSpiltChecked = true;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            string cError = "reps and territory wise commission is not exits database";
                                            CreateErrorOfDatatableForImportOrder(ref dtError, "validation Error", "", cError);
                                            isRepsAndTerritoryValid = false;
                                        }
                                    }
                                    else
                                    {
                                        string cError = "Territory1 is not exits database";
                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Territory1", "", cError);
                                        isRepsAndTerritoryValid = false;
                                    }
                                }
                                else
                                {
                                    string cError = "Rep1 is not exits databse";
                                    CreateErrorOfDatatableForImportOrder(ref dtError, "Rep1", "", cError);
                                    isRepsAndTerritoryValid = false;
                                }

                                // var repsTerritoryExits = db.RepTerritories.Where(x=>x.RepId)
                            }
                            else
                            {
                                //string cError = "Comm1 is blank";
                                //createErrorOfDatatableForImportOrder(ref dtError, "Comm1", "", cError);
                                isRepsAndTerritoryValid = false;
                            }
                        }
                        else
                        {
                            //string cError = "Territory1 is blank";
                            //createErrorOfDatatableForImportOrder(ref dtError, "Territory1", "", cError);
                            isRepsAndTerritoryValid = false;
                        }
                    }
                    else
                    {
                        //string cError = "Rep1 is blank";
                        //createErrorOfDatatableForImportOrder(ref dtError, "Rep1", "", cError);
                        isRepsAndTerritoryValid = false;
                    }
                    #endregion

                    #region Rep2 and Territory2 Wise Commission
                    if (!string.IsNullOrEmpty(rep2))
                    {
                        if (!string.IsNullOrEmpty(territory2))
                        {
                            if (!string.IsNullOrEmpty(comm2))
                            {
                                var repExits = DB.Reps.Where(y => y.RepName.Trim().ToLower() == rep2.Trim().ToLower()).ToList();
                                //Temp Code Starts
                                bool isError2 = false;
                                int repId = 0;
                                if (repExits.Count == 0)
                                {
                                    Rep newRep = new Rep();
                                    newRep.RepName = rep2.Trim();
                                    newRep.CreatedOn = DateTime.Now;
                                    //newRep.DeletedInd = false;

                                    //*
                                    newRep.UpdatedOn = DateTime.Now;
                                    newRep.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                                    newRep.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                    DB.Reps.Add(newRep);
                                    DB.SaveChanges();
                                    if (newRep.RepId == 0)
                                    {
                                        isError2 = true;
                                    }
                                    else
                                    {
                                        repId = newRep.RepId; ;
                                    }
                                }
                                else
                                {
                                    repId = repExits.Select(x => x.RepId).FirstOrDefault();
                                }


                                if (!isError2)
                                {
                                    var territoryExits = DB.Territories.Where(y => y.TerritoryName.Trim().ToLower() == territory2.Trim().ToLower()).ToList();
                                    //Temp Code Starts
                                    int territoryId = 0;
                                    if (territoryExits.Count == 0)
                                    {
                                        Territory newTerritory = new Territory();
                                        newTerritory.TerritoryName = territory2.Trim();
                                        newTerritory.CreatedOn = DateTime.Now;
                                        //newTerritory.DeletedInd = false;

                                        //*
                                        newTerritory.UpdatedOn = DateTime.Now;
                                        newTerritory.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                                        newTerritory.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                        DB.Territories.Add(newTerritory);
                                        DB.SaveChanges();
                                        if (newTerritory.TerritoryId == 0)
                                        {
                                            isError2 = true;
                                        }
                                        else
                                        {
                                            territoryId = newTerritory.TerritoryId; ;
                                        }
                                    }
                                    else
                                    {
                                        territoryId = territoryExits.Select(x => x.TerritoryId).FirstOrDefault();
                                    }

                                    if (!isError2)
                                    {
                                        float commission = float.Parse(comm2, CultureInfo.InvariantCulture.NumberFormat);
                                        var isRepAndTerritoryExits = DB.RepTerritories.AsEnumerable().Where(x => (x.RepId == repId && x.TerritoryId == territoryId
                                        /*&& x.DeletedInd != true*/)).FirstOrDefault();
                                        if (isRepAndTerritoryExits == null)
                                        {
                                            //Insert
                                            isRepAndTerritoryExits = new RepTerritory();
                                            isRepAndTerritoryExits.RepId = repId;
                                            isRepAndTerritoryExits.TerritoryId = territoryId;
                                            isRepAndTerritoryExits.Commission = double.Parse(commission.ToString());
                                            isRepAndTerritoryExits.CreatedOn = DateTime.Now;
                                            //isRepAndTerritoryExits.DeletedInd = false;

                                            //*
                                            isRepAndTerritoryExits.UpdatedOn = DateTime.Now;
                                            isRepAndTerritoryExits.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                                            isRepAndTerritoryExits.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                            DB.RepTerritories.Add(isRepAndTerritoryExits);
                                            DB.SaveChanges();
                                            if (isRepAndTerritoryExits.RepTerritoryId == 0)
                                            {
                                                isError2 = true;
                                            }
                                        }
                                        else if (isRepAndTerritoryExits.Commission.ToString() != commission.ToString())
                                        {
                                            //Update or throw error
                                            isRepAndTerritoryExits.Commission = double.Parse(commission.ToString());

                                            //*
                                            isRepAndTerritoryExits.UpdatedOn = DateTime.Now;
                                            isRepAndTerritoryExits.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                            DB.SaveChanges();
                                        }

                                        if (!isError2)
                                        {
                                            if (isSpiltChecked == false)
                                            {
                                                if (!string.IsNullOrEmpty(split1) || !string.IsNullOrEmpty(split2) || !string.IsNullOrEmpty(split3) || !string.IsNullOrEmpty(split4))
                                                {
                                                    decimal sp1 = 0;
                                                    decimal sp2 = 0;
                                                    decimal sp3 = 0;
                                                    decimal sp4 = 0;
                                                    if (!string.IsNullOrEmpty(split1))
                                                    {
                                                        sp1 = Convert.ToDecimal(split1) * 100;
                                                    }
                                                    if (!string.IsNullOrEmpty(split2))
                                                    {
                                                        sp2 = Convert.ToDecimal(split2) * 100;
                                                    }
                                                    if (!string.IsNullOrEmpty(split3))
                                                    {
                                                        sp3 = Convert.ToDecimal(split3) * 100;
                                                    }
                                                    if (!string.IsNullOrEmpty(split4))
                                                    {
                                                        sp4 = Convert.ToDecimal(split4) * 100;
                                                    }

                                                    var totalSpilt = Convert.ToDecimal(sp1 + sp2 + sp3 + sp4);
                                                    if (totalSpilt > 100)
                                                    {
                                                        string cError = "spilt sum is equal to 100";
                                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Split1,Split2,Split3,Split4", "", cError);
                                                        isRepsAndTerritoryValid = false;
                                                    }
                                                    else if (totalSpilt < 100)
                                                    {
                                                        string cError = "spilt sum is equal to 100";
                                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Split1,Split2,Split3,Split4", "", cError);
                                                        isRepsAndTerritoryValid = false;
                                                    }
                                                    else if (totalSpilt == 100)
                                                    {
                                                        if (Convert.ToDecimal(split1) + Convert.ToDecimal(split2) <= 100)
                                                        {
                                                            PostMediaOrderRepsModel objRepsComm = new PostMediaOrderRepsModel();
                                                            objRepsComm.RepId = repId;
                                                            objRepsComm.TerritoryId = territoryId;
                                                            objRepsComm.Split = Convert.ToDouble(split2) * 100;
                                                            objRepsComm.Commission = double.Parse(commission.ToString());
                                                            lstRepsComm.Add(objRepsComm);
                                                        }
                                                    }
                                                    if (sp1 + sp2 == 100)
                                                    {
                                                        isSpiltChecked = true;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            string cError = "reps and territory wise commission is not exits database";
                                            CreateErrorOfDatatableForImportOrder(ref dtError, "validation Error", "", cError);
                                            isRepsAndTerritoryValid = false;
                                        }
                                    }
                                    else
                                    {
                                        string cError = "Territory2 is not exits database";
                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Territory2", "", cError);
                                        isRepsAndTerritoryValid = false;
                                    }
                                }
                                else
                                {
                                    string cError = "Rep2 is not exits databse";
                                    CreateErrorOfDatatableForImportOrder(ref dtError, "Rep2", "", cError);
                                    isRepsAndTerritoryValid = false;
                                }

                                // var repsTerritoryExits = db.RepTerritories.Where(x=>x.RepId)
                            }
                            else
                            {
                                //string cError = "Comm2 is blank";
                                //createErrorOfDatatableForImportOrder(ref dtError, "Comm2", "", cError);
                                isRepsAndTerritoryValid = false;
                            }
                        }
                        else
                        {
                            //string cError = "Territory2 is blank";
                            //createErrorOfDatatableForImportOrder(ref dtError, "Territory2", "", cError);
                            isRepsAndTerritoryValid = false;
                        }
                    }
                    else
                    {
                        //string cError = "Rep2 is blank";
                        //createErrorOfDatatableForImportOrder(ref dtError, "Rep2", "", cError);
                        isRepsAndTerritoryValid = false;
                    }
                    #endregion

                    #region Rep3 and Territory3 Wise Commission
                    if (!string.IsNullOrEmpty(rep3))
                    {
                        if (!string.IsNullOrEmpty(territory3))
                        {
                            if (!string.IsNullOrEmpty(comm3))
                            {
                                var repExits = DB.Reps.Where(y => y.RepName.Trim().ToLower() == rep3.Trim().ToLower()).ToList();
                                //Temp Code Starts
                                bool isError3 = false;
                                int repId = 0;
                                if (repExits.Count == 0)
                                {
                                    Rep newRep = new Rep();
                                    newRep.RepName = rep3.Trim();
                                    newRep.CreatedOn = DateTime.Now;
                                    //newRep.DeletedInd = false;

                                    //*
                                    newRep.UpdatedOn = DateTime.Now;
                                    newRep.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                                    newRep.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                    DB.Reps.Add(newRep);
                                    DB.SaveChanges();
                                    if (newRep.RepId == 0)
                                    {
                                        isError3 = true;
                                    }
                                    else
                                    {
                                        repId = newRep.RepId; ;
                                    }
                                }
                                else
                                {
                                    repId = repExits.Select(x => x.RepId).FirstOrDefault();
                                }

                                if (!isError3)
                                {
                                    var territoryExits = DB.Territories.Where(y => y.TerritoryName.Trim().ToLower() == territory3.Trim().ToLower()).ToList();
                                    //Temp Code Starts
                                    int territoryId = 0;
                                    if (territoryExits.Count == 0)
                                    {
                                        Territory newTerritory = new Territory();
                                        newTerritory.TerritoryName = territory3.Trim();
                                        newTerritory.CreatedOn = DateTime.Now;
                                        //newTerritory.DeletedInd = false;

                                        //*
                                        newTerritory.UpdatedOn = DateTime.Now;
                                        newTerritory.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                                        newTerritory.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                        DB.Territories.Add(newTerritory);
                                        DB.SaveChanges();
                                        if (newTerritory.TerritoryId == 0)
                                        {
                                            isError3 = true;
                                        }
                                        else
                                        {
                                            territoryId = newTerritory.TerritoryId; ;
                                        }
                                    }
                                    else
                                    {
                                        territoryId = territoryExits.Select(x => x.TerritoryId).FirstOrDefault();
                                    }

                                    if (!isError3)
                                    {
                                        float commission = float.Parse(comm3, CultureInfo.InvariantCulture.NumberFormat);
                                        var isRepAndTerritoryExits = DB.RepTerritories.AsEnumerable().Where(x => (x.RepId == repId && x.TerritoryId == territoryId
                                        /*&& x.DeletedInd != true*/)).FirstOrDefault();
                                        if (isRepAndTerritoryExits == null)
                                        {
                                            //Insert
                                            isRepAndTerritoryExits = new RepTerritory();
                                            isRepAndTerritoryExits.RepId = repId;
                                            isRepAndTerritoryExits.TerritoryId = territoryId;
                                            isRepAndTerritoryExits.Commission = double.Parse(commission.ToString());
                                            isRepAndTerritoryExits.CreatedOn = DateTime.Now;
                                            //isRepAndTerritoryExits.DeletedInd = false;

                                            //*
                                            isRepAndTerritoryExits.UpdatedOn = DateTime.Now;
                                            isRepAndTerritoryExits.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                                            isRepAndTerritoryExits.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                            DB.RepTerritories.Add(isRepAndTerritoryExits);
                                            DB.SaveChanges();
                                            if (isRepAndTerritoryExits.RepTerritoryId == 0)
                                            {
                                                isError3 = true;
                                            }
                                        }
                                        else if (isRepAndTerritoryExits.Commission.ToString() != commission.ToString())
                                        {
                                            //Update or throw error
                                            isRepAndTerritoryExits.Commission = double.Parse(commission.ToString());

                                            //*
                                            isRepAndTerritoryExits.UpdatedOn = DateTime.Now;
                                            isRepAndTerritoryExits.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                            DB.SaveChanges();
                                        }

                                        if (!isError3)
                                        {
                                            if (isSpiltChecked == false)
                                            {
                                                if (!string.IsNullOrEmpty(split1) || !string.IsNullOrEmpty(split2) || !string.IsNullOrEmpty(split3) || !string.IsNullOrEmpty(split4))
                                                {
                                                    decimal sp1 = 0;
                                                    decimal sp2 = 0;
                                                    decimal sp3 = 0;
                                                    decimal sp4 = 0;
                                                    if (!string.IsNullOrEmpty(split1))
                                                    {
                                                        sp1 = Convert.ToDecimal(split1) * 100;
                                                    }
                                                    if (!string.IsNullOrEmpty(split2))
                                                    {
                                                        sp2 = Convert.ToDecimal(split2) * 100;
                                                    }
                                                    if (!string.IsNullOrEmpty(split3))
                                                    {
                                                        sp3 = Convert.ToDecimal(split3) * 100;
                                                    }
                                                    if (!string.IsNullOrEmpty(split4))
                                                    {
                                                        sp4 = Convert.ToDecimal(split4) * 100;
                                                    }

                                                    var totalSpilt = Convert.ToDecimal(sp1 + sp2 + sp3 + sp4);
                                                    if (totalSpilt > 100)
                                                    {
                                                        string cError = "spilt sum is equal to 100";
                                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Split1,Split2,Split3,Split4", "", cError);
                                                        isRepsAndTerritoryValid = false;
                                                    }
                                                    else if (totalSpilt < 100)
                                                    {
                                                        string cError = "spilt sum is equal to 100";
                                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Split1,Split2,Split3,Split4", "", cError);
                                                        isRepsAndTerritoryValid = false;
                                                    }
                                                    else if (totalSpilt == 100)
                                                    {
                                                        if (Convert.ToDecimal(split1) + Convert.ToDecimal(split2) + Convert.ToDecimal(split3) <= 100)
                                                        {
                                                            PostMediaOrderRepsModel objRepsComm = new PostMediaOrderRepsModel();
                                                            objRepsComm.RepId = repId;
                                                            objRepsComm.TerritoryId = territoryId;
                                                            objRepsComm.Split = Convert.ToDouble(split3) * 100;
                                                            objRepsComm.Commission = double.Parse(commission.ToString());
                                                            lstRepsComm.Add(objRepsComm);
                                                        }
                                                    }
                                                    if (sp1 + sp2 + sp3 == 100)
                                                    {
                                                        isSpiltChecked = true;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            string cError = "reps and territory wise commission is not exits database";
                                            CreateErrorOfDatatableForImportOrder(ref dtError, "validation Error", "", cError);
                                            isRepsAndTerritoryValid = false;
                                        }
                                    }
                                    else
                                    {
                                        string cError = "Territory3 is not exits database";
                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Territory3", "", cError);
                                        isRepsAndTerritoryValid = false;
                                    }
                                }
                                else
                                {
                                    string cError = "Rep3 is not exits databse";
                                    CreateErrorOfDatatableForImportOrder(ref dtError, "Rep3", "", cError);
                                    isRepsAndTerritoryValid = false;
                                }

                                // var repsTerritoryExits = db.RepTerritories.Where(x=>x.RepId)
                            }
                            else
                            {
                                //string cError = "Comm3 is blank";
                                //createErrorOfDatatableForImportOrder(ref dtError, "Comm3", "", cError);
                                isRepsAndTerritoryValid = false;
                            }
                        }
                        else
                        {
                            //string cError = "Territory3 is blank";
                            //createErrorOfDatatableForImportOrder(ref dtError, "Territory3", "", cError);
                            isRepsAndTerritoryValid = false;
                        }
                    }
                    else
                    {
                        //string cError = "Rep3 is blank";
                        //createErrorOfDatatableForImportOrder(ref dtError, "Rep3", "", cError);
                        isRepsAndTerritoryValid = false;
                    }
                    #endregion

                    #region Rep4 and Territory4 Wise Commission
                    if (!string.IsNullOrEmpty(rep4))
                    {
                        if (!string.IsNullOrEmpty(territory4))
                        {
                            if (!string.IsNullOrEmpty(comm4))
                            {
                                var repExits = DB.Reps.Where(y => y.RepName.Trim().ToLower() == rep4.Trim().ToLower()).ToList();
                                //Temp Code Starts
                                bool isError4 = false;
                                int repId = 0;
                                if (repExits.Count == 0)
                                {
                                    Rep newRep = new Rep();
                                    newRep.RepName = rep4.Trim();
                                    newRep.CreatedOn = DateTime.Now;
                                    //newRep.DeletedInd = false;

                                    //*
                                    newRep.UpdatedOn = DateTime.Now;
                                    newRep.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                                    newRep.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                    DB.Reps.Add(newRep);
                                    DB.SaveChanges();
                                    if (newRep.RepId == 0)
                                    {
                                        isError4 = true;
                                    }
                                    else
                                    {
                                        repId = newRep.RepId; ;
                                    }
                                }
                                else
                                {
                                    repId = repExits.Select(x => x.RepId).FirstOrDefault();
                                }

                                if (!isError4)
                                {
                                    var territoryExits = DB.Territories.Where(y => y.TerritoryName.Trim().ToLower() == territory4.Trim().ToLower()).ToList();
                                    //Temp Code Starts
                                    int territoryId = 0;
                                    if (territoryExits.Count == 0)
                                    {
                                        Territory newTerritory = new Territory();
                                        newTerritory.TerritoryName = territory4.Trim();
                                        newTerritory.CreatedOn = DateTime.Now;
                                        //newTerritory.DeletedInd = false;

                                        //*
                                        newTerritory.UpdatedOn = DateTime.Now;
                                        newTerritory.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                                        newTerritory.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                        DB.Territories.Add(newTerritory);
                                        DB.SaveChanges();
                                        if (newTerritory.TerritoryId == 0)
                                        {
                                            isError4 = true;
                                        }
                                        else
                                        {
                                            territoryId = newTerritory.TerritoryId; ;
                                        }
                                    }
                                    else
                                    {
                                        territoryId = territoryExits.Select(x => x.TerritoryId).FirstOrDefault();
                                    }

                                    if (!isError4)
                                    {
                                        float commission = float.Parse(comm4, CultureInfo.InvariantCulture.NumberFormat);
                                        var isRepAndTerritoryExits = DB.RepTerritories.AsEnumerable().Where(x => (x.RepId == repId && x.TerritoryId == territoryId)).FirstOrDefault();


                                        if (isRepAndTerritoryExits == null)
                                        {
                                            //Insert
                                            isRepAndTerritoryExits = new RepTerritory();
                                            isRepAndTerritoryExits.RepId = repId;
                                            isRepAndTerritoryExits.TerritoryId = territoryId;
                                            isRepAndTerritoryExits.Commission = double.Parse(commission.ToString());
                                            isRepAndTerritoryExits.CreatedOn = DateTime.Now;
                                            //isRepAndTerritoryExits.DeletedInd = false;

                                            //*
                                            isRepAndTerritoryExits.UpdatedOn = DateTime.Now;
                                            isRepAndTerritoryExits.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                                            isRepAndTerritoryExits.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                            DB.RepTerritories.Add(isRepAndTerritoryExits);
                                            DB.SaveChanges();
                                            if (isRepAndTerritoryExits.RepTerritoryId == 0)
                                            {
                                                isError4 = true;
                                            }
                                        }
                                        else if (isRepAndTerritoryExits.Commission.ToString() != commission.ToString())
                                        {
                                            //Update or throw error
                                            isRepAndTerritoryExits.Commission = double.Parse(commission.ToString());

                                            //*
                                            isRepAndTerritoryExits.UpdatedOn = DateTime.Now;
                                            isRepAndTerritoryExits.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                            DB.SaveChanges();
                                        }

                                        if (!isError4)
                                        {
                                            if (isSpiltChecked == false)
                                            {
                                                if (!string.IsNullOrEmpty(split1) || !string.IsNullOrEmpty(split2) || !string.IsNullOrEmpty(split3) || !string.IsNullOrEmpty(split4))
                                                {
                                                    decimal sp1 = 0;
                                                    decimal sp2 = 0;
                                                    decimal sp3 = 0;
                                                    decimal sp4 = 0;
                                                    if (!string.IsNullOrEmpty(split1))
                                                    {
                                                        sp1 = Convert.ToDecimal(split1) * 100;
                                                    }
                                                    if (!string.IsNullOrEmpty(split2))
                                                    {
                                                        sp2 = Convert.ToDecimal(split2) * 100;
                                                    }
                                                    if (!string.IsNullOrEmpty(split3))
                                                    {
                                                        sp3 = Convert.ToDecimal(split3) * 100;
                                                    }
                                                    if (!string.IsNullOrEmpty(split4))
                                                    {
                                                        sp4 = Convert.ToDecimal(split4) * 100;
                                                    }

                                                    var totalSpilt = Convert.ToDecimal(sp1 + sp2 + sp3 + sp4);
                                                    if (totalSpilt > 100)
                                                    {
                                                        string cError = "spilt sum is equal to 100";
                                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Split1,Split2,Split3,Split4", "", cError);
                                                        isRepsAndTerritoryValid = false;
                                                    }
                                                    else if (totalSpilt < 100)
                                                    {
                                                        string cError = "spilt sum is equal to 100";
                                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Split1,Split2,Split3,Split4", "", cError);
                                                        isRepsAndTerritoryValid = false;
                                                    }
                                                    else if (totalSpilt == 100)
                                                    {
                                                        if (Convert.ToDecimal(split1) + Convert.ToDecimal(split2) + Convert.ToDecimal(split3) + Convert.ToDecimal(split4) <= 100)
                                                        {
                                                            PostMediaOrderRepsModel objRepsComm = new PostMediaOrderRepsModel();
                                                            objRepsComm.RepId = repId;
                                                            objRepsComm.TerritoryId = territoryId;
                                                            objRepsComm.Split = Convert.ToDouble(split4) * 100;
                                                            objRepsComm.Commission = double.Parse(commission.ToString());
                                                            lstRepsComm.Add(objRepsComm);
                                                        }
                                                    }
                                                    if (sp1 + sp2 + sp3 + sp4 == 100)
                                                    {
                                                        isSpiltChecked = true;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            string cError = "reps and territory wise commission is not exits database";
                                            CreateErrorOfDatatableForImportOrder(ref dtError, "validation Error", "", cError);
                                            isRepsAndTerritoryValid = false;
                                        }
                                    }
                                    else
                                    {
                                        string cError = "Territory4 is not exits database";
                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Territory4", "", cError);
                                        isRepsAndTerritoryValid = false;
                                    }
                                }
                                else
                                {
                                    string cError = "Rep4 is not exits databse";
                                    CreateErrorOfDatatableForImportOrder(ref dtError, "Rep4", "", cError);
                                    isRepsAndTerritoryValid = false;
                                }

                                // var repsTerritoryExits = db.RepTerritories.Where(x=>x.RepId)
                            }
                            else
                            {
                                //string cError = "Comm4 is blank";
                                //createErrorOfDatatableForImportOrder(ref dtError, "Comm4", "", cError);
                                isRepsAndTerritoryValid = false;
                            }
                        }
                        else
                        {
                            //string cError = "Territory4 is blank";
                            //createErrorOfDatatableForImportOrder(ref dtError, "Territory4", "", cError);
                            isRepsAndTerritoryValid = false;
                        }
                    }
                    else
                    {
                        //string cError = "Rep4 is blank";
                        //createErrorOfDatatableForImportOrder(ref dtError, "Rep4", "", cError);
                        isRepsAndTerritoryValid = false;
                    }
                    #endregion

                    objPostMOM.MediaOrderReps = lstRepsComm;

                    #endregion

                    #region object for Media Schedule (step = 2)
                    var dbRateCard = mResponseData.RateCards.Where(x => x.RateCardName.Trim().ToLower() == rateCardNum.Trim().ToLower()).FirstOrDefault();

                    AdType adType = DB.AdTypes.Where(x => x.AdTypeName == adTypeName).FirstOrDefault();
                    if (adType == null)
                    {
                        adType = new AdType
                        {
                            AdTypeName = adTypeName,
                            CreatedOn = DateTime.Now,
                            //DeletedInd = false,

                            //*
                            UpdatedOn = DateTime.Now,
                            CreatedByUserKey = Constants.Created_Update_ByUserKey,
                            UpdatedByUserKey = Constants.Created_Update_ByUserKey
                        };
                        DB.AdTypes.Add(adType);
                        DB.SaveChanges();
                    }
                    MediaTypeRef mediaType = null;
                    mediaType = DB.MediaTypes.Where(x => x.MediaTypeName == "Print").FirstOrDefault();
                    if (mediaType == null)
                    {
                        mediaType = new MediaTypeRef
                        {
                            MediaTypeName = "Print"
                        };
                        DB.MediaTypes.Add(mediaType);
                        DB.SaveChanges();
                    }

                    //* 2020-11-10 Change : No longer needed
                    //MediaBillingMethod objBillingMethod = null;
                    //objBillingMethod = DB.MediaBillingMethods.Where(x => x.MediaBillingMethodName == "Flat Rate").FirstOrDefault();

                    //* 2020-11-10 Change : No longer needed
                    //if (objBillingMethod == null)
                    //{
                    //    objBillingMethod = new MediaBillingMethod
                    //    {
                    //        MediaBillingMethodName = "Flat Rate",
                    //        CreatedOn = DateTime.Now,
                    //        //DeletedInd = false,

                    //        //*
                    //        UpdatedOn = DateTime.Now,
                    //        CreatedByUserKey = Constants.Created_Update_ByUserKey,
                    //        UpdatedByUserKey = Constants.Created_Update_ByUserKey
                    //    };
                    //    DB.MediaBillingMethods.Add(objBillingMethod);
                    //    DB.SaveChanges();
                    //}

                    //* Change on 2020-11-10
                    //var objBillingMethod = Get_MediaBillingMethodName_Values.GetMediaBillingMethodNameValues().Where(x => x.MediaBillingMethodName == "Flat Rate").FirstOrDefault();

                    //* Change on 2020-11-25
                    var _MediaBillingMethodName = BillingMethod.Flat_Rate.ToString().Replace("_", " ");

                    DateTime issueDateDT = Convert.ToDateTime(issueDate.Trim());
                    DateTime validStartDate = new DateTime(issueDateDT.Year, 01, 01);
                    DateTime validEndDate = new DateTime(issueDateDT.Year, 12, 31);

                    if (dbRateCard == null || dbRateCard.RateCardId == 0)
                    {
                        //Temp Code Starts
                        dbRateCard = new RateCard
                        {
                            MediaAssetId = mResponseData.MediaAssetId,
                            MediaTypeId = mediaType.MediaTypeId,
                            RateCardName = rateCardNum.Trim(),
                            AdTypeId = adType.AdTypeId,

                            //* 2020-11-10 Change
                            //MediaBillingMethodId = objBillingMethod.MediaBillingMethodId,

                            //* Change on 2020-11-25
                            MediaBillingMethodName = _MediaBillingMethodName,

                            ValidFromDate = validStartDate,
                            ValidToDate = validEndDate,
                            ProductCode = "code3",
                            CreatedOn = DateTime.Now,
                            //DeletedInd = false,

                            //*
                            UpdatedOn = DateTime.Now,
                            CreatedByUserKey = Constants.Created_Update_ByUserKey,
                            UpdatedByUserKey = Constants.Created_Update_ByUserKey
                        };
                        DB.RateCards.Add(dbRateCard);
                        DB.SaveChanges();
                    }


                    if (dbRateCard.RateCardId != 0)
                    {
                        DateTime iDate = Convert.ToDateTime(issueDate.Trim());
                        List<int> issueDates = new List<int>();
                        var isIssueDateExit = mResponseData.AdIssueDates.Where(x => x.CoverDate == iDate).FirstOrDefault();
                        //Temp Code Starts
                        if (isIssueDateExit == null)
                        {
                            isIssueDateExit = new AdIssueDate
                            {
                                MediaAssetId = mResponseData.MediaAssetId,
                                CoverDate = iDate,
                                CreatedOn = DateTime.Now,
                                //DeletedInd = false,

                                //*
                                UpdatedOn = DateTime.Now,
                                CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                UpdatedByUserKey = Constants.Created_Update_ByUserKey
                            };
                            DB.AdIssueDates.Add(isIssueDateExit);
                            DB.SaveChanges();
                            if (isIssueDateExit.IssueDateId == 0)
                            {
                                isError = false;
                            }
                        }
                        if (!isError)
                        {
                            int issueDateId = isIssueDateExit.IssueDateId;
                            issueDates.Add(issueDateId);
                            DateTime startDate = Convert.ToDateTime(flightStartDate);
                            DateTime endDate = Convert.ToDateTime(flightEndDate);
                            if (iDate >= startDate && iDate <= endDate)
                            {
                                objPostMOM.MediaOrderIssueDates = issueDates;
                                objPostMOM.FlightStartDate = Convert.ToDateTime(flightStartDate);
                                objPostMOM.FlightEndDate = Convert.ToDateTime(flightEndDate);
                            }
                            else
                            {
                                string cError = "Issue Date is not exit between FlightStartDate and FlightEndDate";
                                CreateErrorOfDatatableForImportOrder(ref dtError, "Issue Date", "", cError);
                                isValid = false;
                            }

                            objPostMOM.RateCardId = dbRateCard.RateCardId;
                            objPostMOM.CampaignName = string.Empty;
                            objPostMOM.ProductCode = mResponseData.ProductCode;

                            //* 2020-11-10 Change
                            //int billingMethod = dbRateCard.MediaBillingMethodId;
                            //string billingMethodName = dbRateCard.MediaBillingMethod.MediaBillingMethodName;

                            //int billingMethod = Get_MediaBillingMethodName_Values.GetMediaBillingMethodNameValues()
                            //    .Where(x=> x.MediaBillingMethodName == dbRateCard.MediaBillingMethodName)
                            //    .Select(x=> x.MediaBillingMethodId).FirstOrDefault();

                            string billingMethodName = dbRateCard.MediaBillingMethodName;

                            var dbFrequency = DB.Frequencies.Where(x => x.FrequencyName == rate /*&& x.DeletedInd == false*/).FirstOrDefault();
                            var dbAdColor = DB.AdColors.Where(x => x.AdColorName == adColorName /*&& x.DeletedInd == false*/).FirstOrDefault();
                            var dbAdSize = DB.AdSizes.Where(x => x.AdSizeName == adSizeName /*&& x.DeletedInd == false*/).FirstOrDefault();
                            //Temp Code Starts
                            if (dbFrequency == null || dbFrequency.FrequencyId == 0)
                            {
                                dbFrequency = new Frequency
                                {
                                    FrequencyName = rate,
                                    CreatedOn = DateTime.Now,
                                    //DeletedInd = false,

                                    //*
                                    UpdatedOn = DateTime.Now,
                                    CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                    UpdatedByUserKey = Constants.Created_Update_ByUserKey
                                };
                                DB.Frequencies.Add(dbFrequency);
                                DB.SaveChanges();

                            }
                            if (dbAdColor == null || dbAdColor.AdColorId == 0)
                            {
                                dbAdColor = new AdColor
                                {
                                    AdColorName = adColorName,
                                    CreatedOn = DateTime.Now,
                                    //DeletedInd = false,

                                    //*
                                    UpdatedOn = DateTime.Now,
                                    CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                    UpdatedByUserKey = Constants.Created_Update_ByUserKey
                                };
                                DB.AdColors.Add(dbAdColor);
                                DB.SaveChanges();
                            }
                            if (dbAdSize == null || dbAdSize.AdSizeId == 0)
                            {
                                dbAdSize = new AdSize
                                {
                                    AdSizeName = adSizeName,
                                    PageFraction = 0,
                                    MediaAssetId = mResponseData.MediaAssetId,
                                    CreatedOn = DateTime.Now,
                                    //DeletedInd = false,

                                    //*
                                    UpdatedOn = DateTime.Now,
                                    CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                    UpdatedByUserKey = Constants.Created_Update_ByUserKey
                                };
                                DB.AdSizes.Add(dbAdSize);
                                DB.SaveChanges();
                            }
                            #region Flat Rate Cost Calculation

                            if (billingMethodName == BillingMethod.Flat_Rate.ToString().Replace("_", " "))

                            {
                                if (dbFrequency.FrequencyId > 0 && dbAdColor.AdColorId > 0 && dbAdSize.AdSizeId > 0 && dbRateCard.RateCardId > 0)
                                {
                                    bool isGetCost = true;
                                    int frequencyId = dbFrequency.FrequencyId;
                                    int adColorId = dbAdColor.AdColorId;
                                    int adSizeId = dbAdSize.AdSizeId;
                                    int rateCardId = dbRateCard.RateCardId;

                                    //Temp Code Starts
                                    var checkRateCardDetails = DB.RateCardDetails.Where(x => x.RateCardId == rateCardId && x.AdSizeId == adSizeId && x.AdColorId == adColorId && x.FrequencyId == frequencyId).ToList();
                                    if (checkRateCardDetails == null || checkRateCardDetails.Count() == 0)
                                    {
                                        RateCardDetail rateCardDetail = new RateCardDetail
                                        {
                                            RateCardId = rateCardId,
                                            AdTypeId = dbRateCard.AdTypeId,
                                            AdSizeId = adSizeId,
                                            AdColorId = adColorId,
                                            FrequencyId = frequencyId,
                                            RateCardCost = Math.Round(Convert.ToDecimal(baseRateCardCost), 2),
                                            CreatedOn = DateTime.Now,
                                            //DeletedInd = false,

                                            //*
                                            UpdatedOn = DateTime.Now,
                                            CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                            UpdatedByUserKey = Constants.Created_Update_ByUserKey,
                                        };
                                        DB.RateCardDetails.Add(rateCardDetail);
                                        DB.SaveChanges();
                                    }
                                    List<RateCardDetail> dbRateCardDetails = DB.RateCardDetails.Where(x => x.RateCardId == dbRateCard.RateCardId).ToList();
                                    var isFrequencyExitinRCD = dbRateCardDetails.Where(x => x.FrequencyId == frequencyId).ToList();
                                    var isAdColorExitinRCD = dbRateCardDetails.Where(x => x.AdColorId == adColorId).ToList();
                                    var isAdSizeExitinRCD = dbRateCardDetails.Where(x => x.AdSizeId == adSizeId).ToList();

                                    if (isFrequencyExitinRCD == null)
                                    {
                                        string cError = "Frequency is not present in this rateCardDetails";
                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Rate", "", cError);
                                        isGetCost = false;
                                        isValid = false;
                                    }

                                    if (isAdColorExitinRCD == null)
                                    {
                                        string cError = "AdColor is not present in this rateCardDetails";
                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Color", "", cError);
                                        isGetCost = false;
                                        isValid = false;
                                    }

                                    if (isAdSizeExitinRCD == null)
                                    {
                                        string cError = "Ad Size is not present in this rateCardDetails";
                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Ad Size", "", cError);
                                        isGetCost = false;
                                        isValid = false;
                                    }

                                    if (isGetCost == true)
                                    {
                                        var responseData = _IRateCardDetailRepository.GetMultipleRateCardsCost(rateCardId, adColorId, adSizeId, frequencyId);
                                        string rateCardCostJsonData = JsonConvert.SerializeObject(responseData.Data, Formatting.Indented);
                                        List<GetBasicRateCardCost> lstGetCostForFlatRate = JsonConvert.DeserializeObject<List<GetBasicRateCardCost>>(rateCardCostJsonData);

                                        if (lstGetCostForFlatRate.Count > 0)
                                        {
                                            bool isCalled = false;
                                            foreach (GetBasicRateCardCost getCostForFlatRate in lstGetCostForFlatRate)
                                            {
                                                if (isCalled == false)
                                                {
                                                    if (Math.Round(Convert.ToDecimal(getCostForFlatRate.RateCardCost), 2) == Math.Round(Convert.ToDecimal(baseRateCardCost), 2))
                                                    {
                                                        isCalled = true;
                                                        if (getCostForFlatRate.RateCardDetailId > 0)
                                                        {
                                                            objPostMOM.RateCardDetailId = getCostForFlatRate.RateCardDetailId;
                                                        }
                                                        else
                                                        {
                                                            string cError = "AdRateCardDetailId is not found";
                                                            CreateErrorOfDatatableForImportOrder(ref dtError, "validation", "", cError);
                                                            isGetCost = false;
                                                            isValid = false;
                                                        }
                                                        objPostMOM.BaseCalculationCost = Convert.ToDecimal(baseRateCardCost);
                                                        objPostMOM.BaseRateCardCost = Convert.ToDecimal(getCostForFlatRate.RateCardCost);
                                                        objPostMOM.RateCardCost = Convert.ToDecimal(adCost);

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region CPM Cost Calculation
                            else if (billingMethodName == BillingMethod.CPM.ToString())
                            {
                                if (dbFrequency.FrequencyId > 0 && dbAdColor.AdColorId > 0 && dbAdSize.AdSizeId > 0 && dbRateCard.RateCardId > 0)
                                {
                                    bool isGetCost = true;
                                    int frequencyId = dbFrequency.FrequencyId;
                                    int adColorId = dbAdColor.AdColorId;
                                    int adSizeId = dbAdSize.AdSizeId;
                                    int rateCardId = dbRateCard.RateCardId;
                                    //Temp Code Starts
                                    var checkRateCardDetails = DB.RateCardDetails.Where(x => x.RateCardId == rateCardId && x.AdSizeId == adSizeId && x.AdColorId == adColorId && x.FrequencyId == frequencyId).ToList();
                                    if (checkRateCardDetails == null || checkRateCardDetails.Count() == 0)
                                    {
                                        RateCardDetail rateCardDetail = new RateCardDetail
                                        {
                                            RateCardId = rateCardId,
                                            AdTypeId = dbRateCard.AdTypeId,
                                            AdSizeId = adSizeId,
                                            AdColorId = adColorId,
                                            FrequencyId = frequencyId,
                                            RateCardCost = Math.Round(Convert.ToDecimal(baseRateCardCost), 2),
                                            CreatedOn = DateTime.Now,
                                            //DeletedInd = false,

                                            //*
                                            UpdatedOn = DateTime.Now,
                                            CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                            UpdatedByUserKey = Constants.Created_Update_ByUserKey,
                                        };
                                        DB.RateCardDetails.Add(rateCardDetail);
                                        DB.SaveChanges();
                                    }
                                    List<RateCardDetail> dbRateCardDetails = DB.RateCardDetails.Where(x => x.RateCardId == dbRateCard.RateCardId).ToList();

                                    var isFrequencyExitinRCD = dbRateCardDetails.Where(x => x.FrequencyId == frequencyId).ToList();
                                    var isAdColorExitinRCD = dbRateCardDetails.Where(x => x.AdColorId == adColorId).ToList();
                                    var isAdSizeExitinRCD = dbRateCardDetails.Where(x => x.AdSizeId == adSizeId).ToList();

                                    if (isFrequencyExitinRCD == null)
                                    {
                                        string cError = "Frequency is not present in this rateCardDetails";
                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Rate", "", cError);
                                        isGetCost = false;
                                        isValid = false;
                                    }

                                    if (isAdColorExitinRCD == null)
                                    {
                                        string cError = "AdColor is not present in this rateCardDetails";
                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Color", "", cError);
                                        isGetCost = false;
                                        isValid = false;
                                    }

                                    if (isAdSizeExitinRCD == null)
                                    {
                                        string cError = "Ad Size is not present in this rateCardDetails";
                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Ad Size", "", cError);
                                        isGetCost = false;
                                        isValid = false;
                                    }

                                    if (isGetCost == true)
                                    {
                                        var responseData = _IRateCardDetailRepository.GetMultipleRateCardsCost(rateCardId, adColorId, adSizeId, frequencyId);
                                        //GetBasicRateCardCost getCostForFlatRate = (responseData.data) as GetBasicRateCardCost;

                                        string rateCardCostJsonData = JsonConvert.SerializeObject(responseData.Data, Formatting.Indented);
                                        List<GetBasicRateCardCost> lstGetCostForFlatRate = JsonConvert.DeserializeObject<List<GetBasicRateCardCost>>(rateCardCostJsonData);

                                        if (lstGetCostForFlatRate.Count > 0)
                                        {
                                            bool isCalled = false;
                                            foreach (GetBasicRateCardCost getCostForFlatRate in lstGetCostForFlatRate)
                                            {
                                                if (isCalled == false)
                                                {
                                                    if (Math.Round(Convert.ToDecimal(getCostForFlatRate.RateCardCost), 2) == Math.Round(Convert.ToDecimal(baseRateCardCost), 2))
                                                    {
                                                        isCalled = true;
                                                        if (getCostForFlatRate.RateCardDetailId > 0)
                                                        {
                                                            objPostMOM.RateCardDetailId = getCostForFlatRate.RateCardDetailId;
                                                        }
                                                        else
                                                        {
                                                            string cError = "AdRateCardDetailId is not found";
                                                            CreateErrorOfDatatableForImportOrder(ref dtError, "validation", "", cError);
                                                            isGetCost = false;
                                                            isValid = false;
                                                        }

                                                        decimal rateCardCost = Convert.ToDecimal(getCostForFlatRate.RateCardCost);
                                                        decimal noOfInserts = Convert.ToDecimal(units);
                                                        decimal cpmCost = ((rateCardCost * noOfInserts) / 1000);

                                                        objPostMOM.Units = Convert.ToDecimal(noOfInserts);
                                                        objPostMOM.BaseCalculationCost = Convert.ToDecimal(cpmCost);
                                                        objPostMOM.BaseRateCardCost = Convert.ToDecimal(getCostForFlatRate.RateCardCost);
                                                        objPostMOM.RateCardCost = Convert.ToDecimal(adCost);

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region Web CPM Cost Calculation
                            else if (billingMethodName == BillingMethod.Web_CPM.ToString().Replace("_", " "))
                            {
                                if (dbFrequency.FrequencyId > 0 && dbAdColor.AdColorId > 0 && dbAdSize.AdSizeId > 0 && dbRateCard.RateCardId > 0)
                                {
                                    bool isGetCost = true;
                                    int frequencyId = dbFrequency.FrequencyId;
                                    int adColorId = dbAdColor.AdColorId;
                                    int adSizeId = dbAdSize.AdSizeId;
                                    int rateCardId = dbRateCard.RateCardId;
                                    //Temp Code Starts
                                    var checkRateCardDetails = DB.RateCardDetails.Where(x => x.RateCardId == rateCardId && x.AdSizeId == adSizeId && x.AdColorId == adColorId && x.FrequencyId == frequencyId).ToList();
                                    if (checkRateCardDetails == null || checkRateCardDetails.Count() == 0)
                                    {
                                        RateCardDetail rateCardDetail = new RateCardDetail();
                                        rateCardDetail.RateCardId = rateCardId;
                                        rateCardDetail.AdTypeId = dbRateCard.AdTypeId;
                                        rateCardDetail.AdSizeId = adSizeId;
                                        rateCardDetail.AdColorId = adColorId;
                                        rateCardDetail.FrequencyId = frequencyId;
                                        rateCardDetail.RateCardCost = Math.Round(Convert.ToDecimal(baseRateCardCost), 2);
                                        rateCardDetail.CreatedOn = DateTime.Now;
                                        //rateCardDetail.DeletedInd = false;

                                        //*
                                        rateCardDetail.UpdatedOn = DateTime.Now;
                                        rateCardDetail.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                                        rateCardDetail.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                        DB.RateCardDetails.Add(rateCardDetail);
                                        DB.SaveChanges();
                                    }
                                    List<RateCardDetail> dbRateCardDetails = DB.RateCardDetails.Where(x => x.RateCardId == dbRateCard.RateCardId).ToList();

                                    var isFrequencyExitinRCD = dbRateCardDetails.Where(x => x.FrequencyId == frequencyId).ToList();
                                    var isAdColorExitinRCD = dbRateCardDetails.Where(x => x.AdColorId == adColorId).ToList();
                                    var isAdSizeExitinRCD = dbRateCardDetails.Where(x => x.AdSizeId == adSizeId).ToList();

                                    if (isFrequencyExitinRCD == null)
                                    {
                                        string cError = "Frequency is not present in this rateCardDetails";
                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Rate", "", cError);
                                        isGetCost = false;
                                        isValid = false;
                                    }

                                    if (isAdColorExitinRCD == null)
                                    {
                                        string cError = "AdColor is not present in this rateCardDetails";
                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Color", "", cError);
                                        isGetCost = false;
                                        isValid = false;
                                    }

                                    if (isAdSizeExitinRCD == null)
                                    {
                                        string cError = "Ad Size is not present in this rateCardDetails";
                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Ad Size", "", cError);
                                        isGetCost = false;
                                        isValid = false;
                                    }

                                    if (isGetCost == true)
                                    {
                                        var responseData = _IRateCardDetailRepository.GetMultipleRateCardsCost(rateCardId, adColorId, adSizeId, frequencyId);
                                        //GetBasicRateCardCost getCostForFlatRate = (responseData.data) as GetBasicRateCardCost;
                                        string rateCardCostJsonData = JsonConvert.SerializeObject(responseData.Data, Formatting.Indented);
                                        List<GetBasicRateCardCost> lstGetCostForFlatRate = JsonConvert.DeserializeObject<List<GetBasicRateCardCost>>(rateCardCostJsonData);

                                        if (lstGetCostForFlatRate.Count > 0)
                                        {
                                            bool isCalled = false;
                                            foreach (GetBasicRateCardCost getCostForFlatRate in lstGetCostForFlatRate)
                                            {
                                                if (isCalled == false)
                                                {
                                                    if (Math.Round(Convert.ToDecimal(getCostForFlatRate.RateCardCost), 2) == Math.Round(Convert.ToDecimal(baseRateCardCost), 2))
                                                    {
                                                        isCalled = true;
                                                        if (getCostForFlatRate.RateCardDetailId > 0)
                                                        {
                                                            objPostMOM.RateCardDetailId = getCostForFlatRate.RateCardDetailId;
                                                        }
                                                        else
                                                        {
                                                            string cError = "AdRateCardDetailId is not found";
                                                            CreateErrorOfDatatableForImportOrder(ref dtError, "validation", "", cError);
                                                            isGetCost = false;
                                                            isValid = false;
                                                        }

                                                        decimal rateCardCost = Convert.ToDecimal(getCostForFlatRate.RateCardCost);
                                                        decimal impression = Convert.ToDecimal(units);
                                                        objPostMOM.Units = impression;
                                                        objPostMOM.BaseCalculationCost = Convert.ToDecimal(baseRateCardCost);
                                                        objPostMOM.BaseRateCardCost = Convert.ToDecimal(getCostForFlatRate.RateCardCost);
                                                        objPostMOM.RateCardCost = Convert.ToDecimal(adCost);

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region PCI Cost Calculation
                            else if (billingMethodName == BillingMethod.PCI.ToString())
                            {
                                if (dbFrequency.FrequencyId > 0 && dbAdColor.AdColorId > 0 && dbAdSize.AdSizeId > 0 && dbRateCard.RateCardId > 0)
                                {
                                    bool isGetCost = true;
                                    int frequencyId = dbFrequency.FrequencyId;
                                    int adColorId = dbAdColor.AdColorId;
                                    int adSizeId = dbAdSize.AdSizeId;
                                    int rateCardId = dbRateCard.RateCardId;
                                    //Temp Code Starts
                                    var checkRateCardDetails = DB.RateCardDetails.Where(x => x.RateCardId == rateCardId && x.AdSizeId == adSizeId && x.AdColorId == adColorId && x.FrequencyId == frequencyId).ToList();
                                    if (checkRateCardDetails == null || checkRateCardDetails.Count() == 0)
                                    {
                                        RateCardDetail rateCardDetail = new RateCardDetail
                                        {
                                            RateCardId = rateCardId,
                                            AdTypeId = dbRateCard.AdTypeId,
                                            AdSizeId = adSizeId,
                                            AdColorId = adColorId,
                                            FrequencyId = frequencyId,
                                            RateCardCost = Math.Round(Convert.ToDecimal(baseRateCardCost), 2),
                                            CreatedOn = DateTime.Now,
                                            //DeletedInd = false,

                                            //*
                                            UpdatedOn = DateTime.Now,
                                            CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                            UpdatedByUserKey = Constants.Created_Update_ByUserKey,
                                        };
                                        DB.RateCardDetails.Add(rateCardDetail);
                                        DB.SaveChanges();
                                    }
                                    List<RateCardDetail> dbRateCardDetails = DB.RateCardDetails.Where(x => x.RateCardId == dbRateCard.RateCardId).ToList();

                                    var isFrequencyExitinRCD = dbRateCardDetails.Where(x => x.FrequencyId == frequencyId).ToList();
                                    var isAdColorExitinRCD = dbRateCardDetails.Where(x => x.AdColorId == adColorId).ToList();
                                    var isAdSizeExitinRCD = dbRateCardDetails.Where(x => x.AdSizeId == adSizeId).ToList();

                                    if (isFrequencyExitinRCD == null)
                                    {
                                        string cError = "Frequency is not present in this rateCardDetails";
                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Rate", "", cError);
                                        isGetCost = false;
                                        isValid = false;
                                    }

                                    if (isAdColorExitinRCD == null)
                                    {
                                        string cError = "AdColor is not present in this rateCardDetails";
                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Color", "", cError);
                                        isGetCost = false;
                                        isValid = false;
                                    }

                                    if (isAdSizeExitinRCD == null)
                                    {
                                        string cError = "Ad Size is not present in this rateCardDetails";
                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Ad Size", "", cError);
                                        isGetCost = false;
                                        isValid = false;
                                    }

                                    if (isGetCost == true)
                                    {
                                        var responseData = _IRateCardDetailRepository.GetMultipleRateCardsCost(rateCardId, adColorId, adSizeId, frequencyId);
                                        //GetBasicRateCardCost getCostForFlatRate = (responseData.data) as GetBasicRateCardCost;
                                        string rateCardCostJsonData = JsonConvert.SerializeObject(responseData.Data, Formatting.Indented);
                                        List<GetBasicRateCardCost> lstGetCostForFlatRate = JsonConvert.DeserializeObject<List<GetBasicRateCardCost>>(rateCardCostJsonData);

                                        if (lstGetCostForFlatRate.Count > 0)
                                        {
                                            bool isCalled = false;
                                            foreach (GetBasicRateCardCost getCostForFlatRate in lstGetCostForFlatRate)
                                            {
                                                if (isCalled == false)
                                                {
                                                    if (Math.Round(Convert.ToDecimal(getCostForFlatRate.RateCardCost), 2) == Math.Round(Convert.ToDecimal(baseRateCardCost), 2))
                                                    {
                                                        isCalled = true;
                                                        if (getCostForFlatRate.RateCardDetailId > 0)
                                                        {
                                                            objPostMOM.RateCardDetailId = getCostForFlatRate.RateCardDetailId;
                                                        }
                                                        else
                                                        {
                                                            string cError = "AdRateCardDetailId is not found";
                                                            CreateErrorOfDatatableForImportOrder(ref dtError, "validation", "", cError);
                                                            isGetCost = false;
                                                            isValid = false;
                                                        }


                                                        decimal col = Convert.ToDecimal(columns);
                                                        decimal inc = Convert.ToDecimal(inches);
                                                        decimal pciAmount = Convert.ToDecimal(baseRateCardCost) * col * inc;
                                                        objPostMOM.Column = Convert.ToInt32(col);
                                                        objPostMOM.Inches = Convert.ToDouble(inc);
                                                        objPostMOM.BaseCalculationCost = Convert.ToDecimal(pciAmount);
                                                        objPostMOM.BaseRateCardCost = Convert.ToDecimal(getCostForFlatRate.RateCardCost);
                                                        objPostMOM.RateCardCost = Convert.ToDecimal(adCost);

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region Per Word Cost Calculation
                            else if (billingMethodName == BillingMethod.Per_Word.ToString().Replace("_", " "))
                            {
                                if (dbFrequency.FrequencyId > 0 && dbAdColor.AdColorId > 0 && dbAdSize.AdSizeId > 0 && dbRateCard.RateCardId > 0)
                                {
                                    bool isGetCost = true;
                                    int adColorId = dbAdColor.AdColorId;
                                    int rateCardId = dbRateCard.RateCardId;
                                    //Temp Code Starts
                                    var checkRateCardDetails = DB.RateCardDetails.Where(x => x.RateCardId == rateCardId && x.AdSizeId == dbAdSize.AdSizeId && x.AdColorId == adColorId && x.FrequencyId == dbFrequency.FrequencyId).ToList();
                                    if (checkRateCardDetails == null || checkRateCardDetails.Count() == 0)
                                    {
                                        RateCardDetail rateCardDetail = new RateCardDetail
                                        {
                                            RateCardId = rateCardId,
                                            AdTypeId = dbRateCard.AdTypeId,
                                            AdSizeId = dbAdSize.AdSizeId,
                                            AdColorId = adColorId,
                                            FrequencyId = dbFrequency.FrequencyId,
                                            RateCardCost = Math.Round(Convert.ToDecimal(baseRateCardCost), 2),
                                            CreatedOn = DateTime.Now,
                                            //DeletedInd = false,

                                            //*
                                            UpdatedOn = DateTime.Now,
                                            CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                            UpdatedByUserKey = Constants.Created_Update_ByUserKey,
                                        };
                                        DB.RateCardDetails.Add(rateCardDetail);
                                        DB.SaveChanges();
                                    }
                                    List<RateCardDetail> dbRateCardDetails = DB.RateCardDetails.Where(x => x.RateCardId == dbRateCard.RateCardId).ToList();

                                    var isAdColorExitinRCD = dbRateCardDetails.Where(x => x.AdColorId == adColorId).ToList();

                                    if (isAdColorExitinRCD == null)
                                    {
                                        string cError = "AdColor is not present in this rateCardDetails";
                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Color", "", cError);
                                        isGetCost = false;
                                        isValid = false;
                                    }

                                    string tmpClassifiedText = classifiedText.ToString();
                                    string[] tmpClassifiedTextCount = tmpClassifiedText.Split(' ');
                                    decimal finalAmount = 0;
                                    decimal fixedAmount = 0;

                                    if (isGetCost == true)
                                    {
                                        var responseData = _IRateCardDetailRepository.GetMultipleRateCardsCostForPerWord(rateCardId, adColorId);
                                        // GetRateCardCostForPerWord getCostForFlatRate = (responseData.data) as GetRateCardCostForPerWord;
                                        string rateCardCostJsonData = JsonConvert.SerializeObject(responseData.Data, Formatting.Indented);
                                        List<GetMultipleRateCardsCostForPerWord> lstGetCostForFlatRate = JsonConvert.DeserializeObject<List<GetMultipleRateCardsCostForPerWord>>(rateCardCostJsonData);

                                        if (lstGetCostForFlatRate.Count > 0)
                                        {
                                            bool isCalled = false;
                                            foreach (GetMultipleRateCardsCostForPerWord getCostForFlatRate in lstGetCostForFlatRate)
                                            {
                                                if (isCalled == false)
                                                {
                                                    if (Math.Round(Convert.ToDecimal(getCostForFlatRate.RateCardCost), 2) == Math.Round(Convert.ToDecimal(baseRateCardCost), 2))
                                                    {
                                                        isCalled = true;
                                                        if (getCostForFlatRate.RateCardDetailId > 0)
                                                        {
                                                            objPostMOM.RateCardDetailId = getCostForFlatRate.RateCardDetailId;
                                                        }
                                                        else
                                                        {
                                                            string cError = "AdRateCardDetailId is not found";
                                                            CreateErrorOfDatatableForImportOrder(ref dtError, "validation", "", cError);
                                                            isGetCost = false;
                                                            isValid = false;
                                                        }

                                                        decimal RateCardCost = Convert.ToDecimal(getCostForFlatRate.RateCardCost);
                                                        decimal NoOfWord = Convert.ToDecimal(getCostForFlatRate.NoOfWord);
                                                        decimal PerWordCost = Convert.ToDecimal(getCostForFlatRate.PerWordCost);

                                                        if (tmpClassifiedTextCount.Count() > 0)
                                                        {
                                                            if (tmpClassifiedTextCount.Count() >= NoOfWord)
                                                            {
                                                                int noWords = Convert.ToInt16(NoOfWord);
                                                                string[] tmp100ClassifiedText = tmpClassifiedTextCount.Take(noWords).ToArray();
                                                                fixedAmount = RateCardCost;
                                                                finalAmount = fixedAmount;
                                                                string[] tmpOtherClassifiedText = tmpClassifiedTextCount.Skip(noWords).ToArray();
                                                                if (tmpOtherClassifiedText.Count() > 0)
                                                                {
                                                                    decimal nextClassifiedText = Convert.ToDecimal(tmpOtherClassifiedText.Count());
                                                                    var tmpOtherAmount = PerWordCost * nextClassifiedText;
                                                                    finalAmount = fixedAmount = tmpOtherAmount;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                int noWords = Convert.ToInt16(NoOfWord);
                                                                string[] tmp100ClassifiedText = tmpClassifiedTextCount.Take(noWords).ToArray();
                                                                fixedAmount = RateCardCost;
                                                                finalAmount = fixedAmount;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            string cError = "Text is not present ";
                                                            CreateErrorOfDatatableForImportOrder(ref dtError, "ClassifiedText", "", cError);
                                                            isValid = false;
                                                        }

                                                        objPostMOM.ClassifiedText = tmpClassifiedText;
                                                        objPostMOM.PerWordCost = Convert.ToDecimal(PerWordCost);
                                                        objPostMOM.BaseCalculationCost = Convert.ToDecimal(finalAmount);
                                                        objPostMOM.BaseRateCardCost = Convert.ToDecimal(RateCardCost);
                                                        objPostMOM.RateCardCost = Convert.ToDecimal(adCost);

                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                            #region Sponsorship Cost Calculation
                            else if (billingMethodName == BillingMethod.Sponsorship.ToString())
                            {
                                if (dbFrequency.FrequencyId > 0 && dbAdColor.AdColorId > 0 && dbAdSize.AdSizeId > 0 && dbRateCard.RateCardId > 0)
                                {
                                    bool isGetCost = true;
                                    int frequencyId = dbFrequency.FrequencyId;
                                    int adColorId = dbAdColor.AdColorId;
                                    int adSizeId = dbAdSize.AdSizeId;
                                    int rateCardId = dbRateCard.RateCardId;
                                    //Temp Code Starts
                                    var checkRateCardDetails = DB.RateCardDetails.Where(x => x.RateCardId == rateCardId && x.AdSizeId == adSizeId && x.AdColorId == adColorId && x.FrequencyId == frequencyId).ToList();
                                    if (checkRateCardDetails == null || checkRateCardDetails.Count() == 0)
                                    {
                                        RateCardDetail rateCardDetail = new RateCardDetail();
                                        rateCardDetail.RateCardId = rateCardId;
                                        rateCardDetail.AdTypeId = dbRateCard.AdTypeId;
                                        rateCardDetail.AdSizeId = dbAdSize.AdSizeId;
                                        rateCardDetail.AdColorId = adColorId;
                                        rateCardDetail.FrequencyId = dbFrequency.FrequencyId;
                                        rateCardDetail.RateCardCost = Math.Round(Convert.ToDecimal(baseRateCardCost), 2);
                                        rateCardDetail.CreatedOn = DateTime.Now;
                                        //rateCardDetail.DeletedInd = false;

                                        //*
                                        rateCardDetail.UpdatedOn = DateTime.Now;
                                        rateCardDetail.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                                        rateCardDetail.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                        DB.RateCardDetails.Add(rateCardDetail);
                                        DB.SaveChanges();
                                    }
                                    List<RateCardDetail> dbRateCardDetails = DB.RateCardDetails.Where(x => x.RateCardId == dbRateCard.RateCardId).ToList();

                                    var isFrequencyExitinRCD = dbRateCardDetails.Where(x => x.FrequencyId == frequencyId).ToList();
                                    var isAdColorExitinRCD = dbRateCardDetails.Where(x => x.AdColorId == adColorId).ToList();
                                    var isAdSizeExitinRCD = dbRateCardDetails.Where(x => x.AdSizeId == adSizeId).ToList();

                                    if (isFrequencyExitinRCD == null)
                                    {
                                        string cError = "Frequency is not present in this rateCardDetails";
                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Rate", "", cError);
                                        isGetCost = false;
                                        isValid = false;
                                    }

                                    if (isAdColorExitinRCD == null)
                                    {
                                        string cError = "AdColor is not present in this rateCardDetails";
                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Color", "", cError);
                                        isGetCost = false;
                                        isValid = false;
                                    }

                                    if (isAdSizeExitinRCD == null)
                                    {
                                        string cError = "Ad Size is not present in this rateCardDetails";
                                        CreateErrorOfDatatableForImportOrder(ref dtError, "Ad Size", "", cError);
                                        isGetCost = false;
                                        isValid = false;
                                    }

                                    if (isGetCost == true)
                                    {
                                        var responseData = _IRateCardDetailRepository.GetMultipleRateCardsCost(rateCardId, adColorId, adSizeId, frequencyId);
                                        string rateCardCostJsonData = JsonConvert.SerializeObject(responseData.Data, Formatting.Indented);
                                        List<GetBasicRateCardCost> lstGetCostForFlatRate = JsonConvert.DeserializeObject<List<GetBasicRateCardCost>>(rateCardCostJsonData);

                                        if (lstGetCostForFlatRate.Count > 0)
                                        {
                                            bool isCalled = false;
                                            foreach (GetBasicRateCardCost getCostForFlatRate in lstGetCostForFlatRate)
                                            {
                                                if (isCalled == false)
                                                {
                                                    if (Math.Round(Convert.ToDecimal(getCostForFlatRate.RateCardCost), 2) == Math.Round(Convert.ToDecimal(baseRateCardCost), 2))
                                                    {
                                                        isCalled = true;
                                                        if (getCostForFlatRate.RateCardDetailId > 0)
                                                        {
                                                            objPostMOM.RateCardDetailId = getCostForFlatRate.RateCardDetailId;
                                                        }
                                                        else
                                                        {
                                                            string cError = "AdRateCardDetailId is not found";
                                                            CreateErrorOfDatatableForImportOrder(ref dtError, "validation", "", cError);
                                                            isGetCost = false;
                                                            isValid = false;
                                                        }

                                                        decimal rateCardCost = Convert.ToDecimal(getCostForFlatRate.RateCardCost);
                                                        objPostMOM.BaseCalculationCost = Convert.ToDecimal(baseRateCardCost);
                                                        objPostMOM.BaseRateCardCost = Convert.ToDecimal(rateCardCost);
                                                        objPostMOM.RateCardCost = Convert.ToDecimal(adCost);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion

                        }
                        else
                        {
                            string cError = "Issue Date is not exits in database";
                            CreateErrorOfDatatableForImportOrder(ref dtError, "Issue Date", "", cError);
                            isValid = false;
                        }
                    }
                    else
                    {
                        string cError = "Rate Card Num is not exits in database";
                        CreateErrorOfDatatableForImportOrder(ref dtError, "Rate Card Num", "", cError);
                        isValid = false;
                    }
                    #endregion


                    if (objPostMOM.MediaOrderReps.Count() >= 1)
                    {
                        isRepsAndTerritoryValid = true;
                    }

                    if (isValid == true && isRepsAndTerritoryValid == true)
                    {
                        objPostMOM.IsImportOrder = true;
                        addOrderResponse = AddImportMediaOrder(objPostMOM);


                        string addMediaOrderJsonData = JsonConvert.SerializeObject(addOrderResponse.Data, Formatting.Indented);
                        AddMediaOrderResponse objAddNewMediaOrder = JsonConvert.DeserializeObject<AddMediaOrderResponse>(addMediaOrderJsonData);

                        if (addOrderResponse.StatusCode == 1)
                        {
                            int mIds = objAddNewMediaOrder.MediaOrderIds[0];
                            excelData["MediaOrderId"] = mIds;
                            mediaOrderId = mIds;
                            objResponse.StatusCode = ApiStatus.Ok;
                            objResponse.Data = mediaOrderId;

                            string insStatus = string.Empty;
                            string orderStatus = excelData["Canceled"].ToString();
                            if (orderStatus == "0")
                            {
                                insStatus = APIMediaOrderStatus.Run.ToString();
                            }
                            else if (orderStatus == "-1")
                            {
                                insStatus = APIMediaOrderStatus.Cancel.ToString();
                            }
                            else if (orderStatus == "2")
                            {
                                insStatus = APIMediaOrderStatus.Proposal.ToString();
                            }

                            ResponseCommon objStatusResponse = new ResponseCommon();
                            List<int> objMoD = new List<int>
                            {
                                mIds
                            };
                            MediaOrderChangeStatusModel objMChangeStatus = new MediaOrderChangeStatusModel();
                            objMChangeStatus.MediaOrderIds = objMoD;
                            objMChangeStatus.OrderStatus = insStatus;

                            objStatusResponse = ChangeStatus(objMChangeStatus);
                            if (objStatusResponse.StatusCode == 1)
                            {
                                objResponse.StatusCode = ApiStatus.Ok;
                                objResponse.Data = mediaOrderId;
                            }
                            else
                            {
                                objResponse.StatusCode = ApiStatus.Exception;
                                objResponse.Data = mediaOrderId;
                            }
                        }
                        else
                        {

                            objResponse.StatusCode = ApiStatus.Exception;
                            objResponse.Data = 0;
                        }
                    }
                    else
                    {
                        objResponse.StatusCode = ApiStatus.Exception;
                        objResponse.Data = 0;
                    }
                }
                else
                {
                    string cError = "MatPub and Publication both Media Asset Name is not same";
                    CreateErrorOfDatatableForImportOrder(ref dtError, "Publication", "", cError);
                }
            }

            catch (Exception ex)
            {

                string cError = "";
                CreateErrorOfDatatableForImportOrder(ref dtError, "", ex.Message, cError);
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Data = 0;
                objResponse.Message = ex.Message.ToString();
            }
            return objResponse;
        }

        private bool ValidationForMediaOrderLines(ref DataTable dtError, MediaAsset mResponseData, DataRow excelData, List<PartyModel> lstAsiPart, string mediaOrderId)
        {
            bool status = true;
            try
            {
                #region access Excel field
                string basicRateCardCost = excelData["RC Ad Cost"].ToString();
                string NDS1AC = excelData["NDS1 AC"].ToString();
                string LSD1AC = excelData["LSD1 AC"].ToString();
                string LPA1AC = excelData["LPA1 AC"].ToString();
                string DS1AC = excelData["DS1 AC"].ToString();
                string NDS2AC = excelData["NDS2 AC"].ToString();
                string LSD2AC = excelData["LSD2 AC"].ToString();
                string LPA2AC = excelData["LPA2 AC"].ToString();
                string DS2AC = excelData["DS2 AC"].ToString();
                string NDS3AC = excelData["NDS3 AC"].ToString();
                string LSD3AC = excelData["LSD3 AC"].ToString();
                string LPA3AC = excelData["LPA3 AC"].ToString();
                string DS3AC = excelData["DS3 AC"].ToString();
                string NDS4AC = excelData["NDS4 AC"].ToString();
                string LSD4AC = excelData["LSD4 AC"].ToString();
                string LPA4AC = excelData["LPA4 AC"].ToString();
                string DS4AC = excelData["DS4 AC"].ToString();
                string NDS5AC = excelData["NDS5 AC"].ToString();
                string LSD5AC = excelData["LSD5 AC"].ToString();
                string LPA5AC = excelData["LPA5 AC"].ToString();
                string DS5AC = excelData["DS5 AC"].ToString();
                string NDS1GC = excelData["NDS1 GC"].ToString();
                string LSD1GC = excelData["LSD1 GC"].ToString();
                string LPA1GC = excelData["LPA1 GC"].ToString();
                string DS1GC = excelData["DS1 GC"].ToString();
                string NDS2GC = excelData["NDS2 GC"].ToString();
                string LSD2GC = excelData["LSD2 GC"].ToString();
                string LPA2GC = excelData["LPA2 GC"].ToString();
                string DS2GC = excelData["DS2 GC"].ToString();
                string NDS3GC = excelData["NDS3 GC"].ToString();
                string LSD3GC = excelData["LSD3 GC"].ToString();
                string LPA3GC = excelData["LPA3 GC"].ToString();
                string DS3GC = excelData["DS3 GC"].ToString();
                string NDS4GC = excelData["NDS4 GC"].ToString();
                string LSD4GC = excelData["LSD4 GC"].ToString();
                string LPA4GC = excelData["LPA4 GC"].ToString();
                string DS4GC = excelData["DS4 GC"].ToString();
                string NDS5GC = excelData["NDS5 GC"].ToString();
                string lsd5gc = excelData["lsd5 gc"].ToString();
                string LPA5GC = excelData["LPA5 GC"].ToString();
                string DS5GC = excelData["DS5 GC"].ToString();
                string NDS1NC = excelData["NDS1 NC"].ToString();
                string LSD1NC = excelData["LSD1 NC"].ToString();
                string LPA1NC = excelData["LPA1 NC"].ToString();
                string DS1NC = excelData["DS1 NC"].ToString();
                string NDS2NC = excelData["NDS2 NC"].ToString();
                string LSD2NC = excelData["LSD2 NC"].ToString();
                string LPA2NC = excelData["LPA2 NC"].ToString();
                string DS2NC = excelData["DS2 NC"].ToString();
                string NDS3NC = excelData["NDS3 NC"].ToString();
                string LSD3NC = excelData["LSD3 NC"].ToString();
                string LPA3NC = excelData["LPA3 NC"].ToString();
                string DS3NC = excelData["DS3 NC"].ToString();
                string NDS4NC = excelData["NDS4 NC"].ToString();
                string LSD4NC = excelData["LSD4 NC"].ToString();
                string LPA4NC = excelData["LPA4 NC"].ToString();
                string DS4NC = excelData["DS4 NC"].ToString();
                string NDS5NC = excelData["NDS5 NC"].ToString();
                string LSD5NC = excelData["LSD5 NC"].ToString();
                string LPA5NC = excelData["LPA5 NC"].ToString();
                string DS5NC = excelData["DS5 NC"].ToString();

                string netCost = excelData["Bill Cost"].ToString();

                string createdByExcel = excelData["_User"].ToString();
                string createdDate = excelData["_Date"].ToString();
                string createdTime = excelData["_Time"].ToString();

                #endregion

                string createdBy = string.Empty;
                DateTime dtNow = DateTime.Now;

                if (!string.IsNullOrEmpty(createdTime))
                {
                    DateTime? TimeTo = string.IsNullOrEmpty(createdTime.ToString()) ? (DateTime?)null : DateTime.Parse(createdTime.ToString());
                    DateTime createdDt = DateTime.ParseExact(createdDate + " " + TimeTo, "dd/MM/yy h:mm:ss tt", CultureInfo.InvariantCulture);
                    createdBy = createdByExcel;
                    dtNow = Convert.ToDateTime(createdDt);
                }
                else
                {
                    createdBy = createdByExcel;
                    dtNow = Convert.ToDateTime(createdDate);
                }

                ResponseCommon isMediaOrderLineSaved1AC = CreateObjectOfMediaOrderLinesForPost(ref dtError, mResponseData, mediaOrderId, NDS1AC, LSD1AC, LPA1AC, DS1AC, true, "NDS1 AC", "LSD1 AC", "LPA1 AC", "DS1 AC", basicRateCardCost, createdBy, dtNow);
                if (isMediaOrderLineSaved1AC != null)
                {
                    basicRateCardCost = isMediaOrderLineSaved1AC.Data.ToString();
                }
                if (isMediaOrderLineSaved1AC.Message.ToString() == "fail")
                {
                    status = false;
                }

                ResponseCommon isMediaOrderLineSaved2AC = CreateObjectOfMediaOrderLinesForPost(ref dtError, mResponseData, mediaOrderId, NDS2AC, LSD2AC, LPA2AC, DS2AC, true, "NDS2 AC", "LSD2 AC", "LPA2 AC", "DS2 AC", basicRateCardCost, createdBy, dtNow);
                if (isMediaOrderLineSaved1AC != null)
                {
                    basicRateCardCost = isMediaOrderLineSaved2AC.Data.ToString();
                }
                if (isMediaOrderLineSaved2AC.Message.ToString() == "fail")
                {
                    status = false;
                }

                ResponseCommon isMediaOrderLineSaved3AC = CreateObjectOfMediaOrderLinesForPost(ref dtError, mResponseData, mediaOrderId, NDS3AC, LSD3AC, LPA3AC, DS3AC, true, "NDS3 AC", "LSD3 AC", "LPA3 AC", "DS3 AC", basicRateCardCost, createdBy, dtNow);
                if (isMediaOrderLineSaved3AC != null)
                {
                    basicRateCardCost = isMediaOrderLineSaved3AC.Data.ToString();
                }
                if (isMediaOrderLineSaved3AC.Message.ToString() == "fail")
                {
                    status = false;
                }

                ResponseCommon isMediaOrderLineSaved4AC = CreateObjectOfMediaOrderLinesForPost(ref dtError, mResponseData, mediaOrderId, NDS4AC, LSD4AC, LPA4AC, DS4AC, true, "NDS4 AC", "LSD4 AC", "LPA4 AC", "DS4 AC", basicRateCardCost, createdBy, dtNow);
                if (isMediaOrderLineSaved4AC != null)
                {
                    basicRateCardCost = isMediaOrderLineSaved4AC.Data.ToString();
                }
                if (isMediaOrderLineSaved4AC.Message.ToString() == "fail")
                {
                    status = false;
                }

                ResponseCommon isMediaOrderLineSaved5AC = CreateObjectOfMediaOrderLinesForPost(ref dtError, mResponseData, mediaOrderId, NDS5AC, LSD5AC, LPA5AC, DS5AC, true, "NDS5 AC", "LSD5 AC", "LPA5 AC", "DS5 AC", basicRateCardCost, createdBy, dtNow);
                if (isMediaOrderLineSaved5AC != null)
                {
                    basicRateCardCost = isMediaOrderLineSaved5AC.Data.ToString();
                }
                if (isMediaOrderLineSaved5AC.Message.ToString() == "fail")
                {
                    status = false;
                }

                ResponseCommon isMediaOrderLineSaved1GC = CreateObjectOfMediaOrderLinesForPost(ref dtError, mResponseData, mediaOrderId, NDS1GC, LSD1GC, LPA1GC, DS1GC, true, "NDS1 GC", "LSD1 GC", "LPA1 GC", "DS1 GC", basicRateCardCost, createdBy, dtNow);
                if (isMediaOrderLineSaved1GC != null)
                {
                    basicRateCardCost = isMediaOrderLineSaved1GC.Data.ToString();
                }
                if (isMediaOrderLineSaved1GC.Message.ToString() == "fail")
                {
                    status = false;
                }

                ResponseCommon isMediaOrderLineSaved2GC = CreateObjectOfMediaOrderLinesForPost(ref dtError, mResponseData, mediaOrderId, NDS2GC, LSD2GC, LPA2GC, DS2GC, true, "NDS2 GC", "LSD2 GC", "LPA2 GC", "DS2 GC", basicRateCardCost, createdBy, dtNow);
                if (isMediaOrderLineSaved2GC != null)
                {
                    basicRateCardCost = isMediaOrderLineSaved2GC.Data.ToString();
                }
                if (isMediaOrderLineSaved2GC.Message.ToString() == "fail")
                {
                    status = false;
                }

                ResponseCommon isMediaOrderLineSaved3GC = CreateObjectOfMediaOrderLinesForPost(ref dtError, mResponseData, mediaOrderId, NDS3GC, LSD3GC, LPA3GC, DS3GC, true, "NDS3 GC", "LSD3 GC", "LPA3 GC", "DS3 GC", basicRateCardCost, createdBy, dtNow);
                if (isMediaOrderLineSaved3GC != null)
                {
                    basicRateCardCost = isMediaOrderLineSaved3GC.Data.ToString();
                }
                if (isMediaOrderLineSaved3GC.Message.ToString() == "fail")
                {
                    status = false;
                }

                ResponseCommon isMediaOrderLineSaved4GC = CreateObjectOfMediaOrderLinesForPost(ref dtError, mResponseData, mediaOrderId, NDS4GC, LSD4GC, LPA4GC, DS4GC, true, "NDS4 GC", "LSD4 GC", "LPA4 GC", "DS4 GC", basicRateCardCost, createdBy, dtNow);
                if (isMediaOrderLineSaved4GC != null)
                {
                    basicRateCardCost = isMediaOrderLineSaved4GC.Data.ToString();
                }
                if (isMediaOrderLineSaved4GC.Message.ToString() == "fail")
                {
                    status = false;
                }

                ResponseCommon isMediaOrderLineSaved5GC = CreateObjectOfMediaOrderLinesForPost(ref dtError, mResponseData, mediaOrderId, NDS5GC, lsd5gc, LPA5GC, DS5GC, true, "NDS5 GC", "lsd5 gc", "LPA5 GC", "DS5 GC", basicRateCardCost, createdBy, dtNow);
                if (isMediaOrderLineSaved5GC != null)
                {
                    basicRateCardCost = isMediaOrderLineSaved5GC.Data.ToString();
                }
                if (isMediaOrderLineSaved5GC.Message.ToString() == "fail")
                {
                    status = false;
                }

                ResponseCommon isMediaOrderLineSaved1NC = CreateObjectOfMediaOrderLinesForPost(ref dtError, mResponseData, mediaOrderId, NDS1NC, LSD1NC, LPA1NC, DS1NC, false, "NDS1 NC", "LSD1 NC", "LPA1 NC", "DS1 NC", basicRateCardCost, createdBy, dtNow);
                if (isMediaOrderLineSaved1NC != null)
                {
                    basicRateCardCost = isMediaOrderLineSaved1NC.Data.ToString();
                }
                if (isMediaOrderLineSaved1NC.Message.ToString() == "fail")
                {
                    status = false;
                }

                ResponseCommon isMediaOrderLineSaved2NC = CreateObjectOfMediaOrderLinesForPost(ref dtError, mResponseData, mediaOrderId, NDS2NC, LSD2NC, LPA2NC, DS2NC, false, "NDS2 NC", "LSD2 NC", "LPA2 NC", "DS2 NC", basicRateCardCost, createdBy, dtNow);
                if (isMediaOrderLineSaved2NC != null)
                {
                    basicRateCardCost = isMediaOrderLineSaved2NC.Data.ToString();
                }
                if (isMediaOrderLineSaved2NC.Message.ToString() == "fail")
                {
                    status = false;
                }

                ResponseCommon isMediaOrderLineSaved3NC = CreateObjectOfMediaOrderLinesForPost(ref dtError, mResponseData, mediaOrderId, NDS3NC, LSD3NC, LPA3NC, DS3NC, false, "NDS3 NC", "LSD3 NC", "LPA3 NC", "DS3 NC", basicRateCardCost, createdBy, dtNow);
                if (isMediaOrderLineSaved3NC != null)
                {
                    basicRateCardCost = isMediaOrderLineSaved3NC.Data.ToString();
                }
                if (isMediaOrderLineSaved3NC.Message.ToString() == "fail")
                {
                    status = false;
                }

                ResponseCommon isMediaOrderLineSaved4NC = CreateObjectOfMediaOrderLinesForPost(ref dtError, mResponseData, mediaOrderId, NDS4NC, LSD4NC, LPA4NC, DS4NC, false, "NDS4 NC", "LSD4 NC", "LPA4 NC", "DS4 NC", basicRateCardCost, createdBy, dtNow);
                if (isMediaOrderLineSaved4NC != null)
                {
                    basicRateCardCost = isMediaOrderLineSaved4NC.Data.ToString();
                }
                if (isMediaOrderLineSaved4NC.Message.ToString() == "fail")
                {
                    status = false;
                }

                ResponseCommon isMediaOrderLineSaved5NC = CreateObjectOfMediaOrderLinesForPost(ref dtError, mResponseData, mediaOrderId, NDS5NC, LSD5NC, LPA5NC, DS5NC, false, "NDS5 NC", "LSD5 NC", "LPA5 NC", "DS5 NC", basicRateCardCost, createdBy, dtNow);
                if (isMediaOrderLineSaved5NC != null)
                {
                    basicRateCardCost = isMediaOrderLineSaved5NC.Data.ToString();
                }
                if (isMediaOrderLineSaved5NC.Message.ToString() == "fail")
                {
                    status = false;
                }

                int ids = Convert.ToInt32(mediaOrderId);
                MediaOrder order = DB.MediaOrders.Where(x => x.MediaOrderId == ids /*&& x.DeletedInd != true*/).FirstOrDefault();
                if (order != null)
                {
                    order.NetCost = Convert.ToDecimal(netCost);
                    //db.Entry(order).State = EntityState.Modified;
                    //db.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                string cError = "";
                CreateErrorOfDatatableForImportOrder(ref dtError, "", ex.Message, cError);
            }
            return status;
        }

        private ResponseCommon CreateObjectOfMediaOrderLinesForPost(ref DataTable dtError, MediaAsset mResponseData, string mediaOrderId, string adAdjustmentName, string surchargeDiscount, string amountPercent, string adjustmentValue, bool grossInd, string adAdjustmentNameField, string amountPercentField, string surchargeDiscountField, string adjustmentValueField, string basicRateCardCost, string createdBy, DateTime createdDate)
        {
            ResponseCommon addOrderLinesResponse = new ResponseCommon();
            string status = "noError";
            double lastAdjustmentValue = 0;
            lastAdjustmentValue = Convert.ToDouble(basicRateCardCost);
            try
            {
                short amtPercent = 0;
                short surchgDiscount = 0;
                short amountInPer = Convert.ToInt16(amountPercent);
                short surDis = Convert.ToInt16(surchargeDiscount);
                if (amountInPer == 1)
                {
                    amtPercent = 0;
                }
                else
                {
                    amtPercent = 1;
                }
                surchgDiscount = surDis;

                List<AdAdjustment> lstAdAdjustments = mResponseData.AdAdjustments.Where(x => (x.AdAdjustmentName.Trim().ToLower() == adAdjustmentName.Trim().ToLower() && x.AmountPercent == amtPercent && x.SurchargeDiscount == surchgDiscount)).ToList();
                if (lstAdAdjustments.Count() > 0)
                {
                    foreach (AdAdjustment adAdjustments in lstAdAdjustments)
                    {

                        bool isValid = true;

                        PostImportMediaOrderLineModel obj = new PostImportMediaOrderLineModel();
                        List<int> lstMediaOrderIds = new List<int>();
                        if (!string.IsNullOrEmpty(mediaOrderId))
                        {
                            int moId = Convert.ToInt32(mediaOrderId);
                            if (moId > 0)
                            {
                                lstMediaOrderIds.Add(moId);
                                obj.MediaOrderIds = lstMediaOrderIds;

                                if (!string.IsNullOrEmpty(adAdjustments.AdAdjustmentName))
                                {
                                    obj.ProductCode = adAdjustments.ProductCode;
                                    obj.AdAdjustmentName = adAdjustments.AdAdjustmentName;
                                    obj.CreatedBy = createdBy;
                                    obj.CreatedDate = createdDate;

                                    short aPer = Convert.ToInt16(adAdjustments.AmountPercent);
                                    short surChgDis = Convert.ToInt16(adAdjustments.SurchargeDiscount);
                                    double adValue = Convert.ToDouble(adAdjustments.AdjustmentValue);

                                    short eAmountPercent = Convert.ToInt16(amountPercent);
                                    var isAmtPercentExits = eAmountPercent == aPer;
                                    if (isAmtPercentExits != true)
                                    {

                                        obj.AmountPercent = adAdjustments.AmountPercent;
                                        string cError = "Excel AmountPercent and database AmountPercent does not match";
                                        CreateErrorOfDatatableForImportOrder(ref dtError, amountPercentField, "", cError);

                                    }
                                    else
                                    {
                                        short AmPer = Convert.ToInt16(amountPercent);
                                        if (AmPer == 1)
                                        {
                                            obj.AmountPercent = 0;
                                        }
                                        else
                                        {
                                            obj.AmountPercent = 1;
                                        }

                                    }

                                    short eSurchargeDiscount = Convert.ToInt16(surchargeDiscount);
                                    var isSurchargeDiscountExists = eSurchargeDiscount == surChgDis;
                                    if (isSurchargeDiscountExists != true)
                                    {
                                        // for now temporaryy
                                        short SurDis = Convert.ToInt16(surchargeDiscount);
                                        obj.SurchargeDiscount = SurDis;
                                        string cError = "Excel SurchargeDiscount and Database SurchargeDiscount does not match";
                                        CreateErrorOfDatatableForImportOrder(ref dtError, surchargeDiscountField, "", cError);
                                        //isValid = false;
                                    }
                                    else
                                    {
                                        obj.SurchargeDiscount = adAdjustments.SurchargeDiscount;
                                    }

                                    decimal basicCost = 0;
                                    if (!string.IsNullOrEmpty(basicRateCardCost))
                                    {
                                        basicCost = Convert.ToDecimal(basicRateCardCost);
                                    }


                                    if (grossInd == true || grossInd == false)
                                    {
                                        obj.GrossInd = grossInd;
                                        #region GrossCost Calculation
                                        // for grosscost calculation 
                                        double adAdjustmentValue = 0;
                                        double adAdjustmentAmount = 0;
                                        if (isValid == true)
                                        {
                                            // add new discount/surcharges
                                            // amount and percentage is different for our system and excel import
                                            if (obj.AmountPercent == 0)
                                            {
                                                //Amount 
                                                if (obj.SurchargeDiscount == 0)
                                                {
                                                    // Surcharge
                                                    adAdjustmentValue = Convert.ToDouble(adAdjustmentValue);
                                                    adAdjustmentAmount = Convert.ToDouble(adAdjustmentValue);
                                                    obj.AdjustmentValue = adAdjustmentValue;
                                                    obj.AdjustmentAmount = adAdjustmentAmount;
                                                    lastAdjustmentValue = Convert.ToDouble(basicCost + Convert.ToDecimal(obj.AdjustmentAmount));
                                                }
                                                else
                                                {
                                                    // Discount
                                                    adAdjustmentValue = Convert.ToDouble(adAdjustmentValue);
                                                    adAdjustmentAmount = Convert.ToDouble(adAdjustmentValue);
                                                    obj.AdjustmentValue = adAdjustmentValue;
                                                    obj.AdjustmentAmount = adAdjustmentAmount;
                                                    lastAdjustmentValue = Convert.ToDouble(basicCost - Convert.ToDecimal(obj.AdjustmentAmount));
                                                }
                                            }
                                            else
                                            {
                                                double adjustmentPercentage = 0;
                                                //decimal adjustmentValue = 0;
                                                //Percentage
                                                if (obj.SurchargeDiscount == 0)
                                                {

                                                    // Surcharge
                                                    adjustmentPercentage = Convert.ToDouble(adAdjustmentValue) * 100;
                                                    adAdjustmentAmount = Math.Round(adjustmentPercentage * Convert.ToDouble(basicCost) / 100);
                                                    lastAdjustmentValue = Convert.ToDouble(basicCost + Convert.ToDecimal(adAdjustmentAmount));
                                                    obj.AdjustmentValue = Convert.ToDouble(adjustmentPercentage);
                                                    obj.AdjustmentAmount = Convert.ToDouble(adAdjustmentAmount);
                                                }
                                                else
                                                {


                                                    // Discount
                                                    adjustmentPercentage = Convert.ToDouble(adAdjustmentValue) * 100;
                                                    adAdjustmentAmount = Math.Round(adjustmentPercentage * Convert.ToDouble(basicCost) / 100);
                                                    lastAdjustmentValue = Convert.ToDouble(basicCost - Convert.ToDecimal(adAdjustmentAmount));
                                                    obj.AdjustmentValue = Convert.ToDouble(adjustmentPercentage);
                                                    obj.AdjustmentAmount = Convert.ToDouble(adAdjustmentAmount);
                                                }
                                            }
                                        }



                                        addOrderLinesResponse = _IMediaOrderLinesRepository.AddImportMediaOrderLine(obj);
                                        if (addOrderLinesResponse.StatusCode == 1)
                                        {
                                            status = "success";
                                            addOrderLinesResponse.StatusCode = ApiStatus.Ok;
                                            addOrderLinesResponse.Message = status;
                                            addOrderLinesResponse.Data = lastAdjustmentValue;
                                        }
                                        else
                                        {
                                            status = "fail";
                                            addOrderLinesResponse.StatusCode = ApiStatus.Exception;
                                            addOrderLinesResponse.Message = status;
                                            addOrderLinesResponse.Data = lastAdjustmentValue;
                                        }
                                        #endregion
                                    }

                                }
                                else
                                {
                                    string cError = "AdAdjustmentName is empty";
                                    CreateErrorOfDatatableForImportOrder(ref dtError, adAdjustmentNameField, "", cError);
                                    isValid = false;
                                    addOrderLinesResponse.StatusCode = ApiStatus.Exception;
                                    addOrderLinesResponse.Message = status;
                                    addOrderLinesResponse.Data = lastAdjustmentValue;
                                }
                            }
                            else
                            {
                                status = "fail";
                                string cError = "MediaOrderId is empty";
                                CreateErrorOfDatatableForImportOrder(ref dtError, "MediaOrderID", "", cError);
                                isValid = false;
                                addOrderLinesResponse.StatusCode = ApiStatus.Exception;
                                addOrderLinesResponse.Message = status;
                                addOrderLinesResponse.Data = lastAdjustmentValue;
                            }
                        }
                        else
                        {
                            status = "fail";
                            string cError = "MediaOrderId is empty";
                            CreateErrorOfDatatableForImportOrder(ref dtError, "MediaOrderID", "", cError);
                            addOrderLinesResponse.StatusCode = ApiStatus.Exception;
                            addOrderLinesResponse.Message = status;
                            addOrderLinesResponse.Data = lastAdjustmentValue;
                        }

                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(adAdjustmentName.Trim()))
                    {
                        List<AdAdjustment> noOfAdAdjustments = mResponseData.AdAdjustments.Where(x => (x.AdAdjustmentName.Trim().ToLower() == adAdjustmentName.Trim().ToLower())).ToList();
                        if (noOfAdAdjustments.Count() < 1)
                        {
                            PostAdAdjustmentModel objadAdjustment = new PostAdAdjustmentModel();
                            int mId = mResponseData.MediaAssetId;
                            objadAdjustment.MediaAssetId = mId;
                            objadAdjustment.ProductCode = "ADVSALE";
                            objadAdjustment.Name = adAdjustmentName.Trim();
                            short AmPer = Convert.ToInt16(amountPercent);
                            if (AmPer == 1)
                            {
                                objadAdjustment.AmountPercent = 0;
                            }
                            else
                            {
                                objadAdjustment.AmountPercent = 1;
                            }
                            short eSurchargeDiscount = Convert.ToInt16(surchargeDiscount);
                            objadAdjustment.SurchargeDiscount = eSurchargeDiscount;
                            objadAdjustment.AdjustmentTarget = "Running Total";
                            objadAdjustment.AdjustmentValue = Convert.ToDouble(0);

                            addOrderLinesResponse = _IAdAdjustmentRepository.PostAdAdjustment(objadAdjustment);
                            if (addOrderLinesResponse.StatusCode == 1)
                            {

                                AdAdjustment adAdjustment = DB.AdAdjustments.Where(x => (x.AdAdjustmentName.Trim().ToLower() == adAdjustmentName.Trim().ToLower() && x.MediaAssetId == mId)).FirstOrDefault();
                                if (adAdjustment != null)
                                {
                                    bool isValid = true;

                                    PostImportMediaOrderLineModel obj = new PostImportMediaOrderLineModel();
                                    List<int> lstMediaOrderIds = new List<int>();
                                    if (!string.IsNullOrEmpty(mediaOrderId))
                                    {
                                        int moId = Convert.ToInt32(mediaOrderId);
                                        if (moId > 0)
                                        {
                                            lstMediaOrderIds.Add(moId);
                                            obj.MediaOrderIds = lstMediaOrderIds;

                                            if (!string.IsNullOrEmpty(adAdjustment.AdAdjustmentName))
                                            {
                                                obj.ProductCode = adAdjustment.ProductCode;
                                                obj.AdAdjustmentName = adAdjustment.AdAdjustmentName;
                                                obj.CreatedBy = createdBy;
                                                obj.CreatedDate = createdDate;

                                                short aPer = Convert.ToInt16(adAdjustment.AmountPercent);
                                                short surChgDis = Convert.ToInt16(adAdjustment.SurchargeDiscount);
                                                double adValue = Convert.ToDouble(adAdjustment.AdjustmentValue);

                                                short eAmountPercent = Convert.ToInt16(amountPercent);
                                                var isAPExits = eAmountPercent == aPer;
                                                if (isAPExits != true)
                                                {

                                                    obj.AmountPercent = adAdjustment.AmountPercent;
                                                    string cError = "Excel AmountPercent and database AmountPercent does not match";
                                                    CreateErrorOfDatatableForImportOrder(ref dtError, amountPercentField, "", cError);

                                                }
                                                else
                                                {
                                                    short amtPer = Convert.ToInt16(amountPercent);
                                                    if (amtPer == 1)
                                                    {
                                                        obj.AmountPercent = 0;
                                                    }
                                                    else
                                                    {
                                                        obj.AmountPercent = 1;
                                                    }

                                                }

                                                short excelSurchargeDiscount = Convert.ToInt16(surchargeDiscount);
                                                var isSDExits = excelSurchargeDiscount == surChgDis;
                                                if (isSDExits != true)
                                                {
                                                    // for now temporaryy
                                                    short surDisc = Convert.ToInt16(surchargeDiscount);
                                                    obj.SurchargeDiscount = surDisc;
                                                    string cError = "Excel SurchargeDiscount and database SurchargeDiscount does not match";
                                                    CreateErrorOfDatatableForImportOrder(ref dtError, surchargeDiscountField, "", cError);
                                                    // isValid = false;
                                                }
                                                else
                                                {
                                                    obj.SurchargeDiscount = adAdjustment.SurchargeDiscount;
                                                }

                                                decimal basicCost = 0;
                                                if (!string.IsNullOrEmpty(basicRateCardCost))
                                                {
                                                    basicCost = Convert.ToDecimal(basicRateCardCost);
                                                }

                                                if (grossInd == true || grossInd == false)
                                                {
                                                    obj.GrossInd = grossInd;
                                                    #region GrossCost Calculation
                                                    // for grosscost calculation 
                                                    double adAdjustmentValue = 0;
                                                    double adjustmentAmount = 0;
                                                    if (isValid == true)
                                                    {
                                                        // add new discount/surcharges
                                                        // amount and percentage is different for our system and excel import
                                                        if (obj.AmountPercent == 0)
                                                        {
                                                            //Amount 
                                                            if (obj.SurchargeDiscount == 0)
                                                            {
                                                                // Surcharge
                                                                adAdjustmentValue = Convert.ToDouble(adAdjustmentValue);
                                                                adjustmentAmount = Convert.ToDouble(adAdjustmentValue);
                                                                obj.AdjustmentValue = adAdjustmentValue;
                                                                obj.AdjustmentAmount = adjustmentAmount;
                                                                lastAdjustmentValue = Convert.ToDouble(basicCost + Convert.ToDecimal(obj.AdjustmentAmount));
                                                            }
                                                            else
                                                            {
                                                                // Discount
                                                                adAdjustmentValue = Convert.ToDouble(adAdjustmentValue);
                                                                adjustmentAmount = Convert.ToDouble(adAdjustmentValue);
                                                                obj.AdjustmentValue = adAdjustmentValue;
                                                                obj.AdjustmentAmount = adjustmentAmount;
                                                                lastAdjustmentValue = Convert.ToDouble(basicCost - Convert.ToDecimal(obj.AdjustmentAmount));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            double adjustmentPercentage = 0;
                                                            //decimal adjustmentValue = 0;
                                                            //Percentage
                                                            if (obj.SurchargeDiscount == 0)
                                                            {

                                                                // Surcharge
                                                                adjustmentPercentage = Convert.ToDouble(adAdjustmentValue) * 100;
                                                                adjustmentAmount = Math.Round(adjustmentPercentage * Convert.ToDouble(basicCost) / 100);
                                                                lastAdjustmentValue = Convert.ToDouble(basicCost + Convert.ToDecimal(adjustmentAmount));
                                                                obj.AdjustmentValue = Convert.ToDouble(adjustmentPercentage);
                                                                obj.AdjustmentAmount = Convert.ToDouble(adjustmentAmount);
                                                            }
                                                            else
                                                            {

                                                                // Discount
                                                                adjustmentPercentage = Convert.ToDouble(adAdjustmentValue) * 100;
                                                                adjustmentAmount = Math.Round(adjustmentPercentage * Convert.ToDouble(basicCost) / 100);
                                                                lastAdjustmentValue = Convert.ToDouble(basicCost - Convert.ToDecimal(adjustmentAmount));
                                                                obj.AdjustmentValue = Convert.ToDouble(adjustmentPercentage);
                                                                obj.AdjustmentAmount = Convert.ToDouble(adjustmentAmount);
                                                            }
                                                        }
                                                    }

                                                    // addOrderLinesResponse = _IMediaOrderLinesRepository.AddMediaOrderLine(obj);
                                                    addOrderLinesResponse = _IMediaOrderLinesRepository.AddImportMediaOrderLine(obj);
                                                    if (addOrderLinesResponse.StatusCode == 1)
                                                    {
                                                        status = "success";
                                                        addOrderLinesResponse.StatusCode = ApiStatus.Ok;
                                                        addOrderLinesResponse.Message = status;
                                                        addOrderLinesResponse.Data = lastAdjustmentValue;
                                                    }
                                                    else
                                                    {
                                                        status = "fail";
                                                        addOrderLinesResponse.StatusCode = ApiStatus.Exception;
                                                        addOrderLinesResponse.Message = status;
                                                        addOrderLinesResponse.Data = lastAdjustmentValue;
                                                    }
                                                    #endregion
                                                }

                                            }
                                            else
                                            {
                                                string cError = "AdAdjustmentName is empty";
                                                CreateErrorOfDatatableForImportOrder(ref dtError, adAdjustmentNameField, "", cError);
                                                isValid = false;
                                                addOrderLinesResponse.StatusCode = ApiStatus.Exception;
                                                addOrderLinesResponse.Message = status;
                                                addOrderLinesResponse.Data = lastAdjustmentValue;
                                            }
                                        }
                                        else
                                        {
                                            status = "fail";
                                            string cError = "MediaOrderId is empty";
                                            CreateErrorOfDatatableForImportOrder(ref dtError, "MediaOrderID", "", cError);
                                            isValid = false;
                                            addOrderLinesResponse.StatusCode = ApiStatus.Exception;
                                            addOrderLinesResponse.Message = status;
                                            addOrderLinesResponse.Data = lastAdjustmentValue;
                                        }
                                    }
                                    else
                                    {
                                        status = "fail";
                                        string cError = "MediaOrderId is empty";
                                        CreateErrorOfDatatableForImportOrder(ref dtError, "MediaOrderID", "", cError);
                                        addOrderLinesResponse.StatusCode = ApiStatus.Exception;
                                        addOrderLinesResponse.Message = status;
                                        addOrderLinesResponse.Data = lastAdjustmentValue;
                                    }
                                }
                            }
                            else
                            {
                                status = "fail";
                                addOrderLinesResponse.StatusCode = ApiStatus.Exception;
                                addOrderLinesResponse.Message = status;
                                addOrderLinesResponse.Data = lastAdjustmentValue;
                            }
                        }
                        else
                        {
                            status = "noError";
                            addOrderLinesResponse.StatusCode = ApiStatus.Exception;
                            addOrderLinesResponse.Message = status;
                            addOrderLinesResponse.Data = lastAdjustmentValue;
                            string cError = "Duplicate Adjustment Name is not allowed for MediaAssets";
                            CreateErrorOfDatatableForImportOrder(ref dtError, "NDS1AC", "", cError);
                        }
                    }
                    else
                    {
                        status = "noError";
                        addOrderLinesResponse.StatusCode = ApiStatus.Exception;
                        addOrderLinesResponse.Message = status;
                        addOrderLinesResponse.Data = lastAdjustmentValue;
                    }
                }
            }
            catch (Exception ex)
            {
                status = "fail";
                string cError = "";
                CreateErrorOfDatatableForImportOrder(ref dtError, "", ex.Message.ToString(), cError);
                addOrderLinesResponse.StatusCode = ApiStatus.Exception;
                addOrderLinesResponse.Message = status;
                addOrderLinesResponse.Data = lastAdjustmentValue;
            }
            return addOrderLinesResponse;
        }

        private ResponseCommon ValidationForMediaOrderProduction(ref DataTable dtError, MediaAsset mResponseData, DataRow excelData, List<PartyModel> lstAsiPart, string mediaOrderId)
        {
            string status = "false";
            ResponseCommon response = new ResponseCommon();
            ResponseCommon medioOrderProductionResponse = new ResponseCommon();
            try
            {

                bool isValid = true;
                string headline = excelData["Headline"].ToString();
                string matOnHand = excelData["MatOnHand"].ToString();
                string materials = excelData["Materials"].ToString();
                string matExpected = excelData["MatExpected"].ToString();
                string matTrackNum = excelData["MatTrackNum"].ToString();
                string matPub = excelData["MatPub"].ToString();
                string matFromPage = excelData["matFromPage"].ToString();
                string matChanges = excelData["MatChanges"].ToString();
                string lastRanDate = excelData["LastRanDate"].ToString();
                string newMaterialsDate = excelData["NewMaterialsDate"].ToString();
                string position = excelData["Position"].ToString();
                string separation = excelData["Separation"].ToString();
                string comments = excelData["Comments"].ToString();
                string pageNum = excelData["PageNum"].ToString();
                string materialsContactID = excelData["MaterialsContactID"].ToString();
                string prodStatusId = excelData["ProdStatusId"].ToString();
                string urlAddress = excelData["URLAddress"].ToString();
                string tearSheetCount = excelData["TearSheetCount"].ToString();
                string pdfFile = excelData["PDF_File"].ToString();
                string proofFile = excelData["ProofFile"].ToString();
                string finalFile = excelData["FinalFile"].ToString();
                string prodComplete = excelData["ProdComplete"].ToString();
                string materialContactId = excelData["MaterialsContactID"].ToString();

                string createdByExcel = excelData["_User"].ToString();
                string createdDateExcel = excelData["_Date"].ToString();
                string createdTimeExcel = excelData["_Time"].ToString();


                PostImportMediaOrderProductionDetailModel objProductionModel = new PostImportMediaOrderProductionDetailModel();
                List<int> lstMediaOrderIds = new List<int>();
                if (!string.IsNullOrEmpty(mediaOrderId))
                {
                    int moId = Convert.ToInt32(mediaOrderId);
                    if (moId > 0)
                    {
                        lstMediaOrderIds.Add(moId);
                        objProductionModel.MediaOrderIds = lstMediaOrderIds;

                        if (!string.IsNullOrEmpty(prodComplete))
                        {
                            bool isComplete = Convert.ToBoolean(prodComplete);
                            if (isComplete == true)
                            {
                                objProductionModel.Completed = true;
                            }
                            else
                            {
                                objProductionModel.Completed = false;
                            }
                        }
                        else
                        {
                            objProductionModel.Completed = false;
                        }

                        var isPDFFileExist = System.IO.File.Exists(pdfFile.Trim());
                        if (isPDFFileExist)
                        {
                            byte[] bytes = System.Text.ASCIIEncoding.ASCII.GetBytes("@" + pdfFile.Trim());
                            string base64String = System.Convert.ToBase64String(bytes);
                            // string myFilePath = @"C:\MyFile.txt";
                            string ext = Path.GetExtension("@" + pdfFile.Trim());
                            objProductionModel.OriginalFile = base64String;
                            objProductionModel.OriginalFileExtension = ext;
                        }
                        else
                        {
                            string errorMessage = "PDF File is not Exit on this path";
                            CreateErrorOfDatatableForImportOrder(ref dtError, "PDF_File", "", errorMessage);
                        }

                        var isProofFileExist = System.IO.File.Exists(proofFile.Trim());
                        if (isProofFileExist)
                        {
                            byte[] bytes = System.Text.ASCIIEncoding.ASCII.GetBytes("@" + proofFile.Trim());
                            string base64String = System.Convert.ToBase64String(bytes);
                            string ext = Path.GetExtension("@" + proofFile.Trim());
                            objProductionModel.ProofFile = base64String;
                            objProductionModel.ProofFileExtension = ext;
                        }
                        else
                        {
                            string errorMessage = "Proof File does not exist on this path";
                            CreateErrorOfDatatableForImportOrder(ref dtError, "ProofFile", "", errorMessage);
                        }

                        var isFinalFileExist = System.IO.File.Exists(finalFile.Trim());
                        if (isFinalFileExist)
                        {
                            byte[] bytes = System.Text.ASCIIEncoding.ASCII.GetBytes("@" + finalFile.Trim());
                            string base64String = System.Convert.ToBase64String(bytes);
                            string ext = Path.GetExtension("@" + finalFile.Trim());
                            objProductionModel.FinalFile = base64String;
                            objProductionModel.FinalFileExtension = ext;
                        }
                        else
                        {
                            string errorMessage = "Final File does not exist on this path";
                            CreateErrorOfDatatableForImportOrder(ref dtError, "FinalFile", "", errorMessage);
                        }

                        int positionId = 0;
                        // FOCUS47 CODE BEGIN
                        if (!string.IsNullOrEmpty(position))
                        {
                            var isPositionExist = DB.Positions.Where(x => x.PositionName.Trim().ToLower() == position.Trim().ToLower()).FirstOrDefault();
                            if (isPositionExist == null)
                            {
                                isPositionExist = new Position
                                {
                                    PositionName = position.Trim(),
                                    CreatedOn = DateTime.Now,
                                    //DeletedInd = false,

                                    UpdatedOn = DateTime.Now,
                                    CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                    UpdatedByUserKey = Constants.Created_Update_ByUserKey
                                };
                                DB.Positions.Add(isPositionExist);
                                DB.SaveChanges();
                            }

                            if (isPositionExist.PositionId != 0)
                            {
                                positionId = isPositionExist.PositionId;
                            }
                            else
                            {
                                string errorMessage = "Position Name is not Present in Database";
                                CreateErrorOfDatatableForImportOrder(ref dtError, "Position", "", errorMessage);
                            }
                        }
                        // FOCUS47 CODE END

                        // FOCUS47 CODE BEGIN
                        int separationId = 0;
                        if (!string.IsNullOrEmpty(separation))
                        {
                            var isSeparationExist = DB.Separations.Where(x => x.SeparationName.Trim().ToLower() == separation.Trim().ToLower()).FirstOrDefault();
                            if (isSeparationExist == null)
                            {
                                isSeparationExist = new Separation
                                {
                                    SeparationName = separation.Trim(),
                                    CreatedOn = DateTime.Now,
                                    //DeletedInd = false,

                                    UpdatedOn = DateTime.Now,
                                    CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                    UpdatedByUserKey = Constants.Created_Update_ByUserKey
                                };
                                DB.Separations.Add(isSeparationExist);
                                DB.SaveChanges();
                            }

                            if (isSeparationExist != null)
                            {
                                separationId = isSeparationExist.SeparationId;
                            }
                            else
                            {
                                string errorMessage = "Separation Name is not Present in Database";
                                CreateErrorOfDatatableForImportOrder(ref dtError, "Separation", "", errorMessage);
                            }
                        }
                        // FOCUS47 CODE END

                        // FOCUS47 CODE BEGIN
                        if (!string.IsNullOrEmpty(prodStatusId))
                        {
                            var isStatusExist = DB.ProductionStatuses.Where(x => x.ProductionStatusName.Trim() == prodStatusId.Trim()).FirstOrDefault();
                            if (isStatusExist == null)
                            {
                                isStatusExist = new ProductionStatu();
                                isStatusExist.ProductionStatusName = prodStatusId.Trim();
                                isStatusExist.CreatedOn = DateTime.Now;
                                //isStatusExist.DeletedInd = false;

                                isStatusExist.UpdatedOn = DateTime.Now;
                                isStatusExist.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                                isStatusExist.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                DB.ProductionStatuses.Add(isStatusExist);
                                DB.SaveChanges();
                            }

                            if (isStatusExist != null)
                            {
                                int statusId = isStatusExist.ProductionStatusId;
                                objProductionModel.ProductionStatusId = statusId;
                            }
                            else
                            {
                                string errorMessage = "ProductionStatus Name is not present in Database";
                                CreateErrorOfDatatableForImportOrder(ref dtError, "ProdStatusId", "", errorMessage);
                            }
                        }

                        // FOCUS47 CODE END
                        if (Convert.ToBoolean(materials) == true)
                        {
                            // New Salection
                            bool newPickupInd = Convert.ToBoolean(materials);
                            objProductionModel.NewPickupInd = newPickupInd;

                            bool createExpectedInd = Convert.ToBoolean(matExpected);
                            objProductionModel.CreateExpectedInd = createExpectedInd;

                            bool onHandInd = Convert.ToBoolean(matOnHand);
                            objProductionModel.OnHandInd = onHandInd;

                            string TrackingNumber = matTrackNum.ToString();
                            objProductionModel.TrackingNumber = TrackingNumber;

                            if (!string.IsNullOrEmpty(newMaterialsDate))
                            {
                                DateTime materialExpectedDate = Convert.ToDateTime(newMaterialsDate);
                                objProductionModel.MaterialExpectedDate = materialExpectedDate;
                            }
                        }
                        else
                        {
                            // Pickup selection
                            bool newPickupInd = Convert.ToBoolean(materials);
                            objProductionModel.NewPickupInd = newPickupInd;

                            bool changesInd = Convert.ToBoolean(matChanges);
                            objProductionModel.ChangesInd = changesInd;
                            if (!string.IsNullOrEmpty(matFromPage))
                            {
                                int pickupMediaOrderId = Convert.ToInt32(matFromPage);
                                objProductionModel.PickupMediaOrderId = pickupMediaOrderId;
                            }
                            if (!string.IsNullOrEmpty(materialContactId))
                            {
                                int productionMaterialContactId = Convert.ToInt32(materialContactId);
                                objProductionModel.MaterialContactId = Convert.ToInt32(materialContactId);
                            }

                        }

                        objProductionModel.WebAdUrl = urlAddress;
                        objProductionModel.TearSheets = Convert.ToInt32(tearSheetCount);
                        objProductionModel.PositionId = positionId;
                        objProductionModel.SeparationId = separationId;
                        objProductionModel.PageNumber = pageNum;
                        objProductionModel.ProductionComment = comments;
                        objProductionModel.Completed = Convert.ToBoolean(prodComplete);



                        if (!string.IsNullOrEmpty(createdTimeExcel))
                        {
                            DateTime? toTime = string.IsNullOrEmpty(createdTimeExcel.ToString()) ? (DateTime?)null : DateTime.Parse(createdTimeExcel.ToString());
                            DateTime createdDate = DateTime.ParseExact(createdDateExcel + " " + toTime, "dd/MM/yy h:mm:ss tt", CultureInfo.InvariantCulture);
                            objProductionModel.CreatedBy = createdByExcel;
                            objProductionModel.CreatedDate = Convert.ToDateTime(createdDate);
                            objProductionModel.CreatedTime = createdTimeExcel;
                        }
                        else
                        {
                            objProductionModel.CreatedBy = createdByExcel;
                            objProductionModel.CreatedDate = Convert.ToDateTime(createdDateExcel);
                            objProductionModel.CreatedTime = createdTimeExcel;
                        }


                        if (!string.IsNullOrEmpty(objProductionModel.OriginalFile) || !string.IsNullOrEmpty(objProductionModel.OriginalFileExtension) || !string.IsNullOrEmpty(objProductionModel.ProofFile) || !string.IsNullOrEmpty(objProductionModel.ProofFileExtension) || !string.IsNullOrEmpty(objProductionModel.FinalFile) || !string.IsNullOrEmpty(objProductionModel.FinalFileExtension)
                            || !string.IsNullOrEmpty(objProductionModel.WebAdUrl) || Convert.ToInt32(objProductionModel.TearSheets) > 0 || Convert.ToInt32(objProductionModel.PositionId) > 0 || Convert.ToInt32(objProductionModel.SeparationId) > 0 || !string.IsNullOrEmpty(objProductionModel.PageNumber) || !string.IsNullOrEmpty(objProductionModel.ProductionComment)
                            || Convert.ToInt32(objProductionModel.ProductionStatusId) > 0 || Convert.ToBoolean(matExpected) == true || Convert.ToBoolean(matExpected) == false || Convert.ToBoolean(matOnHand) == true || Convert.ToBoolean(matOnHand) == false
                            || !string.IsNullOrEmpty(newMaterialsDate) || !string.IsNullOrEmpty(matTrackNum) || Convert.ToBoolean(matChanges) == true || Convert.ToBoolean(matChanges) == false || !string.IsNullOrEmpty(matFromPage) || !string.IsNullOrEmpty(materialContactId))
                        {

                            if (Convert.ToBoolean(materials) == true)
                            {

                                if (isValid == true)
                                {
                                    // save code

                                    medioOrderProductionResponse = _IMediaOrderProductionDetailsRepository.AddImportUpdateMediaOrderProductionDetail(objProductionModel);
                                    List<MediaOrderProductionDetailModel> lstMediaOrderProductionDetail = medioOrderProductionResponse.Data as List<MediaOrderProductionDetailModel>;
                                    if (medioOrderProductionResponse.StatusCode == 1)
                                    {
                                        status = "true";
                                        response.StatusCode = ApiStatus.Ok;
                                        response.Message = status;

                                    }
                                    else
                                    {
                                        status = "false";
                                        response.StatusCode = ApiStatus.Exception;
                                        response.Message = status;
                                    }
                                }
                                else
                                {
                                    string errorMessage = "MediaOrderId is empty";

                                }

                            }
                            else if (Convert.ToBoolean(materials) == false)
                            {

                                if (isValid == true)
                                {
                                    // save code

                                    medioOrderProductionResponse = _IMediaOrderProductionDetailsRepository.AddImportUpdateMediaOrderProductionDetail(objProductionModel);
                                    List<MediaOrderProductionDetailModel> lstMediaOrderProductionDetail = medioOrderProductionResponse.Data as List<MediaOrderProductionDetailModel>;
                                    if (medioOrderProductionResponse.StatusCode == 1)
                                    {
                                        status = "true";
                                        response.StatusCode = ApiStatus.Ok;
                                        response.Message = status;
                                    }
                                    else
                                    {
                                        status = "false";
                                        response.StatusCode = ApiStatus.Exception;
                                        response.Message = status;
                                    }
                                }
                                else
                                {
                                    string errorMessage = "MediaOrderId is empty";

                                }

                            }
                        }
                        else
                        {
                            status = "notSaved";
                            string cError = "production is not saved";
                            response.StatusCode = ApiStatus.InvalidInput;
                            response.Message = status;
                        }
                    }
                    else
                    {
                        status = "notSaved";
                        response.StatusCode = ApiStatus.InvalidInput;
                        response.Message = status;
                        string errorMessage = "MediaOrderId is empty";
                        CreateErrorOfDatatableForImportOrder(ref dtError, "MediaOrderID", "", errorMessage);
                        isValid = false;
                    }
                }

            }
            catch (Exception ex)
            {
                response.StatusCode = ApiStatus.Exception;
                response.Message = ex.Message.ToString();
                string cError = "";
                CreateErrorOfDatatableForImportOrder(ref dtError, "", ex.Message.ToString(), cError);
            }
            return response;
        }

        private string CreateErrorOfDatatableForImportOrder(ref DataTable dtError, string asiFieldName, string catchErrorMessage, string catchError)
        {
            string responseMsg = string.Empty;
            string basePath = AppDomain.CurrentDomain.BaseDirectory;
            string importOrderPath = ConfigurationManager.AppSettings["ImportOrdersPath"];
            try
            {
                int errorTableCount = dtError.Rows.Count;
                if (errorTableCount != 0)
                {
                    DataRow row;
                    row = dtError.NewRow();
                    row["Sr No"] = dtError.Rows.Count + 1;
                    row["Column Name"] = asiFieldName;
                    row["System Error"] = catchErrorMessage;
                    row["Custom Error"] = catchError;
                    dtError.Rows.Add(row);
                }
                else
                {
                    dtError.Columns.Add("Sr No");
                    dtError.Columns.Add("Column Name");
                    dtError.Columns.Add("System Error");
                    dtError.Columns.Add("Custom Error");

                    DataRow row;
                    row = dtError.NewRow();
                    row["Sr No"] = 1;
                    row["Column Name"] = asiFieldName;
                    row["System Error"] = catchErrorMessage;
                    row["Custom Error"] = catchError;
                    dtError.Rows.Add(row);



                    string fullPath = basePath + importOrderPath;
                    if (!Directory.Exists(fullPath))
                    {
                        Directory.CreateDirectory(fullPath);
                    }

                    string fileName = "importOrder" + DateTime.Now.ToFileTime() + ".xlsx";
                    string excelPath = Path.Combine(fullPath, fileName);

                }
                return responseMsg;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        private string ImportOrderStatusExcelSheet(ref DataTable dtError, DataTable tmpDt, string asiFieldName, string catchErrorMessage, string catchError, string excelSheetPath)
        {
            string responseMsg = string.Empty;
            try
            {
                string basePath = AppDomain.CurrentDomain.BaseDirectory;
                string importOrderPath = ConfigurationManager.AppSettings["ImportOrdersPath"];

                string fullPath = basePath + importOrderPath;
                if (!Directory.Exists(fullPath))
                {
                    Directory.CreateDirectory(fullPath);
                }

                if (!string.IsNullOrEmpty(excelSheetPath))
                {
                    var isErrorFileExit = System.IO.File.Exists(excelSheetPath);
                    if (isErrorFileExit == true)
                    {
                        File.Delete(excelSheetPath);

                        string fileName = "importOrder" + DateTime.Now.ToFileTime() + ".xls";
                        string excelPath = Path.Combine(fullPath, fileName);

                        string createdExcelPath = string.Empty;

                        createdExcelPath = CommonUtility.ExportToExcel(tmpDt, excelPath);
                        responseMsg = excelPath;
                    }
                    else
                    {

                        string fileName = "importOrder" + DateTime.Now.ToFileTime() + ".xls";
                        string excelPath = Path.Combine(fullPath, fileName);

                        string createdExcelPath = string.Empty;

                        createdExcelPath = CommonUtility.ExportToExcel(tmpDt, excelPath);
                        responseMsg = excelPath;
                    }
                }
                else
                {

                    string fileName = "importOrder" + DateTime.Now.ToFileTime() + ".xls";
                    string excelPath = Path.Combine(fullPath, fileName);

                    string createdExcelPath = string.Empty;

                    createdExcelPath = CommonUtility.ExportToExcel(tmpDt, excelPath);
                    responseMsg = excelPath;
                }

                return responseMsg;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        #endregion

        #region Public Methods
        public ResponseCommon GenerateBuyId()
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                MediaOrderBuy objMediaOrdersBuys = DB.MediaOrdersBuys.FirstOrDefault();
                if (objMediaOrdersBuys == null)
                {
                    objMediaOrdersBuys = new MediaOrderBuy()
                    {
                        UpdatedOn = DateTime.Now,
                        BuyId = 1000
                    };
                    DB.MediaOrdersBuys.Add(objMediaOrdersBuys);
                }
                else
                {
                    objMediaOrdersBuys.BuyId = objMediaOrdersBuys.BuyId + 1;

                    //*
                    objMediaOrdersBuys.UpdatedOn = DateTime.Now;

                    DB.Entry(objMediaOrdersBuys).State = EntityState.Modified;
                }
                DB.SaveChanges();

                response.StatusCode = ApiStatus.Ok;
                response.Data = new { buyId = objMediaOrdersBuys.BuyId };
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }

        public ResponseCommon GetOrdersByBuyId(int buyId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                string domainUrl = CommonUtility.CurrentHostDomain;
                List<MediaOrderModel> lstMediaOrderModel = new List<MediaOrderModel>();
                lstMediaOrderModel = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/ x.BuyId == buyId).OrderBy(x => (x.MediaOrderId))
                                          .Select(x => new MediaOrderModel
                                          {
                                              MediaOrderId = x.MediaOrderId,
                                              BuyId = x.BuyId,
                                              MediaAssetId = x.MediaAssetId,
                                              MediaAssetName = x.MediaAsset.MediaAssetName,
                                              IssueDateId = x.IssueDateId,
                                              Likelihood = x.Likelihood,
                                              MediaAssetLogo = x.MediaAsset.Logo == null | x.MediaAsset.Logo == "" ? null : domainUrl + x.MediaAsset.Logo,
                                              IssueDate = x.AdIssueDate.CoverDate,
                                              AdSizeId = x.RateCardDetail.AdSize.AdSizeId,

                                              //* 2020-11-10 Change : MediaBillingMethod No longer exists
                                              //AdSizeName = (x.RateCard.MediaBillingMethod.MediaBillingMethodName == BillingMethod.Web_CPM.ToString().Replace("_", " ")) ? (x.Units == null ? 0 : (int)x.Units.Value) + " Impressions"
                                              //              : (x.RateCard.MediaBillingMethod.MediaBillingMethodName == BillingMethod.Per_Word.ToString().Replace("_", " ")) ? (x.Units == null ? 0 : (int)x.Units.Value) + " Words"
                                              //              : (x.RateCard.MediaBillingMethod.MediaBillingMethodName == BillingMethod.CPM.ToString().Replace("_", " ")) ? (x.Units == null ? 0 : (int)x.Units.Value) + " Inserts"
                                              //              : x.RateCardDetail.AdSize.AdSizeName,

                                              AdSizeName = (x.RateCard.MediaBillingMethodName == BillingMethod.Web_CPM.ToString().Replace("_", " ")) ? (x.Units == null ? 0 : (int)x.Units.Value) + " Impressions"
                                                            : (x.RateCard.MediaBillingMethodName == BillingMethod.Per_Word.ToString().Replace("_", " ")) ? (x.Units == null ? 0 : (int)x.Units.Value) + " Words"
                                                            : (x.RateCard.MediaBillingMethodName == BillingMethod.CPM.ToString()) ? (x.Units == null ? 0 : (int)x.Units.Value) + " Inserts"
                                                            : x.RateCardDetail.AdSize.AdSizeName,

                                              AdTypeId = x.RateCard.AdType.AdTypeId,
                                              AdTypeName = x.RateCard.AdType.AdTypeName,
                                              AdColorId = x.RateCardDetail.AdColor.AdColorId,
                                              BoothLocation = string.IsNullOrEmpty(x.BoothLocation) ? "" : x.BoothLocation,
                                              AdColorName = x.RateCardDetail.AdColor.AdColorName,

                                              //* 2020-11-10 Change 
                                              //MediaBillingMethodId = x.RateCard.MediaBillingMethodId,
                                              //MediaBillingMathodeName = x.RateCard.MediaBillingMethod.MediaBillingMethodName,

                                              MediaBillingMethodName = x.RateCard.MediaBillingMethodName,

                                              FrequencyId = x.RateCardDetail.FrequencyId,
                                              FrequencyName = x.RateCardDetail.Frequency.FrequencyName,
                                              OrderStatus = x.OrderStatus,
                                              Units = x.Units,
                                              BaseRateCardCost = x.BaseRateCardCost,
                                              BaseCalculationCost = x.BaseCalculationCost,
                                              RateCardCost = x.RateCardCost,
                                              GrossCost = x.GrossCost,
                                              NetCost = x.NetCost,
                                              RateCardId = x.RateCardId,
                                              RateCardDetailId = x.RateCardDetailId,
                                              AdvertiserId = x.AdvertiserId,
                                              AgencyId = x.AgencyId,
                                              BillToInd = x.BillToInd,
                                              MediaOrderReps = (from y in x.MediaOrderReps
                                                                join r in DB.Reps on y.RepId equals r.RepId
                                                                join t in DB.Territories on y.TerritoryId equals t.TerritoryId
                                                                //where y.DeletedInd != true 
                                                                //    && r.DeletedInd != true 
                                                                //    && t.DeletedInd != true
                                                                select new MediaOrderRepsModel
                                                                {
                                                                    MediaOrderId = y.MediaOrderId,
                                                                    MediaOrderRepId = y.MediaOrderRepId,
                                                                    Commission = y.Commission,
                                                                    RepId = y.RepId,
                                                                    Split = y.Split,
                                                                    RepsName = r.RepName,
                                                                    TerritoryId = y.TerritoryId,
                                                                    TerritoryName = t.TerritoryName
                                                                }),
                                              ST_ID = x.StId,
                                              BT_ID = x.BtId,
                                              PageNumber = x.MediaOrderProductionDetails/*.Where(y => y.DeletedInd != true)*/.Select(y => y.PageNumber).FirstOrDefault(),
                                              HeadLine = x.MediaOrderProductionDetails/*.Where(y => y.DeletedInd != true)*/.Select(y => y.HeadLine).FirstOrDefault(),
                                              PositionName = (from z in x.MediaOrderProductionDetails/*.Where(y => y.DeletedInd != true)*/
                                                              join p in DB.Positions on z.PositionId equals p.PositionId
                                                              select p.PositionName
                                                              ).FirstOrDefault(),
                                              CampaignName = x.CampaignName,
                                              IsFrozen = x.IsFrozen,
                                              CreatedDate = x.CreatedOn
                                          }).AsEnumerable()
                                          .Select(x => new MediaOrderModel
                                          {
                                              MediaOrderId = x.MediaOrderId,
                                              BuyId = x.BuyId,
                                              MediaAssetId = x.MediaAssetId,
                                              MediaAssetName = x.MediaAssetName,
                                              IssueDateId = x.IssueDateId,
                                              Likelihood = x.Likelihood,
                                              MediaAssetLogo = x.MediaAssetLogo,
                                              IssueDate = x.IssueDate,

                                              AdSizeId = x.AdSizeId,
                                              AdSizeName = x.AdSizeName,
                                              AdTypeId = x.AdTypeId,
                                              AdTypeName = x.AdTypeName,
                                              AdColorId = x.AdColorId,

                                              BoothLocation = x.BoothLocation,
                                              AdColorName = x.AdColorName,

                                              // 2020-11-10 Getting id from Hardcoded list
                                              //MediaBillingMethodId = Get_MediaBillingMethodName_Values.GetMediaBillingMethodNameValues()
                                              //                                                      .Where(x1 => x1.MediaBillingMethodName == x.MediaBillingMethodName)
                                              //                                                      .Select(x1 => x1.MediaBillingMethodId)
                                              //                                                      .FirstOrDefault(),

                                              // 2020-11-25 Getting id from enums                                              

                                              MediaBillingMethodId = Enum.GetValues(typeof(BillingMethod)).Cast<BillingMethod>()
                                                                    .Where(x1 => x1.ToString() == x.MediaBillingMethodName.Replace(" ", "_"))
                                                                    .Select(x1 => (int)x1)
                                                                    .FirstOrDefault(),

                                              MediaBillingMethodName = x.MediaBillingMethodName,
                                              FrequencyId = x.FrequencyId,
                                              FrequencyName = x.FrequencyName,
                                              OrderStatus = x.OrderStatus,
                                              Units = x.Units,
                                              BaseRateCardCost = x.BaseRateCardCost,
                                              BaseCalculationCost = x.BaseCalculationCost,
                                              RateCardCost = x.RateCardCost,
                                              GrossCost = x.GrossCost,
                                              NetCost = x.NetCost,
                                              RateCardId = x.RateCardId,
                                              RateCardDetailId = x.RateCardDetailId,
                                              AdvertiserId = x.AdvertiserId,
                                              AgencyId = x.AgencyId,
                                              BillToInd = x.BillToInd,
                                              MediaOrderReps = x.MediaOrderReps,
                                              ST_ID = x.ST_ID,
                                              BT_ID = x.BT_ID,
                                              PageNumber = x.PageNumber,
                                              HeadLine = x.HeadLine,
                                              PositionName = x.PositionName,
                                              CampaignName = x.CampaignName,
                                              IsFrozen = x.IsFrozen,
                                              CreatedDate = x.CreatedDate
                                          }).ToList();
                if (lstMediaOrderModel.Count > 0)
                {
                    response.StatusCode = ApiStatus.Ok;
                    response.Data = lstMediaOrderModel;
                }
                else
                {
                    response.StatusCode = ApiStatus.NoRecords;
                    response.Message = ApiStatus.NoRecordsMessge;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }

        public ResponseCommon GetOrdersNotInBuyer(int buyId, string advertiserId)
        {
            DateTime dtNow = DateTime.Now.Date;
            ResponseCommon response = new ResponseCommon();
            try
            {
                //&& x.BuyId != buyId
                List<MediaOrderModel> lstMediaOrderModel = new List<MediaOrderModel>();
                lstMediaOrderModel = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/ x.AdvertiserId == advertiserId
                                                            && x.AdIssueDate.CoverDate < dtNow
                                                            && x.OrderStatus == APIMediaOrderStatus.Run)
                                          .Select(x => new MediaOrderModel
                                          {
                                              MediaOrderId = x.MediaOrderId,
                                              BuyId = x.BuyId,
                                              MediaAssetId = x.MediaAssetId,
                                              MediaAssetName = x.MediaAsset.MediaAssetName,
                                              IssueDateId = x.IssueDateId,
                                              IssueDate = x.AdIssueDate.CoverDate,//.ToString("MM/dd/yyyy"),
                                              AdSizeId = x.RateCardDetail.AdSize.AdSizeId,

                                              //* 2020-11-10 Change : MediaBillingMethod no longer exists
                                              //AdSizeName = (x.RateCard.MediaBillingMethod.MediaBillingMethodName == BillingMethod.Web_CPM.ToString().Replace("_", " ")) ? (x.Units == null ? 0 : (int)x.Units.Value) + " Impressions"
                                              //              : (x.RateCard.MediaBillingMethod.MediaBillingMethodName == BillingMethod.Per_Word.ToString().Replace("_", " ")) ? (x.Units == null ? 0 : (int)x.Units.Value) + " Words"
                                              //              : (x.RateCard.MediaBillingMethod.MediaBillingMethodName == BillingMethod.CPM.ToString().Replace("_", " ")) ? (x.Units == null ? 0 : (int)x.Units.Value) + " Inserts"
                                              //              : x.RateCardDetail.AdSize.AdSizeName,

                                              AdSizeName = (x.RateCard.MediaBillingMethodName == BillingMethod.Web_CPM.ToString().Replace("_", " ")) ? (x.Units == null ? 0 : (int)x.Units.Value) + " Impressions"
                                                            : (x.RateCard.MediaBillingMethodName == BillingMethod.Per_Word.ToString().Replace("_", " ")) ? (x.Units == null ? 0 : (int)x.Units.Value) + " Words"
                                                            : (x.RateCard.MediaBillingMethodName == BillingMethod.CPM.ToString()) ? (x.Units == null ? 0 : (int)x.Units.Value) + " Inserts"
                                                            : x.RateCardDetail.AdSize.AdSizeName,

                                              AdColorId = x.RateCardDetail.AdColor.AdColorId,
                                              AdColorName = x.RateCardDetail.AdColor.AdColorName,
                                              AdTypeId = x.RateCard.AdType.AdTypeId,
                                              AdTypeName = x.RateCard.AdType.AdTypeName,

                                              //* 2020-11-10 Change
                                              //MediaBillingMethodId = x.RateCard.MediaBillingMethodId,
                                              //MediaBillingMathodeName = x.RateCard.MediaBillingMethod.MediaBillingMethodName,                                             
                                              MediaBillingMethodName = x.RateCard.MediaBillingMethodName,

                                              FrequencyId = x.RateCardDetail.FrequencyId,
                                              FrequencyName = x.RateCardDetail.Frequency.FrequencyName,
                                              OrderStatus = x.OrderStatus,
                                              Units = x.Units,
                                              BaseRateCardCost = x.BaseRateCardCost,
                                              BaseCalculationCost = x.BaseCalculationCost,
                                              RateCardCost = x.RateCardCost,
                                              GrossCost = x.GrossCost,
                                              NetCost = x.NetCost,
                                              RateCardId = x.RateCardId,
                                              RateCardDetailId = x.RateCardDetailId,
                                              AdvertiserId = x.AdvertiserId,
                                              AgencyId = x.AgencyId,
                                              PageNumber = x.MediaOrderProductionDetails/*.Where(y => y.DeletedInd != true)*/.Select(y => y.PageNumber).FirstOrDefault(),
                                              HeadLine = x.MediaOrderProductionDetails/*.Where(y => y.DeletedInd != true)*/.Select(y => y.HeadLine).FirstOrDefault(),
                                              IsFrozen = x.IsFrozen,
                                              PositionName = (from z in x.MediaOrderProductionDetails/*.Where(y => y.DeletedInd != true)*/
                                                              join p in DB.Positions on z.PositionId equals p.PositionId
                                                              select p.PositionName
                                                            ).FirstOrDefault(),
                                              CampaignName = x.CampaignName,
                                              CreatedDate = x.CreatedOn
                                          }).AsEnumerable()
                                          .Select(x => new MediaOrderModel
                                          {
                                              MediaOrderId = x.MediaOrderId,
                                              BuyId = x.BuyId,
                                              MediaAssetId = x.MediaAssetId,
                                              MediaAssetName = x.MediaAssetName,
                                              IssueDateId = x.IssueDateId,
                                              IssueDate = x.IssueDate,
                                              AdSizeId = x.AdSizeId,
                                              AdSizeName = x.AdSizeName,
                                              AdColorId = x.AdColorId,
                                              AdColorName = x.AdColorName,
                                              AdTypeId = x.AdTypeId,
                                              AdTypeName = x.AdTypeName,
                                              // 2020-11-10
                                              //MediaBillingMethodId = Get_MediaBillingMethodName_Values.GetMediaBillingMethodNameValues()
                                              //                                                      .Where(x1 => x1.MediaBillingMethodName == x.MediaBillingMethodName)
                                              //                                                      .Select(x1 => x1.MediaBillingMethodId)
                                              //                                                      .FirstOrDefault(),

                                              // 2020-11-25 Getting id from enum
                                              MediaBillingMethodId = Enum.GetValues(typeof(BillingMethod)).Cast<BillingMethod>()
                                                                    .Where(x1 => x1.ToString() == x.MediaBillingMethodName.Replace(" ", "_"))
                                                                    .Select(x1 => (int)x1)
                                                                    .FirstOrDefault(),

                                              MediaBillingMethodName = x.MediaBillingMethodName,
                                              FrequencyId = x.FrequencyId,
                                              FrequencyName = x.FrequencyName,
                                              OrderStatus = x.OrderStatus,
                                              Units = x.Units,
                                              BaseRateCardCost = x.BaseRateCardCost,
                                              BaseCalculationCost = x.BaseCalculationCost,
                                              RateCardCost = x.RateCardCost,
                                              GrossCost = x.GrossCost,
                                              NetCost = x.NetCost,
                                              RateCardId = x.RateCardId,
                                              RateCardDetailId = x.RateCardDetailId,
                                              AdvertiserId = x.AdvertiserId,
                                              AgencyId = x.AgencyId,
                                              PageNumber = x.PageNumber,
                                              HeadLine = x.HeadLine,
                                              IsFrozen = x.IsFrozen,
                                              PositionName = x.PositionName,
                                              CampaignName = x.CampaignName,
                                              CreatedDate = x.CreatedDate
                                          }).ToList();
                if (lstMediaOrderModel.Count > 0)
                {
                    response.StatusCode = ApiStatus.Ok;
                    response.Data = lstMediaOrderModel;
                }
                else
                {
                    response.StatusCode = ApiStatus.NoRecords;
                    response.Message = ApiStatus.NoRecordsMessge;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }

        public ResponseCommon GetOrdersByAdvertisor(GetOrdersByAdvertisorModel model)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                List<MediaOrdersByAdvertisorModel> lstMediaOrdersByAdvertisor = new List<MediaOrdersByAdvertisorModel>();
                int draw = model.Draw;
                string skip = model.Skip.ToString();
                string take = model.Take.ToString();

                string advertisorIds = string.Empty;
                foreach (var item in model.AdvertisorId)
                {
                    advertisorIds += item + ",";
                }
                advertisorIds = advertisorIds.TrimEnd(',');

                List<asi_GetOrdersByAdvertiserIds_Result> lstSpResultMediaAdvertiser = DB.GetOrdersByAdvertiserIds(advertisorIds, skip, take).ToList();
                foreach (var item in lstSpResultMediaAdvertiser)
                {
                    lstMediaOrdersByAdvertisor.Add(new MediaOrdersByAdvertisorModel
                    {
                        TotalCount = Convert.ToInt32(item.TotalCount),
                        AdvertiserId = item.AdvertiserId,
                        ST_ID = item.StId,
                        NoOfOrders = item.NoOfOrders,
                        GrossCost = item.GrossCost,
                        NetCost = item.NetCost
                    });
                }

                if (lstMediaOrdersByAdvertisor.Count > 0)
                {
                    response.StatusCode = ApiStatus.Ok;
                    response.Data = lstMediaOrdersByAdvertisor;
                }
                else
                {
                    response.StatusCode = ApiStatus.NoRecords;
                    response.Message = ApiStatus.NoRecordsMessge;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }


        public ResponseCommonForDatatable GetOrdersBuyIdWise()
        {
            GetOrdersBuyIdWiseModal[] model = new GetOrdersBuyIdWiseModal[10];
            ResponseCommonForDatatable response = new ResponseCommonForDatatable();
            List<MediaOrdersBuyIdWise> lstMediaOrderModel = new List<MediaOrdersBuyIdWise>();
            int recordsTotal = 0;
            try
            {

                #region fetch post filter data
                GetOrdersBuyIdWiseModal objFilter = new GetOrdersBuyIdWiseModal
                {
                    FilterOption = new GetOrdersBuyIdWiseFilterOptionModal()
                };
                int index = 0;
                var context = HttpContext.Current;
                var currentRequest = HttpContext.Current.Request;

                string[] lstId = new string[] { };
                var anFieldName = currentRequest["AdvertiserName[FieldName]"];
                var anColumnNo = currentRequest["AdvertiserName[ColumnNo]"];
                var anFilterIsArray = currentRequest["AdvertiserName[FilterOption][FilterIsArray]"];
                var anIds = currentRequest["AdvertiserName[FilterOption][Ids][]"];
                if (!string.IsNullOrEmpty(anIds))
                {
                    lstId = anIds.Split(',');
                }
                if (!string.IsNullOrEmpty(anFieldName) && !string.IsNullOrEmpty(anFilterIsArray) && lstId.Length > 0)
                {
                    bool isFilterArray = Convert.ToBoolean(anFilterIsArray);
                    objFilter.FieldName = anFieldName;
                    objFilter.FilterOption.FilterIsArray = (isFilterArray);
                    objFilter.FilterOption.Ids = lstId;
                    model[index] = objFilter;
                    objFilter = new GetOrdersBuyIdWiseModal();
                    objFilter.FilterOption = new GetOrdersBuyIdWiseFilterOptionModal();
                    index++;
                }

                string[] Ids2 = new string[] { };
                var advertiserIdFieldName = currentRequest["AdvertiserId[FieldName]"];
                var advertiserIdColumnNo = currentRequest["AdvertiserId[ColumnNo]"];
                var advertiserIdFilterIsArray = currentRequest["AdvertiserId[FilterOption][FilterIsArray]"];
                var advertiserFilterIds = currentRequest["AdvertiserId[FilterOption][Ids][]"];
                if (!string.IsNullOrEmpty(advertiserFilterIds))
                {
                    Ids2 = advertiserFilterIds.Split(',');
                }
                if (!string.IsNullOrEmpty(advertiserIdFieldName) && !string.IsNullOrEmpty(advertiserIdFilterIsArray) && Ids2.Length > 0)
                {
                    bool isIdArrayFilter = Convert.ToBoolean(advertiserIdFilterIsArray);
                    objFilter.FieldName = advertiserIdFieldName;
                    objFilter.FilterOption.FilterIsArray = (isIdArrayFilter);
                    objFilter.FilterOption.Ids = Ids2;
                    model[index] = objFilter;
                    objFilter = new GetOrdersBuyIdWiseModal();
                    objFilter.FilterOption = new GetOrdersBuyIdWiseFilterOptionModal();
                    index++;
                }

                if (!string.IsNullOrEmpty(anIds))
                {
                    if (string.IsNullOrEmpty(advertiserFilterIds))
                    {
                        advertiserFilterIds = "";
                        advertiserFilterIds = anIds;
                        advertiserIdFieldName = anFieldName;
                    }
                    else
                    {
                        advertiserFilterIds += "," + anIds;
                        advertiserIdFieldName = anFieldName;
                    }
                }
                else
                {

                }

                string[] idFiltersByName = new string[] { };
                var snFieldName = currentRequest["ST_IDName[FieldName]"];
                var snColumnNo = currentRequest["ST_IDName[ColumnNo]"];
                var snFilterIsArray = currentRequest["ST_IDName[FilterOption][FilterIsArray]"];
                var snIds = currentRequest["ST_IDName[FilterOption][Ids][]"];
                if (!string.IsNullOrEmpty(snIds))
                {
                    idFiltersByName = snIds.Split(',');
                }
                if (!string.IsNullOrEmpty(snFieldName) && !string.IsNullOrEmpty(snFilterIsArray) && idFiltersByName.Length > 0)
                {
                    bool _aIdFilterIsArray = Convert.ToBoolean(snFilterIsArray);
                    objFilter.FieldName = snFieldName;
                    objFilter.FilterOption.FilterIsArray = (_aIdFilterIsArray);
                    objFilter.FilterOption.Ids = idFiltersByName;
                    model[index] = objFilter;
                    objFilter = new GetOrdersBuyIdWiseModal();
                    objFilter.FilterOption = new GetOrdersBuyIdWiseFilterOptionModal();
                    index++;
                }

                string[] filters = new string[] { };
                var sIdFieldName = currentRequest["ST_ID[FieldName]"];
                var sIdColumnNo = currentRequest["ST_ID[ColumnNo]"];
                var sIdFilterIsArray = currentRequest["ST_ID[FilterOption][FilterIsArray]"];
                var sIdIds = currentRequest["ST_ID[FilterOption][Ids][]"];
                if (!string.IsNullOrEmpty(sIdIds))
                {
                    filters = sIdIds.Split(',');
                }
                if (!string.IsNullOrEmpty(sIdFieldName) && !string.IsNullOrEmpty(sIdFilterIsArray) && filters.Length > 0)
                {
                    bool isIdArrayFilter = Convert.ToBoolean(sIdFilterIsArray);
                    objFilter.FieldName = sIdFieldName;
                    objFilter.FilterOption.FilterIsArray = (isIdArrayFilter);
                    objFilter.FilterOption.Ids = filters;
                    model[index] = objFilter;
                    objFilter = new GetOrdersBuyIdWiseModal();
                    objFilter.FilterOption = new GetOrdersBuyIdWiseFilterOptionModal();
                    index++;
                }

                if (!string.IsNullOrEmpty(snIds))
                {
                    if (string.IsNullOrEmpty(sIdIds))
                    {
                        sIdIds = "";
                        sIdIds = snIds;
                        sIdFieldName = snFieldName;
                    }
                    else
                    {
                        sIdIds += "," + snIds;
                        sIdFieldName = snFieldName;
                    }
                }
                else
                {

                }

                var buyIdFieldName = currentRequest["BuyId[FieldName]"];
                var buyIdColumnNo = currentRequest["BuyId[ColumnNo]"];
                var buyIdFilterIsNumber = currentRequest["BuyId[FilterOption][FilterIsNumber]"];
                var buyIdType = currentRequest["BuyId[FilterOption][Type]"];
                var buyIdvalue = currentRequest["BuyId[FilterOption][value]"];

                if (!string.IsNullOrEmpty(buyIdFieldName) && !string.IsNullOrEmpty(buyIdFilterIsNumber) && !string.IsNullOrEmpty(buyIdType) &&
                    !string.IsNullOrEmpty(buyIdvalue))
                {
                    bool isNumberFilter = Convert.ToBoolean(buyIdFilterIsNumber);
                    objFilter.FieldName = buyIdFieldName;
                    objFilter.FilterOption.FilterIsNumber = (isNumberFilter);
                    objFilter.FilterOption.Type = buyIdType;
                    objFilter.FilterOption.Value = buyIdvalue;
                    model[index] = objFilter;
                    objFilter = new GetOrdersBuyIdWiseModal
                    {
                        FilterOption = new GetOrdersBuyIdWiseFilterOptionModal()
                    };
                    index++;
                }
                var mediaAssetFieldName = currentRequest["MediaAssetStatus[FieldName]"];
                var mediaAssetColumnNo = currentRequest["MediaAssetStatus[ColumnNo]"];
                var mediaAssetType = currentRequest["MediaAssetStatus[FilterOption][Type]"];
                var mediaAssetValue = currentRequest["MediaAssetStatus[FilterOption][value]"];

                if (!string.IsNullOrEmpty(mediaAssetFieldName) && !string.IsNullOrEmpty(mediaAssetType) && !string.IsNullOrEmpty(mediaAssetValue))
                {
                    objFilter.FieldName = mediaAssetFieldName;
                    objFilter.FilterOption.Type = mediaAssetType;
                    objFilter.FilterOption.Value = mediaAssetValue;
                    model[index] = objFilter;
                    objFilter = new GetOrdersBuyIdWiseModal();
                    objFilter.FilterOption = new GetOrdersBuyIdWiseFilterOptionModal();
                    index++;
                }
                var orderStatusFieldName = currentRequest["OrderStatus[FieldName]"];
                var orderStatusColumnNo = currentRequest["OrderStatus[ColumnNo]"];
                var orderStatusType = currentRequest["OrderStatus[FilterOption][Type]"];
                var orderStatusValue = currentRequest["OrderStatus[FilterOption][value]"];

                if (!string.IsNullOrEmpty(orderStatusFieldName) && !string.IsNullOrEmpty(orderStatusType) && !string.IsNullOrEmpty(orderStatusValue))
                {
                    objFilter.FieldName = orderStatusFieldName;
                    objFilter.FilterOption.Type = orderStatusType;
                    objFilter.FilterOption.Value = orderStatusValue;
                    model[index] = objFilter;
                    objFilter = new GetOrdersBuyIdWiseModal();
                    objFilter.FilterOption = new GetOrdersBuyIdWiseFilterOptionModal();
                    index++;
                }

                var startDateFieldName = currentRequest["FlightStartDate[FieldName]"];
                var startDateColumnNo = currentRequest["FlightStartDate[ColumnNo]"];
                var filterIsDate = currentRequest["FlightStartDate[FilterOption][FilterIsDate]"];
                var toDate = currentRequest["FlightStartDate[FilterOption][ToDate]"];
                var fromDate = currentRequest["FlightStartDate[FilterOption][FromDate]"];

                if (!string.IsNullOrEmpty(startDateFieldName) && !string.IsNullOrEmpty(filterIsDate) &&
                    !string.IsNullOrEmpty(toDate) && !string.IsNullOrEmpty(fromDate))
                {
                    bool isDateFilter = Convert.ToBoolean(filterIsDate);
                    objFilter.FieldName = startDateFieldName;
                    objFilter.FilterOption.FilterIsDate = (isDateFilter);
                    objFilter.FilterOption.ToDate = toDate;
                    objFilter.FilterOption.FromDate = fromDate;
                    model[index] = objFilter;
                    objFilter = new GetOrdersBuyIdWiseModal
                    {
                        FilterOption = new GetOrdersBuyIdWiseFilterOptionModal()
                    };
                    index++;
                }

                var fedFieldName = currentRequest["FlightEndDate[FieldName]"];
                var fedColumnNo = currentRequest["FlightEndDate[ColumnNo]"];
                var fedFilterIsDate = currentRequest["FlightEndDate[FilterOption][FilterIsDate]"];
                var fedToDate = currentRequest["FlightEndDate[FilterOption][ToDate]"];
                var fedFromDate = currentRequest["FlightEndDate[FilterOption][FromDate]"];

                if (!string.IsNullOrEmpty(fedFieldName) && !string.IsNullOrEmpty(fedFilterIsDate) &&
                    !string.IsNullOrEmpty(fedToDate) && !string.IsNullOrEmpty(fedFromDate))
                {
                    bool isDateFilter = Convert.ToBoolean(fedFilterIsDate);
                    objFilter.FieldName = fedFieldName;
                    objFilter.FilterOption.FilterIsDate = (isDateFilter);
                    objFilter.FilterOption.ToDate = fedToDate;
                    objFilter.FilterOption.FromDate = fedFromDate;
                    model[index] = objFilter;
                    objFilter = new GetOrdersBuyIdWiseModal();
                    objFilter.FilterOption = new GetOrdersBuyIdWiseFilterOptionModal();
                    index++;
                }

                var gFieldName = currentRequest["GrossCost[FieldName]"];
                var gColumnNo = currentRequest["GrossCost[ColumnNo]"];
                var gFilterIsNumber = currentRequest["GrossCost[FilterOption][FilterIsNumber]"];
                var gType = currentRequest["GrossCost[FilterOption][Type]"];
                var gvalue = currentRequest["GrossCost[FilterOption][value]"];

                if (!string.IsNullOrEmpty(gFieldName) && !string.IsNullOrEmpty(gFilterIsNumber) && !string.IsNullOrEmpty(gType) &&
                    !string.IsNullOrEmpty(gvalue))
                {
                    bool isNumberFilter = Convert.ToBoolean(gFilterIsNumber);
                    objFilter.FieldName = gFieldName;
                    objFilter.FilterOption.FilterIsNumber = (isNumberFilter);
                    objFilter.FilterOption.Type = gType;
                    objFilter.FilterOption.Value = gvalue;
                    model[index] = objFilter;
                    objFilter = new GetOrdersBuyIdWiseModal();
                    objFilter.FilterOption = new GetOrdersBuyIdWiseFilterOptionModal();
                    index++;
                }

                var nFieldName = currentRequest["NetCost[FieldName]"];
                var nColumnNo = currentRequest["NetCost[ColumnNo]"];
                var nFilterIsNumber = currentRequest["NetCost[FilterOption][FilterIsNumber]"];
                var nType = currentRequest["NetCost[FilterOption][Type]"];
                var nvalue = currentRequest["NetCost[FilterOption][value]"];

                if (!string.IsNullOrEmpty(nFieldName) && !string.IsNullOrEmpty(nFilterIsNumber) && !string.IsNullOrEmpty(nType) &&
                    !string.IsNullOrEmpty(nvalue))
                {
                    bool isNumberFilter = Convert.ToBoolean(nFilterIsNumber);
                    objFilter.FieldName = nFieldName;
                    objFilter.FilterOption.FilterIsNumber = isNumberFilter;
                    objFilter.FilterOption.Type = nType;
                    objFilter.FilterOption.Value = nvalue;
                    model[index] = objFilter;
                    objFilter = new GetOrdersBuyIdWiseModal();
                    objFilter.FilterOption = new GetOrdersBuyIdWiseFilterOptionModal();
                    index++;
                }

                string draw = currentRequest["draw"];
                string start = currentRequest["start"];
                string length = currentRequest["length"];
                //Find Order Column

                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;

                #endregion

                List<asi_GetOrdersbyFilterOption_Result> lstGetOrdersbyFilterOptionResult = DB.GetOrdersbyFilterOption(buyIdFieldName, buyIdType, buyIdvalue, advertiserIdFieldName,
                    advertiserFilterIds, sIdFieldName, sIdIds, startDateFieldName, toDate, fromDate, fedFieldName, fedToDate, fedFromDate, gFieldName, gType,
                    gvalue, nFieldName, nType, nvalue, mediaAssetFieldName, mediaAssetType, mediaAssetValue, orderStatusFieldName, orderStatusValue, skip.ToString(), pageSize.ToString()).ToList();

                foreach (var item in lstGetOrdersbyFilterOptionResult)
                {
                    recordsTotal = (Int32)item.Totalcount;
                    lstMediaOrderModel.Add(new MediaOrdersBuyIdWise
                    {
                        BuyId = Convert.ToInt32(item.BuyId),
                        AdvertiserId = (item.AdvertiserId),
                        AgencyId = item.AgencyId,
                        ST_ID = item.StId,
                        BT_ID = item.BtId,
                        GrossCost = item.GrossCost,
                        NetCost = item.NetCost,
                        FlightStartDate = item.FlightStartDate,
                        FlightEndDate = item.FlightEndDate,
                        MediaAssets = item.MediaAsset,
                        OrderStatus = (new List<MediaOrdersBuyIdWiseStatusDetail>{
                       new MediaOrdersBuyIdWiseStatusDetail { Status = APIMediaOrderStatus.Proposal, NoOfOrders = item.OrderStatusProposalCount},
                       new MediaOrdersBuyIdWiseStatusDetail { Status = APIMediaOrderStatus.Run, NoOfOrders = item.OrderStatusRunCount},
                       new MediaOrdersBuyIdWiseStatusDetail { Status = APIMediaOrderStatus.Cancel, NoOfOrders = item.OrderStatusCancelCount}

                  }).ToList()
                    });
                }

                if (lstMediaOrderModel.Count > 0)
                {
                    response.Draw = Convert.ToInt32(draw);
                    response.RecordsTotal = recordsTotal;
                    response.RecordsFiltered = recordsTotal;
                    response.Data = lstMediaOrderModel;
                }
                else
                {
                    response.Draw = Convert.ToInt32(draw);
                    response.RecordsTotal = recordsTotal;
                    response.RecordsFiltered = recordsTotal;
                    response.Data = lstMediaOrderModel;
                }
            }
            catch (Exception ex)
            {
                var context = HttpContext.Current;
                var currentRequest = HttpContext.Current.Request;
                string draw = currentRequest["draw"];
                recordsTotal = lstMediaOrderModel.Count();
                response.Draw = Convert.ToInt32(draw);
                response.RecordsTotal = recordsTotal;
                response.RecordsFiltered = recordsTotal;
                response.Data = lstMediaOrderModel;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return response;
        }

        public ResponseCommon AddMediaOrder(PostMediaOrderModel model)
        {
            ResponseCommon response = new ResponseCommon();
            List<MediaOrder> lstMediaOrder = new List<MediaOrder>();
            bool flag = true;
            var issueDate = "";
            var successIssueDate = "";
            var errorMessage = "";
            var successMessage = "";
            var errorStatus = 0;
            try
            {

                double splitSum = model.MediaOrderReps.Select(x => x.Split).Sum();
                if (splitSum != 100)
                {
                    response.StatusCode = ApiStatus.InvalidInput;
                    response.Message = "Order reps split should be 100";
                    return response;
                }
                foreach (var issueDateId in model.MediaOrderIssueDates)
                {
                    MediaOrder order = new MediaOrder()
                    {
                        BuyId = model.BuyId,
                        AdvertiserId = model.AdvertiserId,
                        AgencyId = model.AgencyId,
                        BillToInd = model.BillToInd,
                        BtId = model.BT_ID,
                        StId = model.ST_ID,
                        Likelihood = 10,
                        MediaAssetId = model.MediaAssetId,
                        RateCardId = model.RateCardId,
                        RateCardDetailId = model.RateCardDetailId,
                        ProductCode = model.ProductCode,
                        CampaignName = model.CampaignName,
                        Column = model.Column,
                        Inches = model.Inches,
                        Units = model.Units,
                        ClassifiedText = model.ClassifiedText,
                        PerWordCost = model.PerWordCost,
                        BoothLocation = model.BoothLocation,
                        BaseRateCardCost = model.BaseRateCardCost,
                        BaseCalculationCost = model.BaseCalculationCost,
                        RateCardCost = model.RateCardCost,
                        GrossCost = model.RateCardCost,
                        NetCost = model.RateCardCost,
                        FlightStartDate = model.FlightStartDate,
                        FlightEndDate = model.FlightEndDate,
                        IssueDateId = issueDateId,
                        OrderStatus = MediaOrderStatus.Proposal.ToString(),
                        CreatedOn = DateTime.Now,

                        //*
                        UpdatedOn = DateTime.Now,
                        CreatedByUserKey = Constants.Created_Update_ByUserKey,
                        UpdatedByUserKey = Constants.Created_Update_ByUserKey,
                    };
                    var inventoryMaster = (from m in DB.MediaInventoryMasters
                                           where m.IssueDateId == order.IssueDateId
                                           && m.AdSizeId == model.AdSizeId
                                           && m.MediaAssetId == order.MediaAssetId
                                           //&& m.DeletedInd == false
                                           select new { m.TotalInventory, m.InventoryId }).FirstOrDefault();

                    if (inventoryMaster != null && inventoryMaster.TotalInventory != null)
                    {
                        var blockedInventory = (from d in DB.MediaInventoryDetails
                                                where d.InventoryId == inventoryMaster.InventoryId
                                                group d by d.InventoryId into g
                                                select new { total = g.Sum(s => s.BlockedInventory) }).FirstOrDefault();
                        if (blockedInventory != null)
                        {
                            if (Convert.ToInt32(blockedInventory.total) >= Convert.ToInt32(inventoryMaster.TotalInventory))
                            {
                                flag = false;
                                var coverdate = (from d in DB.AdIssueDates
                                                 where d.IssueDateId == order.IssueDateId
                                                 select d.CoverDate).FirstOrDefault();
                                var adSize = (from a in DB.AdSizes
                                              where a.AdSizeId == model.AdSizeId
                                              select a.AdSizeName).FirstOrDefault();
                                if (coverdate != null && adSize != null && adSize != "")
                                {
                                    if (issueDate != "")
                                    {
                                        issueDate = issueDate + ", " + Convert.ToString(coverdate.Month + "/" + coverdate.Day + "/" + coverdate.Year);
                                    }
                                    else
                                    {
                                        issueDate = Convert.ToString(coverdate.Month + "/" + coverdate.Day + "/" + coverdate.Year);
                                    }
                                    errorMessage = "All ad spaces for " + issueDate + " issue date and " + adSize + " size are out of inventory";
                                    errorStatus = ApiStatus.OutOfInventory;
                                }
                            }
                            else
                            {
                                var coverdate = (from d in DB.AdIssueDates
                                                 where d.IssueDateId == order.IssueDateId
                                                 select d.CoverDate).FirstOrDefault();
                                if (successIssueDate != "")
                                {
                                    successIssueDate = successIssueDate + ", " + Convert.ToString(coverdate.Month + "/" + coverdate.Day + "/" + coverdate.Year);
                                }
                                else
                                {
                                    successIssueDate = Convert.ToString(coverdate.Month + "/" + coverdate.Day + "/" + coverdate.Year);
                                }
                                var adSize = (from a in DB.AdSizes
                                              where a.AdSizeId == model.AdSizeId
                                              select a.AdSizeName).FirstOrDefault();
                                //successMessage = "All ad spaces for " + successIssueDate + " issue date and " + adSize + " size are add to the Buy ID";
                                successMessage = "All ad spaces for " + successIssueDate + " issue date and " + adSize + " size has been booked";

                                List<MediaOrderRep> lstMediaOrderReps = new List<MediaOrderRep>();
                                foreach (var rep in model.MediaOrderReps)
                                {
                                    MediaOrderRep moRep = new MediaOrderRep()
                                    {
                                        RepId = rep.RepId,
                                        TerritoryId = rep.TerritoryId,
                                        Commission = rep.Commission,
                                        Split = rep.Split,
                                        CreatedOn = DateTime.Now,

                                        //*
                                        UpdatedOn = DateTime.Now,
                                        CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                        UpdatedByUserKey = Constants.Created_Update_ByUserKey,
                                    };
                                    lstMediaOrderReps.Add(moRep);
                                }
                                order.MediaOrderReps = lstMediaOrderReps;
                                lstMediaOrder.Add(order);
                                flag = true;
                            }
                        }
                        else
                        {
                            List<MediaOrderRep> lstMediaOrderReps = new List<MediaOrderRep>();
                            foreach (var rep in model.MediaOrderReps)
                            {
                                MediaOrderRep moRep = new MediaOrderRep()
                                {
                                    RepId = rep.RepId,
                                    TerritoryId = rep.TerritoryId,
                                    Commission = rep.Commission,
                                    Split = rep.Split,
                                    CreatedOn = DateTime.Now,

                                    //*
                                    UpdatedOn = DateTime.Now,
                                    CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                    UpdatedByUserKey = Constants.Created_Update_ByUserKey,
                                };
                                lstMediaOrderReps.Add(moRep);
                            }
                            order.MediaOrderReps = lstMediaOrderReps;
                            lstMediaOrder.Add(order);
                            flag = true;
                        }
                    }
                    else
                    {
                        List<MediaOrderRep> lstMediaOrderReps = new List<MediaOrderRep>();
                        foreach (var rep in model.MediaOrderReps)
                        {
                            MediaOrderRep moRep = new MediaOrderRep()
                            {
                                RepId = rep.RepId,
                                TerritoryId = rep.TerritoryId,
                                Commission = rep.Commission,
                                Split = rep.Split,
                                CreatedOn = DateTime.Now,

                                //*
                                UpdatedOn = DateTime.Now,
                                CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                UpdatedByUserKey = Constants.Created_Update_ByUserKey,
                            };
                            lstMediaOrderReps.Add(moRep);
                        }
                        order.MediaOrderReps = lstMediaOrderReps;
                        lstMediaOrder.Add(order);
                        flag = true;
                    }


                    if (flag)
                    {
                        //Edit Existing
                        List<MediaOrderRep> lstMediaOrderRepsData = new List<MediaOrderRep>();
                        foreach (var item in DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/ x.BuyId == model.BuyId).ToList())
                        {
                            item.AdvertiserId = model.AdvertiserId;
                            item.AgencyId = model.AgencyId;
                            item.BillToInd = model.BillToInd;
                            item.BtId = model.BT_ID;
                            item.StId = model.ST_ID;
                            item.CampaignName = model.CampaignName;
                            item.UpdatedOn = DateTime.Now;
                            //*
                            item.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                            DB.MediaOrderReps.RemoveRange(item.MediaOrderReps);
                            DB.Entry(item).State = EntityState.Modified;

                            foreach (var rep in model.MediaOrderReps)
                            {
                                MediaOrderRep moRep = new MediaOrderRep()
                                {
                                    MediaOrderId = item.MediaOrderId,
                                    RepId = rep.RepId,
                                    TerritoryId = rep.TerritoryId,
                                    Commission = rep.Commission,
                                    Split = rep.Split,
                                    CreatedOn = DateTime.Now,
                                    //*
                                    UpdatedOn = DateTime.Now,
                                    CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                    UpdatedByUserKey = Constants.Created_Update_ByUserKey
                                };
                                lstMediaOrderRepsData.Add(moRep);
                            }
                        }
                        if (lstMediaOrderRepsData.Count > 0)
                            DB.MediaOrderReps.AddRange(lstMediaOrderRepsData);
                        DB.MediaOrders.AddRange(lstMediaOrder);
                        DB.SaveChanges();


                        var costResult = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/ x.BuyId == model.BuyId)
                                                    .GroupBy(x => x.BuyId).Select(x => new
                                                    {
                                                        //  BuyId = x.Key,
                                                        GrossCost = x.Select(y => y.GrossCost).Sum(),
                                                        NetCost = x.Select(y => y.NetCost).Sum(),
                                                        MediaOrderIds = DB.MediaOrders.Where(y => /*y.DeletedInd != true && */ y.BuyId == model.BuyId).OrderByDescending(z => z.MediaOrderId).Select(z => z.MediaOrderId).ToList()
                                                    }).FirstOrDefault();

                        var inventoryDetail = (from m in DB.MediaOrders
                                               where /*m.DeletedInd != true &&*/ m.BuyId == model.BuyId
                                               orderby m.MediaOrderId descending
                                               select new
                                               {
                                                   MediaOrderId = m.MediaOrderId,
                                                   MediaAssetId = m.MediaAssetId,
                                                   AdSizeId = m.RateCardDetail.AdSizeId,
                                                   IssueDateId = m.IssueDateId
                                               }).ToList();

                        foreach (var item in inventoryDetail)
                        {

                            var inventoryId = (from t in DB.MediaInventoryMasters
                                               where /*t.DeletedInd != true &&*/ t.MediaAssetId == item.MediaAssetId && t.AdSizeId == item.AdSizeId && t.IssueDateId == item.IssueDateId
                                               select t.InventoryId).FirstOrDefault();
                            if (inventoryId > 0)
                            {
                                int isRecordExist = DB.MediaInventoryDetails.Where(x => x.InventoryId == inventoryId
                                                                                   && x.MediaOrderId == item.MediaOrderId).Count();
                                if (isRecordExist <= 0)
                                {
                                    MediaInventoryDetail lstInventory = new MediaInventoryDetail
                                    {
                                        InventoryId = inventoryId,
                                        MediaOrderId = item.MediaOrderId,
                                        BlockedInventory = 1
                                    };
                                    DB.MediaInventoryDetails.Add(lstInventory);
                                    DB.SaveChanges();
                                }
                            }
                            else
                            {
                                MediaInventoryMaster objInventoryMaster = new MediaInventoryMaster
                                {
                                    MediaAssetId = item.MediaAssetId,
                                    AdSizeId = item.AdSizeId,
                                    IssueDateId = item.IssueDateId,
                                    CreatedOn = DateTime.Now,
                                    //DeletedInd = false,

                                    //*
                                    UpdatedOn = DateTime.Now,
                                    CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                    UpdatedByUserKey = Constants.Created_Update_ByUserKey,
                                };
                                DB.MediaInventoryMasters.Add(objInventoryMaster);
                                DB.SaveChanges();
                                var invId = (from t in DB.MediaInventoryMasters
                                             where /*t.DeletedInd != true &&*/ t.MediaAssetId == item.MediaAssetId && t.AdSizeId == item.AdSizeId && t.IssueDateId == item.IssueDateId
                                             select t.InventoryId).FirstOrDefault();
                                int isRecordExist = DB.MediaInventoryDetails.Where(x => x.MediaOrderId == item.MediaOrderId).Count();
                                if (isRecordExist <= 0)
                                {
                                    MediaInventoryDetail lstInventory = new MediaInventoryDetail
                                    {
                                        InventoryId = invId,
                                        MediaOrderId = item.MediaOrderId,
                                        BlockedInventory = 1
                                    };
                                    DB.MediaInventoryDetails.Add(lstInventory);
                                    DB.SaveChanges();
                                }
                            }

                        }
                        response.StatusCode = ApiStatus.Ok;
                        response.Data = costResult;
                        lstMediaOrder = new List<MediaOrder>();
                    }
                }
                if (errorMessage != "" && successMessage != "")
                {
                    response.Message = successMessage + "||" + errorMessage;
                    response.StatusCode = ApiStatus.Ok;
                }
                else if (errorMessage != "")
                {
                    response.Message = errorMessage;
                    response.StatusCode = ApiStatus.OutOfInventory;
                }
            }
            catch (DbUpdateException ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;

                var entries = ex.Entries;
                foreach (var entry in entries)
                {
                    //change state to remove it from context 
                    switch (entry.State)
                    {
                        case EntityState.Added:
                            entry.State = EntityState.Detached;
                            break;
                        case EntityState.Modified:
                            entry.CurrentValues.SetValues(entry.OriginalValues);
                            entry.State = EntityState.Unchanged;
                            break;
                        case EntityState.Deleted:
                            entry.State = EntityState.Unchanged;
                            break;
                    }
                    // entry.State = System.Data.Entity.EntityState.Detached;
                }
            }

            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
                foreach (MediaOrder obj in lstMediaOrder)
                {
                    DB.Entry(obj).State = EntityState.Detached;
                }
            }
            return response;
        }
        public ResponseCommon AddImportMediaOrder(PostImportMediaOrderModel model)
        {
            ResponseCommon response = new ResponseCommon();
            List<MediaOrder> lstMediaOrder = new List<MediaOrder>();
            try
            {
                double splitSum = model.MediaOrderReps.Select(x => x.Split).Sum();
                if (splitSum != 100)
                {
                    response.StatusCode = ApiStatus.InvalidInput;
                    response.Message = "Order reps split should be 100";
                    return response;
                }

                foreach (var issueDateId in model.MediaOrderIssueDates)
                {
                    decimal grossAmount = model.RateCardCost;
                    decimal netAmount = model.RateCardCost;
                    MediaOrder order = new MediaOrder()
                    {
                        ImportBuyId = model.ImportBuyId,
                        ImportMediaOrderId = model.ImportMediaOrderId,
                        BuyId = model.BuyId,
                        AdvertiserId = model.AdvertiserId,
                        AgencyId = model.AgencyId,
                        BillToInd = model.BillToInd,
                        BtId = model.BT_ID,
                        StId = model.ST_ID,
                        MediaAssetId = model.MediaAssetId,
                        RateCardId = model.RateCardId,
                        RateCardDetailId = model.RateCardDetailId,
                        ProductCode = model.ProductCode,
                        CampaignName = model.CampaignName,
                        Column = model.Column,
                        Inches = model.Inches,
                        Units = model.Units,
                        ClassifiedText = model.ClassifiedText,
                        PerWordCost = model.PerWordCost,
                        BaseRateCardCost = model.BaseRateCardCost,
                        BaseCalculationCost = model.BaseCalculationCost,
                        RateCardCost = model.RateCardCost,
                        GrossCost = model.RateCardCost,
                        NetCost = model.RateCardCost,
                        FlightStartDate = model.FlightStartDate,
                        FlightEndDate = model.FlightEndDate,
                        IssueDateId = issueDateId,
                        OrderStatus = MediaOrderStatus.Proposal.ToString(),
                        //CreatedByUserKey = Guid.Parse(model.CreatedBy),
                        CreatedByUserKey = Constants.Created_Update_ByUserKey,
                        CreatedOn = model.CreatedDate,

                        //*
                        UpdatedOn = model.CreatedDate,
                        //UpdatedByUserKey = Guid.Parse(model.CreatedBy)
                        UpdatedByUserKey = Constants.Created_Update_ByUserKey
                    };

                    List<MediaOrderRep> lstMediaOrderReps = new List<MediaOrderRep>();
                    foreach (var rep in model.MediaOrderReps)
                    {
                        MediaOrderRep moRep = new MediaOrderRep()
                        {
                            RepId = rep.RepId,
                            TerritoryId = rep.TerritoryId,
                            Commission = rep.Commission,
                            Split = rep.Split,
                            //CreatedByUserKey = Guid.Parse(model.CreatedBy),
                            CreatedByUserKey = Constants.Created_Update_ByUserKey,
                            CreatedOn = model.CreatedDate,

                            //*
                            UpdatedOn = model.CreatedDate,
                            //UpdatedByUserKey = Guid.Parse(model.CreatedBy),
                            UpdatedByUserKey = Constants.Created_Update_ByUserKey
                        };
                        lstMediaOrderReps.Add(moRep);
                    }
                    order.MediaOrderReps = lstMediaOrderReps;
                    lstMediaOrder.Add(order);
                }

                //Edit Existing
                List<MediaOrderRep> lstMediaOrderRepsData = new List<MediaOrderRep>();

                foreach (var item in DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/ x.BuyId == model.BuyId).ToList())
                {
                    var objMediaOrder = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/ x.MediaOrderId == item.MediaOrderId).Select(x => new
                    {
                        x.NetCost,
                        x.GrossCost,
                        x.BaseRateCardCost,
                        x.BaseCalculationCost,
                        x.RateCardCost
                    }).FirstOrDefault();

                    item.AdvertiserId = model.AdvertiserId;
                    item.AgencyId = model.AgencyId;
                    item.BillToInd = model.BillToInd;
                    item.BtId = model.BT_ID;
                    item.StId = model.ST_ID;
                    item.CampaignName = model.CampaignName;
                    item.GrossCost = objMediaOrder.GrossCost;
                    item.NetCost = objMediaOrder.NetCost;
                    item.RateCardCost = objMediaOrder.RateCardCost;
                    item.BaseCalculationCost = objMediaOrder.BaseCalculationCost;
                    item.BaseRateCardCost = objMediaOrder.BaseRateCardCost;
                    item.UpdatedOn = model.CreatedDate;

                    //*
                    item.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    //item.ModifiedDate = DateTime.Now;

                    DB.MediaOrderReps.RemoveRange(item.MediaOrderReps);
                    DB.Entry(item).State = EntityState.Modified;

                    foreach (var rep in model.MediaOrderReps)
                    {
                        MediaOrderRep moRep = new MediaOrderRep()
                        {
                            MediaOrderId = item.MediaOrderId,
                            RepId = rep.RepId,
                            TerritoryId = rep.TerritoryId,
                            Commission = rep.Commission,
                            Split = rep.Split,
                            //CreatedByUserKey = Guid.Parse(model.CreatedBy),
                            CreatedByUserKey = Constants.Created_Update_ByUserKey,
                            CreatedOn = model.CreatedDate,

                            //*
                            UpdatedOn = model.CreatedDate,
                            //UpdatedByUserKey = Guid.Parse(model.CreatedBy)
                            UpdatedByUserKey = Constants.Created_Update_ByUserKey
                        };
                        lstMediaOrderRepsData.Add(moRep);
                    }
                }

                if (lstMediaOrderRepsData.Count > 0)
                    DB.MediaOrderReps.AddRange(lstMediaOrderRepsData);
                DB.MediaOrders.AddRange(lstMediaOrder);
                DB.SaveChanges();

                var costResult = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/ x.BuyId == model.BuyId)
                                            .GroupBy(x => x.BuyId).Select(x => new
                                            {

                                                GrossCost = x.Select(y => y.GrossCost).Sum(),
                                                NetCost = x.Select(y => y.NetCost).Sum(),
                                                MediaOrderIds = DB.MediaOrders.Where(y => /*y.DeletedInd != true &&*/ y.BuyId == model.BuyId).OrderByDescending(z => z.MediaOrderId).Select(z => z.MediaOrderId).ToList()
                                            }).FirstOrDefault();

                response.StatusCode = ApiStatus.Ok;
                response.Data = costResult;

            }

            catch (DbUpdateException ex)
            {

                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
                var entries = ex.Entries;
                foreach (var entry in entries)
                {
                    //change state to remove it from context 
                    switch (entry.State)
                    {
                        case EntityState.Added:
                            entry.State = EntityState.Detached;
                            break;
                        case EntityState.Modified:
                            entry.CurrentValues.SetValues(entry.OriginalValues);
                            entry.State = EntityState.Unchanged;
                            break;
                        case EntityState.Deleted:
                            entry.State = EntityState.Unchanged;
                            break;
                    }
                    // entry.State = System.Data.Entity.EntityState.Detached;
                }
            }

            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);

                    }
                }
            }

            catch (Exception ex)
            {

                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
                foreach (MediaOrder obj in lstMediaOrder)
                {
                    DB.Entry(obj).State = EntityState.Detached;
                }
            }
            return response;
        }

        public ResponseCommon DeleteMediaOrder(int mediaOrderId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                var inventoryDetailId = (from m in DB.MediaInventoryDetails
                                         where m.MediaOrderId == mediaOrderId
                                         select m.InventoryDetailId).FirstOrDefault();
                MediaOrder order = DB.MediaOrders.Where(x => x.MediaOrderId == mediaOrderId /*&& x.DeletedInd != true*/).FirstOrDefault();
                if (order != null)
                {
                    // update order
                    //order.DeletedInd = true;
                    order.UpdatedOn = DateTime.Now;
                    //*
                    order.UpdatedByUserKey = Constants.Created_Update_ByUserKey;
                    DB.Entry(order).State = EntityState.Modified;

                    // update orderlines
                    List<MediaOrderLine> lstMediaOrderLines = order.MediaOrderLines/*.Where(x => x.DeletedInd != true)*/.ToList();
                    if (lstMediaOrderLines.Count > 0)
                    {
                        foreach (var orderLine in lstMediaOrderLines)
                        {
                            //orderLine.DeletedInd = true;
                            orderLine.UpdatedOn = DateTime.Now;
                            //*
                            orderLine.UpdatedByUserKey = Constants.Created_Update_ByUserKey;
                            DB.Entry(orderLine).State = EntityState.Modified;
                        }
                    }

                    // update orderreps
                    List<MediaOrderRep> lstOrderReps = order.MediaOrderReps/*.Where(x => x.DeletedInd != true)*/.ToList();
                    if (lstOrderReps.Count > 0)
                    {
                        foreach (var orderRep in lstOrderReps)
                        {
                            //orderRep.DeletedInd = true;
                            orderRep.UpdatedOn = DateTime.Now;
                            //*
                            orderRep.UpdatedByUserKey = Constants.Created_Update_ByUserKey;
                            DB.Entry(orderRep).State = EntityState.Modified;
                        }
                    }

                    // update production table
                    List<MediaOrderProductionDetail> lstProductionDetails = order.MediaOrderProductionDetails/*.Where(x => x.DeletedInd != true)*/.ToList();
                    if (lstProductionDetails.Count > 0)
                    {
                        foreach (var prodDetail in lstProductionDetails)
                        {
                            //prodDetail.DeletedInd = true;
                            prodDetail.UpdatedOn = DateTime.Now;
                            //*
                            prodDetail.UpdatedByUserKey = Constants.Created_Update_ByUserKey;
                            DB.Entry(prodDetail).State = EntityState.Modified;
                        }
                    }
                    MediaInventoryDetail objInventoryMaster = new MediaInventoryDetail();
                    if (inventoryDetailId != 0)
                    {
                        int inv = Convert.ToInt32(inventoryDetailId);

                        objInventoryMaster = DB.MediaInventoryDetails.Where(x => x.InventoryDetailId == inv).FirstOrDefault();
                        objInventoryMaster.BlockedInventory = 0;

                        DB.Entry(objInventoryMaster).State = EntityState.Modified;
                    }

                    DB.SaveChanges();

                    MediaOrderCostModel costResult = new MediaOrderCostModel();
                    var lstCosts = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/ x.BuyId == order.BuyId)
                                                 .Select(x => new { x.GrossCost, x.NetCost }).ToList();
                    costResult.BuyId = order.BuyId;
                    costResult.GrossCost = lstCosts.Sum(x => x.GrossCost);
                    costResult.NetCost = lstCosts.Sum(x => x.NetCost);

                    response.Data = costResult;
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    response.StatusCode = ApiStatus.NotFound;
                    response.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon ChangeStatus(MediaOrderChangeStatusModel mediaOrderChangeStatusModel)
        {
            ResponseCommon response = new ResponseCommon();
            List<MediaOrder> lstMediaOrder = new List<MediaOrder>();
            try
            {
                lstMediaOrder = DB.MediaOrders.Where(x => mediaOrderChangeStatusModel.MediaOrderIds.Contains(x.MediaOrderId) /*&& x.DeletedInd != true*/).ToList();
                if (lstMediaOrder == null || lstMediaOrder.Count == 0)
                {
                    response.StatusCode = ApiStatus.InvalidInput;
                    response.Message = ApiStatus.InvalidInputMessge;
                }

                foreach (var item in lstMediaOrder)
                {
                    item.OrderStatus = mediaOrderChangeStatusModel.OrderStatus;
                    item.Likelihood = mediaOrderChangeStatusModel.Likelihood == 0 ? item.Likelihood : mediaOrderChangeStatusModel.Likelihood;
                    //*
                    item.UpdatedOn = DateTime.Now;
                    item.UpdatedByUserKey = Constants.Created_Update_ByUserKey;
                    DB.Entry(item).State = EntityState.Modified;
                }
                DB.SaveChanges();

                response.StatusCode = ApiStatus.Ok;
                response.Message = ApiStatus.OkMessge;
            }

            catch (DbUpdateException ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;

                var entries = ex.Entries;
                foreach (var entry in entries)
                {
                    //change state to remove it from context 
                    switch (entry.State)
                    {
                        case EntityState.Added:
                            entry.State = EntityState.Detached;
                            break;
                        case EntityState.Modified:
                            entry.CurrentValues.SetValues(entry.OriginalValues);
                            entry.State = EntityState.Unchanged;
                            break;
                        case EntityState.Deleted:
                            entry.State = EntityState.Unchanged;
                            break;
                    }
                    // entry.State = System.Data.Entity.EntityState.Detached;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;

                foreach (var item in lstMediaOrder)
                {
                    DB.Entry(item).State = EntityState.Detached;
                }
            }
            return response;
        }
        public ResponseCommon GetOrderByMediaOrderId(int mediaOrderId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                EditMediaOrderModel editMediaOrderModel = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/ x.MediaOrderId == mediaOrderId)
                                          .Select(x => new EditMediaOrderModel
                                          {
                                              MediaOrderId = x.MediaOrderId,
                                              BuyId = x.BuyId,
                                              AdvertiserId = x.AdvertiserId,
                                              AgencyId = x.AgencyId,
                                              BillToInd = x.BillToInd,
                                              BT_ID = x.BtId,
                                              ST_ID = x.StId,
                                              MediaOrderReps = (from y in x.MediaOrderReps
                                                                join r in DB.Reps on y.RepId equals r.RepId
                                                                join t in DB.Territories on y.TerritoryId equals t.TerritoryId
                                                                //where y.DeletedInd != true 
                                                                //&& r.DeletedInd != true 
                                                                //&& t.DeletedInd != true
                                                                select new MediaOrderRepsModel
                                                                {
                                                                    MediaOrderId = y.MediaOrderId,
                                                                    MediaOrderRepId = y.MediaOrderRepId,
                                                                    Commission = y.Commission,
                                                                    RepId = y.RepId,
                                                                    Split = y.Split,
                                                                    RepsName = r.RepName,
                                                                    TerritoryId = y.TerritoryId,
                                                                    TerritoryName = t.TerritoryName
                                                                }).ToList(),
                                              MediaAssetId = x.MediaAssetId,
                                              MediaAssetName = x.MediaAsset.MediaAssetName,
                                              RateCardId = x.RateCardId,
                                              RateCardName = x.RateCard.RateCardName,
                                              AdSizeId = x.RateCardDetail.AdSize.AdSizeId,

                                              //* 2020-11-10 Change :  MediaBillingMethod No longer exists
                                              //AdSizeName = (x.RateCard.MediaBillingMethod.MediaBillingMethodName == BillingMethod.Web_CPM.ToString().Replace("_", " ")) ? (x.Units == null ? 0 : (int)x.Units.Value) + " Impressions"
                                              //              : (x.RateCard.MediaBillingMethod.MediaBillingMethodName == BillingMethod.Per_Word.ToString().Replace("_", " ")) ? (x.Units == null ? 0 : (int)x.Units.Value) + " Words"
                                              //              : (x.RateCard.MediaBillingMethod.MediaBillingMethodName == BillingMethod.CPM.ToString().Replace("_", " ")) ? (x.Units == null ? 0 : (int)x.Units.Value) + " Inserts"
                                              //              : x.RateCardDetail.AdSize.AdSizeName,

                                              AdSizeName = (x.RateCard.MediaBillingMethodName == BillingMethod.Web_CPM.ToString().Replace("_", " ")) ? (x.Units == null ? 0 : (int)x.Units.Value) + " Impressions"
                                                            : (x.RateCard.MediaBillingMethodName == BillingMethod.Per_Word.ToString().Replace("_", " ")) ? (x.Units == null ? 0 : (int)x.Units.Value) + " Words"
                                                            : (x.RateCard.MediaBillingMethodName == BillingMethod.CPM.ToString()) ? (x.Units == null ? 0 : (int)x.Units.Value) + " Inserts"
                                                            : x.RateCardDetail.AdSize.AdSizeName,

                                              AdColorId = x.RateCardDetail.AdColor.AdColorId,
                                              AdColorName = x.RateCardDetail.AdColor.AdColorName,
                                              AdTypeId = x.RateCard.AdType.AdTypeId,
                                              AdTypeName = x.RateCard.AdType.AdTypeName,

                                              //* 2020-11-10 Change
                                              //MediaBillingMethodId = x.RateCard.MediaBillingMethodId,
                                              //MediaBillingMathodeName = x.RateCard.MediaBillingMethod.MediaBillingMethodName,                                              
                                              MediaBillingMethodName = x.RateCard.MediaBillingMethodName,

                                              FrequencyId = x.RateCardDetail.FrequencyId,
                                              FrequencyName = x.RateCardDetail.Frequency.FrequencyName,
                                              IssueDateId = x.IssueDateId,
                                              IssueDateName = x.AdIssueDate.IssueName,
                                              OrderStatus = x.OrderStatus,
                                              ProductCode = x.ProductCode,
                                              BaseRateCardCost = x.BaseRateCardCost,
                                              BaseCalculationCost = x.BaseCalculationCost,
                                              RateCardCost = x.RateCardCost,
                                              RateCardDetailId = x.RateCardDetailId,
                                              PageNumber = x.MediaOrderProductionDetails./*Where(y => y.DeletedInd != true).*/Select(y => y.PageNumber).FirstOrDefault(),
                                              HeadLine = x.MediaOrderProductionDetails./*Where(y => y.DeletedInd != true).*/Select(y => y.HeadLine).FirstOrDefault(),
                                              FlightStartDate = x.FlightStartDate,
                                              FlightEndDate = x.FlightEndDate,
                                              CampaignName = x.CampaignName,
                                              Column = x.Column,
                                              Inches = x.Inches,
                                              Units = x.Units,
                                              ClassifiedText = x.ClassifiedText,
                                              PerWordCost = x.PerWordCost,
                                              IsFrozen = x.IsFrozen
                                          }).AsEnumerable()
                                          .Select(x => new EditMediaOrderModel
                                          {
                                              MediaOrderId = x.MediaOrderId,
                                              BuyId = x.BuyId,
                                              AdvertiserId = x.AdvertiserId,
                                              AgencyId = x.AgencyId,
                                              BillToInd = x.BillToInd,
                                              BT_ID = x.BT_ID,
                                              ST_ID = x.ST_ID,

                                              MediaOrderReps = x.MediaOrderReps,

                                              MediaAssetId = x.MediaAssetId,
                                              MediaAssetName = x.MediaAssetName,
                                              RateCardId = x.RateCardId,
                                              RateCardName = x.RateCardName,
                                              AdSizeId = x.AdSizeId,
                                              AdSizeName = x.AdSizeName,
                                              AdColorId = x.AdColorId,
                                              AdColorName = x.AdColorName,
                                              AdTypeId = x.AdTypeId,
                                              AdTypeName = x.AdTypeName,
                                              // 2020-11-10 Getting id from hardcoded list
                                              //MediaBillingMethodId = Get_MediaBillingMethodName_Values
                                              //                      .GetMediaBillingMethodNameValues()
                                              //                      .Where(x1 => x1.MediaBillingMethodName == x.MediaBillingMethodName)
                                              //                      .Select(x1 => x1.MediaBillingMethodId)
                                              //                      .FirstOrDefault(),

                                              // 2020-11-25 Getting id of enum
                                              MediaBillingMethodId = Enum.GetValues(typeof(BillingMethod)).Cast<BillingMethod>()
                                                                    .Where(x1 => x1.ToString() == x.MediaBillingMethodName.Replace(" ", "_"))
                                                                    .Select(x1 => (int)x1)
                                                                    .FirstOrDefault(),

                                              MediaBillingMethodName = x.MediaBillingMethodName,
                                              FrequencyId = x.FrequencyId,
                                              FrequencyName = x.FrequencyName,
                                              IssueDateId = x.IssueDateId,
                                              IssueDateName = x.IssueDateName,
                                              OrderStatus = x.OrderStatus,
                                              ProductCode = x.ProductCode,
                                              BaseRateCardCost = x.BaseRateCardCost,
                                              BaseCalculationCost = x.BaseCalculationCost,
                                              RateCardCost = x.RateCardCost,
                                              RateCardDetailId = x.RateCardDetailId,
                                              PageNumber = x.PageNumber,
                                              HeadLine = x.HeadLine,
                                              FlightStartDate = x.FlightStartDate,
                                              FlightEndDate = x.FlightEndDate,
                                              CampaignName = x.CampaignName,
                                              Column = x.Column,
                                              Inches = x.Inches,
                                              Units = x.Units,
                                              ClassifiedText = x.ClassifiedText,
                                              PerWordCost = x.PerWordCost,
                                              IsFrozen = x.IsFrozen
                                          }).FirstOrDefault();
                if (editMediaOrderModel != null)
                {
                    var orderGrossNetSummary = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/
                    x.BuyId == editMediaOrderModel.BuyId).Select(x => new
                    {
                        x.GrossCost,
                        x.NetCost
                    }).ToList();
                    editMediaOrderModel.GrossCost = orderGrossNetSummary.Sum(x => x.GrossCost);
                    editMediaOrderModel.NetCost = orderGrossNetSummary.Sum(x => x.NetCost);
                    response.StatusCode = ApiStatus.Ok;
                    response.Data = editMediaOrderModel;
                }
                else
                {
                    response.StatusCode = ApiStatus.NotFound;
                    response.Message = "Either Order does not exist or Frozen";
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon UpdateMediaOrder(PutMediaOrderModel model)
        {
            ResponseCommon response = new ResponseCommon();
            var inventoryId = (from m in DB.MediaInventoryDetails
                               where m.MediaOrderId == model.MediaOrderId
                               select m.InventoryId).FirstOrDefault();
            try
            {
                double splitSum = model.MediaOrderReps.Select(x => x.Split).Sum();
                if (splitSum != 100)
                {
                    response.StatusCode = ApiStatus.InvalidInput;
                    response.Message = "Order reps split should be 100";
                    return response;
                }
                List<MediaOrder> lstExistingOrders = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/ x.BuyId == model.BuyId).ToList();
                if (lstExistingOrders == null)
                {
                    response.StatusCode = ApiStatus.NotFound;
                    response.Message = "Invalid BuyId";
                    return response;
                }

                List<MediaOrder> lstMediaOrders = new List<MediaOrder>();
                List<MediaOrderRep> lstMediaOrderRepsData = new List<MediaOrderRep>();
                MediaOrder checkForFrozenOrder = lstExistingOrders.Where(x => x.IsFrozen == true).FirstOrDefault();
                // check for fronzen order if found update 1st screen records with existing order
                if (checkForFrozenOrder != null)
                {
                    // found fronzed order = update exiting with old detail + add new order with old detail
                    foreach (var issueDateId in model.MediaOrderIssueDates)
                    {
                        MediaOrder order = new MediaOrder()
                        {
                            BuyId = checkForFrozenOrder.BuyId,
                            AdvertiserId = checkForFrozenOrder.AdvertiserId,
                            AgencyId = checkForFrozenOrder.AgencyId,
                            BillToInd = checkForFrozenOrder.BillToInd,
                            BtId = checkForFrozenOrder.BtId,
                            StId = checkForFrozenOrder.StId,
                            MediaAssetId = model.MediaAssetId,
                            RateCardId = model.RateCardId,
                            RateCardDetailId = model.RateCardDetailId,
                            ProductCode = model.ProductCode,
                            CampaignName = model.CampaignName,
                            Column = model.Column,
                            Inches = model.Inches,
                            Units = model.Units,
                            ClassifiedText = model.ClassifiedText,
                            PerWordCost = model.PerWordCost,
                            BaseRateCardCost = model.BaseRateCardCost,
                            BaseCalculationCost = model.BaseCalculationCost,
                            RateCardCost = model.RateCardCost,
                            GrossCost = model.RateCardCost,
                            NetCost = model.RateCardCost,
                            FlightStartDate = model.FlightStartDate,
                            FlightEndDate = model.FlightEndDate,
                            IssueDateId = issueDateId,
                            OrderStatus = MediaOrderStatus.Proposal.ToString(),
                            CreatedOn = DateTime.Now,

                            //*
                            UpdatedOn = DateTime.Now,
                            CreatedByUserKey = Constants.Created_Update_ByUserKey,
                            UpdatedByUserKey = Constants.Created_Update_ByUserKey
                        };

                        List<MediaOrderRep> lstMediaOrderReps = new List<MediaOrderRep>();
                        foreach (var rep in checkForFrozenOrder.MediaOrderReps)
                        {
                            MediaOrderRep moRep = new MediaOrderRep()
                            {
                                RepId = rep.RepId,
                                TerritoryId = rep.TerritoryId,
                                Commission = rep.Commission,
                                Split = rep.Split,
                                CreatedOn = DateTime.Now,

                                //*
                                UpdatedOn = DateTime.Now,
                                CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                UpdatedByUserKey = Constants.Created_Update_ByUserKey,
                            };
                            lstMediaOrderReps.Add(moRep);
                        }
                        order.MediaOrderReps = lstMediaOrderReps;
                        lstMediaOrders.Add(order);

                    }
                }
                else
                {
                    // no fronzed order = add new order + update existing order with new details
                    if (model.MediaOrderIssueDates != null)
                    {
                        foreach (var issueDateId in model.MediaOrderIssueDates)
                        {
                            MediaOrder order = new MediaOrder()
                            {
                                BuyId = model.BuyId,
                                AdvertiserId = model.AdvertiserId,
                                AgencyId = model.AgencyId,
                                BillToInd = model.BillToInd,
                                BtId = model.BT_ID,
                                StId = model.ST_ID,
                                MediaAssetId = model.MediaAssetId,
                                RateCardId = model.RateCardId,
                                RateCardDetailId = model.RateCardDetailId,
                                ProductCode = model.ProductCode,
                                CampaignName = model.CampaignName,
                                Column = model.Column,
                                Inches = model.Inches,
                                Units = model.Units,
                                ClassifiedText = model.ClassifiedText,
                                PerWordCost = model.PerWordCost,
                                BaseRateCardCost = model.BaseRateCardCost,
                                BaseCalculationCost = model.BaseCalculationCost,
                                RateCardCost = model.RateCardCost,
                                GrossCost = model.RateCardCost,
                                NetCost = model.RateCardCost,
                                FlightStartDate = model.FlightStartDate,
                                FlightEndDate = model.FlightEndDate,
                                IssueDateId = issueDateId,
                                OrderStatus = MediaOrderStatus.Proposal.ToString(),
                                CreatedOn = DateTime.Now,

                                //*
                                UpdatedOn = DateTime.Now,
                                CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                UpdatedByUserKey = Constants.Created_Update_ByUserKey
                            };

                            List<MediaOrderRep> lstMediaOrderReps = new List<MediaOrderRep>();
                            foreach (var rep in model.MediaOrderReps)
                            {
                                MediaOrderRep moRep = new MediaOrderRep()
                                {
                                    RepId = rep.RepId,
                                    TerritoryId = rep.TerritoryId,
                                    Commission = rep.Commission,
                                    Split = rep.Split,
                                    CreatedOn = DateTime.Now,

                                    //*
                                    UpdatedOn = DateTime.Now,
                                    CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                    UpdatedByUserKey = Constants.Created_Update_ByUserKey
                                };
                                lstMediaOrderReps.Add(moRep);
                            }
                            order.MediaOrderReps = lstMediaOrderReps;
                            lstMediaOrders.Add(order);
                        }
                    }

                    // modify existing orders
                    foreach (var item in lstExistingOrders.Where(x => x.MediaOrderId != model.MediaOrderId).ToList())
                    {
                        item.AdvertiserId = model.AdvertiserId;
                        item.AgencyId = model.AgencyId;
                        item.BtId = model.BT_ID;
                        item.StId = model.ST_ID;
                        item.BillToInd = model.BillToInd;
                        item.UpdatedOn = DateTime.Now;
                        //*
                        item.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                        DB.MediaOrderReps.RemoveRange(item.MediaOrderReps);
                        DB.Entry(item).State = EntityState.Modified;

                        foreach (var rep in model.MediaOrderReps)
                        {
                            MediaOrderRep moRep = new MediaOrderRep()
                            {
                                MediaOrderId = item.MediaOrderId,
                                RepId = rep.RepId,
                                TerritoryId = rep.TerritoryId,
                                Commission = rep.Commission,
                                Split = rep.Split,
                                CreatedOn = DateTime.Now,

                                //*
                                UpdatedOn = DateTime.Now,
                                CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                UpdatedByUserKey = Constants.Created_Update_ByUserKey,
                            };
                            lstMediaOrderRepsData.Add(moRep);
                        }
                    }
                    // modify existing orders end
                }

                // modify current order start
                var updateExistingOrder = lstExistingOrders.Where(x => x.MediaOrderId == model.MediaOrderId && x.IsFrozen == false).FirstOrDefault();
                if (updateExistingOrder != null)
                {
                    if (checkForFrozenOrder == null)
                    {
                        updateExistingOrder.AdvertiserId = model.AdvertiserId;
                        updateExistingOrder.AgencyId = model.AgencyId;
                        updateExistingOrder.BtId = model.BT_ID;
                        updateExistingOrder.StId = model.ST_ID;
                        updateExistingOrder.BillToInd = model.BillToInd;

                        DB.MediaOrderReps.RemoveRange(updateExistingOrder.MediaOrderReps);

                        foreach (var rep in model.MediaOrderReps)
                        {
                            MediaOrderRep moRep = new MediaOrderRep()
                            {
                                MediaOrderId = updateExistingOrder.MediaOrderId,
                                RepId = rep.RepId,
                                TerritoryId = rep.TerritoryId,
                                Commission = rep.Commission,
                                Split = rep.Split,
                                CreatedOn = DateTime.Now,

                                //*
                                UpdatedOn = DateTime.Now,
                                CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                UpdatedByUserKey = Constants.Created_Update_ByUserKey
                            };
                            lstMediaOrderRepsData.Add(moRep);
                        }
                    }
                    updateExistingOrder.MediaAssetId = model.MediaAssetId;
                    updateExistingOrder.RateCardId = model.RateCardId;
                    updateExistingOrder.RateCardDetailId = model.RateCardDetailId;
                    updateExistingOrder.ProductCode = model.ProductCode;
                    updateExistingOrder.CampaignName = model.CampaignName;
                    updateExistingOrder.Column = model.Column;
                    updateExistingOrder.Inches = model.Inches;
                    updateExistingOrder.Units = model.Units;
                    updateExistingOrder.ClassifiedText = model.ClassifiedText;
                    updateExistingOrder.PerWordCost = model.PerWordCost;
                    updateExistingOrder.BaseRateCardCost = model.BaseRateCardCost;
                    updateExistingOrder.BaseCalculationCost = model.BaseCalculationCost;
                    updateExistingOrder.RateCardCost = model.RateCardCost;
                    updateExistingOrder.FlightStartDate = model.FlightStartDate;
                    updateExistingOrder.FlightEndDate = model.FlightEndDate;
                    updateExistingOrder.UpdatedOn = DateTime.Now;

                    //*                    
                    updateExistingOrder.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    // recalculate gross/net cost
                    double finalGrossCostOfOrder = (double)updateExistingOrder.RateCardCost;
                    double finalNetCostOfOrder = (double)updateExistingOrder.RateCardCost;
                    List<MediaOrderLine> lstBaseMediaOrderLines = DB.MediaOrderLines.Where(x => /*x.DeletedInd != true &&*/ x.MediaOrderId == model.MediaOrderId).OrderBy(x => x.SequenceNumber).ToList();
                    List<MediaOrderLine> currentMediaOrderLine = new List<MediaOrderLine>();
                    currentMediaOrderLine = lstBaseMediaOrderLines.Where(x => x.GrossInd == true && x.MediaOrderId == updateExistingOrder.MediaOrderId)
                                                       .OrderBy(x => x.SequenceNumber).ToList();
                    CommonUtility.CalculateNetCost(ref finalNetCostOfOrder, currentMediaOrderLine);
                    // calculate gross cost
                    finalGrossCostOfOrder = finalNetCostOfOrder;
                    currentMediaOrderLine = lstBaseMediaOrderLines.Where(x => x.GrossInd != true && x.MediaOrderId == updateExistingOrder.MediaOrderId)
                                                        .OrderBy(x => x.SequenceNumber)
                                                        .ToList();
                    CommonUtility.CalculateNetCost(ref finalNetCostOfOrder, currentMediaOrderLine);
                    // recalculate gross/net cost end

                    updateExistingOrder.GrossCost = Convert.ToDecimal(finalGrossCostOfOrder);
                    updateExistingOrder.NetCost = Convert.ToDecimal(finalNetCostOfOrder);

                    DB.Entry(updateExistingOrder).State = EntityState.Modified;
                }
                // modify current order end

                if (lstMediaOrders.Count > 0)
                    DB.MediaOrders.AddRange(lstMediaOrders);

                if (lstMediaOrderRepsData.Count > 0)
                    DB.MediaOrderReps.AddRange(lstMediaOrderRepsData);
                MediaInventoryMaster objInventoryMaster = new MediaInventoryMaster();
                if (inventoryId != null)
                {
                    int inv = Convert.ToInt32(inventoryId);
                    objInventoryMaster = DB.MediaInventoryMasters.Where(x => x.InventoryId == inv).FirstOrDefault();
                    objInventoryMaster.AdSizeId = model.AdSizeId;
                    objInventoryMaster.UpdatedOn = DateTime.Now;

                    //*
                    objInventoryMaster.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objInventoryMaster).State = EntityState.Modified;
                }
                DB.SaveChanges();

                // calclulate cost after saving all data
                var costResult = DB.MediaOrders.Where(x => /* x.DeletedInd != true && */ x.BuyId == model.BuyId)
                                        .GroupBy(x => x.BuyId).Select(x => new
                                        {
                                            // BuyId = x.Key,
                                            GrossCost = x.Select(y => y.GrossCost).Sum(),
                                            NetCost = x.Select(y => y.NetCost).Sum()
                                        }).FirstOrDefault();


                response.StatusCode = ApiStatus.Ok;
                response.Data = costResult;

            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommonForDatatable GetOrdersForPostOrder()
        {
            GetOrdersForPostOrderbyFilterOption model = new GetOrdersForPostOrderbyFilterOption();
            //ResponseCommon response = new ResponseCommon();
            ResponseCommonForDatatable response = new ResponseCommonForDatatable();
            List<MediaOrdersMediaOrderIdWise> lstMediaOrderModel = new List<MediaOrdersMediaOrderIdWise>();
            int recordsTotal = 0;
            try
            {
                var context = HttpContext.Current;
                var currentRequest = HttpContext.Current.Request;

                int mediaAssetId = currentRequest["MediaAssetId"] == "" ? 0 : Convert.ToInt32(currentRequest["MediaAssetId"]);
                int issueDateId = currentRequest["IssueDateId"] == "" ? 0 : Convert.ToInt32(currentRequest["IssueDateId"]);
                int adTypeId = currentRequest["AdTypeId"] == "" ? 0 : Convert.ToInt32(currentRequest["AdTypeId"]);
                int advertiserId = currentRequest["AdvertiserId"] == "" ? 0 : Convert.ToInt32(currentRequest["AdvertiserId"]);

                model.MediaAssetId = mediaAssetId;
                model.IssueDateId = issueDateId;
                model.AdTypeId = adTypeId;
                model.AdvertiserId = advertiserId;

                string draw = currentRequest["draw"];
                string start = currentRequest["start"];
                string length = currentRequest["length"];
                string pageSize = (length != null ? Convert.ToInt32(length) : 0).ToString();
                string skip = (start != null ? Convert.ToInt32(start) : 0).ToString();

                if (model != null)
                {


                    List<asi_GetDataForPostOrder_Result> lstSpResultMediaAdvertiserTest = DB.GetDataForPostOrder(mediaAssetId, issueDateId, adTypeId, advertiserId, APIMediaOrderStatus.Run, skip, pageSize).ToList();
                    foreach (var item in lstSpResultMediaAdvertiserTest)
                    {
                        lstMediaOrderModel.Add(new MediaOrdersMediaOrderIdWise
                        {
                            Totalcount = item.totalcount,
                            BuyId = (int)item.BuyId,
                            MediaOrderId = (int)item.MediaOrderId,
                            MediaAssetId = (int)item.MediaAssetId,
                            IssueDateId = (int)item.IssueDateId,
                            AdTypeId = item.AdTypeId,
                            AdTypeName = item.AdTypeName,
                            AdcolorId = item.AdColorId,
                            AdcolorName = item.AdColorName,
                            AdSizeId = item.AdSizeId,
                            AdSizeName = item.AdSizeName,
                            FrequencyId = item.FrequencyId,
                            FrequencyName = item.FrequencyName,
                            AdvertiserId = item.AdvertiserId,
                            BT_ID = item.BtId,
                            ST_ID = item.StId,
                            ProductCode = item.ProductCode,
                            RateCardCost = (decimal)item.RateCardCost,
                            GrossCost = (decimal)item.GrossCost,
                            NetCost = (decimal)item.NetCost,
                            FlightStartDate = item.FlightStartDate,
                            FlightEndDate = item.FlightEndDate,
                            MediaAssetName = item.MediaAssetName
                        });
                    }




                    recordsTotal = Convert.ToInt32(lstMediaOrderModel[0].Totalcount);

                    if (lstMediaOrderModel.Count > 0)
                    {

                        response.Draw = Convert.ToInt32(draw);
                        response.RecordsTotal = recordsTotal;
                        response.RecordsFiltered = recordsTotal;
                        response.Data = lstMediaOrderModel;
                    }
                    else
                    {

                        response.Draw = Convert.ToInt32(draw);
                        response.RecordsTotal = recordsTotal;
                        response.RecordsFiltered = recordsTotal;
                        response.Data = lstMediaOrderModel;
                    }

                }
            }

            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);

                    }
                }
            }
            catch (Exception ex)
            {

                var context = HttpContext.Current;
                var currentRequest = HttpContext.Current.Request;
                string draw = currentRequest["draw"];
                recordsTotal = lstMediaOrderModel.Count();
                response.Draw = Convert.ToInt32(draw);
                response.RecordsTotal = recordsTotal;
                response.RecordsFiltered = recordsTotal;
                response.Data = lstMediaOrderModel;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return response;
        }

        public ResponseCommon GetAllAdvertiserForPostOrder()
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                var lstAdvertiser = (from ord in DB.MediaOrders
                                     where /*ord.DeletedInd != true &&*/ ord.IsFrozen == false
                                     orderby ord.MediaOrderId descending
                                     group ord by ord.AdvertiserId into g
                                     select new
                                     {
                                         AdvertiserId = g.Select(y => y.AdvertiserId).FirstOrDefault(),
                                     }).ToList();

                if (lstAdvertiser.Count > 0)
                {
                    response.StatusCode = ApiStatus.Ok;
                    response.Data = lstAdvertiser;
                }
                else
                {
                    response.StatusCode = ApiStatus.NoRecords;
                    response.Message = ApiStatus.NoRecordsMessge;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }

        public class TempPostOrderModel1
        {
            public int BuyId { get; set; }
            public string OrderStatus { get; set; }
            public List<PostOrderAdType> LstAdTypes { get; set; }
            public List<PostOrderMediaAsset> LstMediaAssets { get; set; }
            public List<PostOrderIssueDate> LstIssueDates { get; set; }
            public List<PostOrderAdvertiser> LstAdvertisers { get; set; }
        }

        public ResponseCommon FillDropdownForPostOrder(GetOrdersForPostOrderbyFilterOption model)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                var command = DB.Databases.Connection.CreateCommand();
                command.CommandText = "[dbo].[asi_FillPostOrderDropDown]";
                command.CommandType = CommandType.StoredProcedure;
                List<SqlParameter> sqlParameters = new List<SqlParameter>()
                {
                     new SqlParameter() {ParameterName = "@mediaAssetId", SqlDbType = SqlDbType.Int, Value= model.MediaAssetId == 0 ? 0 : model.MediaAssetId.GetHashCode() },
                     new SqlParameter() {ParameterName = "@issueDateId", SqlDbType = SqlDbType.Int, Value= model.IssueDateId == 0 ? 0 : model.IssueDateId},
                     new SqlParameter() {ParameterName = "@adTypeId", SqlDbType = SqlDbType.Int, Value= model.AdTypeId == 0 ? 0 : model.AdTypeId},
                     new SqlParameter() {ParameterName = "@advertiserId", SqlDbType = SqlDbType.Int, Value= model.AdvertiserId == 0 ? 0 : model.AdvertiserId},
                     new SqlParameter() {ParameterName = "@status", SqlDbType = SqlDbType.VarChar, Value= model.Status ??""},
                     new SqlParameter() {ParameterName = "@productionStatusId", SqlDbType = SqlDbType.Int, Value= model.ProductionStatusId == 0 ? 0 : model.ProductionStatusId},
                     new SqlParameter() {ParameterName = "@isCurrentYear", SqlDbType = SqlDbType.Int, Value= model.IsCurrentYear == true ? 1 : 0},
                };

                command.Parameters.AddRange(sqlParameters.ToArray());

                DB.Databases.Connection.Open();
                var reader = command.ExecuteReader();

                List<PostOrderMediaAsset> listOfMediaAsset =
                ((IObjectContextAdapter)DB).ObjectContext.Translate<PostOrderMediaAsset>
                (reader).ToList();
                reader.NextResult();

                List<PostOrderIssueDate> listOfIssueDate =
                    ((IObjectContextAdapter)DB).ObjectContext.Translate<PostOrderIssueDate>
               (reader).ToList();
                reader.NextResult();

                List<PostOrderAdType> listOfAddTypes =
                 ((IObjectContextAdapter)DB).ObjectContext.Translate<PostOrderAdType>
               (reader).ToList();
                reader.NextResult();

                List<PostOrderAdvertiser> listOfAdvertiser =
                ((IObjectContextAdapter)DB).ObjectContext.Translate<PostOrderAdvertiser>
                (reader).ToList();

                FillPostOrderDropdownsModel objPostOrderDropdown1 = new FillPostOrderDropdownsModel();
                List<FillPostOrderDropdownsModel> lstPostOrderDropdown1 = new List<FillPostOrderDropdownsModel>();

                //objPostOrderDropdown1.BuyId = reader[0].ToString();

                objPostOrderDropdown1.MediaAssets = listOfMediaAsset;
                objPostOrderDropdown1.IssueDates = listOfIssueDate;
                objPostOrderDropdown1.AdTypes = listOfAddTypes;
                objPostOrderDropdown1.Advertisers = listOfAdvertiser;
                lstPostOrderDropdown1.Add(objPostOrderDropdown1);
                DB.Databases.Connection.Close();


                if (lstPostOrderDropdown1.Count > 0)
                {
                    response.StatusCode = ApiStatus.Ok;
                    response.Data = lstPostOrderDropdown1;
                }
                else
                {
                    response.StatusCode = ApiStatus.NoRecords;
                    response.Message = ApiStatus.NoRecordsMessge;
                }
            }
            catch (Exception ex)
            {
                DB.Databases.Connection.Close();
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }

        public ResponseCommon GetOrdersBuyIdWiseForCopy(int buyId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {

                List<MediaOrderIssueDatesModel> lstMediaOrderModel = new List<MediaOrderIssueDatesModel>();
                lstMediaOrderModel = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/ x.BuyId == buyId)
                                        .Select(x => new MediaOrderIssueDatesModel
                                        {
                                            MediaOrderId = x.MediaOrderId,
                                            BuyId = x.BuyId,
                                            MediaAssetId = x.MediaAssetId,
                                            AdvertiserId = x.AdvertiserId,
                                            MediaAssetName = x.MediaAsset.MediaAssetName,
                                            IssueDateId = x.IssueDateId,
                                            IssueDate = x.AdIssueDate.CoverDate,//.ToString("MM/dd/yyyy"),
                                            IssueDates = DB.AdIssueDates.Where(z => /*z.DeletedInd != true &&*/ z.MediaAssetId == x.MediaAssetId)
                                            .Select(t => new PostOrderIssueDate
                                            {
                                                IssueDateId = t.IssueDateId,
                                                CoverDate = t.CoverDate,
                                            }).ToList(),
                                            AdSizeId = x.RateCardDetail.AdSize.AdSizeId,
                                            AdSizeName = x.RateCardDetail.AdSize.AdSizeName,
                                            AdTypeId = x.RateCard.AdType.AdTypeId,
                                            AdTypeName = x.RateCard.AdType.AdTypeName,
                                            AdColorId = x.RateCardDetail.AdColor.AdColorId,
                                            AdColorName = x.RateCardDetail.AdColor.AdColorName,
                                            FrequencyId = x.RateCardDetail.FrequencyId,
                                            FrequencyName = x.RateCardDetail.Frequency.FrequencyName,
                                        }).ToList();

                if (lstMediaOrderModel.Count > 0)
                {
                    response.StatusCode = ApiStatus.Ok;
                    response.Data = lstMediaOrderModel;
                }
                else
                {
                    response.StatusCode = ApiStatus.NoRecords;
                    response.Message = ApiStatus.NoRecordsMessge;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon PostOrdersForCopy(MediaOrderCopyModel[] model)
        {
            ResponseCommon response = new ResponseCommon();
            bool flag = true;

            var errorMessage = "";
            var successMessage = "";
            var errorStatus = 0;
            try
            {
                if (model.Length > 0)
                {
                    var objModel = model;
                    List<MediaOrder> lstMediaOrder = new List<MediaOrder>();
                    var newBuyId = Convert.ToInt32(GetGenerateBuyId());
                    foreach (var item in objModel)
                    {
                        if (item.MediaOrderId != 0 && item != null)
                        {
                            MediaOrder dbMediaOrder = null;
                            dbMediaOrder = DB.MediaOrders.Where(x => x.MediaOrderId == item.MediaOrderId).FirstOrDefault();

                            if (dbMediaOrder != null)
                            {
                                MediaOrder order = new MediaOrder()
                                {
                                    BuyId = newBuyId,
                                    AdvertiserId = dbMediaOrder.AdvertiserId,
                                    AgencyId = dbMediaOrder.AgencyId,
                                    BillToInd = dbMediaOrder.BillToInd,
                                    BtId = dbMediaOrder.BtId,
                                    StId = dbMediaOrder.StId,
                                    MediaAssetId = dbMediaOrder.MediaAssetId,
                                    RateCardId = dbMediaOrder.RateCardId,
                                    RateCardDetailId = dbMediaOrder.RateCardDetailId,
                                    ProductCode = dbMediaOrder.ProductCode,
                                    CampaignName = dbMediaOrder.CampaignName,
                                    Column = dbMediaOrder.Column,
                                    Inches = dbMediaOrder.Inches,
                                    Units = dbMediaOrder.Units,
                                    ClassifiedText = dbMediaOrder.ClassifiedText,
                                    PerWordCost = dbMediaOrder.PerWordCost,
                                    BaseRateCardCost = dbMediaOrder.BaseRateCardCost,
                                    BaseCalculationCost = dbMediaOrder.BaseCalculationCost,
                                    RateCardCost = dbMediaOrder.RateCardCost,
                                    GrossCost = dbMediaOrder.GrossCost,
                                    NetCost = dbMediaOrder.NetCost,
                                    FlightStartDate = dbMediaOrder.FlightStartDate,
                                    FlightEndDate = dbMediaOrder.FlightEndDate,
                                    IssueDateId = item.IssueDateId,
                                    OrderStatus = MediaOrderStatus.Proposal.ToString(),
                                    CreatedOn = DateTime.Now,

                                    //*
                                    UpdatedOn = DateTime.Now,
                                    CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                    UpdatedByUserKey = Constants.Created_Update_ByUserKey,
                                };
                                var adsizeId = DB.RateCardDetails.Where(x => x.RateCardDetailId == order.RateCardDetailId).Select(z => z.AdSizeId).FirstOrDefault();
                                var inventoryMaster = (from m in DB.MediaInventoryMasters
                                                       where m.IssueDateId == order.IssueDateId && m.AdSizeId == adsizeId && m.MediaAssetId == order.MediaAssetId /*&& m.DeletedInd == false*/
                                                       select new { m.TotalInventory, m.InventoryId }).FirstOrDefault();
                                if (inventoryMaster != null && inventoryMaster.TotalInventory != null)
                                {
                                    var blockedInventory = (from mid in DB.MediaInventoryDetails
                                                            where mid.InventoryId == inventoryMaster.InventoryId
                                                            group mid by mid.InventoryId into g
                                                            select new { total = g.Sum(s => s.BlockedInventory) }).FirstOrDefault();
                                    //  var remainInventory = Convert.ToInt32(InventoryMaster.TotalInventory) - Convert.ToInt32(blockedInventory.total);
                                    if (blockedInventory != null)
                                    {

                                        if (Convert.ToInt32(blockedInventory.total) >= Convert.ToInt32(inventoryMaster.TotalInventory))
                                        {
                                            flag = false;
                                            var coverdate = (from aid in DB.AdIssueDates
                                                             where aid.IssueDateId == order.IssueDateId
                                                             select aid.CoverDate).FirstOrDefault();
                                            var adSize = (from ads in DB.AdSizes
                                                          where ads.AdSizeId == adsizeId
                                                          select ads.AdSizeName).FirstOrDefault();


                                            if (coverdate != null && adSize != null && adSize != "")
                                            {

                                                if (errorMessage != "")
                                                {
                                                    errorMessage = errorMessage + "<br/>" + "All ad spaces for " + Convert.ToString(coverdate.Month + "/" + coverdate.Day + "/" + coverdate.Year) + " issue date and " + adSize + " size are out of inventory";
                                                    errorStatus = ApiStatus.OutOfInventory;
                                                }
                                                else
                                                {
                                                    errorMessage = "All ad spaces for " + Convert.ToString(coverdate.Month + "/" + coverdate.Day + "/" + coverdate.Year) + " issue date and " + adSize + " size are out of inventory";
                                                    errorStatus = ApiStatus.OutOfInventory;
                                                }
                                            }

                                        }
                                        else
                                        {
                                            var coverdate = (from aid in DB.AdIssueDates
                                                             where aid.IssueDateId == order.IssueDateId
                                                             select aid.CoverDate).FirstOrDefault();
                                            var adSize = (from ads in DB.AdSizes
                                                          where ads.AdSizeId == adsizeId
                                                          select ads.AdSizeName).FirstOrDefault();
                                            if (coverdate != null && adSize != null && adSize != "")
                                            {

                                                if (successMessage != "")
                                                {
                                                    successMessage = successMessage + "<br/>" + "All ad spaces for " + Convert.ToString(coverdate.Month + "/" + coverdate.Day + "/" + coverdate.Year) + " issue date and " + adSize + " size has been booked";

                                                }
                                                else
                                                {
                                                    successMessage = "All ad spaces for " + Convert.ToString(coverdate.Month + "/" + coverdate.Day + "/" + coverdate.Year) + " issue date and " + adSize + " size has been booked";

                                                }
                                            }
                                            // successMessage = "All ad spaces for " + successIssueDate + " issue date and " + successAdsizes + " size are copy to the Buy ID";

                                            List<MediaOrderRep> lstMediaOrderReps = new List<MediaOrderRep>();
                                            foreach (var rep in dbMediaOrder.MediaOrderReps)
                                            {
                                                MediaOrderRep moRep = new MediaOrderRep()
                                                {
                                                    RepId = rep.RepId,
                                                    TerritoryId = rep.TerritoryId,
                                                    Commission = rep.Commission,
                                                    Split = rep.Split,
                                                    CreatedOn = DateTime.Now,

                                                    //*
                                                    UpdatedOn = DateTime.Now,
                                                    CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                                    UpdatedByUserKey = Constants.Created_Update_ByUserKey,
                                                };
                                                lstMediaOrderReps.Add(moRep);
                                            }
                                            order.MediaOrderReps = lstMediaOrderReps;

                                            List<MediaOrderLine> dbMediaOrderLines = dbMediaOrder.MediaOrderLines.
                                                /*Where(x => x.DeletedInd != true).*/
                                                OrderByDescending(x => (x.MediaOrderLineId & x.SequenceNumber)).ToList();
                                            List<MediaOrderLine> lstMediaOrderLines = new List<MediaOrderLine>();
                                            foreach (var line in dbMediaOrderLines)
                                            {
                                                MediaOrderLine moLine = new MediaOrderLine()
                                                {
                                                    SequenceNumber = line.SequenceNumber,
                                                    AdAdjustmentName = line.AdAdjustmentName,
                                                    AmountPercent = line.AmountPercent,
                                                    SurchargeDiscount = line.SurchargeDiscount,
                                                    AdjustmentValue = line.AdjustmentValue,
                                                    ProductCode = line.ProductCode,
                                                    GrossInd = line.GrossInd,
                                                    CreatedOn = DateTime.Now,

                                                    //*
                                                    UpdatedOn = DateTime.Now,
                                                    CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                                    UpdatedByUserKey = Constants.Created_Update_ByUserKey,
                                                };
                                                lstMediaOrderLines.Add(moLine);
                                            }
                                            order.MediaOrderLines = lstMediaOrderLines;

                                            List<MediaOrderProductionDetail> dbMediaOrderProductionDetails = dbMediaOrder.MediaOrderProductionDetails
                                                //.Where(x => x.DeletedInd != true)
                                                .OrderByDescending(x => x.MediaOrderProductionDetailId).ToList();
                                            List<MediaOrderProductionDetail> lstMediaOrderProductionDetails = new List<MediaOrderProductionDetail>();
                                            foreach (var pDetails in dbMediaOrderProductionDetails)
                                            {
                                                MediaOrderProductionDetail mopdetails = new MediaOrderProductionDetail()
                                                {

                                                    NewPickupInd = pDetails.NewPickupInd,
                                                    PickupMediaOrderId = pDetails.PickupMediaOrderId,
                                                    CreateExpectedInd = pDetails.CreateExpectedInd,
                                                    ChangesInd = pDetails.ChangesInd,
                                                    MaterialExpectedDate = pDetails.MaterialExpectedDate,
                                                    TrackingNumber = pDetails.TrackingNumber,
                                                    OnHandInd = pDetails.OnHandInd,
                                                    AdvertiserAgencyInd = pDetails.AdvertiserAgencyInd,
                                                    ProductionStatusId = pDetails.ProductionStatusId,
                                                    MaterialContactId = pDetails.MaterialContactId,
                                                    OriginalFile = pDetails.OriginalFile,
                                                    ProofFile = pDetails.ProofFile,
                                                    FinalFile = pDetails.FinalFile,
                                                    WebAdUrl = pDetails.WebAdUrl,
                                                    TearSheets = pDetails.TearSheets,
                                                    HeadLine = pDetails.HeadLine,
                                                    PositionId = pDetails.PositionId,
                                                    SeparationId = pDetails.SeparationId,
                                                    PageNumber = pDetails.PageNumber,
                                                    ProductionComment = pDetails.ProductionComment,
                                                    CreatedOn = DateTime.Now,

                                                    //*
                                                    UpdatedOn = DateTime.Now,
                                                    CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                                    UpdatedByUserKey = Constants.Created_Update_ByUserKey,
                                                };
                                                lstMediaOrderProductionDetails.Add(mopdetails);
                                            }
                                            order.MediaOrderProductionDetails = lstMediaOrderProductionDetails;

                                            lstMediaOrder.Add(order);
                                            flag = true;
                                        }
                                    }
                                    else
                                    {
                                        List<MediaOrderRep> lstMediaOrderReps = new List<MediaOrderRep>();
                                        foreach (var rep in dbMediaOrder.MediaOrderReps)
                                        {
                                            MediaOrderRep moRep = new MediaOrderRep()
                                            {
                                                RepId = rep.RepId,
                                                TerritoryId = rep.TerritoryId,
                                                Commission = rep.Commission,
                                                Split = rep.Split,
                                                CreatedOn = DateTime.Now,

                                                //*
                                                UpdatedOn = DateTime.Now,
                                                CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                                UpdatedByUserKey = Constants.Created_Update_ByUserKey,
                                            };
                                            lstMediaOrderReps.Add(moRep);
                                        }
                                        order.MediaOrderReps = lstMediaOrderReps;

                                        List<MediaOrderLine> dbMediaOrderLines = dbMediaOrder.MediaOrderLines./*Where(x => x.DeletedInd != true)*/
                                            OrderByDescending(x => (x.MediaOrderLineId & x.SequenceNumber)).ToList();
                                        List<MediaOrderLine> lstMediaOrderLines = new List<MediaOrderLine>();
                                        foreach (var line in dbMediaOrderLines)
                                        {
                                            MediaOrderLine moLine = new MediaOrderLine()
                                            {
                                                SequenceNumber = line.SequenceNumber,
                                                AdAdjustmentName = line.AdAdjustmentName,
                                                AmountPercent = line.AmountPercent,
                                                SurchargeDiscount = line.SurchargeDiscount,
                                                AdjustmentValue = line.AdjustmentValue,
                                                ProductCode = line.ProductCode,
                                                GrossInd = line.GrossInd,
                                                CreatedOn = DateTime.Now,

                                                //*
                                                UpdatedOn = DateTime.Now,
                                                CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                                UpdatedByUserKey = Constants.Created_Update_ByUserKey
                                            };
                                            lstMediaOrderLines.Add(moLine);
                                        }
                                        order.MediaOrderLines = lstMediaOrderLines;

                                        List<MediaOrderProductionDetail> dbMediaOrderProductionDetails = dbMediaOrder.MediaOrderProductionDetails.
                                            /*Where(x => x.DeletedInd != true)*/OrderByDescending(x => x.MediaOrderProductionDetailId).ToList();
                                        List<MediaOrderProductionDetail> lstMediaOrderProductionDetails = new List<MediaOrderProductionDetail>();
                                        foreach (var pDetails in dbMediaOrderProductionDetails)
                                        {
                                            MediaOrderProductionDetail mopdetails = new MediaOrderProductionDetail()
                                            {
                                                NewPickupInd = pDetails.NewPickupInd,
                                                PickupMediaOrderId = pDetails.PickupMediaOrderId,
                                                CreateExpectedInd = pDetails.CreateExpectedInd,
                                                ChangesInd = pDetails.ChangesInd,
                                                MaterialExpectedDate = pDetails.MaterialExpectedDate,
                                                TrackingNumber = pDetails.TrackingNumber,
                                                OnHandInd = pDetails.OnHandInd,
                                                AdvertiserAgencyInd = pDetails.AdvertiserAgencyInd,
                                                ProductionStatusId = pDetails.ProductionStatusId,
                                                MaterialContactId = pDetails.MaterialContactId,
                                                OriginalFile = pDetails.OriginalFile,
                                                ProofFile = pDetails.ProofFile,
                                                FinalFile = pDetails.FinalFile,
                                                WebAdUrl = pDetails.WebAdUrl,
                                                TearSheets = pDetails.TearSheets,
                                                HeadLine = pDetails.HeadLine,
                                                PositionId = pDetails.PositionId,
                                                SeparationId = pDetails.SeparationId,
                                                PageNumber = pDetails.PageNumber,
                                                ProductionComment = pDetails.ProductionComment,
                                                CreatedOn = DateTime.Now,

                                                //*
                                                UpdatedOn = DateTime.Now,
                                                CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                                UpdatedByUserKey = Constants.Created_Update_ByUserKey
                                            };
                                            lstMediaOrderProductionDetails.Add(mopdetails);
                                        }
                                        order.MediaOrderProductionDetails = lstMediaOrderProductionDetails;

                                        lstMediaOrder.Add(order);
                                        flag = true;
                                    }
                                }
                                else
                                {
                                    List<MediaOrderRep> lstMediaOrderReps = new List<MediaOrderRep>();
                                    foreach (var rep in dbMediaOrder.MediaOrderReps)
                                    {
                                        MediaOrderRep moRep = new MediaOrderRep()
                                        {
                                            RepId = rep.RepId,
                                            TerritoryId = rep.TerritoryId,
                                            Commission = rep.Commission,
                                            Split = rep.Split,
                                            CreatedOn = DateTime.Now,

                                            //*
                                            UpdatedOn = DateTime.Now,
                                            CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                            UpdatedByUserKey = Constants.Created_Update_ByUserKey,
                                        };
                                        lstMediaOrderReps.Add(moRep);
                                    }
                                    order.MediaOrderReps = lstMediaOrderReps;

                                    List<MediaOrderLine> dbMediaOrderLines = dbMediaOrder.MediaOrderLines.
                                        /*Where(x => x.DeletedInd != true)*/
                                        OrderByDescending(x => (x.MediaOrderLineId & x.SequenceNumber)).ToList();
                                    List<MediaOrderLine> lstMediaOrderLines = new List<MediaOrderLine>();
                                    foreach (var line in dbMediaOrderLines)
                                    {
                                        MediaOrderLine moLine = new MediaOrderLine()
                                        {
                                            SequenceNumber = line.SequenceNumber,
                                            AdAdjustmentName = line.AdAdjustmentName,
                                            AmountPercent = line.AmountPercent,
                                            SurchargeDiscount = line.SurchargeDiscount,
                                            AdjustmentValue = line.AdjustmentValue,
                                            ProductCode = line.ProductCode,
                                            GrossInd = line.GrossInd,
                                            CreatedOn = DateTime.Now,

                                            //*
                                            UpdatedOn = DateTime.Now,
                                            CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                            UpdatedByUserKey = Constants.Created_Update_ByUserKey
                                        };
                                        lstMediaOrderLines.Add(moLine);
                                    }
                                    order.MediaOrderLines = lstMediaOrderLines;

                                    List<MediaOrderProductionDetail> dbMediaOrderProductionDetails = dbMediaOrder.MediaOrderProductionDetails
                                        /*.Where(x => x.DeletedInd != true)*/.OrderByDescending(x => x.MediaOrderProductionDetailId).ToList();
                                    List<MediaOrderProductionDetail> lstMediaOrderProductionDetails = new List<MediaOrderProductionDetail>();
                                    foreach (var pDetails in dbMediaOrderProductionDetails)
                                    {
                                        MediaOrderProductionDetail mopdetails = new MediaOrderProductionDetail()
                                        {
                                            // MediaOrderId = pDetails.MediaOrderId,
                                            NewPickupInd = pDetails.NewPickupInd,
                                            PickupMediaOrderId = pDetails.PickupMediaOrderId,
                                            CreateExpectedInd = pDetails.CreateExpectedInd,
                                            ChangesInd = pDetails.ChangesInd,
                                            MaterialExpectedDate = pDetails.MaterialExpectedDate,
                                            TrackingNumber = pDetails.TrackingNumber,
                                            OnHandInd = pDetails.OnHandInd,
                                            AdvertiserAgencyInd = pDetails.AdvertiserAgencyInd,
                                            ProductionStatusId = pDetails.ProductionStatusId,
                                            MaterialContactId = pDetails.MaterialContactId,
                                            OriginalFile = pDetails.OriginalFile,
                                            ProofFile = pDetails.ProofFile,
                                            FinalFile = pDetails.FinalFile,
                                            WebAdUrl = pDetails.WebAdUrl,
                                            TearSheets = pDetails.TearSheets,
                                            HeadLine = pDetails.HeadLine,
                                            PositionId = pDetails.PositionId,
                                            SeparationId = pDetails.SeparationId,
                                            PageNumber = pDetails.PageNumber,
                                            ProductionComment = pDetails.ProductionComment,
                                            CreatedOn = DateTime.Now,

                                            //*
                                            UpdatedOn = DateTime.Now,
                                            CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                            UpdatedByUserKey = Constants.Created_Update_ByUserKey
                                        };
                                        lstMediaOrderProductionDetails.Add(mopdetails);
                                    }
                                    order.MediaOrderProductionDetails = lstMediaOrderProductionDetails;

                                    lstMediaOrder.Add(order);
                                    flag = true;
                                }

                            }
                            else
                            {
                                response.StatusCode = ApiStatus.InvalidInput;
                                response.Message = ApiStatus.InvalidInputMessge;
                                return response;
                            }
                        }
                        else
                        {
                            response.StatusCode = ApiStatus.InvalidInput;
                            response.Message = ApiStatus.InvalidInputMessge;
                            return response;
                        }

                        if (flag)
                        {
                            if (lstMediaOrder.Count > 0)
                            {
                                DB.MediaOrders.AddRange(lstMediaOrder);
                                DB.SaveChanges();

                            }
                            var InventoryDetail = (from m in DB.MediaOrders
                                                   where /*m.DeletedInd != true &&*/ m.BuyId == newBuyId
                                                   orderby m.MediaOrderId descending
                                                   select new
                                                   {
                                                       MediaOrderId = m.MediaOrderId,
                                                       MediaAssetId = m.MediaAssetId,
                                                       AdSizeId = m.RateCardDetail.AdSizeId,
                                                       IssueDateId = m.IssueDateId
                                                   }).ToList();

                            foreach (var inventory in InventoryDetail)
                            {
                                var inventoryId = (from t in DB.MediaInventoryMasters
                                                   where /*t.DeletedInd != true  && */
                                                   t.MediaAssetId == inventory.MediaAssetId
                                                   && t.AdSizeId == inventory.AdSizeId
                                                   && t.IssueDateId == inventory.IssueDateId
                                                   select t.InventoryId).FirstOrDefault();
                                if (inventoryId > 0)
                                {
                                    int isRecordExist = DB.MediaInventoryDetails.Where(x => x.InventoryId == inventoryId
                                                                                       && x.MediaOrderId == inventory.MediaOrderId).Count();
                                    if (isRecordExist <= 0)
                                    {
                                        MediaInventoryDetail lstInventory = new MediaInventoryDetail
                                        {
                                            InventoryId = inventoryId,
                                            MediaOrderId = inventory.MediaOrderId,
                                            BlockedInventory = 1
                                        };
                                        DB.MediaInventoryDetails.Add(lstInventory);
                                        DB.SaveChanges();
                                    }
                                }
                                else
                                {
                                    MediaInventoryMaster objInventoryMaster = new MediaInventoryMaster
                                    {
                                        MediaAssetId = inventory.MediaAssetId,
                                        AdSizeId = inventory.AdSizeId,
                                        IssueDateId = inventory.IssueDateId,
                                        CreatedOn = DateTime.Now,
                                        //DeletedInd = false,

                                        //*
                                        UpdatedOn = DateTime.Now,
                                        CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                        UpdatedByUserKey = Constants.Created_Update_ByUserKey
                                    };
                                    DB.MediaInventoryMasters.Add(objInventoryMaster);
                                    DB.SaveChanges();
                                    var invId = (from t in DB.MediaInventoryMasters
                                                 where /*t.DeletedInd != true &&*/ t.MediaAssetId == inventory.MediaAssetId && t.AdSizeId == inventory.AdSizeId && t.IssueDateId == inventory.IssueDateId
                                                 select t.InventoryId).FirstOrDefault();
                                    int isRecordExist = DB.MediaInventoryDetails.Where(x => x.MediaOrderId == inventory.MediaOrderId).Count();
                                    if (isRecordExist <= 0)
                                    {
                                        MediaInventoryDetail lstInventory = new MediaInventoryDetail
                                        {
                                            InventoryId = invId,
                                            MediaOrderId = inventory.MediaOrderId,
                                            BlockedInventory = 1
                                        };
                                        DB.MediaInventoryDetails.Add(lstInventory);
                                        DB.SaveChanges();
                                    }
                                }
                            }
                            var result = lstMediaOrder.Select(x => new
                            {
                                BuyId = x.BuyId,
                                MediaOrderId = x.MediaOrderId,
                                MediaOrderRepIds = x.MediaOrderReps.Select(t => t.MediaOrderRepId).ToList(),
                                MediaOrderLineIds = x.MediaOrderLines.Select(t => t.MediaOrderLineId).ToList(),
                                MediaOrderProductionDetailIds = x.MediaOrderProductionDetails.Select(t => t.MediaOrderProductionDetailId).ToList(),
                                GrossCost = x.GrossCost,
                                NetCost = x.NetCost
                            }).ToList();

                            response.StatusCode = ApiStatus.Ok;
                            response.Data = result;
                            lstMediaOrder = new List<MediaOrder>();
                        }
                    }
                }
                else
                {
                    response.StatusCode = ApiStatus.InvalidInput;
                    response.Message = "Model is null found";
                    return response;
                }

                if (errorMessage != "" && successMessage != "")
                {
                    response.Message = successMessage + "||" + errorMessage;
                    response.StatusCode = ApiStatus.Ok;
                }
                else if (errorMessage != "")
                {
                    response.Message = errorMessage;
                    response.StatusCode = ApiStatus.OutOfInventory;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }

            return response;
        }
        public ResponseCommonForDatatable GetOrdersbyProductionStatus()
        {
            // ResponseCommon response = new ResponseCommon();
            GetOrdersForMissingMaterialsbyFilterOption model = new GetOrdersForMissingMaterialsbyFilterOption();
            ResponseCommonForDatatable response = new ResponseCommonForDatatable();
            List<MediaOrdersMediaOrderIdWise> lstMediaOrderModel = new List<MediaOrdersMediaOrderIdWise>();
            int recordsTotal = 0;
            try
            {
                var context = HttpContext.Current;
                var currentRequest = HttpContext.Current.Request;
                int mediaAssetId = (currentRequest["MediaAssetId"] == "" ? 0 : Convert.ToInt32(currentRequest["MediaAssetId"]));
                int issueDateId = (currentRequest["IssueDateId"] == "" ? 0 : Convert.ToInt32(currentRequest["IssueDateId"]));
                int productionStatusId = (currentRequest["productionstatusId"] == "" ? 0 : Convert.ToInt32(currentRequest["productionstatusId"]));

                model.MediaAssetId = mediaAssetId;
                model.IssueDateId = issueDateId;
                model.productionstatusId = productionStatusId;

                string draw = currentRequest["draw"];
                string start = currentRequest["start"];
                string length = currentRequest["length"];
                string pageSize = (length != null ? Convert.ToInt32(length) : 0).ToString();
                string skip = (start != null ? Convert.ToInt32(start) : 0).ToString();

                if (model != null)
                {

                    List<asi_GetSPOrdersForPostOrder_Result> lstSpResultMediaAdvertiserTest = DB.GetSPOrdersForPostOrder(mediaAssetId, issueDateId, 0, 0, productionStatusId, "", skip, pageSize).ToList();
                    foreach (var item in lstSpResultMediaAdvertiserTest)
                    {
                        lstMediaOrderModel.Add(new MediaOrdersMediaOrderIdWise
                        {
                            Totalcount = item.TotalCount,
                            BuyId = (Int32)item.BuyId,
                            MediaOrderId = (Int32)item.MediaOrderId,
                            MediaAssetId = (Int32)item.MediaAssetId,
                            IssueDateId = (Int32)item.IssueDateId,
                            AdTypeId = item.AdTypeId,
                            AdTypeName = item.AdTypeName,
                            AdcolorId = item.AdColorId,
                            AdcolorName = item.AdColorName,
                            AdSizeId = item.AdSizeId,
                            AdSizeName = item.AdSizeName,
                            FrequencyId = item.FrequencyId,
                            FrequencyName = item.FrequencyName,
                            AdvertiserId = item.AdvertiserId,
                            BT_ID = item.BtId,
                            ST_ID = item.StId,
                            ProductCode = item.ProductCode,
                            RateCardCost = (decimal)item.RateCardCost,
                            GrossCost = (decimal)item.GrossCost,
                            NetCost = (decimal)item.NetCost,
                            FlightStartDate = item.FlightStartDate,
                            FlightEndDate = item.FlightEndDate
                        });
                    }
                    recordsTotal = Convert.ToInt32(lstMediaOrderModel[0].Totalcount);
                    if (lstMediaOrderModel.Count > 0)
                    {
                        response.Draw = Convert.ToInt32(draw);
                        response.RecordsTotal = recordsTotal;
                        response.RecordsFiltered = recordsTotal;
                        response.Data = lstMediaOrderModel;
                    }
                    else
                    {
                        response.Draw = Convert.ToInt32(draw);
                        response.RecordsTotal = recordsTotal;
                        response.RecordsFiltered = recordsTotal;
                        response.Data = lstMediaOrderModel;
                    }

                }
                else
                {

                }
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);

                    }
                }
            }
            catch (Exception ex)
            {
                var context = HttpContext.Current;
                var currentRequest = HttpContext.Current.Request;
                string draw = currentRequest["draw"];
                recordsTotal = lstMediaOrderModel.Count();
                response.Draw = Convert.ToInt32(draw);
                response.RecordsTotal = recordsTotal;
                response.RecordsFiltered = recordsTotal;
                response.Data = lstMediaOrderModel;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return response;
        }

        public ResponseCommon ImportExcelSheet()
        {
            ResponseCommon response = new ResponseCommon();
            DataTable dtError = new DataTable();
            DataTable tmpDt = new DataTable();
            DataTable dt = new DataTable();
            ImportOrderResponseModel importOrderResponse = new ImportOrderResponseModel();
            int rowIndex = -1;
            string excelSheetPath = string.Empty;
            int totalOrder = 0;
            int successOrder = 0;
            int failOrder = 0;
            List<PartyModel> lstAsiPart = new List<PartyModel>();
            try
            {
                string filePath = string.Empty;
                string extension = string.Empty;
                string excelCon = string.Empty;
                string asiPartApiUrl = string.Empty;
                string asiPartyAuthToken = string.Empty;

                string basePath = AppDomain.CurrentDomain.BaseDirectory;
                string applicationPath = ConfigurationManager.AppSettings["MediaOrderExcelFilesUploadPath"];

                var context = HttpContext.Current;
                var currentRequest = HttpContext.Current.Request;
                //int test = Convert.ToInt32("");

                #region Access Post Data and Excel File

                asiPartApiUrl = currentRequest["AsiPartyAPI"];
                asiPartyAuthToken = currentRequest["AsiToken"];

                for (int i = 0; i < currentRequest.Files.Count; i++)
                {
                    if (currentRequest.Files[i].FileName != "")
                    {
                        string fullPath = basePath + applicationPath;
                        extension = Path.GetExtension(currentRequest.Files[i].FileName);
                        if (!Directory.Exists(fullPath))
                            Directory.CreateDirectory(fullPath);
                        string fileName = System.DateTime.Now.Ticks.ToString() + currentRequest.Files[i].FileName + extension;
                        var path = Path.Combine(fullPath, fileName);
                        currentRequest.Files[i].SaveAs(path);
                        filePath = path;
                    }
                }

                #endregion

                #region Read Excel File
                if (!string.IsNullOrEmpty(filePath) && !string.IsNullOrEmpty(extension))
                {
                    switch (extension)
                    {
                        case ".xls": //Excel 97-03
                            excelCon = ConfigurationManager.AppSettings["Excel03ConString"];
                            break;
                        case ".xlsx": //Excel 07
                            excelCon = ConfigurationManager.AppSettings["Excel07ConString"];
                            break;
                    }
                    excelCon = String.Format(excelCon, filePath);
                    OleDbConnection connExcel = new OleDbConnection(excelCon);
                    OleDbCommand cmdExcel = new OleDbCommand();
                    OleDbDataAdapter oAdapter = new OleDbDataAdapter();
                    cmdExcel.Connection = connExcel;

                    //Get the name of First Sheet
                    connExcel.Open();
                    DataTable dtExcelSchema;
                    dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    string SheetName = "";
                    SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                    connExcel.Close();
                    //Read Data from First Sheet
                    connExcel.Open();
                    cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
                    oAdapter.SelectCommand = cmdExcel;
                    oAdapter.Fill(dt);
                }
                #endregion

                #region Get Party Data from ASI

                lstAsiPart = GetAllASIParty(ref dtError, asiPartApiUrl, asiPartyAuthToken, "");
                #endregion

                tmpDt = dt.Copy();
                tmpDt.Columns.Add("Status");
                tmpDt.Columns.Add("Message");

                dt.Columns.Add("AdvertiserId");
                dt.Columns.Add("AgencyId");
                dt.Columns.Add("BillToContactId");
                dt.Columns.Add("MediaOrderId");
                dt.Columns.Add("BuyId");


                try
                {
                    if (dt.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            try
                            {
                                rowIndex = i;
                                DataRow row = (DataRow)dt.Rows[i];

                                #region Read All Field of Excel File
                                DataRow excelData = row;
                                string advertiserId = string.Empty;
                                string agencyId = string.Empty;
                                string billToContactId = string.Empty;
                                string MaterialContactId = string.Empty;

                                string buyId = excelData["ContractNum"].ToString();
                                string actualBuyId = excelData["BuyId"].ToString();

                                DataRow[] dr = dt.Select("ContractNum = " + buyId.ToString());
                                if (dr.Count() > 1)
                                {
                                    for (int j = 0; j <= dr.Count() - 1; j++)
                                    {
                                        string tmpid = dr[j]["BuyId"].ToString();
                                        if (!string.IsNullOrEmpty(tmpid))
                                        {
                                            row["BuyId"] = tmpid;
                                        }
                                    }
                                }

                                string orderNumber = excelData["OrderNum"].ToString();
                                //consider latter
                                string advertiserName = excelData["Account Name"].ToString();
                                string agencyName = excelData["Agency Name"].ToString();
                                string billToContactName = excelData["BillName"].ToString();


                                //Email
                                string email = string.Empty, mobile = string.Empty, workP = string.Empty;
                                string street1 = string.Empty, street2 = string.Empty, city = string.Empty, state = string.Empty, zip = string.Empty, country = string.Empty;

                                if (excelData["Email"] != null)
                                {
                                    email = excelData["Email"].ToString();
                                }
                                //Mobile

                                if (excelData["Mobile"] != null)
                                {
                                    mobile = excelData["Mobile"].ToString();
                                }
                                //Work

                                if (excelData["Work"] != null)
                                {
                                    workP = excelData["Work"].ToString();
                                }


                                if (excelData["Street1"] != null)
                                {
                                    street1 = excelData["Street1"].ToString();
                                }

                                if (excelData["Street2"] != null)
                                {
                                    street2 = excelData["Street2"].ToString();
                                }
                                if (excelData["City"] != null)
                                {
                                    city = excelData["City"].ToString();
                                }

                                if (excelData["State"] != null)
                                {
                                    state = excelData["State"].ToString();
                                }

                                if (excelData["Zip"] != null)
                                {
                                    zip = excelData["Zip"].ToString();
                                }

                                if (excelData["Country"] != null)
                                {
                                    country = excelData["Country"].ToString();
                                }
                                ////field to consider 

                                string billTo = excelData["Bill"].ToString();
                                string matPub = excelData["MatPub"].ToString();
                                string publication = excelData["Publication"].ToString();
                                string materialsContactID = excelData["MaterialsContactID"].ToString();
                                string excelUser = excelData["_User"].ToString();
                                string createdDateExcel = excelData["_Date"].ToString();
                                string createdTime = excelData["_Time"].ToString();



                                #endregion

                                #region Validation for Advertiser, Agency and BillToContact

                                if (!string.IsNullOrEmpty(advertiserName))
                                {

                                    var isAdvertiserExists = lstAsiPart.Where(x => (x.OrganizationPartyName ?? "").Trim().ToLower() == advertiserName.Trim().ToLower()).ToList();
                                    if (isAdvertiserExists.Count > 0)
                                    {
                                        advertiserId = isAdvertiserExists[0].OrganizationPartyId;
                                        row["AdvertiserId"] = advertiserId;
                                    }
                                    else
                                    {
                                        string orgId = SaveAdvertiserAsParty(ref dtError, asiPartApiUrl, asiPartyAuthToken, advertiserName, street1, street2, city, state, zip, country, workP);
                                        if (!string.IsNullOrEmpty(orgId) && Convert.ToInt32(orgId) >= 0 && orgId != "0")
                                        {
                                            advertiserId = orgId;
                                            row["AdvertiserId"] = advertiserId;
                                            PartyModel obj = new PartyModel
                                            {
                                                PartyId = null,
                                                PartyName = null,
                                                OrganizationPartyId = advertiserId,
                                                OrganizationPartyName = advertiserName,
                                                Email = email
                                            };
                                            lstAsiPart.Add(obj);
                                        }
                                        else
                                        {
                                            string cError = "Advertiser Name is not saved in ASI System";
                                            CreateErrorOfDatatableForImportOrder(ref dtError, "Account Name", "", cError);
                                        }
                                    }
                                }
                                else
                                {
                                    string cError = "Advertiser Name is not present";
                                    CreateErrorOfDatatableForImportOrder(ref dtError, "Account Name", "", cError);
                                }

                                if (!string.IsNullOrEmpty(agencyName))
                                {
                                    var isAgencyExists = lstAsiPart.Where(x => (x.OrganizationPartyName ?? "").Trim().ToLower() == agencyName.Trim().ToLower()).ToList();
                                    if (isAgencyExists.Count > 0)
                                    {
                                        agencyId = isAgencyExists[0].OrganizationPartyId;
                                        row["AgencyId"] = agencyId;
                                    }
                                    else
                                    {
                                        string orgId = SaveAdvertiserAsParty(ref dtError, asiPartApiUrl, asiPartyAuthToken, agencyName, street1, street2, city, state, zip, country, workP);
                                        if (string.IsNullOrEmpty(orgId) && Convert.ToInt32(orgId) >= 0 && orgId != "0")
                                        {
                                            agencyId = orgId;
                                            row["AgencyId"] = agencyId;
                                            PartyModel obj = new PartyModel
                                            {
                                                PartyId = null,
                                                PartyName = null,
                                                OrganizationPartyId = agencyId,
                                                OrganizationPartyName = agencyName,
                                                Email = email
                                            };
                                            lstAsiPart.Add(obj);
                                        }
                                        else
                                        {
                                            string cError = "Agency Name is not saved in ASI System";
                                            CreateErrorOfDatatableForImportOrder(ref dtError, "Agency Name", "", cError);
                                        }
                                    }
                                }
                                else
                                {
                                    agencyId = "0";
                                    row["AgencyId"] = agencyId;
                                    string cError = "Agency Name is not present";
                                    CreateErrorOfDatatableForImportOrder(ref dtError, "Agency Name", "", cError);
                                }

                                if (!string.IsNullOrEmpty(billToContactName))
                                {
                                    List<PartyModel> isBillToContactExists = lstAsiPart.Where(x => (x.PartyName ?? "").Trim().ToLower() == billToContactName.Trim().ToLower()).ToList();
                                    if (isBillToContactExists.Count > 0)
                                    {
                                        if (billTo == "0")
                                        {
                                            var billData = (from s in isBillToContactExists
                                                            where (s.OrganizationPartyName ?? "").Trim().ToLower() == agencyName.Trim().ToLower()
                                                            select new PartyModel
                                                            {
                                                                OrganizationPartyId = s.OrganizationPartyId,
                                                                OrganizationPartyName = s.OrganizationPartyName,
                                                                PartyId = s.PartyId,
                                                                PartyName = s.PartyName,
                                                                Mobile = s.Mobile
                                                            }).ToList();

                                            if (billData.Count() > 0)
                                            {
                                                billToContactId = billData[0].PartyId;
                                                row["BillToContactId"] = billToContactId;
                                            }
                                            else
                                            {
                                                string conId = SaveBillToContactAsParty(ref dtError, asiPartApiUrl, asiPartyAuthToken, billToContactName, email, mobile, workP, street1, street2, city, state, zip, country, agencyId);
                                                if (!string.IsNullOrEmpty(conId) && Convert.ToInt32(conId) >= 0 && conId != "0")
                                                {
                                                    billToContactId = conId;
                                                    row["BillToContactId"] = billToContactId;
                                                    PartyModel obj = new PartyModel
                                                    {
                                                        PartyId = billToContactId,
                                                        PartyName = billToContactName,
                                                        OrganizationPartyId = agencyId,
                                                        OrganizationPartyName = agencyName,
                                                        Email = email,
                                                        Mobile = mobile
                                                    };
                                                    lstAsiPart.Add(obj);
                                                }
                                                else
                                                {
                                                    string cError = "BillToContact Name is not saved in ASI System";
                                                    CreateErrorOfDatatableForImportOrder(ref dtError, "Agency Name", "", cError);
                                                }
                                            }
                                        }
                                        else if (billTo == "-1")
                                        {

                                            var billData = (from s in isBillToContactExists
                                                            where (s.OrganizationPartyName ?? "").Trim().ToLower() == advertiserName.Trim().ToLower()
                                                            select new PartyModel
                                                            {
                                                                OrganizationPartyId = s.OrganizationPartyId,
                                                                OrganizationPartyName = s.OrganizationPartyName,
                                                                PartyId = s.PartyId,
                                                                PartyName = s.PartyName,
                                                            }).ToList();
                                            if (billData.Count() > 0)
                                            {
                                                billToContactId = billData[0].PartyId;
                                                row["BillToContactId"] = billToContactId;
                                            }
                                            else
                                            {
                                                string conId = SaveBillToContactAsParty(ref dtError, asiPartApiUrl, asiPartyAuthToken, billToContactName, email, mobile, workP, street1, street2, city, state, zip, country, advertiserId);

                                                if (!string.IsNullOrEmpty(conId) && Convert.ToInt32(conId) >= 0 && conId != "0")
                                                {
                                                    billToContactId = conId;
                                                    row["BillToContactId"] = billToContactId;
                                                    PartyModel obj = new PartyModel
                                                    {
                                                        PartyId = billToContactId,
                                                        PartyName = billToContactName,
                                                        OrganizationPartyId = advertiserId,
                                                        OrganizationPartyName = advertiserName,
                                                        Mobile = mobile,
                                                        Email = email
                                                    };
                                                    lstAsiPart.Add(obj);
                                                }
                                                else
                                                {
                                                    string cError = "BillToContact Name is not saved in ASI System";
                                                    CreateErrorOfDatatableForImportOrder(ref dtError, "Agency Name", "", cError);
                                                }
                                            }

                                        }
                                    }
                                    else
                                    {
                                        if (billTo == "0")
                                        {
                                            string conId = SaveBillToContactAsParty(ref dtError, asiPartApiUrl, asiPartyAuthToken, billToContactName, email, mobile, workP, street1, street2, city, state, zip, country, agencyId);
                                            if (!string.IsNullOrEmpty(conId) && Convert.ToInt32(conId) >= 0 && conId != "0")
                                            {
                                                billToContactId = conId;
                                                row["BillToContactId"] = billToContactId;
                                                PartyModel obj = new PartyModel
                                                {
                                                    PartyId = billToContactId,
                                                    PartyName = billToContactName,
                                                    OrganizationPartyId = agencyId,
                                                    OrganizationPartyName = agencyName,
                                                    Email = email,
                                                    Mobile = mobile
                                                };
                                                lstAsiPart.Add(obj);
                                            }
                                            else
                                            {
                                                string cError = "BillToContact Name is not saved in ASI System";
                                                CreateErrorOfDatatableForImportOrder(ref dtError, "Agency Name", "", cError);
                                            }
                                        }
                                        else if (billTo == "-1")
                                        {
                                            string conId = SaveBillToContactAsParty(ref dtError, asiPartApiUrl, asiPartyAuthToken, billToContactName, email, mobile, workP, street1, street2, city, state, zip, country, advertiserId);
                                            if (!string.IsNullOrEmpty(conId) && Convert.ToInt32(conId) >= 0 && conId != "0")
                                            {
                                                billToContactId = conId;
                                                row["BillToContactId"] = billToContactId;
                                                PartyModel obj = new PartyModel
                                                {
                                                    PartyId = billToContactId,
                                                    PartyName = billToContactName,
                                                    OrganizationPartyId = advertiserId,
                                                    OrganizationPartyName = advertiserName,
                                                    Email = email,
                                                    Mobile = mobile
                                                };
                                                lstAsiPart.Add(obj);
                                            }
                                            else
                                            {
                                                string cError = "BillToContact Name is not saved in ASI System";
                                                CreateErrorOfDatatableForImportOrder(ref dtError, "Agency Name", "", cError);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    string cError = "BillToContact  is not present";
                                    CreateErrorOfDatatableForImportOrder(ref dtError, "BillName", "", cError);
                                }

                                if (!string.IsNullOrEmpty(materialsContactID))
                                {
                                    int.TryParse(materialsContactID, out int parsedResult);
                                    if (parsedResult > 0)
                                    {
                                        List<PartyModel> isBillToContactExists = lstAsiPart.Where(x => x.PartyId.Trim().ToLower().ToString() == parsedResult.ToString().ToLower()).ToList();
                                        if (isBillToContactExists.Count > 0)
                                        {
                                            MaterialContactId = isBillToContactExists[0].PartyId;
                                            row["MaterialContactId"] = MaterialContactId;
                                        }
                                        else
                                        {
                                            row["MaterialContactId"] = parsedResult;
                                        }
                                    }
                                    else
                                    {
                                        List<PartyModel> isBillToContactExists = lstAsiPart.Where(x => (x.PartyName ?? "").Trim().ToLower().ToLower() == materialsContactID.Trim().ToLower().ToLower()).ToList();
                                        if (isBillToContactExists.Count > 0)
                                        {
                                            MaterialContactId = isBillToContactExists[0].PartyId;
                                            row["BillToContactId"] = MaterialContactId;
                                        }
                                        else
                                        {

                                        }
                                    }
                                }
                                else
                                {
                                    string cError = "Material Contact Id  is not present";
                                    //  createErrorOfDatatableForImportOrder(ref dtError, "MaterialsContactID", "", cError);
                                }
                                #endregion

                                #region Validation for Media Asset and it's Relative Table

                                // Do after confirmation
                                //if (MatPub.ToString() == Publication.ToString())
                                if (!string.IsNullOrEmpty(publication.ToString()) || !string.IsNullOrEmpty(matPub.ToString()))
                                {
                                    MediaAsset mResponseData = new MediaAsset();
                                    string mediaAssetName = "";
                                    if (!string.IsNullOrEmpty(publication.Trim().ToString()))
                                    {
                                        mediaAssetName = publication;
                                        mResponseData = GetAllDatabyMediaAsset(ref dtError, publication);
                                    }
                                    else if (!string.IsNullOrEmpty(matPub.ToString()))
                                    {
                                        mediaAssetName = matPub;
                                        mResponseData = GetAllDatabyMediaAsset(ref dtError, matPub);
                                    }
                                    //Temp Code Start
                                    //Check for product code in db
                                    if (mResponseData == null || mResponseData.MediaAssetId == 0)
                                    {
                                        MediaAssetGroup mediaAssetGroup = null;
                                        mediaAssetGroup = DB.MediaAssetGroups.Where(x => x.MediaAssetGroupName == "Print").FirstOrDefault();

                                        if (mediaAssetGroup == null)
                                        {
                                            mediaAssetGroup = new MediaAssetGroup();
                                            mediaAssetGroup.MediaAssetGroupName = "Print";
                                            mediaAssetGroup.CreatedOn = DateTime.Now;
                                            //mediaAssetGroup.DeletedInd = false;

                                            //*
                                            mediaAssetGroup.UpdatedOn = DateTime.Now;
                                            mediaAssetGroup.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                                            mediaAssetGroup.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                            DB.MediaAssetGroups.Add(mediaAssetGroup);
                                            DB.SaveChanges();
                                        }
                                        MediaTypeRef mediaType = null;
                                        mediaType = DB.MediaTypes.Where(x => x.MediaTypeName == "Print").FirstOrDefault();

                                        if (mediaType == null)
                                        {
                                            mediaType = new MediaTypeRef
                                            {
                                                MediaTypeName = "Print"
                                            };
                                            DB.MediaTypes.Add(mediaType);
                                            DB.SaveChanges();
                                        }
                                        mResponseData = new MediaAsset
                                        {
                                            MediaAssetName = mediaAssetName
                                        };
                                        if (mediaAssetName.Length > 25)
                                        {
                                            mResponseData.MediaCode = mediaAssetName.Substring(0, 24);
                                        }
                                        else
                                        {
                                            mResponseData.MediaCode = mediaAssetName;
                                        }

                                        //Check in db if exists
                                        mResponseData.ProductCode = "ADVSALE";
                                        mResponseData.MediaAssetGroupId = mediaAssetGroup.MediaAssetGroupId;
                                        mResponseData.MediaTypeId = mediaType.MediaTypeId;
                                        mResponseData.CreatedOn = DateTime.Now;
                                        //mResponseData.DeletedInd = false;
                                        //*
                                        mResponseData.UpdatedOn = DateTime.Now;
                                        mResponseData.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                                        mResponseData.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                        DB.MediaAssets.Add(mResponseData);
                                        DB.SaveChanges();
                                    }

                                    if (mResponseData.MediaAssetId > 0)
                                    {
                                        ResponseCommon objMediaOrder = ValidationForOrderDetailsAndMediaScheduleTab(ref dtError, mResponseData, row, lstAsiPart);
                                        if (objMediaOrder.StatusCode == ApiStatus.Ok)
                                        {
                                            int mediaOrderId = Convert.ToInt32(objMediaOrder.Data);
                                            if (mediaOrderId > 0)
                                            {
                                                tmpDt.Rows[rowIndex]["Status"] = "Success";
                                                tmpDt.Rows[rowIndex]["Message"] = "Success";
                                                //int mediaOrderId = 12;
                                                bool orderAdjustment = ValidationForMediaOrderLines(ref dtError, mResponseData, row, lstAsiPart, mediaOrderId.ToString());
                                                if (orderAdjustment == false)
                                                {
                                                    tmpDt.Rows[rowIndex]["Status"] = "Fail";
                                                    tmpDt.Rows[rowIndex]["Message"] = "Something went wrong while storing data for Media Order Lines";
                                                    string excelPath = ImportOrderStatusExcelSheet(ref dtError, tmpDt, "", "", "", excelSheetPath);
                                                    excelSheetPath = excelPath;
                                                    importOrderResponse.ExcelSheetPath = excelPath;
                                                }

                                                ResponseCommon orderProduction = ValidationForMediaOrderProduction(ref dtError, mResponseData, row, lstAsiPart, mediaOrderId.ToString());
                                                if (orderProduction.StatusCode == ApiStatus.Exception)
                                                {
                                                    if (orderProduction.Message.ToString() == "false")
                                                    {
                                                        tmpDt.Rows[rowIndex]["Status"] = "Fail";
                                                        tmpDt.Rows[rowIndex]["Message"] = "Something went wrong while storing data into Production tables";
                                                        string excelPath = ImportOrderStatusExcelSheet(ref dtError, tmpDt, "", "", "", excelSheetPath);
                                                        excelSheetPath = excelPath;
                                                        importOrderResponse.ExcelSheetPath = excelPath;
                                                    }
                                                }
                                                else if (orderProduction.StatusCode == ApiStatus.InvalidInput)
                                                {
                                                    if (orderProduction.Message.ToString() == "notSaved")
                                                    {

                                                    }
                                                }
                                            }
                                        }
                                        else if (objMediaOrder.StatusCode == ApiStatus.Exception)
                                        {
                                            int mediaOrderId = Convert.ToInt32(objMediaOrder.Data);
                                            if (mediaOrderId > 0)
                                            {
                                                tmpDt.Rows[rowIndex]["Status"] = "Fail";
                                                tmpDt.Rows[rowIndex]["Message"] = "Production Status Updation is failed";
                                                string excelPath = ImportOrderStatusExcelSheet(ref dtError, tmpDt, "", "", "", excelSheetPath);
                                                excelSheetPath = excelPath;
                                                importOrderResponse.ExcelSheetPath = excelPath;

                                                bool orderAdjustment = ValidationForMediaOrderLines(ref dtError, mResponseData, row, lstAsiPart, mediaOrderId.ToString());
                                                if (orderAdjustment == false)
                                                {
                                                    tmpDt.Rows[rowIndex]["Status"] = "Fail";
                                                    tmpDt.Rows[rowIndex]["Message"] = "Something went wrong while storing data for Media Order Lines";
                                                    string excelPath1 = ImportOrderStatusExcelSheet(ref dtError, tmpDt, "", "", "", excelSheetPath);
                                                    excelSheetPath = excelPath1;
                                                    importOrderResponse.ExcelSheetPath = excelPath1;
                                                }

                                                ResponseCommon orderProduction = ValidationForMediaOrderProduction(ref dtError, mResponseData, row, lstAsiPart, mediaOrderId.ToString());
                                                if (orderProduction.StatusCode == ApiStatus.Exception)
                                                {
                                                    if (orderProduction.Message.ToString() == "false")
                                                    {
                                                        tmpDt.Rows[rowIndex]["Status"] = "Fail";
                                                        tmpDt.Rows[rowIndex]["Message"] = "Something went wrong while storing data in Production tables";
                                                        string excelPath2 = ImportOrderStatusExcelSheet(ref dtError, tmpDt, "", "", "", excelSheetPath);
                                                        excelSheetPath = excelPath2;
                                                        importOrderResponse.ExcelSheetPath = excelPath2;
                                                    }
                                                }
                                                else if (orderProduction.StatusCode == ApiStatus.InvalidInput)
                                                {
                                                    if (orderProduction.Message.ToString() == "notSaved")
                                                    {

                                                    }
                                                }
                                            }
                                            else
                                            {
                                                tmpDt.Rows[rowIndex]["Status"] = "Fail";
                                                tmpDt.Rows[rowIndex]["Message"] = "Something went wrong";
                                                string excelPath = ImportOrderStatusExcelSheet(ref dtError, tmpDt, "", "", "", excelSheetPath);
                                                excelSheetPath = excelPath;
                                                importOrderResponse.ExcelSheetPath = excelPath;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        string mediaName = string.Empty;
                                        if (!string.IsNullOrEmpty(matPub))
                                        {
                                            mediaName = matPub;
                                        }
                                        else if (!string.IsNullOrEmpty(publication))
                                        {
                                            mediaName = publication;
                                        }

                                        string errorMessage = "Media Asset does not exists in system";
                                        tmpDt.Rows[rowIndex]["Status"] = "Fail";
                                        tmpDt.Rows[rowIndex]["Message"] = "Media Asset does not exists in system";
                                        string excelPath = ImportOrderStatusExcelSheet(ref dtError, tmpDt, "", errorMessage.ToString(), "", excelSheetPath);
                                        excelSheetPath = excelPath;
                                        importOrderResponse.ExcelSheetPath = excelPath;
                                    }
                                }
                                else
                                {
                                    string mediaName = string.Empty;
                                    if (!string.IsNullOrEmpty(matPub))
                                    {
                                        mediaName = matPub;
                                    }
                                    else if (!string.IsNullOrEmpty(publication))
                                    {
                                        mediaName = publication;
                                    }

                                    string errorMessage = "Media Asset does not exists in system";
                                    tmpDt.Rows[rowIndex]["Status"] = "Fail";
                                    tmpDt.Rows[rowIndex]["Message"] = "Media Asset does not exists in system";
                                    string excelPath = ImportOrderStatusExcelSheet(ref dtError, tmpDt, "", errorMessage.ToString(), "", excelSheetPath);
                                    excelSheetPath = excelPath;
                                    importOrderResponse.ExcelSheetPath = excelPath;
                                }

                                #endregion
                            }
                            catch (Exception ex)
                            {
                                tmpDt.Rows[rowIndex]["Status"] = "Fail";
                                tmpDt.Rows[rowIndex]["Message"] = ex.Message.ToString();
                                string excelPath = ImportOrderStatusExcelSheet(ref dtError, tmpDt, "", ex.Message.ToString(), "", excelSheetPath);
                                excelSheetPath = excelPath;
                                importOrderResponse.ExcelSheetPath = excelPath;
                            }

                        }
                    }
                }

                catch (Exception ex)
                {
                    tmpDt.Rows[rowIndex]["Status"] = "Fail";
                    tmpDt.Rows[rowIndex]["Message"] = ex.Message.ToString();
                    string excelPath = ImportOrderStatusExcelSheet(ref dtError, tmpDt, "", ex.Message.ToString(), "", excelSheetPath);
                    excelSheetPath = excelPath;
                    importOrderResponse.ExcelSheetPath = excelPath;
                }


                if (tmpDt.Rows.Count > 0)
                {
                    string successMsg = "Success";
                    string failMsg = "Fail";

                    var successResults = from myRow in tmpDt.AsEnumerable()
                                         where myRow.Field<string>("Status") == successMsg.ToString()
                                         select myRow;
                    successOrder = successResults.Count();

                    var failResults = from myRow in tmpDt.AsEnumerable()
                                      where myRow.Field<string>("Status") == failMsg.ToString()
                                      select myRow;
                    failOrder = failResults.Count();
                    totalOrder = tmpDt.Rows.Count;

                    string excelPath = ImportOrderStatusExcelSheet(ref dtError, tmpDt, "", "", "", excelSheetPath);
                    excelSheetPath = excelPath;
                    importOrderResponse.ExcelSheetPath = excelPath;
                }

                importOrderResponse.TotalImportOrder = totalOrder;
                importOrderResponse.SuccessImportOrder = successOrder;
                importOrderResponse.FailImportOrder = failOrder;

                //Process.Start(excelSheetPath);
                if (!string.IsNullOrEmpty(excelSheetPath))
                {
                }
                else
                {
                }

                response.StatusCode = ApiStatus.Ok;
                response.Data = importOrderResponse;
                response.Message = ApiStatus.OkMessge;
                //CommonUtility.writeError("success");
            }
            catch (Exception ex)
            {

                string excelPath = ImportOrderStatusExcelSheet(ref dtError, tmpDt, "", ex.Message.ToString(), "", excelSheetPath);
                excelSheetPath = excelPath;
                importOrderResponse.ExcelSheetPath = excelPath;

                if (!string.IsNullOrEmpty(excelSheetPath))
                {
                    //Process.Start(excelSheetPath);

                }
                else
                {

                }

                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ex.Message.ToString();

            }

            return response;
        }

        public ResponseCommon UpdateIsFrozenTrue(List<int> mediaOrderIds)
        {
            ResponseCommon response = new ResponseCommon();
            List<MediaOrder> lstMediaOrder = new List<MediaOrder>();
            try
            {
                lstMediaOrder = DB.MediaOrders.Where(x => mediaOrderIds.Contains(x.MediaOrderId) /*&& x.DeletedInd != true*/).ToList();
                if (lstMediaOrder == null || lstMediaOrder.Count == 0)
                {
                    response.StatusCode = ApiStatus.InvalidInput;
                    response.Message = ApiStatus.InvalidInputMessge;
                }

                foreach (var item in lstMediaOrder)
                {
                    item.IsFrozen = true;
                    //*
                    item.UpdatedOn = DateTime.Now;
                    item.UpdatedByUserKey = Constants.Created_Update_ByUserKey;
                    DB.Entry(item).State = EntityState.Modified;
                }
                DB.SaveChanges();

                response.StatusCode = ApiStatus.Ok;
                response.Message = ApiStatus.OkMessge;
            }

            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }


        #endregion

        public string GetOperator(string comparisonOperator)
        {
            if (comparisonOperator == "GreaterThan")
                return ">";
            else if (comparisonOperator == "LessThan")
                return "<";
            else if (comparisonOperator == "GreaterOrEqual")
                return ">=";
            else if (comparisonOperator == "LessOrEqual")
                return "<=";
            else
                return "=";
        }
    }

}
