﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace Asi.Repository
{
    public class MediaOrderLinesRepository : IMediaOrderLinesRepository
    {
        #region Private Fields
        private readonly IDBEntities DB;
        #endregion

        #region Constructor
        public MediaOrderLinesRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }
        #endregion

        #region Private Methods
        private MediaOrderCostModel GetOrderNetAndGrossCost(int buyId)
        {
            MediaOrderCostModel model = new MediaOrderCostModel();
            try
            {
                var lstCosts = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/ x.BuyId == buyId)
                                             .Select(x => new { x.GrossCost, x.NetCost }).ToList();
                model.BuyId = buyId;
                model.GrossCost = lstCosts.Sum(x => x.GrossCost);
                model.NetCost = lstCosts.Sum(x => x.NetCost);
            }
            catch (Exception ex)
            {
                LoggerRepsitory.LogError(ex);
            }
            return model;
        }
        #endregion

        #region Public Methods
        public ResponseCommon AddMediaOrderLine(PostMediaOrderLineModel model)
        {
            ResponseCommon response = new ResponseCommon();
            List<MediaOrderLine> lstMediaOrderLine = new List<MediaOrderLine>();
            try
            {
                List<MediaOrder> lstMediaOrders = DB.MediaOrders.Where(x => model.MediaOrderIds.Contains(x.MediaOrderId) /*&& x.DeletedInd != true*/).ToList();
                if (lstMediaOrders.Count > 0)
                {
                    int[] mediaOrders = lstMediaOrders.Select(x => x.MediaOrderId).ToArray();
                    List<MediaOrderLine> lstBaseMediaOrderLines = lstMediaOrders.SelectMany(x => x.MediaOrderLines)./*Where(x => x.DeletedInd != true).*/OrderBy(x => x.SequenceNumber).ToList();
                    //List<MediaOrderLine> lstBaseMediaOrderLines = db.MediaOrderLines.Where(x => mediaOrders.Contains(x.MediaOrderId) && x.DeletedInd != true).ToList();
                    foreach (var order in lstMediaOrders)
                    {
                        double finalGrossCostOfOrder = (double)order.RateCardCost;
                        double finalNetCostOfOrder = (double)order.RateCardCost;
                        List<MediaOrderLine> currentMol = new List<MediaOrderLine>();
                        if (model.GrossInd)
                        {
                            currentMol = lstBaseMediaOrderLines.Where(x => x.GrossInd == true && x.MediaOrderId == order.MediaOrderId)
                                                               .OrderBy(x => x.SequenceNumber).ToList();

                            // calculate previous discount/surcharges
                            CommonUtility.CalculateNetCost(ref finalNetCostOfOrder, currentMol);

                            // add new discount/surcharges
                            if (model.AmountPercent == 0)
                            {
                                if (model.SurchargeDiscount == 0)
                                    finalNetCostOfOrder = finalNetCostOfOrder + model.AdjustmentValue;
                                else
                                    finalNetCostOfOrder = finalNetCostOfOrder - model.AdjustmentValue;
                            }
                            else
                            {
                                if (model.SurchargeDiscount == 0)
                                    finalNetCostOfOrder = finalNetCostOfOrder + ((finalNetCostOfOrder * model.AdjustmentValue) / 100);
                                else
                                    finalNetCostOfOrder = finalNetCostOfOrder - ((finalNetCostOfOrder * model.AdjustmentValue) / 100);
                            }

                            int sequenceNo = 1;
                            if (currentMol.Count > 0)
                                sequenceNo = currentMol.Max(x => x.SequenceNumber) + 1;

                            MediaOrderLine moline = new MediaOrderLine()
                            {
                                MediaOrderId = order.MediaOrderId,
                                SequenceNumber = sequenceNo,
                                AdAdjustmentName = model.AdAdjustmentName,
                                AmountPercent = model.AmountPercent,
                                SurchargeDiscount = model.SurchargeDiscount,
                                AdjustmentValue = model.AdjustmentValue,
                                AdjustmentAmount = model.AdjustmentAmount,
                                ProductCode = order.ProductCode,
                                GrossInd = model.GrossInd,
                                CreatedOn = DateTime.Now,

                                //*
                                UpdatedOn = DateTime.Now,
                                CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                UpdatedByUserKey = Constants.Created_Update_ByUserKey
                            };
                            lstMediaOrderLine.Add(moline);
                            // add new discount/surcharges end

                            // calculate gross cost
                            finalGrossCostOfOrder = finalNetCostOfOrder;

                            // calculate next discount/surcharges
                            currentMol = lstBaseMediaOrderLines.Where(x => x.GrossInd != true && x.MediaOrderId == order.MediaOrderId)
                                                                .OrderBy(x => x.SequenceNumber)
                                                                .ToList();
                            CommonUtility.CalculateNetCost(ref finalNetCostOfOrder, currentMol);
                        }
                        else
                        {

                            currentMol = lstBaseMediaOrderLines.Where(x => x.GrossInd == true
                                                                                      && x.MediaOrderId == order.MediaOrderId)
                                                                                   .OrderBy(x => x.SequenceNumber)
                                                                                   .ToList();
                            // calculate previous discount/surcharges
                            CommonUtility.CalculateNetCost(ref finalNetCostOfOrder, currentMol);

                            // calculate gross cost
                            finalGrossCostOfOrder = finalNetCostOfOrder;

                            currentMol = lstBaseMediaOrderLines.Where(x => x.GrossInd == false
                                                                                      && x.MediaOrderId == order.MediaOrderId)
                                                                                   .OrderBy(x => x.SequenceNumber)
                                                                                   .ToList();
                            // calculate next discount/surcharges
                            CommonUtility.CalculateNetCost(ref finalNetCostOfOrder, currentMol);

                            // add new discount/surcharges
                            if (model.AmountPercent == 0)
                            {
                                if (model.SurchargeDiscount == 0)
                                    finalNetCostOfOrder = finalNetCostOfOrder + model.AdjustmentValue;
                                else
                                    finalNetCostOfOrder = finalNetCostOfOrder - model.AdjustmentValue;
                            }
                            else
                            {
                                if (model.SurchargeDiscount == 0)
                                    finalNetCostOfOrder = finalNetCostOfOrder + ((finalNetCostOfOrder * model.AdjustmentValue) / 100);
                                else
                                    finalNetCostOfOrder = finalNetCostOfOrder - ((finalNetCostOfOrder * model.AdjustmentValue) / 100);
                            }

                            int sequenceNo = 1;
                            if (currentMol.Count > 0)
                                sequenceNo = currentMol.Max(x => x.SequenceNumber) + 1;

                            MediaOrderLine moline = new MediaOrderLine()
                            {
                                MediaOrderId = order.MediaOrderId,
                                SequenceNumber = sequenceNo,
                                AdAdjustmentName = model.AdAdjustmentName,
                                AmountPercent = model.AmountPercent,
                                SurchargeDiscount = model.SurchargeDiscount,
                                AdjustmentValue = model.AdjustmentValue,
                                AdjustmentAmount = model.AdjustmentAmount,
                                ProductCode = order.ProductCode,
                                GrossInd = model.GrossInd,
                                CreatedOn = DateTime.Now,

                                //*
                                UpdatedOn = DateTime.Now,
                                CreatedByUserKey = Constants.Created_Update_ByUserKey,
                                UpdatedByUserKey = Constants.Created_Update_ByUserKey
                            };
                            lstMediaOrderLine.Add(moline);
                        }
                        // add new discount/surcharges end

                        // Update ORDER NetCost
                        order.GrossCost = (decimal)finalGrossCostOfOrder;
                        order.NetCost = (decimal)finalNetCostOfOrder;
                        order.UpdatedOn = DateTime.Now;

                        //*
                        order.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                        DB.Entry(order).State = EntityState.Modified;
                        // Update ORDER NetCost end
                    }

                    if (lstMediaOrderLine.Count > 0)
                    {
                        DB.MediaOrderLines.AddRange(lstMediaOrderLine);
                    }
                    DB.SaveChanges();

                    List<MediaOrderLineModel> lstResult = new List<MediaOrderLineModel>();
                    lstResult = lstMediaOrderLine.Select(x => new MediaOrderLineModel
                    {
                        AdAdjustmentName = x.AdAdjustmentName,
                        AdjustmentValue = x.AdjustmentValue,

                        AmountPercent = x.AmountPercent,
                        GrossInd = x.GrossInd,
                        MediaOrderId = x.MediaOrderId,
                        MediaOrderLineId = x.MediaOrderLineId,
                        ProductCode = x.ProductCode,
                        SequenceNumber = x.SequenceNumber,
                        SurchargeDiscount = x.SurchargeDiscount,
                    }).OrderBy(x => x.SequenceNumber).ToList();

                    //Cost calculation for gross and net
                    int buyId = lstMediaOrders.Select(x => x.BuyId).FirstOrDefault();
                    response.StatusCode = ApiStatus.Ok;
                    response.Data = new { MediaOrderLines = lstResult, Cost = GetOrderNetAndGrossCost(buyId) };
                }
                else
                {
                    response.StatusCode = ApiStatus.InvalidInput;
                    response.Message = ApiStatus.InvalidInputMessge;
                }
            }

            catch (DbUpdateException ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;

                var entries = ex.Entries;
                foreach (var entry in entries)
                {
                    //change state to remove it from context 
                    switch (entry.State)
                    {
                        case EntityState.Added:
                            entry.State = EntityState.Detached;
                            break;
                        case EntityState.Modified:
                            entry.CurrentValues.SetValues(entry.OriginalValues);
                            entry.State = EntityState.Unchanged;
                            break;
                        case EntityState.Deleted:
                            entry.State = EntityState.Unchanged;
                            break;
                    }
                    // entry.State = System.Data.Entity.EntityState.Detached;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;

                foreach (var item in lstMediaOrderLine)
                {
                    DB.Entry(item).State = EntityState.Detached;
                }

            }
            return response;
        }
        public ResponseCommon AddImportMediaOrderLine(PostImportMediaOrderLineModel lineModel)
        {
            ResponseCommon response = new ResponseCommon();
            List<MediaOrderLine> lstMediaOrderLine = new List<MediaOrderLine>();
            try
            {
                List<MediaOrder> lstMediaOrders = DB.MediaOrders.Where(x => lineModel.MediaOrderIds.Contains(x.MediaOrderId) /*&& x.DeletedInd != true*/).ToList();
                if (lstMediaOrders.Count > 0)
                {
                    int[] mediaOrders = lstMediaOrders.Select(x => x.MediaOrderId).ToArray();
                    List<MediaOrderLine> lstBaseMediaOrderLines = lstMediaOrders.SelectMany(x => x.MediaOrderLines)./*Where(x => x.DeletedInd != true).*/OrderBy(x => x.SequenceNumber).ToList();
                    //List<MediaOrderLine> lstBaseMediaOrderLines = db.MediaOrderLines.Where(x => mediaOrders.Contains(x.MediaOrderId) && x.DeletedInd != true).ToList();
                    foreach (var order in lstMediaOrders)
                    {
                        double finalGrossCostOfOrder = (double)order.RateCardCost;
                        double finalNetCostOfOrder = (double)order.RateCardCost;
                        List<MediaOrderLine> currentMol = new List<MediaOrderLine>();
                        if (lineModel.GrossInd)
                        {
                            currentMol = lstBaseMediaOrderLines.Where(x => x.GrossInd == true && x.MediaOrderId == order.MediaOrderId)
                                                               .OrderBy(x => x.SequenceNumber).ToList();

                            // calculate previous discount/surcharges
                            CommonUtility.CalculateNetCost(ref finalNetCostOfOrder, currentMol);

                            // add new discount/surcharges
                            if (lineModel.AmountPercent == 0)
                            {
                                if (lineModel.SurchargeDiscount == 0)
                                    finalNetCostOfOrder = finalNetCostOfOrder + lineModel.AdjustmentValue;
                                else
                                    finalNetCostOfOrder = finalNetCostOfOrder - lineModel.AdjustmentValue;
                            }
                            else
                            {
                                if (lineModel.SurchargeDiscount == 0)
                                    finalNetCostOfOrder = finalNetCostOfOrder + ((finalNetCostOfOrder * lineModel.AdjustmentValue) / 100);
                                else
                                    finalNetCostOfOrder = finalNetCostOfOrder - ((finalNetCostOfOrder * lineModel.AdjustmentValue) / 100);
                            }


                            int sequenceNo = 1;
                            if (currentMol.Count > 0)
                                sequenceNo = currentMol.Max(x => x.SequenceNumber) + 1;

                            MediaOrderLine moline = new MediaOrderLine()
                            {
                                MediaOrderId = order.MediaOrderId,
                                SequenceNumber = sequenceNo,
                                AdAdjustmentName = lineModel.AdAdjustmentName,
                                AmountPercent = lineModel.AmountPercent,
                                SurchargeDiscount = lineModel.SurchargeDiscount,
                                AdjustmentValue = lineModel.AdjustmentValue,
                                AdjustmentAmount = lineModel.AdjustmentAmount,
                                ProductCode = order.ProductCode,
                                GrossInd = lineModel.GrossInd,
                                CreatedOn = lineModel.CreatedDate,
                                //CreatedByUserKey = Guid.Parse(lineModel.CreatedBy),
                                CreatedByUserKey = Constants.Created_Update_ByUserKey,

                                //*
                                UpdatedOn = lineModel.CreatedDate,
                                //UpdatedByUserKey = Guid.Parse(lineModel.CreatedBy)
                                UpdatedByUserKey = Constants.Created_Update_ByUserKey
                            };
                            lstMediaOrderLine.Add(moline);
                            // add new discount/surcharges end

                            // calculate gross cost
                            finalGrossCostOfOrder = finalNetCostOfOrder;

                            // calculate next discount/surcharges
                            currentMol = lstBaseMediaOrderLines.Where(x => x.GrossInd != true && x.MediaOrderId == order.MediaOrderId)
                                                                .OrderBy(x => x.SequenceNumber)
                                                                .ToList();
                            CommonUtility.CalculateNetCost(ref finalNetCostOfOrder, currentMol);
                        }
                        else
                        {

                            currentMol = lstBaseMediaOrderLines.Where(x => x.GrossInd == true
                                                                                      && x.MediaOrderId == order.MediaOrderId)
                                                                                   .OrderBy(x => x.SequenceNumber)
                                                                                   .ToList();
                            // calculate previous discount/surcharges
                            CommonUtility.CalculateNetCost(ref finalNetCostOfOrder, currentMol);

                            // calculate gross cost
                            finalGrossCostOfOrder = finalNetCostOfOrder;

                            currentMol = lstBaseMediaOrderLines.Where(x => x.GrossInd == false
                                                                                      && x.MediaOrderId == order.MediaOrderId)
                                                                                   .OrderBy(x => x.SequenceNumber)
                                                                                   .ToList();
                            // calculate next discount/surcharges
                            CommonUtility.CalculateNetCost(ref finalNetCostOfOrder, currentMol);

                            // add new discount/surcharges
                            if (lineModel.AmountPercent == 0)
                            {
                                if (lineModel.SurchargeDiscount == 0)
                                    finalNetCostOfOrder = finalNetCostOfOrder + lineModel.AdjustmentValue;
                                else
                                    finalNetCostOfOrder = finalNetCostOfOrder - lineModel.AdjustmentValue;
                            }
                            else
                            {
                                if (lineModel.SurchargeDiscount == 0)
                                    finalNetCostOfOrder = finalNetCostOfOrder + ((finalNetCostOfOrder * lineModel.AdjustmentValue) / 100);
                                else
                                    finalNetCostOfOrder = finalNetCostOfOrder - ((finalNetCostOfOrder * lineModel.AdjustmentValue) / 100);
                            }

                            int sequenceNo = 1;
                            if (currentMol.Count > 0)
                                sequenceNo = currentMol.Max(x => x.SequenceNumber) + 1;

                            MediaOrderLine moline = new MediaOrderLine()
                            {
                                MediaOrderId = order.MediaOrderId,
                                SequenceNumber = sequenceNo,
                                AdAdjustmentName = lineModel.AdAdjustmentName,
                                AmountPercent = lineModel.AmountPercent,
                                SurchargeDiscount = lineModel.SurchargeDiscount,
                                AdjustmentValue = lineModel.AdjustmentValue,
                                AdjustmentAmount = lineModel.AdjustmentAmount,
                                ProductCode = order.ProductCode,
                                GrossInd = lineModel.GrossInd,
                                CreatedOn = lineModel.CreatedDate,
                                //CreatedByUserKey = Guid.Parse(lineModel.CreatedBy),
                                CreatedByUserKey = Constants.Created_Update_ByUserKey,

                                //*
                                UpdatedOn = lineModel.CreatedDate,
                                //UpdatedByUserKey = Guid.Parse(lineModel.CreatedBy)
                                UpdatedByUserKey = Constants.Created_Update_ByUserKey
                            };
                            lstMediaOrderLine.Add(moline);
                        }
                        // add new discount/surcharges end

                        // Update ORDER NetCost
                        order.GrossCost = (decimal)finalGrossCostOfOrder;
                        order.NetCost = (decimal)finalNetCostOfOrder;
                        order.UpdatedOn = DateTime.Now;

                        //*
                        order.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                        DB.Entry(order).State = EntityState.Modified;
                        // Update ORDER NetCost end
                    }

                    if (lstMediaOrderLine.Count > 0)
                    {
                        DB.MediaOrderLines.AddRange(lstMediaOrderLine);
                    }
                    DB.SaveChanges();

                    List<MediaOrderLineModel> lstResult = new List<MediaOrderLineModel>();
                    lstResult = lstMediaOrderLine.Select(x => new MediaOrderLineModel
                    {
                        AdAdjustmentName = x.AdAdjustmentName,
                        AdjustmentValue = x.AdjustmentValue,

                        AmountPercent = x.AmountPercent,
                        GrossInd = x.GrossInd,
                        MediaOrderId = x.MediaOrderId,
                        MediaOrderLineId = x.MediaOrderLineId,
                        ProductCode = x.ProductCode,
                        SequenceNumber = x.SequenceNumber,
                        SurchargeDiscount = x.SurchargeDiscount,
                    }).OrderBy(x => x.SequenceNumber).ToList();

                    //Cost calculation for gross and net
                    int buyId = lstMediaOrders.Select(x => x.BuyId).FirstOrDefault();
                    response.StatusCode = ApiStatus.Ok;
                    response.Data = new { MediaOrderLines = lstResult, Cost = GetOrderNetAndGrossCost(buyId) };
                }
                else
                {
                    response.StatusCode = ApiStatus.InvalidInput;
                    response.Message = ApiStatus.InvalidInputMessge;
                }
            }

            catch (DbUpdateException ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;

                var entries = ex.Entries;
                foreach (var entry in entries)
                {
                    //change state to remove it from context 
                    switch (entry.State)
                    {
                        case EntityState.Added:
                            entry.State = EntityState.Detached;
                            break;
                        case EntityState.Modified:
                            entry.CurrentValues.SetValues(entry.OriginalValues);
                            entry.State = EntityState.Unchanged;
                            break;
                        case EntityState.Deleted:
                            entry.State = EntityState.Unchanged;
                            break;
                    }
                    // entry.State = System.Data.Entity.EntityState.Detached;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;

                foreach (var item in lstMediaOrderLine)
                {
                    DB.Entry(item).State = EntityState.Detached;
                }

            }
            return response;
        }
        public ResponseCommon GetMediaOrderLinesByMediaOrderId(int mediaOrderId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                List<MediaOrderLineModel> lstMediaOrderLines = new List<MediaOrderLineModel>();
                lstMediaOrderLines = DB.MediaOrderLines.Where(x => /*x.DeletedInd != true &&*/ x.MediaOrderId == mediaOrderId)
                                                        .Select(x => new MediaOrderLineModel
                                                        {
                                                            MediaOrderLineId = x.MediaOrderLineId,
                                                            MediaOrderId = x.MediaOrderId,
                                                            AdAdjustmentName = x.AdAdjustmentName,
                                                            AdAdjustmentId = x.MediaOrder.MediaAsset.AdAdjustments.Where(u => u.AdAdjustmentName == x.AdAdjustmentName).Select(u => u.AdAdjustmentId).FirstOrDefault(),
                                                            AdjustmentValue = x.AdjustmentValue,
                                                            AmountPercent = x.AmountPercent,
                                                            ProductCode = x.MediaOrder.MediaAsset.AdAdjustments.Where(y => /*y.DeletedInd != true &&*/ y.AdAdjustmentName == x.AdAdjustmentName).Select(z => z.ProductCode).FirstOrDefault(),
                                                            SequenceNumber = x.SequenceNumber,
                                                            SurchargeDiscount = x.SurchargeDiscount,
                                                            GrossInd = x.GrossInd
                                                        }).OrderBy(x => x.SequenceNumber).ToList();

                if (lstMediaOrderLines.Count > 0)
                {
                    response.StatusCode = ApiStatus.Ok;
                    response.Data = lstMediaOrderLines;
                }
                else
                {
                    response.StatusCode = ApiStatus.NoRecords;
                    response.Message = ApiStatus.NoRecordsMessge;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }

        public ResponseCommon GetLogoByBuyId(int buyId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                string imagepath = "";
                imagepath = (from mediaAsset in DB.MediaAssets
                             join mo in DB.MediaOrders
                             on mediaAsset.MediaAssetId
                             equals mo.MediaAssetId
                             where mo.BuyId == buyId
                             select mediaAsset.Logo).FirstOrDefault().ToString();
                response.StatusCode = ApiStatus.Ok;
                response.Data = imagepath;
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon DeleteMediaOrderLineByMediaOrderId(int mediaOrderId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                MediaOrder order = DB.MediaOrders.Where(x => x.MediaOrderId == mediaOrderId /*&& x.DeletedInd != true*/).FirstOrDefault();
                if (order != null)
                {
                    List<MediaOrderLine> lstMediaOrderLines = DB.MediaOrderLines.Where(x => x.MediaOrderId == mediaOrderId /*&& x.DeletedInd != true*/).OrderBy(x => x.SequenceNumber).ToList();
                    if (lstMediaOrderLines.Count > 0)
                    {
                        foreach (var ol in lstMediaOrderLines)
                        {
                            //ol.DeletedInd = true;
                            ol.UpdatedOn = DateTime.Now;

                            //*
                            ol.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                            DB.Entry(ol).State = EntityState.Modified;
                        }
                        order.NetCost = order.RateCardCost;
                        order.GrossCost = order.RateCardCost;
                        order.UpdatedOn = DateTime.Now;

                        //*
                        order.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                        DB.Entry(order).State = EntityState.Modified;

                        DB.SaveChanges();
                    }
                    response.Data = GetOrderNetAndGrossCost(order.BuyId);
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    response.StatusCode = ApiStatus.NotFound;
                    response.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon DeleteMediaOrderLineByMediaOrderLineIds(DeleteMediaOrderLinesModel lineModel)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                List<MediaOrderLine> lstMediaOrderLines = DB.MediaOrderLines.Where(x => lineModel.MediaOrderLineIds.Contains(x.MediaOrderLineId) /*&& x.DeletedInd != true*/).OrderBy(x => x.SequenceNumber).ToList();
                if (lstMediaOrderLines.Count > 0)
                {
                    foreach (var ol in lstMediaOrderLines)
                    {
                        //ol.DeletedInd = true;
                        ol.UpdatedOn = DateTime.Now;

                        //*
                        ol.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                        DB.Entry(ol).State = EntityState.Modified;
                    }
                    DB.SaveChanges();

                    // update NetCost 
                    int orderId = lstMediaOrderLines[0].MediaOrderId;
                    MediaOrder order = DB.MediaOrders.Where(x => x.MediaOrderId == orderId /*&& x.DeletedInd != true*/).FirstOrDefault();
                    if (order != null)
                    {
                        double finalGrossCostOfOrder = (double)order.RateCardCost;
                        double finalNetCostOfOrder = (double)order.RateCardCost;

                        List<MediaOrderLine> lstBaseMediaOrderLines = DB.MediaOrderLines.Where(x => x.MediaOrderId == orderId /*&& x.DeletedInd != true*/).OrderBy(x => x.SequenceNumber).ToList();
                        List<MediaOrderLine> currentMol = new List<MediaOrderLine>();
                        currentMol = lstBaseMediaOrderLines.Where(x => x.GrossInd == true && x.MediaOrderId == order.MediaOrderId)
                                                                                   .OrderBy(x => x.SequenceNumber).ToList();
                        // calculate previous discount/surcharges
                        CommonUtility.CalculateNetCost(ref finalNetCostOfOrder, currentMol);

                        // calculate gross cost
                        finalGrossCostOfOrder = finalNetCostOfOrder;

                        currentMol = lstBaseMediaOrderLines.Where(x => x.GrossInd == false && x.MediaOrderId == order.MediaOrderId)
                                                                               .OrderBy(x => x.SequenceNumber).ToList();
                        // calculate next discount/surcharges
                        CommonUtility.CalculateNetCost(ref finalNetCostOfOrder, currentMol);

                        order.GrossCost = (decimal)finalGrossCostOfOrder;
                        order.NetCost = (decimal)finalNetCostOfOrder;
                        order.UpdatedOn = DateTime.Now;

                        //*
                        order.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                        DB.Entry(order).State = EntityState.Modified;
                        DB.SaveChanges();

                        response.Data = GetOrderNetAndGrossCost(order.BuyId);
                        response.StatusCode = ApiStatus.Ok;
                    } // update NetCost  end
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon UpdateMediaOrderLines(MediaOrderLineUpdateModel lineModel)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                MediaOrder chkorder = DB.MediaOrders.Where(x => x.MediaOrderId == lineModel.MediaOrderId /*&& x.DeletedInd != true*/).FirstOrDefault();
                if (chkorder != null)
                {
                    List<MediaOrderLine> lstUpdateMediaOrderLines = DB.MediaOrderLines.Where(x => x.MediaOrderId == chkorder.MediaOrderId /*&& x.DeletedInd != true*/).OrderBy(x => x.SequenceNumber).ToList();
                    List<MediaOrderLine> sortedOrderLines = new List<MediaOrderLine>();

                    MediaOrderLine currentMediaOrderLine = lstUpdateMediaOrderLines.Where(x => x.MediaOrderLineId == lineModel.MediaOrderLineId).FirstOrDefault();
                    MediaOrderLine nextprevMediaOrderLine;
                    int nextprevSequence, currentSequence;
                    int tempSequence = currentMediaOrderLine.SequenceNumber;

                    if (currentMediaOrderLine.GrossInd)
                    {
                        sortedOrderLines = lstUpdateMediaOrderLines.Where(x => x.GrossInd == true).OrderBy(x => x.SequenceNumber).ToList();
                    }
                    else
                    {
                        sortedOrderLines = lstUpdateMediaOrderLines.Where(x => x.GrossInd == false).OrderBy(x => x.SequenceNumber).ToList();
                    }

                    int currentIndex = sortedOrderLines.IndexOf(currentMediaOrderLine);
                    if (lineModel.MovedUp)
                    {
                        if (currentIndex > 0)
                            nextprevMediaOrderLine = sortedOrderLines[currentIndex - 1];
                        else
                            nextprevMediaOrderLine = sortedOrderLines[currentIndex];
                    }
                    else
                    {
                        if ((currentIndex + 1) == sortedOrderLines.Count)
                            nextprevMediaOrderLine = sortedOrderLines[currentIndex];
                        else
                            nextprevMediaOrderLine = sortedOrderLines[currentIndex + 1];
                    }
                    if (!currentMediaOrderLine.Equals(nextprevMediaOrderLine))
                    {
                        currentSequence = nextprevMediaOrderLine.SequenceNumber;
                        nextprevSequence = tempSequence;

                        currentMediaOrderLine.SequenceNumber = currentSequence;
                        currentMediaOrderLine.UpdatedOn = DateTime.Now;

                        //*
                        currentMediaOrderLine.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                        nextprevMediaOrderLine.SequenceNumber = nextprevSequence;
                        //currentMediaOrderLine.UpdatedOn = DateTime.Now;

                        DB.Entry(currentMediaOrderLine).State = EntityState.Modified;
                        DB.Entry(nextprevMediaOrderLine).State = EntityState.Modified;

                        DB.SaveChanges();

                        // update GrossCost - NetCost 
                        int orderId = lineModel.MediaOrderId;
                        MediaOrder order = DB.MediaOrders.Where(x => x.MediaOrderId == orderId /*&& x.DeletedInd != true*/).FirstOrDefault();
                        if (order != null)
                        {
                            double finalGrossCostOfOrder = (double)order.RateCardCost;
                            double finalNetCostOfOrder = (double)order.RateCardCost;

                            List<MediaOrderLine> lstBaseMediaOrderLines = DB.MediaOrderLines.Where(x => x.MediaOrderId == orderId /*&& x.DeletedInd != true*/).OrderBy(x => x.SequenceNumber).ToList();
                            List<MediaOrderLine> currentMol = new List<MediaOrderLine>();
                            currentMol = lstBaseMediaOrderLines.Where(x => x.GrossInd == true && x.MediaOrderId == order.MediaOrderId)
                                                                                       .OrderBy(x => x.SequenceNumber).ToList();
                            // calculate previous discount/surcharges
                            CommonUtility.CalculateNetCost(ref finalNetCostOfOrder, currentMol);

                            // calculate gross cost
                            finalGrossCostOfOrder = finalNetCostOfOrder;

                            currentMol = lstBaseMediaOrderLines.Where(x => x.GrossInd == false && x.MediaOrderId == order.MediaOrderId)
                                                                                   .OrderBy(x => x.SequenceNumber).ToList();
                            // calculate next discount/surcharges
                            CommonUtility.CalculateNetCost(ref finalNetCostOfOrder, currentMol);

                            order.GrossCost = (decimal)finalGrossCostOfOrder;
                            order.NetCost = (decimal)finalNetCostOfOrder;
                            order.UpdatedOn = DateTime.Now;

                            //*
                            order.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                            DB.Entry(order).State = EntityState.Modified;
                            DB.SaveChanges();

                            response.Data = GetOrderNetAndGrossCost(order.BuyId);
                        } // update NetCost  end
                    }
                    else
                    {
                        response.Message = "Item is on same position";
                    }
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    response.StatusCode = ApiStatus.NotFound;
                    response.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        #endregion
    }
}
