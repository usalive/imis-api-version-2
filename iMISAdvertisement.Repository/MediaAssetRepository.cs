﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using Serilog;

namespace Asi.Repository
{
    public class MediaAssetRepository : IMediaAssetRepository
    {
        private readonly IDBEntities DB;
        public MediaAssetRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        #region Public Methods
        public ResponseCommonForDatatable GetMediaAssets()
        {
          
            ResponseCommonForDatatable response = new ResponseCommonForDatatable();
            List<GetMediaAssetModel> getMediaAssetModels = new List<GetMediaAssetModel>();
            int recordsTotal = 0;
            try
            {
                var context = HttpContext.Current;
                var currentRequest = HttpContext.Current.Request;
                string draw = currentRequest["draw"];
                string start = currentRequest["start"];
                string length = currentRequest["length"];
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                string domainUrl = CommonUtility.CurrentHostDomain;
                getMediaAssetModels = DB.MediaAssets./*Where(x => x.DeletedInd != true).*/Select(x =>
                    new GetMediaAssetModel
                    {
                        MediaAssetId = x.MediaAssetId,
                        MediaAssetName = x.MediaAssetName,
                        Logo = x.Logo == null | x.Logo == "" ? null : domainUrl + x.Logo,
                        ThumbLogo = x.Logo == null | x.Logo == "" ? null : domainUrl + x.Logo,
                        MediaAssetGroupId = x.MediaAssetGroupId,
                        MediaAssetGroupName = x.MediaAssetGroup.MediaAssetGroupName,
                        MediaCode = x.MediaCode,
                        MediaTypeId = x.MediaTypeId,
                        MediaTypeName = x.MediaTypeRef.MediaTypeName,
                        ProductCode = x.ProductCode,
                        NumberOfActiveRateCards = x.RateCards./*Where(y => y.DeletedInd != true).*/Count()
                    }).OrderBy(x => x.MediaAssetName).ToList();
                recordsTotal = getMediaAssetModels.Count();
                getMediaAssetModels = getMediaAssetModels.Skip(skip).Take(pageSize).ToList();

                if (getMediaAssetModels.Count > 0)
                {
                    int _draw = Convert.ToInt32(draw);
                    response.Draw = _draw;
                    response.RecordsTotal = recordsTotal;
                    response.RecordsFiltered = recordsTotal;
                    response.Data = getMediaAssetModels;
                    //Log.Information("Get List of mediaasset");
                }
                else
                {
                    int _draw = Convert.ToInt32(draw);
                    response.Draw = _draw;
                    response.RecordsTotal = recordsTotal;
                    response.RecordsFiltered = recordsTotal;
                    response.Data = getMediaAssetModels;
                }
              

           

                // Important to call at exit so that batched events are flushed.
                Log.CloseAndFlush();

                //Console.ReadKey(true);
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);

                    }
                }
            }
            catch (Exception ex)
            {
                //LoggerRepsitory.WriteErrorLog(ex);
                //response.statusCode = ApiStatus.Exception;
                //response.message = ApiStatus.ExceptionMessge;

                var context = HttpContext.Current;
                var currentRequest = HttpContext.Current.Request;
                string draw = currentRequest["draw"];
                int _draw = Convert.ToInt32(draw);
                recordsTotal = getMediaAssetModels.Count();
                response.Draw = _draw;
                response.RecordsTotal = recordsTotal;
                response.RecordsFiltered = recordsTotal;
                response.Data = getMediaAssetModels;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return response;
        }

        public ResponseCommon GetAllMediaAssets()
        {
            ResponseCommon objResponse = new ResponseCommon();
            List<GetMediaAssetModel> getMediaAssetModels = new List<GetMediaAssetModel>();
           
            try
            {
                string domainUrl = CommonUtility.CurrentHostDomain;
                getMediaAssetModels = DB.MediaAssets./*Where(x => x.DeletedInd != true).*/Select(x =>
                    new GetMediaAssetModel
                    {
                        MediaAssetId = x.MediaAssetId,
                        MediaAssetName = x.MediaAssetName,
                        Logo = x.Logo == null | x.Logo == "" ? null : domainUrl + x.Logo,
                        ThumbLogo = x.Logo == null | x.Logo == "" ? null : domainUrl + x.Logo,
                        MediaAssetGroupId = x.MediaAssetGroupId,
                        MediaAssetGroupName = x.MediaAssetGroup.MediaAssetGroupName,
                        MediaCode = x.MediaCode,
                        MediaTypeId = x.MediaTypeId,
                        MediaTypeName = x.MediaTypeRef.MediaTypeName,
                        ProductCode = x.ProductCode,
                        NumberOfActiveRateCards = x.RateCards./*Where(y => y.DeletedInd != true).*/Count()
                    }).OrderBy(x => x.MediaAssetName).ToList();

                if (getMediaAssetModels.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Data = null;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.Ok;
                    objResponse.Data = getMediaAssetModels;
                }
            }

            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
               
            }
            return objResponse;
        }


        public ResponseCommon GetMediaAssetsById(int mediaAssetId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                GetMediaAssetModel assetModel = null;
                string domainUrl = CommonUtility.CurrentHostDomain;

                assetModel = DB.MediaAssets.Where(x => /*x.DeletedInd != true &&*/ x.MediaAssetId == mediaAssetId)
                    .Select(x => new GetMediaAssetModel
                    {
                        MediaAssetId = x.MediaAssetId,
                        MediaAssetName = x.MediaAssetName,
                        Logo = x.Logo == null | x.Logo == "" ? null : domainUrl + x.Logo,
                        ThumbLogo = x.Logo == null | x.Logo == "" ? null : domainUrl + x.Logo,
                        MediaAssetGroupId = x.MediaAssetGroupId,
                        MediaAssetGroupName = x.MediaAssetGroup.MediaAssetGroupName,
                        MediaCode = x.MediaCode,
                        MediaTypeId = x.MediaTypeId,
                        MediaTypeName = x.MediaTypeRef.MediaTypeName,
                        ProductCode = x.ProductCode,
                        NumberOfActiveRateCards = x.RateCards./*Where(y => y.DeletedInd != true).*/Count()
                    }).FirstOrDefault();
                if (assetModel == null)
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Data = null;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                }
                else
                {
                    objResponse.Data = assetModel;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }
        public ResponseCommon PostMediaAsset(PostMediaAssetModel objMediaAsset)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
         
                int isRecordExist = DB.MediaAssets.Where(x => x.MediaAssetName.ToLower() == objMediaAsset.Name.ToLower()
                                                        /*&& x.DeletedInd != true*/).Count();
                if (isRecordExist <= 0)
                {
                    string fileName = CommonUtility.SaveBase64ToImage(objMediaAsset.Logo);

                    MediaAsset insertMediaAsset = new MediaAsset
                    {
                        MediaAssetName = objMediaAsset.Name,
                        Logo = fileName,
                        MediaAssetGroupId = objMediaAsset.MediaAssetGroupId,
                        MediaCode = objMediaAsset.MediaCode,
                        MediaTypeId = objMediaAsset.MediaTypeId,
                        ProductCode = objMediaAsset.ProductCode,
                        CreatedOn = DateTime.Now,
                        //DeletedInd = false,

                        //*
                        UpdatedOn = DateTime.Now,
                        CreatedByUserKey = Constants.Created_Update_ByUserKey,
                        UpdatedByUserKey = Constants.Created_Update_ByUserKey
                    };
                    DB.MediaAssets.Add(insertMediaAsset);
                    DB.SaveChanges();

                    objResponse.Data = insertMediaAsset.MediaAssetId;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.AlreadyExist;
                    objResponse.Message = ApiStatus.AlreadyExistCustomMessge("Media Asset Name");
                }
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);

                    }
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
                //Log.Information(ex.Message);
            }
            //Log.CloseAndFlush();
            return objResponse;
        }
        public ResponseCommon PutMediaAsset(PutMediaAssetModel objMediaAsset)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                int isRecordExist = DB.MediaAssets.Where(x => x.MediaAssetId != x.MediaAssetId
                                                        && x.MediaAssetName.ToLower() == objMediaAsset.Name.ToLower()
                                                        /*&& x.DeletedInd != true*/)
                                                    .Count();
                if (isRecordExist <= 0)
                {
                    MediaAsset objMediaAssetEdit = DB.MediaAssets
                                                    .Where(x => /*x.DeletedInd != true && */
                                                            x.MediaAssetId == objMediaAsset.MediaAssetId)
                                                    .FirstOrDefault();
                    if (objMediaAssetEdit != null)
                    {
                        if (!string.IsNullOrEmpty(objMediaAsset.Logo))
                        {
                            string fileName = CommonUtility.SaveBase64ToImage(objMediaAsset.Logo);
                            objMediaAssetEdit.Logo = fileName;
                        }
                        objMediaAssetEdit.MediaAssetName = objMediaAsset.Name;
                        objMediaAssetEdit.MediaAssetGroupId = objMediaAsset.MediaAssetGroupId;
                        objMediaAssetEdit.MediaCode = objMediaAsset.MediaCode;
                        objMediaAssetEdit.MediaTypeId = objMediaAsset.MediaTypeId;
                        objMediaAssetEdit.ProductCode = objMediaAsset.ProductCode;
                        objMediaAssetEdit.UpdatedOn = DateTime.Now;

                        //*
                        objMediaAssetEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                        DB.Entry(objMediaAssetEdit).State = EntityState.Modified;
                        DB.SaveChanges();

                        objResponse.Data = objMediaAssetEdit.MediaAssetId;
                        objResponse.StatusCode = ApiStatus.Ok;
                    }
                    else
                    {
                        objResponse.Message = ApiStatus.NotFoundMessage;
                        objResponse.StatusCode = ApiStatus.NotFound;
                    }
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.AlreadyExist;
                    objResponse.Message = ApiStatus.AlreadyExistCustomMessge("Media Asset Name");
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }
        public ResponseCommon DeleteMediaAsset(int mediaAssetId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                int status = 0;
                MediaAsset objMediaAssetEdit = DB.MediaAssets.Where(x => /*x.DeletedInd != true &&*/ x.MediaAssetId == mediaAssetId).FirstOrDefault();
                if (objMediaAssetEdit != null)
                {
                    //objMediaAssetEdit.DeletedInd = true;

                    //*
                    objMediaAssetEdit.UpdatedOn = DateTime.Now;
                    objMediaAssetEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objMediaAssetEdit).State = EntityState.Modified;

                    // Remove AdSizes of given MediaAsset
                    List<AdSize> lstSizes = DB.AdSizes.Where(x => x.MediaAssetId == mediaAssetId /*&& x.DeletedInd == false*/).ToList();
                    foreach (var size in lstSizes)
                    {
                        //size.DeletedInd = true;
                        size.UpdatedOn = DateTime.Now;

                        //*
                        size.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                        DB.Entry(size).State = EntityState.Modified;
                    }

                    // Remove AdAdjustments of given MediaAsset
                    List<AdAdjustment> lstAdAdjustment = DB.AdAdjustments.Where(x => x.MediaAssetId == mediaAssetId /*&& x.DeletedInd == false*/).ToList();
                    foreach (var adj in lstAdAdjustment)
                    {
                        //adj.DeletedInd = true;
                        adj.UpdatedOn = DateTime.Now;

                        //*
                        adj.UpdatedByUserKey = Constants.Created_Update_ByUserKey;
                        DB.Entry(adj).State = EntityState.Modified;
                    }

                    // Remove IssueDates of given MediaAsset
                    List<AdIssueDate> lstIssueDate = DB.AdIssueDates.Where(x => x.MediaAssetId == mediaAssetId /*&& x.DeletedInd == false*/).ToList();
                    foreach (var issuedate in lstIssueDate)
                    {
                        //issuedate.DeletedInd = true;
                        issuedate.UpdatedOn = DateTime.Now;

                        //*
                        issuedate.UpdatedByUserKey = Constants.Created_Update_ByUserKey;
                        DB.Entry(issuedate).State = EntityState.Modified;
                    }

                    // Remove RateCards of given MediaAsset
                    List<RateCard> lstRateCard = DB.RateCards.Where(x => x.MediaAssetId == mediaAssetId /*&& x.DeletedInd == false*/).ToList();
                    foreach (var ratecard in lstRateCard)
                    {
                        //ratecard.DeletedInd = true;
                        ratecard.UpdatedOn = DateTime.Now;

                        //*
                        ratecard.UpdatedByUserKey = Constants.Created_Update_ByUserKey;
                        DB.Entry(ratecard).State = EntityState.Modified;
                    }

                    // Remove RateDardDetails belong to RateCards found by given MediaAsset
                    var lstRateCardId = lstRateCard.Select(x => x.RateCardId);
                    List<RateCardDetail> lstRateCardDetails = DB.RateCardDetails.Where(x => lstRateCardId.Contains(x.RateCardId) /*&& x.DeletedInd == false*/).ToList();
                    foreach (var rateCardDetail in lstRateCardDetails)
                    {
                        //rateCardDetail.DeletedInd = true;
                        rateCardDetail.UpdatedOn = DateTime.Now;

                        //*
                        rateCardDetail.UpdatedByUserKey = Constants.Created_Update_ByUserKey;
                        DB.Entry(rateCardDetail).State = EntityState.Modified;
                    }

                    status = DB.SaveChanges();

                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.Message = ApiStatus.NotFoundMessage;
                    objResponse.StatusCode = ApiStatus.NotFound;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }
        public string GetDomainURL()
        {
            var urlPath = HttpContext.Current.Request.Url.AbsoluteUri;
            if (HttpContext.Current.Request.Url.AbsolutePath == "/")
                urlPath = urlPath.Substring(0, urlPath.Length - 1);
            if (urlPath.IndexOf(HttpContext.Current.Request.Url.AbsolutePath) != -1)
                urlPath = urlPath.Remove(urlPath.IndexOf(HttpContext.Current.Request.Url.AbsolutePath));
            return urlPath;
        }
        #endregion
    }
}
