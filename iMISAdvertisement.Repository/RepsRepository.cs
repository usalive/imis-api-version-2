﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Asi.Repository
{
    public class RepsRepository : IRepsRepository
    {
        private readonly IDBEntities DB;
        public RepsRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        #region Public Methods
        public ResponseCommon GetReps()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<RepModel> lstRep = new List<RepModel>();

                lstRep = DB.Reps
                                //.Where(x => x.DeletedInd != true)
                                .Select(x => new RepModel
                                {
                                    RepId = x.RepId,
                                    Name = x.RepName
                                })
                                .OrderBy(x => x.Name).ToList();
                if (lstRep.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }
                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstRep;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Data = null;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetRepsForOrderCreation()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<RepModel> lstRep = new List<RepModel>();

                var temp = DB.RepTerritories
                            //.Where(y => y.DeletedInd != true)
                            .Select(x => x.RepId).ToList();
                lstRep = DB.Reps.Where(x => /* x.DeletedInd != true && */ temp.Contains(x.RepId))
                                .Select(x => new RepModel
                                {
                                    RepId = x.RepId,
                                    Name = x.RepName
                                })
                                .OrderBy(x => x.Name).ToList();
                if (lstRep.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }
                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstRep;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Data = null;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetRepById(int repId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                RepModel objRep = null;
                objRep = DB.Reps.Where(x => /*x.DeletedInd != true &&*/ x.RepId == repId)
                                .Select(x => new RepModel
                                {
                                    RepId = x.RepId,
                                    Name = x.RepName
                                })
                                .FirstOrDefault();
                if (objRep == null)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                }
                else
                {
                    objResponse.Data = objRep;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon PostRep(RepModel repModel)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                int isRepExist = DB.Reps.Where(x => /*x.DeletedInd == false &&*/ x.RepName.ToLower() == repModel.Name.Trim().ToLower()).Count();
                if (isRepExist <= 0)
                {
                    Rep objnewrep = new Rep();
                    objnewrep.RepName = repModel.Name.Trim();
                    objnewrep.CreatedOn = DateTime.Now;
                    //objnewrep.DeletedInd = false;

                    //*
                    objnewrep.UpdatedOn = DateTime.Now;
                    objnewrep.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                    objnewrep.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Reps.Add(objnewrep);
                    DB.SaveChanges();

                    objResponse.Data = objnewrep.RepId;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.AlreadyExist;
                    objResponse.Message = ApiStatus.AlreadyExistMessge;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon PutRep(RepModel repModel)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                int isNameExist = DB.Reps
                                    .Where(x => x.RepName.ToLower() == repModel.Name.ToLower()
                                            && x.RepId != repModel.RepId
                                            /*&& x.DeletedInd == false*/).Count();
                if (isNameExist > 0)
                {
                    objResponse.StatusCode = ApiStatus.AlreadyExist;
                    objResponse.Message = ApiStatus.AlreadyExistCustomMessge("Rep Name");
                    return objResponse;
                }

                Rep objUpdateRep = DB.Reps.Where(x => /*x.DeletedInd != true &&*/ x.RepId == repModel.RepId).FirstOrDefault();
                if (objUpdateRep != null)
                {
                    objUpdateRep.RepName = repModel.Name;
                    objUpdateRep.UpdatedOn = DateTime.Now;

                    //*
                    objUpdateRep.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objUpdateRep).State = EntityState.Modified;
                    DB.SaveChanges();

                    objResponse.Data = repModel.RepId;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.Data = objUpdateRep.RepId;
                    objResponse.StatusCode = ApiStatus.NotFound;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon DeleteRep(int repId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                Rep objRep = DB.Reps.Where(x => /*x.DeletedInd != true &&*/ x.RepId == repId).FirstOrDefault();
                if (objRep != null)
                {
                    //objRep.DeletedInd = true;

                    //*
                    objRep.UpdatedOn = DateTime.Now;
                    objRep.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objRep).State = EntityState.Modified;
                    int status = DB.SaveChanges();
                    objResponse.Data = status <= 0 ? 0 : 1;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }
        #endregion
    }
}
