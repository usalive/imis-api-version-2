﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Asi.Repository
{
    public class AdvertiserRepTerritoryMapRepository : IAdvertiserRepTerritoryMapRepository
    {
        private readonly IDBEntities DB;
        public AdvertiserRepTerritoryMapRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        public ResponseCommon GetAdvertiserRepTerritoryMappingList()
        {
            //ResponseCommon response = new ResponseCommon();
            //response.Data = DB.AdvertiserRepTerritoryMappings.ToList();
            //response.StatusCode = ApiStatus.Ok;
            //return response;

            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<GetAdvertiserRepTerritoryMappingsModel> lstRepTerrMapping = new List<GetAdvertiserRepTerritoryMappingsModel>();
                lstRepTerrMapping = DB.AdvertiserRepTerritoryMappings
                                .Select(x => new GetAdvertiserRepTerritoryMappingsModel
                                {
                                    AdvertiserRepTerritoryMapId = x.AdvertiserRepTerritoryMapId,
                                    AdvertiserId = x.AdvertiserId,
                                    AdvertiserName = x.AdvertiserName,
                                    RepId = x.RepId,
                                    RepName = x.RepName,
                                    TerritoryId = x.TerritoryId,
                                    TerritoryName = x.TerritoryName,
                                    Percentage = x.Percentage,
                                    CreatedOn = x.CreatedOn,
                                    CreatedByUserKey = x.CreatedByUserKey,
                                    UpdatedOn = x.UpdatedOn,
                                    UpdatedByUserKey = x.UpdatedByUserKey
                                })
                                .OrderBy(x => x.AdvertiserRepTerritoryMapId)
                                .ToList();
                if (lstRepTerrMapping.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }
                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstRepTerrMapping;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetRepTerritoryMapping(string AdvertiserId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                //var data = DB.AdvertiserAgencyMappings.FirstOrDefault(h => h.AdvertiserId == AdvertiserId);

                //if (data != null)
                //{
                //    AdvertiserAgencyMappingGetModel advertiserAgencyMappingGetModel = new AdvertiserAgencyMappingGetModel
                //    {
                //        AdvertiserID = data.AdvertiserId,
                //        AgencyID = data.AgencyId,
                //        Relation = data.Relation
                //    };
                //    response.Data = advertiserAgencyMappingGetModel;
                //    response.StatusCode = ApiStatus.Ok;
                //    return response;
                //}
                var data = DB.AdvertiserRepTerritoryMappings.Where(h => h.AdvertiserId == AdvertiserId).ToList();

                if (data != null)
                {
                    if (data.Count() > 0)
                    {
                        List<AdvertiserRepTerritoryMappingGetModel> list = data.Select(x => new AdvertiserRepTerritoryMappingGetModel()
                        {
                            AdvertiserID = x.AdvertiserId,
                            Percentage = x.Percentage,
                            RepID = x.RepId,
                            TerritoryID = x.TerritoryId
                        }).ToList();
                        response.Data = list;
                    }
                    response.StatusCode = ApiStatus.Ok;
                    return response;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }

        public ResponseCommon GetRepTerritoryMappingListByAdvertiserId(string advertiserId)
        {
            ResponseCommon response = new ResponseCommon();
            List<AdvertiserRepTerritoryMappingGetModel> objRepTerritoryMappingList = new List<AdvertiserRepTerritoryMappingGetModel>();
            var advertiserRepTerritoryMappingListInDb = DB.AdvertiserRepTerritoryMappings.Where(x => x.AdvertiserId == advertiserId).ToList();
            if (advertiserRepTerritoryMappingListInDb != null)
            {
                foreach (AdvertiserRepTerritoryMap objData in advertiserRepTerritoryMappingListInDb)
                {
                    Int32 repId = Convert.ToInt32(objData.RepId);
                    Int32 territoryId = Convert.ToInt32(objData.TerritoryId);
                    var commissionInRepTerritoryDb = DB.RepTerritories.Where(x => x.Rep.RepId == repId && x.Territory.TerritoryId == territoryId).Take(1).SingleOrDefault();
                    objRepTerritoryMappingList.Add(new AdvertiserRepTerritoryMappingGetModel
                    {
                        AdvertiserID = objData.AdvertiserId,
                        AdvertiserName = objData.AdvertiserName,
                        RepID = objData.RepId,
                        RepName = objData.RepName,
                        TerritoryID = objData.TerritoryId,
                        TerritoryName = objData.TerritoryName,
                        Percentage = objData.Percentage,
                        Name = objData.RepName,
                        Commission = commissionInRepTerritoryDb == null ? 0 : Convert.ToInt64(commissionInRepTerritoryDb.Commission)
                    });
                }
                response.Data = objRepTerritoryMappingList;
            }
            else
            {
                response.Data = null;
            }
            response.StatusCode = ApiStatus.Ok;
            return response;
        }

        public ResponseCommon PostMapping(AdvertiserRepTerritoryMappingModel mappingModel)
        {
            int NumOfRecordInserted = 0;
            ResponseCommon response = new ResponseCommon();
            try
            {
                // First delete all the record based on advertiserID
                var data = DB.AdvertiserRepTerritoryMappings.Where(x => x.AdvertiserId == mappingModel.AdvertiserID).ToList();

                if (data != null)
                {
                    DB.AdvertiserRepTerritoryMappings.RemoveRange(data);
                    DB.SaveChanges();
                }

                // Add new data
                foreach (var item in mappingModel.ListRepTerritoryPercentage)
                {
                    try
                    {
                        Int16 repId = Convert.ToInt16(item.RepID);
                        Int16 territoryId = Convert.ToInt16(item.TerritoryID);
                        var repNameInDb = DB.Reps.Where(x => x.RepId == repId).SingleOrDefault();
                        var territoryNameInDb = DB.Territories.Where(x => x.TerritoryId == territoryId).SingleOrDefault();

                        AdvertiserRepTerritoryMap advertiserRepTerritoryMapping = new AdvertiserRepTerritoryMap
                        {
                            AdvertiserId = mappingModel.AdvertiserID,
                            AdvertiserName = mappingModel.AdvertiserName,
                            RepId = item.RepID,
                            RepName = (repNameInDb != null ? repNameInDb.RepName : ""),
                            TerritoryId = item.TerritoryID,
                            TerritoryName = (territoryNameInDb != null ? territoryNameInDb.TerritoryName : ""),
                            Percentage = item.Percentage,

                            //*
                            CreatedOn = DateTime.Now,
                            UpdatedOn = DateTime.Now,

                            CreatedByUserKey = Constants.Created_Update_ByUserKey,
                            UpdatedByUserKey = Constants.Created_Update_ByUserKey
                        };

                        DB.AdvertiserRepTerritoryMappings.Add(advertiserRepTerritoryMapping);
                        DB.SaveChanges();

                        if (advertiserRepTerritoryMapping.AdvertiserRepTerritoryMapId > 0)
                            NumOfRecordInserted += 1;
                        response.Data = NumOfRecordInserted;
                        response.StatusCode = ApiStatus.Ok;

                    }
                    catch (Exception ex)
                    {
                        // if an error occured then delete record created and set numofinsertedrow = 0
                        var deletedata = DB.AdvertiserRepTerritoryMappings.Where(x => x.AdvertiserId == mappingModel.AdvertiserID).ToList();
                        if (deletedata != null)
                        {
                            DB.AdvertiserRepTerritoryMappings.RemoveRange(deletedata);
                            DB.SaveChanges();
                        }
                        NumOfRecordInserted = 0;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }

        public ResponseCommon DeleteAdvertiserRepTerritoryMapping(int advertiserRepTerritoryMapId)
        {
            ResponseCommon response = new ResponseCommon();
            AdvertiserRepTerritoryMap advertiserRepTerritoryMap = DB.AdvertiserRepTerritoryMappings.Find(advertiserRepTerritoryMapId);
            if (advertiserRepTerritoryMap == null)
            {
                response.StatusCode = ApiStatus.NoRecords;
                response.Message = ApiStatus.NotFoundMessage;
            }
            else
            {
                DB.AdvertiserRepTerritoryMappings.Remove(advertiserRepTerritoryMap);
                DB.SaveChanges();
                response.StatusCode = ApiStatus.Ok;
                response.Message = ApiStatus.OkMessge;
            }
            return response;
        }
    }
}