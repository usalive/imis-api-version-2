﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Asi.Repository
{
    public class AdAdjustmentRepository : IAdAdjustmentRepository
    {
        private readonly IDBEntities DB;
        public AdAdjustmentRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        #region Public Methods
        public ResponseCommon GetAdAdjustments()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<GetAdAdjustmentModel> lstAdAdjustments = new List<GetAdAdjustmentModel>();
                lstAdAdjustments = DB.AdAdjustments
                                        //.Where(x => x.DeletedInd != true)
                                        .Select(x => new GetAdAdjustmentModel
                                        {
                                            AdAdjustmentId = x.AdAdjustmentId,
                                            Name = x.AdAdjustmentName,
                                            MediaAssetId = x.MediaAssetId,
                                            MediaAssetName = x.MediaAsset.MediaAssetName,
                                            AmountPercent = x.AmountPercent,
                                            SurchargeDiscount = x.SurchargeDiscount,
                                            AdjustmentTarget = x.AdjustmentTarget,
                                            AdjustmentValue = x.AdjustmentValue,
                                            ProductCode = x.ProductCode
                                        }).ToList();
                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstAdAdjustments;
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
            }
            return objResponse;
        }
        public ResponseCommon GetAdAdjustmentById(int adAdjustmentId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                GetAdAdjustmentModel objAdAdjustment = DB.AdAdjustments
                                                .Where(x => /*x.DeletedInd != true &&*/ x.AdAdjustmentId == adAdjustmentId)
                                                .Select(x => new GetAdAdjustmentModel
                                                {
                                                    AdAdjustmentId = x.AdAdjustmentId,
                                                    Name = x.AdAdjustmentName,
                                                    MediaAssetId = x.MediaAssetId,
                                                    MediaAssetName = x.MediaAsset.MediaAssetName,
                                                    AmountPercent = x.AmountPercent,
                                                    SurchargeDiscount = x.SurchargeDiscount,
                                                    AdjustmentTarget = x.AdjustmentTarget,
                                                    AdjustmentValue = x.AdjustmentValue,
                                                    ProductCode = x.ProductCode
                                                }).FirstOrDefault();
                if (objAdAdjustment != null)
                {
                    response.Data = objAdAdjustment;
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    response.Message = ApiStatus.NotFoundMessage;
                    response.StatusCode = ApiStatus.NotFound;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon GetAdAdjustmentsByMediaAsset(int mediaAssetId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                var lstAdAdjustments = DB.MediaAssets.Where(x =>/* x.DeletedInd != true &&*/ x.MediaAssetId == mediaAssetId)
                    .Select(x => new GetAdAdjustmentsByMediaAssetModel
                    {
                        MediaAssetId = x.MediaAssetId,
                        MediaAssetName = x.MediaAssetName,
                        AdAdjustments = x.AdAdjustments
                                        //.Where(y => y.DeletedInd != true)
                                        .Select(y => new GetAdAdjustmentModel
                        {
                            AdAdjustmentId = y.AdAdjustmentId,
                            Name = y.AdAdjustmentName,
                            MediaAssetId = y.MediaAssetId,
                            MediaAssetName = y.MediaAsset.MediaAssetName,
                            AmountPercent = y.AmountPercent,
                            SurchargeDiscount = y.SurchargeDiscount,
                            AdjustmentTarget = y.AdjustmentTarget,
                            AdjustmentValue = y.AdjustmentValue,
                            ProductCode = y.ProductCode
                        })
                    }).ToList();
                response.Data = lstAdAdjustments;
                response.StatusCode = ApiStatus.Ok;
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon GetAdAdjustmentsByMediaAssets(GetAdjustmentsByMediaAssetIdsModel model)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                // Base Query
                //SELECT aj.AdAdjustmentName,aj.AmountPercent,aj.SurchargeDiscount,aj.AdjustmentValue,COUNT(*) AS RecordCount
                //FROM dbo.MediaAsset ma
                //JOIN dbo.AdAdjustment aj ON aj.MediaAssetId = ma.MediaAssetId
                //GROUP BY aj.AmountPercent,aj.SurchargeDiscount,aj.AdjustmentValue,aj.AdAdjustmentName

                List<int> mediaAssetIds = model.MediaAssetIds.Distinct().ToList();
                var lstAdAdjustments = (from adj in DB.AdAdjustments
                                        join ma in DB.MediaAssets on adj.MediaAssetId equals ma.MediaAssetId
                                        where /*adj.DeletedInd != true && ma.DeletedInd != true &&*/ mediaAssetIds.Contains(ma.MediaAssetId)
                                        group adj by new { adj.AmountPercent, adj.SurchargeDiscount, adj.AdjustmentValue, adj.AdAdjustmentName, adj.ProductCode } into g
                                        let RecordCount = g.Count()
                                        select new
                                        {
                                            AdAdjustmentName = g.Key.AdAdjustmentName,
                                            AmountPercent = g.Key.AmountPercent,
                                            SurchargeDiscount = g.Key.SurchargeDiscount,
                                            AdjustmentValue = g.Key.AdjustmentValue,
                                            ProductCode = g.Key.ProductCode,
                                            RecordCount = RecordCount
                                        }).ToList();
                int i = 1;
                response.Data = lstAdAdjustments.Where(x => x.RecordCount == mediaAssetIds.Count)
                                .Select(x => new
                                {
                                    SrNo = i++,
                                    AdAdjustmentName = x.AdAdjustmentName,
                                    AmountPercent = x.AmountPercent,
                                    SurchargeDiscount = x.SurchargeDiscount,
                                    AdjustmentValue = x.AdjustmentValue,
                                    ProductCode = x.ProductCode
                                }).ToList();
                response.StatusCode = ApiStatus.Ok;
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon PostAdAdjustment(PostAdAdjustmentModel adAdjustmentModel)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                int isRecordExist = DB.AdAdjustments.Where(x => x.MediaAssetId == adAdjustmentModel.MediaAssetId
                                                    && x.AdAdjustmentName.ToLower() == adAdjustmentModel.Name.ToLower()
                                                    /*&& x.DeletedInd == false*/)
                                                     .Count();
                if (isRecordExist <= 0)
                {
                    AdAdjustment objAdAdj = new AdAdjustment();
                    objAdAdj.AdAdjustmentName = adAdjustmentModel.Name;
                    objAdAdj.MediaAssetId = adAdjustmentModel.MediaAssetId;
                    objAdAdj.AmountPercent = adAdjustmentModel.AmountPercent;
                    objAdAdj.SurchargeDiscount = adAdjustmentModel.SurchargeDiscount;
                    objAdAdj.AdjustmentTarget = adAdjustmentModel.AdjustmentTarget;
                    objAdAdj.AdjustmentValue = adAdjustmentModel.AdjustmentValue;
                    objAdAdj.ProductCode = adAdjustmentModel.ProductCode;
                    objAdAdj.CreatedOn = DateTime.Now;
                    //objAdAdj.DeletedInd = false;

                    //*
                    objAdAdj.UpdatedOn = DateTime.Now;
                    objAdAdj.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                    objAdAdj.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.AdAdjustments.Add(objAdAdj);
                    DB.SaveChanges();

                    response.Data = objAdAdj.AdAdjustmentId;
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    response.StatusCode = ApiStatus.AlreadyExist;
                    response.Message = ApiStatus.AlreadyExistMessge;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon PutAdAdjustment(GetAdAdjustmentModel adAdjustmentModel)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                int isNameExist = DB.AdAdjustments
                                    .Where(x => x.AdAdjustmentId != adAdjustmentModel.AdAdjustmentId
                                            && x.AdAdjustmentName.ToLower() == adAdjustmentModel.Name.ToLower()
                                            && x.MediaAssetId==adAdjustmentModel.MediaAssetId
                                            && x.AmountPercent == adAdjustmentModel.AmountPercent
                                            && x.SurchargeDiscount == adAdjustmentModel.SurchargeDiscount
                                            && x.AdjustmentTarget == adAdjustmentModel.AdjustmentTarget
                                            && x.AdjustmentValue == adAdjustmentModel.AdjustmentValue
                                            && x.ProductCode == adAdjustmentModel.ProductCode
                                            /*&& x.DeletedInd == false*/).Count();
                if (isNameExist > 0)
                {
                    response.StatusCode = ApiStatus.AlreadyExist;
                    response.Message = ApiStatus.AlreadyExistCustomMessge("Ad Adjustment details");
                    return response;
                }
                AdAdjustment objAdAdjustmentEdit = DB.AdAdjustments.Where(x => /*x.DeletedInd != true && */
                                                    x.AdAdjustmentId == adAdjustmentModel.AdAdjustmentId).FirstOrDefault();
                if (objAdAdjustmentEdit != null)
                {
                    objAdAdjustmentEdit.AdAdjustmentName = adAdjustmentModel.Name;
                    objAdAdjustmentEdit.MediaAssetId = adAdjustmentModel.MediaAssetId;
                    objAdAdjustmentEdit.AmountPercent = adAdjustmentModel.AmountPercent;
                    objAdAdjustmentEdit.SurchargeDiscount = adAdjustmentModel.SurchargeDiscount;
                    objAdAdjustmentEdit.AdjustmentTarget = adAdjustmentModel.AdjustmentTarget;
                    objAdAdjustmentEdit.AdjustmentValue = adAdjustmentModel.AdjustmentValue;
                    objAdAdjustmentEdit.ProductCode = adAdjustmentModel.ProductCode;
                    objAdAdjustmentEdit.UpdatedOn = DateTime.Now;
                    //*
                    objAdAdjustmentEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objAdAdjustmentEdit).State = EntityState.Modified;
                    DB.SaveChanges();

                    response.Data = adAdjustmentModel.AdAdjustmentId;
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    response.StatusCode = ApiStatus.NotFound;
                    response.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon DeleteAdAdjustment(int adAdjustmentId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                AdAdjustment objAdAdjustmentEdit = null;
                int status = 0;
                objAdAdjustmentEdit = DB.AdAdjustments
                                     .Where(x => /*x.DeletedInd != true &&*/ x.AdAdjustmentId == adAdjustmentId)
                                     .FirstOrDefault();
                if (objAdAdjustmentEdit != null)
                {
                    //objAdAdjustmentEdit.DeletedInd = true;
                    DB.Entry(objAdAdjustmentEdit).State = EntityState.Modified;
                    status = DB.SaveChanges();

                    response.Data = status <= 0 ? 0 : 1;
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    response.StatusCode = ApiStatus.NotFound;
                    response.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        #endregion
    }
}
