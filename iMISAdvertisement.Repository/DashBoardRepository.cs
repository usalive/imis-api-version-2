﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Globalization;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Web;

namespace Asi.Repository
{
    public class DashBoardRepository : IDashBoardRepository
    {
        private readonly IDBEntities DB;

        #region Constructor
        public DashBoardRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }
        #endregion

        #region Private Method
        private List<PartyModel> GetPartyData(List<string> advertiserIds)
        {
            dynamic data = null;
            List<PartyModel> lstParty = new List<PartyModel>();
            string ids = string.Empty;
            //PartyResponseModel objResponseParty = new PartyResponseModel();
            try
            {
                if (advertiserIds.Count > 0)
                {
                    for (int i = 0; i < advertiserIds.Count; i++)
                    {
                        ids += advertiserIds[i] + "|";
                    }
                }

                string asiPartApiUrl = string.Empty;
                //  string asiToken = string.Empty;
                string asiUserName = ConfigurationManager.AppSettings["ASIusername"].ToString();
                string asiPassword = ConfigurationManager.AppSettings["ASIpassword"].ToString();
                string asiGrantType = ConfigurationManager.AppSettings["ASIgrant_type"].ToString();

                var context = HttpContext.Current;
                var currentRequest = HttpContext.Current.Request;

                //ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });

                asiPartApiUrl = currentRequest["AsiPartyAPI"];
                // asiToken = currentRequest["AsiToken"];
                string basePath = asiPartApiUrl.Replace("api/Party", "");

                var client2 = new HttpClient();
                //client2.BaseAddress = new Uri("https://imistest.com/");
                client2.BaseAddress = new Uri(basePath);
                var request = new HttpRequestMessage(HttpMethod.Post, "token");

                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("username", asiUserName));
                keyValues.Add(new KeyValuePair<string, string>("password", asiPassword));
                keyValues.Add(new KeyValuePair<string, string>("grant_type", asiGrantType));

                request.Content = new FormUrlEncodedContent(keyValues);
                var response3 = client2.SendAsync(request);
                var responseJSONData = response3.Result.Content.ReadAsStringAsync().Result;
                var responseData = JsonConvert.DeserializeObject(responseJSONData);
                int issuedIndexOf = responseJSONData.LastIndexOf(".issued");
                int expiresIndexOf = responseJSONData.LastIndexOf(".expires");

                var textAfterissuedIndexOf = responseJSONData.Substring(issuedIndexOf);
                //var textAfterexpiresIndexOf = responseJSONData.Substring(expiresIndexOf);

                var textAfterReplaceissuedIndexOf = textAfterissuedIndexOf.Replace(".issued", "issued").Replace(".expires", "expires");
                //var textAfterReplaceexpiresIndexOf = textAfterexpiresIndexOf.Replace(".expires", "expires");
                var responseJSONDataAfterChanged = responseJSONData.Replace(textAfterissuedIndexOf, textAfterReplaceissuedIndexOf);
                TokenAPIResponse objAsiResponseData = JsonConvert.DeserializeObject<TokenAPIResponse>(responseJSONDataAfterChanged);

                if (!string.IsNullOrEmpty(objAsiResponseData.Access_token))
                //if (!string.IsNullOrEmpty(asiToken))
                {
                    string getPartyPath = string.Empty;
                    //url: websiteRoot + "api/Party?Id=in:" + AdvertisersIds + "",
                    getPartyPath = "api/Party?Id=in:" + ids;
                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri(basePath);
                    // Add an Accept header for JSON format.  
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + objAsiResponseData.Access_token);
                    // List all Names.  
                    HttpResponseMessage response = client.GetAsync(getPartyPath).Result;  // Blocking call!  
                    string text = string.Empty;
                    if (response.IsSuccessStatusCode)
                    {
                        text = response.Content.ReadAsStringAsync().Result.ToString();
                        data = JObject.Parse(text);

                        if (data != null)
                        {
                            var partyItemsData = data["Items"];
                            if (partyItemsData != null)
                            {

                                var partyValuesData = data["Items"]["$values"];
                                if (partyValuesData != null)
                                {
                                    var partyItemsDataFirstIndex = partyValuesData[0];
                                    JArray partyItemsDataInArray = (JArray)data["Items"]["$values"];
                                    int dataCount = partyItemsDataInArray.Count;
                                    if (dataCount > 0)
                                    {
                                        for (int i = 0; i < dataCount; i++)
                                        {
                                            try
                                            {
                                                PartyModel objParty = new PartyModel();
                                                var singleParty = (JObject)partyItemsDataInArray[i];
                                                var singlePartyType = singleParty["$type"].ToString();
                                                if (singlePartyType != null)
                                                {
                                                    string partyId = "0";
                                                    string partyName = string.Empty;
                                                    object _partyId = singleParty["Id"];
                                                    if (_partyId != null)
                                                    {
                                                        partyId = singleParty["Id"].ToString();
                                                    }

                                                    object _partyName = singleParty["Name"];
                                                    if (_partyName != null)
                                                    {
                                                        partyName = singleParty["Name"].ToString();
                                                    }

                                                    objParty.PartyId = partyId;
                                                    objParty.PartyName = partyName;
                                                    lstParty.Add(objParty);
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        string limit = (string)data.SelectToken("Limit");
                        string count = (string)data.SelectToken("Count");
                        string totalCount = (string)data.SelectToken("TotalCount");
                        string cOffset = (string)data.SelectToken("Offset");
                        int nextOffset = Convert.ToInt32(cOffset) + 500;

                    }
                }
            }
            catch (Exception ex)
            {
                string cError = "";
            }
            return lstParty;
        }

        #endregion

        #region Public Methods
        public ResponseCommon GetInsertionOrderCountbyOrderStatusForCurrentYear(GetOrdersWithProposalStatusbyFilterOption model)
        {
            ResponseCommon objResponse = new ResponseCommon();
            string test = string.Empty;

            try
            {
                string zoneQuery = model.MediaAssetId > 0 ? "MediaAssetId == " + model.MediaAssetId : "1=1";
                zoneQuery += model.IssueDateId > 0 ? " && IssueDateId == " + model.IssueDateId : " && 1=1";

                IQueryable<MediaOrder> lstMediaOrders;
                lstMediaOrders = DB.MediaOrders.Where(zoneQuery);

                if (lstMediaOrders.ToList().Count > 0)
                {
                    IQueryable<MediaOrdersBuyIdWiseStatusDetail> IQMediaOrderIdWiseStatusModel;
                    IQMediaOrderIdWiseStatusModel = (from ord in lstMediaOrders
                                                         //where ord.DeletedInd != true
                                                         //ord.AdIssueDate.CoverDate.Year == DateTime.Now.Year
                                                         // where ord.DeletedInd != true && ord.CreatedOn.Year == DateTime.Now.Year
                                                     orderby ord.MediaOrderId descending
                                                     group ord by new { ord.OrderStatus } into d
                                                     select new MediaOrdersBuyIdWiseStatusDetail
                                                     {
                                                         Status = d.Select(y => y.OrderStatus).FirstOrDefault(),
                                                         NoOfOrders = d.Count(),
                                                         MediaAssetId = d.Select(y => y.MediaAssetId).FirstOrDefault(),
                                                         IssueDateId = d.Select(y => y.IssueDateId).FirstOrDefault(),
                                                     });

                    List<MediaOrdersBuyIdWiseStatusDetail> lstMediaOrdersBuyIdWiseStatusDetail = IQMediaOrderIdWiseStatusModel.ToList();

                    if (lstMediaOrdersBuyIdWiseStatusDetail.Count == 0)
                    {
                        objResponse.StatusCode = ApiStatus.NoRecords;
                        objResponse.Message = ApiStatus.NoRecordsMessge;
                        return objResponse;
                    }

                    objResponse.StatusCode = ApiStatus.Ok;
                    objResponse.Data = lstMediaOrdersBuyIdWiseStatusDetail;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetOrderCountbyMonthlyForCurrentYear()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                var orderCountByMonthWiseModel = (from ord in DB.MediaOrders.AsEnumerable()
                                                  where /*ord.DeletedInd != true &&*/
                                                  ord.AdIssueDate.CoverDate.Year == DateTime.Now.Year
                                                  && ord.OrderStatus == APIMediaOrderStatus.Run
                                                  orderby ord.MediaOrderId descending
                                                  group ord by new { ord.AdIssueDate.CoverDate.Year, ord.AdIssueDate.CoverDate.Month } into d
                                                  select new GetOrderCountByMonthWise
                                                  {
                                                      Year = d.Select(y => y.AdIssueDate.CoverDate.Year).FirstOrDefault(),
                                                      Month = d.Select(y => y.AdIssueDate.CoverDate.Month).FirstOrDefault(),
                                                      MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(d.Select(y => y.AdIssueDate.CoverDate.Month).FirstOrDefault()),
                                                      //MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName("Mon").FirstOrDefault()),
                                                      NoOfOrders = d.Count(),
                                                      Revenue = d.Sum(y => y.NetCost),
                                                  });

                List<GetOrderCountByMonthWise> lstOrderCountByMonthWiseModel = orderCountByMonthWiseModel.ToList();
                if (lstOrderCountByMonthWiseModel.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }

                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstOrderCountByMonthWiseModel;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetOrderCountbyMonthlyForYearly()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<GetOrderCountByYearlyModel> lstGetOrderCountByYearlyModel = new List<GetOrderCountByYearlyModel>();
                var orderCountByMonthWiseModelForCurrentYr = (from ord in DB.MediaOrders.AsEnumerable()
                                                              where /*ord.DeletedInd != true &&*/ ord.CreatedOn.Year == DateTime.Now.Year && ord.OrderStatus == APIMediaOrderStatus.Run
                                                              orderby ord.MediaOrderId descending
                                                              group ord by new { ord.CreatedOn.Year, ord.CreatedOn.Month } into d
                                                              select new GetOrderCountByMonthWise
                                                              {
                                                                  Year = d.Select(y => y.CreatedOn.Year).FirstOrDefault(),
                                                                  Month = d.Select(y => y.CreatedOn.Month).FirstOrDefault(),
                                                                  MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(d.Select(y => y.CreatedOn.Month).FirstOrDefault()),
                                                                  NoOfOrders = d.Count(),
                                                                  Revenue = d.Sum(y => y.NetCost),
                                                              });

                List<GetOrderCountByMonthWise> lstOrderCountByMonthWiseModelCurrentYr = orderCountByMonthWiseModelForCurrentYr.ToList();
                var orderCountByMonthWiseModelPreviousYr = (from ord in DB.MediaOrders.AsEnumerable()
                                                            where /*ord.DeletedInd != true &&*/
                                                            (ord.CreatedOn.Year == DateTime.Now.Year - 1) && ord.OrderStatus == APIMediaOrderStatus.Run
                                                            orderby ord.MediaOrderId descending
                                                            group ord by new { ord.CreatedOn.Year, ord.CreatedOn.Month } into d
                                                            select new GetOrderCountByMonthWise
                                                            {
                                                                Year = d.Select(y => y.CreatedOn.Year).FirstOrDefault(),
                                                                Month = d.Select(y => y.CreatedOn.Month).FirstOrDefault(),
                                                                MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(d.Select(y => y.CreatedOn.Month).FirstOrDefault()),
                                                                NoOfOrders = d.Count(),
                                                                Revenue = d.Sum(y => y.NetCost),
                                                            });

                List<GetOrderCountByMonthWise> lstOrderCountByMonthWiseModelPreviousYr = orderCountByMonthWiseModelPreviousYr.ToList();
                if (lstOrderCountByMonthWiseModelCurrentYr.Count > 0 || lstOrderCountByMonthWiseModelPreviousYr.Count > 0)
                {
                    GetOrderCountByYearlyModel objGetOrderCountByYearlyModel = new GetOrderCountByYearlyModel
                    {
                        CurrentYear = lstOrderCountByMonthWiseModelCurrentYr,
                        PreviousYear = lstOrderCountByMonthWiseModelPreviousYr
                    };
                    lstGetOrderCountByYearlyModel.Add(objGetOrderCountByYearlyModel);
                }
                if (lstGetOrderCountByYearlyModel.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }
                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstGetOrderCountByYearlyModel;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetInsertionOrderCount()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<GetInserationOrderCountYearlyModel> lstInserationOrderCountModel = new List<GetInserationOrderCountYearlyModel>();
                List<MediaOrder> lstMediaOrderCurrentYear = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/
                x.OrderStatus == APIMediaOrderStatus.Run
                && x.CreatedOn.Year == DateTime.Now.Year).ToList();

                List<MediaOrder> lstMediaOrderPreviousYear = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/
                x.OrderStatus == APIMediaOrderStatus.Run
                && x.CreatedOn.Year == (DateTime.Now.Year - 1)).ToList();


                if (lstMediaOrderCurrentYear.Count > 0 || lstMediaOrderPreviousYear.Count > 0)
                {
                    GetInserationOrderCountYearlyModel objInserationOrderCountYearlyModel = new GetInserationOrderCountYearlyModel();
                    GetInserationOrderCountModel objPreviousYrOrderModel = new GetInserationOrderCountModel();
                    List<GetInserationOrderCountModel> lstPreviousYrOrderModel = new List<GetInserationOrderCountModel>();
                    objPreviousYrOrderModel.InserationOrderCount = lstMediaOrderPreviousYear.Count;
                    lstPreviousYrOrderModel.Add(objPreviousYrOrderModel);

                    GetInserationOrderCountModel objCurrentYrOrderModel = new GetInserationOrderCountModel();
                    List<GetInserationOrderCountModel> lstCurrentYrOrderModel = new List<GetInserationOrderCountModel>();
                    objCurrentYrOrderModel.InserationOrderCount = lstMediaOrderCurrentYear.Count;
                    lstCurrentYrOrderModel.Add(objCurrentYrOrderModel);
                    objInserationOrderCountYearlyModel.PreviousYear = lstPreviousYrOrderModel;
                    objInserationOrderCountYearlyModel.CurrentYear = lstCurrentYrOrderModel;
                    lstInserationOrderCountModel.Add(objInserationOrderCountYearlyModel);
                }

                if (lstInserationOrderCountModel.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }

                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstInserationOrderCountModel;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }
        public ResponseCommon GetTotalNetAmount()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {

                List<GetTotalNetCostYearlyModel> lstTotalNetCostModel = new List<GetTotalNetCostYearlyModel>();
                List<MediaOrder> lstMediaOrderCurrentYr = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/
                x.OrderStatus == APIMediaOrderStatus.Run
                && x.CreatedOn.Year == DateTime.Now.Year).ToList();

                List<MediaOrder> lstMediaOrderPreviousYr = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/
                x.OrderStatus == APIMediaOrderStatus.Run
                && x.CreatedOn.Year == (DateTime.Now.Year - 1)).ToList();

                if (lstMediaOrderCurrentYr.Count > 0 || lstMediaOrderPreviousYr.Count > 0)
                {
                    GetTotalNetCostYearlyModel objTotalNetCostYearlyModel = new GetTotalNetCostYearlyModel();
                    GetTotalNetCostModel objTotalNetCostModelCurrentYr = new GetTotalNetCostModel();
                    List<GetTotalNetCostModel> lstTotalNetCostModelCurrentYr = new List<GetTotalNetCostModel>();
                    objTotalNetCostModelCurrentYr.NetCost = lstMediaOrderCurrentYr.Sum(x => x.NetCost);
                    lstTotalNetCostModelCurrentYr.Add(objTotalNetCostModelCurrentYr);

                    GetTotalNetCostModel objTotalNetCostModelPreviousYr = new GetTotalNetCostModel();
                    List<GetTotalNetCostModel> lstTotalNetCostModelPreviousYr = new List<GetTotalNetCostModel>();
                    objTotalNetCostModelPreviousYr.NetCost = lstMediaOrderPreviousYr.Sum(x => x.NetCost);
                    lstTotalNetCostModelPreviousYr.Add(objTotalNetCostModelPreviousYr);

                    objTotalNetCostYearlyModel.PreviousYear = lstTotalNetCostModelPreviousYr;
                    objTotalNetCostYearlyModel.CurrentYear = lstTotalNetCostModelCurrentYr;
                    lstTotalNetCostModel.Add(objTotalNetCostYearlyModel);
                }

                if (lstTotalNetCostModel.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }

                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstTotalNetCostModel;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }
        public ResponseCommon GetMediaAssetUsedInOrder()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                IQueryable<GetUsedMediaAssetModel> getUsedMediaAssetModels;
                getUsedMediaAssetModels = (from ord in DB.MediaOrders
                                           where /*ord.DeletedInd != true &&*/
                                           ord.OrderStatus == APIMediaOrderStatus.Run
                                           && (ord.CreatedOn.Year == DateTime.Now.Year - 1)
                                           group ord by new { ord.MediaAssetId } into d
                                           select new GetUsedMediaAssetModel
                                           {
                                               MediaAssetCount = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/
                                               x.CreatedOn.Year == DateTime.Now.Year - 1).GroupBy(x => x.MediaAssetId).Count(),
                                           }).Take(1);

                List<GetUsedMediaAssetModel> lstUsedMediaAssetModelPreviousYr = getUsedMediaAssetModels.ToList();

                IQueryable<GetUsedMediaAssetModel> assetModels;
                assetModels = (from ord in DB.MediaOrders
                               where /*ord.DeletedInd != true &&*/ ord.OrderStatus == APIMediaOrderStatus.Run && ord.CreatedOn.Year == DateTime.Now.Year
                               group ord by new { ord.MediaAssetId } into d
                               select new GetUsedMediaAssetModel
                               {
                                   MediaAssetCount = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/ 
                                   x.OrderStatus == APIMediaOrderStatus.Run 
                                   && x.CreatedOn.Year == DateTime.Now.Year).GroupBy(x => x.MediaAssetId).Count(),
                               }).Take(1);

                List<GetUsedMediaAssetModel> lstUsedMediaAssetModelCurrentYr = assetModels.ToList();

                GetUsedMediaAssetYearlyModel objUsedMediaAssetYearlyModel = new GetUsedMediaAssetYearlyModel();
                List<GetUsedMediaAssetYearlyModel> lstUsedMediaAssetYearlyModel = new List<GetUsedMediaAssetYearlyModel>();
                objUsedMediaAssetYearlyModel.PreviousYear = lstUsedMediaAssetModelPreviousYr;
                objUsedMediaAssetYearlyModel.CurrentYear = lstUsedMediaAssetModelCurrentYr;
                lstUsedMediaAssetYearlyModel.Add(objUsedMediaAssetYearlyModel);

                if (lstUsedMediaAssetYearlyModel.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }

                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstUsedMediaAssetYearlyModel;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }
        public ResponseCommon GetAdvertiserUsedInOrder()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                IQueryable<GetUsedAdvertiserModel> getUsedAdvertiserModels;
                getUsedAdvertiserModels = (from ord in DB.MediaOrders
                                           where /*ord.DeletedInd != true &&*/ 
                                           ord.OrderStatus == APIMediaOrderStatus.Run 
                                           && ord.CreatedOn.Year == (DateTime.Now.Year - 1)
                                           group ord by new { ord.AdvertiserId, ord.BtId } into d
                                           select new GetUsedAdvertiserModel
                                           {
                                               AdvertiserCount = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/ 
                                               x.OrderStatus == APIMediaOrderStatus.Run 
                                               && x.CreatedOn.Year == (DateTime.Now.Year - 1)).GroupBy(x => new { x.AdvertiserId, x.BtId }).Count(),
                                           }).Take(1);

                List<GetUsedAdvertiserModel> lstUsedAdvertiserModelPreviousYr = getUsedAdvertiserModels.ToList();

                IQueryable<GetUsedAdvertiserModel> advertiserModels;
                advertiserModels = (from ord in DB.MediaOrders
                                    where /*ord.DeletedInd != true &&*/ 
                                    ord.OrderStatus == APIMediaOrderStatus.Run 
                                    && ord.CreatedOn.Year == DateTime.Now.Year
                                    group ord by new { ord.AdvertiserId, ord.BtId } into d
                                    select new GetUsedAdvertiserModel
                                    {
                                        AdvertiserCount = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/ 
                                        x.OrderStatus == APIMediaOrderStatus.Run 
                                        && x.CreatedOn.Year == (DateTime.Now.Year)).GroupBy(x => new { x.AdvertiserId, x.BtId }).Count(),
                                    }).Take(1);

                List<GetUsedAdvertiserModel> lstUsedAdvertiserModelCurrentYr = advertiserModels.ToList();

                GetUsedAdvertiserYearlyModel objUsedAdvertiserYearlyModel = new GetUsedAdvertiserYearlyModel();
                List<GetUsedAdvertiserYearlyModel> lstUsedAdvertiserModel = new List<GetUsedAdvertiserYearlyModel>();
                objUsedAdvertiserYearlyModel.PreviousYear = lstUsedAdvertiserModelPreviousYr;
                objUsedAdvertiserYearlyModel.CurrentYear = lstUsedAdvertiserModelCurrentYr;
                lstUsedAdvertiserModel.Add(objUsedAdvertiserYearlyModel);

                if (lstUsedAdvertiserModel.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }

                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstUsedAdvertiserModel;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommonForDatatable GetOrderByAdvertisers()
        {
            // ResponseCommon response = new ResponseCommon();
            ResponseCommonForDatatable response = new ResponseCommonForDatatable();
            List<GetOrdersByAdvertiserModel> lstMediaOrderModel = new List<GetOrdersByAdvertiserModel>();
            int recordsTotal = 0;
            try
            {
                var context = HttpContext.Current;
                var currentRequest = HttpContext.Current.Request;

                string draw = currentRequest["draw"];
                string start = currentRequest["start"];
                string length = currentRequest["length"];
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;

                IQueryable<GetOrdersByAdvertiserModel> getOrdersByAdvertiserModels;
                getOrdersByAdvertiserModels = (from ord in DB.MediaOrders
                                               where /*ord.DeletedInd != true &&*/ 
                                               ord.AdIssueDate.CoverDate.Year == DateTime.Now.Year 
                                               && ord.OrderStatus == APIMediaOrderStatus.Run
                                               // where ord.DeletedInd != true && ord.CreatedOn.Year == DateTime.Now.Year
                                               // orderby ord.NetCost descending
                                               group ord by new { ord.AdvertiserId } into d
                                               select new GetOrdersByAdvertiserModel
                                               {
                                                   AdvertiserId = d.Key.AdvertiserId,
                                                   BuyId = d.Select(y => y.BuyId).FirstOrDefault(),
                                                   NoOfOrders = d.Count(),
                                                   NetCostAmount = d.Sum(y => y.NetCost)
                                               }).OrderByDescending(t => t.NetCostAmount);
                //  List<GetOrdersByAdvertiserModel> lstOrdersByAdvertiserModel = IQOrdersByAdvertiserModel.ToList();
                lstMediaOrderModel = getOrdersByAdvertiserModels.ToList();

                recordsTotal = lstMediaOrderModel.Count();
                lstMediaOrderModel = lstMediaOrderModel.Skip(skip).Take(pageSize).ToList();

                if (lstMediaOrderModel.Count > 0)
                {
                    List<string> advertiserIds = lstMediaOrderModel.Select(t => t.AdvertiserId).ToList();
                    List<PartyModel> objPartyData = new List<PartyModel>();
                    objPartyData = GetPartyData(advertiserIds);
                    foreach (var item in lstMediaOrderModel)
                    {
                        var objParty = objPartyData.Where(x => x.PartyId == item.AdvertiserId).FirstOrDefault();
                        item.AdvertiserName = objParty.PartyName;
                    }
                }

                if (lstMediaOrderModel.Count > 0)
                {

                    response.Draw = Convert.ToInt32(draw);
                    response.RecordsTotal = recordsTotal;
                    response.RecordsFiltered = recordsTotal;
                    response.Data = lstMediaOrderModel;
                }
                else
                {

                    response.Draw = Convert.ToInt32(draw);
                    response.RecordsTotal = recordsTotal;
                    response.RecordsFiltered = recordsTotal;
                    response.Data = lstMediaOrderModel;
                }
                if (lstMediaOrderModel.Count == 0)
                {
                    //  objResponse.statusCode = ApiStatus.NoRecords;
                    //   objResponse.message = ApiStatus.NoRecordsMessge;
                    //  return objResponse;
                }

                //  objResponse.statusCode = ApiStatus.Ok;
                // objResponse.data = lstOrdersByAdvertiserModel;
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);

                    }
                }
            }
            catch (Exception ex)
            {
                //LoggerRepsitory.WriteErrorLog(ex);
                //response.statusCode = ApiStatus.Exception;
                //response.message = ApiStatus.ExceptionMessge;

                var context = HttpContext.Current;
                var currentRequest = HttpContext.Current.Request;
                string draw = currentRequest["draw"];
                recordsTotal = lstMediaOrderModel.Count();
                response.Draw = Convert.ToInt32(draw);
                response.RecordsTotal = recordsTotal;
                response.RecordsFiltered = recordsTotal;
                response.Data = lstMediaOrderModel;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return response;
        }
        public ResponseCommonForDatatable GetOrderDetailsByMediaAssets()
        {
            // ResponseCommon objResponse = new ResponseCommon();
            ResponseCommonForDatatable response = new ResponseCommonForDatatable();
            List<GetOrdersDetailsByMediaAssetsModel> lstMediaOrderModel = new List<GetOrdersDetailsByMediaAssetsModel>();
            int recordsTotal = 0;
            try
            {
                var context = HttpContext.Current;
                var currentRequest = HttpContext.Current.Request;
                string draw = currentRequest["draw"];
                string start = currentRequest["start"];
                string length = currentRequest["length"];
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;

                IQueryable<GetOrdersDetailsByMediaAssetsModel> getOrdersDetailsByMediaAssetsModels;
                getOrdersDetailsByMediaAssetsModels = (from ord in DB.MediaOrders
                                                       where /*ord.DeletedInd != true &&*/
                                                       ord.AdIssueDate.CoverDate.Year == DateTime.Now.Year
                                                       && ord.OrderStatus == APIMediaOrderStatus.Run
                                                       //where ord.DeletedInd != true && ord.CreatedOn.Year == DateTime.Now.Year
                                                       orderby ord.NetCost descending
                                                       group ord by new { ord.MediaAssetId } into d
                                                       select new GetOrdersDetailsByMediaAssetsModel
                                                       {
                                                           MediaAssetId = d.Key.MediaAssetId,
                                                           MediaAssetName = d.Select(y => y.MediaAsset.MediaAssetName).FirstOrDefault(),
                                                           Revenue = d.Sum(y => y.NetCost),
                                                           TotalQuantity = d.Count(),
                                                           LastOrderDate = d.OrderByDescending(y => y.CreatedOn).Select(z => z.CreatedOn).FirstOrDefault(),
                                                       }).OrderByDescending(y => y.Revenue);
                //  List<GetOrdersDetailsByMediaAssetsModel> lstOrdersDetailsByMediaAssetsModel = IQOrdersDetailsByMediaAssetsModel.ToList();
                lstMediaOrderModel = getOrdersDetailsByMediaAssetsModels.ToList();

                recordsTotal = lstMediaOrderModel.Count();
                lstMediaOrderModel = lstMediaOrderModel.Skip(skip).Take(pageSize).ToList();

                if (lstMediaOrderModel.Count > 0)
                {

                    response.Draw = Convert.ToInt32(draw);
                    response.RecordsTotal = recordsTotal;
                    response.RecordsFiltered = recordsTotal;
                    response.Data = lstMediaOrderModel;
                }
                else
                {
                    response.Draw = Convert.ToInt32(draw);
                    response.RecordsTotal = recordsTotal;
                    response.RecordsFiltered = recordsTotal;
                    response.Data = lstMediaOrderModel;
                }

            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);

                    }
                }
            }
            catch (Exception ex)
            {
                //LoggerRepsitory.WriteErrorLog(ex);
                //response.statusCode = ApiStatus.Exception;
                //response.message = ApiStatus.ExceptionMessge;

                var context = HttpContext.Current;
                var currentRequest = HttpContext.Current.Request;
                string draw = currentRequest["draw"];

                recordsTotal = lstMediaOrderModel.Count();
                response.Draw = Convert.ToInt32(draw);
                response.RecordsTotal = recordsTotal;
                response.RecordsFiltered = recordsTotal;
                response.Data = lstMediaOrderModel;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return response;
        }
        public ResponseCommonForDatatable GetOrdersByProposalStatus()
        {
            // ResponseCommon objResponse = new ResponseCommon();
            GetOrdersWithProposalStatusbyFilterOption model = new GetOrdersWithProposalStatusbyFilterOption();
            ResponseCommonForDatatable response = new ResponseCommonForDatatable();
            List<MediaOrderWithProposalStatusModel> lstMediaOrderModel = new List<MediaOrderWithProposalStatusModel>();
            int recordsTotal = 0;
            try
            {
                var context = HttpContext.Current;
                var currentRequest = HttpContext.Current.Request;

                string mediaAssetId = currentRequest["MediaAssetId"];
                string issueDateId = currentRequest["IssueDateId"];

                int _MediaAssetId = mediaAssetId == "" ? 0 : Convert.ToInt32(mediaAssetId);
                int _IssueDateId = issueDateId == "" ? 0 : Convert.ToInt32(issueDateId);

                model.MediaAssetId = _MediaAssetId;
                model.IssueDateId = _IssueDateId;

                string draw = currentRequest["draw"];
                string start = currentRequest["start"];
                string length = currentRequest["length"];
                string pageSize = (length != null ? Convert.ToInt32(length) : 0).ToString();
                string skip = (start != null ? Convert.ToInt32(start) : 0).ToString();

                List<asi_GetSPOrdersForPostOrder_Result> lstSpResultMediaAdvertiser = DB.GetSPOrdersForPostOrder(_MediaAssetId, _IssueDateId, 0, 0, 0, APIMediaOrderStatus.Proposal, skip, pageSize).ToList();
                foreach (var item in lstSpResultMediaAdvertiser)
                {
                    recordsTotal = Convert.ToInt32(item.TotalCount);
                    lstMediaOrderModel.Add(new MediaOrderWithProposalStatusModel
                    {
                        BuyId = (Int32)item.BuyId,
                        MediaOrderId = (Int32)item.MediaOrderId,
                        MediaAssetId = (Int32)item.MediaAssetId,
                        IssueDateId = (Int32)item.IssueDateId,
                        AdvertiserId = item.AdvertiserId,
                        OrderStatus = item.OrderStatus,
                        BT_ID = item.BtId,
                        ST_ID = item.StId,
                        GrossCost = (decimal)item.GrossCost,
                        NetCost = (decimal)item.NetCost,
                    });
                }




                if (lstMediaOrderModel.Count > 0)
                {
                    List<string> advertiserIds = lstMediaOrderModel.Select(t => t.AdvertiserId).ToList();
                    List<string> stIds = lstMediaOrderModel.Select(t => t.ST_ID).ToList();
                    advertiserIds.AddRange(stIds);
                    List<PartyModel> objPartyData = new List<PartyModel>();
                    objPartyData = GetPartyData(advertiserIds);
                    foreach (var item in lstMediaOrderModel)
                    {
                        var objAdvParty = objPartyData.Where(x => x.PartyId == item.AdvertiserId).FirstOrDefault();
                        item.AdvertiserName = objAdvParty.PartyName;

                        var objSTParty = objPartyData.Where(x => x.PartyId == item.ST_ID).FirstOrDefault();
                        item.BillToContactName = objSTParty.PartyName;
                    }
                }

                if (lstMediaOrderModel.Count > 0)
                {

                    response.Draw = Convert.ToInt32(draw);
                    response.RecordsTotal = recordsTotal;
                    response.RecordsFiltered = recordsTotal;
                    response.Data = lstMediaOrderModel;
                }
                else
                {

                    response.Draw = Convert.ToInt32(draw);
                    response.RecordsTotal = recordsTotal;
                    response.RecordsFiltered = recordsTotal;
                    response.Data = lstMediaOrderModel;
                }

            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);

                    }
                }
            }
            catch (Exception ex)
            {
                //LoggerRepsitory.WriteErrorLog(ex);
                //response.statusCode = ApiStatus.Exception;
                //response.message = ApiStatus.ExceptionMessge;

                var context = HttpContext.Current;
                var currentRequest = HttpContext.Current.Request;
                string draw = currentRequest["draw"];
                recordsTotal = lstMediaOrderModel.Count();
                response.Draw = Convert.ToInt32(draw);
                response.RecordsTotal = recordsTotal;
                response.RecordsFiltered = recordsTotal;
                response.Data = lstMediaOrderModel;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return response;
        }


        //public ResponseCommonForDatatable GetOrdersByProposalStatus()
        //{
        //    ResponseCommonForDatatable objResponse = new ResponseCommonForDatatable();
        //    GetOrdersWithProposalStatusbyFilterOption model = new GetOrdersWithProposalStatusbyFilterOption();
        //    List<MediaOrderWithProposalStatusModel1> lstMediaOrderWithProposalStatusModel = new List<MediaOrderWithProposalStatusModel1>();
        //    int recordsTotal = 0;
        //    try
        //    {
        //        var context = HttpContext.Current;
        //        var currentRequest = HttpContext.Current.Request;
        //        var MediaAssetId = currentRequest["MediaAssetId"];
        //        var IssueDateId = currentRequest["IssueDateId"];
        //        int _MediaAssetId = MediaAssetId == "" ? 0 : Convert.ToInt32(MediaAssetId);
        //        int _IssueDateId = IssueDateId == "" ? 0 : Convert.ToInt32(IssueDateId);
        //        model.MediaAssetId = _MediaAssetId;
        //        model.IssueDateId = _IssueDateId;

        //        string draw = currentRequest["draw"];
        //        string start = currentRequest["start"];
        //        string length = currentRequest["length"];
        //        int pageSize = length != null ? Convert.ToInt32(length) : 0;
        //        int skip = start != null ? Convert.ToInt32(start) : 0;

        //        string strZoneQry = model.MediaAssetId > 0 ? "MediaAssetId == " + model.MediaAssetId : "1=1";
        //        strZoneQry += model.IssueDateId > 0 ? " && IssueDateId == " + model.IssueDateId : " && 1=1";

        //        IQueryable<MediaOrderWithProposalStatusModel1> IQMediaOrderWithProposalStatusModel;
        //        IQMediaOrderWithProposalStatusModel = (from ord in db.MediaOrders
        //                                               where ord.DeletedInd != true && ord.OrderStatus == APIMediaOrderStatus.Proposal && ord.AdIssueDate.CoverDate.Year == DateTime.Now.Year
        //                                               //where ord.DeletedInd != true && ord.OrderStatus == APIMediaOrderStatus.Proposal
        //                                               orderby ord.MediaOrderId descending
        //                                               select new MediaOrderWithProposalStatusModel1
        //                                               {
        //                                                   BuyId = ord.BuyId,
        //                                                   //MediaAssetId = ord.MediaAssetId,
        //                                                   //MediaAssetName = ord.MediaAsset.MediaAssetName,
        //                                                   //MediaOrderId = ord.MediaOrderId,
        //                                                   //IssueDateId = ord.IssueDateId,
        //                                                   //IssueDate = ord.AdIssueDate.CoverDate,
        //                                                   //AdTypeId = ord.RateCardDetail.AdTypeId,
        //                                                   //AdTypeName = ord.RateCardDetail.AdType.AdTypeName,
        //                                                   //AdcolorId = ord.RateCardDetail.AdColorId,
        //                                                   //AdcolorName = ord.RateCardDetail.AdColor.AdColorName,
        //                                                   //AdSizeId = ord.RateCardDetail.AdSizeId,
        //                                                   //AdSizeName = ord.RateCardDetail.AdSize.AdSizeName,
        //                                                   //FrequencyId = ord.RateCardDetail.FrequencyId,
        //                                                   //FrequencyName = ord.RateCardDetail.Frequency.FrequencyName,
        //                                                   AdvertiserId = ord.AdvertiserId,
        //                                                   OrderStatus = ord.OrderStatus,
        //                                                   BT_ID = ord.BT_ID,
        //                                                   ST_ID = ord.ST_ID,
        //                                                   GrossCost = ord.GrossCost,
        //                                                   NetCost = ord.NetCost,
        //                                                   //FlightStartDate = ord.FlightStartDate,
        //                                                   //FlightEndDate = ord.FlightEndDate,
        //                                               }).Where(strZoneQry);

        //        lstMediaOrderWithProposalStatusModel = IQMediaOrderWithProposalStatusModel.ToList();
        //        recordsTotal = lstMediaOrderWithProposalStatusModel.Count();
        //        lstMediaOrderWithProposalStatusModel = lstMediaOrderWithProposalStatusModel.Skip(skip).Take(pageSize).ToList();

        //        if (lstMediaOrderWithProposalStatusModel.Count == 0)
        //        {
        //            int _draw = Convert.ToInt32(draw);
        //            objResponse.draw = _draw;
        //            objResponse.recordsTotal = recordsTotal;
        //            objResponse.recordsFiltered = recordsTotal;
        //            objResponse.data = lstMediaOrderWithProposalStatusModel;
        //        }
        //        else
        //        {
        //            int _draw = Convert.ToInt32(draw);
        //            objResponse.draw = _draw;
        //            objResponse.recordsTotal = recordsTotal;
        //            objResponse.recordsFiltered = recordsTotal;
        //            objResponse.data = lstMediaOrderWithProposalStatusModel;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        var context = HttpContext.Current;
        //        var currentRequest = HttpContext.Current.Request;
        //        string draw = currentRequest["draw"];
        //        int _draw = Convert.ToInt32(draw);
        //        recordsTotal = lstMediaOrderWithProposalStatusModel.Count();
        //        objResponse.draw = _draw;
        //        objResponse.recordsTotal = recordsTotal;
        //        objResponse.recordsFiltered = recordsTotal;
        //        objResponse.data = lstMediaOrderWithProposalStatusModel;
        //        LoggerRepsitory.WriteErrorLog(ex);
        //    }
        //    return objResponse;
        //}


        public ResponseCommon GetDashboardData()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                GetDashboardModel objDashboard = new GetDashboardModel();

                // Total Net Amount Count 
                ResponseCommon totalNetAmount = GetTotalNetAmount();
                if (totalNetAmount.StatusCode == 1)
                {
                    objDashboard.TotalNetCostModel = (List<GetTotalNetCostYearlyModel>)totalNetAmount.Data;
                }
                else
                {
                    objDashboard.TotalNetCostModel = new List<GetTotalNetCostYearlyModel>();
                }

                // Total Media Asset Used Count 
                ResponseCommon mediaAssetUsedInOrder = GetMediaAssetUsedInOrder();
                if (mediaAssetUsedInOrder.StatusCode == 1)
                {
                    objDashboard.UsedMediaAssetCountModel = (List<GetUsedMediaAssetYearlyModel>)mediaAssetUsedInOrder.Data;
                }
                else
                {
                    objDashboard.UsedMediaAssetCountModel = new List<GetUsedMediaAssetYearlyModel>();
                }

                // Total Advertiser Used Count 
                ResponseCommon advertiserUsedInOrder = GetAdvertiserUsedInOrder();
                if (advertiserUsedInOrder.StatusCode == 1)
                {
                    objDashboard.UsedAdvertiserCountModel = (List<GetUsedAdvertiserYearlyModel>)advertiserUsedInOrder.Data;
                }
                else
                {
                    objDashboard.UsedAdvertiserCountModel = new List<GetUsedAdvertiserYearlyModel>();
                }

                // Inseration Order Count 
                ResponseCommon inserationOrderCount = GetInsertionOrderCount();
                if (inserationOrderCount.StatusCode == 1)
                {
                    objDashboard.InserationOrderCountModel = (List<GetInserationOrderCountYearlyModel>)inserationOrderCount.Data;
                }
                else
                {
                    objDashboard.InserationOrderCountModel = new List<GetInserationOrderCountYearlyModel>();
                }

                // Inseration Order Count Month Wise for Current Year 
                ResponseCommon responseOrderCountbyMonthlyForCurrentYear = GetOrderCountbyMonthlyForCurrentYear();
                if (responseOrderCountbyMonthlyForCurrentYear.StatusCode == 1)
                {
                    objDashboard.OrderCountAndRevenueByMonthWise = (List<GetOrderCountByMonthWise>)responseOrderCountbyMonthlyForCurrentYear.Data;
                }
                else
                {
                    objDashboard.OrderCountAndRevenueByMonthWise = new List<GetOrderCountByMonthWise>();
                }

                // Inseration Order Count Month Wise for Yearly 
                ResponseCommon response = GetOrderCountbyMonthlyForYearly();
                if (response.StatusCode == 1)
                {
                    objDashboard.OrderCountByMonthAndYear = (List<GetOrderCountByYearlyModel>)response.Data;
                }
                else
                {
                    objDashboard.OrderCountByMonthAndYear = new List<GetOrderCountByYearlyModel>();
                }

                // Status wise Orders 
                GetOrdersWithProposalStatusbyFilterOption objModel = new GetOrdersWithProposalStatusbyFilterOption();
                ResponseCommon responseCommon = GetInsertionOrderCountbyOrderStatusForCurrentYear(objModel);
                if (responseCommon.StatusCode == 1)
                {
                    objDashboard.OrderCountByStatus = (List<MediaOrdersBuyIdWiseStatusDetail>)responseCommon.Data;
                }
                else
                {
                    objDashboard.OrderCountByStatus = new List<MediaOrdersBuyIdWiseStatusDetail>();
                }



                if (objDashboard.TotalNetCostModel == null && objDashboard.UsedMediaAssetCountModel == null && objDashboard.UsedAdvertiserCountModel == null && objDashboard.InserationOrderCountModel == null
                   && objDashboard.OrderCountAndRevenueByMonthWise == null && objDashboard.OrderCountByMonthAndYear == null && objDashboard.OrderCountByStatus == null)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.Ok;
                    objResponse.Data = objDashboard;
                }
                return objResponse;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }
        #endregion
    }
}
