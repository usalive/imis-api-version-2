﻿using Asi.DAL;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net.Http;

namespace Asi.Repository
{
    public static class CommonUtility
    {
        #region Public Variables
        public static string basePath = AppDomain.CurrentDomain.BaseDirectory;
        public static string CurrentHostDomain = ConfigurationManager.AppSettings["CurrentHostedDomain"];
        #endregion

        #region Public Methods
        public static string SaveBase64ToImage(string base64String, string extension = ".jpeg")
        {
            if (string.IsNullOrEmpty(base64String) || string.IsNullOrWhiteSpace(base64String)) return null;
            string fileName = string.Empty;
            try
            {
                string ImageUploadPath = ConfigurationManager.AppSettings["MediaAssetLogoUploadPath"];
                string fullPath = basePath + ImageUploadPath;
                if (!Directory.Exists(fullPath))
                    Directory.CreateDirectory(fullPath);

                fileName = System.DateTime.Now.Ticks.ToString() + extension;

                Image img = Image.FromStream(new MemoryStream(Convert.FromBase64String(base64String)));
                img.Save(Path.Combine(fullPath, fileName), ImageFormat.Jpeg);

                fileName = ImageUploadPath + fileName;
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                return null;
            }
            return fileName;
        }

        public static string GetBase64FromImage(string imageName)
        {
            if (string.IsNullOrEmpty(imageName) || string.IsNullOrWhiteSpace(imageName)) return null;
            try
            {
                string ImageUploadPath = ConfigurationManager.AppSettings["MediaAssetLogoUploadPath"];
                Image image = Image.FromFile(Path.Combine(ImageUploadPath, imageName));
                using (MemoryStream ms = new MemoryStream())
                {
                    // Convert Image to byte[]
                    image.Save(ms, ImageFormat.Jpeg);
                    byte[] imageBytes = ms.ToArray();

                    // Convert byte[] to Base64 String
                    string base64String = Convert.ToBase64String(imageBytes);
                    return base64String;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return null;
        }

        public static string SaveBase64ToFile(string base64String, string extension = ".jpeg")
        {
            extension = (string.IsNullOrEmpty(extension)) ? ".jpeg" : extension;
            if (string.IsNullOrEmpty(base64String) || string.IsNullOrWhiteSpace(base64String)) return null;
            string fileName = string.Empty;
            try
            {
                string ImageUploadPath = ConfigurationManager.AppSettings["MediaOrderFilesUploadPath"];
                string fullPath = basePath + ImageUploadPath;
                if (!Directory.Exists(fullPath))
                    Directory.CreateDirectory(fullPath);

                fileName = System.DateTime.Now.Ticks.ToString() + extension;
                using (Stream file = File.Create(Path.Combine(fullPath, fileName)))
                {
                    CopyStream(new MemoryStream(Convert.FromBase64String(base64String)), file);
                }
                fileName = ImageUploadPath + fileName;
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                return null;
            }
            return fileName;
        }

        public static string SaveBase64ToFile_SaveWithFileName(string base64String, string fileNameToSave, string extension = ".jpeg")
        {
            extension = (string.IsNullOrEmpty(extension)) ? ".jpeg" : extension;
            if (string.IsNullOrEmpty(base64String) || string.IsNullOrWhiteSpace(base64String)) return null;
            string fileName = string.Empty;
            try
            {
                string ImageUploadPath = ConfigurationManager.AppSettings["MediaOrderFilesUploadPath"];
                string fullPath = basePath + ImageUploadPath;
                if (!Directory.Exists(fullPath))
                    Directory.CreateDirectory(fullPath);

                fileName = Format_Image_Name(fileNameToSave); //System.DateTime.Now.Ticks.ToString() + extension;
                using (Stream file = File.Create(Path.Combine(fullPath, fileName)))
                {
                    CopyStream(new MemoryStream(Convert.FromBase64String(base64String)), file);
                }
                fileName = ImageUploadPath + fileName;
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                return null;
            }
            return fileName;
        }

        public static String Format_Image_Name(String _Name)
        {
            string sFormatedName = _Name.Replace(" ", "-").Replace(":", "").Replace("'", "-").Replace("&", "").Replace("?", "").Replace("+", "");
            return sFormatedName.ToLower();
        }

        public static string SaveBase64ToFile4MediaOrderSignedDocuments(string base64String, string extension, string fileName)
        {
            extension = (string.IsNullOrEmpty(extension)) ? ".jpeg" : extension;
            if (string.IsNullOrEmpty(base64String) || string.IsNullOrWhiteSpace(base64String)) return null;
            try
            {
                string ImageUploadPath = ConfigurationManager.AppSettings["MediaOrderFilesUploadPath"];
                string fullPath = basePath + ImageUploadPath;
                if (!Directory.Exists(fullPath))
                    Directory.CreateDirectory(fullPath);

                using (Stream file = File.Create(Path.Combine(fullPath, fileName)))
                {
                    CopyStream(new MemoryStream(Convert.FromBase64String(base64String)), file);
                }
                fileName = ImageUploadPath + fileName;
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                return null;
            }
            return fileName;
        }

        /// <summary>
        /// Copies the contents of input to output. Doesn't close either stream.
        /// </summary>
        public static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[8 * 1024];
            int len;
            while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, len);
            }
        }


        //Ex.
        //using (Stream file = File.Create(HttpContext.Current.Server.MapPath("~/") + filename))
        //  {
        //      CopyStream(new MemoryStream(Convert.FromBase64String(data.streamss)), file);
        //  }
        //  return Request.CreateResponse(HttpStatusCode.OK, filename);

        //System.Drawing.Image
        //HttpContext.Current.Server

        public static void CalculateNetCost(ref double finalNetCostOfOrder, List<MediaOrderLine> currentmol)
        {
            try
            {
                foreach (var i in currentmol)
                {
                    if (i.AmountPercent == 0)
                    {
                        if (i.SurchargeDiscount == 0)
                        {
                            finalNetCostOfOrder = finalNetCostOfOrder + i.AdjustmentValue;
                        }
                        else
                        {
                            finalNetCostOfOrder = finalNetCostOfOrder - i.AdjustmentValue;
                        }
                    }
                    else
                    {
                        if (i.SurchargeDiscount == 0)
                        {
                            finalNetCostOfOrder = finalNetCostOfOrder + ((finalNetCostOfOrder * i.AdjustmentValue) / 100);
                        }
                        else
                        {
                            finalNetCostOfOrder = finalNetCostOfOrder - ((finalNetCostOfOrder * i.AdjustmentValue) / 100);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.LogError(ex);
            }
        }

        public static void GenerateFromJSON(string webapiurl, DataTable dtError1)
        {
            var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Products?$format=json";  //get products from ODATA service

            using (var clientjs = new HttpClient())
            {
                HttpResponseMessage responseJSON = clientjs.GetAsync(url).Result;
                responseJSON.EnsureSuccessStatusCode();
                var responseBody = responseJSON.Content.ReadAsStringAsync().Result;  //Get JSON from ODATA service

                var data = JsonConvert.DeserializeObject<Hashtable>(responseBody)["value"];  //use JsonConvert to deserialize raw json

                using (var client = new HttpClient())
                using (var formData = new MultipartFormDataContent())
                {
                    var fileFormat = "xlsx";
                    formData.Add(new StringContent("Test"), "FileName");
                    formData.Add(new StringContent(fileFormat), "FileFormat");
                    formData.Add(new StringContent(JsonConvert.SerializeObject(data)), "Data");
                    //Call WebAPI to get Excel
                    var response = client.PostAsync(webapiurl, formData).Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        // MessageBox.Show("Invalid response.");
                        return;
                    }
                    var tempPath = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());
                    if (!Directory.Exists(tempPath))
                    {
                        Directory.CreateDirectory(tempPath);
                    }
                    //Save Excel to Tem directory.
                    var tempFilePath = Path.Combine(tempPath, string.Format("{0}.{1}", "Test", fileFormat));

                    using (var newFile = File.Create(tempFilePath))
                    {
                        //// var bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(dtError1.ToCSV());
                        //byte[] buffer = Encoding.ASCII.GetBytes(dtError1.ToCSV());
                        //MemoryStream ms = new MemoryStream(buffer);

                        ////write to file
                        //FileStream file = new FileStream(tempFilePath, FileMode.Create, FileAccess.Write);
                        //ms.WriteTo(file);
                        //file.Close();
                        //ms.Close();

                        // DataTable dtError1 = new DataTable();

                        // System.IO.MemoryStream stream = new System.IO.MemoryStream();
                        // System.Runtime.Serialization.IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                        // formatter.Serialize(stream, dtError1); // dtUsers is a DataTable
                        // byte[] bytes = stream.GetBuffer();

                        // var bytes1 = Encoding.GetEncoding("iso-8859-1").GetBytes(dtError1.ToCSV());
                        // MemoryStream stream2 = new MemoryStream(bytes1);
                        // StreamReader reader = new StreamReader(stream2);
                        // System.IO.Stream textStream = reader.BaseStream;
                        //// textStream.read
                        // Console.WriteLine(reader.ReadToEnd());
                        // textStream.CopyTo(newFile);

                        //reader.ReadToEnd();
                        //stream.GetBuffer().CopyTo(newFile);
                        //bytes.CopyTo(bytes, 0);
                        //stream.CopyTo(newFile);
                        //  dtError1.ReadAsStreamAsync
                        //response.Content.ReadAsStreamAsync().Result.CopyTo(newFile);
                        //string tt = response.Content.ReadAsStreamAsync();
                    }

                    //Open Excel to view.
                    //Process.Start(tempFilePath);
                }
            }
        }

        /// <summary>
        /// FUNCTION FOR EXPORT TO EXCEL
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="worksheetName"></param>
        /// <param name="saveAsLocation"></param>
        /// <returns></returns>
        public static string WriteDataTableToExcel(System.Data.DataTable dataTable, string worksheetName, string saveAsLocation, string ReporType)
        {
            writeError("function initialize successfully");
            //System.Diagnostics.Debug.WriteLine("function initialize successfully");
            try
            {
                Microsoft.Office.Interop.Excel.Application excel;
                Microsoft.Office.Interop.Excel.Workbook excelworkBook;
                Microsoft.Office.Interop.Excel.Worksheet excelSheet;
                Microsoft.Office.Interop.Excel.Range excelCellrange;

                try
                {
                    System.Diagnostics.Debug.WriteLine("called successfully");

                    // Start Excel and get Application object.
                    excel = new Microsoft.Office.Interop.Excel.Application();

                    // for making Excel visible
                    excel.Visible = false;
                    excel.DisplayAlerts = false;

                    // Creation a new Workbook
                    excelworkBook = excel.Workbooks.Add(Type.Missing);

                    // Workk sheet
                    excelSheet = (Microsoft.Office.Interop.Excel.Worksheet)excelworkBook.ActiveSheet;
                    excelSheet.Name = worksheetName;


                    excelSheet.Cells[1, 1] = ReporType;
                    excelSheet.Cells[1, 2] = "Date : " + DateTime.Now.ToShortDateString();

                    // loop through each row and add values to our sheet
                    int rowcount = 2;

                    foreach (DataRow datarow in dataTable.Rows)
                    {
                        rowcount += 1;
                        for (int i = 1; i <= dataTable.Columns.Count; i++)
                        {
                            // on the first iteration we add the column headers
                            if (rowcount == 3)
                            {
                                excelSheet.Cells[2, i] = dataTable.Columns[i - 1].ColumnName;
                                excelSheet.Cells.Font.Color = System.Drawing.Color.Black;
                            }

                            excelSheet.Cells[rowcount, i] = datarow[i - 1].ToString();

                            //for alternate rows
                            if (rowcount > 3)
                            {
                                if (i == dataTable.Columns.Count)
                                {
                                    if (rowcount % 2 == 0)
                                    {
                                        excelCellrange = excelSheet.Range[excelSheet.Cells[rowcount, 1], excelSheet.Cells[rowcount, dataTable.Columns.Count]];
                                        FormattingExcelCells(excelCellrange, "#CCCCFF", System.Drawing.Color.Black, false);
                                    }

                                }
                            }

                        }
                    }

                    // now we resize the columns
                    excelCellrange = excelSheet.Range[excelSheet.Cells[1, 1], excelSheet.Cells[rowcount, dataTable.Columns.Count]];
                    excelCellrange.EntireColumn.AutoFit();
                    Microsoft.Office.Interop.Excel.Borders border = excelCellrange.Borders;
                    border.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                    border.Weight = 2d;


                    excelCellrange = excelSheet.Range[excelSheet.Cells[1, 1], excelSheet.Cells[2, dataTable.Columns.Count]];
                    FormattingExcelCells(excelCellrange, "#000099", System.Drawing.Color.White, true);


                    //now save the workbook and exit Excel
                    excelworkBook.SaveAs(saveAsLocation);
                    excelworkBook.Close();
                    excel.Quit();
                    // Process.Start(saveAsLocation);
                    return saveAsLocation;
                }
                catch (Exception ex)
                {
                    writeError(ex.Message.ToString());
                    //System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
                    return "";
                }
                finally
                {
                    System.Diagnostics.Debug.WriteLine("called successfully");
                    excelSheet = null;
                    excelCellrange = null;
                    excelworkBook = null;
                }
            }
            catch (Exception ex)
            {
                writeError(ex.Message.ToString());
                //System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
                return "";
            }

        }

        /// <summary>
        /// FUNCTION FOR FORMATTING EXCEL CELLS
        /// </summary>
        /// <param name="range"></param>
        /// <param name="HTMLcolorCode"></param>
        /// <param name="fontColor"></param>
        /// <param name="IsFontbool"></param>
        public static void FormattingExcelCells(Microsoft.Office.Interop.Excel.Range range, string HTMLcolorCode, System.Drawing.Color fontColor, bool IsFontbool)
        {
            range.Interior.Color = System.Drawing.ColorTranslator.FromHtml(HTMLcolorCode);
            range.Font.Color = System.Drawing.ColorTranslator.ToOle(fontColor);
            if (IsFontbool == true)
            {
                range.Font.Bold = IsFontbool;
            }
        }

        public static string fileLoc = @"G:\sampleError1.txt";

        public static void writeError(string Error)
        {
            FileStream fs = null;
            if (!File.Exists(fileLoc))
            {
                using (fs = File.Create(fileLoc))
                {
                    using (StreamWriter sw = new StreamWriter(fileLoc))
                    {
                        sw.Write(Error);
                    }
                }
            }
            else
            {
                using (StreamWriter sw = new StreamWriter(fileLoc))
                {
                    sw.Write(Error);
                }
            }
        }

        public static string ExportToExcel(System.Data.DataTable dt, string saveAsLocation)
        {
            try
            {
                System.Web.UI.WebControls.GridView GridView1 = new System.Web.UI.WebControls.GridView();
                GridView1.DataSource = dt;
                GridView1.DataBind();
                StringWriter s_Write = new StringWriter();
                System.Web.UI.HtmlTextWriter h_write = new System.Web.UI.HtmlTextWriter(s_Write);
                GridView1.ShowHeader = true;
                GridView1.RenderControl(h_write);
                string renderedGridView = s_Write.ToString();
                System.IO.File.WriteAllText(saveAsLocation, renderedGridView);

                //string xlsxExcelPath = saveAsLocation.Replace(".xls", ".xlsx");
                //System.IO.File.WriteAllText(xlsxExcelPath, renderedGridView);

                if (File.Exists(saveAsLocation))
                {
                    //CommonUtility.writeError(saveAsLocation);
                    //Process.Start(saveAsLocation);
                }
                else
                {
                    string tt = "file not found";
                    //CommonUtility.writeError(tt);
                }
            }
            catch (Exception ex)
            {
                //CommonUtility.writeError(ex.Message.ToString());
            }
            return saveAsLocation;
        }


        #endregion
    }
}
