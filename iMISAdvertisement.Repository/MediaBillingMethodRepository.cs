﻿using Asi.DAL;
using Asi.DAL.Model;
//using Asi.DAL.Model.StaticList;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Asi.Repository
{
    public class MediaBillingMethodRepository : IMediaBillingMethodRepository
    {
        private readonly IDBEntities DB;
        public MediaBillingMethodRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        #region Public Methods
        public ResponseCommon GetMediaBillingMethods()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<MediaBillingMethodModel> lstMediaBillingMethod = new List<MediaBillingMethodModel>();
                //lstMediaBillingMethod = DB.MediaBillingMethods/*.Where(x => x.DeletedInd != true)*/
                //    .Select(x => new MediaBillingMethodModel
                //    {
                //        MediaBillingMethodId = x.MediaBillingMethodId,
                //        Name = x.MediaBillingMethodName
                //    })
                //    .OrderBy(x => x.Name)
                //    .ToList();

                //* 2020-11-10 Change : Get Hardcoded list
                //lstMediaBillingMethod = Get_MediaBillingMethodName_Values.GetMediaBillingMethodNameValues()
                //                                                         .Select(x => new MediaBillingMethodModel
                //                                                         {
                //                                                             MediaBillingMethodId = x.MediaBillingMethodId,
                //                                                             Name = x.MediaBillingMethodName
                //                                                         }).OrderBy(x => x.Name).ToList();

                //* 2020-11-25 Change : Get Hardcoded list From Enum
                lstMediaBillingMethod = Enum.GetValues(typeof(BillingMethod)).Cast<BillingMethod>()
                    .OrderBy(x => x.ToString()).Select(x => new MediaBillingMethodModel
                    {
                        MediaBillingMethodId = (int)x,
                        Name = x.ToString().Replace("_", " ")
                    }).OrderBy(x => x.Name).ToList();

                if (lstMediaBillingMethod.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }

                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstMediaBillingMethod;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetMediaBillingMethodById(int mediaBillingMethodId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                MediaBillingMethodModel objMediaBillingMethod = null;
                //objMediaBillingMethod = DB.MediaBillingMethods.Where(x => /*x.DeletedInd != true &&*/ x.MediaBillingMethodId == mediaBillingMethodId)
                //                        .Select(x => new MediaBillingMethodModel
                //                        {
                //                            MediaBillingMethodId = x.MediaBillingMethodId,
                //                            Name = x.MediaBillingMethodName
                //                        }).FirstOrDefault();

                //* 2020-11-10 Change : Filter From HardCoded List
                //objMediaBillingMethod = Get_MediaBillingMethodName_Values.GetMediaBillingMethodNameValues()
                //                                                        .Where(x => x.MediaBillingMethodId == mediaBillingMethodId)
                //                                                        .Select(x => new MediaBillingMethodModel
                //                                                        {
                //                                                            MediaBillingMethodId = x.MediaBillingMethodId,
                //                                                            Name = x.MediaBillingMethodName
                //                                                        }).FirstOrDefault();

                //* 2020-11-10 Change : Filter From Enum List
                objMediaBillingMethod = Enum.GetValues(typeof(BillingMethod)).Cast<BillingMethod>()
                                                                        .Where(x => (int)x == mediaBillingMethodId)
                                                                        .Select(x => new MediaBillingMethodModel
                                                                        {
                                                                            MediaBillingMethodId = (int)x,
                                                                            Name = x.ToString().Replace("_", " ")
                                                                        }).FirstOrDefault();

                if (objMediaBillingMethod == null)
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                    return objResponse;
                }
                objResponse.Data = objMediaBillingMethod;
                objResponse.StatusCode = ApiStatus.Ok;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        //* 2020-11-10 Change : Comment this method. because now we get list from hardcoded method.
        //public ResponseCommon PostMediaBillingMethod(MediaBillingMethodModel mediaBillingMethod)
        //{
        //    ResponseCommon objResponse = new ResponseCommon();
        //    try
        //    {
        //        int isRecordExist = DB.MediaBillingMethods.Where(x => x.MediaBillingMethodName.ToLower() == mediaBillingMethod.Name.ToLower()
        //                                                /*&& x.DeletedInd == false*/).Count();
        //        if (isRecordExist <= 0)
        //        {
        //            MediaBillingMethod objMediaBillingMethodAdd = new MediaBillingMethod();
        //            objMediaBillingMethodAdd.MediaBillingMethodName = mediaBillingMethod.Name;
        //            objMediaBillingMethodAdd.CreatedOn = DateTime.Now;
        //            //objMediaBillingMethodAdd.DeletedInd = false;

        //            //*
        //            objMediaBillingMethodAdd.UpdatedOn = DateTime.Now;
        //            objMediaBillingMethodAdd.CreatedByUserKey = Constants.Created_Update_ByUserKey;
        //            objMediaBillingMethodAdd.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

        //            DB.MediaBillingMethods.Add(objMediaBillingMethodAdd);
        //            DB.SaveChanges();

        //            objResponse.Data = objMediaBillingMethodAdd.MediaBillingMethodId;
        //            objResponse.StatusCode = ApiStatus.Ok;

        //            return objResponse;
        //        }
        //        else
        //        {
        //            objResponse.StatusCode = ApiStatus.AlreadyExist;
        //            objResponse.Message = ApiStatus.AlreadyExistCustomMessge("Media Billing Method Name");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objResponse.StatusCode = ApiStatus.Exception;
        //        objResponse.Message = ApiStatus.ExceptionMessge;
        //        LoggerRepsitory.WriteErrorLog(ex);
        //    }
        //    return objResponse;
        //}

        //public ResponseCommon PutMediaBillingMethod(MediaBillingMethodModel mediaBillingMethod)
        //{
        //    ResponseCommon objResponse = new ResponseCommon();
        //    try
        //    {
        //        MediaBillingMethod objMediaBillingMethodEdit = null;
        //        int isNameExist = DB.MediaBillingMethods
        //                            .Where(x => x.MediaBillingMethodName.ToLower() == mediaBillingMethod.Name.ToLower()
        //                                    && x.MediaBillingMethodId != mediaBillingMethod.MediaBillingMethodId
        //                                    /*&& x.DeletedInd == false*/).Count();
        //        if (isNameExist > 0)
        //        {
        //            objResponse.StatusCode = ApiStatus.AlreadyExist;
        //            objResponse.Message = ApiStatus.AlreadyExistCustomMessge("Media Billing Method Name");
        //            return objResponse;
        //        }

        //        objMediaBillingMethodEdit = DB.MediaBillingMethods.Where(x => /*x.DeletedInd != true &&*/
        //        x.MediaBillingMethodId == mediaBillingMethod.MediaBillingMethodId).FirstOrDefault();
        //        if (objMediaBillingMethodEdit != null)
        //        {
        //            objMediaBillingMethodEdit.MediaBillingMethodName = mediaBillingMethod.Name;
        //            objMediaBillingMethodEdit.UpdatedOn = DateTime.Now;

        //            //*
        //            objMediaBillingMethodEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

        //            DB.Entry(objMediaBillingMethodEdit).State = EntityState.Modified;
        //            DB.SaveChanges();

        //            objResponse.Data = objMediaBillingMethodEdit.MediaBillingMethodId;
        //            objResponse.StatusCode = ApiStatus.Ok;
        //        }
        //        else
        //        {
        //            objResponse.StatusCode = ApiStatus.NotFound;
        //            objResponse.Message = ApiStatus.NotFoundMessage;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objResponse.StatusCode = ApiStatus.Exception;
        //        objResponse.Message = ApiStatus.ExceptionMessge;
        //        LoggerRepsitory.WriteErrorLog(ex);
        //    }
        //    return objResponse;
        //}

        //public ResponseCommon DeleteMediaBillingMethod(int mediaBillingMethodId)
        //{
        //    ResponseCommon objResponse = new ResponseCommon();
        //    try
        //    {
        //        MediaBillingMethod objMediaBillingMethodEdit = null;
        //        int status = 0;
        //        objMediaBillingMethodEdit = DB.MediaBillingMethods.Where(x => /*x.DeletedInd != true &&*/ x.MediaBillingMethodId == mediaBillingMethodId).FirstOrDefault();
        //        if (objMediaBillingMethodEdit != null)
        //        {
        //            //objMediaBillingMethodEdit.DeletedInd = true;

        //            //*
        //            objMediaBillingMethodEdit.UpdatedOn = DateTime.Now;
        //            objMediaBillingMethodEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

        //            DB.Entry(objMediaBillingMethodEdit).State = EntityState.Modified;
        //            status = DB.SaveChanges();
        //            objResponse.StatusCode = ApiStatus.Ok;
        //        }
        //        else
        //        {
        //            objResponse.StatusCode = ApiStatus.NotFound;
        //            objResponse.Message = ApiStatus.NotFoundMessage;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objResponse.StatusCode = ApiStatus.Exception;
        //        objResponse.Message = ApiStatus.ExceptionMessge;
        //        LoggerRepsitory.WriteErrorLog(ex);
        //    }
        //    return objResponse;
        //}
        #endregion
    }
}
