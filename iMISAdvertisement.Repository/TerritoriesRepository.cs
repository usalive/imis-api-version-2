﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Asi.Repository
{
    public class Territoriesrepository : ITerritoriesRepository
    {
        private readonly IDBEntities DB;
        public Territoriesrepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        #region Public Methods
        public ResponseCommon GetTerritories()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<TerritoryModel> lstTerritory = new List<TerritoryModel>();
                lstTerritory = DB.Territories
                                //.Where(x => x.DeletedInd != true)
                                .Select(x => new TerritoryModel
                                {
                                    TerritoryId = x.TerritoryId,
                                    Name = x.TerritoryName
                                })
                                .OrderBy(x => x.Name).ToList();
                if (lstTerritory.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }
                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstTerritory;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetTerritoryById(int territoryId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                TerritoryModel objTerritory = null;
                objTerritory = DB.Territories.Where(x => /*x.DeletedInd != true && */
                                                        x.TerritoryId == territoryId)
                                .Select(x => new TerritoryModel
                                {
                                    TerritoryId = x.TerritoryId,
                                    Name = x.TerritoryName
                                })
                                .FirstOrDefault();
                if (objTerritory == null)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                }
                else
                {
                    objResponse.Data = objTerritory;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon PostTerritory(TerritoryModel territoryModel)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                int IsTerritoryExist = DB.Territories.Where(x => /*x.DeletedInd == false &&*/
                            x.TerritoryName.ToLower() == territoryModel.Name.Trim().ToLower()).Count();
                if (IsTerritoryExist <= 0)
                {
                    Territory objrep = new Territory();
                    objrep.TerritoryName = territoryModel.Name.Trim();
                    objrep.CreatedOn = DateTime.Now;
                    //objrep.DeletedInd = false;

                    //*
                    objrep.UpdatedOn = DateTime.Now;
                    objrep.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                    objrep.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Territories.Add(objrep);
                    DB.SaveChanges();

                    objResponse.Data = objrep.TerritoryId;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.AlreadyExist;
                    objResponse.Message = ApiStatus.AlreadyExistCustomMessge("Territory Name");
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon PutTerritory(TerritoryModel territoryModel)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                int isNameExist = DB.Territories
                                    .Where(x => x.TerritoryName.ToLower() == territoryModel.Name.ToLower()
                                            && x.TerritoryId != territoryModel.TerritoryId
                                            /*&& x.DeletedInd == false*/).Count();
                if (isNameExist > 0)
                {
                    objResponse.StatusCode = ApiStatus.AlreadyExist;
                    objResponse.Message = ApiStatus.AlreadyExistCustomMessge("Territory Name");
                    return objResponse;
                }

                Territory objUpdateTerritory = DB.Territories.Where(x => /*x.DeletedInd != true &&*/
                                                x.TerritoryId == territoryModel.TerritoryId).FirstOrDefault();
                if (objUpdateTerritory != null)
                {
                    objUpdateTerritory.TerritoryName = territoryModel.Name;
                    objUpdateTerritory.UpdatedOn = DateTime.Now;

                    //*
                    objUpdateTerritory.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objUpdateTerritory).State = EntityState.Modified;
                    DB.SaveChanges();

                    objResponse.Data = territoryModel.TerritoryId;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.Data = objUpdateTerritory.TerritoryId;
                    objResponse.StatusCode = ApiStatus.NotFound;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon DeleteTerritory(int terittoryId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                Territory objTerritory = DB.Territories.Where(x => /*x.DeletedInd != true &&*/ x.TerritoryId == terittoryId).FirstOrDefault();
                if (objTerritory != null)
                {
                    //objTerritory.DeletedInd = true;

                    //*
                    objTerritory.UpdatedOn = DateTime.Now;
                    objTerritory.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objTerritory).State = EntityState.Modified;
                    int status = DB.SaveChanges();
                    objResponse.Data = status <= 0 ? 0 : 1;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }
        #endregion
    }
}
