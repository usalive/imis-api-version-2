﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Asi.Repository
{
    public class SeparationRepository : ISeparationRepository
    {
        private readonly IDBEntities DB;
        public SeparationRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        #region Public Methods
        public ResponseCommon GetSeparations()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<SeparationModel> lstSeparation = new List<SeparationModel>();
                lstSeparation = DB.Separations
                    //.Where(x => x.DeletedInd != true)
                    .Select(x => new SeparationModel
                    {
                        SeparationId = x.SeparationId,
                        Name = x.SeparationName
                    })
                    .OrderBy(x => x.Name)
                    .ToList();

                if (lstSeparation.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }

                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstSeparation;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetSeparationById(int separationId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                SeparationModel objSeparation = null;
                objSeparation = DB.Separations.Where(x => /*x.DeletedInd != true && */ x.SeparationId == separationId)
                                        .Select(x => new SeparationModel
                                        {
                                            SeparationId = x.SeparationId,
                                            Name = x.SeparationName
                                        }).FirstOrDefault();
                if (objSeparation == null)
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                    return objResponse;
                }
                objResponse.Data = objSeparation;
                objResponse.StatusCode = ApiStatus.Ok;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon PostSeparation(SeparationModel separationModel)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                int isRecordExist = DB.Separations.Where(x => x.SeparationName.ToLower() == separationModel.Name.ToLower()
                                                        /*&& x.DeletedInd == false*/).Count();
                if (isRecordExist <= 0)
                {
                    Separation objSeparationAdd = new Separation();
                    objSeparationAdd.SeparationName = separationModel.Name;
                    objSeparationAdd.CreatedOn = DateTime.Now;
                    //objSeparationAdd.DeletedInd = false;

                    //*
                    objSeparationAdd.UpdatedOn = DateTime.Now;
                    objSeparationAdd.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                    objSeparationAdd.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Separations.Add(objSeparationAdd);
                    DB.SaveChanges();

                    objResponse.Data = objSeparationAdd.SeparationId;
                    objResponse.StatusCode = ApiStatus.Ok;

                    return objResponse;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.AlreadyExist;
                    objResponse.Message = ApiStatus.AlreadyExistCustomMessge("Separation Name");
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon PutSeparation(SeparationModel separationModel)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                Separation objSeparationEdit = null;
                int isNameExist = DB.Separations
                                    .Where(x => x.SeparationName.ToLower() == separationModel.Name.ToLower()
                                            && x.SeparationId != separationModel.SeparationId
                                            /*&& x.DeletedInd == false*/).Count();
                if (isNameExist > 0)
                {
                    objResponse.StatusCode = ApiStatus.AlreadyExist;
                    objResponse.Message = ApiStatus.AlreadyExistCustomMessge("Separation Name");
                    return objResponse;
                }

                objSeparationEdit = DB.Separations.Where(x => /*x.DeletedInd != true &&*/ x.SeparationId == separationModel.SeparationId).FirstOrDefault();
                if (objSeparationEdit != null)
                {
                    objSeparationEdit.SeparationName = separationModel.Name;
                    objSeparationEdit.UpdatedOn = DateTime.Now;

                    //*
                    objSeparationEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objSeparationEdit).State = EntityState.Modified;
                    DB.SaveChanges();

                    objResponse.Data = objSeparationEdit.SeparationId;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon DeleteSeparation(int separationId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                Separation objSeparationEdit = null;
                int status = 0;
                objSeparationEdit = DB.Separations.Where(x => /* x.DeletedInd != true &&*/ x.SeparationId == separationId).FirstOrDefault();
                if (objSeparationEdit != null)
                {
                    //objSeparationEdit.DeletedInd = true;

                    //*
                    objSeparationEdit.UpdatedOn = DateTime.Now;
                    objSeparationEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objSeparationEdit).State = EntityState.Modified;
                    status = DB.SaveChanges();
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }
        #endregion
    }
}
