﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace Asi.Repository
{
    public class MediaOrderProductionDetailsRepository : IMediaOrderProductionDetailsRepository
    {
        private readonly IDBEntities DB;

        public MediaOrderProductionDetailsRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        public ResponseCommon AddUpdateMediaOrderProductionDetail(PostMediaOrderProductionDetailModel model)
        {
            ResponseCommon response = new ResponseCommon();
            List<MediaOrderProductionDetail> lstProductionDetail = new List<MediaOrderProductionDetail>();
            MediaOrderProductionDetail productionDetail = new MediaOrderProductionDetail();
            MediaOrderProductionDetail orderProductionDetail = new MediaOrderProductionDetail();
            try
            {
                List<MediaOrderProductionDetail> lstresponse = new List<MediaOrderProductionDetail>();
                foreach (var mediaOrderId in model.MediaOrderIds)
                {
                    productionDetail = DB.MediaOrderProductionDetails.Where(x => x.MediaOrderId == mediaOrderId /*&& x.DeletedInd != true*/).FirstOrDefault();
                    string originamFileName = null, proofFile = null, finalFile = null;
                    if (!string.IsNullOrEmpty(model.OriginalFile))
                        originamFileName = CommonUtility.SaveBase64ToFile(model.OriginalFile, model.OriginalFileExtension);
                    if (!string.IsNullOrEmpty(model.ProofFile))
                        proofFile = CommonUtility.SaveBase64ToFile(model.ProofFile, model.ProofFileExtension);
                    if (!string.IsNullOrEmpty(model.FinalFile))
                        finalFile = CommonUtility.SaveBase64ToFile(model.FinalFile, model.FinalFileExtension);

                    if (productionDetail != null)
                    {
                        productionDetail.NewPickupInd = model.NewPickupInd;
                        productionDetail.PickupMediaOrderId = model.PickupMediaOrderId;
                        productionDetail.CreateExpectedInd = model.CreateExpectedInd;
                        productionDetail.MaterialExpectedDate = model.MaterialExpectedDate;
                        productionDetail.TrackingNumber = model.TrackingNumber;
                        productionDetail.ChangesInd = model.ChangesInd;
                        productionDetail.OnHandInd = model.OnHandInd;
                        productionDetail.AdvertiserAgencyInd = model.AdvertiserAgencyInd;
                        productionDetail.ProductionStatusId = model.ProductionStatusId;
                        productionDetail.MaterialContactId = model.MaterialContactId;
                        productionDetail.OriginalFile = string.IsNullOrEmpty(originamFileName) ? productionDetail.OriginalFile : originamFileName;
                        productionDetail.ProofFile = string.IsNullOrEmpty(proofFile) ? productionDetail.ProofFile : proofFile;
                        productionDetail.FinalFile = string.IsNullOrEmpty(finalFile) ? productionDetail.FinalFile : finalFile;
                        productionDetail.WebAdUrl = model.WebAdUrl;
                        productionDetail.TearSheets = model.TearSheets;
                        productionDetail.HeadLine = model.HeadLine;
                        productionDetail.PositionId = model.PositionId;
                        productionDetail.SeparationId = model.SeparationId;
                        productionDetail.PageNumber = model.PageNumber;
                        productionDetail.ProductionComment = model.ProductionComment;
                        productionDetail.ProductionComment = model.ProductionComment;
                        productionDetail.Completed = false;

                        productionDetail.UpdatedOn = DateTime.Now;

                        //*
                        productionDetail.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                        DB.Entry(productionDetail).State = EntityState.Modified;

                        lstresponse.Add(productionDetail);
                    }
                    else
                    {
                        //MediaOrderProductionDetail _detail = new MediaOrderProductionDetail();
                        orderProductionDetail.MediaOrderId = mediaOrderId;
                        orderProductionDetail.NewPickupInd = model.NewPickupInd;
                        orderProductionDetail.PickupMediaOrderId = model.PickupMediaOrderId;
                        orderProductionDetail.CreateExpectedInd = model.CreateExpectedInd;
                        orderProductionDetail.MaterialExpectedDate = model.MaterialExpectedDate;
                        orderProductionDetail.TrackingNumber = model.TrackingNumber;
                        orderProductionDetail.ChangesInd = model.ChangesInd;
                        orderProductionDetail.OnHandInd = model.OnHandInd;
                        orderProductionDetail.AdvertiserAgencyInd = model.AdvertiserAgencyInd;
                        orderProductionDetail.ProductionStatusId = model.ProductionStatusId;
                        orderProductionDetail.MaterialContactId = model.MaterialContactId;
                        orderProductionDetail.OriginalFile = string.IsNullOrEmpty(originamFileName) ? null : originamFileName;
                        orderProductionDetail.ProofFile = string.IsNullOrEmpty(proofFile) ? null : proofFile;
                        orderProductionDetail.FinalFile = string.IsNullOrEmpty(finalFile) ? null : finalFile;
                        orderProductionDetail.WebAdUrl = model.WebAdUrl;
                        orderProductionDetail.TearSheets = model.TearSheets;
                        orderProductionDetail.HeadLine = model.HeadLine;
                        orderProductionDetail.PositionId = model.PositionId;
                        orderProductionDetail.SeparationId = model.SeparationId;
                        orderProductionDetail.PageNumber = model.PageNumber;
                        orderProductionDetail.ProductionComment = model.ProductionComment;
                        orderProductionDetail.CreatedOn = DateTime.Now;
                        orderProductionDetail.Completed = model.Completed;

                        //*
                        orderProductionDetail.UpdatedOn = DateTime.Now;
                        orderProductionDetail.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                        orderProductionDetail.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                        lstProductionDetail.Add(orderProductionDetail);
                    }
                }
                if (lstProductionDetail.Count > 0)
                    DB.MediaOrderProductionDetails.AddRange(lstProductionDetail);
                DB.SaveChanges();

                if (lstProductionDetail.Count > 0)
                    lstresponse.AddRange(lstProductionDetail);

                response.Data = lstresponse.Select(x => new MediaOrderProductionDetailModel
                {
                    MediaOrderProductionDetailId = x.MediaOrderProductionDetailId,
                    CreateExpectedInd = x.CreateExpectedInd,
                    FinalFile = CommonUtility.CurrentHostDomain + x.FinalFile,
                    HeadLine = x.HeadLine,
                    MaterialContactId = x.MaterialContactId,
                    MaterialExpectedDate = x.MaterialExpectedDate,
                    MediaOrderId = x.MediaOrderId,
                    NewPickupInd = x.NewPickupInd,
                    ChangesInd = x.ChangesInd,
                    OnHandInd = x.OnHandInd,
                    AdvertiserAgencyInd = x.AdvertiserAgencyInd,
                    ProductionStatusId = x.ProductionStatusId,
                    OriginalFile = CommonUtility.CurrentHostDomain + x.OriginalFile,
                    PageNumber = x.PageNumber,
                    PickupMediaOrderId = x.PickupMediaOrderId,
                    PositionId = x.PositionId,
                    ProductionComment = x.ProductionComment,
                    ProofFile = CommonUtility.CurrentHostDomain + x.ProofFile,
                    SeparationId = x.SeparationId,
                    TearSheets = x.TearSheets,
                    TrackingNumber = x.TrackingNumber,
                    WebAdUrl = x.WebAdUrl,
                    Completed = x.Completed
                }).ToList();
                response.StatusCode = ApiStatus.Ok;
            }
            catch (DbUpdateException ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;

                var entries = ex.Entries;
                foreach (var entry in entries)
                {
                    //change state to remove it from context 
                    switch (entry.State)
                    {
                        case EntityState.Added:
                            entry.State = EntityState.Detached;
                            break;
                        case EntityState.Modified:
                            entry.CurrentValues.SetValues(entry.OriginalValues);
                            entry.State = EntityState.Unchanged;
                            break;
                        case EntityState.Deleted:
                            entry.State = EntityState.Unchanged;
                            break;
                    }
                    // entry.State = System.Data.Entity.EntityState.Detached;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;

                if (orderProductionDetail != null)
                {
                    foreach (MediaOrderProductionDetail obj in lstProductionDetail)
                    {
                        DB.Entry(obj).State = EntityState.Detached;
                    }
                }
            }
            return response;
        }

        public ResponseCommon AddImportUpdateMediaOrderProductionDetail(PostImportMediaOrderProductionDetailModel model)
        {
            ResponseCommon response = new ResponseCommon();
            List<MediaOrderProductionDetail> lstProductionDetail = new List<MediaOrderProductionDetail>();
            MediaOrderProductionDetail productionDetail = new MediaOrderProductionDetail();
            MediaOrderProductionDetail orderProductionDetail = new MediaOrderProductionDetail();
            try
            {
                List<MediaOrderProductionDetail> lstresponse = new List<MediaOrderProductionDetail>();
                foreach (var mediaOrderId in model.MediaOrderIds)
                {
                    productionDetail = DB.MediaOrderProductionDetails.Where(x => x.MediaOrderId == mediaOrderId /*&& x.DeletedInd != true*/).FirstOrDefault();
                    string originamFileName = null, proofFile = null, finalFile = null;
                    if (!string.IsNullOrEmpty(model.OriginalFile))
                        originamFileName = CommonUtility.SaveBase64ToFile(model.OriginalFile, model.OriginalFileExtension);
                    if (!string.IsNullOrEmpty(model.ProofFile))
                        proofFile = CommonUtility.SaveBase64ToFile(model.ProofFile, model.ProofFileExtension);
                    if (!string.IsNullOrEmpty(model.FinalFile))
                        finalFile = CommonUtility.SaveBase64ToFile(model.FinalFile, model.FinalFileExtension);

                    if (productionDetail != null)
                    {
                        productionDetail.NewPickupInd = model.NewPickupInd;
                        productionDetail.PickupMediaOrderId = model.PickupMediaOrderId;
                        productionDetail.CreateExpectedInd = model.CreateExpectedInd;
                        productionDetail.MaterialExpectedDate = model.MaterialExpectedDate;
                        productionDetail.TrackingNumber = model.TrackingNumber;
                        productionDetail.ChangesInd = model.ChangesInd;
                        productionDetail.OnHandInd = model.OnHandInd;
                        productionDetail.AdvertiserAgencyInd = model.AdvertiserAgencyInd;
                        productionDetail.ProductionStatusId = model.ProductionStatusId;
                        productionDetail.MaterialContactId = model.MaterialContactId;
                        productionDetail.OriginalFile = string.IsNullOrEmpty(originamFileName) ? productionDetail.OriginalFile : originamFileName;
                        productionDetail.ProofFile = string.IsNullOrEmpty(proofFile) ? productionDetail.ProofFile : proofFile;
                        productionDetail.FinalFile = string.IsNullOrEmpty(finalFile) ? productionDetail.FinalFile : finalFile;
                        productionDetail.WebAdUrl = model.WebAdUrl;
                        productionDetail.TearSheets = model.TearSheets;
                        productionDetail.HeadLine = model.HeadLine;
                        productionDetail.PositionId = model.PositionId;
                        productionDetail.SeparationId = model.SeparationId;
                        productionDetail.PageNumber = model.PageNumber;
                        productionDetail.ProductionComment = model.ProductionComment;
                        productionDetail.ProductionComment = model.ProductionComment;
                        productionDetail.Completed = false;

                        productionDetail.UpdatedOn = DateTime.Now;

                        //*
                        productionDetail.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                        DB.Entry(productionDetail).State = EntityState.Modified;

                        lstresponse.Add(productionDetail);
                    }
                    else
                    {
                        //MediaOrderProductionDetail _detail = new MediaOrderProductionDetail();
                        orderProductionDetail.MediaOrderId = mediaOrderId;
                        orderProductionDetail.NewPickupInd = model.NewPickupInd;
                        orderProductionDetail.PickupMediaOrderId = model.PickupMediaOrderId;
                        orderProductionDetail.CreateExpectedInd = model.CreateExpectedInd;
                        orderProductionDetail.MaterialExpectedDate = model.MaterialExpectedDate;
                        orderProductionDetail.TrackingNumber = model.TrackingNumber;
                        orderProductionDetail.ChangesInd = model.ChangesInd;
                        orderProductionDetail.OnHandInd = model.OnHandInd;
                        orderProductionDetail.AdvertiserAgencyInd = model.AdvertiserAgencyInd;
                        orderProductionDetail.ProductionStatusId = model.ProductionStatusId;
                        orderProductionDetail.MaterialContactId = model.MaterialContactId;
                        orderProductionDetail.OriginalFile = string.IsNullOrEmpty(originamFileName) ? null : originamFileName;
                        orderProductionDetail.ProofFile = string.IsNullOrEmpty(proofFile) ? null : proofFile;
                        orderProductionDetail.FinalFile = string.IsNullOrEmpty(finalFile) ? null : finalFile;
                        orderProductionDetail.WebAdUrl = model.WebAdUrl;
                        orderProductionDetail.TearSheets = model.TearSheets;
                        orderProductionDetail.HeadLine = model.HeadLine;
                        orderProductionDetail.PositionId = model.PositionId;
                        orderProductionDetail.SeparationId = model.SeparationId;
                        orderProductionDetail.PageNumber = model.PageNumber;
                        orderProductionDetail.ProductionComment = model.ProductionComment;
                        orderProductionDetail.CreatedOn = model.CreatedDate;
                        //orderProductionDetail.CreatedByUserKey = Guid.Parse(model.CreatedBy);                        
                        orderProductionDetail.Completed = model.Completed;

                        //*
                        orderProductionDetail.UpdatedOn = model.CreatedDate;
                        orderProductionDetail.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                        orderProductionDetail.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                        lstProductionDetail.Add(orderProductionDetail);
                    }
                }
                if (lstProductionDetail.Count > 0)
                    DB.MediaOrderProductionDetails.AddRange(lstProductionDetail);
                DB.SaveChanges();

                if (lstProductionDetail.Count > 0)
                    lstresponse.AddRange(lstProductionDetail);


                response.Data = lstresponse.Select(x => new MediaOrderProductionDetailModel
                {
                    MediaOrderProductionDetailId = x.MediaOrderProductionDetailId,
                    CreateExpectedInd = x.CreateExpectedInd,
                    FinalFile = CommonUtility.CurrentHostDomain + x.FinalFile,
                    HeadLine = x.HeadLine,
                    MaterialContactId = x.MaterialContactId,
                    MaterialExpectedDate = x.MaterialExpectedDate,
                    MediaOrderId = x.MediaOrderId,
                    NewPickupInd = x.NewPickupInd,
                    ChangesInd = x.ChangesInd,
                    OnHandInd = x.OnHandInd,
                    AdvertiserAgencyInd = x.AdvertiserAgencyInd,
                    ProductionStatusId = x.ProductionStatusId,
                    OriginalFile = CommonUtility.CurrentHostDomain + x.OriginalFile,
                    PageNumber = x.PageNumber,
                    PickupMediaOrderId = x.PickupMediaOrderId,
                    PositionId = x.PositionId,
                    ProductionComment = x.ProductionComment,
                    ProofFile = CommonUtility.CurrentHostDomain + x.ProofFile,
                    SeparationId = x.SeparationId,
                    TearSheets = x.TearSheets,
                    TrackingNumber = x.TrackingNumber,
                    WebAdUrl = x.WebAdUrl,
                    Completed = x.Completed
                }).ToList();
                response.StatusCode = ApiStatus.Ok;
            }
            catch (DbUpdateException ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;

                var entries = ex.Entries;
                foreach (var entry in entries)
                {
                    //change state to remove it from context 
                    switch (entry.State)
                    {
                        case EntityState.Added:
                            entry.State = EntityState.Detached;
                            break;
                        case EntityState.Modified:
                            entry.CurrentValues.SetValues(entry.OriginalValues);
                            entry.State = EntityState.Unchanged;
                            break;
                        case EntityState.Deleted:
                            entry.State = EntityState.Unchanged;
                            break;
                    }
                    // entry.State = System.Data.Entity.EntityState.Detached;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;

                if (orderProductionDetail != null)
                {
                    foreach (MediaOrderProductionDetail obj in lstProductionDetail)
                    {
                        DB.Entry(obj).State = EntityState.Detached;
                    }
                }
            }
            return response;
        }


        public ResponseCommon DeleteMediaOrderProductionDetailByMediaOrderId(int mediaOrderId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                MediaOrderProductionDetail detail = DB.MediaOrderProductionDetails.Where(x => x.MediaOrderId == mediaOrderId /*&& x.DeletedInd != true*/).FirstOrDefault();
                if (detail != null)
                {
                    //detail.DeletedInd = true;
                    detail.UpdatedOn = DateTime.Now;

                    //*
                    detail.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(detail).State = EntityState.Modified;
                    DB.SaveChanges();

                    response.StatusCode = ApiStatus.Ok;
                    response.Data = null;
                }
                else
                {
                    response.StatusCode = ApiStatus.NotFound;
                    response.Message = ApiStatus.NotFoundMessage;
                }

            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }

        public ResponseCommon GetMediaOrderProductionDetailByMediaOrderId(int mediaOrderId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                string currentDomain = CommonUtility.CurrentHostDomain;
                MediaOrderProductionDetailModel detail = DB.MediaOrderProductionDetails.Where(x => x.MediaOrderId == mediaOrderId /*&& x.DeletedInd != true*/)
                                                        .Select(x => new MediaOrderProductionDetailModel
                                                        {
                                                            MediaOrderProductionDetailId = x.MediaOrderProductionDetailId,
                                                            CreateExpectedInd = x.CreateExpectedInd,
                                                            FinalFile = x.FinalFile == null ? null : currentDomain + x.FinalFile,
                                                            HeadLine = x.HeadLine,
                                                            MaterialContactId = x.MaterialContactId,
                                                            MaterialExpectedDate = x.MaterialExpectedDate,
                                                            MediaOrderId = x.MediaOrderId,
                                                            NewPickupInd = x.NewPickupInd,
                                                            ChangesInd = x.ChangesInd,
                                                            OnHandInd = x.OnHandInd,
                                                            AdvertiserAgencyInd = x.AdvertiserAgencyInd,
                                                            ProductionStatusId = x.ProductionStatusId,
                                                            OriginalFile = x.OriginalFile == null ? null : currentDomain + x.OriginalFile,
                                                            PageNumber = x.PageNumber,
                                                            PickupMediaOrderId = x.PickupMediaOrderId,
                                                            PositionId = x.PositionId,
                                                            ProductionComment = x.ProductionComment,
                                                            ProofFile = x.ProofFile == null ? null : currentDomain + x.ProofFile,
                                                            SeparationId = x.SeparationId,
                                                            TearSheets = x.TearSheets,
                                                            TrackingNumber = x.TrackingNumber,
                                                            WebAdUrl = x.WebAdUrl,
                                                            Completed = x.Completed
                                                        }).FirstOrDefault();
                if (detail != null)
                {
                    response.StatusCode = ApiStatus.Ok;
                    response.Data = detail;
                }
                else
                {
                    response.StatusCode = ApiStatus.NotFound;
                    response.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }

        public ResponseCommon UpdateMediaOrderProductionStatusComplete(int mediaOrderId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                MediaOrderProductionDetail detail = DB.MediaOrderProductionDetails.Where(x => x.MediaOrderId == mediaOrderId /*&& x.DeletedInd != true*/).FirstOrDefault();
                if (detail != null)
                {
                    detail.Completed = true;
                    detail.UpdatedOn = DateTime.Now;

                    //*
                    detail.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(detail).State = EntityState.Modified;
                    DB.SaveChanges();

                    response.StatusCode = ApiStatus.Ok;
                    response.Data = detail.MediaOrderId;
                }
                else
                {
                    response.StatusCode = ApiStatus.NotFound;
                    response.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }

        public ResponseCommon UpdateMediaOrderProductionStatusInComplete(int mediaOrderId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                MediaOrderProductionDetail detail = DB.MediaOrderProductionDetails.Where(x => x.MediaOrderId == mediaOrderId /*&& x.DeletedInd != true*/).FirstOrDefault();
                if (detail != null)
                {
                    detail.Completed = false;
                    detail.UpdatedOn = DateTime.Now;

                    //*
                    detail.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(detail).State = EntityState.Modified;
                    DB.SaveChanges();

                    response.StatusCode = ApiStatus.Ok;
                    response.Data = detail.MediaOrderId;
                }
                else
                {
                    response.StatusCode = ApiStatus.NotFound;
                    response.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
    }
}
