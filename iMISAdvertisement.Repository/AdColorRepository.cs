﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Asi.Repository
{
    public class AdColorRepository : IAdColorRepository
    {
        private readonly IDBEntities DB;
        public AdColorRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        #region Public Methods
        public ResponseCommon GetAdColors()
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<AdColorModel> lstAdColor = new List<AdColorModel>();
                lstAdColor = DB.AdColors/*.Where(x => x.DeletedInd != true)*/
                    .Select(x => new AdColorModel
                    {
                        AdColorId = x.AdColorId,
                        Name = x.AdColorName
                    })
                    .OrderBy(x => x.Name)
                    .ToList();

                if (lstAdColor.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }

                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstAdColor;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetAdColorById(int adColorId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                AdColorModel objAdColor = null;
                objAdColor = DB.AdColors.Where(x => /*x.DeletedInd != true &&*/ x.AdColorId == adColorId)
                                        .Select(x => new AdColorModel
                                        {
                                            AdColorId = x.AdColorId,
                                            Name = x.AdColorName
                                        }).FirstOrDefault();
                if (objAdColor == null)
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                    return objResponse;
                }
                objResponse.Data = objAdColor;
                objResponse.StatusCode = ApiStatus.Ok;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetAdColorsByRateCardId(int rateCardId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<AdColorModel> lstAdColor = (from rc in DB.RateCards
                                                 join rd in DB.RateCardDetails on rc.RateCardId equals rd.RateCardId
                                                 join c in DB.AdColors on rd.AdColorId equals c.AdColorId
                                                 where rd.RateCardId == rateCardId 
                                                        //&& rc.DeletedInd == false
                                                        //&& rd.DeletedInd == false 
                                                        //&& c.DeletedInd == false
                                                 select new AdColorModel()
                                                 {
                                                     AdColorId = c.AdColorId,
                                                     Name = c.AdColorName
                                                 }).Distinct().ToList();
                if (lstAdColor.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }

                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstAdColor;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetAdColorsByRateCardIdAndAdSizeId(int rateCardId, int adSizeId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                List<AdColorModel> lstAdColor = (from rc in DB.RateCards
                                                 join rd in DB.RateCardDetails on rc.RateCardId equals rd.RateCardId
                                                 join c in DB.AdColors on rd.AdColorId equals c.AdColorId
                                                 where rd.RateCardId == rateCardId 
                                                        && rd.AdSizeId == adSizeId 
                                                        //&& rc.DeletedInd == false
                                                        //&& rd.DeletedInd == false 
                                                        //&& c.DeletedInd == false
                                                 select new AdColorModel()
                                                 {
                                                     AdColorId = c.AdColorId,
                                                     Name = c.AdColorName
                                                 }).Distinct().ToList();
                if (lstAdColor.Count == 0)
                {
                    objResponse.StatusCode = ApiStatus.NoRecords;
                    objResponse.Message = ApiStatus.NoRecordsMessge;
                    return objResponse;
                }

                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = lstAdColor;
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon PostAdColor(AdColorModel colorModel)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                int isRecordExist = DB.AdColors.Where(x => x.AdColorName.ToLower() == colorModel.Name.ToLower()
                                                        /*&& x.DeletedInd == false*/).Count();
                if (isRecordExist <= 0)
                {
                    AdColor objAdColorAdd = new AdColor();
                    objAdColorAdd.AdColorName = colorModel.Name;
                    objAdColorAdd.CreatedOn = DateTime.Now;
                    //objAdColorAdd.DeletedInd = false;

                    //*
                    objAdColorAdd.UpdatedOn = DateTime.Now;
                    objAdColorAdd.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                    objAdColorAdd.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.AdColors.Add(objAdColorAdd);
                    DB.SaveChanges();

                    objResponse.Data = objAdColorAdd.AdColorId;
                    objResponse.StatusCode = ApiStatus.Ok;

                    return objResponse;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.AlreadyExist;
                    objResponse.Message = ApiStatus.AlreadyExistCustomMessge("Ad Color Name");

                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon PutAdColor(AdColorModel colorModel)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                AdColor objAdColorEdit = null;
                int isNameExist = DB.AdColors
                                    .Where(x => x.AdColorName.ToLower() == colorModel.Name.ToLower()
                                            && x.AdColorId != colorModel.AdColorId
                                            /*&& x.DeletedInd == false*/).Count();
                if (isNameExist > 0)
                {
                    objResponse.StatusCode = ApiStatus.AlreadyExist;
                    objResponse.Message = ApiStatus.AlreadyExistCustomMessge("Ad Color Name");
                    return objResponse;
                }

                objAdColorEdit = DB.AdColors.Where(x => /*x.DeletedInd != true &&*/ x.AdColorId == colorModel.AdColorId).FirstOrDefault();
                if (objAdColorEdit != null)
                {
                    objAdColorEdit.AdColorName = colorModel.Name;
                    objAdColorEdit.UpdatedOn = DateTime.Now;

                    //*
                    objAdColorEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objAdColorEdit).State = EntityState.Modified;
                    DB.SaveChanges();

                    objResponse.Data = objAdColorEdit.AdColorId;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon DeleteAdColor(int adColorId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                AdColor objAdColorEdit = null;
                int status = 0;
                objAdColorEdit = DB.AdColors.Where(x => /*x.DeletedInd != true &&*/ x.AdColorId == adColorId).FirstOrDefault();
                if (objAdColorEdit != null)
                {
                    //objAdColorEdit.DeletedInd = true;

                    //*
                    objAdColorEdit.UpdatedOn = DateTime.Now;
                    objAdColorEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                    DB.Entry(objAdColorEdit).State = EntityState.Modified;
                    status = DB.SaveChanges();
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }
        #endregion
    }
}
