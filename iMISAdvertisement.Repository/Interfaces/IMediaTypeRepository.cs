﻿using Asi.DAL;
using Asi.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.Repository
{
    public interface IMediaTypeRepository
    {
        #region MediaType
        ResponseCommon GetMediaTypes();
        ResponseCommon GetMediaTypesById(int mediaTypeId);
        ResponseCommon PostMediaType(PostMediaTypeModel objAdType);
        ResponseCommon PutMediaType(GetMediaTypeModel objAdType);
        ResponseCommon DeleteMediaType(int mediaTypeId);
        #endregion
    }
}
