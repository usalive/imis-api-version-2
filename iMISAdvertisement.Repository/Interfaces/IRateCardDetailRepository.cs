﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface IRateCardDetailRepository
    {
        ResponseCommon GetRateCardDetails(int rateCardId);
        ResponseCommon GetRateCardDetailById(int rateCardDetailId);
        ResponseCommon GetRateCardDetailsByColor(int rateCardId, int adColorId);
        ResponseCommon GetRateCardsCost(int rateCardId, int adColorId, int adSizeId, int frequencyId);
        ResponseCommon GetRateCardsCostForPerWord(int rateCardId, int adColorId);
        ResponseCommon GetMultipleRateCardsCost(int rateCardId, int adColorId, int adSizeId, int frequencyId);
        ResponseCommon GetMultipleRateCardsCostForPerWord(int rateCardId, int adColorId);
        ResponseCommon PostRateCardDetail(PostRateCardDetailModel cardDetailModel);
    }
}
