﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface IMediaOrderSignedDocumentsRepository
    {
        ResponseCommon getMediaOrderSignedDocumentsByBuyId(int Id);

        ResponseCommon saveMediaOrderSignedDocumentsByBuyId(MediaOrderSignedDocumentsModel objMediaOrderSignedDocumentsModel);
        ResponseCommon deleteMediaOrderSignedDocumentById(long signedDocumentId);
    }
}
