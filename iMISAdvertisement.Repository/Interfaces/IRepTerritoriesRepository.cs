﻿using Asi.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.Repository
{
    public interface IRepTerritoriesRepository
    {
        ResponseCommon GetRepTerritories();
        ResponseCommon GetRepTerritoriesByRepId(int repId);
        ResponseCommon GetRepTerritoriesByTerritoryId(int territoryId);
        ResponseCommon PostRepTerritory(RepTerritoryModel repTerritoryModel);
        ResponseCommon PutRepTerritory(RepTerritoryModel repTerritoryModel);
        ResponseCommon DeleteRepTerritoryByRepIdTerritoryId(int repId, int territoryId);
    }
}
