﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface IMediaOrderProductionDetailsRepository
    {
        ResponseCommon GetMediaOrderProductionDetailByMediaOrderId(int mediaOrderId);
        ResponseCommon AddUpdateMediaOrderProductionDetail(PostMediaOrderProductionDetailModel model);
        ResponseCommon AddImportUpdateMediaOrderProductionDetail(PostImportMediaOrderProductionDetailModel model);
        ResponseCommon DeleteMediaOrderProductionDetailByMediaOrderId(int mediaOrderId);
        ResponseCommon UpdateMediaOrderProductionStatusComplete(int mediaOrderId);
        ResponseCommon UpdateMediaOrderProductionStatusInComplete(int mediaOrderId);
    }
}
