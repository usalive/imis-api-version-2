﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface IPositionRepository
    {
        ResponseCommon GetPositions();
        ResponseCommon GetPositionById(int positionId);
        ResponseCommon PostPosition(PositionModel model);
        ResponseCommon PutPosition(PositionModel model);
        ResponseCommon DeletePosition(int positionId);
    }
}
