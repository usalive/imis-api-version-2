﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface ICompleteMediaOrderRepository
    {
        ResponseCommon GetCompleteMediaOrderByMediaOrderId(int mediaOrderId);
        ResponseCommon PostCompleteMediaOrder(PostInCompleteMediaOrderModel objInCompleteMediaOrder);
        ResponseCommon GetCompleteOrdersByFilterOptions(GetInCompleteOrdersbyFilterOption objMediaOrderFilterOptions);
        ResponseCommon FillDropdownForCompleteOrder(GetInCompleteOrdersbyFilterOption objMediaOrderFilterOptions);
    }
}
