﻿using Asi.DAL;
using Asi.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.Repository
{
    public interface IMediaAssetGroupRepository
    {
        #region MediaAssetGroup
        ResponseCommon GetMediaAssetGroups();
        ResponseCommon GetMediaAssetGroupsById(int mediaAssetGroupId);
        ResponseCommon PostMediaAssetGroup(PostMediaAssetGroupModel objMediaAssetGroup);
        ResponseCommon PutMediaAssetGroup(GetMediaAssetGroupModel objMediaAssetGroup);
        ResponseCommon DeleteMediaAssetGroup(int mediaAssetGroupId);
        #endregion
    }
}
