﻿using Asi.DAL.Model;


namespace Asi.Repository
{
    public interface IAdvAgencyMappingRepository
    {
        ResponseCommon GetAdvertiserAgencyMapping(string AdvertiserId);
        ResponseCommon PostMapping(AdvertiserAgencyMappingModel model);

        //-----------------------------------------------------------        
        ResponseCommon GetAdvertiserAgencyMappingList();
        ResponseCommon DeleteAdvertiserAgencyMappingById(int advertiserAgencyMapId);
        ResponseCommon GetAdvertiserAgencyMappingListByAdvertiserId(string advertiserId);
    }
}