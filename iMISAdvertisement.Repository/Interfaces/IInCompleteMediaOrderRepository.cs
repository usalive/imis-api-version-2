﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface IInCompleteMediaOrderRepository
    {
        ResponseCommon GetInCompleteMediaOrderByMediaOrderId(int mediaOrderId);
        ResponseCommon PostInCompleteMediaOrder(PostInCompleteMediaOrderModel objInCompleteMediaOrder);
        ResponseCommon GetInCompleteOrdersByFilterOptions(GetInCompleteOrdersbyFilterOption objMediaOrderFilterOptions);
        ResponseCommon FillDropdownForInCompleteOrder(GetInCompleteOrdersbyFilterOption objMediaOrderFilterOptions);
        ResponseCommon FillFilterDropdownInitially();
        ResponseCommon FillProductionDropdownInitially();
    }
}
