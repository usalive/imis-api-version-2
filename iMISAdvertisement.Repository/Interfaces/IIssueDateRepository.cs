﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface IIssueDateRepository
    {
        ResponseCommon GetIssueDates();
        ResponseCommon GetIssueDatesById(int issueDateId);
        ResponseCommon GetIssueDatesByMediaAsset(int mediaAssetId);
        ResponseCommonForDatatable GetIssueDatesByMediaAsset();
        ResponseCommon GetIssueDatesByMediaAssetAndDateRange(GetIssueDatesByMediaAssetAndDateRangeModel model);
        ResponseCommon PostIssueDate(PostIssueDateModel objIssueDate);
        ResponseCommon PutIssueDate(PutIssueDateModel objIssueDate);
        ResponseCommon DeleteIssueDate(int issueDateId);
    }
}
