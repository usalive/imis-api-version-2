﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface IAdAdjustmentRepository
    {
        ResponseCommon GetAdAdjustments();
        ResponseCommon GetAdAdjustmentById(int adAdjustmentId);
        ResponseCommon GetAdAdjustmentsByMediaAsset(int mediaAssetId);
        ResponseCommon PostAdAdjustment(PostAdAdjustmentModel adAdjustmentModel);
        ResponseCommon PutAdAdjustment(GetAdAdjustmentModel adAdjustmentModel);
        ResponseCommon DeleteAdAdjustment(int adAdjustmentId);
        ResponseCommon GetAdAdjustmentsByMediaAssets(GetAdjustmentsByMediaAssetIdsModel ids);
    }
}
