﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface IInventoryRepository
    {
        ResponseCommon GetInventoryDetails(int mediaAssetId);
        //ResponseCommon GetRateCardDetailById(int RateCardDetailId);
        //ResponseCommon GetRateCardDetailsByColor(int rateCardId, int adColorId);
        //ResponseCommon GetRateCardsCost(int RateCardId, int AdColorId, int AdSizeId, int FrequencyId);
        //ResponseCommon GetRateCardsCostForPerWord(int RateCardId, int AdColorId);
        //ResponseCommon GetMultipleRateCardsCost(int RateCardId, int AdColorId, int AdSizeId, int FrequencyId);
        //ResponseCommon GetMultipleRateCardsCostForPerWord(int RateCardId, int AdColorId);
        ResponseCommon PostInventory(PostInventoryModel postInventory);
    }
}
