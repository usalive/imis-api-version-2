﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface IAdSizeRepository
    {
        ResponseCommon GetAdSizes();
        ResponseCommon GetAdSizesById(int adSizeId);
        ResponseCommon PostAdSize(PostAdSizeModel objAdSize);
        ResponseCommon PutAdSize(GetAdSizeModel objAdSize);
        ResponseCommon DeleteAdSize(int adSizeId);
        ResponseCommon GetAdSizesByRateCardIdAndColordId(int rateCardId, int adColorId);
        ResponseCommon GetAdSizesByRateCardId(int rateCardId);
        ResponseCommon GetAdSizesByMediaAsset(int mediaAssetId);
    }
}
