﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface IProductionStatusRepository
    {
        ResponseCommon GetProductionStatuses();
        ResponseCommon GetProductionStatusById(int productionStatusId);
        ResponseCommon PostProductionStatus(ProductionStatusModel productionStatusModel);
        ResponseCommon PutProductionStatus(ProductionStatusModel productionStatusModel);
        ResponseCommon DeleteProductionStatus(int productionStatusId);
    }
}
