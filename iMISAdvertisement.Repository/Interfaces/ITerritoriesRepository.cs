﻿using Asi.DAL;
using Asi.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.Repository
{
    public interface ITerritoriesRepository
    {
        ResponseCommon GetTerritories();
        ResponseCommon GetTerritoryById(int territoryId);
        ResponseCommon PostTerritory(TerritoryModel obj);
        ResponseCommon PutTerritory(TerritoryModel obj);
        ResponseCommon DeleteTerritory(int territoryId);
    }
}
