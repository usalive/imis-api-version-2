﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface IRateCardRepository
    {
        ResponseCommon GetRateCards();
        ResponseCommon GetRateCardById(int rateCardId);
        ResponseCommon GetRateCardsByMediaAsset(int mediaAssetId);
        ResponseCommon PostRateCard(PostRateCardModel objRateCard);
        ResponseCommon PutRateCard(PostRateCardModel objRateCard);
        ResponseCommon DeleteRateCard(int rateCardId);
    }
}
