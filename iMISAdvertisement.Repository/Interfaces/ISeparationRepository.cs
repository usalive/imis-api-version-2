﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface ISeparationRepository
    {
        ResponseCommon GetSeparations();
        ResponseCommon GetSeparationById(int separationId);
        ResponseCommon PostSeparation(SeparationModel model);
        ResponseCommon PutSeparation(SeparationModel model);
        ResponseCommon DeleteSeparation(int separationId);
    }
}
