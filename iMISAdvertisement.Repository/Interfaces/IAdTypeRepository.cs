﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface IAdTypeRepository
    {
        ResponseCommon GetAdTypes();
        ResponseCommon GetAdTypeById(int adTypeId);
        ResponseCommon PostAdType(PostAdTypeModel objAdType);
        ResponseCommon PutAdType(GetAdTypeModel objAdType);
        ResponseCommon DeleteAdType(int adTypeId);
        ResponseCommon GetAllAdTypeForPostOrder();
    }
}
