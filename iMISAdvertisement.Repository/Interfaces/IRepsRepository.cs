﻿using Asi.DAL;
using Asi.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.Repository
{
    public interface IRepsRepository
    {
        ResponseCommon GetReps();
        ResponseCommon GetRepsForOrderCreation();
        ResponseCommon GetRepById(int repId);
        ResponseCommon PostRep(RepModel obj);
        ResponseCommon PutRep(RepModel obj);
        ResponseCommon DeleteRep(int repId);
    }
}
