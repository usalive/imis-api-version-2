﻿using Asi.DAL;
using Asi.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asi.Repository
{
    public interface IMediaAssetRepository
    {
        #region MediaAsset
        ResponseCommonForDatatable GetMediaAssets();
        ResponseCommon GetAllMediaAssets();
        ResponseCommon GetMediaAssetsById(int mediaAssetId);
        ResponseCommon PostMediaAsset(PostMediaAssetModel objAdAsset);
        ResponseCommon PutMediaAsset(PutMediaAssetModel objAdAsset);
        ResponseCommon DeleteMediaAsset(int mediaAssetId);
        #endregion
    }
}
