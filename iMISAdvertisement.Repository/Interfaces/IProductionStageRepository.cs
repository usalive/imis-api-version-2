﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface IProductionStageRepository
    {
        ResponseCommon GetProductionStages();
        ResponseCommon GetProductionStageById(int productionStageId);
        ResponseCommon PostProductionStage(ProductionStageModel objProductionStage);
        ResponseCommon PutProductionStage(ProductionStageModel objProductionStage);
        ResponseCommon DeleteProductionStage(int productionStageId);
    }
}
