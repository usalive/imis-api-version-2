﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface IAdvertiserRepository
    {
        ResponseCommon GetAdvertisers();
        ResponseCommon GetAdvertiserById(int advertiserId);
        //ResponseCommon GetAdAdjustmentsByMediaAsset(int mediaAssetId);
        ResponseCommon PostAdvertiser(PostAdvertisementModel objadvertiser);
        //ResponseCommon PutAdAdjustment(GetAdAdjustmentModel objadAdjustment);
        //ResponseCommon DeleteAdAdjustment(int adAdjustmentId);
        //ResponseCommon GetAdAdjustmentsByMediaAssets(GetAdjustmentsByMediaAssetIdsModel ids);
    }
}
