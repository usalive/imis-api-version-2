﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface IAdvertiserRepTerritoryMapRepository
    {
        ResponseCommon GetRepTerritoryMapping(string AdvertiserId);
        ResponseCommon PostMapping(AdvertiserRepTerritoryMappingModel model);

        //--------------------------------------------------------------------------
        ResponseCommon GetRepTerritoryMappingListByAdvertiserId(string AdvertiserId);
        ResponseCommon GetAdvertiserRepTerritoryMappingList();
        ResponseCommon DeleteAdvertiserRepTerritoryMapping(int id);
    }
}