﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface IPickUpFilesMediaOrderRepository
    {
        ResponseCommon GetPickUpFilesOrdersByFilterOptions(GetInCompleteOrdersbyFilterOption objMediaOrderFilterOptions);
    }
}
