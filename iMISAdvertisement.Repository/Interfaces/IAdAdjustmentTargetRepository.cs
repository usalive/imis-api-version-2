﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface IAdvertiserAgencyMappingRepository
    {
        ResponseCommon GetAdAdjustmentTargets();
        ResponseCommon GetAdAdjustmentTargetById(int adAdjustmentTargetId);
        //ResponseCommon PostAdAdjustmentTarget(GetAdAdjustmentTargetModel model);
        //ResponseCommon PutAdAdjustmentTarget(GetAdAdjustmentTargetModel model);
        //ResponseCommon DeleteAdAdjustmentTarget(int adAdjustmentTargetId);
    }
}
