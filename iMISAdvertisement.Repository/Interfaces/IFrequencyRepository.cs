﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface IFrequencyRepository
    {
        ResponseCommon GetFrequencies();
        ResponseCommon GetFrequencyById(int frequencyId);
        ResponseCommon GetFrequencyByRateCardIdAndColordId(int rateCardId, int adColorId);
        ResponseCommon GetFrequencyByRateCardIdColordIdAndAdSizeId(int rateCardId, int adColorId, int adSizeId);
        ResponseCommon PostFrequency(FrequencyModel frequency);
        ResponseCommon PutFrequency(FrequencyModel frequency);
        ResponseCommon DeleteFrequency(int frequencyId);
    }
}
