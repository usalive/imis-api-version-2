﻿using Asi.DAL.Model;
using System.Collections.Generic;

namespace Asi.Repository
{
    public interface IMediaOrdersRepository
    {
        ResponseCommon GenerateBuyId();
        ResponseCommon GetOrdersByBuyId(int buyId);
        ResponseCommon GetOrdersNotInBuyer(int buyId, string advertiserId);
        // ResponseCommon GetOrdersByAdvertisor(int draw, int skip, int take, string[] advertisorId);
        ResponseCommon GetOrdersByAdvertisor(GetOrdersByAdvertisorModel model);
        //ResponseCommon GetOrdersBuyIdWise(GetOrdersBuyIdWiseModal[] modal);
        //ResponseCommonForDatatable GetOrdersBuyIdWise(GetOrdersBuyIdWiseModal[] modal);
        ResponseCommonForDatatable GetOrdersBuyIdWise();
        //ResponseCommon GetOrdersForPostOrder(GetOrdersForPostOrderbyFilterOption model);
        ResponseCommonForDatatable GetOrdersForPostOrder();
        ResponseCommon FillDropdownForPostOrder(GetOrdersForPostOrderbyFilterOption model);
        ResponseCommon GetAllAdvertiserForPostOrder();
        ResponseCommon AddMediaOrder(PostMediaOrderModel model);
        ResponseCommon AddImportMediaOrder(PostImportMediaOrderModel model);
        ResponseCommon DeleteMediaOrder(int mediaOrderId);
        ResponseCommon ChangeStatus(MediaOrderChangeStatusModel model);
        ResponseCommon GetOrderByMediaOrderId(int mediaOrderId);
        ResponseCommon UpdateMediaOrder(PutMediaOrderModel model);
        ResponseCommon GetOrdersBuyIdWiseForCopy(int buyId);
        ResponseCommon PostOrdersForCopy(MediaOrderCopyModel[] model);
        //ResponseCommon GetOrdersbyProductionStatus(GetOrdersForMissingMaterialsbyFilterOption model);
        ResponseCommonForDatatable GetOrdersbyProductionStatus();
        ResponseCommon ImportExcelSheet();
        ResponseCommon UpdateIsFrozenTrue(List<int> mediaOrderIds);
    }
}
