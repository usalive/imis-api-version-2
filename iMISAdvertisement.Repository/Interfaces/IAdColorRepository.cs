﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface IAdColorRepository
    {
        ResponseCommon GetAdColors();
        ResponseCommon GetAdColorById(int adColorId);
        ResponseCommon GetAdColorsByRateCardId(int rateCardId);
        ResponseCommon GetAdColorsByRateCardIdAndAdSizeId(int rateCardId, int adSizeId);
        ResponseCommon PostAdColor(AdColorModel objAdColor);
        ResponseCommon PutAdColor(AdColorModel objAdColor);
        ResponseCommon DeleteAdColor(int adColorId);
    }
}
