﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface IDashBoardRepository
    {
        ResponseCommon GetInsertionOrderCountbyOrderStatusForCurrentYear(GetOrdersWithProposalStatusbyFilterOption model);
        ResponseCommon GetOrderCountbyMonthlyForCurrentYear();
        ResponseCommon GetInsertionOrderCount();
        ResponseCommon GetTotalNetAmount();
        ResponseCommon GetMediaAssetUsedInOrder();
        ResponseCommon GetAdvertiserUsedInOrder();
       // ResponseCommon GetOrderByAdvertisers();
        ResponseCommonForDatatable GetOrderByAdvertisers();
      //  ResponseCommon GetOrderDetailsByMediaAssets();
        ResponseCommonForDatatable GetOrderDetailsByMediaAssets();
      //  ResponseCommon GetOrdersByProposalStatus(GetOrdersWithProposalStatusbyFilterOption model);
        ResponseCommonForDatatable GetOrdersByProposalStatus();
        ResponseCommon GetOrderCountbyMonthlyForYearly();
        ResponseCommon GetDashboardData();
    }
}
