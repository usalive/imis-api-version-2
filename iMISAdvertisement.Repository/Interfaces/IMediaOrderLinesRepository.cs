﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface IMediaOrderLinesRepository
    {
        ResponseCommon GetMediaOrderLinesByMediaOrderId(int mediaOrderId);

        ResponseCommon GetLogoByBuyId(int buyId);
        ResponseCommon AddMediaOrderLine(PostMediaOrderLineModel model);
        ResponseCommon AddImportMediaOrderLine(PostImportMediaOrderLineModel model);
        ResponseCommon DeleteMediaOrderLineByMediaOrderId(int mediaOrderId);
        ResponseCommon DeleteMediaOrderLineByMediaOrderLineIds(DeleteMediaOrderLinesModel model);
        ResponseCommon UpdateMediaOrderLines(MediaOrderLineUpdateModel model);
    }
}
