﻿using Asi.DAL.Model;

namespace Asi.Repository
{
    public interface IMediaBillingMethodRepository
    {
        ResponseCommon GetMediaBillingMethods();
        ResponseCommon GetMediaBillingMethodById(int mediaBillingMethodId);
        //ResponseCommon PostMediaBillingMethod(MediaBillingMethodModel objMediaBillingMethod);
        //ResponseCommon PutMediaBillingMethod(MediaBillingMethodModel objMediaBillingMethod);
        //ResponseCommon DeleteMediaBillingMethod(int mediaBillingMethodId);
    }
}