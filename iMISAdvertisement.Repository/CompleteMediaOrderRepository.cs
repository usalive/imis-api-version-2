﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;

namespace Asi.Repository
{
    public class CompleteMediaOrderRepository : ICompleteMediaOrderRepository
    {
        private readonly IDBEntities DB;
        public CompleteMediaOrderRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        #region Public Methods
        public ResponseCommon GetCompleteMediaOrderByMediaOrderId(int mediaOrderId)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                string currentDomain = CommonUtility.CurrentHostDomain;
                GetInCompleteMediaOrderModel detail = DB.MediaOrderProductionDetails.Where(x => x.MediaOrderId == mediaOrderId 
                                                                                    /*&& x.DeletedInd != true*/)
                                                        .Select(x => new GetInCompleteMediaOrderModel
                                                        {
                                                            MediaOrderProductionDetailId = x.MediaOrderProductionDetailId,
                                                            CreateExpectedInd = x.CreateExpectedInd,
                                                            FinalFile = x.FinalFile == null ? null : currentDomain + x.FinalFile,
                                                            HeadLine = x.HeadLine,
                                                            MaterialContactId = x.MaterialContactId,
                                                            MaterialExpectedDate = x.MaterialExpectedDate,
                                                            MediaOrderId = x.MediaOrderId,
                                                            NewPickupInd = x.NewPickupInd,
                                                            ChangesInd = x.ChangesInd,
                                                            OnHandInd = x.OnHandInd,
                                                            AdvertiserAgencyInd = x.AdvertiserAgencyInd,
                                                            ProductionStatusId = x.ProductionStatusId,
                                                            ProductionStatusName = DB.ProductionStatuses.Where(y => y.ProductionStatusId == x.ProductionStatusId).Select(z => z.ProductionStatusName).FirstOrDefault(),
                                                            OriginalFile = x.OriginalFile == null ? null : currentDomain + x.OriginalFile,
                                                            PageNumber = x.PageNumber,
                                                            PickupMediaOrderId = x.PickupMediaOrderId,
                                                            PositionId = x.PositionId,
                                                            ProductionComment = x.ProductionComment,
                                                            ProofFile = x.ProofFile == null ? null : currentDomain + x.ProofFile,
                                                            SeparationId = x.SeparationId,
                                                            TearSheets = x.TearSheets,
                                                            TrackingNumber = x.TrackingNumber,
                                                            WebAdUrl = x.WebAdUrl,
                                                            Completed = x.Completed,
                                                            ClassifiedText = x.MediaOrder.ClassifiedText,
                                                            ProductionStages = (from y in DB.MediaOrderProductionStages
                                                                                where y.MediaOrderId == x.MediaOrderId /*&& y.DeletedInd != true*/
                                                                                select new GetProductionStagesModel
                                                                                {
                                                                                    MediaOrderProductionStagesId = y.MediaOrderProductionStagesId,
                                                                                    MediaOrderId = y.MediaOrderId,
                                                                                    ProductionStageId = y.ProductionStageId,
                                                                                    ProductionStagesName = DB.ProductionStages.Where(q => q.ProductionStageId == y.ProductionStageId).Select(w => w.ProductionStageName).FirstOrDefault(),
                                                                                    UpdateBy = Convert.ToString(y.UpdatedByUserKey),
                                                                                    UpdatedDate = y.UpdatedOn
                                                                                }).ToList()
                                                        }).FirstOrDefault();

                objResponse.StatusCode = ApiStatus.Ok;
                objResponse.Data = detail;
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
            }
            return objResponse;
        }

        public ResponseCommon PostCompleteMediaOrder(PostInCompleteMediaOrderModel model)
        {
            ResponseCommon objResponse = new ResponseCommon();
            try
            {
                var mediaOrderId = model.MediaOrderId;
                MediaOrderProductionDetail productionDetail = DB.MediaOrderProductionDetails.Where(x => x.MediaOrderId == mediaOrderId 
                /*&& x.DeletedInd != true*/).FirstOrDefault();
                if (productionDetail != null)
                {
                    string originalFileName = null, proofFile = null, finalFile = null;
                    if (!string.IsNullOrEmpty(model.OriginalFile))
                        originalFileName = CommonUtility.SaveBase64ToFile(model.OriginalFile, model.OriginalFileExtension);
                    if (!string.IsNullOrEmpty(model.ProofFile))
                        proofFile = CommonUtility.SaveBase64ToFile(model.ProofFile, model.ProofFileExtension);
                    if (!string.IsNullOrEmpty(model.FinalFile))
                        finalFile = CommonUtility.SaveBase64ToFile(model.FinalFile, model.FinalFileExtension);

                    if (productionDetail != null)
                    {
                        productionDetail.NewPickupInd = model.NewPickupInd;
                        productionDetail.PickupMediaOrderId = model.PickupMediaOrderId;
                        productionDetail.CreateExpectedInd = model.CreateExpectedInd;
                        productionDetail.MaterialExpectedDate = model.MaterialExpectedDate;
                        productionDetail.TrackingNumber = model.TrackingNumber;
                        productionDetail.ChangesInd = model.ChangesInd;
                        productionDetail.OnHandInd = model.OnHandInd;
                        productionDetail.AdvertiserAgencyInd = model.AdvertiserAgencyInd;
                        productionDetail.ProductionStatusId = model.ProductionStatusId;
                        productionDetail.MaterialContactId = model.MaterialContactId;
                        productionDetail.OriginalFile = string.IsNullOrEmpty(originalFileName) ? productionDetail.OriginalFile : originalFileName;
                        productionDetail.ProofFile = string.IsNullOrEmpty(proofFile) ? productionDetail.ProofFile : proofFile;
                        productionDetail.FinalFile = string.IsNullOrEmpty(finalFile) ? productionDetail.FinalFile : finalFile;
                        productionDetail.WebAdUrl = model.WebAdUrl;
                        productionDetail.TearSheets = model.TearSheets;
                        productionDetail.HeadLine = model.HeadLine;
                        productionDetail.PositionId = model.PositionId;
                        productionDetail.SeparationId = model.SeparationId;
                        productionDetail.PageNumber = model.PageNumber;
                        productionDetail.ProductionComment = model.ProductionComment;
                        productionDetail.ProductionComment = model.ProductionComment;

                        productionDetail.UpdatedOn = DateTime.Now;

                        //*
                        productionDetail.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                        DB.Entry(productionDetail).State = EntityState.Modified;

                        List<PostProductionStagesModel> lstProductionStages = model.ProductionStages;
                        List<MediaOrderProductionStage> details = DB.MediaOrderProductionStages.Where(x => x.MediaOrderId == mediaOrderId 
                        /* && x.DeletedInd != true*/).ToList();

                        foreach (var objdetail in details)
                        {
                            int indexOf = lstProductionStages.FindIndex(x => x.MediaOrderProductionStagesId == objdetail.MediaOrderProductionStagesId.ToString());
                            if (indexOf < 0)
                            {
                                //objdetail.DeletedInd = true;

                                //*
                                objdetail.UpdatedOn = DateTime.Now;
                                objdetail.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                DB.Entry(objdetail).State = EntityState.Modified;
                            }
                            else
                            {
                                PostProductionStagesModel objMOPS = lstProductionStages[indexOf];
                                MediaOrderProductionStage dbMOPS = details.Where(x => x.MediaOrderProductionStagesId.ToString() == objMOPS.MediaOrderProductionStagesId).FirstOrDefault();
                                if (dbMOPS != null)
                                {
                                    dbMOPS.MediaOrderId = mediaOrderId;
                                    dbMOPS.ProductionStageId = objMOPS.ProductionStageId;
                                    //dbMOPS.UpdatedByUserKey = Guid.Parse(objMOPS.UpdateBy);
                                    dbMOPS.UpdatedByUserKey = Constants.Created_Update_ByUserKey;
                                    dbMOPS.UpdatedOn = DateTime.Now;
                                    //dbMOPS.DeletedInd = false;
                                    DB.Entry(dbMOPS).State = EntityState.Modified;
                                }
                            }
                        }

                        foreach (var objPS in lstProductionStages)
                        {
                            string id = objPS.MediaOrderProductionStagesId;
                            //int tmpId = Convert.ToInt32(id);
                            if (!string.IsNullOrEmpty(id))
                            {
                                int tmpId = Convert.ToInt32(id);
                                if (tmpId > 0)
                                {
                                }
                            }
                            else
                            {
                                MediaOrderProductionStage objMediaOrderProductionStage = new MediaOrderProductionStage();
                                objMediaOrderProductionStage.MediaOrderId = mediaOrderId;
                                objMediaOrderProductionStage.ProductionStageId = objPS.ProductionStageId;
                                //objMediaOrderProductionStage.UpdatedByUserKey = Guid.Parse(objPS.UpdateBy);
                                objMediaOrderProductionStage.UpdatedByUserKey = Constants.Created_Update_ByUserKey;
                                objMediaOrderProductionStage.UpdatedOn = DateTime.Now;
                                //objMediaOrderProductionStage.DeletedInd = false;
                                DB.MediaOrderProductionStages.Add(objMediaOrderProductionStage);
                            }
                        }

                        DB.SaveChanges();
                    }
                    objResponse.Data = productionDetail.MediaOrderProductionDetailId;
                    objResponse.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    objResponse.StatusCode = ApiStatus.NotFound;
                    objResponse.Message = ApiStatus.NotFoundMessage;
                }
            }
            catch (Exception ex)
            {
                objResponse.StatusCode = ApiStatus.Exception;
                objResponse.Message = ApiStatus.ExceptionMessge;
                LoggerRepsitory.WriteErrorLog(ex);
            }
            return objResponse;
        }

        public ResponseCommon GetCompleteOrdersByFilterOptions(GetInCompleteOrdersbyFilterOption model)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                if (model != null)
                {
                    string zoneQuery = model.MediaAssetId > 0 ? "MediaAssetId == " + model.MediaAssetId : "1=1";
                    zoneQuery += model.IssueDateId > 0 ? " && IssueDateId == " + model.IssueDateId : " && 1=1";
                    zoneQuery += model.AdTypeId > 0 ? " && AdTypeId == " + model.AdTypeId : " && 1=1";
                    zoneQuery += model.RepId > 0 ? " && RepId == " + model.RepId : " && 1=1";

                    IQueryable<InCompleteInsertionOrderModel> inCompleteInserationOrderModels;
                    inCompleteInserationOrderModels = (from ord in DB.MediaOrders
                                           where /*ord.DeletedInd != true &&*/ (ord.MediaOrderProductionDetails.Select(y => y.Completed).FirstOrDefault() == true)
                                           orderby ord.MediaOrderId descending
                                           select new InCompleteInsertionOrderModel
                                           {
                                               BuyId = ord.BuyId,
                                               MediaOrderId = ord.MediaOrderId,
                                               MediaAssetId = ord.MediaAssetId,
                                               IssueDateId = ord.IssueDateId,
                                               AdTypeId = ord.RateCardDetail.AdType.AdTypeId,
                                               AdTypeName = ord.RateCardDetail.AdType.AdTypeName,
                                               AdcolorId = ord.RateCardDetail.AdColor.AdColorId,
                                               AdcolorName = ord.RateCardDetail.AdColor.AdColorName,
                                               AdSizeId = ord.RateCardDetail.AdSize.AdSizeId,
                                               AdSizeName = ord.RateCardDetail.AdSize.AdSizeName,
                                               FrequencyId = ord.RateCardDetail.Frequency.FrequencyId,
                                               FrequencyName = ord.RateCardDetail.Frequency.FrequencyName,
                                               AdvertiserId = ord.AdvertiserId,
                                               BT_ID = ord.BtId,
                                               ST_ID = ord.StId,
                                               GrossCost = ord.GrossCost,
                                               NetCost = ord.NetCost,
                                               FlightStartDate = ord.FlightEndDate,
                                               FlightEndDate = ord.FlightEndDate,
                                               RepId = ord.MediaOrderReps.Where(x => x.MediaOrderId == ord.MediaOrderId).Select(y => y.RepId).FirstOrDefault(),
                                               RepName = DB.Reps.Where(x => x.RepId == (ord.MediaOrderReps.Where(y => y.MediaOrderId == ord.MediaOrderId).Select(z => z.RepId))
                                                         .FirstOrDefault()).Select(t => t.RepName).FirstOrDefault(),
                                               Completed = ord.MediaOrderProductionDetails.Where(x => x.MediaOrderId == ord.MediaOrderId).Select(y => y.Completed).FirstOrDefault(),
                                               NewPickupInd = ord.MediaOrderProductionDetails.Where(x => x.MediaOrderId == ord.MediaOrderId).Select(y => y.NewPickupInd).FirstOrDefault(),
                                               CreateExpectedInd = ord.MediaOrderProductionDetails.Where(x => x.MediaOrderId == ord.MediaOrderId).Select(y => y.CreateExpectedInd).FirstOrDefault(),
                                               ChangesInd = ord.MediaOrderProductionDetails.Where(x => x.MediaOrderId == ord.MediaOrderId).Select(y => y.ChangesInd).FirstOrDefault(),
                                               PositionId = ord.MediaOrderProductionDetails.Where(x => x.MediaOrderId == ord.MediaOrderId).Select(y => y.PositionId).FirstOrDefault(),
                                               PositionName = DB.Positions.Where(x => x.PositionId == (ord.MediaOrderProductionDetails.Where(y => y.MediaOrderId == ord.MediaOrderId)
                                                       .Select(y => y.PositionId).FirstOrDefault())).Select(t => t.PositionName).FirstOrDefault(),
                                               PageFraction = ord.RateCardDetail.AdSize.PageFraction,
                                               OnHandInd = ord.MediaOrderProductionDetails.Where(x => x.MediaOrderId == ord.MediaOrderId).Select(y => y.OnHandInd).FirstOrDefault(),
                                               TrackingNumber = ord.MediaOrderProductionDetails.Where(x => x.MediaOrderId == ord.MediaOrderId).Select(y => y.TrackingNumber).FirstOrDefault(),
                                               MaterialExpectedDate = ord.MediaOrderProductionDetails.Where(x => x.MediaOrderId == ord.MediaOrderId).Select(y => y.MaterialExpectedDate).FirstOrDefault(),
                                               PickupMediaOrderId = ord.MediaOrderProductionDetails.Where(x => x.MediaOrderId == ord.MediaOrderId).Select(y => y.PickupMediaOrderId).FirstOrDefault(),
                                               PickupIndFromMediaAsset = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/ x.MediaOrderId == ord.MediaOrderProductionDetails.Where(q => q.MediaOrderId == ord.MediaOrderId).Select(u => u.PickupMediaOrderId).FirstOrDefault())
                                                  .Select(z => z.MediaAsset.MediaAssetName).FirstOrDefault(),
                                               PickupIndFromIssueDate = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/ x.MediaOrderId == ord.MediaOrderProductionDetails.Where(q => q.MediaOrderId == ord.MediaOrderId).Select(u => u.PickupMediaOrderId).FirstOrDefault())
                                               .Select(z => z.AdIssueDate.CoverDate).FirstOrDefault(),
                                               PickupIndFromPg = DB.MediaOrders.Where(x => /*x.DeletedInd != true &&*/ x.MediaOrderId == ord.MediaOrderProductionDetails.Where(q => q.MediaOrderId == ord.MediaOrderId).Select(u => u.PickupMediaOrderId).FirstOrDefault())
                                                  .Select(z => z.MediaOrderProductionDetails.Where(y => y.MediaOrderId == z.MediaOrderId).Select(t => t.PageNumber).FirstOrDefault()).FirstOrDefault(),
                                           }).Where(zoneQuery);

                    List<InCompleteInsertionOrderModel> lstIcMediaOrderModel = null;
                    lstIcMediaOrderModel = inCompleteInserationOrderModels.ToList();
                    if (lstIcMediaOrderModel.Count > 0)
                    {
                        response.StatusCode = ApiStatus.Ok;
                        response.Data = lstIcMediaOrderModel;
                    }
                    else
                    {
                        response.StatusCode = ApiStatus.NoRecords;
                        response.Message = ApiStatus.NoRecordsMessge;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }

        public ResponseCommon FillDropdownForCompleteOrder(GetInCompleteOrdersbyFilterOption model)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                string zoneQuery = model.MediaAssetId > 0 ? "MediaAssetId == " + model.MediaAssetId : "1=1";
                zoneQuery += model.IssueDateId > 0 ? " && IssueDateId == " + model.IssueDateId : " && 1=1";
                zoneQuery += model.AdTypeId > 0 ? " && AdTypeId == " + model.AdTypeId : " && 1=1";
                zoneQuery += model.RepId > 0 ? " && RepId == " + model.RepId : " && 1=1";

                IQueryable<TempInCompleteModel> tempInCompleteModels;
                tempInCompleteModels = (from ord in DB.MediaOrders
                                     where /*ord.DeletedInd != true &&*/ ord.IsFrozen == false
                                     orderby ord.MediaOrderId descending
                                     // group ord by new { ord.RateCardId } into g
                                     select new TempInCompleteModel
                                     {
                                         BuyId = ord.BuyId,
                                         MediaAssetId = ord.MediaAssetId,
                                         MediaAssetName = ord.MediaAsset.MediaAssetName,
                                         IssueDateId = ord.IssueDateId,
                                         CoverDate = ord.AdIssueDate.CoverDate,
                                         AdTypeId = ord.RateCard.AdType.AdTypeId,
                                         AdTypeName = ord.RateCard.AdType.AdTypeName,
                                         RepId = ord.MediaOrderReps.Where(x => x.MediaOrderId == ord.MediaOrderId).Select(y => y.RepId).FirstOrDefault(),
                                         RepName = DB.Reps.Where(w => w.RepId == ord.MediaOrderReps.Where(x => x.MediaOrderId == ord.MediaOrderId).Select(y => y.RepId).FirstOrDefault())
                                                  .Select(y => y.RepName).FirstOrDefault(),
                                     }).Where(zoneQuery);

                List<InCompleteAdType> lstAdTypes = tempInCompleteModels.Select(x => new InCompleteAdType
                {
                    AdTypeId = x.AdTypeId,
                    AdTypeName = x.AdTypeName,
                }).GroupBy(x => x.AdTypeId).Select(t => t.FirstOrDefault()).ToList();

                List<InCompleteMediaAsset> lstMediaAssets = tempInCompleteModels.Select(x => new InCompleteMediaAsset
                {
                    MediaAssetId = x.MediaAssetId,
                    MediaAssetName = x.MediaAssetName,
                }).GroupBy(x => x.MediaAssetId).Select(t => t.FirstOrDefault()).ToList();

                List<InCompleteIssueDate> lstIssueDates = tempInCompleteModels.Select(x => new InCompleteIssueDate
                {
                    IssueDateId = x.IssueDateId,
                    CoverDate = x.CoverDate,
                }).GroupBy(x => x.IssueDateId).Select(t => t.FirstOrDefault()).ToList();

                List<InCompleteRep> lstReps = tempInCompleteModels.Select(x => new InCompleteRep
                {
                    RepId = x.RepId,
                    RepName = x.RepName,
                }).GroupBy(x => x.RepId).Select(t => t.FirstOrDefault()).ToList();

                FillInCompleteDropdownsModel objInCompleteDropdown = new FillInCompleteDropdownsModel();
                List<FillInCompleteDropdownsModel> lstInCompleteDropdown = new List<FillInCompleteDropdownsModel>();
                objInCompleteDropdown.BuyId = tempInCompleteModels.ToList().Select(x => x.BuyId).FirstOrDefault();
                objInCompleteDropdown.Reps = lstReps;
                objInCompleteDropdown.IssueDates = lstIssueDates;
                objInCompleteDropdown.MediaAssets = lstMediaAssets;
                objInCompleteDropdown.AdTypes = lstAdTypes;
                lstInCompleteDropdown.Add(objInCompleteDropdown);

                if (lstInCompleteDropdown.Count > 0)
                {
                    response.StatusCode = ApiStatus.Ok;
                    response.Data = lstInCompleteDropdown;
                }
                else
                {
                    response.StatusCode = ApiStatus.NoRecords;
                    response.Message = ApiStatus.NoRecordsMessge;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        #endregion
    }
}
