﻿using System;
using System.Data.Entity.Validation;
using System.IO;

namespace Asi.Repository
{
    public static class LoggerRepsitory
    {
        #region Public Methods
        public static void LogError(Exception ex)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "logs//";
            string filename = DateTime.Now.ToString("dd.MM.yyyy") + ".resource";
            try
            {
                if (!Directory.Exists(path)) Directory.CreateDirectory(path);
                File.AppendAllText(path + filename, DateTime.Now + "\t" + ex.Message + "\n" + ex.StackTrace + "\n");
                if (ex.InnerException != null)
                {
                    File.AppendAllText(path + filename, DateTime.Now + "\t" + ex.InnerException + "\n");
                }
            }
            catch { }
        }

        public static void WriteErrorLog(Exception ex)
        {
            LogError(ex);
            //StreamWriter sw = null;
            //try
            //{
            //    string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\logs";
            //    if (!Directory.Exists(filepath)) Directory.CreateDirectory(filepath);

            //    sw = new StreamWriter(filepath + "\\LogFile.txt", true);
            //    sw.WriteLine(DateTime.Now.ToString("dd/MM/yyyy hh:mm tt") + ": " + ex.Source.ToString().Trim() + "; " + ex.Message.ToString().Trim() + ";" + ex.InnerException);
            //    sw.Flush();
            //    sw.Close();
            //}
            //catch(Exception exception) {
            //    LogError(exception);
            //}
        }

        public static void WriteErrorLog(string Message)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\logs\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString("dd/MM/yyyy hh:mm tt") + ": " + Message);
                sw.Flush();
                sw.Close();
            }
            catch { }
        }

        public static void WriteErrorLog(DbEntityValidationException ex)
        {
            string errorMessage = "";
            foreach (var eve in ex.EntityValidationErrors)
            {
                errorMessage += "Entity of type \"" + eve.Entry.Entity.GetType().Name + "\" in state \"" + eve.Entry.State + "\" has the following validation errors:";
                foreach (var ve in eve.ValidationErrors)
                {
                    errorMessage += "- Property: \"" + ve.PropertyName + "\", Error: \"" + ve.ErrorMessage + "\"";
                }
            }
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\logs\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString("dd/MM/yyyy hh:mm tt") + ": " + errorMessage);
                sw.Flush();
                sw.Close();
            }
            catch { }
        }
        #endregion
    }
}
