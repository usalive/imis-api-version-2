﻿using Asi.DAL;
using Asi.DAL.Model;
using Asi.DAL.ModelContext;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Asi.Repository
{
    public class RateCardDetailRepository : IRateCardDetailRepository
    {
        private readonly IDBEntities DB;
        public RateCardDetailRepository(IDBEntities dBEntities)
        {
            DB = dBEntities;
        }

        #region Public Methods
        public ResponseCommon GetRateCardDetails(int rateCardId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                var lstRateCardDetails = (from rc in DB.RateCards
                                          join rcd in DB.RateCardDetails on rc.RateCardId equals rcd.RateCardId into g
                                          from rcd in g.DefaultIfEmpty()
                                          where /*rc.DeletedInd != true && rcd.DeletedInd != true &&*/ rc.RateCardId == rateCardId
                                          select new
                                          {
                                              RateCardId = rc.RateCardId,
                                              RateCardName = rc.RateCardName,
                                              AdTypeId = rc.AdTypeId,
                                              AdTypeName = rc.AdType.AdTypeName,
                                              MediaAssetName = rc.MediaAsset == null ? "" : rc.MediaAsset.MediaAssetName,
                                              MediaAssetId = rc.MediaAssetId,
                                              AdColorId = rcd == null ? 0 : rcd.AdColorId,
                                              AdColorName = rcd == null || rcd.AdColor == null ? "" : rcd.AdColor.AdColorName,
                                              RateCardDetailId = rcd == null ? 0 : rcd.RateCardDetailId,
                                              AdSizeId = rcd == null ? 0 : rcd.AdSizeId,
                                              AdSizeName = rcd == null || rcd.AdSize == null ? "" : rcd.AdSize.AdSizeName,
                                              FrequencyId = rcd == null ? 0 : rcd.FrequencyId,
                                              FrequencyName = rcd == null || rcd.Frequency == null ? "" : rcd.Frequency.FrequencyName,
                                              RateCardCost = rcd == null ? 0 : rcd.RateCardCost
                                          }).ToList()
                                          .GroupBy(x => new { x.RateCardId, x.RateCardName, x.AdTypeId, x.AdTypeName, x.MediaAssetId, x.MediaAssetName })
                                          .Select(x => new GetRateCardDetailsByRateCardModel
                                          {
                                              RateCardId = x.Key.RateCardId,
                                              RateCardName = x.Key.RateCardName,

                                              AdTypeId = x.Key.AdTypeId,
                                              AdTypeName = x.Key.AdTypeName,

                                              MediaAssetName = x.Key.MediaAssetName,
                                              MediaAssetId = x.Key.MediaAssetId,
                                              RateCardDetails = x.GroupBy(y => new { y.AdColorId, y.AdColorName }).Where(y => y.Key.AdColorId != 0)
                                              .Select(y => new GetRateCardDetailsByRateCardColorDetails
                                              {
                                                  AdColorId = y.Key.AdColorId,
                                                  AdColorName = y.Key.AdColorName,

                                                  RateCardDetailCost = y.Select(z => new GetRateCardDetailsByRateCardCostModel
                                                  {
                                                      RateCardDetailId = z.RateCardDetailId,
                                                      AdSizeId = z.AdSizeId,
                                                      AdSizeName = z.AdSizeName,
                                                      FrequencyId = z.FrequencyId,
                                                      FrequencyName = z.FrequencyName,
                                                      RateCardCost = z.RateCardCost
                                                  })
                                              })
                                          }).ToList();

                response.Data = lstRateCardDetails;
                response.StatusCode = ApiStatus.Ok;
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon GetRateCardDetailById(int rateCardDetailId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                GetRateCardDetailByIdModel objRateCardDetail = DB.RateCardDetails.Where(x => /*x.DeletedInd != true &&*/ x.RateCardDetailId == rateCardDetailId)
                                                .Select(x => new GetRateCardDetailByIdModel
                                                {
                                                    RateCardDetailId = x.RateCardDetailId,
                                                    RateCardId = x.RateCardId,
                                                    RateCardName = x.RateCard.RateCardName,
                                                    AdTypeId = x.AdTypeId,
                                                    AdTypeName = x.AdType.AdTypeName,
                                                    AdSizeId = x.AdSizeId,
                                                    AdSizeName = x.AdSize.AdSizeName,
                                                    AdColorId = x.AdColorId,
                                                    AdColorName = x.AdColor.AdColorName,
                                                    FrequencyId = x.FrequencyId,
                                                    FrequencyName = x.Frequency.FrequencyName,
                                                    RateCardCost = x.RateCardCost,
                                                    MediaAssetName = x.RateCard.MediaAsset.MediaAssetName,
                                                    MediaAssetId = x.RateCard.MediaAssetId
                                                }).FirstOrDefault();
                if (objRateCardDetail != null)
                {
                    response.Data = objRateCardDetail;
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    response.Message = ApiStatus.NotFoundMessage;
                    response.StatusCode = ApiStatus.NotFound;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon GetRateCardDetailsByColor(int rateCardId, int adColorId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                List<GetRateCardDetailModel> lstRateCardDetails = DB.RateCards
                                                 .Where(x => /*x.DeletedInd != true &&*/ x.RateCardId == rateCardId)
                                                 .Select(x => new GetRateCardDetailModel
                                                 {
                                                     RateCardId = x.RateCardId,
                                                     RateCardName = x.RateCardName,

                                                     AdTypeId = x.AdTypeId,
                                                     AdTypeName = x.AdType.AdTypeName,

                                                     MediaAssetName = x.MediaAsset.MediaAssetName,
                                                     MediaAssetId = x.MediaAssetId,

                                                     RateCardDetailCost = x.RateCardDetails/*.Where(y => y.DeletedInd != true)*/
                                                     .Select(y => new GetRateCardDetailCostModel
                                                     {
                                                         AdColorId = y.AdColorId,
                                                         AdColorName = y.AdColor.AdColorName,

                                                         RateCardDetailId = y.RateCardDetailId,
                                                         AdSizeId = y.AdSizeId,
                                                         AdSizeName = y.AdSize.AdSizeName,

                                                         FrequencyId = y.FrequencyId,
                                                         FrequencyName = y.Frequency.FrequencyName,

                                                         RateCardCost = y.RateCardCost
                                                     }).Where(y => y.AdColorId == adColorId)
                                                 }).ToList();
                response.Data = lstRateCardDetails;
                response.StatusCode = ApiStatus.Ok;
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon GetRateCardsCost(int rateCardId, int adColorId, int adSizeId, int frequencyId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                var objRateCardCost = DB.RateCardDetails
                    .Where(x => /*x.DeletedInd != true && x.RateCard.DeletedInd != true &&*/
                    x.RateCardId == rateCardId 
                    && x.AdColorId == adColorId 
                    && x.AdSizeId == adSizeId 
                    && x.FrequencyId == frequencyId)
                    .Select(x => new { x.RateCardCost, x.RateCardDetailId }).FirstOrDefault();


                response.Data = objRateCardCost;
                response.StatusCode = ApiStatus.Ok;
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon GetRateCardsCostForPerWord(int rateCardId, int adColorId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                var rateCardCost = DB.RateCardDetails
                    .Where(x => /*x.DeletedInd != true && x.RateCard.DeletedInd != true &&*/
                    x.RateCardId == rateCardId
                    && x.AdColorId == adColorId)
                    .OrderBy(x => x.RateCardDetailId).Select(x => new
                    {
                        x.AdSize.AdSizeName,
                        x.Frequency.FrequencyName,
                        x.RateCardCost,
                        x.RateCardDetailId
                    }).Take(2).ToList();

                if (rateCardCost.Count < 2)
                {
                    response.StatusCode = ApiStatus.InvalidInput;
                    response.Message = ApiStatus.InvalidInputMessge;
                }
                else
                {
                    var objCostForWord = new
                    {
                        RateCardDetailId = rateCardCost.FirstOrDefault().RateCardDetailId,
                        NoOfWord = rateCardCost.FirstOrDefault().AdSizeName,
                        RateCardCost = rateCardCost.FirstOrDefault().RateCardCost,
                        AdditionalWord = rateCardCost.FirstOrDefault().FrequencyName,
                        PerWordCost = rateCardCost.Skip(1).FirstOrDefault().RateCardCost
                    };

                    response.Data = objCostForWord;
                    response.StatusCode = ApiStatus.Ok;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon GetMultipleRateCardsCost(int rateCardId, int adColorId, int adSizeId, int frequencyId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                List<GetBasicRateCardCost> objRateCardCost = new List<GetBasicRateCardCost>();
                objRateCardCost = DB.RateCardDetails
                   .Where(x => /*x.DeletedInd != true && x.RateCard.DeletedInd != true &&*/
                   x.RateCardId == rateCardId
                   && x.AdColorId == adColorId
                   && x.AdSizeId == adSizeId
                   && x.FrequencyId == frequencyId)
                   .Select(x => new GetBasicRateCardCost { RateCardCost = x.RateCardCost, RateCardDetailId = x.RateCardDetailId }).ToList();


                response.Data = objRateCardCost;
                response.StatusCode = ApiStatus.Ok;
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        public ResponseCommon GetMultipleRateCardsCostForPerWord(int rateCardId, int adColorId)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                List<GetMultipleRateCardsCostForPerWord> lstRateCardCost = new List<DAL.Model.GetMultipleRateCardsCostForPerWord>();
                var rateCardDetails = DB.RateCardDetails
                    .Where(x => /*x.DeletedInd != true && x.RateCard.DeletedInd != true &&*/ x.RateCardId == rateCardId && x.AdColorId == adColorId)
                    .OrderBy(x => x.RateCardDetailId).Select(x => new { x.AdSize.AdSizeName, x.Frequency.FrequencyName, x.RateCardCost, x.RateCardDetailId }).ToList();

                if (rateCardDetails.Count < 2)
                {
                    response.StatusCode = ApiStatus.InvalidInput;
                    response.Message = ApiStatus.InvalidInputMessge;
                }
                else
                {
                    for (int i = 0; i <= rateCardDetails.Count() - 1; i = i + 2)
                    {
                        var objCostForWord = new GetMultipleRateCardsCostForPerWord
                        {
                            RateCardDetailId = rateCardDetails[i].RateCardDetailId,
                            NoOfWord = rateCardDetails[i].AdSizeName.ToString(),
                            RateCardCost = rateCardDetails[i].RateCardCost.ToString(),
                            AdditionalWord = rateCardDetails[i].FrequencyName.ToString(),
                            PerWordCost = rateCardDetails[i + 1].RateCardCost.ToString()
                        };
                        lstRateCardCost.Add(objCostForWord);
                    }

                    response.Data = lstRateCardCost;
                    response.StatusCode = ApiStatus.Ok;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }

        public ResponseCommon PostRateCardDetail(PostRateCardDetailModel cardDetailModel)
        {
            ResponseCommon response = new ResponseCommon();
            try
            {
                List<RateCardDetail> lstRateCardDetailData = DB.RateCardDetails.Where(x => x.RateCardId == cardDetailModel.RateCardId && x.AdColorId == cardDetailModel.AdColorId
                /*&& x.DeletedInd != true*/).ToList();
                List<RateCardDetail> lstRateCardDetail = new List<RateCardDetail>();
                RateCardDetail objCardDetail = null;

                //New rate card details added to db
                if (lstRateCardDetailData.Count <= 0)
                {
                    foreach (var item in cardDetailModel.RateCardDetailCost)
                    {
                        objCardDetail = new RateCardDetail();
                        objCardDetail.AdColorId = cardDetailModel.AdColorId;
                        objCardDetail.AdTypeId = cardDetailModel.AdTypeId;
                        objCardDetail.AdSizeId = item.AdSizeId;
                        objCardDetail.CreatedOn = DateTime.Now;
                        //objCardDetail.DeletedInd = false;
                        objCardDetail.FrequencyId = item.FrequencyId;
                        objCardDetail.RateCardCost = item.RateCardCost;
                        objCardDetail.RateCardId = cardDetailModel.RateCardId;

                        //*
                        objCardDetail.CreatedOn = DateTime.Now;
                        objCardDetail.UpdatedOn = DateTime.Now;

                        objCardDetail.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                        objCardDetail.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                        lstRateCardDetail.Add(objCardDetail);
                    }
                    DB.RateCardDetails.AddRange(lstRateCardDetail);
                    DB.SaveChanges();
                    //response.data = objRateCardDetail.RateCardDetailId;
                    response.StatusCode = ApiStatus.Ok;
                }
                else
                {
                    //Remove Existing rate card which isnnot in request details added to db
                    var lstRateCardDetailDelete = lstRateCardDetailData.Where(x => !(cardDetailModel.RateCardDetailCost.Any(y => y.AdSizeId == x.AdSizeId
                    && y.FrequencyId == x.FrequencyId && y.RateCardDetailId != 0))
                    /*&& x.DeletedInd == false*/).ToList();
                    foreach (var item in lstRateCardDetailDelete)
                    {
                        //item.DeletedInd = true;
                        item.UpdatedOn = DateTime.Now;

                        //*
                        item.UpdatedByUserKey = Constants.Created_Update_ByUserKey;
                    }
                    foreach (var item in cardDetailModel.RateCardDetailCost)
                    {
                        var objRateCardDetailEdit = lstRateCardDetailData.Where(x => x.AdSizeId == item.AdSizeId && x.FrequencyId == item.FrequencyId /*&& x.DeletedInd != true*/).FirstOrDefault();
                        //New rate card details added to db
                        if (item.RateCardDetailId == 0 && objRateCardDetailEdit == null)
                        {
                            objCardDetail = new RateCardDetail();
                            objCardDetail.AdColorId = cardDetailModel.AdColorId;
                            objCardDetail.AdTypeId = cardDetailModel.AdTypeId;
                            objCardDetail.AdSizeId = item.AdSizeId;
                            objCardDetail.CreatedOn = DateTime.Now;
                            //objCardDetail.DeletedInd = false;
                            objCardDetail.FrequencyId = item.FrequencyId;
                            objCardDetail.RateCardCost = item.RateCardCost;
                            objCardDetail.RateCardId = cardDetailModel.RateCardId;

                            //*
                            objCardDetail.UpdatedOn = DateTime.Now;
                            objCardDetail.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                            objCardDetail.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                            lstRateCardDetail.Add(objCardDetail);
                        }
                        else
                        {
                            //New rate card details added to db
                            if (objRateCardDetailEdit == null)
                            {
                                objCardDetail = new RateCardDetail();
                                objCardDetail.AdColorId = cardDetailModel.AdColorId;
                                objCardDetail.AdTypeId = cardDetailModel.AdTypeId;
                                objCardDetail.AdSizeId = item.AdSizeId;
                                objCardDetail.CreatedOn = DateTime.Now;
                                //objCardDetail.DeletedInd = false;
                                objCardDetail.FrequencyId = item.FrequencyId;
                                objCardDetail.RateCardCost = item.RateCardCost;
                                objCardDetail.RateCardId = cardDetailModel.RateCardId;

                                //*
                                objCardDetail.UpdatedOn = DateTime.Now;
                                objCardDetail.CreatedByUserKey = Constants.Created_Update_ByUserKey;
                                objCardDetail.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                lstRateCardDetail.Add(objCardDetail);
                            }
                            else
                            {
                                //Edit Existing rate card details added to db
                                objRateCardDetailEdit.AdColorId = cardDetailModel.AdColorId;
                                objRateCardDetailEdit.AdTypeId = cardDetailModel.AdTypeId;
                                objRateCardDetailEdit.AdSizeId = item.AdSizeId;
                                objRateCardDetailEdit.UpdatedOn = DateTime.Now;
                                objRateCardDetailEdit.FrequencyId = item.FrequencyId;
                                objRateCardDetailEdit.RateCardCost = item.RateCardCost;
                                objRateCardDetailEdit.RateCardId = cardDetailModel.RateCardId;

                                //*                                
                                objRateCardDetailEdit.UpdatedByUserKey = Constants.Created_Update_ByUserKey;

                                DB.Entry(objRateCardDetailEdit).State = EntityState.Modified;
                            }
                        }
                    }
                    if (lstRateCardDetail.Count > 0)
                        DB.RateCardDetails.AddRange(lstRateCardDetail);
                    DB.SaveChanges();
                    response.StatusCode = ApiStatus.Ok;
                }
            }
            catch (Exception ex)
            {
                LoggerRepsitory.WriteErrorLog(ex);
                response.StatusCode = ApiStatus.Exception;
                response.Message = ApiStatus.ExceptionMessge;
            }
            return response;
        }
        #endregion
    }
}
