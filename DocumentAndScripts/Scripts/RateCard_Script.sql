IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCardDetail_RateCard]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[RateCardDetail]') )
    ALTER TABLE [dbo].[RateCardDetail] DROP CONSTRAINT [FK_RateCardDetail_RateCard]
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCardDetail_AdType]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[RateCardDetail]') )
    ALTER TABLE [dbo].[RateCardDetail] DROP CONSTRAINT [FK_RateCardDetail_AdType]
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCardDetail_AdSize]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[RateCardDetail]') )
    ALTER TABLE [dbo].[RateCardDetail] DROP CONSTRAINT [FK_RateCardDetail_AdSize]
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCardDetail_AdColor]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[RateCardDetail]') )
    ALTER TABLE [dbo].[RateCardDetail] DROP CONSTRAINT [FK_RateCardDetail_AdColor]
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCard_MediaType]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[RateCard]') )
    ALTER TABLE [dbo].[RateCard] DROP CONSTRAINT [FK_RateCard_MediaType]
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCard_MediaBillingMethod]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[RateCard]') )
    ALTER TABLE [dbo].[RateCard] DROP CONSTRAINT [FK_RateCard_MediaBillingMethod]
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCard_MediaAsset]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[RateCard]') )
    ALTER TABLE [dbo].[RateCard] DROP CONSTRAINT [FK_RateCard_MediaAsset]
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCard_AdType]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[RateCard]') )
    ALTER TABLE [dbo].[RateCard] DROP CONSTRAINT [FK_RateCard_AdType]
GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_RateCardDetail_CreatedDate]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[RateCardDetail] DROP CONSTRAINT [DF_RateCardDetail_CreatedDate]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_RateCardDetail_DeletedInd]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[RateCardDetail] DROP CONSTRAINT [DF_RateCardDetail_DeletedInd]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_RateCard_CreatedDate]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[RateCard] DROP CONSTRAINT [DF_RateCard_CreatedDate]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_RateCard_DeletedInd]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[RateCard] DROP CONSTRAINT [DF_RateCard_DeletedInd]
    END

GO
/****** Object:  Table [dbo].[RateCardDetail]    Script Date: 11/23/2017 3:27:34 PM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[RateCardDetail]')
                    AND type IN ( N'U' ) )
    DROP TABLE [dbo].[RateCardDetail]
GO
/****** Object:  Table [dbo].[RateCard]    Script Date: 11/23/2017 3:27:34 PM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[RateCard]')
                    AND type IN ( N'U' ) )
    DROP TABLE [dbo].[RateCard]
GO

IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[dbo].[RateCard]')
                        AND type IN ( N'U' ) )
    BEGIN
        CREATE TABLE [dbo].[RateCard]
            (
              [RateCardId] [INT] IDENTITY(1, 1)
                                 NOT NULL ,
              [MediaAssetId] [INT] NOT NULL ,
              [MediaTypeId] [INT] NOT NULL ,
              [RateCardName] [NVARCHAR](255) NOT NULL ,
              [AdTypeId] [INT] NOT NULL ,
              [MediaBillingMethodId] [INT] NOT NULL ,
              [ValidFromDate] [DATE] NOT NULL ,
              [ValidToDate] [DATE] NOT NULL ,
              [ProductCode] [VARCHAR](50) NOT NULL ,
              [DeletedInd] [BIT] NOT NULL ,
              [CreatedDate] [DATETIME] NOT NULL ,
              [CreatedBy] [INT] NULL ,
              [ModifiedDate] [DATETIME] NULL ,
              [ModifiedBy] [INT] NULL ,
              CONSTRAINT [PK_RateCard] PRIMARY KEY CLUSTERED
                ( [RateCardId] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]
    END
GO

IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[dbo].[RateCardDetail]')
                        AND type IN ( N'U' ) )
    BEGIN
        CREATE TABLE [dbo].[RateCardDetail]
            (
              [RateCardDetailId] [INT] IDENTITY(1, 1)
                                       NOT NULL ,
              [RateCardId] [INT] NOT NULL ,
              [AdTypeId] [INT] NOT NULL ,
              [AdSizeId] [INT] NOT NULL ,
              [AdColorId] [INT] NOT NULL ,
              [Frequency] [INT] NOT NULL ,
              [RateCardCost] [SMALLMONEY] NOT NULL ,
              [DeletedInd] [BIT] NOT NULL ,
              [CreatedDate] [DATETIME] NOT NULL ,
              [CreatedBy] [INT] NULL ,
              [ModifiedDate] [DATETIME] NULL ,
              [ModifiedBy] [INT] NULL ,
              CONSTRAINT [PK_RateCardDetail] PRIMARY KEY CLUSTERED
                ( [RateCardDetailId] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]
    END
GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_RateCard_DeletedInd]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[RateCard] ADD  CONSTRAINT [DF_RateCard_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_RateCard_CreatedDate]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[RateCard] ADD  CONSTRAINT [DF_RateCard_CreatedDate]  DEFAULT (GETDATE()) FOR [CreatedDate]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_RateCardDetail_DeletedInd]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[RateCardDetail] ADD  CONSTRAINT [DF_RateCardDetail_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_RateCardDetail_CreatedDate]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[RateCardDetail] ADD  CONSTRAINT [DF_RateCardDetail_CreatedDate]  DEFAULT (GETDATE()) FOR [CreatedDate]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.foreign_keys
                WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCard_AdType]')
                        AND parent_object_id = OBJECT_ID(N'[dbo].[RateCard]') )
    ALTER TABLE [dbo].[RateCard]  WITH CHECK ADD  CONSTRAINT [FK_RateCard_AdType] FOREIGN KEY([AdTypeId])
    REFERENCES [dbo].[AdType] ([AdTypeId])
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCard_AdType]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[RateCard]') )
    ALTER TABLE [dbo].[RateCard] CHECK CONSTRAINT [FK_RateCard_AdType]
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.foreign_keys
                WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCard_MediaAsset]')
                        AND parent_object_id = OBJECT_ID(N'[dbo].[RateCard]') )
    ALTER TABLE [dbo].[RateCard]  WITH CHECK ADD  CONSTRAINT [FK_RateCard_MediaAsset] FOREIGN KEY([MediaAssetId])
    REFERENCES [dbo].[MediaAsset] ([MediaAssetId])
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCard_MediaAsset]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[RateCard]') )
    ALTER TABLE [dbo].[RateCard] CHECK CONSTRAINT [FK_RateCard_MediaAsset]
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.foreign_keys
                WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCard_MediaBillingMethod]')
                        AND parent_object_id = OBJECT_ID(N'[dbo].[RateCard]') )
    ALTER TABLE [dbo].[RateCard]  WITH CHECK ADD  CONSTRAINT [FK_RateCard_MediaBillingMethod] FOREIGN KEY([MediaBillingMethodId])
    REFERENCES [dbo].[MediaBillingMethod] ([MediaBillingMethodId])
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCard_MediaBillingMethod]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[RateCard]') )
    ALTER TABLE [dbo].[RateCard] CHECK CONSTRAINT [FK_RateCard_MediaBillingMethod]
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.foreign_keys
                WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCard_MediaType]')
                        AND parent_object_id = OBJECT_ID(N'[dbo].[RateCard]') )
    ALTER TABLE [dbo].[RateCard]  WITH CHECK ADD  CONSTRAINT [FK_RateCard_MediaType] FOREIGN KEY([MediaTypeId])
    REFERENCES [dbo].[MediaType] ([MediaTypeId])
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCard_MediaType]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[RateCard]') )
    ALTER TABLE [dbo].[RateCard] CHECK CONSTRAINT [FK_RateCard_MediaType]
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.foreign_keys
                WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCardDetail_AdColor]')
                        AND parent_object_id = OBJECT_ID(N'[dbo].[RateCardDetail]') )
    ALTER TABLE [dbo].[RateCardDetail]  WITH CHECK ADD  CONSTRAINT [FK_RateCardDetail_AdColor] FOREIGN KEY([AdColorId])
    REFERENCES [dbo].[AdColor] ([AdColorId])
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCardDetail_AdColor]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[RateCardDetail]') )
    ALTER TABLE [dbo].[RateCardDetail] CHECK CONSTRAINT [FK_RateCardDetail_AdColor]
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.foreign_keys
                WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCardDetail_AdSize]')
                        AND parent_object_id = OBJECT_ID(N'[dbo].[RateCardDetail]') )
    ALTER TABLE [dbo].[RateCardDetail]  WITH CHECK ADD  CONSTRAINT [FK_RateCardDetail_AdSize] FOREIGN KEY([AdSizeId])
    REFERENCES [dbo].[AdSize] ([AdSizeId])
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCardDetail_AdSize]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[RateCardDetail]') )
    ALTER TABLE [dbo].[RateCardDetail] CHECK CONSTRAINT [FK_RateCardDetail_AdSize]
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.foreign_keys
                WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCardDetail_AdType]')
                        AND parent_object_id = OBJECT_ID(N'[dbo].[RateCardDetail]') )
    ALTER TABLE [dbo].[RateCardDetail]  WITH CHECK ADD  CONSTRAINT [FK_RateCardDetail_AdType] FOREIGN KEY([AdTypeId])
    REFERENCES [dbo].[AdType] ([AdTypeId])
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCardDetail_AdType]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[RateCardDetail]') )
    ALTER TABLE [dbo].[RateCardDetail] CHECK CONSTRAINT [FK_RateCardDetail_AdType]
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.foreign_keys
                WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCardDetail_RateCard]')
                        AND parent_object_id = OBJECT_ID(N'[dbo].[RateCardDetail]') )
    ALTER TABLE [dbo].[RateCardDetail]  WITH CHECK ADD  CONSTRAINT [FK_RateCardDetail_RateCard] FOREIGN KEY([RateCardId])
    REFERENCES [dbo].[RateCard] ([RateCardId])
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RateCardDetail_RateCard]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[RateCardDetail]') )
    ALTER TABLE [dbo].[RateCardDetail] CHECK CONSTRAINT [FK_RateCardDetail_RateCard]
GO
