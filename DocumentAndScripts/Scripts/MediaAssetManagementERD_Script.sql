IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaAsset_MediaType]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[MediaAsset]') )
    ALTER TABLE [dbo].[MediaAsset] DROP CONSTRAINT [FK_MediaAsset_MediaType]
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaAsset_MediaAssetGroup]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[MediaAsset]') )
    ALTER TABLE [dbo].[MediaAsset] DROP CONSTRAINT [FK_MediaAsset_MediaAssetGroup]
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_IssueDate_MediaAsset]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[IssueDate]') )
    ALTER TABLE [dbo].[IssueDate] DROP CONSTRAINT [FK_IssueDate_MediaAsset]
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_AdSize_MediaAsset]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[AdSize]') )
    ALTER TABLE [dbo].[AdSize] DROP CONSTRAINT [FK_AdSize_MediaAsset]
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_AdAdjustment_MediaAsset]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[AdAdjustment]') )
    ALTER TABLE [dbo].[AdAdjustment] DROP CONSTRAINT [FK_AdAdjustment_MediaAsset]
GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaType_CreatedDate]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaType] DROP CONSTRAINT [DF_MediaType_CreatedDate]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaType_DeletedInd]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaType] DROP CONSTRAINT [DF_MediaType_DeletedInd]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_MrdiaAssetGroup_CreatedDate]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaAssetGroup] DROP CONSTRAINT [DF_MrdiaAssetGroup_CreatedDate]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_MrdiaAssetGroup_DeletedInd]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaAssetGroup] DROP CONSTRAINT [DF_MrdiaAssetGroup_DeletedInd]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaAsset_CreatedDate]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaAsset] DROP CONSTRAINT [DF_MediaAsset_CreatedDate]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaAsset_DeletedInd]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaAsset] DROP CONSTRAINT [DF_MediaAsset_DeletedInd]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_IssueDate_CreatedDate]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[IssueDate] DROP CONSTRAINT [DF_IssueDate_CreatedDate]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_IssueDate_DeletedInd]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[IssueDate] DROP CONSTRAINT [DF_IssueDate_DeletedInd]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_AdType_CreatedDate]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[AdType] DROP CONSTRAINT [DF_AdType_CreatedDate]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_AdType_DeletedInd]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[AdType] DROP CONSTRAINT [DF_AdType_DeletedInd]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_AdSize_CreatedDate]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[AdSize] DROP CONSTRAINT [DF_AdSize_CreatedDate]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_AdSize_DeletedInd]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[AdSize] DROP CONSTRAINT [DF_AdSize_DeletedInd]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_AdColor_CreatedDate]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[AdColor] DROP CONSTRAINT [DF_AdColor_CreatedDate]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_AdColor_DeletedInd]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[AdColor] DROP CONSTRAINT [DF_AdColor_DeletedInd]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_AdAdjustment_CreatedDate]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[AdAdjustment] DROP CONSTRAINT [DF_AdAdjustment_CreatedDate]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_AdAdjustment_DeletedInd]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[AdAdjustment] DROP CONSTRAINT [DF_AdAdjustment_DeletedInd]
    END

GO
/****** Object:  Table [dbo].[MediaType]    Script Date: 11/8/2017 4:46:59 PM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[MediaType]')
                    AND type IN ( N'U' ) )
    DROP TABLE [dbo].[MediaType]
GO
/****** Object:  Table [dbo].[MediaAssetGroup]    Script Date: 11/8/2017 4:46:59 PM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[MediaAssetGroup]')
                    AND type IN ( N'U' ) )
    DROP TABLE [dbo].[MediaAssetGroup]
GO
/****** Object:  Table [dbo].[MediaAsset]    Script Date: 11/8/2017 4:46:59 PM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[MediaAsset]')
                    AND type IN ( N'U' ) )
    DROP TABLE [dbo].[MediaAsset]
GO
/****** Object:  Table [dbo].[IssueDate]    Script Date: 11/8/2017 4:46:59 PM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[IssueDate]')
                    AND type IN ( N'U' ) )
    DROP TABLE [dbo].[IssueDate]
GO
/****** Object:  Table [dbo].[AdType]    Script Date: 11/8/2017 4:46:59 PM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[AdType]')
                    AND type IN ( N'U' ) )
    DROP TABLE [dbo].[AdType]
GO
/****** Object:  Table [dbo].[AdSize]    Script Date: 11/8/2017 4:46:59 PM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[AdSize]')
                    AND type IN ( N'U' ) )
    DROP TABLE [dbo].[AdSize]
GO
/****** Object:  Table [dbo].[AdColor]    Script Date: 11/8/2017 4:46:59 PM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[AdColor]')
                    AND type IN ( N'U' ) )
    DROP TABLE [dbo].[AdColor]
GO
/****** Object:  Table [dbo].[AdAdjustment]    Script Date: 11/8/2017 4:46:59 PM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[AdAdjustment]')
                    AND type IN ( N'U' ) )
    DROP TABLE [dbo].[AdAdjustment]
GO
/****** Object:  Table [dbo].[AdAdjustment]    Script Date: 11/8/2017 4:46:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[dbo].[AdAdjustment]')
                        AND type IN ( N'U' ) )
    BEGIN
        CREATE TABLE [dbo].[AdAdjustment]
            (
              [AdAdjustmentId] [INT] IDENTITY(1, 1)
                                     NOT NULL ,
              [AdAdjustmentName] [NVARCHAR](255) NOT NULL ,
              [MediaAssetId] [INT] NOT NULL ,
              [AmountPercent] [SMALLINT] NOT NULL ,
              [SurchargeDiscount] [SMALLINT] NOT NULL ,
              [AdjustmentTarget] [NVARCHAR](5) NOT NULL ,
              [AdjustmentValue] [FLOAT] NOT NULL ,
              [ProductCode] [NVARCHAR](50) NOT NULL ,
              [DeletedInd] [BIT] NOT NULL ,
              [CreatedDate] [DATETIME] NOT NULL ,
              [CreatedBy] [INT] NULL ,
              [ModifiedDate] [DATETIME] NULL ,
              [ModifiedBy] [INT] NULL ,
              CONSTRAINT [PK_AdAdjustment] PRIMARY KEY CLUSTERED
                ( [AdAdjustmentId] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]
    END
GO
/****** Object:  Table [dbo].[AdColor]    Script Date: 11/8/2017 4:46:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[dbo].[AdColor]')
                        AND type IN ( N'U' ) )
    BEGIN
        CREATE TABLE [dbo].[AdColor]
            (
              [AdColorId] [INT] IDENTITY(1, 1)
                                NOT NULL ,
              [AdColorName] [NVARCHAR](255) NOT NULL ,
              [DeletedInd] [BIT] NOT NULL ,
              [CreatedDate] [DATETIME] NOT NULL ,
              [CreatedBy] [INT] NULL ,
              [ModifiedDate] [DATETIME] NULL ,
              [ModifiedBy] [INT] NULL ,
              CONSTRAINT [PK_AdColor] PRIMARY KEY CLUSTERED
                ( [AdColorId] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]
    END
GO
/****** Object:  Table [dbo].[AdSize]    Script Date: 11/8/2017 4:46:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[dbo].[AdSize]')
                        AND type IN ( N'U' ) )
    BEGIN
        CREATE TABLE [dbo].[AdSize]
            (
              [AdSizeId] [INT] IDENTITY(1, 1)
                               NOT NULL ,
              [AdSizeName] [NVARCHAR](255) NOT NULL ,
              [PageFraction] [FLOAT] NOT NULL ,
              [MediaAssetId] [INT] NOT NULL ,
              [DeletedInd] [BIT] NOT NULL ,
              [CreatedDate] [DATETIME] NOT NULL ,
              [CreatedBy] [INT] NULL ,
              [ModifiedDate] [DATETIME] NULL ,
              [ModifiedBy] [INT] NULL ,
              CONSTRAINT [PK_AdSize] PRIMARY KEY CLUSTERED ( [AdSizeId] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]
    END
GO
/****** Object:  Table [dbo].[AdType]    Script Date: 11/8/2017 4:46:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[dbo].[AdType]')
                        AND type IN ( N'U' ) )
    BEGIN
        CREATE TABLE [dbo].[AdType]
            (
              [AdTypeId] [INT] IDENTITY(1, 1)
                               NOT NULL ,
              [AdTypeName] [NVARCHAR](255) NOT NULL ,
              [DeletedInd] [BIT] NOT NULL ,
              [CreatedDate] [DATETIME] NOT NULL ,
              [CreatedBy] [INT] NULL ,
              [ModifiedDate] [DATETIME] NULL ,
              [ModifiedBy] [INT] NULL ,
              CONSTRAINT [PK_AdType] PRIMARY KEY CLUSTERED ( [AdTypeId] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]
    END
GO
/****** Object:  Table [dbo].[IssueDate]    Script Date: 11/8/2017 4:46:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[dbo].[IssueDate]')
                        AND type IN ( N'U' ) )
    BEGIN
        CREATE TABLE [dbo].[IssueDate]
            (
              [IssueDateId] [INT] IDENTITY(1, 1)
                                  NOT NULL ,
              [MediaAssetId] [INT] NOT NULL ,
              [CoverDate] [DATE] NOT NULL ,
              [IssueCode] [VARCHAR](25) NOT NULL ,
              [IssueName] [VARCHAR](50) NOT NULL ,
              [AdClosingDate] [DATE] NOT NULL ,
              [MaterialDueDate] [DATE] NOT NULL ,
              [DeletedInd] [BIT] NOT NULL ,
              [CreatedDate] [DATETIME] NOT NULL ,
              [CreatedBy] [INT] NULL ,
              [ModifiedDate] [DATETIME] NULL ,
              [ModifiedBy] [INT] NULL ,
              CONSTRAINT [PK_IssueDate] PRIMARY KEY CLUSTERED
                ( [IssueDateId] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]
    END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[MediaAsset]    Script Date: 11/8/2017 4:46:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[dbo].[MediaAsset]')
                        AND type IN ( N'U' ) )
    BEGIN
        CREATE TABLE [dbo].[MediaAsset]
            (
              [MediaAssetId] [INT] IDENTITY(1, 1)
                                   NOT NULL ,
              [MediaAssetName] [NVARCHAR](255) NOT NULL ,
              [MediaAssetGroupId] [INT] NOT NULL ,
              [MediaTypeId] [INT] NOT NULL ,
              [Logo] [NVARCHAR](255) NULL ,
              [MediaCode] [NVARCHAR](25) NOT NULL ,
              [ProductCode] [NVARCHAR](50) NOT NULL ,
              [DeletedInd] [BIT] NOT NULL ,
              [CreatedDate] [DATETIME] NOT NULL ,
              [CreatedBy] [INT] NULL ,
              [ModifiedDate] [DATETIME] NULL ,
              [ModifiedBy] [INT] NULL ,
              CONSTRAINT [PK_MediaAsset] PRIMARY KEY CLUSTERED
                ( [MediaAssetId] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]
    END
GO
/****** Object:  Table [dbo].[MediaAssetGroup]    Script Date: 11/8/2017 4:46:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[dbo].[MediaAssetGroup]')
                        AND type IN ( N'U' ) )
    BEGIN
        CREATE TABLE [dbo].[MediaAssetGroup]
            (
              [MediaAssetGroupId] [INT] IDENTITY(1, 1)
                                        NOT NULL ,
              [MediaAssetGroupName] [NVARCHAR](255) NOT NULL ,
              [MediaAssetGroupParentGroupId] [INT] NULL ,
              [DeletedInd] [BIT] NOT NULL ,
              [CreatedDate] [DATETIME] NOT NULL ,
              [CreatedBy] [INT] NULL ,
              [ModifiedDate] [DATETIME] NULL ,
              [ModifiedBy] [INT] NULL ,
              CONSTRAINT [PK_MrdiaAssetGroup] PRIMARY KEY CLUSTERED
                ( [MediaAssetGroupId] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]
    END
GO
/****** Object:  Table [dbo].[MediaType]    Script Date: 11/8/2017 4:46:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[dbo].[MediaType]')
                        AND type IN ( N'U' ) )
    BEGIN
        CREATE TABLE [dbo].[MediaType]
            (
              [MediaTypeId] [INT] IDENTITY(1, 1)
                                  NOT NULL ,
              [MediaTypeName] [NVARCHAR](255) NOT NULL ,
              [DeletedInd] [BIT] NOT NULL ,
              [CreatedDate] [DATETIME] NOT NULL ,
              [CreatedBy] [INT] NULL ,
              [ModifiedDate] [DATETIME] NULL ,
              [ModifiedBy] [INT] NULL ,
              CONSTRAINT [PK_MediaType] PRIMARY KEY CLUSTERED
                ( [MediaTypeId] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]
    END
GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_AdAdjustment_DeletedInd]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[AdAdjustment] ADD  CONSTRAINT [DF_AdAdjustment_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_AdAdjustment_CreatedDate]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[AdAdjustment] ADD  CONSTRAINT [DF_AdAdjustment_CreatedDate]  DEFAULT (GETDATE()) FOR [CreatedDate]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_AdColor_DeletedInd]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[AdColor] ADD  CONSTRAINT [DF_AdColor_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_AdColor_CreatedDate]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[AdColor] ADD  CONSTRAINT [DF_AdColor_CreatedDate]  DEFAULT (GETDATE()) FOR [CreatedDate]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_AdSize_DeletedInd]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[AdSize] ADD  CONSTRAINT [DF_AdSize_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_AdSize_CreatedDate]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[AdSize] ADD  CONSTRAINT [DF_AdSize_CreatedDate]  DEFAULT (GETDATE()) FOR [CreatedDate]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_AdType_DeletedInd]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[AdType] ADD  CONSTRAINT [DF_AdType_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_AdType_CreatedDate]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[AdType] ADD  CONSTRAINT [DF_AdType_CreatedDate]  DEFAULT (GETDATE()) FOR [CreatedDate]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_IssueDate_DeletedInd]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[IssueDate] ADD  CONSTRAINT [DF_IssueDate_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_IssueDate_CreatedDate]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[IssueDate] ADD  CONSTRAINT [DF_IssueDate_CreatedDate]  DEFAULT (GETDATE()) FOR [CreatedDate]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaAsset_DeletedInd]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaAsset] ADD  CONSTRAINT [DF_MediaAsset_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaAsset_CreatedDate]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaAsset] ADD  CONSTRAINT [DF_MediaAsset_CreatedDate]  DEFAULT (GETDATE()) FOR [CreatedDate]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_MrdiaAssetGroup_DeletedInd]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaAssetGroup] ADD  CONSTRAINT [DF_MrdiaAssetGroup_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_MrdiaAssetGroup_CreatedDate]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaAssetGroup] ADD  CONSTRAINT [DF_MrdiaAssetGroup_CreatedDate]  DEFAULT (GETDATE()) FOR [CreatedDate]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaType_DeletedInd]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaType] ADD  CONSTRAINT [DF_MediaType_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaType_CreatedDate]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaType] ADD  CONSTRAINT [DF_MediaType_CreatedDate]  DEFAULT (GETDATE()) FOR [CreatedDate]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.foreign_keys
                WHERE   object_id = OBJECT_ID(N'[dbo].[FK_AdAdjustment_MediaAsset]')
                        AND parent_object_id = OBJECT_ID(N'[dbo].[AdAdjustment]') )
    ALTER TABLE [dbo].[AdAdjustment]  WITH CHECK ADD  CONSTRAINT [FK_AdAdjustment_MediaAsset] FOREIGN KEY([MediaAssetId])
    REFERENCES [dbo].[MediaAsset] ([MediaAssetId])
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_AdAdjustment_MediaAsset]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[AdAdjustment]') )
    ALTER TABLE [dbo].[AdAdjustment] CHECK CONSTRAINT [FK_AdAdjustment_MediaAsset]
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.foreign_keys
                WHERE   object_id = OBJECT_ID(N'[dbo].[FK_AdSize_MediaAsset]')
                        AND parent_object_id = OBJECT_ID(N'[dbo].[AdSize]') )
    ALTER TABLE [dbo].[AdSize]  WITH CHECK ADD  CONSTRAINT [FK_AdSize_MediaAsset] FOREIGN KEY([MediaAssetId])
    REFERENCES [dbo].[MediaAsset] ([MediaAssetId])
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_AdSize_MediaAsset]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[AdSize]') )
    ALTER TABLE [dbo].[AdSize] CHECK CONSTRAINT [FK_AdSize_MediaAsset]
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.foreign_keys
                WHERE   object_id = OBJECT_ID(N'[dbo].[FK_IssueDate_MediaAsset]')
                        AND parent_object_id = OBJECT_ID(N'[dbo].[IssueDate]') )
    ALTER TABLE [dbo].[IssueDate]  WITH CHECK ADD  CONSTRAINT [FK_IssueDate_MediaAsset] FOREIGN KEY([MediaAssetId])
    REFERENCES [dbo].[MediaAsset] ([MediaAssetId])
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_IssueDate_MediaAsset]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[IssueDate]') )
    ALTER TABLE [dbo].[IssueDate] CHECK CONSTRAINT [FK_IssueDate_MediaAsset]
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.foreign_keys
                WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaAsset_MediaAssetGroup]')
                        AND parent_object_id = OBJECT_ID(N'[dbo].[MediaAsset]') )
    ALTER TABLE [dbo].[MediaAsset]  WITH CHECK ADD  CONSTRAINT [FK_MediaAsset_MediaAssetGroup] FOREIGN KEY([MediaAssetGroupId])
    REFERENCES [dbo].[MediaAssetGroup] ([MediaAssetGroupId])
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaAsset_MediaAssetGroup]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[MediaAsset]') )
    ALTER TABLE [dbo].[MediaAsset] CHECK CONSTRAINT [FK_MediaAsset_MediaAssetGroup]
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.foreign_keys
                WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaAsset_MediaType]')
                        AND parent_object_id = OBJECT_ID(N'[dbo].[MediaAsset]') )
    ALTER TABLE [dbo].[MediaAsset]  WITH CHECK ADD  CONSTRAINT [FK_MediaAsset_MediaType] FOREIGN KEY([MediaTypeId])
    REFERENCES [dbo].[MediaType] ([MediaTypeId])
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaAsset_MediaType]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[MediaAsset]') )
    ALTER TABLE [dbo].[MediaAsset] CHECK CONSTRAINT [FK_MediaAsset_MediaType]
GO

----------------------------------------------------------------------

IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_AdAdjustmentTarget_CreatedDate]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[AdAdjustmentTarget] DROP CONSTRAINT [DF_AdAdjustmentTarget_CreatedDate]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_AdAdjustmentTarget_DeletedInd]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[AdAdjustmentTarget] DROP CONSTRAINT [DF_AdAdjustmentTarget_DeletedInd]
    END

GO
/****** Object:  Table [dbo].[AdAdjustmentTarget]    Script Date: 11/15/2017 4:42:00 PM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[AdAdjustmentTarget]')
                    AND type IN ( N'U' ) )
    DROP TABLE [dbo].[AdAdjustmentTarget]
GO

IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[dbo].[AdAdjustmentTarget]')
                        AND type IN ( N'U' ) )
    BEGIN
        CREATE TABLE [dbo].[AdAdjustmentTarget]
            (
              [AdAdjustmentTargetId] [INT] IDENTITY(1, 1)
                                           NOT NULL ,
              [AdAdjustmentTargetName] [NVARCHAR](50) NOT NULL ,
              [DeletedInd] [BIT] NOT NULL ,
              [CreatedDate] [DATETIME] NOT NULL ,
              [CreatedBy] [INT] NULL ,
              [ModifiedDate] [DATETIME] NULL ,
              [ModifiedBy] [INT] NULL ,
              CONSTRAINT [PK_AdAdjustmentTarget] PRIMARY KEY CLUSTERED
                ( [AdAdjustmentTargetId] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]
    END
GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_AdAdjustmentTarget_DeletedInd]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[AdAdjustmentTarget] ADD  CONSTRAINT [DF_AdAdjustmentTarget_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_AdAdjustmentTarget_CreatedDate]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[AdAdjustmentTarget] ADD  CONSTRAINT [DF_AdAdjustmentTarget_CreatedDate]  DEFAULT (GETDATE()) FOR [CreatedDate]
    END

GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[Frequency]')
                    AND type IN ( N'U' ) )
    DROP TABLE [dbo].[Frequency]
GO

CREATE TABLE [dbo].[Frequency](
	[FrequencyId] [int] IDENTITY(1,1) NOT NULL,
	[FrequencyName] [nvarchar](255) NULL,
	[DeletedInd] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_Frequency] PRIMARY KEY CLUSTERED 
(
	[FrequencyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Frequency] ADD  CONSTRAINT [DF_Frequency_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
GO

ALTER TABLE [dbo].[Frequency] ADD  CONSTRAINT [DF_Frequency_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO


-----------------------------------------------
-------------------- script updates ------------
-----------------------------------------------
ALTER TABLE dbo.IssueDate ALTER COLUMN IssueName NVARCHAR(50) NULL
ALTER TABLE dbo.IssueDate ALTER COLUMN IssueCode NVARCHAR(25) NULL
GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[MediaBillingMethod]')
                    AND type IN ( N'U' ) )
    DROP TABLE [dbo].[MediaBillingMethod]
GO

CREATE TABLE [dbo].[MediaBillingMethod](
	[MediaBillingMethodId] [INT] IDENTITY(1,1) NOT NULL,
	[MediaBillingMethodName] [NVARCHAR](255) NOT NULL,
	[DeletedInd] [BIT] NOT NULL,
	[CreatedDate] [DATETIME] NOT NULL,
	[CreatedBy] [INT] NULL,
	[ModifiedDate] [DATETIME] NULL,
	[ModifiedBy] [INT] NULL,
 CONSTRAINT [PK_MediaBillingMethod] PRIMARY KEY CLUSTERED 
(
	[MediaBillingMethodId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[MediaBillingMethod] ADD  CONSTRAINT [DF_MediaBillingMethod_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
GO

ALTER TABLE [dbo].[MediaBillingMethod] ADD  CONSTRAINT [DF_MediaBillingMethod_CreatedDate]  DEFAULT (GETDATE()) FOR [CreatedDate]
GO

