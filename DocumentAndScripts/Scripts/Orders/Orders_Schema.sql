IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrders_RateCardDetail]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrders]') )
    ALTER TABLE [dbo].[MediaOrders] DROP CONSTRAINT [FK_MediaOrders_RateCardDetail]
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrders_RateCard]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrders]') )
    ALTER TABLE [dbo].[MediaOrders] DROP CONSTRAINT [FK_MediaOrders_RateCard]
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrders_MediaOrdersBuy]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrders]') )
    ALTER TABLE [dbo].[MediaOrders] DROP CONSTRAINT [FK_MediaOrders_MediaOrdersBuy]
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrders_MediaAsset]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrders]') )
    ALTER TABLE [dbo].[MediaOrders] DROP CONSTRAINT [FK_MediaOrders_MediaAsset]
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrders_IssueDate]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrders]') )
    ALTER TABLE [dbo].[MediaOrders] DROP CONSTRAINT [FK_MediaOrders_IssueDate]
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrderReps_MediaOrders]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrderReps]') )
    ALTER TABLE [dbo].[MediaOrderReps] DROP CONSTRAINT [FK_MediaOrderReps_MediaOrders]
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrderProductionDetails_MediaOrders]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrderProductionDetails]') )
    ALTER TABLE [dbo].[MediaOrderProductionDetails] DROP CONSTRAINT [FK_MediaOrderProductionDetails_MediaOrders]
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrderLines_MediaOrders]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrderLines]') )
    ALTER TABLE [dbo].[MediaOrderLines] DROP CONSTRAINT [FK_MediaOrderLines_MediaOrders]
GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_Table_1_CreatedOn]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrdersBuy] DROP CONSTRAINT [DF_Table_1_CreatedOn]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrdersBuy_DeletedInd]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrdersBuy] DROP CONSTRAINT [DF_MediaOrdersBuy_DeletedInd]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrders_CreatedDate]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrders] DROP CONSTRAINT [DF_MediaOrders_CreatedDate]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrders_DeletedInd]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrders] DROP CONSTRAINT [DF_MediaOrders_DeletedInd]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrders_IsFrozen]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrders] DROP CONSTRAINT [DF_MediaOrders_IsFrozen]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrders_NetCost]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrders] DROP CONSTRAINT [DF_MediaOrders_NetCost]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrders_GrossCost]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrders] DROP CONSTRAINT [DF_MediaOrders_GrossCost]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrders_RateCardCost]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrders] DROP CONSTRAINT [DF_MediaOrders_RateCardCost]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrders_BaseRateCardCost]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrders] DROP CONSTRAINT [DF_MediaOrders_BaseRateCardCost]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrderReps_CreatedDate]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrderReps] DROP CONSTRAINT [DF_MediaOrderReps_CreatedDate]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrderReps_DeletedInd]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrderReps] DROP CONSTRAINT [DF_MediaOrderReps_DeletedInd]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrderProductionDetails_CreatedDate]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrderProductionDetails] DROP CONSTRAINT [DF_MediaOrderProductionDetails_CreatedDate]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrderProductionDetails_DeletedInd]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrderProductionDetails] DROP CONSTRAINT [DF_MediaOrderProductionDetails_DeletedInd]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrderLines_CreatedDate]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrderLines] DROP CONSTRAINT [DF_MediaOrderLines_CreatedDate]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrderLines_DeletedInd]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrderLines] DROP CONSTRAINT [DF_MediaOrderLines_DeletedInd]
    END

GO
/****** Object:  Table [dbo].[MediaOrdersBuy]    Script Date: 12/24/2017 3:10:30 PM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[MediaOrdersBuy]')
                    AND type IN ( N'U' ) )
    DROP TABLE [dbo].[MediaOrdersBuy]
GO
/****** Object:  Table [dbo].[MediaOrders]    Script Date: 12/24/2017 3:10:30 PM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[MediaOrders]')
                    AND type IN ( N'U' ) )
    DROP TABLE [dbo].[MediaOrders]
GO
/****** Object:  Table [dbo].[MediaOrderReps]    Script Date: 12/24/2017 3:10:30 PM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[MediaOrderReps]')
                    AND type IN ( N'U' ) )
    DROP TABLE [dbo].[MediaOrderReps]
GO
/****** Object:  Table [dbo].[MediaOrderProductionDetails]    Script Date: 12/24/2017 3:10:30 PM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[MediaOrderProductionDetails]')
                    AND type IN ( N'U' ) )
    DROP TABLE [dbo].[MediaOrderProductionDetails]
GO
/****** Object:  Table [dbo].[MediaOrderLines]    Script Date: 12/24/2017 3:10:30 PM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[MediaOrderLines]')
                    AND type IN ( N'U' ) )
    DROP TABLE [dbo].[MediaOrderLines]
GO
/****** Object:  Table [dbo].[MediaOrderLines]    Script Date: 12/24/2017 3:10:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[dbo].[MediaOrderLines]')
                        AND type IN ( N'U' ) )
    BEGIN
        CREATE TABLE [dbo].[MediaOrderLines]
            (
              [MediaOrderLineId] [INT] IDENTITY(1, 1)
                                       NOT NULL ,
              [MediaOrderId] [INT] NOT NULL ,
              [SequenceNumber] [INT] NOT NULL ,
              [AdAdjustmentName] [NVARCHAR](255) NOT NULL ,
              [AmountPercent] [SMALLINT] NOT NULL ,
              [SurchargeDiscount] [SMALLINT] NOT NULL ,
              [AdjustmentValue] [FLOAT] NOT NULL ,
              [ProductCode] [NVARCHAR](50) NOT NULL ,
              [DeletedInd] [BIT] NOT NULL ,
              [CreatedDate] [DATETIME] NOT NULL ,
              [CreatedBy] [INT] NULL ,
              [ModifiedDate] [DATETIME] NULL ,
              [ModifiedBy] [INT] NULL ,
              CONSTRAINT [PK_MediaOrderLines] PRIMARY KEY CLUSTERED
                ( [MediaOrderLineId] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]
    END
GO
/****** Object:  Table [dbo].[MediaOrderProductionDetails]    Script Date: 12/24/2017 3:10:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[dbo].[MediaOrderProductionDetails]')
                        AND type IN ( N'U' ) )
    BEGIN
        CREATE TABLE [dbo].[MediaOrderProductionDetails]
            (
              [MediaOrderProductionDetailId] [INT] IDENTITY(1, 1)
                                                   NOT NULL ,
              [MediaOrderId] [INT] NOT NULL ,
              [NewPickupInd] [BIT] NULL ,
              [PickupMediaOrderId] [INT] NULL ,
              [CreateExpectedInd] [BIT] NULL ,
              [MaterialExpectedDate] [DATE] NULL ,
              [TrackingNumber] [NVARCHAR](50) NULL ,
              [OnHandInd] [BIT] NULL ,
              [MaterialContactId] [INT] NULL ,
              [OriginalFile] [NVARCHAR](250) NULL ,
              [ProofFile] [NVARCHAR](250) NULL ,
              [FinalFile] [NVARCHAR](250) NULL ,
              [WebAdUrl] [NVARCHAR](250) NULL ,
              [TearSheets] [INT] NULL ,
              [HeadLine] [NVARCHAR](100) NULL ,
              [PositionId] [INT] NULL ,
              [SeparationId] [INT] NULL ,
              [PageNumber] [NVARCHAR](20) NULL ,
              [ProductionComment] [VARCHAR](MAX) NULL ,
              [DeletedInd] [BIT] NOT NULL ,
              [CreatedDate] [DATETIME] NOT NULL ,
              [CreatedBy] [INT] NULL ,
              [ModifiedDate] [DATETIME] NULL ,
              [ModifiedBy] [INT] NULL ,
              CONSTRAINT [PK_MediaOrderProductionDetails] PRIMARY KEY CLUSTERED
                ( [MediaOrderProductionDetailId] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY] TEXTIMAGE_ON [PRIMARY]
    END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[MediaOrderReps]    Script Date: 12/24/2017 3:10:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[dbo].[MediaOrderReps]')
                        AND type IN ( N'U' ) )
    BEGIN
        CREATE TABLE [dbo].[MediaOrderReps]
            (
              [MediaOrderRepId] [INT] IDENTITY(1, 1)
                                      NOT NULL ,
              [MediaOrderId] [INT] NOT NULL ,
              [RepId] [INT] NOT NULL ,
              [TerritoryId] [INT] NOT NULL ,
              [Commission] [FLOAT] NOT NULL ,
              [Split] [FLOAT] NOT NULL ,
              [DeletedInd] [BIT] NOT NULL ,
              [CreatedDate] [DATETIME] NOT NULL ,
              [CreatedBy] [INT] NULL ,
              [ModifiedDate] [DATETIME] NULL ,
              [ModifiedBy] [INT] NULL ,
              CONSTRAINT [PK_MediaOrderReps] PRIMARY KEY CLUSTERED
                ( [MediaOrderRepId] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]
    END
GO
/****** Object:  Table [dbo].[MediaOrders]    Script Date: 12/24/2017 3:10:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[dbo].[MediaOrders]')
                        AND type IN ( N'U' ) )
    BEGIN
        CREATE TABLE [dbo].[MediaOrders]
            (
              [MediaOrderId] [INT] IDENTITY(1, 1)
                                   NOT NULL ,
              [BuyId] [INT] NOT NULL ,
              [ORDER_NUMBER] [INT] NULL ,
              [AdvertiserId] [VARCHAR](10) NOT NULL ,
              [AgencyId] [VARCHAR](10) NOT NULL ,
              [BT_ID] [VARCHAR](10) NOT NULL ,
              [ST_ID] [VARCHAR](10) NOT NULL ,
              [BillToInd] [BIT] NOT NULL ,
              [CampaignName] [VARCHAR](50) NULL ,
              [MediaAssetId] [INT] NOT NULL ,
              [RateCardId] [INT] NOT NULL ,
              [RateCardDetailId] [INT] NOT NULL ,
              [IssueDateId] [INT] NOT NULL ,
              [ProductCode] [VARCHAR](50) NOT NULL ,
              [Column] [INT] NULL ,
              [Inches] [FLOAT] NULL ,
              [Impressions] [INT] NULL ,
              [ClassifiedText] [VARCHAR](MAX) NULL ,
              [OrderStatus] [VARCHAR](20) NOT NULL ,
              [BaseRateCardCost] [MONEY] NOT NULL ,
              [RateCardCost] [MONEY] NOT NULL ,
              [GrossCost] [MONEY] NOT NULL ,
              [NetCost] [MONEY] NOT NULL ,
              [FlightStartDate] [DATE] NULL ,
              [FlightEndDate] [DATE] NULL ,
              [IsFrozen] [BIT] NOT NULL ,
              [DeletedInd] [BIT] NOT NULL ,
              [CreatedDate] [DATETIME] NOT NULL ,
              [CreatedBy] [INT] NULL ,
              [ModifiedDate] [DATETIME] NULL ,
              [ModifiedBy] [INT] NULL ,
              CONSTRAINT [PK_MediaOrders] PRIMARY KEY CLUSTERED
                ( [MediaOrderId] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY] TEXTIMAGE_ON [PRIMARY]
    END
GO
SET ANSI_PADDING ON
GO
/****** Object:  Table [dbo].[MediaOrdersBuy]    Script Date: 12/24/2017 3:10:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[dbo].[MediaOrdersBuy]')
                        AND type IN ( N'U' ) )
    BEGIN
        CREATE TABLE [dbo].[MediaOrdersBuy]
            (
              [BuyId] [INT] IDENTITY(1000, 1)
                            NOT NULL ,
              [DeletedInd] [BIT] NOT NULL ,
              [CreatedDate] [DATETIME] NOT NULL ,
              [CreatedBy] [INT] NULL ,
              [ModifiedDate] [DATETIME] NULL ,
              [ModifiedBy] [INT] NULL ,
              CONSTRAINT [PK_MediaOrdersBuy] PRIMARY KEY CLUSTERED
                ( [BuyId] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]
    END
GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrderLines_DeletedInd]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrderLines] ADD  CONSTRAINT [DF_MediaOrderLines_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrderLines_CreatedDate]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrderLines] ADD  CONSTRAINT [DF_MediaOrderLines_CreatedDate]  DEFAULT (GETDATE()) FOR [CreatedDate]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrderProductionDetails_DeletedInd]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrderProductionDetails] ADD  CONSTRAINT [DF_MediaOrderProductionDetails_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrderProductionDetails_CreatedDate]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrderProductionDetails] ADD  CONSTRAINT [DF_MediaOrderProductionDetails_CreatedDate]  DEFAULT (GETDATE()) FOR [CreatedDate]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrderReps_DeletedInd]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrderReps] ADD  CONSTRAINT [DF_MediaOrderReps_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrderReps_CreatedDate]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrderReps] ADD  CONSTRAINT [DF_MediaOrderReps_CreatedDate]  DEFAULT (GETDATE()) FOR [CreatedDate]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrders_BaseRateCardCost]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrders] ADD  CONSTRAINT [DF_MediaOrders_BaseRateCardCost]  DEFAULT ((0)) FOR [BaseRateCardCost]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrders_RateCardCost]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrders] ADD  CONSTRAINT [DF_MediaOrders_RateCardCost]  DEFAULT ((0)) FOR [RateCardCost]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrders_GrossCost]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrders] ADD  CONSTRAINT [DF_MediaOrders_GrossCost]  DEFAULT ((0)) FOR [GrossCost]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrders_NetCost]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrders] ADD  CONSTRAINT [DF_MediaOrders_NetCost]  DEFAULT ((0)) FOR [NetCost]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrders_IsFrozen]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrders] ADD  CONSTRAINT [DF_MediaOrders_IsFrozen]  DEFAULT ((0)) FOR [IsFrozen]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrders_DeletedInd]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrders] ADD  CONSTRAINT [DF_MediaOrders_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrders_CreatedDate]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrders] ADD  CONSTRAINT [DF_MediaOrders_CreatedDate]  DEFAULT (GETDATE()) FOR [CreatedDate]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_MediaOrdersBuy_DeletedInd]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrdersBuy] ADD  CONSTRAINT [DF_MediaOrdersBuy_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_Table_1_CreatedOn]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[MediaOrdersBuy] ADD  CONSTRAINT [DF_Table_1_CreatedOn]  DEFAULT (GETDATE()) FOR [CreatedDate]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.foreign_keys
                WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrderLines_MediaOrders]')
                        AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrderLines]') )
    ALTER TABLE [dbo].[MediaOrderLines]  WITH CHECK ADD  CONSTRAINT [FK_MediaOrderLines_MediaOrders] FOREIGN KEY([MediaOrderId])
    REFERENCES [dbo].[MediaOrders] ([MediaOrderId])
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrderLines_MediaOrders]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrderLines]') )
    ALTER TABLE [dbo].[MediaOrderLines] CHECK CONSTRAINT [FK_MediaOrderLines_MediaOrders]
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.foreign_keys
                WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrderProductionDetails_MediaOrders]')
                        AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrderProductionDetails]') )
    ALTER TABLE [dbo].[MediaOrderProductionDetails]  WITH CHECK ADD  CONSTRAINT [FK_MediaOrderProductionDetails_MediaOrders] FOREIGN KEY([MediaOrderId])
    REFERENCES [dbo].[MediaOrders] ([MediaOrderId])
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrderProductionDetails_MediaOrders]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrderProductionDetails]') )
    ALTER TABLE [dbo].[MediaOrderProductionDetails] CHECK CONSTRAINT [FK_MediaOrderProductionDetails_MediaOrders]
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.foreign_keys
                WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrderReps_MediaOrders]')
                        AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrderReps]') )
    ALTER TABLE [dbo].[MediaOrderReps]  WITH CHECK ADD  CONSTRAINT [FK_MediaOrderReps_MediaOrders] FOREIGN KEY([MediaOrderId])
    REFERENCES [dbo].[MediaOrders] ([MediaOrderId])
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrderReps_MediaOrders]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrderReps]') )
    ALTER TABLE [dbo].[MediaOrderReps] CHECK CONSTRAINT [FK_MediaOrderReps_MediaOrders]
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.foreign_keys
                WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrders_IssueDate]')
                        AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrders]') )
    ALTER TABLE [dbo].[MediaOrders]  WITH CHECK ADD  CONSTRAINT [FK_MediaOrders_IssueDate] FOREIGN KEY([IssueDateId])
    REFERENCES [dbo].[IssueDate] ([IssueDateId])
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrders_IssueDate]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrders]') )
    ALTER TABLE [dbo].[MediaOrders] CHECK CONSTRAINT [FK_MediaOrders_IssueDate]
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.foreign_keys
                WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrders_MediaAsset]')
                        AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrders]') )
    ALTER TABLE [dbo].[MediaOrders]  WITH CHECK ADD  CONSTRAINT [FK_MediaOrders_MediaAsset] FOREIGN KEY([MediaAssetId])
    REFERENCES [dbo].[MediaAsset] ([MediaAssetId])
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrders_MediaAsset]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrders]') )
    ALTER TABLE [dbo].[MediaOrders] CHECK CONSTRAINT [FK_MediaOrders_MediaAsset]
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.foreign_keys
                WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrders_MediaOrdersBuy]')
                        AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrders]') )
    ALTER TABLE [dbo].[MediaOrders]  WITH CHECK ADD  CONSTRAINT [FK_MediaOrders_MediaOrdersBuy] FOREIGN KEY([BuyId])
    REFERENCES [dbo].[MediaOrdersBuy] ([BuyId])
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrders_MediaOrdersBuy]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrders]') )
    ALTER TABLE [dbo].[MediaOrders] CHECK CONSTRAINT [FK_MediaOrders_MediaOrdersBuy]
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.foreign_keys
                WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrders_RateCard]')
                        AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrders]') )
    ALTER TABLE [dbo].[MediaOrders]  WITH CHECK ADD  CONSTRAINT [FK_MediaOrders_RateCard] FOREIGN KEY([RateCardId])
    REFERENCES [dbo].[RateCard] ([RateCardId])
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrders_RateCard]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrders]') )
    ALTER TABLE [dbo].[MediaOrders] CHECK CONSTRAINT [FK_MediaOrders_RateCard]
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.foreign_keys
                WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrders_RateCardDetail]')
                        AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrders]') )
    ALTER TABLE [dbo].[MediaOrders]  WITH CHECK ADD  CONSTRAINT [FK_MediaOrders_RateCardDetail] FOREIGN KEY([RateCardDetailId])
    REFERENCES [dbo].[RateCardDetail] ([RateCardDetailId])
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_MediaOrders_RateCardDetail]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[MediaOrders]') )
    ALTER TABLE [dbo].[MediaOrders] CHECK CONSTRAINT [FK_MediaOrders_RateCardDetail]
GO
