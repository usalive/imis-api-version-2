IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RepTerritories_Territories]') AND parent_object_id = OBJECT_ID(N'[dbo].[RepTerritories]'))
ALTER TABLE [dbo].[RepTerritories] DROP CONSTRAINT [FK_RepTerritories_Territories]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RepTerritories_Reps]') AND parent_object_id = OBJECT_ID(N'[dbo].[RepTerritories]'))
ALTER TABLE [dbo].[RepTerritories] DROP CONSTRAINT [FK_RepTerritories_Reps]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Territories_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Territories] DROP CONSTRAINT [DF_Territories_CreatedDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Territories_DeletedInd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Territories] DROP CONSTRAINT [DF_Territories_DeletedInd]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Separations_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Separations] DROP CONSTRAINT [DF_Separations_CreatedDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Separations_DeletedInd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Separations] DROP CONSTRAINT [DF_Separations_DeletedInd]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_RepTerritories_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RepTerritories] DROP CONSTRAINT [DF_RepTerritories_CreatedDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_RepTerritories_DeletedInd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RepTerritories] DROP CONSTRAINT [DF_RepTerritories_DeletedInd]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Reps_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Reps] DROP CONSTRAINT [DF_Reps_CreatedDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Reps_DeletedInd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Reps] DROP CONSTRAINT [DF_Reps_DeletedInd]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_ProductionStatuses_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ProductionStatuses] DROP CONSTRAINT [DF_ProductionStatuses_CreatedDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_ProductionStatuses_DeletedInd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ProductionStatuses] DROP CONSTRAINT [DF_ProductionStatuses_DeletedInd]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_ProductionStages_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ProductionStages] DROP CONSTRAINT [DF_ProductionStages_CreatedDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_ProductionStages_DeletedInd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ProductionStages] DROP CONSTRAINT [DF_ProductionStages_DeletedInd]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Positions_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Positions] DROP CONSTRAINT [DF_Positions_CreatedDate]
END

GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Positions_DeletedInd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Positions] DROP CONSTRAINT [DF_Positions_DeletedInd]
END

GO
/****** Object:  Table [dbo].[Territories]    Script Date: 11/13/2017 1:22:21 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Territories]') AND type in (N'U'))
DROP TABLE [dbo].[Territories]
GO
/****** Object:  Table [dbo].[Separations]    Script Date: 11/13/2017 1:22:21 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Separations]') AND type in (N'U'))
DROP TABLE [dbo].[Separations]
GO
/****** Object:  Table [dbo].[RepTerritories]    Script Date: 11/13/2017 1:22:21 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RepTerritories]') AND type in (N'U'))
DROP TABLE [dbo].[RepTerritories]
GO
/****** Object:  Table [dbo].[Reps]    Script Date: 11/13/2017 1:22:21 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Reps]') AND type in (N'U'))
DROP TABLE [dbo].[Reps]
GO
/****** Object:  Table [dbo].[ProductionStatuses]    Script Date: 11/13/2017 1:22:21 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductionStatuses]') AND type in (N'U'))
DROP TABLE [dbo].[ProductionStatuses]
GO
/****** Object:  Table [dbo].[ProductionStages]    Script Date: 11/13/2017 1:22:21 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductionStages]') AND type in (N'U'))
DROP TABLE [dbo].[ProductionStages]
GO
/****** Object:  Table [dbo].[Positions]    Script Date: 11/13/2017 1:22:21 PM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Positions]') AND type in (N'U'))
DROP TABLE [dbo].[Positions]
GO
/****** Object:  Table [dbo].[Positions]    Script Date: 11/13/2017 1:22:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Positions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Positions](
	[PositionId] [int] IDENTITY(1,1) NOT NULL,
	[PositionName] [nvarchar](50) NOT NULL,
	[DeletedInd] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_Positions] PRIMARY KEY CLUSTERED 
(
	[PositionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ProductionStages]    Script Date: 11/13/2017 1:22:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductionStages]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ProductionStages](
	[ProductionStageId] [int] IDENTITY(1,1) NOT NULL,
	[ProductionStageName] [nvarchar](50) NOT NULL,
	[DeletedInd] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_ProductionStages] PRIMARY KEY CLUSTERED 
(
	[ProductionStageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ProductionStatuses]    Script Date: 11/13/2017 1:22:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProductionStatuses]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ProductionStatuses](
	[ProductionStatusId] [int] IDENTITY(1,1) NOT NULL,
	[ProductionStatusName] [nvarchar](50) NOT NULL,
	[DeletedInd] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_ProductionStatuses] PRIMARY KEY CLUSTERED 
(
	[ProductionStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Reps]    Script Date: 11/13/2017 1:22:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Reps]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Reps](
	[RepId] [int] IDENTITY(1,1) NOT NULL,
	[RepName] [nvarchar](50) NOT NULL,
	[DeletedInd] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_Reps] PRIMARY KEY CLUSTERED 
(
	[RepId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[RepTerritories]    Script Date: 11/13/2017 1:22:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RepTerritories]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RepTerritories](
	[RepId] [int] NOT NULL,
	[TerritoryId] [int] NOT NULL,
	[Commission] [float] NOT NULL,
	[DeletedInd] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Separations]    Script Date: 11/13/2017 1:22:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Separations]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Separations](
	[SeparationId] [int] IDENTITY(1,1) NOT NULL,
	[SeparationName] [nvarchar](50) NOT NULL,
	[DeletedInd] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_Separations] PRIMARY KEY CLUSTERED 
(
	[SeparationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Territories]    Script Date: 11/13/2017 1:22:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Territories]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Territories](
	[TerritoryId] [int] IDENTITY(1,1) NOT NULL,
	[TerritoryName] [varchar](100) NOT NULL,
	[DeletedInd] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_Territories] PRIMARY KEY CLUSTERED 
(
	[TerritoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Positions_DeletedInd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Positions] ADD  CONSTRAINT [DF_Positions_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Positions_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Positions] ADD  CONSTRAINT [DF_Positions_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_ProductionStages_DeletedInd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ProductionStages] ADD  CONSTRAINT [DF_ProductionStages_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_ProductionStages_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ProductionStages] ADD  CONSTRAINT [DF_ProductionStages_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_ProductionStatuses_DeletedInd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ProductionStatuses] ADD  CONSTRAINT [DF_ProductionStatuses_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_ProductionStatuses_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ProductionStatuses] ADD  CONSTRAINT [DF_ProductionStatuses_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Reps_DeletedInd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Reps] ADD  CONSTRAINT [DF_Reps_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Reps_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Reps] ADD  CONSTRAINT [DF_Reps_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_RepTerritories_DeletedInd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RepTerritories] ADD  CONSTRAINT [DF_RepTerritories_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_RepTerritories_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[RepTerritories] ADD  CONSTRAINT [DF_RepTerritories_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Separations_DeletedInd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Separations] ADD  CONSTRAINT [DF_Separations_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Separations_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Separations] ADD  CONSTRAINT [DF_Separations_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Territories_DeletedInd]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Territories] ADD  CONSTRAINT [DF_Territories_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_Territories_CreatedDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Territories] ADD  CONSTRAINT [DF_Territories_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RepTerritories_Reps]') AND parent_object_id = OBJECT_ID(N'[dbo].[RepTerritories]'))
ALTER TABLE [dbo].[RepTerritories]  WITH CHECK ADD  CONSTRAINT [FK_RepTerritories_Reps] FOREIGN KEY([RepId])
REFERENCES [dbo].[Reps] ([RepId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RepTerritories_Reps]') AND parent_object_id = OBJECT_ID(N'[dbo].[RepTerritories]'))
ALTER TABLE [dbo].[RepTerritories] CHECK CONSTRAINT [FK_RepTerritories_Reps]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RepTerritories_Territories]') AND parent_object_id = OBJECT_ID(N'[dbo].[RepTerritories]'))
ALTER TABLE [dbo].[RepTerritories]  WITH CHECK ADD  CONSTRAINT [FK_RepTerritories_Territories] FOREIGN KEY([TerritoryId])
REFERENCES [dbo].[Territories] ([TerritoryId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RepTerritories_Territories]') AND parent_object_id = OBJECT_ID(N'[dbo].[RepTerritories]'))
ALTER TABLE [dbo].[RepTerritories] CHECK CONSTRAINT [FK_RepTerritories_Territories]
GO


--------------------------------------------
------  RepTerritories Schema Update -------
--------------------------------------------

IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RepTerritories_Territories]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[RepTerritories]') )
    ALTER TABLE [dbo].[RepTerritories] DROP CONSTRAINT [FK_RepTerritories_Territories]
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RepTerritories_Reps]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[RepTerritories]') )
    ALTER TABLE [dbo].[RepTerritories] DROP CONSTRAINT [FK_RepTerritories_Reps]
GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_RepTerritories_CreatedDate]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[RepTerritories] DROP CONSTRAINT [DF_RepTerritories_CreatedDate]
    END

GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[DF_RepTerritories_DeletedInd]')
                    AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[RepTerritories] DROP CONSTRAINT [DF_RepTerritories_DeletedInd]
    END

GO
/****** Object:  Table [dbo].[RepTerritories]    Script Date: 11/27/2017 10:45:00 AM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[RepTerritories]')
                    AND type IN ( N'U' ) )
    DROP TABLE [dbo].[RepTerritories]
GO
/****** Object:  Table [dbo].[RepTerritories]    Script Date: 11/27/2017 10:45:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'[dbo].[RepTerritories]')
                        AND type IN ( N'U' ) )
    BEGIN
        CREATE TABLE [dbo].[RepTerritories]
            (
              [RepTerritoryId] [INT] IDENTITY(1, 1)
                                     NOT NULL ,
              [RepId] [INT] NOT NULL ,
              [TerritoryId] [INT] NOT NULL ,
              [Commission] [FLOAT] NOT NULL ,
              [DeletedInd] [BIT] NOT NULL ,
              [CreatedDate] [DATETIME] NOT NULL ,
              [CreatedBy] [INT] NULL ,
              [ModifiedDate] [DATETIME] NULL ,
              [ModifiedBy] [INT] NULL ,
              CONSTRAINT [PK_RepTerritories] PRIMARY KEY CLUSTERED
                ( [RepTerritoryId] ASC )
                WITH ( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF,
                       IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                       ALLOW_PAGE_LOCKS = ON ) ON [PRIMARY]
            )
        ON  [PRIMARY]
    END
GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_RepTerritories_DeletedInd]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[RepTerritories] ADD  CONSTRAINT [DF_RepTerritories_DeletedInd]  DEFAULT ((0)) FOR [DeletedInd]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    dbo.sysobjects
                WHERE   id = OBJECT_ID(N'[dbo].[DF_RepTerritories_CreatedDate]')
                        AND type = 'D' )
    BEGIN
        ALTER TABLE [dbo].[RepTerritories] ADD  CONSTRAINT [DF_RepTerritories_CreatedDate]  DEFAULT (GETDATE()) FOR [CreatedDate]
    END

GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.foreign_keys
                WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RepTerritories_Reps]')
                        AND parent_object_id = OBJECT_ID(N'[dbo].[RepTerritories]') )
    ALTER TABLE [dbo].[RepTerritories]  WITH CHECK ADD  CONSTRAINT [FK_RepTerritories_Reps] FOREIGN KEY([RepId])
    REFERENCES [dbo].[Reps] ([RepId])
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RepTerritories_Reps]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[RepTerritories]') )
    ALTER TABLE [dbo].[RepTerritories] CHECK CONSTRAINT [FK_RepTerritories_Reps]
GO
IF NOT EXISTS ( SELECT  *
                FROM    sys.foreign_keys
                WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RepTerritories_Territories]')
                        AND parent_object_id = OBJECT_ID(N'[dbo].[RepTerritories]') )
    ALTER TABLE [dbo].[RepTerritories]  WITH CHECK ADD  CONSTRAINT [FK_RepTerritories_Territories] FOREIGN KEY([TerritoryId])
    REFERENCES [dbo].[Territories] ([TerritoryId])
GO
IF EXISTS ( SELECT  *
            FROM    sys.foreign_keys
            WHERE   object_id = OBJECT_ID(N'[dbo].[FK_RepTerritories_Territories]')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[RepTerritories]') )
    ALTER TABLE [dbo].[RepTerritories] CHECK CONSTRAINT [FK_RepTerritories_Territories]
GO
