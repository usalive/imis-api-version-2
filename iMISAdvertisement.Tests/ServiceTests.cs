﻿using Asi.DAL.Model;
using Asi.Repository;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace Asi.Tests
{
    [TestFixture]
    public class ServiceTests
    {
        #region Variables
        IAdTypeRepository _adTypeRepository;
        //AdTypeRepository _adTypeService;
        //IDBEntities _dbEntities;
        ResponseCommon lstAdTypes;
        #endregion

        #region Setup
        [SetUp]
        public void Setup()
        {
            lstAdTypes = SetupAdTypes();

            //_dbEntities = new Mock<IDBEntities>().Object;
            _adTypeRepository = SetupAdTypeRepository();
            // _adTypeService = new AdTypeRepository(_dbEntities);
        }

        private ResponseCommon SetupAdTypes()
        {
            ResponseCommon response = new ResponseCommon();
            response.Data = new List<GetAdTypeModel>()
            {
                new GetAdTypeModel() { AdTypeId=1,Name="Digital" },
                new GetAdTypeModel() { AdTypeId=1,Name="Print" },
            };
            return response;
        }

        public IAdTypeRepository SetupAdTypeRepository()
        {
            //Init Repository
            var repo = new Mock<IAdTypeRepository>();

            //Setup Mocking Behaviour
            repo.Setup(r => r.GetAdTypes()).Returns(lstAdTypes);

            //return mock implementation
            return repo.Object;

        }
        #endregion

        #region Tests
        [Test]
        public void ServiceShouldReturnAllAdTypes()
        {
            var adtypes = _adTypeRepository.GetAdTypes();

            Assert.That(adtypes, Is.EqualTo(lstAdTypes));
        }

        #endregion
    }
}
