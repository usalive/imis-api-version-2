﻿'use strict';

app.factory('issueDateFactory', function ($http, $rootScope) {


    var url = "https://localhost:444/api/issuedate";//sf.getServiceRoot('ue') + "api/MediaAsset";
    var cacheData = {};
    var individualAreaDetailCache = {};
    var cacheDataDistinct = null;

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getIssueDate: function (issueDates, callback) {
            $http({
                url: url + "/getbymediaassetanddaterange",
                dataType: 'json',
                method: 'POST',
                data: issueDates,
                headers: {
                    "Content-Type": "application/json"
                }
            }).success(function (result) {
                callback(result);
            }).error(function (error) {
                alert(error);
            });

        },

        getByMediaAsset: function (mediaAssetId, callback) {
            $http({
                url: url + '/getbymediaasset/' + mediaAssetId,
                method: 'GET',
                headers: sfHeaders,
                params: {},
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

        getAll: function (callback) {
            $http({
                url: url + '',
                method: 'GET',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

    };
});