﻿'use strict';

app.factory('DashBoardFactory', function ($http, $rootScope) {

    var url = "https://localhost:444/api/dashboard";//sf.getServiceRoot('ue') + "api/MediaAsset";
    var cacheData = {};
    var individualAreaDetailCache = {};
    var cacheDataDistinct = null;

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getInserationOrderByStatus: getInserationOrderByStatus,
        getOrderCountByYearly: getOrderCountByYearly,
        getInserationOrderCount: getInserationOrderCount,
        getTotalNetAmount: getTotalNetAmount,
        getMediaAssetUsedInOrder: getMediaAssetUsedInOrder,
        getAdvertiserUsedInOrder: getAdvertiserUsedInOrder,
        getorderbyadvertisers: getorderbyadvertisers,
        getorderdetailsbymediaassets: getorderdetailsbymediaassets,
        getOrdersbyProposalStatus: getOrdersbyProposalStatus,
    };


    function getInserationOrderByStatus(callback) {
        $http({
            url: url + "/getinserationordercountbyorderstatus",
            dataType: 'json',
            method: 'GET',
            data: '',
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function getOrderCountByYearly(callback) {
        $http({
            url: url + "/getordercountbymonthlyforcurrentyear",
            dataType: 'json',
            method: 'GET',
            data: '',
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function getInserationOrderCount(callback) {
        $http({
            url: url + "/getinserationordercount",
            dataType: 'json',
            method: 'GET',
            data: '',
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function getTotalNetAmount(callback) {
        $http({
            url: url + "/gettotalnetamount",
            dataType: 'json',
            method: 'GET',
            data: '',
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function getMediaAssetUsedInOrder(callback) {
        $http({
            url: url + "/getmediaassetusedinorder",
            dataType: 'json',
            method: 'GET',
            data: '',
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function getAdvertiserUsedInOrder(callback) {
        $http({
            url: url + "/getadvertiserusedinorder",
            dataType: 'json',
            method: 'GET',
            data: '',
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function getorderbyadvertisers(callback) {
        $http({
            url: url + "/getorderbyadvertisers",
            dataType: 'json',
            method: 'GET',
            async: false,
            data: '',
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function getorderdetailsbymediaassets(callback) {
        $http({
            url: url + "/getorderdetailsbymediaassets",
            dataType: 'json',
            method: 'GET',
            async: false,
            data: '',
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function getOrdersbyProposalStatus(data, callback) {
        $http({
            url: url + "/getordersbyproposalstatus",
            dataType: 'json',
            method: 'POST',
            async: false,
            data: data,
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }
});