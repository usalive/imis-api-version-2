﻿'use strict';

app.factory('advertiserFactory', function ($http, $rootScope) {

    var url = "https://localhost:444/api/MediaAsset";//sf.getServiceRoot('ue') + "api/MediaAsset";
    var cacheData = {};
    var individualAreaDetailCache = {};
    var cacheDataDistinct = null;
    var asi_URL = "https://localhost/Asi.Scheduler_iMIS10/api/party"

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    var returnadvertiserFactory = {
        getAllAdvertiser: getAllAdvertiser,
        getCountry: getCountry,
        saveOrganization: saveOrganization,
        getAllAdvertiser1: getAllAdvertiser1,
    }
    return returnadvertiserFactory;

    function getAllAdvertiser(baseUrl, authToken, callback) {
        $http({
            // url: baseUrl + "api/Party?limit=500&Offset=" + offset,
            // url: asi_URL + "?limit=1000000",
            url: baseUrl + "api/Party?limit=5000",
            dataType: 'json',
            method: 'GET',
            data: '',
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function getCountry(baseUrl, authToken, offset, callback) {
        $http({
            url: baseUrl + "api/country?limit=500&Offset=" + offset,
            // url: asi_URL + "api/country",
            dataType: 'json',
            method: 'GET',
            data: '',
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function saveOrganization(baseUrl, authToken, advertiserObject, callback) {
        $http({
            url: baseUrl + "api/Party",
            // url: asi_URL,
            dataType: 'json',
            method: 'POST',
            data: advertiserObject,
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function getAllAdvertiser1(baseUrl, authToken, Offset, callback) {
        $http({
            url: baseUrl + "api/Party?limit=500&Offset=" + Offset,
            // url: asi_URL + "?limit=1000000",
            //url: baseUrl + "api/Party?limit=5000",
            dataType: 'json',
            method: 'GET',
            async: false,
            data: '',
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

});