﻿app.directive('loading',   ['$http' ,function ($http)
{
    return {
        restrict: 'A',
        template: '<div class="loading-spiner"><img class="progressP" src="{{iMISbaseUrl}}/areas/iMISAngular_Orders/image/loader-elipses.gif" /> </div>',
        link: function (scope, elm, attrs)
        {
            scope.isLoading = function () {
                return $http.pendingRequests.length > 0;
            };

            scope.$watch(scope.isLoading, function (v)
            {
                if(v){
                    elm.show();
                }else{
                    elm.hide();
                }
            });
        }
    };
}])
