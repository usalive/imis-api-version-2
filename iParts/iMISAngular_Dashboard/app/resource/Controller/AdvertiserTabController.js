﻿'use strict';

app.controller('AdvertiserTabController', function AdvertiserTabController($scope, $location, $rootScope, $compile, $stateParams, $exceptionHandler,
    $timeout, $filter, DashBoardFactory, advertiserFactory, authKeyService, toastr) {
    var vm = this;


    var authKey = authKeyService.getConstantAuthKey();
    var baseUrl = authKey.baseUrl;
    var authToken = authKey.authToken;

    $scope.advrertiserData = {};
    $scope.partyAndOrganizationData = [];


    function getAdvertiserThroughASI() {
        try {
            if ($rootScope.mainAdvertisers.length <= 0) {
                var HasNext = true;
                var offset = 0;
                var advertisers = [];
                var contacts = [];
                function getAdvertiserData() {
                    advertiserFactory.getAllAdvertiser1(baseUrl, authToken, offset, function (result) {
                        if (result != null && result != undefined && result != "") {
                            var ItemData = result.Items.$values
                            console.log(ItemData);
                            angular.forEach(ItemData, function (itemdatavalue, key) {
                                $scope.partyAndOrganizationData.push(itemdatavalue);
                                var type = itemdatavalue.$type;
                                if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                                    advertisers.push(itemdatavalue);
                                }
                                else if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                                    contacts.push(itemdatavalue);
                                }
                            });

                            $scope.advertisers = advertisers;
                            $scope.agencies = advertisers;
                            $rootScope.mainAdvertisers = advertisers;
                            $scope.personData = contacts;
                            var TotalCount = result.TotalCount;
                            HasNext = result.HasNext;
                            offset = result.Offset;
                            var Limit = result.Limit;
                            var NextOffset = result.NextOffset;
                            if (HasNext == true) {
                                offset = NextOffset;
                                getAdvertiserData();
                            } else {
                                DashBoardFactory.getorderbyadvertisers(function (result) {
                                    if (result.statusCode == 1) {
                                        console.log(result);
                                        if (result.data != null) {
                                            if (result.data.length > 0) {
                                                $scope.advrertiserData = result.data;
                                                angular.forEach($scope.advrertiserData, function (data, key) {
                                                    if ($scope.advertisers != undefined) {
                                                        var responseData = $scope.advertisers;
                                                        var Advertisers = alasql('SELECT Name FROM ? AS add WHERE Id  = ?', [responseData, data.AdvertiserId.toString()]);
                                                        if (Advertisers.length > 0) {
                                                            $scope.advrertiserData[key].AdvertiserName = Advertisers[0].Name;
                                                        }
                                                    }
                                                });
                                                var data = $scope.advrertiserData;
                                                createListforAdvertiser(data);
                                            }
                                            else {
                                                $scope.advrertiserData = [];
                                            }
                                        } else {
                                            $scope.advrertiserData = [];
                                        }
                                    }
                                    else if (result.statusCode == 3) {
                                        $scope.advrertiserData = [];
                                    }
                                    else {
                                        $scope.advrertiserData = [];
                                        toastr.error(result.message, 'Error!');
                                    }
                                });
                            }
                        }
                        else {
                            $scope.advertisers = [];
                        }
                    })
                }
                getAdvertiserData();
            }
            else {
                $scope.advertisers = $rootScope.mainAdvertisers;
                advertiserFactory.getAllAdvertiser1(baseUrl, authToken, offset, function (result) {
                    if (result != null && result != undefined && result != "") {
                        $scope.isEditMode = true;
                        DashBoardFactory.getorderbyadvertisers(function (result) {
                            if (result.statusCode == 1) {
                                console.log(result);
                                if (result.data != null) {
                                    if (result.data.length > 0) {
                                        $scope.advrertiserData = result.data;
                                        angular.forEach($scope.advrertiserData, function (data, key) {
                                            if ($scope.advertisers != undefined) {
                                                var responseData = $scope.advertisers;
                                                var Advertisers = alasql('SELECT Name FROM ? AS add WHERE Id  = ?', [responseData, data.AdvertiserId.toString()]);
                                                if (Advertisers.length > 0) {
                                                    $scope.advrertiserData[key].AdvertiserName = Advertisers[0].Name;
                                                }
                                            }
                                        });
                                        var data = $scope.advrertiserData;
                                        createListforAdvertiser(data);
                                    }
                                    else {
                                        $scope.advrertiserData = [];
                                    }
                                } else {
                                    $scope.advrertiserData = [];
                                }
                            }
                            else if (result.statusCode == 3) {
                                $scope.advrertiserData = [];
                            }
                            else {
                                $scope.advrertiserData = [];
                                toastr.error(result.message, 'Error!');
                            }
                        });
                        $scope.isEditMode = false;
                    }
                });
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }
    function initPagination() {
        try {
            //pagination
            var sortingOrder = '';
            $scope.sortingOrder = sortingOrder;
            $scope.reverse = false;
            $scope.filteredItems = [];
            $scope.groupedItems = [];
            $scope.itemsPerPage = 10;
            $scope.pagedItems = [];
            $scope.currentPage = 0;
            if ($scope.advrertiserData.length > 0) {
                $scope.items = $scope.advrertiserData;
            } else {
                $scope.items = [];
            }

            var searchMatch = function (haystack, needle) {
                try {
                    if (!needle) {
                        return true;
                    }
                    return haystack.toString().toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                    // return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            // init the filtered items
            $scope.search = function () {
                try {
                    $scope.filteredItems = $filter('filter')($scope.items, function (item) {
                        for (var attr in item) {
                            if (searchMatch(item[attr], $scope.query))
                                return true;
                        }
                        return false;
                    });
                    // take care of the sorting order
                    if ($scope.sortingOrder !== '') {
                        $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                    }
                    $scope.currentPage = 0;
                    // now group by pages
                    $scope.groupToPages();
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            // calculate page in place
            $scope.groupToPages = function () {
                try {
                    $scope.pagedItems = [];

                    for (var i = 0; i < $scope.filteredItems.length; i++) {
                        if (i % $scope.itemsPerPage === 0) {
                            $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [$scope.filteredItems[i]];
                        } else {
                            $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                        }
                    }
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            $scope.range = function (start, end) {
                try {
                    var ret = [];
                    if (!end) {
                        end = start;
                        start = 0;
                    }
                    for (var i = start; i < end; i++) {
                        ret.push(i);
                    }
                    return ret;
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pagedItems.length - 1) {
                    $scope.currentPage++;
                }
            };

            $scope.setPage = function () {
                $scope.currentPage = this.n;
            };

            // functions have been describe process the data for display
            $scope.search();
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }
    function createListforAdvertiser(objAdvertiser) {
        try {
            if (objAdvertiser != null && objAdvertiser != undefined && objAdvertiser != "" && objAdvertiser.length > 0) {
                if (objAdvertiser.length > 0) {
                    initPagination();
                    $scope.isShowDefaultTable = false;
                    $scope.dataLoading = false;
                    $scope.isShowPagination = true;

                } else {
                    $scope.advrertiserData = [];
                    initPagination();
                    $scope.isShowDefaultTable = true;
                    $scope.isShowPagination = false;
                    $scope.dataLoading = false;
                }
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    getAdvertiserThroughASI();
    initPagination();
})