﻿'use strict';

app.controller('DashBoardController', function DashBoardController($scope, $location, $rootScope, $compile, $stateParams, $exceptionHandler,
    $timeout, DashBoardFactory, advertiserFactory, authKeyService, toastr) {
    //var vm = this;
    //vm.calculateColor = calculateColor;

    var authKey = authKeyService.getConstantAuthKey();
    var baseUrl = authKey.baseUrl;
    var authToken = authKey.authToken;
    $scope.MediaAssetTab = false;
    $scope.AdvertiserTab = false;
    $scope.MediaAssetTabDisable = false;
    $scope.AdvertiserDisable = false;

    $scope.OrderByStatus = {};
    $scope.OrderByYearly = {};
    $scope.InserationOrderCount = {};
    $scope.InserationOrderCount.PreviousYear = 0;
    $scope.InserationOrderCount.CurrentYear = 0;

    $scope.TotalNetAmount = {};
    $scope.TotalNetAmount.PreviousYear = 0;
    $scope.TotalNetAmount.CurrentYear = 0;

    $scope.MediaAssetUsedInOrder = {};
    $scope.MediaAssetUsedInOrder.PreviousYear = 0;
    $scope.MediaAssetUsedInOrder.CurrentYear = 0;

    $scope.AdvertiserUsedInOrder = {};
    $scope.AdvertiserUsedInOrder.PreviousYear = 0;
    $scope.AdvertiserUsedInOrder.CurrentYear = 0;
    activate();

    $scope.MediaAssetTab = false;
    $scope.AdvertiserTab = true;
    $scope.MediaAssetTabDisable = false;
    $scope.AdvertiserDisable = false;

    $scope.monthWiseOrderRevenue = 0;

    $scope.setAdvertisersTabActive = function () {
        $scope.MediaAssetTab = false;
        $scope.AdvertiserTab = true;
        $scope.ProposalOrderTab = false;

        $scope.MediaAssetTabDisable = false;
        $scope.AdvertiserDisable = false;
        $scope.ProposalOrderDisable = false;
    }

    $scope.setMediaAssetsTabActive = function () {
        $scope.MediaAssetTab = true;
        $scope.AdvertiserTab = false;
        $scope.ProposalOrderTab = false;

        $scope.MediaAssetTabDisable = false;
        $scope.AdvertiserDisable = false;
        $scope.ProposalOrderDisable = false;
    }

    $scope.setProposalOrderTabActive = function () {
        $scope.MediaAssetTab = false;
        $scope.AdvertiserTab = false;
        $scope.ProposalOrderTab = true;

        $scope.MediaAssetTabDisable = false;
        $scope.AdvertiserDisable = false;
        $scope.ProposalOrderDisable = false;
    }

    function activate() {

    }

    function calculateColor(color, value) {
        //  #006f94
        try {
            if (value === "Proposal") {
                return "red";
            } else if (value === "Run") {
                return "blue";
            } else if (value === "Cancel") {
                return "yellow";
            } else {
                return "green";
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function generatePieChart(data) {
        try {
            var Proposal = 0;
            var Run = 0;
            var Cancel = 0;
            angular.forEach(data, function (d, k) {
                var keyName = Object.keys(d)[0];
                if (keyName == "Cancel") {
                    Cancel = d["Cancel"];
                } else if (keyName == "Proposal") {
                    Proposal = d["Proposal"];
                } else if (keyName == "Run") {
                    Run = d["Run"];
                }
            });

            var chart = c3.generate({
                bindto: '#Piechart',
                data: {
                    // iris data from R
                    columns: [
                        ['Proposal', Proposal],
                        ['Run', Run],
                        ['Cancel', Cancel],
                    ],
                    //columns: data,
                    type: 'pie',

                },
                legend: {
                    show: true
                },

                label: {
                    format: function (value, ratio, id) {
                        return d3.format('$')(value);
                    }
                }
            });
            d3.select('#chart').insert('div', '.chart').attr('class', 'legend').selectAll('span')
              //  .data(['data1', 'data2', 'data3'])
              //.enter().append('span')
              //  .attr('data-id', function (id) { return id; })
              //  .html(function (id) { return id; })
                //.each(function (id) {
                //   // d3.select(this).style('background-color', chart.color(id));
                //})
                .on('mouseover', function (id) {
                    chart.focus(id);
                })
                .on('mouseout', function (id) {
                    chart.revert();
                })
                .on('click', function (id) {
                    chart.toggle(id);
                });
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function generateBarChart() {
        var allMonth = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var tmpMonthWiseData = [];
        var tmpMonthWiseRevenue = [];
        var dataLength = $scope.OrderByYearly.length - 1;
        tmpMonthWiseData.push('No of orders: ');
        angular.forEach($scope.OrderByYearly, function (d, k) {
            tmpMonthWiseData[d.MonthName] = d.NoOfOrders;
            tmpMonthWiseRevenue[d.MonthName] = d.Revenue;
        });

        var monthWiseRevenue = [];
        var monthWiseData = [];
        monthWiseData.push('No of orders ');
        monthWiseRevenue.push('Revenue ');
        angular.forEach(allMonth, function (data, key) {
            var isMonthExits = tmpMonthWiseData[data];
            var monthIndex = getMonthIndex(data);
            if (monthIndex > 0) {
                if (isMonthExits != undefined && isMonthExits != null) {
                    monthWiseData[monthIndex] = tmpMonthWiseData[data];
                    monthWiseRevenue[monthIndex] = tmpMonthWiseRevenue[data];
                } else {
                    monthWiseData[monthIndex] = 0;
                    monthWiseRevenue[monthIndex] = 0;
                }
            }
        });

        var maxValue = 0;
        if (true) {
            var tmpMonthWiseData = monthWiseData;
            tmpMonthWiseData = tmpMonthWiseData.slice(1);
            if (tmpMonthWiseData.length > 0) {
                var Value = Math.max.apply(Math, tmpMonthWiseData);
                if (parseInt(Value) > 0) {
                    maxValue = Value;
                    maxValue = maxValue + 3;
                }
            }
        }

        $scope.monthWiseOrderRevenue = monthWiseRevenue;
        // var xAxisLabel = ['x', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var xAxisLabel = ['x', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var chart = c3.generate({
            bindto: '#Barchart',
            data: {
                x: 'x',
                columns: [
                  xAxisLabel,
                  monthWiseData,
                  //monthWiseRevenue,
                ],
                type: 'bar',
                onclick: function (d, element) { console.log("onclick", d, element); },
                onmouseover: function (d) { console.log("onmouseover", d); },
                onmouseout: function (d) { console.log("onmouseout", d); }
            },
            axis: {
                x: {
                    type: 'categorized',
                    tick: {
                        width: 0.1,
                        rotate: 75,
                        multiline: false
                    },
                    label: {
                        text: 'Months',
                        position: 'outer-center'
                    },
                    padding: {
                        bottom: 20,
                    },
                    height: 70
                },
                y: {
                    label: {
                        text: 'No of orders',
                        position: 'outer-center'
                    },
                    padding: {
                        bottom: 20,
                    },
                    max: maxValue,
                    //min: -1,
                }
            },

            bar: {
                width: {
                    ratio: 0.7,
                    //max: 30
                },
            }
        });

    }

    function generateBarChartforOrderRevenue() {
        var maxValue = 0;
        if (true) {
            var tmpMonthWiseData = $scope.monthWiseOrderRevenue;
            tmpMonthWiseData = tmpMonthWiseData.slice(1);
            if (tmpMonthWiseData.length > 0) {
                var Value = Math.max.apply(Math, tmpMonthWiseData);
                if (parseInt(Value) > 0) {
                    maxValue = Value;
                    maxValue = maxValue + 3;
                }
            }
        }

        var revenueData = $scope.monthWiseOrderRevenue;
        // var xAxisLabel = ['x', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var xAxisLabel = ['x', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var chart = c3.generate({
            bindto: '#RevenueBarchart',
            data: {
                x: 'x',
                columns: [
                  xAxisLabel,
                  revenueData,
                ],
                type: 'bar',
                color: function (d) {
                    return '#df8208';
                },

                onclick: function (d, element) { console.log("onclick", d, element); },
                onmouseover: function (d) { console.log("onmouseover", d); },
                onmouseout: function (d) { console.log("onmouseout", d); }
            },
            legend: {
                show: false
            },
            axis: {
                x: {
                    type: 'categorized',
                    tick: {
                        width: 0.1,
                        rotate: 75,
                        multiline: false
                    },
                    label: {
                        text: 'Months',
                        position: 'outer-center'
                    },
                    padding: {
                        bottom: 20,
                    },
                    height: 70
                },
                y: {
                    label: {
                        text: 'Revenue',
                        position: 'outer-center'
                    },
                    padding: {
                        bottom: 20,
                    },
                    max: maxValue,
                    //min: -1,
                }
            },

            bar: {
                width: {
                    ratio: 0.7,
                    //max: 30
                },
            }
        });
    }

    function getInserationOrderByStatus() {
        try {
            //  if ($scope.issueDateWiseAdjustment.length > 0)
            DashBoardFactory.getInserationOrderByStatus(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            $scope.OrderByStatus = result.data;
                            var pieColumns = [];
                            angular.forEach($scope.OrderByStatus, function (data, key) {
                                var column = [];
                                //var name = "'" + data.Status + "'";
                                var name = data.Status;
                                column[name] = data.NoOfOrders;
                                pieColumns.push(column);
                            });
                            if (pieColumns.length > 0) {
                                generatePieChart(pieColumns);
                            }
                        }
                        else {
                            $scope.OrderByStatus = [];
                        }
                    } else {
                        $scope.OrderByStatus = [];
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.OrderByStatus = [];
                }
                else {
                    $scope.OrderByStatus = [];
                    toastr.error(result.message, 'Error!');
                }
            });

        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getMonthIndex(monthName) {
        try {
            var index = 0;
            if (monthName == 'January') {
                index = 1;
            } else if (monthName == 'February') {
                index = 2;
            } else if (monthName == 'March') {
                index = 3;
            } else if (monthName == 'April') {
                index = 4;
            } else if (monthName == 'May') {
                index = 5;
            } else if (monthName == 'June') {
                index = 6;
            } else if (monthName == 'July') {
                index = 7;
            } else if (monthName == 'August') {
                index = 8;
            } else if (monthName == 'September') {
                index = 9;
            } else if (monthName == 'October') {
                index = 10;
            } else if (monthName == 'November') {
                index = 11;
            } else if (monthName == 'December') {
                index = 12;
            }
            return index;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getOrderCountByYearlyForNoOfOrders() {
        try {
            DashBoardFactory.getOrderCountByYearly(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            $scope.OrderByYearly = result.data;
                            generateBarChart();
                        }
                        else {
                            $scope.OrderByYearly = [];
                        }
                    } else {
                        $scope.OrderByYearly = [];
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.OrderByYearly = [];
                }
                else {
                    $scope.OrderByYearly = [];
                    toastr.error(result.message, 'Error!');
                }
            });

        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getOrderCountByYearlyForRevenue() {
        try {
            if ($scope.monthWiseOrderRevenue.length > 0) {
                generateBarChartforOrderRevenue();
            } else {
                DashBoardFactory.getOrderCountByYearly(function (result) {
                    if (result.statusCode == 1) {
                        console.log(result);
                        if (result.data != null) {
                            if (result.data.length > 0) {
                                $scope.OrderByYearly = result.data;
                                var allMonth = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                                var tmpMonthWiseData = [];
                                var tmpMonthWiseRevenue = [];
                                var dataLength = $scope.OrderByYearly.length - 1;
                                tmpMonthWiseData.push('No of orders: ');
                                angular.forEach($scope.OrderByYearly, function (d, k) {
                                    tmpMonthWiseData[d.MonthName] = d.NoOfOrders;
                                    tmpMonthWiseRevenue[d.MonthName] = d.Revenue;
                                });

                                var monthWiseRevenue = [];
                                //var monthWiseData = [];
                                //monthWiseData.push('No of orders ');
                                monthWiseRevenue.push('Revenue ');
                                angular.forEach(allMonth, function (data, key) {
                                    var isMonthExits = tmpMonthWiseData[data];
                                    var monthIndex = getMonthIndex(data);
                                    if (monthIndex > 0) {
                                        if (isMonthExits != undefined && isMonthExits != null) {
                                            // monthWiseData[monthIndex] = tmpMonthWiseData[data];
                                            monthWiseRevenue[monthIndex] = tmpMonthWiseRevenue[data];
                                        } else {
                                            // monthWiseData[monthIndex] = 0;
                                            monthWiseRevenue[monthIndex] = 0;
                                        }
                                    }
                                });
                                $scope.monthWiseOrderRevenue = monthWiseRevenue;
                                generateBarChartforOrderRevenue();
                            }
                            else {
                                $scope.OrderByYearly = [];
                            }
                        } else {
                            $scope.OrderByYearly = [];
                        }
                    }
                    else if (result.statusCode == 3) {
                        $scope.OrderByYearly = [];
                    }
                    else {
                        $scope.OrderByYearly = [];
                        toastr.error(result.message, 'Error!');
                    }
                });
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getInserationOrderCount() {
        try {
            DashBoardFactory.getInserationOrderCount(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            if (result.data[0].PreviousYear.length > 0) {
                                $scope.InserationOrderCount.PreviousYear = result.data[0].PreviousYear[0].InserationOrderCount;
                            } else {
                                $scope.InserationOrderCount.PreviousYear = 0;
                            }
                            if (result.data[0].CurrentYear.length > 0) {
                                $scope.InserationOrderCount.CurrentYear = result.data[0].CurrentYear[0].InserationOrderCount;
                            } else {
                                $scope.InserationOrderCount.CurrentYear = 0;
                            }
                        }
                        else {
                            $scope.InserationOrderCount.PreviousYear = 0;
                            $scope.InserationOrderCount.CurrentYear = 0;
                        }
                    } else {
                        $scope.InserationOrderCount.PreviousYear = 0;
                        $scope.InserationOrderCount.CurrentYear = 0;
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.InserationOrderCount.PreviousYear = 0;
                    $scope.InserationOrderCount.CurrentYear = 0;
                }
                else {
                    $scope.InserationOrderCount.PreviousYear = 0;
                    $scope.InserationOrderCount.CurrentYear = 0;
                    toastr.error(result.message, 'Error!');
                }
            });

        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getTotalNetAmount() {
        try {
            DashBoardFactory.getTotalNetAmount(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            if (result.data[0].PreviousYear.length > 0) {
                                $scope.TotalNetAmount.PreviousYear = result.data[0].PreviousYear[0].NetCost;
                            } else {
                                $scope.TotalNetAmount.PreviousYear = 0;
                            }
                            if (result.data[0].CurrentYear.length > 0) {
                                $scope.TotalNetAmount.CurrentYear = result.data[0].CurrentYear[0].NetCost;
                            } else {
                                $scope.TotalNetAmount.CurrentYear = 0;
                            }
                        }
                        else {
                            $scope.TotalNetAmount.PreviousYear = 0;
                            $scope.TotalNetAmount.CurrentYear = 0;
                        }
                    } else {
                        $scope.TotalNetAmount.PreviousYear = 0;
                        $scope.TotalNetAmount.CurrentYear = 0;
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.TotalNetAmount.PreviousYear = 0;
                    $scope.TotalNetAmount.CurrentYear = 0;
                }
                else {
                    $scope.TotalNetAmount.PreviousYear = 0;
                    $scope.TotalNetAmount.CurrentYear = 0;
                    toastr.error(result.message, 'Error!');
                }
            });

        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getMediaAssetUsedInOrder() {
        try {
            DashBoardFactory.getMediaAssetUsedInOrder(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            if (result.data[0].PreviousYear.length > 0) {
                                $scope.MediaAssetUsedInOrder.PreviousYear = result.data[0].PreviousYear[0].MediaAssetCount;
                            } else {
                                $scope.MediaAssetUsedInOrder.PreviousYear = 0;
                            }
                            if (result.data[0].CurrentYear.length > 0) {
                                $scope.MediaAssetUsedInOrder.CurrentYear = result.data[0].CurrentYear[0].MediaAssetCount;
                            } else {
                                $scope.MediaAssetUsedInOrder.CurrentYear = 0;
                            }
                        }
                        else {
                            $scope.MediaAssetUsedInOrder.PreviousYear = 0;
                            $scope.MediaAssetUsedInOrder.CurrentYear = 0;
                        }
                    } else {
                        $scope.MediaAssetUsedInOrder.PreviousYear = 0;
                        $scope.MediaAssetUsedInOrder.CurrentYear = 0;
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.MediaAssetUsedInOrder.PreviousYear = 0;
                    $scope.MediaAssetUsedInOrder.CurrentYear = 0;
                }
                else {
                    $scope.MediaAssetUsedInOrder.PreviousYear = 0;
                    $scope.MediaAssetUsedInOrder.CurrentYear = 0;
                    toastr.error(result.message, 'Error!');
                }
            });

        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getAdvertiserUsedInOrder() {
        try {
            DashBoardFactory.getAdvertiserUsedInOrder(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            if (result.data[0].PreviousYear.length > 0) {
                                $scope.AdvertiserUsedInOrder.PreviousYear = result.data[0].PreviousYear[0].AdvertiserCount;
                            } else {
                                $scope.AdvertiserUsedInOrder.PreviousYear = 0;
                            }
                            if (result.data[0].CurrentYear.length > 0) {
                                $scope.AdvertiserUsedInOrder.CurrentYear = result.data[0].CurrentYear[0].AdvertiserCount;
                            } else {
                                $scope.AdvertiserUsedInOrder.CurrentYear = 0;
                            }
                        }
                        else {
                            $scope.AdvertiserUsedInOrder.PreviousYear = 0;
                            $scope.AdvertiserUsedInOrder.CurrentYear = 0;
                        }
                    } else {
                        $scope.AdvertiserUsedInOrder.PreviousYear = 0;
                        $scope.AdvertiserUsedInOrder.CurrentYear = 0;
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.AdvertiserUsedInOrder.PreviousYear = 0;
                    $scope.AdvertiserUsedInOrder.CurrentYear = 0;
                }
                else {
                    $scope.AdvertiserUsedInOrder.PreviousYear = 0;
                    $scope.AdvertiserUsedInOrder.CurrentYear = 0;
                    toastr.error(result.message, 'Error!');
                }
            });

        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }


    getInserationOrderByStatus();
    getOrderCountByYearlyForNoOfOrders();
    getOrderCountByYearlyForRevenue();
    getInserationOrderCount();
    getTotalNetAmount();
    getMediaAssetUsedInOrder();
    getAdvertiserUsedInOrder();
})