﻿'use strict';

app.controller('MediaAssetTabController', function MediaAssetTabController($scope, $location, $rootScope, $compile, $stateParams, $exceptionHandler,
    $timeout, $filter, DashBoardFactory, advertiserFactory, authKeyService, toastr) {
    var vm = this;

    $scope.$parent.setMediaAssetsTabActive();
    var authKey = authKeyService.getConstantAuthKey();
    var baseUrl = authKey.baseUrl;
    var authToken = authKey.authToken;

    $scope.mediaAssetData = {};

    function getDateInFormat(date) {
        var dateObj = new Date(date);
        var month = dateObj.getMonth() + 1;
        if (month.toString().length == 1) {
            month = "0" + month;
        }
        var day = dateObj.getDate();
        if (day.toString().length == 1) {
            day = "0" + day;
        }
        var year = dateObj.getFullYear();
        var newdate = month + "/" + day + "/" + year;
        return newdate;
    }
    function getorderdetailsbymediaassets() {
        try {

            DashBoardFactory.getorderdetailsbymediaassets(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);

                    if (result.data != null) {
                        if (result.data.length > 0) {
                            $scope.mediaAssetData = result.data;
                            angular.forEach($scope.mediaAssetData, function (data, key) {
                                $scope.mediaAssetData[key].LastOrderDate = getDateInFormat(data.LastOrderDate);
                            });
                            var data = $scope.mediaAssetData;
                            createListforMediaAsset(data);

                        }
                        else {
                            $scope.mediaAssetData = [];
                        }
                    } else {
                        $scope.mediaAssetData = [];
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.mediaAssetData = [];
                }
                else {
                    $scope.mediaAssetData = [];
                    toastr.error(result.message, 'Error!');
                }
            });

        }
        catch (e) {
            $exceptionHandler(e);
        }
    }
    function initPagination() {
        try {
            //pagination
            var sortingOrder = '';
            $scope.sortingOrder = sortingOrder;
            $scope.reverse = false;
            $scope.filteredItems = [];
            $scope.groupedItems = [];
            $scope.itemsPerPage = 10;
            $scope.pagedItems = [];
            $scope.currentPage = 0;
            if ($scope.mediaAssetData.length > 0) {
                $scope.items = $scope.mediaAssetData;
            } else {
                $scope.items = [];
            }

            var searchMatch = function (haystack, needle) {
                try {
                    if (!needle) {
                        return true;
                    }
                    return haystack.toString().toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                    // return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            // init the filtered items
            $scope.search = function () {
                try {
                    $scope.filteredItems = $filter('filter')($scope.items, function (item) {
                        for (var attr in item) {
                            if (searchMatch(item[attr], $scope.query))
                                return true;
                        }
                        return false;
                    });
                    // take care of the sorting order
                    if ($scope.sortingOrder !== '') {
                        $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                    }
                    $scope.currentPage = 0;
                    // now group by pages
                    $scope.groupToPages();
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            // calculate page in place
            $scope.groupToPages = function () {
                try {
                    $scope.pagedItems = [];

                    for (var i = 0; i < $scope.filteredItems.length; i++) {
                        if (i % $scope.itemsPerPage === 0) {
                            $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [$scope.filteredItems[i]];
                        } else {
                            $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                        }
                    }
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            $scope.range = function (start, end) {
                try {
                    var ret = [];
                    if (!end) {
                        end = start;
                        start = 0;
                    }
                    for (var i = start; i < end; i++) {
                        ret.push(i);
                    }
                    return ret;
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pagedItems.length - 1) {
                    $scope.currentPage++;
                }
            };

            $scope.setPage = function () {
                $scope.currentPage = this.n;
            };

            // functions have been describe process the data for display
            $scope.search();
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }
    function createListforMediaAsset(objMediaAsset) {
        try {
            if (objMediaAsset != null && objMediaAsset != undefined && objMediaAsset != "" && objMediaAsset.length > 0) {
                if (objMediaAsset.length > 0) {                    
                        initPagination();
                        $scope.isShowDefaultTable = false;
                        $scope.dataLoading = false;
                        $scope.isShowPagination = true;

                } else {
                    $scope.mediaAssetData = [];
                    initPagination();
                    $scope.isShowDefaultTable = true;
                    $scope.isShowPagination = false;
                    $scope.dataLoading = false;
                }
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    getorderdetailsbymediaassets();
    initPagination();

})