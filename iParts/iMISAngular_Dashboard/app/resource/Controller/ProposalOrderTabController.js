﻿'use strict';

app.controller('ProposalOrderTabController', function ProposalOrderTabController($scope, $location, $rootScope, $compile, $stateParams, $exceptionHandler,
    $timeout, $filter, DashBoardFactory, mediaAssetFactory, issueDateFactory, mediaOrdersFactory, advertiserFactory, authKeyService, toastr) {

    var vm = this;
    $scope.$parent.setProposalOrderTabActive();

    var authKey = authKeyService.getConstantAuthKey();
    var baseUrl = authKey.baseUrl;
    var authToken = authKey.authToken;

    $scope.isShowDefaultTable = false;
    $scope.isShowPagination = false;
    $scope.dataLoading = true;

    $scope.MediaAssets = [];
    $scope.IssueDates = [];
    $scope.partyAndOrganizationData = [];
    $scope.proposalOrders = [];
    $scope.proposalOrder = {};

    $scope.mediaAssetSelect = {
        multiple: false,
        formatSearching: 'Searching the Media Assset...',
        formatNoMatches: 'No Media Assset found'
    };

    $scope.IssueDateSelect = {
        multiple: false,
        formatSearching: 'Searching the Issue Date...',
        formatNoMatches: 'No Issue Date found'
    };

    function getAllMediaAssets() {
        try {
            mediaAssetFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            $scope.MediaAssets = result.data;
                        } else {
                            $scope.MediaAssets = [];
                        }
                    } else {
                        $scope.MediaAssets = [];
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.MediaAssets = [];
                }
                else {
                    $scope.MediaAssets = [];
                }
            })
        }
        catch (e) {
            $exceptionHandler(e);
        }
    }

    function getAllIssueDates() {
        try {
            issueDateFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            $scope.IssueDates = result.data;
                        } else {
                            $scope.IssueDates = [];
                        }
                    } else {
                        $scope.IssueDates = [];
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.IssueDates = [];
                }
                else {
                    $scope.IssueDates = [];
                }
            })
        }
        catch (e) {
            $exceptionHandler(e);
        }
    }

    function getDataOnChangeofDropdown() {
        try {
            var data = $scope.proposalOrder;
            mediaOrdersFactory.getDataForPostOrdersDropDown(data, baseUrl, authToken, function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            var apiData = result.data;
                            var iDates = apiData[0].IssueDates

                            // set Issue Date Dropdown
                            if (iDates.length > 0) {
                                $scope.IssueDates = iDates;
                                var issueDate = parseInt($scope.selectedIssueDate);
                                if (issueDate > 0) {
                                    $scope.selectedIssueDate = "";
                                }
                                if ($scope.selectedIssueDate != "" && $scope.selectedIssueDate != undefined && $scope.selectedIssueDate != 0) {
                                    $scope.proposalOrder.IssueDateId = $scope.selectedIssueDate;
                                    setDroupDownValue('ddlIssueDate', ($scope.IssueDates.IssueDateId));
                                    changeSelect2DropDownColor("s2id_ddlIssueDate", $scope.IssueDates.IssueDateId);

                                } else {
                                    $scope.proposalOrder.IssueDateId = "";
                                    setDroupDownValue('ddlIssueDate', "");
                                    changeSelect2DropDownColor("s2id_ddlIssueDate", $scope.IssueDates.IssueDateId);
                                }
                            } else {
                                $scope.IssueDates = [];
                                $scope.proposalOrder.IssueDateId = "";
                                setDroupDownValue('ddlIssueDate', ($scope.IssueDates.IssueDateId));
                                changeSelect2DropDownColor("s2id_ddlIssueDate", $scope.IssueDates.IssueDateId);
                            }

                        }
                    }
                }
            })
        }
        catch (e) {
            $exceptionHandler(e);
        }
    }

    $scope.onChangeMediaAsset = function () {
        try {
            var DropDown = angular.element(document.querySelector('#ddlMediaAsset'));
            var getSelectedData = DropDown.select2("data");
            if (getSelectedData != null && getSelectedData != undefined) {
                var Id = getSelectedData.id;
                var Value = getSelectedData.text;
                if (Id != undefined && Id != "" && Id != 0 && Id != null) {
                    changeSelect2DropDownColor("s2id_ddlMediaAsset", Id);
                    getDataOnChangeofDropdown();
                } else if (Id == "") {
                    $scope.proposalOrder.MediaAssetId = "";
                    $scope.proposalOrder.IssueDateId = "";
                    changeSelect2DropDownColor("s2id_ddlMediaAsset", "");
                    changeSelect2DropDownColor("s2id_ddlIssueDate", "");
                    setDroupDownValue('ddlMediaAsset', "");
                    setDroupDownValue('ddlIssueDate', "");
                    getAllIssueDates();
                } else {
                    changeSelect2DropDownColor("s2id_ddlMediaAsset", "");
                }
            }
        }
        catch (e) {
            $exceptionHandler(e);
        }
    }

    $scope.onChangeIssueDate = function () {
        try {
            var DropDown = angular.element(document.querySelector('#ddlIssueDate'));
            var getSelectedData = DropDown.select2("data");
            if (getSelectedData != null && getSelectedData != undefined) {
                var Id = getSelectedData.id;
                var Value = getSelectedData.text;
                if (Id != undefined && Id != "" && Id != 0 && Id != null) {
                    changeSelect2DropDownColor("s2id_ddlIssueDate", Id);
                } else {
                    changeSelect2DropDownColor("s2id_ddlIssueDate", "");
                }
            }
        }
        catch (e) {
            $exceptionHandler(e);
        }
    }

    function getAllPartyDataThroughASI() {
        try {
            if ($rootScope.mainPartyAndOrganizationData.length <= 0 && $rootScope.mainAdvertisers.length <= 0 && $rootScope.mainAgencies.length <= 0
                && $rootScope.mainBillingToContacts.length <= 0) {
                var HasNext = true;
                var offset = 0;
                var advertisers = [];
                var contacts = [];
                function getPartyThroughASI() {
                    advertiserFactory.getAllAdvertiser1(baseUrl, authToken, offset, function (result) {
                        if (result != null && result != undefined && result != "") {
                            var ItemData = result.Items.$values
                            console.log(ItemData);
                            angular.forEach(ItemData, function (itemdatavalue, key) {
                                $scope.partyAndOrganizationData.push(itemdatavalue);
                                var type = itemdatavalue.$type;
                                if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                                    advertisers.push(itemdatavalue);
                                }
                                else if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                                    contacts.push(itemdatavalue);
                                }
                            });
                            $scope.advertisers = advertisers;
                            $scope.agencies = advertisers;
                            $scope.billingToContacts = contacts;
                            var TotalCount = result.TotalCount;
                            HasNext = result.HasNext;
                            offset = result.Offset;
                            var Limit = result.Limit;
                            var NextOffset = result.NextOffset;
                            if (HasNext == true) {
                                offset = NextOffset;
                                getPartyThroughASI();
                            } else {
                                $rootScope.mainPartyAndOrganizationData = $scope.partyAndOrganizationData;
                                $rootScope.mainAdvertisers = $scope.advertisers;
                                $rootScope.mainAgencies = $scope.agencies;
                                $rootScope.mainBillingToContacts = $scope.billingToContacts;
                                $scope.searchOrdersbyProposalStatus();
                            }
                        }
                        else {
                            $scope.partyAndOrganizationData = [];
                            $scope.advertisers = [];
                            $scope.agencies = [];
                            $scope.billingToContacts = [];
                            $scope.POadvertisers = [];
                        }
                    })
                }
                getPartyThroughASI();
            } else {
                $scope.partyAndOrganizationData = $rootScope.mainPartyAndOrganizationData;
                $scope.advertisers = $rootScope.mainAdvertisers;
                $scope.agencies = $rootScope.mainAgencies;
                $scope.billingToContacts = $rootScope.mainBillingToContacts;
                $scope.searchOrdersbyProposalStatus();
            }
        }
        catch (e) {
            $exceptionHandler(e);
        }
    }

    function initPagination() {
        try {
            //pagination
            var sortingOrder = '';
            $scope.sortingOrder = sortingOrder;
            $scope.reverse = false;
            $scope.filteredItems = [];
            $scope.groupedItems = [];
            $scope.itemsPerPage = 10;
            $scope.pagedItems = [];
            $scope.currentPage = 0;
            if ($scope.proposalOrders.length > 0) {
                $scope.items = $scope.proposalOrders;
            } else {
                $scope.items = [];
            }

            var searchMatch = function (haystack, needle) {
                try {
                    if (!needle) {
                        return true;
                    }
                    return haystack.toString().toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                    // return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            // init the filtered items
            $scope.search = function () {
                try {
                    $scope.filteredItems = $filter('filter')($scope.items, function (item) {
                        for (var attr in item) {
                            if (searchMatch(item[attr], $scope.query))
                                return true;
                        }
                        return false;
                    });
                    // take care of the sorting order
                    if ($scope.sortingOrder !== '') {
                        $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                    }
                    $scope.currentPage = 0;
                    // now group by pages
                    $scope.groupToPages();
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            // calculate page in place
            $scope.groupToPages = function () {
                try {
                    $scope.pagedItems = [];

                    for (var i = 0; i < $scope.filteredItems.length; i++) {
                        if (i % $scope.itemsPerPage === 0) {
                            $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [$scope.filteredItems[i]];
                        } else {
                            $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                        }
                    }
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            $scope.range = function (start, end) {
                try {
                    var ret = [];
                    if (!end) {
                        end = start;
                        start = 0;
                    }
                    for (var i = start; i < end; i++) {
                        ret.push(i);
                    }
                    return ret;
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pagedItems.length - 1) {
                    $scope.currentPage++;
                }
            };

            $scope.setPage = function () {
                $scope.currentPage = this.n;
            };

            // functions have been describe process the data for display
            $scope.search();
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function createListForProposalOrders(objProposalOrders) {
        if (objProposalOrders != null && objProposalOrders != undefined && objProposalOrders != "" && objProposalOrders.length > 0) {
            if (objProposalOrders.length > 0) {
                if ($rootScope.mainPartyAndOrganizationData.length > 0) {
                    $scope.partyAndOrganizationData = $rootScope.mainPartyAndOrganizationData;
                    angular.forEach($scope.proposalOrders, function (data, key) {
                        var AdvertiserId = data.AdvertiserId;
                        var ST_ID = data.ST_ID;
                        var objAdvertiser = alasql('SELECT * FROM ? AS add WHERE Id = ?', [$scope.partyAndOrganizationData, AdvertiserId]);
                        var objContact = alasql('SELECT * FROM ? AS add WHERE Id = ?', [$scope.partyAndOrganizationData, ST_ID]);
                        if (objAdvertiser.length > 0) {
                            $scope.proposalOrders[key].AdvertiserName = objAdvertiser[0].Name;
                        } else {
                            $scope.proposalOrders[key].AdvertiserName = "";
                        }
                        if (objContact.length > 0) {
                            $scope.proposalOrders[key].billToContactName = objContact[0].Name;
                        } else {
                            $scope.proposalOrders[key].billToContactName = "";
                        }
                    });
                    $scope.proposalOrders = alasql('SELECT * FROM ? AS add ORDER BY AdvertiserName ASC', [$scope.proposalOrders])
                    initPagination();
                    $scope.isShowDefaultTable = false;
                    $scope.dataLoading = false;
                    $scope.isShowPagination = true;

                } else {
                    $scope.proposalOrders = [];
                    $scope.isShowDefaultTable = true;
                    $scope.isShowPagination = false;
                    $scope.dataLoading = false;
                    getAllPartyDataThroughASI();
                }
            } else {
                $scope.proposalOrders = [];
                initPagination();
                $scope.isShowDefaultTable = true;
                $scope.isShowPagination = false;
                $scope.dataLoading = false;
            }
        }
    }

    $scope.searchOrdersbyProposalStatus = function () {
        var data = $scope.proposalOrder;
        DashBoardFactory.getOrdersbyProposalStatus(data, function (result) {
            if (result.statusCode == 1) {
                console.log(result);
                if (result.data != null) {
                    if (result.data.length > 0) {
                        $scope.proposalOrders = result.data;
                        createListForProposalOrders($scope.proposalOrders);
                    }
                    else {
                        $scope.proposalOrders = [];
                        $scope.isShowDefaultTable = true;
                        $scope.isShowPagination = false;
                        $scope.dataLoading = false;
                    }
                } else {
                    $scope.proposalOrders = [];
                    $scope.isShowDefaultTable = true;
                    $scope.isShowPagination = false;
                    $scope.dataLoading = false;
                }
            }
            else if (result.statusCode == 3) {
                $scope.proposalOrders = [];
                $scope.isShowDefaultTable = true;
                $scope.isShowPagination = false;
                $scope.dataLoading = false;
            }
            else {
                $scope.proposalOrders = [];
                $scope.isShowDefaultTable = true;
                $scope.isShowPagination = false;
                $scope.dataLoading = false;
                toastr.error(result.message, 'Error!');
            }
        });
    }

    getAllMediaAssets();
    getAllIssueDates();
    getAllPartyDataThroughASI();
    initPagination();
})