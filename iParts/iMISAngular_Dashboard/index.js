"use strict";

var app = angular.module("dashboardApp", ["ui.router", "ui.select2", "toastr", "ngAnimate", "gridshore.c3js.chart"])

.run(function ($rootScope, $timeout) {
    $rootScope.flag = true;
    var Url = JSON.parse(angular.element(document.querySelector("#__ClientContext")).val()).baseUrl;
    var auth = angular.element(document.querySelector("#__RequestVerificationToken")).val();
    $rootScope.iMISbaseUrl = Url;
    $rootScope.authToken = auth;

    $rootScope.mainPartyAndOrganizationData = [];
    $rootScope.mainAdvertisers = [];
    $rootScope.mainAgencies = [];
    $rootScope.mainBillingToContacts = [];

});

app.provider("authKeyService", function () {
    var privateUserList = [];
    var appConstant = {};

    var Url = JSON.parse(angular.element(document.querySelector("#__ClientContext")).val()).baseUrl;
    var auth = angular.element(document.querySelector("#__RequestVerificationToken")).val();
    appConstant.baseUrl = Url;
    appConstant.authToken = auth;

    this.getAuthKey = function (username, email) {
        return appConstant;
    };

    this.$get = function () {
        return {
            getConstantAuthKey: function () {
                return appConstant;
            },
        };
    };
});

app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', 'authKeyServiceProvider', 'toastrConfig', function ($stateProvider, $urlRouterProvider,
    $locationProvider, authKeyServiceProvider, toastrConfig) {

    var curUrl = window.location.pathname + window.location.search;
    var authKey = authKeyServiceProvider.getAuthKey();
    var baseUrl = authKey.baseUrl;
    var authToken = authKey.authToken;

    $stateProvider
        //dashboard tab router
        .state('dashBoard', {
            url: '/dashBoard',
            templateUrl: baseUrl + 'Areas/iMISAngular_Dashboard/app/views/dashboard.html',
            controller: 'DashBoardController',
            controllerAs: 'vm',
        })
     .state('dashBoard.Advertiser', {
         url: '/Advertiser',
         templateUrl: baseUrl + 'Areas/iMISAngular_Dashboard/app/views/advertiserTab.html',
         controller: 'AdvertiserTabController',
     })
     .state('dashBoard.MediaAsset', {
         url: '/MediaAsset',
         templateUrl: baseUrl + 'Areas/iMISAngular_Dashboard/app/views/mediaAssetTab.html',
         controller: 'MediaAssetTabController',
     })
    .state('dashBoard.ProposalOrder', {
        url: '/ProposalOrder',
        templateUrl: baseUrl + 'Areas/iMISAngular_Dashboard/app/views/ProposalOrderTab.html',
        controller: 'ProposalOrderTabController',
    })
    //$urlRouterProvider.otherwise('dashBoard');
    $urlRouterProvider.otherwise('dashBoard/Advertiser');

    angular.extend(toastrConfig, {
        autoDismiss: false,
        containerId: 'toast-container',
        maxOpened: 0,
        newestOnTop: true,
        positionClass: 'toast-top-right',
        preventDuplicates: false,
        preventOpenDuplicates: false,
        target: 'body',
        closeButton: true,
        iconClasses: {
            error: 'toast-error',
            info: 'toast-info',
            success: 'toast-success',
            warning: 'toast-warning'
        },
        progressBar: true
    });

}])
