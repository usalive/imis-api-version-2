﻿'use strict';

app.factory('productionStatusFactory', function ($http, $rootScope) {


    var url = "https://localhost:444/api/ProductionStatus";//sf.getServiceRoot('ue') + "api/MediaAsset";
    var cacheData = {};
    var individualAreaDetailCache = {};
    var cacheDataDistinct = null;

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getAll: function (callback) {
            $http({
                url: url,
                dataType: 'json',
                method: 'GET',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            }).success(function (result) {
                callback(result);
            }).error(function (error) {
                alert(error);
            });

        },

        save: function (PMStatus, callback) {

            return $http({
                url: url + '',
                method: 'POST',
                data: PMStatus,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

        update: function (PMStatus, callback) {

            return $http({
                url: url + '',
                method: 'PUT',
                data: PMStatus,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

        delete: function (ProductionStatusId, callback) {
            return $http({
                url: url + '/' + ProductionStatusId,
                method: 'DELETE',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
    };
});