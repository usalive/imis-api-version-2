﻿'use strict';
app.controller('ProductionManagementController', function ProductionManagementController($scope, $stateParams, positionFactory, separationFactory, productionStatusFactory,
    $exceptionHandler, productionStagesFactory, authKeyService, toastr) {
    var authKey = authKeyService.getConstantAuthKey();
    $scope.baseUrl = authKey.baseUrl;

    //Init for Position Section 
    $scope.PMPosition = {};
    $scope.PMPositions = {};
    $scope.PMPosition.PositionId = 0;
    $scope.PositionshowMsgs = false;
    $scope.isShowDefaultPositionTable = false;

    //Init for Separation Section 
    $scope.PMSeparation = {};
    $scope.PMSeparations = {};
    $scope.PMSeparation.SeparationId = 0;
    $scope.SeparationshowMsgs = false;
    $scope.isShowDefaultSeparationTable = false;

    //Init for Status Section 
    $scope.PMStatus = {};
    $scope.PMStatuses = {};
    $scope.PMStatus.ProductionStatusId = 0;
    $scope.StatusshowMsgs = false;
    $scope.isShowDefaultStatusTable = false;

    //Init for Stages Section 
    $scope.PMStage = {};
    $scope.PMStages = {};
    $scope.PMStage.ProductionStageId = 0;
    $scope.StageshowMsgs = false;
    $scope.isShowDefaultStageTable = false;

    //Position Section Code 
    function GetAllPosition() {
        try {
            positionFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data.length > 0) {
                        $scope.PMPositions = result.data;
                        $scope.isShowDefaultPositionTable = false;
                    }
                    else {
                        $scope.PMPositions = result.data;
                        $scope.isShowDefaultPositionTable = true;
                    }
                } else if (result.statusCode == 3) {
                    $scope.PMPositions = result.data;
                    $scope.isShowDefaultPositionTable = true;
                }
                else {
                    //alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function CreatePositionButtonName() {
        try {
            var positionButton = angular.element(document.querySelector('#btnPosition'));
            positionButton.text('Add');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function UpdatePositionButtonName() {
        try {
            var positionButton = angular.element(document.querySelector('#btnPosition'));
            positionButton.text('Update');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.SavePosition = function (PMPosition) {
        try {
            if ($scope.ProductionManagementPositionForm.$valid) {
                console.log(PMPosition);
                if (PMPosition.PositionId == 0) {
                    positionFactory.save(PMPosition, function (result) {
                        if (result.statusCode == 1) {
                            GetAllPosition($stateParams.mediaAssetId)
                            $scope.PMPosition = {};
                            $scope.PMPosition.PositionId = 0;
                            $scope.ProductionManagementPositionForm.$setPristine();
                            $scope.PositionshowMsgs = false;
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else
                            toastr.error(result.message, 'Error');
                    })
                } else {
                    positionFactory.update(PMPosition, function (result) {
                        if (result.statusCode == 1) {
                            GetAllPosition($stateParams.mediaAssetId)
                            $scope.PMPosition = {};
                            $scope.PMPosition.PositionId = 0;
                            CreatePositionButtonName();
                            $scope.ProductionManagementPositionForm.$setPristine();
                            $scope.PositionshowMsgs = false;
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else
                            toastr.error(result.message, 'Error');
                    })
                }
            }
            else {
                $scope.PositionshowMsgs = true;
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.editPosition = function (PMPosition) {
        try {
            UpdatePositionButtonName();
            $scope.PMPosition = PMPosition;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.deletePosition = function (objPMPosition) {
        try {
            console.log(objPMPosition);
            var selectedPosition = $scope.PMPosition;
            bootbox.confirm({
                message: "Are you sure you want to delete this Production?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        positionFactory.delete(objPMPosition.PositionId, function (result) {
                            if (result.statusCode == 1) {
                                var UpdateRecord = selectedPosition.PositionId;
                                var deletedRecord = objPMPosition.PositionId;
                                if (UpdateRecord == deletedRecord) {
                                    $scope.PMPosition = {};
                                    $scope.PMPosition.PositionId = 0;
                                    CreatePositionButtonName();
                                    $scope.ProductionManagementPositionForm.$setPristine();
                                }
                                GetAllPosition();
                                toastr.success('Deleted successfully.', 'Success!');
                            } else {
                                toastr.error(result.message, 'Error');
                            }
                        })
                    }
                }
            });

            //if (confirm("Are you sure you want to delete the selected object(s) and all of their children?")) {
            //    positionFactory.delete(objPMPosition.PositionId, function (result) {
            //        if (result.statusCode == 1) {
            //            var UpdateRecord = selectedPosition.PositionId;
            //            var deletedRecord = objPMPosition.PositionId;
            //            if (UpdateRecord == deletedRecord) {
            //                $scope.PMPosition = {};
            //                $scope.PMPosition.PositionId = 0;
            //                CreatePositionButtonName();
            //                $scope.ProductionManagementPositionForm.$setPristine();
            //            }
            //            GetAllPosition();
            //            toastr.success('Deleted successfully.', 'Success!');
            //        } else {
            //            toastr.error(result.message, 'Error');
            //        }
            //    })
            //}
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    GetAllPosition();
    //end Position

    //Separation Section Code 
    function GetAllSeparation() {
        try {
            separationFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data.length > 0) {
                        $scope.PMSeparations = result.data;
                        $scope.isShowDefaultSeparationTable = false;
                    }
                    else {
                        $scope.PMSeparations = result.data;
                        $scope.isShowDefaultSeparationTable = true;
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.PMSeparations = result.data;
                    $scope.isShowDefaultSeparationTable = true;
                }
                else {
                    //alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function CreateSeparationButtonName() {
        try {
            var separationButton = angular.element(document.querySelector('#btnSeparation'));
            separationButton.text('Add');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function UpdateSeparationButtonName() {
        try {
            var separationButton = angular.element(document.querySelector('#btnSeparation'));
            separationButton.text('Update');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.SaveSeparation = function (PMSeparation) {
        try {
            if ($scope.ProductionManagementSeparationForm.$valid) {
                console.log(PMSeparation);
                if (PMSeparation.SeparationId == 0) {
                    separationFactory.save(PMSeparation, function (result) {
                        if (result.statusCode == 1) {
                            GetAllSeparation()
                            $scope.PMSeparation = {};
                            $scope.PMSeparation.SeparationId = 0;
                            $scope.ProductionManagementSeparationForm.$setPristine();
                            $scope.SeparationshowMsgs = false;
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else
                            toastr.error(result.message, 'Error');
                    })
                } else {
                    separationFactory.update(PMSeparation, function (result) {
                        if (result.statusCode == 1) {
                            GetAllSeparation()
                            $scope.PMSeparation = {};
                            $scope.PMSeparation.SeparationId = 0;
                            CreateSeparationButtonName();
                            $scope.ProductionManagementSeparationForm.$setPristine();
                            $scope.SeparationshowMsgs = false;
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else
                            toastr.error(result.message, 'Error');
                    })
                }
            }
            else {
                $scope.SeparationshowMsgs = true;
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.editSeparation = function (PMSeparation) {
        try {
            UpdateSeparationButtonName();
            $scope.PMSeparation = PMSeparation;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.deleteSeparation = function (objPMSeparation) {
        try {
            console.log(objPMSeparation);
            var selectedSeparation = $scope.PMSeparation;

            bootbox.confirm({
                message: "Are you sure you want to delete this Production?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        separationFactory.delete(objPMSeparation.SeparationId, function (result) {
                            if (result.statusCode == 1) {
                                var UpdateRecord = selectedSeparation.SeparationId;
                                var deletedRecord = objPMSeparation.SeparationId;
                                if (UpdateRecord == deletedRecord) {
                                    $scope.PMSeparation = {};
                                    $scope.PMSeparation.SeparationId = 0;
                                    CreateSeparationButtonName();
                                    $scope.ProductionManagementSeparationForm.$setPristine();
                                }
                                GetAllSeparation()
                                toastr.success('Deleted successfully.', 'Success!');
                            } else {
                                toastr.error(result.message, 'Error');
                            }
                        })
                    }
                }
            });

        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    GetAllSeparation();
    //end Separation

    //Status Section Code 
    function GetAllStatus() {
        try {
            productionStatusFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data.length > 0) {
                        $scope.PMStatuses = result.data;
                        $scope.isShowDefaultStatusTable = false;
                    }
                    else {
                        $scope.PMStatuses = result.data;
                        $scope.isShowDefaultStatusTable = true;
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.PMStatuses = result.data;
                    $scope.isShowDefaultStatusTable = true;
                }
                else {
                    //alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function CreateStatusButtonName() {
        try {
            var statusButton = angular.element(document.querySelector('#btnStatus'));
            statusButton.text('Add');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function UpdateStatusButtonName() {
        try {
            var statusButton = angular.element(document.querySelector('#btnStatus'));
            statusButton.text('Update');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.SaveStatus = function (PMStatus) {
        try {
            if ($scope.ProductionManagementStatusForm.$valid) {
                console.log(PMStatus);
                if (PMStatus.ProductionStatusId == 0) {
                    productionStatusFactory.save(PMStatus, function (result) {
                        if (result.statusCode == 1) {
                            GetAllStatus()
                            $scope.PMStatus = {};
                            $scope.PMStatus.ProductionStatusId = 0;
                            $scope.ProductionManagementStatusForm.$setPristine();
                            $scope.StatusshowMsgs = false;
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else
                            toastr.error(result.message, 'Error');
                    })
                } else {
                    console.log(PMStatus);
                    productionStatusFactory.update(PMStatus, function (result) {
                        if (result.statusCode == 1) {
                            GetAllStatus();
                            $scope.PMStatus = {};
                            $scope.PMStatus.ProductionStatusId = 0;
                            CreateStatusButtonName();
                            $scope.ProductionManagementStatusForm.$setPristine();
                            $scope.StatusshowMsgs = false;
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else
                            toastr.error(result.message, 'Error');
                    })
                }
            }
            else {
                $scope.StatusshowMsgs = true;
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.editStatus = function (PMStatus) {
        try {
            UpdateStatusButtonName();
            $scope.PMStatus = PMStatus;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.deleteStatus = function (objPMStatus) {
        try {
            console.log(objPMStatus);
            var selectedPMStatus = $scope.PMStatus;

            bootbox.confirm({
                message: "Are you sure you want to delete this Production?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        productionStatusFactory.delete(objPMStatus.ProductionStatusId, function (result) {
                            if (result.statusCode == 1) {
                                var UpdateRecord = selectedPMStatus.ProductionStatusId;
                                var deletedRecord = objPMStatus.ProductionStatusId;
                                if (UpdateRecord == deletedRecord) {
                                    $scope.PMStatus = {};
                                    $scope.PMStatus.ProductionStatusId = 0;
                                    CreateStatusButtonName();
                                    $scope.ProductionManagementStatusForm.$setPristine();
                                }
                                GetAllStatus();
                                toastr.success('Deleted successfully.', 'Success!');
                            } else {
                                toastr.error(result.message, 'Error');
                            }
                        })
                    }
                }
            });

        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    GetAllStatus();
    //end Status

    //Status Stage Code 
    function GetAllStages() {
        try {
            productionStagesFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data.length > 0) {
                        $scope.PMStages = result.data;
                        $scope.isShowDefaultStageTable = false;
                    }
                    else {
                        $scope.PMStages = result.data;
                        $scope.isShowDefaultStageTable = true;
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.PMStages = result.data;
                    $scope.isShowDefaultStageTable = true;
                }
                else {
                    //alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function CreateStagesButtonName() {
        try {
            var stagesButton = angular.element(document.querySelector('#btnStage'));
            stagesButton.text('Add');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function UpdateStagesButtonName() {
        try {
            var stagesButton = angular.element(document.querySelector('#btnStage'));
            stagesButton.text('Update');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.SaveStage = function (PMStage) {
        try {
            if ($scope.ProductionManagementStageForm.$valid) {
                console.log(PMStage);
                if (PMStage.ProductionStageId == 0) {
                    productionStagesFactory.save(PMStage, function (result) {
                        if (result.statusCode == 1) {
                            GetAllStages()
                            $scope.PMStage = {};
                            $scope.PMStage.ProductionStageId = 0;
                            $scope.ProductionManagementStageForm.$setPristine();
                            $scope.StageshowMsgs = false;
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else
                            toastr.error(result.message, 'Error');
                    })
                } else {
                    console.log(PMStage);
                    productionStagesFactory.update(PMStage, function (result) {
                        if (result.statusCode == 1) {
                            GetAllStages()
                            $scope.PMStage = {};
                            $scope.PMStage.ProductionStageId = 0;
                            CreateStagesButtonName();
                            $scope.ProductionManagementStageForm.$setPristine();
                            $scope.StageshowMsgs = false;
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else
                            toastr.error(result.message, 'Error');
                    })
                }
            }
            else {
                $scope.StageshowMsgs = true;
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.editStage = function (PMStage) {
        try {
            UpdateStagesButtonName();
            $scope.PMStage = PMStage;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.deleteStage = function (objPMStage) {
        try {
            console.log(objPMStage);
            var selectedPMStage = $scope.PMStage;

            bootbox.confirm({
                message: "Are you sure you want to delete this Production?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        productionStagesFactory.delete(objPMStage.ProductionStageId, function (result) {
                            if (result.statusCode == 1) {
                                var UpdateRecord = selectedPMStage.ProductionStageId;
                                var deletedRecord = objPMStage.ProductionStageId;
                                if (UpdateRecord == deletedRecord) {
                                    $scope.PMStage = {};
                                    $scope.PMStage.ProductionStageId = 0;
                                    CreateStagesButtonName();
                                    $scope.ProductionManagementStageForm.$setPristine();
                                }
                                GetAllStages();
                                toastr.success('Deleted successfully.', 'Success!');
                            } else {
                                toastr.error(result.message, 'Error');
                            }
                        })
                    }
                }
            });
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    GetAllStages();
    //end Status

})