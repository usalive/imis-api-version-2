﻿'use strict';

app.factory('productionStagesFactory', function ($http, $rootScope) {

    var url = "https://localhost:444/api/ProductionStage";//sf.getServiceRoot('ue') + "api/MediaAsset";
    var cacheData = {};
    var individualAreaDetailCache = {};
    var cacheDataDistinct = null;

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getAll: function (callback) {
            $http({
                url: url,
                dataType: 'json',
                method: 'GET',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            }).success(function (result) {
                callback(result);
            }).error(function (error) {
                alert(error);
            });

        },

        save: function (PMStage, callback) {

            return $http({
                url: url + '',
                method: 'POST',
                data: PMStage,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

        update: function (PMStage, callback) {

            return $http({
                url: url + '',
                method: 'PUT',
                data: PMStage,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

        delete: function (ProductionStageId, callback) {
            return $http({
                url: url + '/' + ProductionStageId,
                method: 'DELETE',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
    };
});