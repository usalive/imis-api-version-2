﻿
'use strict';

app.factory('positionFactory', function ($http, $rootScope) {


    var url = "https://localhost:444/api/Position";//sf.getServiceRoot('ue') + "api/MediaAsset";
    var cacheData = {};
    var individualAreaDetailCache = {};
    var cacheDataDistinct = null;

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getAll: function (callback) {
            $http({
                url: url,
                dataType: 'json',
                method: 'GET',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            }).success(function (result) {
                callback(result);
            }).error(function (error) {
                alert(error);
            });
           
        },
      
        save: function (PMPosition, callback) {

            return $http({
                url: url + '',
                method: 'POST',
                data: PMPosition,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        update: function (PMPosition, callback) {

            return $http({
                url: url + '',
                method: 'PUT',
                data: PMPosition,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        delete: function (PositionId, callback) {
            return $http({
                url: url + '/' + PositionId,
                method: 'DELETE',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
    };
});