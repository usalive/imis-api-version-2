﻿'use strict';

app.factory('separationFactory', function ($http, $rootScope) {


    var url = "https://localhost:444/api/Separation";//sf.getServiceRoot('ue') + "api/MediaAsset";
    var cacheData = {};
    var individualAreaDetailCache = {};
    var cacheDataDistinct = null;

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getAll: function (callback) {
            $http({
                url: url,
                dataType: 'json',
                method: 'GET',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            }).success(function (result) {
                callback(result);
            }).error(function (error) {
                alert(error);
            });

        },

        save: function (PMSeparation, callback) {

            return $http({
                url: url + '',
                method: 'POST',
                data: PMSeparation,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

        update: function (PMSeparation, callback) {

            return $http({
                url: url + '',
                method: 'PUT',
                data: PMSeparation,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

        delete: function (SeparationId, callback) {
            return $http({
                url: url + '/' + SeparationId,
                method: 'DELETE',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
    };
});