﻿'use strict';

app.factory('repsFactory', function ($http, $rootScope) {


    var url = "https://localhost:444/api/reps";//sf.getServiceRoot('ue') + "api/MediaAsset";

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getAll: function (callback) {
            $http({
                url: url + '',
                method: 'GET',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

        save: function (rep, callback) {
            return $http({
                url: url + '',
                method: 'POST',
                data: rep,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

    };
});