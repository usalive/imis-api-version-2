﻿'use strict';

app.factory('repsTerritoryFactory', function ($http, $rootScope) {

    var url = "https://localhost:444/api/repterritories";//sf.getServiceRoot('ue') + "api/MediaAsset";
    var cacheData = {};
    var individualAreaDetailCache = {};
    var cacheDataDistinct = null;

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getAll: function (callback) {
            $http({
                url: url,
                dataType: 'json',
                method: 'GET',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            }).success(function (result) {
                callback(result);
            }).error(function (error) {
                alert(error);
            });
        },

        save: function (repsTerritory, callback) {
            return $http({
                url: url + '',
                method: 'POST',
                data: repsTerritory,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

        update: function (repsTerritory, callback) {

            return $http({
                url: url + '',
                method: 'PUT',
                data: repsTerritory,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

        delete: function (repId, territoryId, callback) {
            return $http({
                url: url + '/' + repId + '/' + territoryId,
                method: 'DELETE',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
    };
});