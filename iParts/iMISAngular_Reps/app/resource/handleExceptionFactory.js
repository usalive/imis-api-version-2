﻿app.factory('$exceptionHandler', function ($log) {
    return function myExceptionHandler(exception, cause) {
        //
        // $log.warn(exception, cause);
        $log.error(exception, cause);
        console.log(cause);
        console.log(exception.message);
    };
});