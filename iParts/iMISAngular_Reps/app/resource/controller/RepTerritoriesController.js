﻿'use strict';

app.controller('RepTerritoriesController', function RepTerritoriesController($scope, $rootScope, $stateParams, repsFactory, territoriesFactory, repsTerritoryFactory,
    $exceptionHandler, authKeyService, toastr) {
    //Main Page Initiallize
    $scope.territories = {};
    $scope.reps = {};
    $scope.repsTerritories = {};
    $scope.repsTerritory = {};
    $scope.repsTerritory.repsTerritoryId = 0;
    $scope.isShowDefaultTable = false;
    $scope.showMsgs = false;

    // Res Pop up Initiallize
    $scope.rep = {};
    $scope.rep.RepId = 0;
    $scope.repsModalMsgs = false;

    // Territory Pop up Initiallize
    $scope.Territory = {};
    $scope.Territory.TerritoryId = 0;
    $scope.TerritoryModalMsgs = false;

    $scope.isLoading = true;
    $scope.dataLoading = true;

    // Global variable for controller
    var authKey = authKeyService.getConstantAuthKey();
    $scope.baseUrl = authKey.baseUrl;

    clearSelect2();

    function GetAllReps() {
        try {
            repsFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    $scope.reps = result.data;
                }
                else {
                    //alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function GetAllTerritory() {
        try {
            territoriesFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    if (result.data.length > 0) {
                        console.log(result);
                        $scope.territories = result.data;
                    }
                }
                else {
                    //alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function GetAllRepsTerritory() {
        try {
            repsTerritoryFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data.length > 0) {
                        $scope.repsTerritories = result.data;
                        $scope.isShowDefaultTable = false;
                        $scope.dataLoading = false;
                    }
                    else {
                        $scope.repsTerritories = [];
                        $scope.isShowDefaultTable = true;
                        $scope.dataLoading = false;
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.repsTerritories = [];
                    $scope.isShowDefaultTable = true;
                    $scope.dataLoading = false;
                }
                else {
                    $scope.repsTerritories = [];
                    $scope.isShowDefaultTable = true;
                    $scope.dataLoading = false;
                    //alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function UpdateAdAdjustmentButtonName() {
        try {
            var repsTerritoryButton = angular.element(document.querySelector('#btnRepsTerritory'));
            repsTerritoryButton.text('Update');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function CreateAdAdjustmentButtonName() {
        try {
            var repsTerritoryButton = angular.element(document.querySelector('#btnRepsTerritory'));
            repsTerritoryButton.text('Create');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.SaveRepsTerritory = function (repsTerritory) {
        try {
            repsTerritory.Commission = parseFloat(repsTerritory.Commission);
            if ($scope.salesRepsCommissionForm.$valid) {
                console.log(repsTerritory);
                if (repsTerritory.repsTerritoryId == 0) {
                    repsTerritoryFactory.save(repsTerritory, function (result) {
                        if (result.statusCode == 1) {
                            GetAllRepsTerritory();
                            $scope.repsTerritory = {};
                            $scope.repsTerritory.repsTerritoryId = 0;
                            CreateAdAdjustmentButtonName();
                            clearSelect2();
                            $scope.salesRepsCommissionForm.$setPristine();
                            $scope.showMsgs = false;
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else
                            toastr.error(result.message, 'Error');
                    })
                } else {
                    repsTerritoryFactory.update(repsTerritory, function (result) {
                        if (result.statusCode == 1) {
                            GetAllRepsTerritory();
                            $scope.repsTerritory = {};
                            $scope.repsTerritory.repsTerritoryId = 0;
                            CreateAdAdjustmentButtonName();
                            clearSelect2();
                            $scope.salesRepsCommissionForm.$setPristine();
                            $scope.showMsgs = false;
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else
                            toastr.error(result.message, 'Error');
                    })
                }
            }
            else {
                $scope.showMsgs = true;
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.EditRepsTerritory = function (repsTerritory) {
        try {
            UpdateAdAdjustmentButtonName();
            $scope.repsTerritory = repsTerritory;
            setSelect2(repsTerritory);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.DeleteRepsTerritory = function (objrepsTerritory) {
        try {
            console.log(objrepsTerritory);
            var selectedrepsTerritory = $scope.repsTerritory;
            bootbox.confirm({
                message: "Are you sure you want to delete this Sales Reps?",
                //message: "Are you sure you want to delete the selected object(s) and all of their children?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        repsTerritoryFactory.delete(objrepsTerritory.RepId, objrepsTerritory.TerritoryId, function (result) {
                            if (result.statusCode == 1) {
                                var UpdateRepsRecord = selectedrepsTerritory.RepId;
                                var deletedRepsRecord = objrepsTerritory.RepId;
                                var UpdateTerRecord = selectedrepsTerritory.TerritoryId;
                                var deletedTerRecord = objrepsTerritory.TerritoryId;
                                if (UpdateRepsRecord == deletedRepsRecord && UpdateTerRecord == deletedTerRecord) {
                                    $scope.repsTerritory = {};
                                    $scope.repsTerritory.repsTerritoryId = 0;
                                    CreateAdAdjustmentButtonName();
                                    clearSelect2();
                                    $scope.salesRepsCommissionForm.$setPristine();
                                }
                                GetAllRepsTerritory();
                                toastr.success('Deleted successfully.', 'Success!');
                            }
                            else {
                                toastr.error(result.message, 'Error!');
                            }
                        })
                    }
                }
            });
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    // Init Pop up 
    $scope.InitRepsPopup = function () {
        try {
            $scope.rep = {};
            $scope.rep.RepId = 0;
            $scope.repsModalForm.$setPristine();
            $scope.repsModalMsgs = false;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.InitTerritoryPopup = function () {
        try {
            $scope.Territory = {};
            $scope.Territory.TerritoryId = 0;
            $scope.TerritoryModalForm.$setPristine();
            $scope.TerritoryModalMsgs = false;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    // Res Pop up Code
    function CreateRepsButtonName() {
        try {
            var repsButton = angular.element(document.querySelector('#btnreps'));
            repsButton.text('Create');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.SaveReps = function (rep) {
        try {
            if ($scope.repsModalForm.$valid) {
                console.log(rep);
                if (rep.RepId == 0) {
                    repsFactory.save(rep, function (result) {
                        if (result.statusCode == 1) {
                            $scope.rep = {};
                            $scope.rep.RepId = 0;
                            GetAllReps();
                            CreateRepsButtonName();
                            var Modal = angular.element(document.querySelector('#repsModal'))
                            Modal.modal('hide');
                            $scope.repsModalForm.$setPristine();
                            $scope.repsModalMsgs = false;
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else
                            toastr.error(result.message, 'Error');
                    })
                } else {

                }
            }
            else {
                $scope.repsModalMsgs = true;
                var repModal = angular.element(document.querySelector('#repsModal'))
                repModal.show();
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.CancelReps = function (mediaAsset) {
        try {
            console.log('Cancel called');
            $scope.repsModalForm.$setPristine();
            $scope.repsModalMsgs = false;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    // Territory Pop up
    function CreateTerritoryButtonName() {
        try {
            var TerritoryButton = angular.element(document.querySelector('#btnTerritory'));
            TerritoryButton.text('Create');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.SaveTerritory = function (Territory) {
        try {
            if ($scope.TerritoryModalForm.$valid) {
                console.log(Territory);
                if (Territory.TerritoryId == 0) {
                    territoriesFactory.save(Territory, function (result) {
                        if (result.statusCode == 1) {
                            $scope.Territory = {};
                            $scope.Territory.TerritoryId = 0;
                            CreateTerritoryButtonName();
                            GetAllTerritory();
                            var Modal = angular.element(document.querySelector('#TerritoryModal'))
                            Modal.modal('hide');
                            $scope.TerritoryModalForm.$setPristine();
                            $scope.TerritoryModalMsgs = false;
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else
                            toastr.error(result.message, 'Error');
                    })
                } else {

                }
            }
            else {
                $scope.TerritoryModalMsgs = true;
                var TerritoryModal = angular.element(document.querySelector('#TerritoryModal'))
                TerritoryModal.show();
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.CancelTerritory = function (Territory) {
        try {
            console.log('Cancel called');
            $scope.TerritoryModalForm.$setPristine();
            $scope.TerritoryModalMsgs = false;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function setSelect2(objrepsTerritory) {
        try {
            $scope.repsTerritory.RepId = objrepsTerritory.RepId.toString();
            $scope.repsTerritory.TerritoryId = objrepsTerritory.TerritoryId.toString();
            initSelect2("s2id_ddlReps", objrepsTerritory.RepName);
            initSelect2("s2id_ddlTerritory", objrepsTerritory.TerritoryName);
            changeSelect2DropDownColor("s2id_ddlReps", objrepsTerritory.RepId);
            changeSelect2DropDownColor("s2id_ddlTerritory", objrepsTerritory.TerritoryId);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function clearSelect2() {
        try {
            initSelect2("s2id_ddlReps", "Select Reps");
            initSelect2("s2id_ddlTerritory", "Select Territory");
            changeSelect2DropDownColor("s2id_ddlReps", "");
            changeSelect2DropDownColor("s2id_ddlTerritory", "");
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }


    $scope.onRepsChange = function () {
        try {
            var DropDown = angular.element(document.querySelector('#ddlReps'));
            var getSelectedData = DropDown.select2("data");
            var Id = getSelectedData.id;
            var Value = getSelectedData.text;
            if (Id != undefined && Id != "" && Id != 0 && Id != null) {
                changeSelect2DropDownColor("s2id_ddlReps", Id);
            } else {
                changeSelect2DropDownColor("s2id_ddlReps", "");
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onTerritoryChange = function () {
        try {
            var DropDown = angular.element(document.querySelector('#ddlTerritory'));
            var getSelectedData = DropDown.select2("data");
            var Id = getSelectedData.id;
            var Value = getSelectedData.text;
            if (Id != undefined && Id != "" && Id != 0 && Id != null) {
                changeSelect2DropDownColor("s2id_ddlTerritory", Id);
            } else {
                changeSelect2DropDownColor("s2id_ddlTerritory", "");
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    // Common function for Select2
    function changeSelect2DropDownColor(controlId, controlValue) {
        try {
            if (controlValue != "" && controlValue != undefined && controlValue != null) {
                // id would be dynamic generated by select2
                var DropDown = angular.element(document.querySelector('#' + controlId));
                var select2ChosenDiv = DropDown.find('.select2-chosen');
                select2ChosenDiv.css('color', '#323232');
            } else {
                var DropDown = angular.element(document.querySelector('#' + controlId));
                var select2ChosenDiv = DropDown.find('.select2-chosen');
                select2ChosenDiv.css('color', '#999999');
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }
    function initSelect2(id, text) {
        try {
            var stateSelect2Button = angular.element(document.querySelector('#' + id))
            var selectedState = stateSelect2Button.find('span.select2-chosen');
            selectedState.text(text);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }
    function setDroupDownValue(id, value) {
        try {
            var adDropDown = angular.element(document.querySelector('#' + id));
            adDropDown.select2('val', value);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }



    GetAllReps();
    GetAllTerritory();
    GetAllRepsTerritory();
})