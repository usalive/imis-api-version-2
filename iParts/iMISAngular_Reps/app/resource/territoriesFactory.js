﻿'use strict';

app.factory('territoriesFactory', function ($http, $rootScope) {


    var url = "https://localhost:444/api/territories";//sf.getServiceRoot('ue') + "api/MediaAsset";

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getAll: function (callback) {
            $http({
                url: url + '',
                method: 'GET',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

        save: function (repsTerritory, callback) {
            return $http({
                url: url + '',
                method: 'POST',
                data: repsTerritory,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

    };
});