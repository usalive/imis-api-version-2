"use strict";

var app = angular.module("mediaAssetsApp", ["ui.router", "angular-toArrayFilter", "ngMessages", "ui.select2", "toastr"])

app.run(function ($rootScope, $timeout) {
    $rootScope.flag = true;
    var Url = JSON.parse(angular.element(document.querySelector("#__ClientContext")).val()).baseUrl;
    var auth = angular.element(document.querySelector("#__RequestVerificationToken")).val();
    $rootScope.iMISbaseUrl = Url;
});

app.provider("authKeyService", function () {
    var privateUserList = [];
    var appConstant = {};

    var Url = JSON.parse(angular.element(document.querySelector("#__ClientContext")).val()).baseUrl;
    var auth = angular.element(document.querySelector("#__RequestVerificationToken")).val();
    appConstant.baseUrl = Url;
    appConstant.authToken = auth;

    this.getAuthKey = function (username, email) {
        return appConstant;
    };

    this.$get = function () {
        return {
            getConstantAuthKey: function () {
                return appConstant;
            },
        };
    };
});

app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', 'authKeyServiceProvider', 'toastrConfig', function ($stateProvider, $urlRouterProvider, $locationProvider, authKeyServiceProvider, toastrConfig) {
    //$locationProvider.hashPrefix('');

    //$locationProvider.html5Mode({
    //    enabled: true,
    //    requireBase: false,
    //    rewriteLinks: false
    //});
    //$urlMatcherFactoryProvider.type('SlashFix', {
    //    raw: true,
    //});

    var curUrl = window.location.pathname + window.location.search;
    var authKey = authKeyServiceProvider.getAuthKey();
    var baseUrl = authKey.baseUrl;

    $stateProvider
      .state('repsTerritory', {
          url: '/repsTerritory',
          templateUrl: baseUrl + 'Areas/iMISAngular_Reps/app/views/repsTerritory.html',
          controller: 'RepTerritoriesController',
          controllerAs: "vm"
      })
    $urlRouterProvider.otherwise('repsTerritory');

    angular.extend(toastrConfig, {
        autoDismiss: false,
        containerId: 'toast-container',
        maxOpened: 0,
        newestOnTop: true,
        positionClass: 'toast-top-right',
        preventDuplicates: false,
        preventOpenDuplicates: false,
        target: 'body',
        closeButton: true,
        iconClasses: {
            error: 'toast-error',
            info: 'toast-info',
            success: 'toast-success',
            warning: 'toast-warning'
        },
        progressBar: true
    });
}])
