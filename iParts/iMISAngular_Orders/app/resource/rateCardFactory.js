﻿'use strict';

app.factory('rateCardFactory', function ($http, $rootScope) {

    var url = "https://localhost:444/api/RateCard";//sf.getServiceRoot('ue') + "api/MediaAsset";


    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getAll: function (callback) {

            $http({
                url: url + '',
                method: 'GET',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

        getByMediaAsset: function (mediaAssetId, callback) {

            $http({
                url: url + '/getbymediaasset/' + mediaAssetId,
                method: 'GET',
                headers: sfHeaders,
                params: {},
                cache: false,
                async:false
            }).success(function (result) {
                callback(result);
            });
        },

        save: function (rateCard, callback) {

            return $http({
                url: url + '',
                method: 'POST',
                data: rateCard,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        update: function (rateCard, callback) {

            return $http({
                url: url + '',
                method: 'PUT',
                data: rateCard,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        delete: function (rateCardId, callback) {

            return $http({
                url: url + '/' + rateCardId,
                method: 'DELETE',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
    };
});