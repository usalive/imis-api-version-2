﻿'use strict';

app.factory('mediaOrderProductionFactory', function ($http, $rootScope) {

    var url = "https://localhost:444/api/mediaorderproductiondetails";//sf.getServiceRoot('ue') + "api/MediaAsset";
    var cacheData = {};
    var individualAreaDetailCache = {};
    var cacheDataDistinct = null;

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        save: save,
        getById:getById,
    };


    function save(mediaOrder, callback) {
        $http({
            url: url,
            dataType: 'json',
            method: 'POST',
            data: mediaOrder,
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }
    function getById (mOrderId,callback) {
        $http({
            url: url+'/getbymediaorder/'+mOrderId,
            dataType: 'json',
            method: 'GET',
            data: '',
            headers: {
                "Content-Type": "application/json"
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });

    }


});