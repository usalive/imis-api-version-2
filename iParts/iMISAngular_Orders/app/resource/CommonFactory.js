﻿'use strict';

app.factory('commonFactory', function ($http, $rootScope) {

    var url = "https://localhost:444/api/MediaAsset";//sf.getServiceRoot('ue') + "api/MediaAsset";
    var cacheData = {};
    var individualAreaDetailCache = {};
    var cacheDataDistinct = null;

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    var returnadvertiserFactory = {
        getCountry: getCountry,
    }
    return returnadvertiserFactory;


    function getCountry(baseUrl, authToken, callback) {
        $http({
            url: baseUrl + "api/country?limit=500",
            dataType: 'json',
            method: 'GET',
            data: '',
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

});