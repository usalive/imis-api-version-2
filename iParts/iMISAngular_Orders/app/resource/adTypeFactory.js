﻿'use strict';

app.factory('AdTypeFactory', function ($http, $rootScope) {

    var url = "https://localhost:444/api/adtype";//sf.getServiceRoot('ue') + "api/MediaAsset";

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getAll: function (callback) {
            $http({
                url: url + '',
                method: 'GET',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

        getMediaOrdersAdType: function (baseUrl, authToken, callback) {
            $http({
                url: url + "/getalladtypeforpostorder",
                dataType: 'json',
                method: 'GET',
                data: '',
                headers: {
                    "Content-Type": "application/json",
                    'RequestVerificationToken': authToken
                }
            }).success(function (result) {
                callback(result);
            }).error(function (error) {
                console.log(error);
            });
        },

        save: function (adAdjustments, callback) {

            return $http({
                url: url + '',
                method: 'POST',
                data: adAdjustments,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        delete: function (AdAdjustmentId, callback) {

            return $http({
                url: url + '/' + AdAdjustmentId,
                method: 'DELETE',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
    };
});