﻿'use strict';

app.factory('rateCardCostFactory', function ($http, $rootScope) {

    var url = "https://localhost:444/api/RateCardDetail";//sf.getServiceRoot('ue') + "api/MediaAsset";


    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getCost: function (rateCardId, adColorId, adSizeId, frequencyId, callback) {

            $http({
                url: url + '/GetRateCardCost/' + rateCardId + '/' + adColorId + '/' + adSizeId + '/' + frequencyId,
                method: 'GET',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        getRateCardCostForPerWord: function (rateCardId, adColorId, callback) {

            $http({
                url: url + '/GetRateCardsCostForPerWord/' + rateCardId + '/' + adColorId,
                method: 'GET',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },



    };
});