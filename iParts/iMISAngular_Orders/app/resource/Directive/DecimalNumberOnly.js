﻿app.directive('allowDecimalNumbers', function () {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs, ctrl) {
            elm.on('keydown', function (event) {
                //var $input = $(this);
                var $input = this;
                //var value = $input.val();
                var value = this.value;
                value = value.replace(/[^0-9\.]/g, '')
                var findsDot = new RegExp(/\./g)
                var containsDot = value.match(findsDot)
                if (containsDot != null && ([110, 190].indexOf(event.which) > -1)) {
                    event.preventDefault();
                    return false;
                }
                // $input.val(value);
                $input.value = value;
                if (event.which == 64 || event.which == 16) {
                    // numbers  
                    return false;
                } if ([8, 13, 27, 37, 38, 39, 40, 110, 46, 9, 20, 16].indexOf(event.which) > -1) {
                    // backspace, enter, escape, arrows ,delete,tab,capsloc,shift 
                    return true;
                } else if (event.which >= 48 && event.which <= 57) {
                    // numbers  
                    return true;
                } else if (event.which >= 96 && event.which <= 105) {
                    // numpad number  
                    return true;
                } else if ([46, 110, 190].indexOf(event.which) > -1) {
                    // dot and numpad dot  
                    //return true;

                    event.preventDefault();
                    return false;
                } else {
                    event.preventDefault();
                    return false;
                }
            });
        }
    }
});

app.directive('allowDecimalNumbersDot', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs, ctrl) {
            elm.on('keydown', function (event) {
                
                //var $input = $(this);
                var $input = this;
                //var value = $input.val();
                var value = this.value;
                value = value.replace(/[^0-9\.]/g, '')
                var findsDot = new RegExp(/\./g)
                var containsDot = value.match(findsDot)
                if (containsDot != null && ([110, 190].indexOf(event.which) > -1)) {
                    event.preventDefault();
                    return false;
                }
                // $input.val(value);
                $input.value = value;
                var modelName = $input.getAttribute('ng-model');

                scope.$watch(modelName, function (newValue, oldValue) {
                    //if (newValue)
                    //    console.log("I see a data change!");
                    if (newValue && (parseFloat(newValue) >= 0 && parseFloat(newValue) <= 100) || newValue == undefined || newValue == "") {
                        // $scope[modelName] = oldValue;
                    }
                    else {
                        var text = oldValue;
                        if (text.length > 3) {
                            var hValue = text.substring(0, 3);
                            if (hValue == 100) {
                                var the_string = modelName;
                                var model = $parse(the_string);
                                model.assign(scope, hValue);
                            } else {
                                var tValue = hValue.substring(0, 2);
                                // tValue = tValue.substring(0, 2);
                                var the_string = modelName;
                                var model = $parse(the_string);
                                model.assign(scope, tValue);
                            }
                        } else {
                            var text1 = text;
                            var the_string = modelName;
                            var model = $parse(the_string);
                            model.assign(scope, text1);
                        }

                    }
                }, true);

                if (event.which == 64 || event.which == 16) {
                    // numbers  
                    return false;
                } if ([8, 13, 27, 37, 38, 39, 40, 110, 46, 9, 20, 16].indexOf(event.which) > -1) {
                    // backspace, enter, escape, arrows ,delete,tab,capsloc,shift 
                    return true;
                } else if (event.which >= 48 && event.which <= 57) {
                    // numbers  
                    return true;
                } else if (event.which >= 96 && event.which <= 105) {
                    // numpad number  
                    return true;
                } else if ([46, 110, 190].indexOf(event.which) > -1) {
                    // dot and numpad dot  
                    return true;
                } else {
                    event.preventDefault();
                    return false;
                }
            });

        }
    }
});

app.directive('bindOnce', function () {
    return {
        scope: true,
        link: function ($scope) {
            setTimeout(function () {
                $scope.$destroy();
            }, 0);
        }
    }
});

//app.factory('focus', function ($timeout, $window) {
//    return function (id) {
//        // timeout makes sure that is invoked after any other event has been triggered.
//        // e.g. click events that need to run before the focus or
//        // inputs elements that are in a disabled state but are enabled when those events
//        // are triggered.
//        $timeout(function () {
//            var element = $window.document.getElementById(id);
//            if (element)
//                element.focus();
//        });
//    };
//})

//app.directive('focus', function ($timeout, $parse) {
//    return {
//        link: function (scope, element, attrs) {
//            var model = $parse(attrs.focus);
//            scope.$watch(model, function (value) {
//                if (value === true) {
//                    $timeout(function () {
//                        element[0].focus();
//                    });
//                }
//            });
//        }
//    };
//});

app.directive('focusMe', function ($timeout) {
    return {
        scope: { trigger: '=focusMe' },
        link: function (scope, element) {
            scope.$watch('trigger', function (value) {
                if (value === true) {
                    //console.log('trigger',value);
                    $timeout(function () {
                        debugger;
                        element[0].focus();
                        element.focusin();
                        scope.trigger = false;
                    }, 1000);
                }
            });
        }
    };
});
