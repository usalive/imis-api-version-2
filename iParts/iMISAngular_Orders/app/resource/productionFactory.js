﻿'use strict';

app.factory('productionFactory', function ($http, $rootScope) {

    var url = "https://localhost:444/api/";
    var cacheData = {};
    var individualAreaDetailCache = {};
    var cacheDataDistinct = null;

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        saveProduction: saveProduction,
    };

    function saveProduction(order, callback) {
        return $http({
            url: url + '',
            method: 'POST',
            data: order,
            headers: sfHeaders,
            cache: false
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });;
    }

});