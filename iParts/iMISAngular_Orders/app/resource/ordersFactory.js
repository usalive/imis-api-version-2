﻿'use strict';

app.factory('ordersFactory', function ($http, $rootScope) {

    var url = "https://localhost:444/api/adorder";//sf.getServiceRoot('ue') + "api/MediaAsset";
    var generateIdURL = "https://localhost:444/api/";
    var cacheData = {};
    var individualAreaDetailCache = {};
    var cacheDataDistinct = null;

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getOrganisationById: getOrganisationById,
        saveOrder: saveOrder,
        getOrderId: getOrderId,
        generateBuyId: generateBuyId,
        getMediaOrderbyBuyId: getMediaOrderbyBuyId,
        getordersbuyIdwiseforcopy: getordersbuyIdwiseforcopy,
    };

    function saveOrder(order, callback) {
        return $http({
            url: url + '',
            method: 'POST',
            data: order,
            headers: sfHeaders,
            cache: false
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });;
    }

    function getOrganisationById(baseUrl, authToken, id, callback) {
        $http({
            url: baseUrl + "api/Party/" + id,
            dataType: 'json',
            method: 'GET',
            data: '',
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    //api/adorder/createorder
    function getOrderId(callback) {
        $http({
            url: url + "/createorder",
            dataType: 'json',
            method: 'POST',
            data: '',
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function generateBuyId(callback) {
        $http({
            url: generateIdURL + "mediaorder/generatebuyid",
            dataType: 'json',
            method: 'GET',
            data: '',
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function getMediaOrderbyBuyId(byID, callback) {
        $http({
            url: generateIdURL + "mediaorder/getbybuyid/" + byID,
            dataType: 'json',
            method: 'GET',
            data: '',
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function getordersbuyIdwiseforcopy(byID, callback) {
        $http({
            url: generateIdURL + "mediaorder/getordersbuyIdwiseforcopy/" + byID,
            dataType: 'json',
            method: 'GET',
            data: '',
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

});