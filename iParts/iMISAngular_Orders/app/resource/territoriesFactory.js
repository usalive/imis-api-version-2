﻿'use strict';

app.factory('territoriesFactory', function ($http, $rootScope) {


    var url = "https://localhost:444/api/repterritories";//sf.getServiceRoot('ue') + "api/MediaAsset";

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getAll: function (id,callback) {
            $http({
                url: url + '/getbyrep/'+id,
                method: 'GET',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

        save: function (repsTerritory, callback) {
            return $http({
                url: url + '',
                method: 'POST',
                data: repsTerritory,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

    };
});