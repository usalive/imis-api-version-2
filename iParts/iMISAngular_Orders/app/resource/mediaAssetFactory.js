﻿'use strict';

app.factory('mediaAssetFactory', function ($http, $rootScope) {


    var url = "https://localhost:444/api/MediaAsset";//sf.getServiceRoot('ue') + "api/MediaAsset";
    var url2 = "https://localhost:444/api/mediaOrder";
    var cacheData = {};
    var individualAreaDetailCache = {};
    var cacheDataDistinct = null;

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getAll: function (callback) {

            $http({
                url: url,
                method: 'GET',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        get: function (mediaAssetId, callback) {

            $http({
                url: url + '/' + mediaAssetId,
                method: 'GET',
                headers: sfHeaders,
                params: {},
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        save: function (mediaAsset, callback) {

            return $http({
                url: url2,
                method: 'POST',
                data: mediaAsset,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        update: function (mediaAsset, callback) {

            return $http({
                url: url2,
                method: 'PUT',
                data: mediaAsset,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        delete: function (mediaAssetId, callback) {

            return $http({
                url: url + '/' + mediaAssetId,
                method: 'DELETE',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        getAllItemsummary: function (baseUrl, authToken, callback) {
            $http({
                //url: "/Scripts/itemCode.txt",
                url: baseUrl + "api/Itemsummary?limit=1000000",
                dataType: 'json',
                method: 'GET',
                data: '',
                headers: {
                    "Content-Type": "application/json",
                    'RequestVerificationToken': authToken
                }
            }).success(function (result) {
                callback(result);
            }).error(function (error) {
                console.log(error);
            });
        },
        getOrdersNotInBuyer: function (buyId,adid, callback) {

            $http({
                url: url2 + '/getordersnotinbuyer/' + buyId + '/' + adid,
                method: 'GET',
                headers: sfHeaders,
                params: {},
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
    };
});