﻿'use strict';

app.factory('agencyFactory', function ($http, $rootScope) {

    var url = "https://localhost:444/api/MediaAsset";//sf.getServiceRoot('ue') + "api/MediaAsset";
    var cacheData = {};
    var individualAreaDetailCache = {};
    var cacheDataDistinct = null;

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getAll: getAll

        //getAll: function (callback) {
        //    $http({
        //        url: url,
        //        dataType: 'json',
        //        method: 'GET',
        //        data: '',
        //        headers: {
        //            "Content-Type": "application/json"
        //        }
        //    }).success(function (result) {
        //        callback(result);
        //    }).error(function (error) {
        //        alert(error);
        //    });
        //},

        //save: function (agency, callback) {
        //    return $http({
        //        url: url + '',
        //        method: 'POST',
        //        data: agency,
        //        headers: sfHeaders,
        //        cache: false
        //    }).success(function (result) {
        //        callback(result);
        //    });
        //},

        //update: function (agency, callback) {

        //    return $http({
        //        url: url + '',
        //        method: 'PUT',
        //        data: agency,
        //        headers: sfHeaders,
        //        cache: false
        //    }).success(function (result) {
        //        callback(result);
        //    });
        //},

        //delete: function (agencyId, callback) {
        //    return $http({
        //        url: url + '/' + agencyId,
        //        method: 'DELETE',
        //        headers: sfHeaders,
        //        cache: false
        //    }).success(function (result) {
        //        callback(result);
        //    });
        //},
    };

    function getAll() {
        var objdata = {};
        var Arraydata = [];
        objdata.id = 1;
        objdata.Name = "Agency1";
        Arraydata.push(objdata);
        objdata = {};
        objdata.id = 2;
        objdata.Name = "Agency2";
        Arraydata.push(objdata);
        objdata = {};
        objdata.id = 3;
        objdata.Name = "Agency3";
        Arraydata.push(objdata);
        objdata = {};
        objdata.id = 4;
        objdata.Name = "Agency4";
        Arraydata.push(objdata);
        objdata = {};
        objdata.id = 5;
        objdata.Name = "Agency5";
        Arraydata.push(objdata);
        return Arraydata;
    }

});