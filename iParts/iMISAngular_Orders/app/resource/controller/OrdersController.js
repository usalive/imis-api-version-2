﻿'use strict';

app.controller('OrdersController', function OrdersController($scope, $stateParams, $compile, $sce, $parse, $location, $rootScope, $timeout, advertiserFactory, agencyFactory,
    $window, ordersFactory, commonFactory, $exceptionHandler, billingToContactFactory, repsFactory, territoriesFactory, mediaOrdersFactory, authKeyService, toastr) {
    // console.log("controller called");

    $scope.tabOrderId = $scope.$parent.tabOrderId;
    $scope.personData = {};
    $scope.partyAndOrganizationData = [];
    $scope.organizationWiseParty = [];
    //$scope.setBillToContactClickable = true;
    $scope.isRequired = false;
    var GrossCost = $rootScope.GrossCost;
    var NetCost = $rootScope.NetCost;
    $scope.orderGrossCost = parseFloat(GrossCost).toFixed(2);
    $scope.orderNetCost = parseFloat(NetCost).toFixed(2);

    //Init Orders
    // $scope.AdOrderId = 0;
    $scope.order = {};
    if ($stateParams.AdOrderId != undefined && $stateParams.AdOrderId != "" && $stateParams.AdOrderId != null) {
        $scope.order.AdOrderId = $stateParams.AdOrderId;
    }
    $scope.orders = {};
    $scope.showMsgs = false;
    $scope.order.BillTo = 0;
    $scope.isShowDefaultTable = true;
    $scope.setAddLink = true;
    $scope.setNoClick = true;
    $scope.selectedAdverdtiser = "";
    $scope.selectedAgency = "";
    $scope.selectedContactValue = "";
    $scope.selectedReps = "";
    $scope.selectedTerritories = "";
    $scope.showRepsMsgs = false;

    //Init Advertiser 
    $scope.advertiser = {};
    $scope.advertiser.AdvertiserId = 0;
    $scope.advertisers = [];
    $scope.advertiserModalMsgs = false;
    $scope.isStatehave = false;
    $scope.SubEntityNameCaption = "State";
    $scope.countries = {};
    //$scope.countries.countryId = 0;
    $scope.states = {};
    $scope.advertiser.CountrySubEntity = "";

    //Init Agency 
    $scope.agency = {};
    $scope.agency.AgencyId = 0;
    $scope.agencies = {};
    $scope.agencyModalMsgs = false;
    $scope.isStatehave_Agency = false;
    $scope.SubEntityNameCaption_Agency = "State";
    $scope.countries_Agency = {};
    $scope.states_Agency = {};
    $scope.agency.CountrySubEntity_Agency = "";

    //Init BillingToContact 
    $scope.contact = {};
    $scope.contact.ContactId = 0;
    $scope.billingToContacts = {};
    $scope.contactModalMsgs = false;
    $scope.isStatehave_Contact = false;
    $scope.SubEntityNameCaption_Contact = "State";
    $scope.$parent.setOrdersTabActive();
    $scope.contactSameAaAbove = 0;
    $scope.contact.CountrySubEntity_Contact = "";

    //Init RepsTerritories
    $scope.reps = {};
    $scope.repsId = 0
    $scope.repsName = "";
    $scope.territoryId = 0
    $scope.territoryName = "";
    // $scope.commission = 0;
    $scope.Commission = 0;
    $scope.tempArray = [];
    $scope.repTerritoryArray = [];
    $scope.parentArray = [];
    $scope.mediaOrderId = "";

    $scope.$parent.setOrdersTabActive();
    function Timer(id) {
        var self = this;
        self.id = id;
        var _times = [];
        self.start = function () {
            var time = performance.now();
            console.log('[' + id + '] Start');
            _times.push(time);
        }
        self.lap = function (time) {
            time = time ? time : performance.now();
            console.log('[' + id + '] Lap ' + time - _times[_times.length - 1]);
            _times.push(time);
        }
        self.stop = function () {
            var time = performance.now();
            if (_times.length > 1) {
                self.lap(time);
            }
            console.log('[' + id + '] Stop ' + (time - _times[0]));
            _times = [];
        }
    }
    
    $scope.advertiserSelect = {
        multiple: false,
        formatSearching: 'Searching the advertiser...',
        formatNoMatches: 'No advertiser found'
    };

    $scope.agencySelect = {
        multiple: false,
        formatSearching: 'Searching the agency...',
        formatNoMatches: 'No agency found'
    };

    $scope.billToContactSelect = {
        multiple: false,
        formatSearching: 'Searching the billToContact...',
        formatNoMatches: 'No billToContact found'
    };

    $scope.countrySelect = {
        multiple: false,
        formatSearching: 'Searching the country...',
        formatNoMatches: 'No country found'
    };

    $scope.stateSelect = {
        multiple: false,
        formatSearching: 'Searching the state...',
        formatNoMatches: 'No state found'
    };

    $scope.memberTypeSelect = {
        multiple: false,
        formatSearching: 'Searching the memberType...',
        formatNoMatches: 'No memberType found'
    };

    $scope.partyAddress = "";
    $scope.partyMobile = "";
    $scope.partyMemberType = "";
    $scope.partyWork = "";
    $scope.partyEmail = "";
    $scope.partyHome = "";

    $scope.dynamicRowData = [];
    var globalSalesValue = [];
    $scope.isEditMode = false;
    $scope.isLoading = true;

    var authKey = authKeyService.getConstantAuthKey();
    var baseUrl = authKey.baseUrl;
    var authToken = authKey.authToken;

    function fillPartyComingDashBoard() {
        try {
            //////StartLoading();
            var OrganizationId = ($rootScope.dashboardOrganizationId);
            var ContactId = $rootScope.dashboardContactId;

            if (parseInt(OrganizationId) > 0 && parseInt(ContactId) > 0) {

                $scope.order.Adverdtiser = OrganizationId;
                $scope.order.BillingContacts = ContactId;

                setDroupDownValue('ddlAdverdtiser', OrganizationId);
                setDroupDownValue('ddlContact', ContactId);
                changeSelect2DropDownColor("s2id_ddlContact", parseInt(ContactId));

                var organizationData = alasql('SELECT * FROM ? AS add WHERE Id = ?', [$scope.partyAndOrganizationData, ContactId]);
                var text = organizationData[0].Name;
                var id = organizationData[0].Id;
                setAddressFielsManually(id, text);
                // EndLoading();
            } else {
                // EndLoading();
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getOrderId() {
        try {
            ////StartLoading();
            var billingDetailsData = $rootScope.billingDetails;
            if (billingDetailsData.AdOrderId != undefined && billingDetailsData.AdOrderId != null && billingDetailsData.AdOrderId != "" && billingDetailsData.AdOrderId > 0) {
                fillBillingDetails();
                // EndLoading();
            }
            else if ($scope.order.AdOrderId > 0) {
                // fillBillingDetailsUsingAPI($scope.order.AdOrderId);
                fillBillingDetails($scope.order.AdOrderId);
                // EndLoading();
            }
            else {
                ordersFactory.generateBuyId(function (result) {
                    if (result.statusCode == 1) {
                        var orderNumber = parseInt(result.data.buyId);
                        $scope.order.AdOrderId = orderNumber;
                        $scope.isEditMode = false;
                    }
                });
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function fillBillingDetails(buyId) {
        try {
            var timer = new Timer('Edit Order');
            if (buyId != undefined && buyId != "" && buyId != null) {
                timer.start();
                mediaOrdersFactory.getOrdersByBuyId(buyId, function (result) {
                    ////StartLoading();
                    if (result.data[0].CampaignName != undefined && result.data[0].CampaignName != null && result.data[0].CampaignName != "") {
                        $rootScope.billingDetails.CampaignName = result.data[0].CampaignName;
                    }
                    //else {
                    //    $rootScope.billingDetails.CampaignName = "";
                    //}
                    $rootScope.billingDetails.AdOrderId = result.data[0].BuyId;
                    $rootScope.billingDetails.AdvertiserId = result.data[0].AdvertiserId;
                    $rootScope.billingDetails.AgencyId = result.data[0].AgencyId;
                    $rootScope.billingDetails.BillTo = result.data[0].BillToInd;
                    $rootScope.billingDetails.AdCommissions = result.data[0].MediaOrderReps;
                    $rootScope.billingDetails.BillToContactId = result.data[0].ST_ID;
                    $rootScope.GrossCost = result.data[0].GrossCost;
                    $rootScope.NetCost = result.data[0].NetCost;
                    var GrossCost = $rootScope.GrossCost;
                    var NetCost = $rootScope.NetCost;
                    $scope.orderGrossCost = parseFloat(GrossCost).toFixed(2);
                    $scope.orderNetCost = parseFloat(NetCost).toFixed(2);

                    var billingDetailsData = $rootScope.billingDetails;
                    if (billingDetailsData.AdOrderId > 0) {
                        $scope.isEditMode = true;
                        $scope.mediaOrderId = $rootScope.billingDetails.MediaOrderId;
                        if (billingDetailsData.CampaignName != null && billingDetailsData.CampaignName != undefined && billingDetailsData.CampaignName != "") {
                            $scope.order.CampaignName = billingDetailsData.CampaignName;
                        }
                        else {
                            $scope.order.CampaignName = "";
                        }
                        $scope.order.AdOrderId = billingDetailsData.AdOrderId;
                        $scope.order.Adverdtiser = billingDetailsData.AdvertiserId;
                        if (billingDetailsData.AgencyId != "") {
                            $scope.order.Agency = billingDetailsData.AgencyId;
                            $scope.changeAgency();
                        }
                        else {
                            $scope.order.Agency = "";
                        }
                        if ($scope.order.Adverdtiser != undefined && $scope.order.Adverdtiser != "" && $scope.order.Adverdtiser != null) {
                            $scope.changeAdvertiser();
                        }
                        $scope.order.BillTo = parseInt(billingDetailsData.BillTo == false ? 0 : 1);
                        $scope.order.BillingContacts = $rootScope.billingDetails.BillToContactId;
                        setDroupDownValue('ddlAdverdtiser', $scope.order.Adverdtiser);
                        if ($scope.order.Agency > 0) {
                            setDroupDownValue('ddlAgency', $scope.order.Agency);
                        }
                        setDroupDownValue('ddlContact', parseInt($scope.order.BillingContacts));
                        changeDropDownColor('s2id_ddlContact', parseInt($scope.order.BillingContacts));
                        // EndLoading();
                        if (billingDetailsData.AdCommissions.length > 0) {
                            createRepsandTerritoryWiseCommissionHTMLWithData(billingDetailsData.AdCommissions);
                        }
                    }
                    timer.stop();
                });
                
            }
            else {
                timer.start();
                var billingDetailsData = $rootScope.billingDetails;
                if (billingDetailsData.AdOrderId > 0) {
                 
                    $scope.isEditMode = true;
                    $scope.mediaOrderId = $rootScope.billingDetails.MediaOrderId;
                    if (billingDetailsData.CampaignName != null && billingDetailsData.CampaignName != undefined && billingDetailsData.CampaignName != "") {
                        $scope.order.CampaignName = billingDetailsData.CampaignName;
                    }
                    else {
                        $scope.order.CampaignName = "";
                    }
                    $scope.order.AdOrderId = billingDetailsData.AdOrderId;
                    $scope.order.Adverdtiser = billingDetailsData.AdvertiserId;
                    if (billingDetailsData.AgencyId != "") {
                        $scope.order.Agency = billingDetailsData.AgencyId;
                        $scope.changeAgency();
                    }
                    else {
                        $scope.order.Agency = "";
                    }
                    if ($scope.order.Adverdtiser != undefined && $scope.order.Adverdtiser != "" && $scope.order.Adverdtiser != null) {
                        $scope.changeAdvertiser();
                    }
                    $scope.order.BillTo = parseInt(billingDetailsData.BillTo == false ? 0 : 1);
                    $scope.order.BillingContacts = $rootScope.billingDetails.BillToContactId;
                    setDroupDownValue('ddlAdverdtiser', $scope.order.Adverdtiser);
                    if ($scope.order.Agency > 0) {
                        setDroupDownValue('ddlAgency', $scope.order.Agency);
                    }
                    setDroupDownValue('ddlContact', parseInt($scope.order.BillingContacts));
                    changeDropDownColor('s2id_ddlContact', parseInt($scope.order.BillingContacts));
                    // EndLoading();
                    if (billingDetailsData.AdCommissions.length > 0) {
                        createRepsandTerritoryWiseCommissionHTMLWithData(billingDetailsData.AdCommissions);
                    }
                }
                timer.stop();
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function createRepsandTerritoryWiseCommissionHTMLWithData(repsAndTerritoryWiseComm) {
        try {
            ////StartLoading();
            var timer = new Timer('createRepsandTerritoryWiseCommissionHTMLWithData');
            timer.start();
            if (repsAndTerritoryWiseComm != null) {
                var adCommissions = repsAndTerritoryWiseComm;
                if (adCommissions.length > 0) {
                    //$scope.parentArray = adCommissions;
                    angular.forEach(adCommissions, function (adCommission, key) {
                        var repId = adCommission.RepId;
                        var TerritoryId = adCommission.TerritoryId;
                        //var OrderContribution = adCommission.OrderContribution;
                        var Split = adCommission.Split;
                        var dynamicModelName = 'input' + TerritoryId + repId;
                        // globalSalesValue[dynamicModelName] = OrderContribution;
                        globalSalesValue[dynamicModelName] = Split;
                    });

                    // start create dynamic HTML
                    $scope.parentArray = [];
                    var allRepsAndTerritory = adCommissions;
                    if (allRepsAndTerritory.length > 0) {
                        angular.forEach(allRepsAndTerritory, function (adCommission, key) {
                            $scope.repTerritoryArray.repsName = adCommission.RepsName;
                            $scope.repTerritoryArray.territoryName = adCommission.TerritoryName;
                            // $scope.repTerritoryArray.commission = adCommission.commission;
                            $scope.repTerritoryArray.Commission = adCommission.Commission;
                            $scope.repTerritoryArray.repsId = adCommission.RepId;
                            $scope.repTerritoryArray.territoryId = adCommission.TerritoryId;
                            //$scope.repTerritoryArray.OrderContribution = adCommission.OrderContribution;
                            $scope.repTerritoryArray.Split = adCommission.Split;

                            $scope.dvId = 'dv' + adCommission.TerritoryId + adCommission.RepId;
                            var mainDiv = angular.element(document.querySelector('#dvGroup'));
                            var id = $scope.dvId;
                            var model = $parse(id);
                            model.assign($scope, id);

                            var dynamicModelName = 'input' + adCommission.TerritoryId + adCommission.RepId;
                            var text = globalSalesValue[dynamicModelName];
                            var the_string = "dynamicRowData.input" + adCommission.TerritoryId + "" + adCommission.RepId + "";
                            var model = $parse(the_string);
                            model.assign($scope, text);

                            var htmlTemp = '<div class="col-md-4" id="' + id + '"><div class="reptDataBlockItem" ><div class="rep-ter">' + adCommission.RepsName + '|' + adCommission.TerritoryName + '</div>' +
                                '<div class="commission">(' + adCommission.Commission + '% comm.)</div><div class="form-group"><div class="input-group">' +
                                '<input type="text" ng-model="' + the_string + '" allow-decimal-numbers-dot class="form-control"  ><div class="input-group-addon">%</div></div>' +
                                '</div><a href="javascript:void(0);" ng-model="' + id + '" ng-click="RemoverepsTerritories(' + id + ',' + adCommission.TerritoryId + ',' + adCommission.RepId + ')"  class="deleteBlock">-</a></div></div>';
                            var finalDiv = $compile(htmlTemp)($scope);
                            mainDiv.append(finalDiv);
                            $scope.showRepsMsgs = false;
                            $scope.parentArray.push($scope.repTerritoryArray);
                            $scope.repTerritoryArray = [];
                        });
                    }
                    // EndLoading();
                    // end create dynamic HTML
                } else {
                    // EndLoading();
                }
            } else {
                // EndLoading();
            }
            timer.stop();
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function setDroupDownValue(id, value) {
        try {
            var adDropDown = angular.element(document.querySelector('#' + id));
            adDropDown.select2('val', value);
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getAdvertiserThroughASI() {
        try {
            ////StartLoading();
            if ($rootScope.mainPartyAndOrganizationData.length <= 0 && $rootScope.mainAdvertisers.length <= 0 && $rootScope.mainAgencies.length <= 0 && $rootScope.mainBillingToContacts.length <= 0) {
                var HasNext = true;
                var offset = 0;
                var advertisers = [];
                var contacts = [];
                function getAdvertiserData() {
                    advertiserFactory.getAllAdvertiser1(baseUrl, authToken, offset, function (result) {
                        ////StartLoading();
                        if (result != null && result != undefined && result != "") {

                            var ItemData = result.Items.$values
                            console.log(ItemData);
                            angular.forEach(ItemData, function (itemdatavalue, key) {
                                $scope.partyAndOrganizationData.push(itemdatavalue);
                                var type = itemdatavalue.$type;
                                if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                                    advertisers.push(itemdatavalue);
                                }
                                else if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                                    contacts.push(itemdatavalue);
                                }
                            });

                            $scope.advertisers = advertisers;
                            $scope.agencies = advertisers;
                            //$scope.billingToContacts = contacts;
                            $scope.personData = contacts;
                            var TotalCount = result.TotalCount;
                            HasNext = result.HasNext;
                            offset = result.Offset;
                            var Limit = result.Limit;
                            var NextOffset = result.NextOffset;
                            if (HasNext == true) {
                                offset = NextOffset;
                                getAdvertiserData();
                            } else {
                                fillPartyComingDashBoard();
                                changeCalledManually();
                                changeCalledManually_Agency();
                                $scope.changeAdvertiser();
                                $scope.changeAgency();
                                // EndLoading();
                            }
                        }
                        else {
                            // EndLoading();
                            $scope.advertisers = [];
                        }
                    })
                }
                getAdvertiserData();
            }
            else {
                $scope.partyAndOrganizationData = $rootScope.mainPartyAndOrganizationData;
                $scope.advertisers = $rootScope.mainAdvertisers;
                $scope.agencies = $rootScope.mainAgencies;
                $scope.personData = $rootScope.mainBillingToContacts;
                // $scope.billingToContacts = $rootScope.mainBillingToContacts;
                // $scope.$on('$viewContentLoaded', function () {
                advertiserFactory.getAllAdvertiser1(baseUrl, authToken, offset, function (result) {
                    ////StartLoading();
                    if (result != null && result != undefined && result != "") {
                        //console.log(result.Items.$values);
                        $scope.isEditMode = true;
                        fillPartyComingDashBoard();
                        changeCalledManually();
                        changeCalledManually_Agency();
                        $scope.changeAdvertiser();
                        $scope.changeAgency();
                        // EndLoading();
                        $scope.isEditMode = false;
                    } else {
                        // EndLoading();
                    }
                });
                // });
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getCountryThroughASI() {
        try {
            ////StartLoading();
            var HasNext = true;
            var offset = 0;
            var country = [];

            function getCountryData() {
                advertiserFactory.getCountry(baseUrl, authToken, offset, function (result) {
                    ////StartLoading();
                    if (result != null && result != undefined && result != "") {
                        var ItemData = result.Items.$values
                        console.log(ItemData);

                        $scope.countries = ItemData;
                        var TotalCount = result.TotalCount;
                        HasNext = result.HasNext;
                        offset = result.Offset;
                        var Limit = result.Limit;
                        var NextOffset = result.NextOffset;
                        if (HasNext == true) {
                            offset = NextOffset;
                            getCountryData();
                        } else {
                            // EndLoading();
                        }
                    }
                    else {
                        // EndLoading();
                        $scope.countries = [];
                    }
                })
            }
            getCountryData();
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function GetAllReps() {
        try {
            ////StartLoading();
            repsFactory.getAll(function (result) {
                ////StartLoading();
                if (result.statusCode == 1) {
                    console.log(result);
                    $scope.reps = result.data;
                    // EndLoading();
                }
                else {
                    // EndLoading();
                    toastr.error(result.message, 'Error');
                }
            })
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    //Init Orders code 
    $scope.SaveOrders = function (order, dynamicRowData) {
        try {
            var adverdtiserDropDown = angular.element(document.querySelector('#ddlAdverdtiser'));
            var adverdtiserId = "";
            var adverdtiserValue = "";
            var SelectedAdverdtiser = adverdtiserDropDown.select2("data");
            if (SelectedAdverdtiser != undefined && SelectedAdverdtiser != null && SelectedAdverdtiser != "") {
                adverdtiserId = SelectedAdverdtiser.id;
                adverdtiserValue = SelectedAdverdtiser.text;
            }
            var agencyDropDown = angular.element(document.querySelector('#ddlAgency'));
            var SelectedAgency = agencyDropDown.select2("data");
            var agencyId = "";
            var agencyValue = "";
            if (SelectedAgency != undefined && SelectedAgency != null && SelectedAgency != "") {
                agencyId = SelectedAgency.id;
                agencyValue = SelectedAgency.text;
            }
            var contactDropDown = angular.element(document.querySelector('#ddlContact'));
            var Selectedcontact = contactDropDown.select2("data");
            var contactId = Selectedcontact.id;
            var contactValue = Selectedcontact.text;

            var objOrder = {};
            objOrder.AdOrderId = $scope.order.AdOrderId;
            objOrder.AdvertiserId = adverdtiserId;
            objOrder.AdvertiserName = adverdtiserValue;
            if (agencyId != "" && agencyId != undefined && agencyId != null) {
                objOrder.AgencyId = agencyId;
            }
            else {
                objOrder.AgencyId = "";
            }

            objOrder.AgencyName = agencyValue;
            objOrder.BillTo = parseInt(order.BillTo);
            objOrder.BillToContactId = contactId;
            objOrder.BillToContactName = contactValue;
            objOrder.CampaignName = $scope.order.CampaignName;
            //objOrder.NetCost = 0;
            //objOrder.GrossCost = 0;
            objOrder.AdCommissions = [];

            var salesCommission = $scope.parentArray;
            if (salesCommission.length > 0) {
                angular.forEach(salesCommission, function (salecommission, key) {
                    var objSales = {};
                    objSales.RepId = salecommission.repsId;
                    objSales.RepsName = salecommission.repsName;
                    objSales.TerritoryId = salecommission.territoryId;
                    objSales.TerritoryName = salecommission.territoryName;
                    //objSales.commission = salecommission.commission;
                    objSales.Commission = salecommission.Commission;

                    var dynamicModelName = 'input' + salecommission.territoryId + salecommission.repsId;
                    var indexof = Object.keys(dynamicRowData).indexOf(dynamicModelName);

                    if (indexof != -1) {
                        var value = dynamicRowData[dynamicModelName];
                        if (value != "" && value != undefined && value != null) {
                            //  objSales.OrderContribution = dynamicRowData[dynamicModelName];
                            objSales.Split = dynamicRowData[dynamicModelName];
                        }
                        else {
                            //objSales.OrderContribution = 0;
                            objSales.Split = 0;
                        }
                    }
                    objOrder.AdCommissions.push(objSales);
                    $scope.ordersFormReps.$valid = true;
                    $scope.ordersForm.$valid = true;
                });
            }

            if ($scope.ordersFormReps.$valid) {
                checkValidation();
                if ($scope.ordersForm.$valid) {
                    console.log(objOrder);
                    if (objOrder.AdCommissions.length > 0) {
                        var Commissions = objOrder.AdCommissions;
                        var totalComm = 0;
                        angular.forEach(Commissions, function (Commission, key) {
                            // var comm = parseFloat(Commission.OrderContribution);
                            var comm = parseFloat(Commission.Split);
                            totalComm += comm;
                        });

                        if (totalComm > 100) {
                            toastr.info('Percentage should not be greater than 100', 'Information!');
                        } else if (totalComm < 100) {
                            toastr.info('Percentage should not be less than 100', 'Information!');
                        }
                        else {
                            if (objOrder.AdOrderId >= 0) {


                                $rootScope.billingDetails = objOrder;
                                $scope.objOrder = {};
                                $scope.objOrder.AdOrderId = 0;
                                //$scope.order = {};
                                //$scope.order.AdOrderId = 0;
                                //$scope.parentArray = [];
                                //$scope.selectedReps = "";
                                //$scope.selectedTerritories = "";
                                //$scope.partyAddress = "";
                                //$scope.partyMobile = "";
                                //$scope.partyMemberType = "";
                                //$scope.partyWork = "";
                                //$scope.partyEmail = "";
                                //$scope.partyHome = "";
                                //initSelect2('s2id_ddlAdverdtiser', 'Select Adverdtiser');
                                //initSelect2('s2id_ddlAgency', 'Select Agency');
                                //initSelect2('s2id_ddlContact', 'Select Contact');
                                //$scope.order.BillTo = 0;

                                removeDynamicAddedCommissionHTML();
                                $scope.ordersForm.$setPristine();
                                $scope.showMsgs = false;
                                $scope.ordersFormReps.$setPristine();
                                $scope.showRepsMsgs = false;

                                $scope.ordersFormReps.$valid = false;
                                $scope.ordersForm.$valid = false;
                                //toastr.success('Save successfully.', 'Success!');
                                var Orderid = objOrder.AdOrderId;
                                var mediaOrder = "";
                                if ($scope.mediaOrderId != undefined && $scope.mediaOrderId != "" && $scope.mediaOrderId != null) {
                                    mediaOrder = $scope.mediaOrderId;
                                }
                                $location.path('ordersDetails/mediaSchedule/' + Orderid + '/' + mediaOrder);
                            }
                        }

                    } else {
                        toastr.info('Please select alteast one reps and territory', 'Information!');
                    }

                }
                else {
                    $scope.showMsgs = true;
                }
            }
            else {
                $scope.showRepsMsgs = true;
                if ($scope.ordersForm.$valid == false) {
                    $scope.showMsgs = true;
                }
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function checkValidation() {
        try {
            var CountryDropDown = angular.element(document.querySelector('#ddlAdverdtiser'));
            var getSelectedCountry = CountryDropDown.select2("data");
            var id = getSelectedCountry.id;
            var value = getSelectedCountry.text;
            if (id == undefined || id == "" || id == null) {
                $scope.ordersForm.$valid = false;
            }

            //var CountryDropDown1 = angular.element(document.querySelector('#ddlAgency'));
            //var getSelectedCountry1 = CountryDropDown1.select2("data");
            //var id1 = getSelectedCountry1.id;
            //var value1 = getSelectedCountry1.text;
            //if (id1 == undefined || id1 == "" || id1 == null) {
            //    $scope.ordersForm.$valid = false;
            //}

            var CountryDropDown2 = angular.element(document.querySelector('#ddlContact'));
            var getSelectedCountry2 = CountryDropDown2.select2("data");
            var id2 = getSelectedCountry2.id;
            var value = getSelectedCountry2.text;
            if (id2 == undefined || id2 == "" || id2 == null) {
                $scope.ordersForm.$valid = false;
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.createOrdersTable = function (orders, selectedAdvertiserValue, selectedAgencyValue) {
        try {
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    //Init Advertiser Modal Pop up and code
    $scope.InitAdvertiserPopup = function () {
        try {
            $scope.advertiser = {};
            $scope.advertiser.AdvertiserId = 0;
            intiAdvertiserSelect2();
            $scope.advertiser.CountrySubEntity = "";
            $scope.isStatehave = false;
            $scope.SubEntityNameCaption = "State";
            var stateSelect2Button = angular.element(document.querySelector('#s2id_ddlState'))
            var selectedState = stateSelect2Button.find('span.select2-chosen');
            selectedState.text('Select State');
            setCountryDefault_Advertiser();
            $scope.AdvertiserModalForm.$setPristine();
            $scope.advertiserModalMsgs = false;
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function setCountryDefault_Advertiser() {
        try {
            var id = "US";
            var value = "United States";
            if (id != "" && id != undefined && id != null) {
                if (value != "" && value != undefined && value != null) {
                    var countries = $scope.countries;
                    angular.forEach(countries, function (countryvalue, key) {
                        if (countryvalue.CountryName == value && countryvalue.CountryCode == id) {
                            $scope.SubEntityNameCaption_Agency = countryvalue.SubEntityNameCaption;
                            var state = countryvalue.CountrySubEntities;
                            if (state != "" && state != undefined && state != null && state.$values.length > 0) {
                                $scope.advertiser.CountryCode = "US";
                                var adDropDown = angular.element(document.querySelector('#ddlCountry'));
                                adDropDown.select2('val', $scope.advertiser.CountryCode);
                                changeDropDownColor('s2id_ddlCountry', $scope.advertiser.CountryCode);
                                $scope.isStatehave = true;
                                $scope.advertiser.State = "";
                                $scope.states = state.$values;
                                $scope.advertiser.CountrySubEntity = "";
                            } else {
                                $scope.advertiser.CountryCode = "US";
                                var adDropDown = angular.element(document.querySelector('#ddlCountry'));
                                adDropDown.select2('val', $scope.advertiser.CountryCode);
                                changeDropDownColor('s2id_ddlCountry', $scope.advertiser.CountryCode);
                                $scope.isStatehave = false;
                                $scope.advertiser.State = "";
                                $scope.states = [];
                                $scope.advertiser.CountrySubEntity = "";
                            }
                        }
                    });
                }
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function setCountryDefault_Agency() {
        try {
            var id = "US";
            var value = "United States";
            if (id != "" && id != undefined && id != null) {
                if (value != "" && value != undefined && value != null) {
                    var countries = $scope.countries;

                    angular.forEach(countries, function (countryvalue, key) {
                        if (countryvalue.CountryName == value && countryvalue.CountryCode == id) {
                            $scope.SubEntityNameCaption_Agency = countryvalue.SubEntityNameCaption;
                            var state = countryvalue.CountrySubEntities;
                            if (state != "" && state != undefined && state != null && state.$values.length > 0) {
                                $scope.isStatehave_Agency = true;
                                $scope.agency.State = "";
                                $scope.agency.CountryCode = "US";
                                var adDropDown = angular.element(document.querySelector('#ddlCountry_Agency'));
                                adDropDown.select2('val', $scope.agency.CountryCode);
                                changeDropDownColor('s2id_ddlCountry_Agency', $scope.agency.CountryCode);
                                //setUISelect2Value('s2id_ddlCountry_Agency', 'United States');
                                $scope.states = state.$values;
                                $scope.agency.CountrySubEntity_Agency = "";
                            } else {
                                $scope.isStatehave_Agency = false;
                                $scope.agency.State = "";
                                $scope.agency.CountryCode = "US";
                                var adDropDown = angular.element(document.querySelector('#ddlCountry_Agency'));
                                adDropDown.select2('val', $scope.agency.CountryCode);
                                changeDropDownColor('s2id_ddlCountry_Agency', $scope.agency.CountryCode);
                                //setUISelect2Value('s2id_ddlCountry_Agency', 'United States');
                                $scope.states = [];
                                $scope.agency.CountrySubEntity_Agency = "";
                            }
                        }
                    });
                }
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function setCountryDefault_Contact() {
        try {
            var id = "US";
            var value = "United States";
            if (id != "" && id != undefined && id != null) {
                if (value != "" && value != undefined && value != null) {
                    var countries = $scope.countries;

                    angular.forEach(countries, function (countryvalue, key) {
                        if (countryvalue.CountryName == value && countryvalue.CountryCode == id) {
                            $scope.SubEntityNameCaption_Agency = countryvalue.SubEntityNameCaption;
                            var state = countryvalue.CountrySubEntities;
                            if (state != "" && state != undefined && state != null && state.$values.length > 0) {
                                $scope.isStatehave_Contact = true;
                                $scope.contact.CountryCode = "US";
                                var adDropDown = angular.element(document.querySelector('#ddlCountry_Contact'));
                                adDropDown.select2('val', $scope.contact.CountryCode);
                                changeDropDownColor('s2id_ddlCountry_Contact', $scope.contact.CountryCode);
                                // setUISelect2Value('s2id_ddlCountry_Contact', 'United States');
                                $scope.states = state.$values;
                                $scope.contact.CountrySubEntity_Contact = "";
                            } else {
                                $scope.agency.CountryCode = "US";
                                var adDropDown = angular.element(document.querySelector('#ddlCountry_Contact'));
                                adDropDown.select2('val', $scope.contact.CountryCode);
                                changeDropDownColor('s2id_ddlCountry_Contact', $scope.contact.CountryCode);
                                $scope.isStatehave_Contact = false;
                                $scope.contact.CountrySubEntity_Contact = "";
                            }
                        }
                    });
                }
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function setUISelect2Value(id, text) {
        try {
            var stateSelect2Button = angular.element(document.querySelector('#' + id))
            var selectedState = stateSelect2Button.find('span.select2-chosen');
            selectedState.text(text);
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function intiAdvertiserSelect2() {
        try {
            var countrySelect2Button = angular.element(document.querySelector('#select2-chosen-6'));
            countrySelect2Button.text("Select country");
            var mediaTypeSelect2Button = angular.element(document.querySelector('#select2-chosen-7'));
            mediaTypeSelect2Button.text("Select Customer Type");
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function CreateAdvertiserButtonName() {
        try {
            var advertiserButton = angular.element(document.querySelector('#btnadvertiser'));
            advertiserButton.text('Add Advertiser');
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function generateObjectForAdvertiserSave(advertiser) {
        try {
            var stateId = "";
            var stateValue = "";

            var CountryDropDown = angular.element(document.querySelector('#ddlCountry'));
            var getSelectedCountry = CountryDropDown.select2("data");
            var countryId = getSelectedCountry.id;
            var countryValue = getSelectedCountry.text;
            if (countryId == "") {
                countryValue = "";
            }


            if ($scope.advertiser.CountrySubEntity == "statelistY") {
                var StateDropDown = angular.element(document.querySelector('#ddlState'));
                var getSelectedState = StateDropDown.select2("data");
                stateId = getSelectedState.id;
                stateValue = getSelectedState.text;

                if (stateId == undefined || stateId == "" || stateId == null) {
                    if (stateValue == "Select State") {
                        stateValue = "";
                    }
                    else {
                        stateValue = getSelectedState.text;
                    }
                }
            } else if ($scope.advertiser.CountrySubEntity == "") {
                var StateDropDown = angular.element(document.querySelector('#ddlState'));
                var getSelectedState = StateDropDown.select2("data");
                stateId = getSelectedState.id;
                if (stateId == undefined || stateId == "" || stateId == null) {
                    if (stateValue == "Select State") {
                        stateValue = "";
                    }
                    else {
                        stateValue = "";
                    }
                } else {
                    stateValue = getSelectedState.text;
                }
            } else if ($scope.advertiser.CountrySubEntity == "statelistN") {
                var StateDropDown = angular.element(document.querySelector('#ddlState'));
                var getSelectedState = StateDropDown.select2("data");
                stateId = "";
                stateValue = $scope.agency.State;
            }
            if (advertiser.Organization == undefined) {
                advertiser.Organization = "";
            }
            if (advertiser.Address == undefined) {
                advertiser.Address = "";
            }
            if (advertiser.City == undefined) {
                advertiser.City = "";
            }
            if (advertiser.PostalCode == undefined) {
                advertiser.PostalCode = "";
            }
            if (advertiser.Mobile == undefined) {
                advertiser.Mobile = "";
            }
            if (advertiser.Email == undefined) {
                advertiser.Email = "";
            }
            if (advertiser.Work == undefined) {
                advertiser.Work = "";
            }
            if (advertiser.Home == undefined) {
                advertiser.Home = "";
            }
            var advertiserObject = {
                "$type": "Asi.Soa.Membership.DataContracts.OrganizationData, Asi.Contracts",
                "OrganizationName": "" + advertiser.Organization + "",
                "Addresses": {
                    "$type": "Asi.Soa.Membership.DataContracts.FullAddressDataCollection, Asi.Contracts",
                    "$values": [
                        {
                            "$type": "Asi.Soa.Membership.DataContracts.FullAddressData, Asi.Contracts",
                            "Address": {
                                "$type": "Asi.Soa.Membership.DataContracts.AddressData, Asi.Contracts",
                                "AddressLines": {
                                    "$type": "Asi.Soa.Membership.DataContracts.AddressLineDataCollection, Asi.Contracts",
                                    "$values": [
                                        "" + advertiser.Address + ""
                                    ]
                                },
                                "CityName": "" + advertiser.City + "",
                                "CountryCode": "" + countryId + "",
                                "CountryName": "" + countryValue + "",
                                "CountrySubEntityCode": "" + stateId + "",
                                "CountrySubEntityName": "" + stateValue + "",
                                "FullAddress": "" + advertiser.Address + "\r" + advertiser.City + ", " + stateId + "  " + advertiser.PostalCode + "\r" + countryValue + "",
                                "PostalCode": "" + advertiser.PostalCode + "",
                            },
                            "AddresseeText": "" + advertiser.Organization + "\r\n" + advertiser.Address + "\r" + advertiser.City + ", " + stateId + "  " + advertiser.PostalCode + "\r" + countryValue + "",
                            "AddressPurpose": "Address",
                            "CommunicationPreferences": {
                                "$type": "Asi.Soa.Membership.DataContracts.CommunicationPreferenceDataCollection, Asi.Contracts",
                                "$values": [
                                    {
                                        "$type": "Asi.Soa.Membership.DataContracts.CommunicationPreferenceData, Asi.Contracts",
                                        "Reason": "bill"
                                    },
                                    {
                                        "$type": "Asi.Soa.Membership.DataContracts.CommunicationPreferenceData, Asi.Contracts",
                                        "Reason": "ship"
                                    }
                                ]
                            },
                            "Phone": "" + advertiser.Mobile + ""
                        }
                    ]
                },
                "Emails": {
                    "$type": "Asi.Soa.Membership.DataContracts.EmailDataCollection, Asi.Contracts",
                    "$values": [
                        {
                            "$type": "Asi.Soa.Membership.DataContracts.EmailData, Asi.Contracts",
                            "Address": "" + advertiser.Email + "",
                            "EmailType": "_Primary",
                            "IsPrimary": true
                        }
                    ]
                },
                "Phones": {
                    "$type": "Asi.Soa.Membership.DataContracts.PhoneDataCollection, Asi.Contracts",
                    "$values": [
                        {
                            "$type": "Asi.Soa.Membership.DataContracts.PhoneData, Asi.Contracts",
                            "Number": "" + advertiser.Work + "",
                            "PhoneType": "_Work Phone"
                        },
                        {
                            "$type": "Asi.Soa.Membership.DataContracts.PhoneData, Asi.Contracts",
                            "Number": "" + advertiser.Mobile + "",
                            "PhoneType": "Mobile"
                        },
                        {
                            "$type": "Asi.Soa.Membership.DataContracts.PhoneData, Asi.Contracts",
                            "Number": "" + advertiser.Home + "",
                            "PhoneType": "Address"
                        }
                    ]
                }
            }
            return advertiserObject;
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.SaveAdvertiser = function (advertiser) {
        try {
            if ($scope.AdvertiserModalForm.$valid) {
                console.log(advertiser);
                var advertiserObject = generateObjectForAdvertiserSave(advertiser);
                if (advertiserObject != null && advertiserObject != undefined && advertiserObject != "") {
                    if (advertiser.AdvertiserId == 0) {
                        advertiserFactory.saveOrganization(baseUrl, authToken, advertiserObject, function (result) {
                            if (result != null && result != undefined && result != "") {
                                $scope.advertiser = {};

                                $scope.advertiser.AdvertiserId = 0;
                                if (parseInt(result.Id) > 0) {
                                    var aData = $scope.advertisers;
                                    $scope.advertisers = {};
                                    $scope.advertisers = aData;
                                    $scope.advertisers.push(result);
                                    $rootScope.mainAdvertisers.push(result);

                                    var agData = $scope.agencies;
                                    $scope.agencies = {};
                                    $scope.agencies = $scope.advertisers;
                                    $rootScope.mainAgencies.push(result);

                                    $scope.partyAndOrganizationData.push(result);
                                    $rootScope.mainPartyAndOrganizationData.push(result);

                                    setSelect2DataNew('ddlAdverdtiser', $scope.advertisers);
                                    initSelect2('s2id_ddlAdverdtiser', 'Select Adverdtiser');
                                    setSelect2DataNew('ddlAgency', $scope.agencies);
                                    initSelect2('s2id_ddlAgency', 'Select Agency');
                                }

                                CreateAdvertiserButtonName();
                                intiAdvertiserSelect2();
                                $scope.isStatehave = false;
                                $scope.SubEntityNameCaption = "State";
                                var advertiserModal = angular.element(document.querySelector('#advertiserModal'))
                                advertiserModal.modal('hide');;
                                $scope.AdvertiserModalForm.$setPristine();
                                $scope.advertiserModalMsgs = false;
                                toastr.success('Save successfully.', 'Success!');
                            }

                            else {
                                toastr.error(result.message, 'Error');
                            }
                        })
                    }
                }
            }
            else {
                $scope.advertiserModalMsgs = true;
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function setSelect2DataNew(id, data) {
        try {
            var adDropDown = angular.element(document.querySelector('#' + id));
            adDropDown.select2('data', data);
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.CancelAdvertiser = function (advertiser) {
        try {
            $scope.advertiser = {};
            $scope.advertiser.AdvertiserId = 0;
            intiAdvertiserSelect2();
            $scope.isStatehave = false;
            $scope.SubEntityNameCaption = "State";
            $scope.AdvertiserModalForm.$setPristine();
            $scope.advertiserModalMsgs = false;
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    //Init Agency Modal Pop up and code 
    $scope.InitAgencyPopup = function () {
        try {
            $scope.agency = {};
            $scope.agency.AgencyId = 0;
            intiAgencySelect2();
            $scope.agency.CountrySubEntity_Agency = "";
            $scope.isStatehave_Agency = false;
            $scope.SubEntityNameCaption_Agency = "State";
            var stateSelect2Button = angular.element(document.querySelector('#s2id_ddlState_Agency'))
            var selectedState = stateSelect2Button.find('span.select2-chosen');
            selectedState.text('Select State');
            setCountryDefault_Agency();
            $scope.AgencyModalForm.$setPristine();
            $scope.agencyModalMsgs = false;
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function intiAgencySelect2() {
        try {
            var countrySelect2Button = angular.element(document.querySelector('#select2-chosen-8'));
            countrySelect2Button.text("Select country");
            var mediaTypeSelect2Button = angular.element(document.querySelector('#select2-chosen-9'));
            mediaTypeSelect2Button.text("Select Customer Type");
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function CreateAgencyButtonName() {
        try {
            var agencyButton = angular.element(document.querySelector('#btnagency'));
            agencyButton.text('Add Agency');
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function generateObjectForAgencySave(agency) {
        try {
            var stateId = "";
            var stateValue = "";

            var CountryDropDown = angular.element(document.querySelector('#ddlCountry_Agency'));
            var getSelectedCountry = CountryDropDown.select2("data");
            var countryId = getSelectedCountry.id;
            var countryValue = getSelectedCountry.text;
            if (countryId == "") {
                countryValue = "";
            }
            if ($scope.agency.CountrySubEntity_Agency == "statelistY") {
                var StateDropDown = angular.element(document.querySelector('#ddlState_Agency'));
                var getSelectedState = StateDropDown.select2("data");
                stateId = getSelectedState.id;
                stateValue = getSelectedState.text;

                if (stateId == undefined || stateId == "" || stateId == null) {
                    if (stateValue == "Select State") {
                        stateValue = "";
                    }
                    else {
                        stateValue = getSelectedState.text;
                    }
                }
            } else if ($scope.agency.CountrySubEntity_Agency == "") {
                var StateDropDown = angular.element(document.querySelector('#ddlState_Agency'));
                var getSelectedState = StateDropDown.select2("data");
                stateId = getSelectedState.id;
                if (stateId == undefined || stateId == "" || stateId == null) {
                    if (stateValue == "Select State") {
                        stateValue = "";
                    }
                    else {
                        stateValue = "";
                    }
                } else {
                    stateValue = getSelectedState.text;
                }
            } else if ($scope.agency.CountrySubEntity_Agency == "statelistN") {
                var StateDropDown = angular.element(document.querySelector('#ddlState_Agency'));
                var getSelectedState = StateDropDown.select2("data");
                stateId = "";
                stateValue = $scope.agency.State;
            }
            if (agency.Organization == undefined) {
                agency.Organization = "";
            }
            if (agency.Address == undefined) {
                agency.Address = "";
            }
            if (agency.City == undefined) {
                agency.City = "";
            }
            if (agency.PostalCode == undefined) {
                agency.PostalCode = "";
            }
            if (agency.Mobile == undefined) {
                agency.Mobile = "";
            }
            if (agency.Email == undefined) {
                agency.Email = "";
            }
            if (agency.Work == undefined) {
                agency.Work = "";
            }
            if (agency.Home == undefined) {
                agency.Home = "";
            }
            var agencyObject = {
                "$type": "Asi.Soa.Membership.DataContracts.OrganizationData, Asi.Contracts",
                "OrganizationName": "" + agency.Organization + "",
                "Addresses": {
                    "$type": "Asi.Soa.Membership.DataContracts.FullAddressDataCollection, Asi.Contracts",
                    "$values": [
                        {
                            "$type": "Asi.Soa.Membership.DataContracts.FullAddressData, Asi.Contracts",
                            "Address": {
                                "$type": "Asi.Soa.Membership.DataContracts.AddressData, Asi.Contracts",
                                "AddressLines": {
                                    "$type": "Asi.Soa.Membership.DataContracts.AddressLineDataCollection, Asi.Contracts",
                                    "$values": [
                                        "" + agency.Address + ""
                                    ]
                                },
                                "CityName": "" + agency.City + "",
                                "CountryCode": "" + countryId + "",
                                "CountryName": "" + countryValue + "",
                                "CountrySubEntityCode": "" + stateId + "",
                                "CountrySubEntityName": "" + stateValue + "",
                                "FullAddress": "" + agency.Address + "\r" + agency.City + ", " + stateId + "  " + agency.PostalCode + "\r" + countryValue + "",
                                "PostalCode": "" + agency.PostalCode + "",
                            },
                            "AddresseeText": "" + agency.Organization + "\r\n" + agency.Address + "\r" + agency.City + ", " + stateId + "  " + agency.PostalCode + "\r" + countryValue + "",
                            "AddressPurpose": "Address",
                            "CommunicationPreferences": {
                                "$type": "Asi.Soa.Membership.DataContracts.CommunicationPreferenceDataCollection, Asi.Contracts",
                                "$values": [
                                    {
                                        "$type": "Asi.Soa.Membership.DataContracts.CommunicationPreferenceData, Asi.Contracts",
                                        "Reason": "bill"
                                    },
                                    {
                                        "$type": "Asi.Soa.Membership.DataContracts.CommunicationPreferenceData, Asi.Contracts",
                                        "Reason": "ship"
                                    }
                                ]
                            },
                            "Phone": "" + agency.Mobile + ""
                        }
                    ]
                },
                "Emails": {
                    "$type": "Asi.Soa.Membership.DataContracts.EmailDataCollection, Asi.Contracts",
                    "$values": [
                        {
                            "$type": "Asi.Soa.Membership.DataContracts.EmailData, Asi.Contracts",
                            "Address": "" + agency.Email + "",
                            "EmailType": "_Primary",
                            "IsPrimary": true
                        }
                    ]
                },
                "Phones": {
                    "$type": "Asi.Soa.Membership.DataContracts.PhoneDataCollection, Asi.Contracts",
                    "$values": [
                        {
                            "$type": "Asi.Soa.Membership.DataContracts.PhoneData, Asi.Contracts",
                            "Number": "" + agency.Work + "",
                            "PhoneType": "_Work Phone"
                        },
                        {
                            "$type": "Asi.Soa.Membership.DataContracts.PhoneData, Asi.Contracts",
                            "Number": "" + agency.Mobile + "",
                            "PhoneType": "Mobile"
                        },
                        {
                            "$type": "Asi.Soa.Membership.DataContracts.PhoneData, Asi.Contracts",
                            "Number": "" + agency.Home + "",
                            "PhoneType": "Address"
                        }
                    ]
                }
            }
            return agencyObject;
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.SaveAgency = function (agency) {
        try {
            if ($scope.AgencyModalForm.$valid) {
                console.log(agency);
                var agencyObject = generateObjectForAgencySave(agency);
                if (agencyObject != null && agencyObject != undefined && agencyObject != "") {
                    if (agency.AgencyId == 0) {
                        advertiserFactory.saveOrganization(baseUrl, authToken, agencyObject, function (result) {
                            if (result != null && result != undefined && result != "") {
                                $scope.agency = {};
                                $scope.agency.AgencyId = 0;
                                //CreateAgencyButtonName();
                                if (parseInt(result.Id) > 0) {

                                    var aData = $scope.advertisers;
                                    $scope.advertisers = {};
                                    $scope.advertisers = aData;
                                    $scope.advertisers.push(result);
                                    $rootScope.mainAdvertisers.push(result);

                                    var agData = $scope.agencies;
                                    $scope.agencies = {};
                                    $scope.agencies = $scope.advertisers;
                                    $rootScope.mainAgencies.push(result);

                                    $scope.partyAndOrganizationData.push(result);
                                    $rootScope.mainPartyAndOrganizationData.push(result);

                                    setSelect2DataNew('ddlAdverdtiser', $scope.advertisers);
                                    initSelect2('s2id_ddlAdverdtiser', 'Select Adverdtiser');
                                    setSelect2DataNew('ddlAgency', $scope.agencies);
                                    initSelect2('s2id_ddlAgency', 'Select Agency');
                                }
                                $scope.AgencyModalForm.$setPristine();
                                $scope.agencyModalMsgs = false;
                                intiAgencySelect2();
                                $scope.isStatehave_Agency = false;
                                $scope.SubEntityNameCaption_Agency = "State";
                                var agencyModal = angular.element(document.querySelector('#agencyModal'))
                                agencyModal.modal('hide');;
                                toastr.success('Save successfully.', 'Success!');
                            }

                            else {
                                toastr.error(result.message, 'Error');
                            }
                        })
                    }
                }
            }
            else {
                $scope.agencyModalMsgs = true;
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.CancelAgency = function (agency) {
        try {
            $scope.agency = {};
            $scope.agency.AgencyId = 0;
            intiAgencySelect2();
            $scope.isStatehave_Agency = false;
            $scope.SubEntityNameCaption_Agency = "State";
            $scope.AgencyModalForm.$setPristine();
            $scope.agencyModalMsgs = false;
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }


    //Init Contact Modal Pop up and code by sneha
    $scope.InitContactPopup = function () {
        try {
            $scope.contact = {};
            $scope.contact.ContactId = 0;
            intiContactSelect2();
            $scope.selectedNamePrefixValue = "";
            var stateSelect2Button = angular.element(document.querySelector('#s2id_ddlState_Contact'))
            var selectedState = stateSelect2Button.find('span.select2-chosen');
            selectedState.text('Select State');
            $scope.contactSameAaAbove = 0;
            setCountryDefault_Contact();
            var flag = $scope.order.BillTo;
            if (parseInt(flag) == 0) {
                var AdvertiserDropDown = angular.element(document.querySelector('#ddlAdverdtiser'));
                var getSelectedAdvertiser = AdvertiserDropDown.select2("data");
                var id = getSelectedAdvertiser.id;
                ordersFactory.getOrganisationById(baseUrl, authToken, id, function (result) {
                    if (result != null && result != undefined && result != "") {
                        var ItemData = result
                        console.log(ItemData);
                        $scope.organisation = ItemData;
                        $scope.selectedPartyOrganisationName = ItemData.OrganizationName;
                        // $scope.selectedPartyOrganisationAddress = ItemData.Addresses.$values[0].AddresseeText; FullAddress
                        $scope.selectedPartyOrganisationAddress = ItemData.Addresses.$values[0].Address.FullAddress;
                        $scope.organisationDetailsID = $scope.organisation.Id;
                    }
                    else {
                        $scope.organisation = [];
                    }
                })
            }
            else if (parseInt(flag) == 1) {
                var AgencyDropDown = angular.element(document.querySelector('#ddlAgency'));
                var getSelectedAgency = AgencyDropDown.select2("data");
                var id = getSelectedAgency.id;
                ordersFactory.getOrganisationById(baseUrl, authToken, id, function (result) {
                    if (result != null && result != undefined && result != "") {
                        var ItemData = result
                        console.log(ItemData);
                        $scope.organisation = ItemData;
                        $scope.selectedPartyOrganisationName = ItemData.OrganizationName;
                        //  $scope.selectedPartyOrganisationAddress = ItemData.Addresses.$values[0].AddresseeText;
                        $scope.selectedPartyOrganisationAddress = ItemData.Addresses.$values[0].Address.FullAddress;
                        $scope.organisationDetailsID = $scope.organisation.Id;
                    }
                    else {
                        $scope.organisation = [];
                    }
                })
            }

            // $scope.contact.CountrySubEntity_Contact = "";
            console.log($scope.organisation);

            $scope.ContactModalForm.$setPristine();
            $scope.contactModalMsgs = false;
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function intiContactSelect2() {
        try {
            var countrySelect2Button = angular.element(document.querySelector('#select2-chosen-10'));
            countrySelect2Button.text("Select Prefix");
            var mediaTypeSelect2Button = angular.element(document.querySelector('#select2-chosen-12'));
            mediaTypeSelect2Button.text("Select Customer Type");
            var mediaTypeSelect2Button1 = angular.element(document.querySelector('#select2-chosen-11'));
            mediaTypeSelect2Button1.text("Select country");
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function CreateContactButtonName() {
        try {
            var contactButton = angular.element(document.querySelector('#btncontact'));
            contactButton.text('Add Contact');
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function objectForSaveContact(contact) {
        try {
            var stateId = "";
            var stateValue = "";
            var NamePrefixValue = "";
            var countryId = "";
            var countryValue = "";

            if ($scope.contactSameAaAbove == false) {
                var CountryDropDown = angular.element(document.querySelector('#ddlCountry_Contact'));
                var getSelectedCountry = CountryDropDown.select2("data");
                countryId = getSelectedCountry.id;
                countryValue = getSelectedCountry.text;
                if (countryId == "") {
                    countryValue = "";
                }
                var NamePrefixDropDown = angular.element(document.querySelector('#ddlNamePrefix'));
                var NamePrefixId = NamePrefixDropDown.select2("data").id;
                NamePrefixValue = NamePrefixDropDown.select2("data").text;

                if ($scope.contact.CountrySubEntity_Contact == "statelistY") {
                    var StateDropDown = angular.element(document.querySelector('#ddlState_Contact'));
                    var getSelectedState = StateDropDown.select2("data");
                    stateId = getSelectedState.id;
                    stateValue = getSelectedState.text;

                    if (stateId == undefined || stateId == "" || stateId == null) {
                        if (stateValue == "Select State") {
                            stateValue = "";
                        }
                        else {
                            stateValue = getSelectedState.text;
                        }
                    }
                } else if ($scope.contact.CountrySubEntity_Contact == "") {
                    var StateDropDown = angular.element(document.querySelector('#ddlState_Contact'));
                    var getSelectedState = StateDropDown.select2("data");
                    stateId = getSelectedState.id;
                    if (stateId == undefined || stateId == "" || stateId == null) {
                        if (stateValue == "Select State") {
                            stateValue = "";
                        }
                        else {
                            stateValue = "";
                        }
                    } else {
                        stateValue = getSelectedState.text;
                    }
                } else if ($scope.contact.CountrySubEntity_Contact == "statelistN") {
                    var StateDropDown = angular.element(document.querySelector('#ddlState_Contact'));
                    var getSelectedState = StateDropDown.select2("data");
                    stateId = "";
                    stateValue = $scope.contact.State;
                }

            } else {
                var NamePrefixDropDown = angular.element(document.querySelector('#ddlNamePrefix'));
                var NamePrefixId = NamePrefixDropDown.select2("data").id;
                NamePrefixValue = NamePrefixDropDown.select2("data").text;
                var organizationData = $scope.organisation
                stateId = contact.State;
                if (stateId == undefined || stateId == null || stateId == "") {
                    stateId = "";
                }
                if (stateId == "nocodefound") {
                    stateId = "";
                }
                countryId = $scope.selectedCountryValue;
                countryValue = $scope.organisation.Addresses.$values[0].Address.CountryName;
                stateValue = $scope.organisation.Addresses.$values[0].Address.CountrySubEntityName;

            }
            if (contact.MiddleName == undefined) {
                contact.MiddleName = "";
            }
            if (contact.Address == undefined) {
                contact.Address = "";
            }
            if (contact.City == undefined) {
                contact.City = "";
            }
            if (contact.PinCode == undefined) {
                contact.PinCode = "";
            }
            if (contact.Work == undefined) {
                contact.Work = "";
            }
            if (contact.Mobile == undefined) {
                contact.Mobile = "";
            }
            if (contact.Home == undefined) {
                contact.Home = "";
            }

            var object = {
                "$type": "Asi.Soa.Membership.DataContracts.PersonData, Asi.Contracts",
                "PersonName": {
                    "$type": "Asi.Soa.Membership.DataContracts.PersonNameData, Asi.Soa.Membership.Contracts",
                    "FirstName": contact.FirstName,
                    "MiddleName": contact.MiddleName,
                    //"InformalName": "Sneha",
                    "LastName": contact.LastName,
                    "NamePrefix": NamePrefixValue
                },
                "PrimaryOrganization": {
                    "$type": "Asi.Soa.Membership.DataContracts.PrimaryOrganizationInformationData, Asi.Contracts",
                    "OrganizationPartyId": "" + $scope.organisationDetailsID + ""
                },
                "Addresses": {
                    "$type": "Asi.Soa.Membership.DataContracts.FullAddressDataCollection, Asi.Contracts",
                    "$values": [
                        {
                            "$type": "Asi.Soa.Membership.DataContracts.FullAddressData, Asi.Contracts",
                            "Address": {
                                "$type": "Asi.Soa.Membership.DataContracts.AddressData, Asi.Contracts",
                                "AddressLines": {
                                    "$type": "Asi.Soa.Membership.DataContracts.AddressLineDataCollection, Asi.Contracts",
                                    "$values": [contact.Address]
                                },
                                "CityName": contact.City,
                                "CountryCode": countryId,
                                "CountryName": countryValue,
                                "CountrySubEntityCode": stateId,
                                "CountrySubEntityName": stateValue,
                                "FullAddress": contact.Address + "\r" + contact.City + "," + stateId + " " + contact.PinCode + "\r" + countryId,
                                "PostalCode": contact.PinCode,
                            },
                            "AddresseeText": "Mrs. Patrice Aaron\r\n" + contact.Address + "\r" + contact.City + "," + stateId + " " + contact.PinCode + "\r" + countryId,
                            "AddressPurpose": "Address",
                            "CommunicationPreferences": {
                                "$type": "Asi.Soa.Membership.DataContracts.CommunicationPreferenceDataCollection, Asi.Contracts",
                                "$values": [
                                    //{
                                    //    "$type": "Asi.Soa.Membership.DataContracts.CommunicationPreferenceData, Asi.Contracts",
                                    //    "Reason": "default"
                                    //},
                                    //{
                                    //    "$type": "Asi.Soa.Membership.DataContracts.CommunicationPreferenceData, Asi.Contracts",
                                    //    "Reason": "mail"
                                    //},
                                    {
                                        "$type": "Asi.Soa.Membership.DataContracts.CommunicationPreferenceData, Asi.Contracts",
                                        "Reason": "bill"
                                    },
                                    {
                                        "$type": "Asi.Soa.Membership.DataContracts.CommunicationPreferenceData, Asi.Contracts",
                                        "Reason": "ship"
                                    }
                                ]
                            },
                            //  "Email": "patriceaaron@imisdemo.com",
                            "Phones": {
                                "$type": "Asi.Soa.Membership.DataContracts.PhoneDataCollection, Asi.Contracts",
                                "$values": [
                                    {
                                        "$type": "Asi.Soa.Membership.DataContracts.PhoneData, Asi.Contracts",
                                        "Number": contact.Work,
                                        "PhoneType": "_Work Phone"
                                    },
                                    {
                                        "$type": "Asi.Soa.Membership.DataContracts.PhoneData, Asi.Contracts",
                                        "Number": contact.Mobile,
                                        "PhoneType": "Mobile"
                                    },
                                    {
                                        "$type": "Asi.Soa.Membership.DataContracts.PhoneData, Asi.Contracts",
                                        "Number": contact.Home,
                                        "PhoneType": "Address"
                                    }
                                ]
                            },
                        }
                    ]
                }
            }
            return object;
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.SaveContact = function (contact) {
        try {
            if ($scope.ContactModalForm.$valid) {

                var object = objectForSaveContact(contact);
                console.log(object);
                if (object != null && object != undefined && object != "") {
                    if (contact.ContactId == 0) {
                        billingToContactFactory.save(baseUrl, authToken, object, function (result) {
                            if (result != null && result != undefined && result != "") {
                                $scope.contact = {};
                                if (parseInt(result.Id) > 0) {

                                    var cData = $scope.billingToContacts;
                                    $scope.billingToContacts = {};
                                    $scope.billingToContacts = cData;
                                    $scope.billingToContacts.push(result);
                                    $rootScope.mainBillingToContacts.push(result);
                                    $scope.partyAndOrganizationData.push(result);
                                    $rootScope.mainPartyAndOrganizationData.push(result);
                                    setSelect2DataNew('ddlContact', $scope.billingToContacts);
                                    initSelect2('s2id_ddlContact', 'Select Adverdtiser');

                                }
                                $scope.contact.ContactId = 0;
                                $scope.selectedNamePrefixValue = "";
                                CreateContactButtonName();
                                var contactModal = angular.element(document.querySelector('#contactModal'));
                                contactModal.modal('hide');
                                $scope.ContactModalForm.$setPristine();
                                $scope.contactModalMsgs = false;
                                toastr.success('Save successfully.', 'Success!');
                            }
                            else {
                                toastr.error(result.message, 'Error');
                            }
                        })
                    } else {

                    }
                }
            }
            else {
                $scope.contactModalMsgs = true;
                var contactModal = angular.element(document.querySelector('#contactModal'))
                contactModal.show();
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.CancelContact = function (contact) {
        try {
            // console.log('Cancel called');
            $scope.ContactModalForm.$setPristine();
            $scope.contactModalMsgs = false;
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }


    //Dropdown change event
    $scope.test = "first";

    $scope.changeAdvertiser = function () {
        try {
            var advertiserSelected = $scope.order.BillTo;
            var DropDown = angular.element(document.querySelector('#ddlAdverdtiser'));
            var getSelectedData = DropDown.select2("data");
            if (getSelectedData != null && getSelectedData != undefined) {
                var advertiserId = getSelectedData.id;
                var aadvertiserValue = getSelectedData.text;

                if (advertiserId == undefined || advertiserId == null || advertiserId == "") {
                    if ($scope.order.Adverdtiser != undefined && $scope.order.Adverdtiser != "" && $scope.order.Adverdtiser != null) {
                        changeDropDownColor('s2id_ddlAdverdtiser', $scope.order.Adverdtiser);
                        advertiserId = $scope.order.Adverdtiser;
                    }
                    else {
                        changeDropDownColor('s2id_ddlAdverdtiser', "");
                    }
                } else {
                    changeDropDownColor('s2id_ddlAdverdtiser', advertiserId);
                }

                if (parseInt(advertiserSelected) == 0) {
                    var partyData = $scope.partyAndOrganizationData;
                    $scope.organizationWiseParty = [];

                    if ($scope.isEditMode == false) {
                        angular.forEach(partyData, function (person, key) {
                            if (person.PrimaryOrganization != undefined) {
                                if (person.PrimaryOrganization.OrganizationPartyId != undefined) {
                                    var organizationI = person.PrimaryOrganization.OrganizationPartyId;
                                    if (advertiserId == organizationI) {
                                        $scope.organizationWiseParty.push(person);

                                        if ($scope.organizationWiseParty.length > 0) {
                                            $scope.billingToContacts = $scope.organizationWiseParty;

                                            if ($scope.order.BillingContacts != undefined && $scope.order.BillingContacts != "" && $scope.order.BillingContacts != null) {
                                                var responseData = $scope.billingToContacts;
                                                var Contacts = alasql('SELECT PersonName FROM ? AS add WHERE Id  = ?', [responseData, $scope.order.BillingContacts.toString()]);
                                                changeDropDownColor('s2id_ddlContact', $scope.order.BillingContacts);
                                                if (Contacts != undefined && Contacts != null && Contacts != "") {
                                                    setAddressFielsManually($scope.order.BillingContacts, Contacts[0].PersonName.FullName);
                                                }
                                            }
                                            else {
                                                changeDropDownColor('s2id_ddlContact', "");
                                            }

                                        } else {
                                            $scope.billingToContacts = [];
                                            changeDropDownColor('s2id_ddlContact', "");
                                        }

                                    }
                                }
                            }
                        });
                        $scope.partyAddress = "";
                        $scope.partyMobile = "";
                        $scope.partyMemberType = "";
                        $scope.partyWork = "";
                        $scope.partyEmail = "";
                        $scope.partyHome = "";
                    }
                    // $scope.billingToContacts = $scope.organizationWiseParty;
                }
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function changeDropDownColor(controlId, controlValue) {
        try {
            if (controlValue != "" && controlValue != undefined && controlValue != null) {
                // id would be dynamic generated by select2
                var DropDown = angular.element(document.querySelector('#' + controlId));
                var select2ChosenDiv = DropDown.find('.select2-chosen');
                select2ChosenDiv.css('color', '#323232');
            } else {
                var DropDown = angular.element(document.querySelector('#' + controlId));
                var select2ChosenDiv = DropDown.find('.select2-chosen');
                select2ChosenDiv.css('color', '#999999');
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function changeCalledManually() {
        try {
            if ($scope.isEditMode == true) {
                var advertiserSelected = $scope.order.BillTo;
                if (parseInt(advertiserSelected) == 0) {

                    var DropDown = angular.element(document.querySelector('#ddlAdverdtiser'));
                    var getSelectedData = DropDown.select2("data");
                    var advertiserId = getSelectedData.id;
                    var aadvertiserValue = getSelectedData.text;
                    var partyData = $scope.partyAndOrganizationData;
                    $scope.organizationWiseParty = [];

                    angular.forEach(partyData, function (person, key) {
                        if (person.PrimaryOrganization != undefined) {
                            if (person.PrimaryOrganization.OrganizationPartyId != undefined) {
                                var organizationI = person.PrimaryOrganization.OrganizationPartyId;
                                if (advertiserId == organizationI) {
                                    $scope.organizationWiseParty.push(person);
                                    $scope.test = "secound";

                                    if ($scope.organizationWiseParty.length > 0) {
                                        $scope.billingToContacts = $scope.organizationWiseParty;
                                        if ($scope.order.BillingContacts != undefined && $scope.order.BillingContacts != "" && $scope.order.BillingContacts != null) {
                                            var responseData = $scope.billingToContacts;
                                            var Contacts = alasql('SELECT PersonName FROM ? AS add WHERE Id  = ?', [responseData, $scope.order.BillingContacts.toString()]);
                                            changeDropDownColor('s2id_ddlContact', $scope.order.BillingContacts);
                                            if (Contacts != undefined && Contacts != null && Contacts != "") {
                                                setAddressFielsManually($scope.order.BillingContacts, Contacts[0].PersonName.FullName);
                                            }
                                        }
                                        else {
                                            changeDropDownColor('s2id_ddlContact', "");
                                        }
                                    } else {
                                        $scope.billingToContacts = [];
                                        changeDropDownColor('s2id_ddlContact', "");
                                    }
                                }
                            }
                        }
                    });

                    if ($scope.test = "first") {
                        var billingDetailsData = $rootScope.billingDetails;
                        var IsAdvertiser = parseInt(billingDetailsData.BillTo);
                        if (IsAdvertiser == 0) {
                            var billingDetailsData = $rootScope.billingDetails;
                            $scope.order.BillingContacts = parseInt(billingDetailsData.BillToContactId);
                            setDroupDownValue('ddlContact', parseInt(billingDetailsData.BillToContactId));
                            $scope.isEditMode = false;
                        }
                    }

                }
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.ChangeReps = function () {
        try {
            var RepsDropDown = angular.element(document.querySelector('#ddlReps'));
            var getSelectedReps = RepsDropDown.select2("data").id;
            $scope.repsId = getSelectedReps.split(':')[1];
            $scope.repsName = RepsDropDown.select2("data").text;
            changeDropDownColor('s2id_ddlReps', $scope.repsId);
            if ($scope.repsId != undefined) {
                territoriesFactory.getAll($scope.repsId, function (result) {
                    if (result.statusCode == 1) {
                        if (result.data.length > 0) {
                            console.log(result);
                            initSelect2('s2id_ddlTerritories', 'Select Territories');
                            changeDropDownColor('s2id_ddlTerritories', "");
                            $scope.territories = result.data;
                            // $scope.tempArray.push(result.data);
                        }
                        else {
                            console.log(result);
                            $scope.territories = [];
                            $scope.selectedTerritories = "";
                            setDroupDownValue('ddlTerritories', 0);
                            initSelect2('s2id_ddlTerritories', 'Select Territories');
                            changeDropDownColor('s2id_ddlTerritories', "");
                        }
                    }
                    else {
                        toastr.error(result.message, 'Error');
                    }
                })
            } else {
                $scope.territories = [];
                $scope.selectedTerritories = "";
                setDroupDownValue('ddlTerritories', 0);
                initSelect2('s2id_ddlTerritories', 'Select Territories');
                changeDropDownColor('s2id_ddlTerritories', "");
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.ChangeTerritory = function () {
        try {
            var RepsDropDown = angular.element(document.querySelector('#ddlTerritories'));
            var getSelectedReps = RepsDropDown.select2("data").id;
            $scope.TerritoryId = getSelectedReps.split(':')[1];
            $scope.TerritoryName = RepsDropDown.select2("data").text;
            changeDropDownColor('s2id_ddlTerritories', $scope.TerritoryId);
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.test1 = "first";

    $scope.changeAgency = function () {
        try {
            var agencySelected = $scope.order.BillTo;
            //if (parseInt(agencySelected) == 1) {
            var DropDown = angular.element(document.querySelector('#ddlAgency'));
            var getSelectedData = DropDown.select2("data");
            if (getSelectedData != null && getSelectedData != undefined) {
                var advertiserId = getSelectedData.id;
                var aadvertiserValue = getSelectedData.text;
                //  changeDropDownColor('s2id_ddlAgency', advertiserId);
                // }
                //else {
                //  changeDropDownColor('s2id_ddlAgency', "");
                //}


                if (advertiserId == undefined || advertiserId == null || advertiserId == "") {
                    if ($scope.order.Agency != undefined && $scope.order.Agency != "" && $scope.order.Agency != null) {
                        changeDropDownColor('s2id_ddlAgency', $scope.order.Agency);
                        advertiserId = $scope.order.Agency;
                    }
                    else {
                        changeDropDownColor('s2id_ddlAgency', "");
                    }
                } else {
                    changeDropDownColor('s2id_ddlAgency', advertiserId);
                }
                //var DropDown = angular.element(document.querySelector('#ddlAgency'));
                //var getSelectedData = DropDown.select2("data");
                //var advertiserId = getSelectedData.id;
                //var aadvertiserValue = getSelectedData.text;

                //if (advertiserId == undefined || advertiserId == null || advertiserId == "") {
                //    changeDropDownColor('s2id_ddlAgency', "");
                //} else {
                //    changeDropDownColor('s2id_ddlAgency', advertiserId);
                //}


                if (parseInt(agencySelected) == 1) {
                    //var DropDown = angular.element(document.querySelector('#ddlAgency'));
                    //var getSelectedData = DropDown.select2("data");
                    //var advertiserId = getSelectedData.id;
                    //var aadvertiserValue = getSelectedData.text;
                    var partyData = $scope.partyAndOrganizationData;
                    $scope.organizationWiseParty = [];

                    if ($scope.isEditMode == false) {
                        angular.forEach(partyData, function (person, key) {
                            if (person.PrimaryOrganization != undefined) {
                                if (person.PrimaryOrganization.OrganizationPartyId != undefined) {
                                    var organizationI = person.PrimaryOrganization.OrganizationPartyId;
                                    if (advertiserId == organizationI) {
                                        $scope.organizationWiseParty.push(person);
                                        if ($scope.organizationWiseParty.length > 0) {
                                            $scope.billingToContacts = $scope.organizationWiseParty;
                                            if ($scope.order.BillingContacts != undefined && $scope.order.BillingContacts != "" && $scope.order.BillingContacts != null) {
                                                var responseData = $scope.billingToContacts;
                                                var Contacts = alasql('SELECT PersonName FROM ? AS add WHERE Id  = ?', [responseData, $scope.order.BillingContacts.toString()]);
                                                changeDropDownColor('s2id_ddlContact', $scope.order.BillingContacts);
                                                if (Contacts != undefined && Contacts != null && Contacts != "") {
                                                    setAddressFielsManually($scope.order.BillingContacts, Contacts[0].PersonName.FullName);
                                                }
                                            }
                                            else {
                                                changeDropDownColor('s2id_ddlContact', "");
                                            }
                                        } else {
                                            $scope.billingToContacts = [];
                                            changeDropDownColor('s2id_ddlContact', "");
                                        }

                                    }
                                }
                            }
                        });

                        $scope.partyAddress = "";
                        $scope.partyMobile = "";
                        $scope.partyMemberType = "";
                        $scope.partyWork = "";
                        $scope.partyEmail = "";
                        $scope.partyHome = "";
                    }

                    // $scope.billingToContacts = $scope.organizationWiseParty;
                }
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function changeCalledManually_Agency() {
        try {
            if ($scope.isEditMode == true) {
                var agencySelected = $scope.order.BillTo;
                if (parseInt(agencySelected) == 1) {

                    var DropDown = angular.element(document.querySelector('#ddlAgency'));
                    var getSelectedData = DropDown.select2("data");
                    var advertiserId = getSelectedData.id;
                    var aadvertiserValue = getSelectedData.text;
                    var partyData = $scope.partyAndOrganizationData;
                    $scope.organizationWiseParty = [];
                    angular.forEach(partyData, function (person, key) {
                        if (person.PrimaryOrganization != undefined) {
                            if (person.PrimaryOrganization.OrganizationPartyId != undefined) {
                                var organizationI = person.PrimaryOrganization.OrganizationPartyId;
                                if (advertiserId == organizationI) {
                                    $scope.organizationWiseParty.push(person);
                                    if ($scope.organizationWiseParty.length > 0) {
                                        $scope.billingToContacts = $scope.organizationWiseParty;
                                        $scope.test1 = "secound";
                                        var billingDetailsData = $rootScope.billingDetails;
                                        if ($scope.order.BillingContacts != undefined && $scope.order.BillingContacts != "" && $scope.order.BillingContacts != null) {
                                            var responseData = $scope.billingToContacts;
                                            var Contacts = alasql('SELECT PersonName FROM ? AS add WHERE Id  = ?', [responseData, $scope.order.BillingContacts.toString()]);
                                            changeDropDownColor('s2id_ddlContact', $scope.order.BillingContacts);
                                            if (Contacts != undefined && Contacts != null && Contacts != "") {
                                                setAddressFielsManually($scope.order.BillingContacts, Contacts[0].PersonName.FullName);
                                            }
                                        }
                                        else {
                                            changeDropDownColor('s2id_ddlContact', "");
                                        }
                                        if (parseInt(billingDetailsData.BillToContactId) == organizationI) {
                                        }
                                    } else {
                                        $scope.billingToContacts = [];
                                        changeDropDownColor('s2id_ddlContact', "");
                                    }
                                }
                            }
                        }
                    });

                    if ($scope.test1 = "first") {

                        var billingDetailsData = $rootScope.billingDetails;
                        var IsAgency = parseInt(billingDetailsData.BillTo);
                        if (IsAgency == 1) {
                            var billingDetailsData = $rootScope.billingDetails;
                            $scope.order.BillingContacts = parseInt(billingDetailsData.BillToContactId);
                            setDroupDownValue('ddlContact', parseInt(billingDetailsData.BillToContactId));
                            $scope.isEditMode = false;
                        }
                    }
                }
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onCountryChange = function () {
        try {
            var CountryDropDown = angular.element(document.querySelector('#ddlCountry'));
            var getSelectedCountry = CountryDropDown.select2("data");
            var id = getSelectedCountry.id;
            var value = getSelectedCountry.text;
            if (id != "" && id != undefined && id != null) {
                if (value != "" && value != undefined && value != null) {
                    var countries = $scope.countries;
                    changeDropDownColor('s2id_ddlCountry', id);
                    angular.forEach(countries, function (countryvalue, key) {
                        if (countryvalue.CountryName == value && countryvalue.CountryCode == id) {
                            $scope.SubEntityNameCaption = countryvalue.SubEntityNameCaption;
                            var state = countryvalue.CountrySubEntities;
                            if (state != "" && state != undefined && state != null && state.$values.length > 0) {
                                $scope.isStatehave = true;
                                $scope.advertiser.State = "";
                                $scope.states = state.$values;
                                // $scope.advertiser.CountrySubEntity = true;
                                $scope.advertiser.CountrySubEntity = "statelistY";
                                setDroupDownValue('ddlState', "");
                                initSelect2('s2id_ddlState', 'Select State');
                                changeDropDownColor('s2id_ddlState', "");
                            } else {
                                $scope.states = [];
                                $scope.advertiser.State = "";
                                $scope.isStatehave = false;
                                // $scope.advertiser.CountrySubEntity = false;
                                $scope.advertiser.CountrySubEntity = "statelistN";

                                //$scope.selectedTerritories = "";
                                setDroupDownValue('ddlState', "");
                                initSelect2('s2id_ddlState', 'Select State');
                                changeDropDownColor('s2id_ddlState', "");
                            }
                        }
                    });
                }
            } else {
                $scope.states = [];
                $scope.SubEntityNameCaption = "State";
                $scope.isStatehave = false;
                setDroupDownValue('ddlState', "");
                initSelect2('s2id_ddlState', 'Select State');
                changeDropDownColor('s2id_ddlState', "");
                var adDropDown = angular.element(document.querySelector('#ddlCountry'));
                adDropDown.select2('val', id);
                changeDropDownColor('s2id_ddlCountry', "");
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onCountryChange_Agency = function () {
        try {
            var CountryDropDown = angular.element(document.querySelector('#ddlCountry_Agency'));
            var getSelectedCountry = CountryDropDown.select2("data");
            var id = getSelectedCountry.id;
            var value = getSelectedCountry.text;
            if (id != "" && id != undefined && id != null) {
                if (value != "" && value != undefined && value != null) {
                    var countries = $scope.countries;
                    changeDropDownColor('s2id_ddlCountry_Agency', id);
                    angular.forEach(countries, function (countryvalue, key) {
                        if (countryvalue.CountryName == value && countryvalue.CountryCode == id) {
                            $scope.SubEntityNameCaption_Agency = countryvalue.SubEntityNameCaption;
                            var state = countryvalue.CountrySubEntities;
                            if (state != "" && state != undefined && state != null && state.$values.length > 0) {
                                $scope.isStatehave_Agency = true;
                                $scope.agency.State = "";
                                $scope.states = state.$values;
                                // $scope.agency.CountrySubEntity_Agency = true;
                                $scope.agency.CountrySubEntity_Agency = "statelistY";
                                setDroupDownValue('ddlState_Agency', "");
                                initSelect2('s2id_ddlState_Agency', 'Select State');
                                changeDropDownColor('s2id_ddlState_Agency', "");
                            } else {
                                $scope.isStatehave_Agency = false;
                                $scope.agency.State = "";
                                $scope.states = [];
                                // $scope.agency.CountrySubEntity_Agency = false;
                                $scope.agency.CountrySubEntity_Agency = "statelistN";
                                setDroupDownValue('ddlState_Agency', "");
                                initSelect2('s2id_ddlState_Agency', 'Select State');
                                changeDropDownColor('s2id_ddlState_Agency', "");
                            }
                        }
                    });
                }
            } else {
                $scope.states = [];
                $scope.SubEntityNameCaption_Agency = "State";
                $scope.isStatehave_Agency = false;
                setDroupDownValue('ddlState_Agency', "");
                initSelect2('s2id_ddlState_Agency', 'Select State');
                changeDropDownColor('s2id_ddlState_Agency', "");
                var adDropDown = angular.element(document.querySelector('#ddlCountry_Agency'));
                adDropDown.select2('val', id);
                changeDropDownColor('s2id_ddlCountry_Agency', "");
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.ChangeCountry_Contacts = function () {
        try {
            var CountryDropDown = angular.element(document.querySelector('#ddlCountry_Contact'));
            var getSelectedCountry = CountryDropDown.select2("data");
            var id = getSelectedCountry.id;
            var value = getSelectedCountry.text;
            if (id != "" && id != undefined && id != null) {
                if (value != "" && value != undefined && value != null) {
                    var countries = $scope.countries;
                    changeDropDownColor('s2id_ddlCountry_Contact', id);
                    angular.forEach(countries, function (countryvalue, key) {
                        if (countryvalue.CountryName == value && countryvalue.CountryCode == id) {
                            $scope.SubEntityNameCaption_Contact = countryvalue.SubEntityNameCaption;
                            var state = countryvalue.CountrySubEntities;
                            if (state != "" && state != undefined && state != null && state.$values.length > 0) {
                                $scope.isStatehave_Contact = true;
                                $scope.states = state.$values;
                                $scope.contact.CountrySubEntity_Contact = "statelistY";
                                setDroupDownValue('ddlState_Contact', "");
                                initSelect2('s2id_ddlState_Contact', 'Select State');
                                changeDropDownColor('s2id_ddlState_Contact', "");

                            } else {
                                $scope.isStatehave_Contact = false;
                                $scope.contact.State = "";
                                $scope.contact = [];
                                $scope.contact.CountrySubEntity_Contact = "statelistN";
                                setDroupDownValue('ddlState_Contact', "");
                                initSelect2('s2id_ddlState_Contact', 'Select State');
                                changeDropDownColor('s2id_ddlState_Contact', "");

                            }
                        }
                    });
                }
            } else {
                $scope.states = [];
                $scope.SubEntityNameCaption_Contact = "State";
                $scope.isStatehave_Contact = false;
                setDroupDownValue('ddlState_Contact', "");
                initSelect2('s2id_ddlState_Contact', 'Select State');
                changeDropDownColor('s2id_ddlState_Contact', "");
                var adDropDown = angular.element(document.querySelector('#ddlCountry_Contact'));
                adDropDown.select2('val', id);
                changeDropDownColor('s2id_ddlCountry_Contact', "");
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.changeContact = function () {
        try {
            //setDefaultPartyDetails();
            $scope.partyAddress = "";
            $scope.partyMobile = "";
            $scope.partyMemberType = "";
            $scope.partyWork = "";
            $scope.partyEmail = "";
            $scope.partyHome = "";


            var ContactDropDown = angular.element(document.querySelector('#ddlContact'));
            var getSelectedContact = ContactDropDown.select2("data");

            if (getSelectedContact != null) {
                var id = getSelectedContact.id;
                var value = getSelectedContact.text;
                changeDropDownColor('s2id_ddlContact', id);
                if (parseInt(id) > 0) {
                    if (id != "" && id != undefined && id != null) {
                        if (value != "" && value != undefined && value != null) {
                            var partyData = $scope.billingToContacts;
                            angular.forEach(partyData, function (partyValue, key) {

                                var partyFullName = partyValue.PersonName.FullName;
                                var partyId = partyValue.Id;
                                if (parseInt(partyId) == parseInt(id)) {

                                    var additionalAttr = partyValue.AdditionalAttributes;
                                    if (additionalAttr != undefined) {
                                        additionalAttr = partyValue.AdditionalAttributes.$values
                                        if (additionalAttr != undefined) {
                                            angular.forEach(additionalAttr, function (addValue, key) {
                                                var type = addValue.Name;
                                                if (type == "CustomerTypeDescription") {
                                                    $scope.partyMemberType = addValue.Value;
                                                }
                                            });
                                        }
                                    }

                                    var Phones = partyValue.Phones;
                                    if (Phones != undefined) {
                                        Phones = partyValue.Phones.$values;
                                        if (Phones != undefined) {
                                            angular.forEach(Phones, function (phoneValue, key) {
                                                var type = phoneValue.PhoneType;
                                                if (type == "Mobile") {
                                                    if (phoneValue.Number == "undefined") {
                                                        $scope.partyMobile = "";
                                                    }
                                                    else {
                                                        $scope.partyMobile = phoneValue.Number;
                                                    }
                                                }
                                                else if (type == "_Work Phone") {
                                                    if (phoneValue.Number == "undefined") {
                                                        $scope.partyWork = "";
                                                    }
                                                    else {
                                                        $scope.partyWork = phoneValue.Number;
                                                    }

                                                    // $scope.partyWork = phoneValue.Number;
                                                }
                                                else if (type == "Address") {
                                                    if (phoneValue.Number == "undefined") {
                                                        $scope.partyHome = "";
                                                    }
                                                    else {
                                                        $scope.partyHome = phoneValue.Number;
                                                    }
                                                    // $scope.partyHome = phoneValue.Number;
                                                }
                                            });
                                        }
                                    }


                                    var Emails = partyValue.Emails;
                                    if (Emails != undefined) {
                                        Emails = partyValue.Emails.$values;
                                        if (Emails != undefined) {
                                            angular.forEach(Emails, function (emailValue, key) {
                                                var type = emailValue.EmailType;
                                                if (type == "_Primary") {
                                                    if (emailValue.Address == "" && emailValue.Address == undefined && emailValue.Address == null) {
                                                        $scope.partyEmail = "";
                                                    }
                                                    else {
                                                        $scope.partyEmail = emailValue.Address;
                                                    }
                                                }
                                            });
                                        }
                                    }

                                    //ItemData.Addresses.$values[0].Address.FullAddress
                                    var partyAddress = partyValue.Addresses;
                                    if (partyAddress != undefined) {
                                        partyAddress = partyValue.Addresses.$values[0];
                                        if (partyAddress != undefined) {
                                            partyAddress = partyValue.Addresses.$values[0].Address;
                                            if (partyAddress != undefined) {
                                                //partyAddress = partyValue.Addresses.$values[0].Address.FullAddress;

                                                var streetArray = partyValue.Addresses.$values[0].Address.AddressLines.$values;
                                                var street = "";
                                                var addStreet = angular.element(document.querySelector('#addStreet'));
                                                addStreet.empty();
                                                angular.forEach(streetArray, function (itemdatavalue, key) {
                                                    if (key == 0) {
                                                        street = '<span>' + itemdatavalue + '</span>,<br />';
                                                    }
                                                    else {
                                                        street = street + '<span style="margin-left:65px">' + itemdatavalue + '</span>,<br />';
                                                    }
                                                });
                                                var finalDiv = $compile(street)($scope);
                                                addStreet.append(finalDiv);
                                                if (partyAddress != undefined) {
                                                    $scope.partyCity = partyValue.Addresses.$values[0].Address.CityName;
                                                    $scope.partyPostalCode = partyValue.Addresses.$values[0].Address.PostalCode;
                                                    $scope.partyCountry = partyValue.Addresses.$values[0].Address.CountryName;
                                                }
                                            }
                                        }
                                    }

                                }
                            });
                        }
                    }
                }
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function setDefaultPartyDetails() {
        try {
            $scope.partyAddress = "";
            $scope.partyMobile = "";
            $scope.partyMemberType = "";
            $scope.partyWork = "";
            $scope.partyEmail = "";
            $scope.partyHome = "";
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.setSameAsAbove = function () {
        try {
            // console.log('call');
            var checkboxStatus = this.contactSameAaAbove;
            if (checkboxStatus == true) {
                var flag = this.order.BillTo;
                if (parseInt(flag) == 0) {
                    var organisationDetails = $scope.organisation;
                    $scope.organisationDetailsID = organisationDetails.Id;
                    $scope.contact.Address = $scope.organisation.Addresses.$values[0].Address.AddressLines.$values[0];
                    $scope.selectedCountryValue = $scope.organisation.Addresses.$values[0].Address.CountryCode;
                    $scope.contact.City = $scope.organisation.Addresses.$values[0].Address.CityName;
                    $scope.contact.State = $scope.organisation.Addresses.$values[0].Address.CountrySubEntityCode;
                    $scope.contact.PinCode = $scope.organisation.Addresses.$values[0].Address.PostalCode;

                    if ($scope.contact.State == undefined || $scope.contact.State == null || $scope.contact.State == "") {
                        //  $scope.contact.State = $scope.organisation.Addresses.$values[0].Address.CountrySubEntityName;
                        $scope.contact.State = "nocodefound";
                    }

                    var Phones = $scope.organisation.Phones.$values;
                    angular.forEach(Phones, function (phoneValue, key) {
                        var type = phoneValue.PhoneType;
                        if (type == "Mobile") {
                            $scope.contact.Mobile = phoneValue.Number;
                        }
                        else if (type == "_Work Phone") {
                            $scope.contact.Work = phoneValue.Number;
                        }
                        else if (type == "Address") {
                            $scope.contact.Home = phoneValue.Number;
                        }
                    });
                }
                else if (parseInt(flag) == 1) {
                    var organisationDetails = $scope.organisation;
                    $scope.organisationDetailsID = organisationDetails.Id;
                    $scope.contact.Address = $scope.organisation.Addresses.$values[0].Address.AddressLines.$values[0];
                    $scope.selectedCountryValue = $scope.organisation.Addresses.$values[0].Address.CountryCode;
                    $scope.contact.City = $scope.organisation.Addresses.$values[0].Address.CityName;
                    $scope.contact.State = $scope.organisation.Addresses.$values[0].Address.CountrySubEntityCode;
                    $scope.contact.PinCode = $scope.organisation.Addresses.$values[0].Address.PostalCode;

                    if ($scope.contact.State == undefined || $scope.contact.State == null || $scope.contact.State == "") {
                        // $scope.contact.State = $scope.organisation.Addresses.$values[0].Address.CountrySubEntityName;
                        $scope.contact.State = "nocodefound";
                    }
                    var Phones = $scope.organisation.Phones.$values;
                    angular.forEach(Phones, function (phoneValue, key) {
                        var type = phoneValue.PhoneType;
                        if (type == "Mobile") {
                            $scope.contact.Mobile = phoneValue.Number;
                        }
                        else if (type == "_Work Phone") {
                            $scope.contact.Work = phoneValue.Number;
                        }
                        else if (type == "Address") {
                            $scope.contact.Home = phoneValue.Number;
                        }
                    });
                }
            }
            else {
                $scope.contactSameAaAbove = 0;
                $scope.contact.Address = "";
                $scope.selectedCountryValue = "";
                $scope.contact.City = "";
                $scope.contact.State = "";
                $scope.contact.PinCode = "";
                $scope.contact.Mobile = "";
                $scope.contact.Work = "";
                $scope.contact.Home = "";
                $scope.ContactModalForm.$setPristine();
                $scope.contactModalMsgs = false;
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.setContactEnableOrDisable = function (changeAdverdtiserValue, changeAgencyValue, changeBillToValue) {
        try {
            if (changeAdverdtiserValue != 0 && changeAdverdtiserValue != "" && changeAdverdtiserValue != undefined && changeAdverdtiserValue != null) {

                if (changeAgencyValue != 0 && changeAgencyValue != "" && changeAgencyValue != undefined && changeAgencyValue != null) {

                    if (parseInt(changeBillToValue) == 0 || parseInt(changeBillToValue) == 1) {
                        $scope.setNoClick = false;

                    } else {
                        $scope.setNoClick = true;
                    }
                } else {
                    $scope.setNoClick = true;
                }
            } else {
                $scope.setNoClick = true;
            }
            if (parseInt(changeBillToValue) == 0) {
                $scope.isRequired = false;
            }
            else if (parseInt(changeBillToValue) == 1) {
                $scope.isRequired = true;
            }
            //  console.log('called');
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.setBillToContact = function (changeBillToValue) {
        try {
            if (parseInt(changeBillToValue) == 0 || parseInt(changeBillToValue) == 1) {
                if (parseInt(changeBillToValue) == 0) {
                    var DropDown = angular.element(document.querySelector('#ddlAdverdtiser'));
                    var getSelectedData = DropDown.select2("data");
                    var advertiserId = getSelectedData.id;
                    var aadvertiserValue = getSelectedData.text;
                    changeDropDownColor('s2id_ddlContact', "");
                    var partyData = $scope.partyAndOrganizationData;
                    $scope.organizationWiseParty = [];
                    if (parseInt(advertiserId) > 0) {
                        angular.forEach(partyData, function (person, key) {
                            if (person.PrimaryOrganization != undefined) {
                                if (person.PrimaryOrganization.OrganizationPartyId != undefined) {
                                    var organizationI = person.PrimaryOrganization.OrganizationPartyId;
                                    if (advertiserId == organizationI) {
                                        $scope.organizationWiseParty.push(person);
                                    }
                                }
                            }
                        });
                    } else {
                        toastr.info('please select Adverdtiser', 'Information!');
                        $scope.organizationWiseParty = [];
                    }
                    $scope.partyAddress = "";
                    $scope.partyMobile = "";
                    $scope.partyMemberType = "";
                    $scope.partyWork = "";
                    $scope.partyEmail = "";
                    $scope.partyHome = "";
                    $scope.billingToContacts = $scope.organizationWiseParty;

                } else if (parseInt(changeBillToValue) == 1) {
                    var DropDown = angular.element(document.querySelector('#ddlAgency'));
                    var getSelectedData = DropDown.select2("data");
                    var agencyId = getSelectedData.id;
                    var agencyValue = getSelectedData.text;
                    changeDropDownColor('s2id_ddlContact', "");
                    var partyData = $scope.partyAndOrganizationData;
                    $scope.organizationWiseParty = [];
                    if (parseInt(agencyId) > 0) {
                        angular.forEach(partyData, function (person, key) {
                            if (person.PrimaryOrganization != undefined) {
                                if (person.PrimaryOrganization.OrganizationPartyId != undefined) {
                                    var organizationI = person.PrimaryOrganization.OrganizationPartyId;
                                    if (agencyId == organizationI) {
                                        $scope.organizationWiseParty.push(person);
                                    }
                                }
                            }
                        });
                    }
                    else {
                        toastr.info('please select Agency', 'Information!');
                        $scope.organizationWiseParty = [];
                    }
                    $scope.partyAddress = "";
                    $scope.partyMobile = "";
                    $scope.partyMemberType = "";
                    $scope.partyWork = "";
                    $scope.partyEmail = "";
                    $scope.partyHome = "";
                    $scope.billingToContacts = $scope.organizationWiseParty;
                }
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.AddrepsTerritories = function (ddlReps, ddlTerritories) {
        try {
            var TerritoryDropDown = angular.element(document.querySelector('#ddlTerritories'));
            var getSelectedTerritory = TerritoryDropDown.select2("data").id;
            $scope.TerritoryId = getSelectedTerritory.split(':')[1];
            $scope.TerritoryName = TerritoryDropDown.select2("data").text;

            var RepsDropDown = angular.element(document.querySelector('#ddlReps'));
            var getSelectedResp = RepsDropDown.select2("data").id;
            var RepsId = getSelectedResp.split(':')[1];
            var RepsName = RepsDropDown.select2("data").text;


            if ($scope.ordersFormReps.$valid) {
                var allRepsAndTerritory = $scope.parentArray;
                var isDublicate = false;
                if (allRepsAndTerritory.length > 0) {
                    var isBreak = false;
                    angular.forEach(allRepsAndTerritory, function (tempRTvalue, key) {
                        if (tempRTvalue.territoryId == $scope.TerritoryId
                            && tempRTvalue.repsId == RepsId) {
                            if (isBreak == false) {
                                toastr.info('This reps and territory already exits', 'Information!');
                                isBreak = true;
                                isDublicate = true;
                            }
                        }
                    });
                }
                if (isDublicate == false) {
                    var repsData = $scope.territories;
                    var commissionData = allRepsAndTerritory.length == 0 ? 100 : (100 / (allRepsAndTerritory.length + 1));
                    commissionData = parseFloat(commissionData);
                    commissionData = commissionData.toFixed(2);

                    angular.forEach(repsData, function (repsValue, key) {
                        if (repsValue.TerritoryId == $scope.TerritoryId && repsValue.RepId == RepsId) {
                            $scope.repTerritoryArray.repsName = RepsName;
                            $scope.repTerritoryArray.territoryName = $scope.TerritoryName;
                            //$scope.repTerritoryArray.commission = repsValue.Commission;
                            $scope.repTerritoryArray.Commission = repsValue.Commission;
                            $scope.repTerritoryArray.repsId = RepsId;
                            $scope.repTerritoryArray.territoryId = $scope.TerritoryId;

                            // var OrderContribution = commissionData;
                            var Split = commissionData;
                            var dynamicModelName = 'input' + $scope.TerritoryId + RepsId;
                            //  globalSalesValue[dynamicModelName] = OrderContribution;
                            globalSalesValue[dynamicModelName] = Split;
                        }
                    })

                    angular.forEach($scope.parentArray, function (adCommission, key) {
                        var repId = adCommission.repsId;
                        var TerritoryId = adCommission.territoryId;
                        // var OrderContribution = commissionData;
                        var Split = commissionData;
                        var dynamicModelName = 'input' + TerritoryId + repId;
                        // globalSalesValue[dynamicModelName] = OrderContribution;
                        globalSalesValue[dynamicModelName] = Split;

                        var text = globalSalesValue[dynamicModelName];
                        var the_string = "dynamicRowData.input" + TerritoryId + "" + repId + "";
                        var model = $parse(the_string);
                        model.assign($scope, text);
                    });


                    $scope.dvId = 'dv' + $scope.repTerritoryArray.territoryId + RepsId;
                    var mainDiv = angular.element(document.querySelector('#dvGroup'));
                    var id = $scope.dvId;
                    var model = $parse(id);
                    model.assign($scope, id);

                    var dynamicModelName = 'input' + $scope.repTerritoryArray.territoryId + $scope.repTerritoryArray.repsId;
                    var text = globalSalesValue[dynamicModelName];
                    var the_string = "dynamicRowData.input" + $scope.repTerritoryArray.territoryId + "" + $scope.repTerritoryArray.repsId + "";
                    var model = $parse(the_string);
                    model.assign($scope, text);


                    var htmlTemp = '<div class="col-md-4" id="' + id + '"><div class="reptDataBlockItem" ><div class="rep-ter">' + $scope.repTerritoryArray.repsName + '|' + $scope.repTerritoryArray.territoryName + '</div><div class="commission">(' + $scope.repTerritoryArray.Commission + '% comm.)</div><div class="form-group"><div class="input-group"><input type="text"  ng-model="' + the_string + '" allow-decimal-numbers-dot class="form-control" id="" value=""><div class="input-group-addon">%</div></div></div><a href="javascript:void(0);" ng-model="' + id + '" ng-click="RemoverepsTerritories(' + id + ',' + $scope.repTerritoryArray.territoryId + ',' + $scope.repTerritoryArray.repsId + ')"  class="deleteBlock">-</a></div></div>';
                    var finalDiv = $compile(htmlTemp)($scope);
                    mainDiv.append(finalDiv);
                    // setModalDynamic(the_string);
                    changeDropDownColor('s2id_ddlTerritories', "");
                    changeDropDownColor('s2id_ddlReps', "");
                    initSelect2('s2id_ddlReps', 'Select Reps');
                    initSelect2('s2id_ddlTerritories', 'Select Territories');
                    $scope.selectedReps = "";
                    $scope.selectedTerritories = "";
                    $scope.ordersFormReps.$setPristine();
                    $scope.showRepsMsgs = false;
                    $scope.parentArray.push($scope.repTerritoryArray);
                    $scope.repTerritoryArray = [];
                }
            }
            else {
                $scope.showRepsMsgs = true;
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function initSelect2(id, text) {
        try {
            var stateSelect2Button = angular.element(document.querySelector('#' + id))
            var selectedState = stateSelect2Button.find('span.select2-chosen');
            selectedState.text(text);
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function removeDynamicAddedCommissionHTML() {
        try {
            var myEl = angular.element(document.querySelector('#dvGroup'));
            myEl.html('');
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.RemoverepsTerritories = function (removeId, tId, rId) {
        try {
            // var salesCommission = $scope.parentArray;
            angular.forEach($scope.parentArray, function (salecommission, key) {
                if (salecommission.repsId == rId && salecommission.territoryId == tId) {
                    var dynamicModelName = 'input' + salecommission.territoryId + salecommission.repsId;
                    $scope.parentArray.splice(key, 1);
                    delete globalSalesValue[dynamicModelName];
                }
            });
            updateDynamicModelforReps();
            $scope.selectedReps = "";
            $scope.selectedTerritories = "";
            $scope.ordersFormReps.$setPristine();
            $scope.showRepsMsgs = false;
            var myEl = angular.element(document.querySelector('#' + removeId));
            myEl.remove();
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function updateDynamicModelforReps() {
        try {
            var commissionData = $scope.parentArray.length == 0 ? 100 : (100 / ($scope.parentArray.length));
            commissionData = parseFloat(commissionData);
            commissionData = commissionData.toFixed(2);

            angular.forEach($scope.parentArray, function (adCommission, key) {
                var repId = adCommission.repsId;
                var TerritoryId = adCommission.territoryId;
                //var OrderContribution = commissionData;
                var Split = commissionData;
                var dynamicModelName = 'input' + TerritoryId + repId;
                //globalSalesValue[dynamicModelName] = OrderContribution;
                globalSalesValue[dynamicModelName] = Split;

                var text = globalSalesValue[dynamicModelName];
                var the_string = "dynamicRowData.input" + TerritoryId + "" + repId + "";
                var model = $parse(the_string);
                model.assign($scope, text);
            });

        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onCustomerTypeChange_Advertiser = function () {
        try {
            var DropDown = angular.element(document.querySelector('#ddlMembeType'));
            var getSelectedData = DropDown.select2("data");
            var Id = getSelectedData.id;
            var Value = getSelectedData.text;

            changeDropDownColor('s2id_ddlMembeType', Id);
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onCustomerTypeChange_Aency = function () {
        try {
            var DropDown = angular.element(document.querySelector('#ddlMembeType_Agency'));
            var getSelectedData = DropDown.select2("data");
            var Id = getSelectedData.id;
            var Value = getSelectedData.text;

            changeDropDownColor('s2id_ddlMembeType_Agency', Id);
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onCustomerTypeChange_Contact = function () {
        try {
            var DropDown = angular.element(document.querySelector('#ddlMembeType_Contact'));
            var getSelectedData = DropDown.select2("data");
            var Id = getSelectedData.id;
            var Value = getSelectedData.text;

            changeDropDownColor('s2id_ddlMembeType_Contact', Id);
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onStateChange_Contacts = function () {
        try {
            var DropDown = angular.element(document.querySelector('#ddlState_Contact'));
            var getSelectedData = DropDown.select2("data");
            var Id = getSelectedData.id;
            var Value = getSelectedData.text;

            changeDropDownColor('s2id_ddlState_Contact', Id);
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onStateChange_Agency = function () {
        try {
            var DropDown = angular.element(document.querySelector('#ddlState_Agency'));
            var getSelectedData = DropDown.select2("data");
            var Id = getSelectedData.id;
            var Value = getSelectedData.text;

            changeDropDownColor('s2id_ddlState_Agency', Id);
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onStateChange_Advertiser = function () {
        try {
            var DropDown = angular.element(document.querySelector('#ddlState'));
            var getSelectedData = DropDown.select2("data");
            var Id = getSelectedData.id;
            var Value = getSelectedData.text;

            changeDropDownColor('s2id_ddlState', Id);
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function setAddressFielsManually(id, text) {
        try {
            //console.log('');
            //setDefaultPartyDetails();
            ////StartLoading();
            $scope.partyAddress = "";
            $scope.partyMobile = "";
            $scope.partyMemberType = "";
            $scope.partyWork = "";
            $scope.partyEmail = "";
            $scope.partyHome = "";

            var id = id;
            var value = text;
            changeDropDownColor('s2id_ddlContact', id);
            if (parseInt(id) > 0) {
                if (id != "" && id != undefined && id != null) {
                    if (value != "" && value != undefined && value != null) {
                        if ($rootScope.mainBillingToContacts.length > 0) {
                            $scope.billingToContacts = $rootScope.mainBillingToContacts;
                        }
                        var partyData = $scope.billingToContacts;
                        angular.forEach(partyData, function (partyValue, key) {

                            var partyFullName = partyValue.PersonName.FullName;
                            var partyId = partyValue.Id;
                            if (parseInt(partyId) == parseInt(id)) {

                                var additionalAttr = partyValue.AdditionalAttributes;
                                if (additionalAttr != undefined) {
                                    additionalAttr = partyValue.AdditionalAttributes.$values
                                    if (additionalAttr != undefined) {
                                        angular.forEach(additionalAttr, function (addValue, key) {
                                            var type = addValue.Name;
                                            if (type == "CustomerTypeDescription") {
                                                $scope.partyMemberType = addValue.Value;
                                            }
                                        });
                                    }
                                }

                                var Phones = partyValue.Phones;
                                if (Phones != undefined) {
                                    Phones = partyValue.Phones.$values;
                                    if (Phones != undefined) {
                                        angular.forEach(Phones, function (phoneValue, key) {
                                            var type = phoneValue.PhoneType;
                                            if (type == "Mobile") {
                                                if (phoneValue.Number == "undefined") {
                                                    $scope.partyMobile = "";
                                                }
                                                else {
                                                    $scope.partyMobile = phoneValue.Number;
                                                }
                                            }
                                            else if (type == "_Work Phone") {
                                                if (phoneValue.Number == "undefined") {
                                                    $scope.partyWork = "";
                                                }
                                                else {
                                                    $scope.partyWork = phoneValue.Number;
                                                }

                                                // $scope.partyWork = phoneValue.Number;
                                            }
                                            else if (type == "Address") {
                                                if (phoneValue.Number == "undefined") {
                                                    $scope.partyHome = "";
                                                }
                                                else {
                                                    $scope.partyHome = phoneValue.Number;
                                                }
                                                // $scope.partyHome = phoneValue.Number;
                                            }
                                        });
                                    }
                                }


                                var Emails = partyValue.Emails;
                                if (Emails != undefined) {
                                    Emails = partyValue.Emails.$values;
                                    if (Emails != undefined) {
                                        angular.forEach(Emails, function (emailValue, key) {
                                            var type = emailValue.EmailType;
                                            if (type == "_Primary") {
                                                if (emailValue.Address == "" && emailValue.Address == undefined && emailValue.Address == null) {
                                                    $scope.partyEmail = "";
                                                }
                                                else {
                                                    $scope.partyEmail = emailValue.Address;
                                                }
                                            }
                                        });
                                    }
                                }

                                //ItemData.Addresses.$values[0].Address.FullAddress
                                var partyAddress = partyValue.Addresses;
                                if (partyAddress != undefined) {
                                    partyAddress = partyValue.Addresses.$values[0];
                                    if (partyAddress != undefined) {
                                        partyAddress = partyValue.Addresses.$values[0].Address;
                                        if (partyAddress != undefined) {
                                            //partyAddress = partyValue.Addresses.$values[0].Address.FullAddress;
                                            var streetArray = partyValue.Addresses.$values[0].Address.AddressLines.$values;
                                            var street = "";
                                            var addStreet = angular.element(document.querySelector('#addStreet'));
                                            addStreet.empty();
                                            angular.forEach(streetArray, function (itemdatavalue, key) {
                                                if (key == 0) {
                                                    street = '<span>' + itemdatavalue + '</span>,<br />';
                                                }
                                                else {
                                                    street = street + '<span style="margin-left:65px">' + itemdatavalue + '</span>,<br />';
                                                }
                                            });
                                            var finalDiv = $compile(street)($scope);
                                            addStreet.append(finalDiv);
                                            if (partyAddress != undefined) {
                                                $scope.partyCity = partyValue.Addresses.$values[0].Address.CityName;
                                                $scope.partyPostalCode = partyValue.Addresses.$values[0].Address.PostalCode;
                                                $scope.partyCountry = partyValue.Addresses.$values[0].Address.CountryName;
                                            }
                                        }
                                    }
                                }
                            }
                            // EndLoading();
                        });
                    } else {
                        // EndLoading();
                    }
                } else {
                    // EndLoading();
                }
            } else {
                // EndLoading();
            }
        }
        catch (e) { 
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    getAdvertiserThroughASI();
    getCountryThroughASI();
    GetAllReps();
    getOrderId();

})
