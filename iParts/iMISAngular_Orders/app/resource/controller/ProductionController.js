﻿'use strict';

app.controller('ProductionController', function ProductionController($scope, $stateParams, $compile, $sce, $rootScope, $parse, $location,
    productionFactory, mediaAssetFactory, $exceptionHandler, ordersFactory, authKeyService, productionStatusFactory, advertiserFactory, mediaOrderProductionFactory, mediaOrderLinesFactory, positionFactory, separationFactory, toastr) {
    console.log('calleded');
    var authKey = authKeyService.getConstantAuthKey();
    $scope.baseUrl = authKey.baseUrl;
    var authKey = authKeyService.getConstantAuthKey();
    var baseUrl = authKey.baseUrl;
    var authToken = authKey.authToken;
    $scope.partyAndOrganizationData = [];
    //tab setting
    $scope.$parent.tabOrderId = $stateParams.AdOrderId;
    $scope.$parent.setProductionTabActive();

    //page modal
    $scope.production = {};
    $scope.production.BuyId = $stateParams.AdOrderId;
    $scope.issueDateWiseAdjustment = [];
    $scope.isShowDefaultTable = false;
    $scope.selectedIssueMediaOrder = {};
    $scope.selectedCheckbox = [];
    $scope.selectedMediaOrderIds = [];

    // page array data
    $scope.ProductionTab = [];
    $scope.flag = true;
    $scope.dvNew = false;
    $scope.dvPickup = true;


    //page modal
    $scope.productionStatus = {};
    $scope.positions = {};
    $scope.separations = {};
    $scope.mediaAssets = {};
    $scope.MediaAssetIssueDate = "";
    $scope.tempArray = [];
    $scope.mediaAsset = [];
    $scope.production.NewPickupInd = 1;
    $scope.production.CreateExpectedInd = 0;
    $scope.production.AdvertiserAgencyInd = 1;
    $scope.buttonText = "Add";


    $scope.FromSelect = {
        multiple: false,
        formatSearching: 'Searching the MediaAsset...',
        formatNoMatches: 'No Media Asset found'
    };

    $scope.ContactSelect = {
        multiple: false,
        formatSearching: 'Searching the Contact...',
        formatNoMatches: 'No Contact found'
    };

    $scope.StatusSelect = {
        multiple: false,
        formatSearching: 'Searching the Status...',
        formatNoMatches: 'No Status found'
    };

    $scope.PositionSelect = {
        multiple: false,
        formatSearching: 'Searching the Status...',
        formatNoMatches: 'No Position found'
    };

    var adClosingDateControl = angular.element(document.querySelector('#txtOnDate'));
    adClosingDateControl.datepicker();

    function initGrossandNetCost() {
        var totalSum = alasql('SELECT sum(GrossCost),sum(NetCost) FROM ? AS add ', [$scope.issueDateWiseAdjustment]);
        var grossAmount = parseFloat(totalSum[0]["SUM(GrossCost)"]);
        var netAmount = parseFloat(totalSum[0]["SUM(NetCost)"]);
        $rootScope.GrossCost = (grossAmount).toFixed(2);
        $rootScope.NetCost = (netAmount).toFixed(2);
    }

    function getNoOfIssueMediaOrder() {
        try {
            var BuyerId = $scope.production.BuyId;
            if (BuyerId != null && BuyerId != undefined && BuyerId != "") {
                ordersFactory.getMediaOrderbyBuyId(BuyerId, function (result) {
                    if (result.statusCode == 1) {
                        console.log(result);
                        if (result.data != null) {
                            if (result.data.length > 0) {
                                $scope.isShowDefaultTable = false;
                                $scope.issueDateWiseAdjustment = result.data;
                                initGrossandNetCost();
                            }
                            else {
                                $scope.isShowDefaultTable = true;
                                $scope.issueDateWiseAdjustment = [];
                            }
                        } else {
                            $scope.isShowDefaultTable = true;
                            $scope.issueDateWiseAdjustment = [];
                        }
                    }
                    else if (result.statusCode == 3) {
                        $scope.isShowDefaultTable = true;
                        $scope.issueDateWiseAdjustment = [];
                    }
                    else {
                        toastr.error(result.message, 'Error!');
                    }
                });
            }
            else {
                console.log('Buyer Id is null');
            }

        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }
    function getAllProductionStatus() {
        try {
            productionStatusFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            $scope.productionStatus = result.data;
                            initGrossandNetCost();
                        }
                        else {
                            $scope.productionStatus = {};
                        }
                    } else {
                        $scope.productionStatus = {};
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.productionStatus = {};
                }
                else {
                    $scope.productionStatus = {};
                    toastr.error(result.message, 'Error!');
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getAllPosition() {
        try {
            positionFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            $scope.positions = result.data;
                            initGrossandNetCost();
                        }
                        else {
                            $scope.positions = {};
                        }
                    } else {
                        $scope.positions = {};
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.positions = {};
                }
                else {
                    $scope.positions = {};
                    toastr.error(result.message, 'Error!');
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getAllSeparation() {
        try {
            separationFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            $scope.separations = result.data;
                            initGrossandNetCost();
                        }
                        else {
                            $scope.separations = {};
                        }
                    } else {
                        $scope.separations = {};
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.separations = {};
                }
                else {
                    $scope.separations = {};
                    toastr.error(result.message, 'Error!');
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getOrdersNotInBuyer(buyId, adid) {
        try {
            mediaAssetFactory.getOrdersNotInBuyer(buyId, adid, function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            // $scope.mediaAssets = result.data;

                            angular.forEach(result.data, function (value, key) {
                                var issueDate = getDateInFormat(value.IssueDate);
                                var pageNo = value.PageNumber == null ? '' : value.PageNumber;
                                if (pageNo != null && pageNo != undefined && pageNo != "") {
                                    $scope.mediaAsset.MediaAssetIssueDate = value.MediaAssetName + ' | ' + issueDate + ' | ' + pageNo;
                                }
                                else {
                                    $scope.mediaAsset.MediaAssetIssueDate = value.MediaAssetName + ' | ' + issueDate;
                                }
                                $scope.mediaAsset.MediaOrderId = value.MediaOrderId;
                                $scope.tempArray.push($scope.mediaAsset);
                                $scope.mediaAsset = [];
                            })
                            $scope.mediaAssets = $scope.tempArray;
                            initGrossandNetCost();
                        }
                        else {
                            $scope.mediaAssets = [];
                        }
                    } else {
                        $scope.mediaAssets = [];
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.mediaAssets = [];
                }
                else {
                    $scope.mediaAssets = [];
                    toastr.error(result.message, 'Error!');
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getAdvertiserThroughASI() {
        try {
            var HasNext = true;
            var offset = 0;
            var advertisers = [];
            var contacts = [];

            function getAdvertiserData() {
                advertiserFactory.getAllAdvertiser1(baseUrl, authToken, offset, function (result) {
                    if (result != null && result != undefined && result != "") {
                        var ItemData = result.Items.$values
                        console.log(ItemData);
                        angular.forEach(ItemData, function (itemdatavalue, key) {
                            $scope.partyAndOrganizationData.push(itemdatavalue);
                            var type = itemdatavalue.$type;
                            if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                                advertisers.push(itemdatavalue);
                            }
                            else if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                                contacts.push(itemdatavalue);
                            }
                        });

                        $scope.advertisers = advertisers;
                        $scope.agencies = advertisers;
                        //$scope.billingToContacts = contacts;
                        $scope.personData = contacts;
                        var TotalCount = result.TotalCount;
                        HasNext = result.HasNext;
                        offset = result.Offset;
                        var Limit = result.Limit;
                        var NextOffset = result.NextOffset;
                        if (HasNext == true) {
                            offset = NextOffset;
                            getAdvertiserData();
                        } else {
                            // $scope.setBillToContactClickable = false;
                            // changeCalledManually();
                            // changeCalledManually_Agency();
                            //$scope.changeAdvertiser();
                            // $scope.changeAgency();
                        }
                        $scope.production.AdvertiserAgencyInd = 1;
                    }
                    else {
                        $scope.advertisers = [];
                    }
                })
            }
            getAdvertiserData();
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    }
    function commonInit() {

    }
    //deleteIssueOrderAdjustment
    $scope.deleteMediaOrderMediaLinesbyMediaOrderId = function (objProduction) {
        try {
            //toastr["warning"]("Clear itself?<br /><br /><button type='button' class='btn clear'>Yes</button>")
            bootbox.confirm({
                message: "Are you sure you want to delete the selected object(s) and all of their children?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    // alert(result);
                    if (result) {
                        debugger;
                        var mediaOrderId = objProduction.MediaOrderId;
                        if (mediaOrderId != null && mediaOrderId != undefined && mediaOrderId != "") {
                            mediaOrderLinesFactory.deleteMediaOrderLinesbyMediaOrder(mediaOrderId, function (result) {
                                if (result.statusCode == 1) {

                                    if (result.data != null) {
                                        var data = result.data;
                                        var grossCost = data.GrossCost;
                                        var netCost = data.NetCost;
                                        $rootScope.GrossCost = (grossCost).toFixed(2);
                                        $rootScope.NetCost = (netCost).toFixed(2);
                                        getNoOfIssueMediaOrder();

                                        console.log(result);
                                        //$scope.isShowDefaultGrossAdjustmentTable = true;
                                        //$scope.isShowDefaultNetAdjustmentTable = true;
                                        //$scope.grossAdAdjustments = [];
                                        //$scope.netAdAdjustments = [];
                                        toastr.success('Deleted successfully.', 'Success!');
                                    }
                                }
                                else {
                                    toastr.error(result.message, 'Error!');
                                }
                            });
                        }
                        else {
                            console.log('media Order Id is null');
                        }
                    }
                }
            });

            //if (objAdjustment.IssueDateId > 0) {
            //    angular.forEach($scope.issueDateWiseAdjustment, function (objIssueDateWiseAdjustment, key) {
            //        if (objIssueDateWiseAdjustment.IssueDateId == objAdjustment.IssueDateId) {
            //            $scope.issueDateWiseAdjustment.splice(key, 1);
            //        }
            //    });
            //}
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    //editIssueOrderAdjustment
    $scope.editMediaOrderMediaLinesbyMediaOrderId = function (MediaOrderId) {
        $location.path('ordersDetails/mediaSchedule/' + $stateParams.AdOrderId + '/' + MediaOrderId);
    }
    $scope.setEnableOrDisable = function (rdoName) {

        if (parseInt(rdoName) == 1) {
            $scope.dvNew = false;
            $scope.dvPickup = true;
            $scope.production.ChangesInd = "";
            $scope.production.PickupMediaOrderId = "";
            initSelect2('s2id_ddlFrom', 'Media Asset, Issue Date,Pg#');
            changeDropDownColor('s2id_ddlFrom', "");
            $scope.production.CreateExpectedInd = 0;
        }
        else if (parseInt(rdoName) == 0) {
            $scope.dvNew = true;
            $scope.dvPickup = false;
            $scope.production.CreateExpectedInd = "";
            $scope.production.OnHandInd = "";
            $scope.production.MaterialExpectedDate = "";
            $scope.production.TrackingNumber = "";
        }
        //  console.log('called');
    }

    $scope.Save = function (production) {
        try {
            console.log("called");
            production.MediaOrderIds = $scope.selectedMediaOrderIds;
            if (production.PickupMediaOrderId != null && production.PickupMediaOrderId != "" && production.PickupMediaOrderId != undefined) {
                production.PickupMediaOrderId = parseInt(production.PickupMediaOrderId);
            }
            if (production.NewPickupInd == "0") {
                production.NewPickupInd = false;
            }
            else if (production.NewPickupInd == "1") {
                production.NewPickupInd = true;
            }
            if (production.AdvertiserAgencyInd == "0") {
                production.AdvertiserAgencyInd = false;
            }
            else if (production.AdvertiserAgencyInd == "1") {
                production.AdvertiserAgencyInd = true;
            }
            if (production.CreateExpectedInd == "0") {
                production.CreateExpectedInd = false;
            }
            else if (production.CreateExpectedInd == "1") {
                production.CreateExpectedInd = true;
            }

            if (production.MaterialExpectedDate == undefined) {
                production.MaterialExpectedDate = "";
            }
            if (production.TrackingNumber == undefined) {
                production.TrackingNumber = "";
            }
            else {
                production.TrackingNumber = parseInt(production.TrackingNumber);
            }
            if (production.ChangesInd == undefined) {
                production.ChangesInd = null;
            }
            if (production.OnHandInd == undefined) {
                production.OnHandInd = null;
            }
            if (production.OriginalFile == undefined) {
                production.OriginalFile = null;
                production.OriginalFileExtension = null;
            }
            if (production.ProofFile == undefined) {
                production.ProofFile = null;
                production.ProofFileExtension = null;
            }
            if (production.FinalFile == undefined) {
                production.FinalFile = null;
                production.FinalFileExtension = null;
            }
            if (production.WebAdUrl == undefined) {
                production.WebAdUrl = "";
            }
            if (production.TearSheets == undefined) {
                production.TearSheets = "";
            }
            else {
                production.TearSheets = parseInt(production.TearSheets);
            }
            if (production.HeadLine == undefined) {
                production.HeadLine = "";
            }
            if (production.PageNumber == undefined) {
                production.PageNumber = "";
            }
            else {
                production.PageNumber = parseInt(production.PageNumber);
            }
            if (production.ProductionComment == undefined) {
                production.ProductionComment = "";
            }
            mediaOrderProductionFactory.save(production, function (result) {
                if (result.statusCode == 1) {
                    $scope.production = {};
                    $scope.production.BuyId = $stateParams.AdOrderId;
                    $scope.production.NewPickupInd = 1;
                    $scope.production.CreateExpectedInd = 0;
                    $scope.production.AdvertiserAgencyInd = 1;
                    $scope.originalFileImage = "";
                    $scope.proofFileImage = "";
                    $scope.finalFileImage = "";
                    $scope.originalFileImageSrc = "";
                    $scope.proofFileImageSrc = "";
                    $scope.finalFileImageSrc = "";
                    $scope.tempPdfPathForOriginal = "";
                    $scope.tempPdfPathForProof = "";
                    $scope.tempPdfPathForFinal = "";
                    //$scope.production.MediaOrderIds = [];
                    $scope.selectedIssueMediaOrder = [];
                    $scope.selectedCheckbox = [];
                    $scope.selectedMediaOrderIds = [];
                    $scope.selectedAdvertiserIds = [];
                    $scope.selectedAgencyIds = [];
                    //getNoOfIssueMediaOrder();
                    initSelect2('s2id_ddlFrom', 'Media Asset | Issue Date | Pg#');
                    initSelect2('s2id_ddlContact', 'Select Contact');
                    initSelect2('s2id_ddlStatus', 'Select Status');
                    initSelect2('s2id_ddlSeparation', 'Select Separation');
                    initSelect2('s2id_ddlPosition', 'Select Position');
                    changeDropDownColor('s2id_ddlFrom', "");
                    changeDropDownColor('s2id_ddlContact', "");
                    changeDropDownColor('s2id_ddlStatus', "");
                    changeDropDownColor('s2id_ddlSeparation', "");
                    changeDropDownColor('s2id_ddlPosition', "");
                    $scope.productionForm.$setPristine();
                    $scope.showMsgs = false;
                    toastr.success('Order save successfully.', 'Success!');

                }
                else
                    toastr.error(result.message, 'Error!');
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getDateInFormat(date) {
        try {
            var dateObj = new Date(date);
            var month = dateObj.getMonth() + 1;
            if (month.toString().length == 1) {
                month = "0" + month;
            }
            var day = dateObj.getDate();
            if (day.toString().length == 1) {
                day = "0" + day;
            }
            var year = dateObj.getFullYear();
            var newdate = month + "/" + day + "/" + year;
            return newdate;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    }
    $scope.selectedAdvertiserIds = [];
    $scope.selectedAgencyIds = [];
    $scope.checkboxChange = function (objProduction) {
        try {
            var grossAndNetDiv = angular.element(document.querySelector('#grossAndNetDiv'))
            grossAndNetDiv.addClass('no-click');

            var qId = objProduction.IssueDateId;
            var mId = objProduction.MediaAssetId;
            var mOrderId = objProduction.MediaOrderId;
            var advertiserId = objProduction.AdvertiserId;
            var agencyId = objProduction.AgencyId;

            var idx = $scope.selectedCheckbox.indexOf(qId);
            if (idx > -1) {
                $scope.selectedCheckbox.splice(idx, 1);
                angular.forEach($scope.selectedMediaOrderIds, function (data, key) {
                    if (parseInt(data) == parseInt(mOrderId)) {
                        $scope.selectedMediaOrderIds.splice(key, 1);
                        $scope.selectedAdvertiserIds.splice(key, 1);
                        $scope.selectedAgencyIds.splice(key, 1);
                    }

                });
                if ($scope.selectedCheckbox.length < 1) {
                    $scope.tempGrossAmount = parseFloat(0).toFixed(2);
                    //initPageControls();
                    $scope.isShowDefaultGrossAdjustmentTable = true;
                    $scope.isShowDefaultNetAdjustmentTable = true;
                    $scope.production = {};
                    $scope.production.BuyId = $stateParams.AdOrderId;
                    $scope.production.NewPickupInd = 1;
                    $scope.production.CreateExpectedInd = 0;
                    $scope.production.AdvertiserAgencyInd = 1;
                    $scope.originalFileImage = "";
                    $scope.proofFileImage = "";
                    $scope.finalFileImage = "";
                    $scope.originalFileImageSrc = "";
                    $scope.proofFileImageSrc = "";
                    $scope.finalFileImageSrc = "";
                    $scope.tempPdfPathForOriginal = "";
                    $scope.tempPdfPathForProof = "";
                    $scope.tempPdfPathForFinal = "";
                    initSelect2('s2id_ddlFrom', 'Media Asset | Issue Date | Pg#');
                    initSelect2('s2id_ddlContact', 'Select Contact');
                    initSelect2('s2id_ddlStatus', 'Select Status');
                    initSelect2('s2id_ddlSeparation', 'Select Separation');
                    initSelect2('s2id_ddlPosition', 'Select Position');
                    changeDropDownColor('s2id_ddlFrom', "");
                    changeDropDownColor('s2id_ddlContact', "");
                    changeDropDownColor('s2id_ddlStatus', "");
                    changeDropDownColor('s2id_ddlSeparation', "");
                    changeDropDownColor('s2id_ddlPosition', "");
                    $scope.buttonText = "Add";
                };
                if ($scope.selectedCheckbox.length == 1) {

                    $scope.selectedMediaOrderId = $scope.selectedMediaOrderIds[0];
                    $scope.selectedAdvertiserId = $scope.selectedAdvertiserIds[0];
                    $scope.selectedAgencyId = $scope.selectedAgencyIds[0];
                    getOrdersNotInBuyer($scope.$parent.tabOrderId, $scope.selectedAdvertiserId);
                    var changeBillToValue = $scope.production.AdvertiserAgencyInd;
                    GetBillToContact(changeBillToValue);
                    getMediaOrderProduction($scope.selectedMediaOrderId);

                }
                if ($scope.selectedCheckbox.length > 1) {
                    $scope.production = {};
                    $scope.production.BuyId = $stateParams.AdOrderId;
                    $scope.production.NewPickupInd = 1;
                    $scope.production.CreateExpectedInd = 0;
                    $scope.production.AdvertiserAgencyInd = 1;
                    initSelect2('s2id_ddlFrom', 'Media Asset | Issue Date | Pg#');
                    initSelect2('s2id_ddlContact', 'Select Contact');
                    initSelect2('s2id_ddlStatus', 'Select Status');
                    initSelect2('s2id_ddlSeparation', 'Select Separation');
                    initSelect2('s2id_ddlPosition', 'Select Position');
                    changeDropDownColor('s2id_ddlFrom', "");
                    changeDropDownColor('s2id_ddlContact', "");
                    changeDropDownColor('s2id_ddlStatus', "");
                    changeDropDownColor('s2id_ddlSeparation', "");
                    changeDropDownColor('s2id_ddlPosition', "");
                    $scope.originalFileImage = "";
                    $scope.proofFileImage = "";
                    $scope.finalFileImage = "";
                    $scope.originalFileImageSrc = "";
                    $scope.proofFileImageSrc = ""; $scope.selectedAdvertiserIds
                    $scope.finalFileImageSrc = "";
                    $scope.tempPdfPathForOriginal = "";
                    $scope.tempPdfPathForProof = "";
                    $scope.tempPdfPathForFinal = "";
                    $scope.buttonText = "Add";
                }
            }
            else {
                $scope.selectedCheckbox.push(qId);
                $scope.selectedMediaOrderIds.push(mOrderId);
                $scope.selectedAdvertiserIds.push(advertiserId);
                $scope.selectedAgencyIds.push(agencyId);
                if ($scope.selectedCheckbox.length > 1) {
                    $scope.tempGrossAmount = parseFloat(0).toFixed(2);
                    $scope.isShowDefaultGrossAdjustmentTable = true;
                    $scope.isShowDefaultNetAdjustmentTable = true;
                    $scope.grossAdAdjustments = [];
                    $scope.netAdAdjustments = [];
                }

                if ($scope.selectedCheckbox.length == 1) {
                    $scope.selectedMediaOrderId = $scope.selectedMediaOrderIds[0];
                    $scope.selectedAdvertiserId = $scope.selectedAdvertiserIds[0];
                    $scope.selectedAgencyId = $scope.selectedAgencyIds[0];
                    getOrdersNotInBuyer($scope.$parent.tabOrderId, $scope.selectedAdvertiserId);
                    // getMediaOrderProduction($scope.selectedMediaOrderId);
                    var changeBillToValue = $scope.production.AdvertiserAgencyInd;
                    GetBillToContact(changeBillToValue);
                    getMediaOrderProduction($scope.selectedMediaOrderId);

                }
            }
            if ($scope.selectedCheckbox.length > 1) {
                $scope.production = {};
                $scope.production.BuyId = $stateParams.AdOrderId;
                $scope.production.NewPickupInd = 1;
                $scope.production.CreateExpectedInd = 0;
                $scope.production.AdvertiserAgencyInd = 1;
                initSelect2('s2id_ddlFrom', 'Media Asset | Issue Date | Pg#');
                initSelect2('s2id_ddlContact', 'Select Contact');
                initSelect2('s2id_ddlStatus', 'Select Status');
                initSelect2('s2id_ddlSeparation', 'Select Separation');
                initSelect2('s2id_ddlPosition', 'Select Position');
                changeDropDownColor('s2id_ddlFrom', "");
                changeDropDownColor('s2id_ddlContact', "");
                changeDropDownColor('s2id_ddlStatus', "");
                changeDropDownColor('s2id_ddlSeparation', "");
                changeDropDownColor('s2id_ddlPosition', "");
                $scope.originalFileImage = "";
                $scope.proofFileImage = "";
                $scope.finalFileImage = "";
                $scope.originalFileImageSrc = "";
                $scope.proofFileImageSrc = "";
                $scope.finalFileImageSrc = "";
                $scope.tempPdfPathForOriginal = "";
                $scope.tempPdfPathForProof = "";
                $scope.tempPdfPathForFinal = "";
                $scope.buttonText = "Add";
            }


        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    };

    $scope.changeddlFrom = function () {
        var FromDropDown = angular.element(document.querySelector('#ddlFrom'));
        var Id = FromDropDown.select2("data").id;
        changeDropDownColor('s2id_ddlFrom', Id);
    }
    $scope.changeddlContact = function () {
        //var ContactDropDown = angular.element(document.querySelector('#ddlContact'));
        //var Id = ContactDropDown.select2("data").id;
        changeDropDownColor('s2id_ddlContact', $scope.production.MaterialContactId);
    }
    $scope.changeddlStatus = function () {
        var StatusDropDown = angular.element(document.querySelector('#ddlStatus'));
        var Id = StatusDropDown.select2("data").id;
        changeDropDownColor('s2id_ddlStatus', Id);
    }
    $scope.changeddlSeparation = function () {
        var SeparationDropDown = angular.element(document.querySelector('#ddlSeparation'));
        var Id = SeparationDropDown.select2("data").id;
        changeDropDownColor('s2id_ddlSeparation', Id);
    }
    $scope.changeddlPosition = function () {
        var PositionDropDown = angular.element(document.querySelector('#ddlPosition'));
        var Id = PositionDropDown.select2("data").id;
        changeDropDownColor('s2id_ddlPosition', Id);
    }
    function getMediaOrderProduction(mOrderId) {
        try {
            mediaOrderProductionFactory.getById(mOrderId, function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        $scope.buttonText = "Update";
                        $scope.production = result.data;
                        if ($scope.production.NewPickupInd == 0) {
                            $scope.dvNew = true;
                            $scope.dvPickup = false;
                        }
                        else if ($scope.production.NewPickupInd == 1) {
                            $scope.dvNew = false;
                            $scope.dvPickup = true;
                        }

                        $scope.production.BuyId = $stateParams.AdOrderId;
                        if ($scope.production.OriginalFile != null) {
                            var tempOriginal = $scope.production.OriginalFile;
                            var fileNameOriginal = tempOriginal.substring(tempOriginal.lastIndexOf('/')).split("/").pop();
                            var extensionOriginal = fileNameOriginal.substring(fileNameOriginal.lastIndexOf('.'));
                            $scope.production.OrigFileName = fileNameOriginal;
                        }
                        if ($scope.production.ProofFile != null) {

                            var tempProof = $scope.production.ProofFile;
                            var fileNameProof = tempProof.substring(tempProof.lastIndexOf('/')).split("/").pop();
                            var extensionProof = fileNameProof.substring(fileNameProof.lastIndexOf('.'));
                            $scope.production.ProofFileName = fileNameProof;
                        }
                        if ($scope.production.FinalFile != null) {
                            var tempFinal = $scope.production.FinalFile;
                            var fileNameFinal = tempFinal.substring(tempFinal.lastIndexOf('/')).split("/").pop();
                            var extensionFinal = fileNameFinal.substring(fileNameFinal.lastIndexOf('.'));
                            $scope.production.FinalFileName = fileNameFinal;
                        }
                        if (extensionOriginal == ".pdf") {
                            $scope.isPdfTrueForOriginal = true;
                            $scope.tempPdfPathForOriginal = $scope.production.OriginalFile;
                        }
                        else {
                            $scope.isPdfTrueForOriginal = false;
                        }
                        if (extensionProof == ".pdf") {
                            $scope.isPdfTrueForProof = true
                            $scope.tempPdfPathForProof = $scope.production.ProofFile;
                        }
                        else {
                            $scope.isPdfTrueForProof = false;
                        }
                        if (extensionFinal == ".pdf") {
                            $scope.isPdfTrueForfinal = true
                            $scope.tempPdfPathForfinal = $scope.production.FinalFile;
                        } else {
                            $scope.isPdfTrueForfinal = false;
                        }
                        if (result.data.MaterialContactId != null && result.data.MaterialContactId != "" && result.data.MaterialContactId != undefined) {
                            $scope.production.MaterialContactId = (result.data.MaterialContactId).toString();
                        }
                        $scope.production.MaterialExpectedDate = getDateInFormat(result.data.MaterialExpectedDate);
                        $scope.originalFileImageSrc = $scope.production.OriginalFile;
                        $scope.proofFileImageSrc = $scope.production.ProofFile;
                        $scope.finalFileImageSrc = $scope.production.FinalFile;

                        if ($scope.production.PickupMediaOrderId != null && $scope.production.PickupMediaOrderId != "" && $scope.production.PickupMediaOrderId != undefined) {
                            var formDropDown = angular.element(document.querySelector('#ddlFrom'));
                            formDropDown.select2('val', $scope.production.PickupMediaOrderId);
                            changeDropDownColor('s2id_ddlFrom', $scope.production.PickupMediaOrderId);
                            //var responseData = $scope.mediaAssets;
                            //var MediaAssets = alasql('SELECT MediaAssetIssueDate FROM ? AS add WHERE MediaOrderId  = ?', [responseData, parseInt($scope.production.PickupMediaOrderId)]);
                            //initSelect2('s2id_ddlSeparation', MediaAssets[0].MediaAssetIssueDate);
                        }
                        else {
                            changeDropDownColor('s2id_ddlFrom', "");
                        }
                        if ($scope.production.PositionId != null && $scope.production.PositionId != "" && $scope.production.PositionId != undefined) {
                            var positionDropDown = angular.element(document.querySelector('#ddlPosition'));
                            positionDropDown.select2('val', $scope.production.PositionId);
                            changeDropDownColor('s2id_ddlPosition', $scope.production.PositionId);
                            var responseData = $scope.positions;
                            var Positions = alasql('SELECT PositionName FROM ? AS add WHERE PositionId  = ?', [responseData, parseInt($scope.production.PositionId)]);
                            if (Positions != undefined && Positions != "" && Positions != null) {
                                initSelect2('s2id_ddlPosition', Positions[0].PositionName);
                            }

                        }
                        else {
                            changeDropDownColor('s2id_ddlPosition', "");
                        }
                        if ($scope.production.SeparationId != null && $scope.production.SeparationId != "" && $scope.production.SeparationId != undefined) {
                            var separationDropDown = angular.element(document.querySelector('#ddlSeparation'));
                            separationDropDown.select2('val', $scope.production.SeparationId);
                            changeDropDownColor('s2id_ddlSeparation', $scope.production.SeparationId);
                            var responseData = $scope.separations;
                            var Separations = alasql('SELECT SeparationName FROM ? AS add WHERE SeparationId  = ?', [responseData, parseInt($scope.production.SeparationId)]);
                            if (Separations != undefined && Separations != "" && Separations != null) {
                                initSelect2('s2id_ddlSeparation', Separations[0].SeparationName);
                            }
                        }
                        else {
                            changeDropDownColor('s2id_ddlSeparation', "");
                        }
                        if ($scope.production.MaterialContactId != null && $scope.production.MaterialContactId != "" && $scope.production.MaterialContactId != undefined) {
                            //var temp = 0;
                            //if ($scope.production.AdvertiserAgencyInd == true)
                            //{
                            //    temp = 1;
                            //}
                            //else if ($scope.production.AdvertiserAgencyInd == false) {
                            //    temp = 0;
                            //}
                            GetBillToContact($scope.production.AdvertiserAgencyInd);
                            var responseData = $scope.billingToContacts;
                            var contactDropDown = angular.element(document.querySelector('#ddlContact'));
                            contactDropDown.select2('val', $scope.production.MaterialContactId);
                            //setDroupDownValue('ddlContact', parseInt($scope.production.MaterialContactId));
                            changeDropDownColor('s2id_ddlContact', $scope.production.MaterialContactId);
                            var Contacts = alasql('SELECT PersonName FROM ? AS add WHERE Id  = ?', [responseData, $scope.production.MaterialContactId.toString()]);
                            if (Contacts != null && Contacts != "" && Contacts != undefined) {
                                initSelect2('s2id_ddlContact', Contacts[0].PersonName.FullName);
                            }
                        }
                        else {
                            initSelect2('s2id_ddlContact', "Select contact");
                            changeDropDownColor('s2id_ddlContact', "");
                        }
                        if ($scope.production.ProductionStatusId != null && $scope.production.ProductionStatusId != "" && $scope.production.ProductionStatusId != undefined) {
                            var statusDropDown = angular.element(document.querySelector('#ddlStatus'));
                            statusDropDown.select2('val', $scope.production.ProductionStatusId);
                            changeDropDownColor('s2id_ddlStatus', $scope.production.ProductionStatusId);
                            var responseData = $scope.productionStatus;
                            var ProductionStatus = alasql('SELECT ProductionStatusName FROM ? AS add WHERE ProductionStatusId  = ?', [responseData, parseInt($scope.production.ProductionStatusId)]);
                            if (ProductionStatus != undefined && ProductionStatus != "" && ProductionStatus != null) {
                                initSelect2('s2id_ddlStatus', ProductionStatus[0].ProductionStatusName);
                            }
                        }
                        else {
                            changeDropDownColor('s2id_ddlStatus', "");
                        }
                        initGrossandNetCost();
                    }
                    else {
                        $scope.buttonText = "Add";
                    }
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function changeDropDownColor(controlId, controlValue) {
        if (controlValue != "" && controlValue != undefined && controlValue != null) {
            // id would be dynamic generated by select2
            var DropDown = angular.element(document.querySelector('#' + controlId));
            var select2ChosenDiv = DropDown.find('.select2-chosen');
            select2ChosenDiv.css('color', '#323232');
        } else {
            var DropDown = angular.element(document.querySelector('#' + controlId));
            var select2ChosenDiv = DropDown.find('.select2-chosen');
            select2ChosenDiv.css('color', '#999999');
        }
    }

    $scope.setBillToContact = function (changeBillToValue) {
        try {
            GetBillToContact(changeBillToValue);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }
    function GetBillToContact(changeBillToValue) {
        try {
            var id = 0;
            var temp = changeBillToValue;
            if (temp == true) {
                changeBillToValue = 1;
            }
            else if (temp == false) {
                changeBillToValue = 0;
            }
            if (parseInt(changeBillToValue) == 0 || parseInt(changeBillToValue) == 1) {
                if (parseInt(changeBillToValue) == 1) {
                    initSelect2('s2id_ddlContact', "Select contact");
                    changeDropDownColor('s2id_ddlContact', "");
                    $scope.billingToContacts = [];
                    id = $scope.selectedAdvertiserId;
                } else if (parseInt(changeBillToValue) == 0) {
                    initSelect2('s2id_ddlContact', "Select contact");
                    changeDropDownColor('s2id_ddlContact', "");
                    $scope.billingToContacts = [];
                    id = $scope.selectedAgencyId;
                }
            }
            var partyData = $scope.partyAndOrganizationData;
            $scope.organizationWiseParty = [];
            if (id != undefined && parseInt(id) > 0) {
                angular.forEach(partyData, function (person, key) {
                    if (person.PrimaryOrganization != undefined) {
                        if (person.PrimaryOrganization.OrganizationPartyId != undefined) {
                            var organizationI = person.PrimaryOrganization.OrganizationPartyId;
                            if (id == organizationI) {
                                $scope.organizationWiseParty.push(person);
                            }
                        }
                    }
                });
            } else {
                $scope.organizationWiseParty = [];
            }
            $scope.billingToContacts = $scope.organizationWiseParty;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    //upload Image

    initImageViewerForOriginalFile();
    initImageViewerForProofFile();
    initImageViewerForFinalFile();

    function initImageViewerForOriginalFile() {
        try {
            var pictureClasssName = "docs-pictures-originalFile";
            var buttonClasssName = "docs-buttons-originalFile";
            showImageInViewerForOriginalFile(pictureClasssName, buttonClasssName);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function initImageViewerForProofFile() {
        try {
            var pictureClasssName = "docs-pictures-proofFile";
            var buttonClasssName = "docs-buttons-proofFile";
            showImageInViewerForProofFile(pictureClasssName, buttonClasssName);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function initImageViewerForFinalFile() {
        try {
            var pictureClasssName = "docs-pictures-finalFile";
            var buttonClasssName = "docs-buttons-finalFile";
            showImageInViewerForFinalFile(pictureClasssName, buttonClasssName);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.originalFileImageSrc = "";
    $scope.isPdfTrueForOriginal = false;
    $scope.tempPdfPathForOriginal = "";
    $scope.isImgTrueForOriginal = false;

    $scope.uploadImage = function (files) {
        try {
            var logoElement = angular.element(document.querySelector("#originalfile-upload"))[0];
            console.log();
            var uploadType = logoElement.files[0].type;
            if (uploadType != undefined && uploadType != null) {
                if (logoElement.files[0].type.indexOf("application/pdf") != -1) {
                    var tmppath = URL.createObjectURL(logoElement.files[0]);
                    $scope.tempPdfPathForOriginal = tmppath;
                    $scope.isPdfTrueForOriginal = true;
                    $scope.isImgTrueForOriginal = false;
                } else if (logoElement.files[0].type.indexOf("image") != -1) {
                    $scope.isImgTrueForOriginal = true;
                    $scope.isPdfTrueForOriginal = false;
                }
                else {
                    $scope.isImgTrueForOriginal = false;
                    $scope.isPdfTrueForOriginal = false;
                }
                if (logoElement.files && logoElement.files[0]) {
                    var FR = new FileReader();
                    FR.onload = function (e) {
                        var strImage = e.target.result.substring((e.target.result.indexOf(',') + 1));
                        $scope.$apply(function () {
                            if ($scope.isImgTrueForOriginal == false && $scope.isPdfTrueForOriginal == false) {
                                $scope.originalFileImageSrc = "";
                            }
                            else {
                                $scope.originalFileImageSrc = e.target.result;
                            }
                            $scope.production.OriginalFile = strImage;
                            $scope.production.OriginalFileExtension = "";
                            var temp = logoElement.files[0].name;
                            $scope.production.OriginalFileExtension = temp.substring(temp.lastIndexOf('.'));
                            $scope.production.OrigFileName = "";
                            $scope.production.OrigFileName = logoElement.files[0].name;
                        });
                    };
                    FR.readAsDataURL(logoElement.files[0]);
                }
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    }

    $scope.proofFileImageSrc = "";
    $scope.isPdfTrueForProof = false;
    $scope.tempPdfPathForProof = "";
    $scope.isImgTrueForProof = false;

    $scope.uploadImageforProof = function (files) {
        try {
            var logoElement = angular.element(document.querySelector("#prooffile-upload"))[0];
            console.log();
            var uploadType = logoElement.files[0].type;
            if (uploadType != undefined && uploadType != null) {
                if (logoElement.files[0].type.indexOf("application/pdf") != -1) {
                    var tmppath = URL.createObjectURL(logoElement.files[0]);
                    $scope.tempPdfPathForProof = tmppath;
                    $scope.isPdfTrueForProof = true;
                    $scope.isImgTrueForProof = false;
                } else if (logoElement.files[0].type.indexOf("image") != -1) {
                    $scope.isImgTrueForProof = true;
                    $scope.isPdfTrueForProof = false;
                }
                else {
                    $scope.isImgTrueForProof = false;
                    $scope.isPdfTrueForProof = false;
                }
                if (logoElement.files && logoElement.files[0]) {
                    var FR = new FileReader();
                    FR.onload = function (e) {
                        // var strImage = e.target.result.replace(/^data:image\/[a-z]+;base64,/, "");
                        var strImage = e.target.result.substring((e.target.result.indexOf(',') + 1));
                        $scope.$apply(function () {
                            if ($scope.isImgTrueForProof == false && $scope.isPdfTrueForProof == false) {
                                $scope.proofFileImageSrc = "";
                            }
                            else {
                                $scope.proofFileImageSrc = e.target.result;
                            }
                            $scope.production.ProofFile = strImage;
                            $scope.isPdfTrueForProof = false;
                            $scope.production.ProofFileExtension = "";
                            var temp = logoElement.files[0].name;
                            $scope.production.ProofFileExtension = temp.substring(temp.lastIndexOf('.'));
                            $scope.production.ProofFileName = "";
                            $scope.production.ProofFileName = logoElement.files[0].name;

                        });
                    };
                    FR.readAsDataURL(logoElement.files[0]);
                }
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.FinalFileImageSrc = "";
    $scope.isPdfTrueForfinal = false;
    $scope.tempPdfPathForfinal = "";
    $scope.isImgTrueForfinal = false;

    $scope.uploadImageforfinal = function (files) {
        try {
            var logoElement = angular.element(document.querySelector("#finalfile-upload"))[0];
            console.log();
            var uploadType = logoElement.files[0].type;
            if (uploadType != undefined && uploadType != null) {
                if (logoElement.files[0].type.indexOf("application/pdf") != -1) {
                    var tmppath = URL.createObjectURL(logoElement.files[0]);
                    $scope.tempPdfPathForProof = tmppath;
                    $scope.isPdfTrueForfinal = true;
                    $scope.isImgTrueForfinal = false;
                } else if (logoElement.files[0].type.indexOf("image") != -1) {
                    $scope.isImgTrueForfinal = true;
                    $scope.isPdfTrueForfinal = false;
                }
                else {
                    $scope.isImgTrueForfinal = false;
                    $scope.isPdfTrueForfinal = false;
                }
                if (logoElement.files && logoElement.files[0]) {
                    $scope.isPdfTrueForfinal = false;
                    var FR = new FileReader();
                    FR.onload = function (e) {
                        //var strImage = e.target.result.replace(/^data:image\/[a-z]+;base64,/, "");
                        var strImage = e.target.result.substring((e.target.result.indexOf(',') + 1));
                        $scope.$apply(function () {
                            if ($scope.isImgTrueForProof == false && $scope.isPdfTrueForProof == false) {
                                $scope.FinalFileImageSrc = "";
                            }
                            else {
                                $scope.FinalFileImageSrc = e.target.result;
                            }
                            $scope.production.FinalFile = strImage;
                            $scope.isPdfTrueForfinal = false;
                            $scope.production.FinalFileExtension = "";
                            var temp = logoElement.files[0].name;
                            $scope.production.FinalFileExtension = temp.substring(temp.lastIndexOf('.'));
                            $scope.production.FinalFileName = "";
                            $scope.production.FinalFileName = logoElement.files[0].name;

                        });
                    };
                    FR.readAsDataURL(logoElement.files[0]);
                }
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }


    function showImageInViewerForOriginalFile(pictureClasssName, buttonClasssName) {
        try {
            var $images = angular.element(document.querySelector('.' + pictureClasssName));
            var $buttons = angular.element(document.querySelector('.' + buttonClasssName));
            var options = {
                // inline: true,
                url: 'data-original',
                toolbar: false,
                tooltip: false,
                title: false,
                navbar: false,
                // different events
                ready: function (e) {
                },
                show: function (e) {
                },
                shown: function (e) {
                },
                hide: function (e) {
                },
                hidden: function (e) {
                },
                view: function (e) {
                },
                viewed: function (e) {
                }
            };

            function toggleButtons(mode) {
                if (/modal|inline|none/.test(mode)) {
                    $buttons
                      .find('button[data-enable]')
                        .prop('disabled', true)
                      .filter('[data-enable*="' + mode + '"]')
                        .prop('disabled', false);
                }
            }

            $images.on({

                ready: function (e) {
                },
                show: function (e) {
                },
                shown: function (e) {
                },
                hide: function (e) {
                },
                hidden: function (e) {
                },
                view: function (e) {
                },
                viewed: function (e) {
                }
            }).viewer(options);

            toggleButtons(options.inline ? 'inline' : 'modal');

            $buttons.on('click', 'button', function () {
                if ($scope.isPdfTrueForOriginal == true && $scope.originalFileImageSrc != "" && $scope.originalFileImageSrc != undefined && $scope.originalFileImageSrc != null) {
                    window.open($scope.tempPdfPathForOriginal, '_blank');
                    return;
                }
                else if ($scope.isImgTrueForOriginal == false && $scope.originalFileImageSrc != "" && $scope.originalFileImageSrc != undefined && $scope.originalFileImageSrc != null) {
                    window.open($scope.originalFileImageSrc, '_blank');
                    return;
                }

                var buttonElement = angular.element(this)[0];
                var buttonId = buttonElement.id;
                //if (buttonId == "originalImageButton") {
                //    var logoElement = angular.element(document.querySelector("#originalfile-upload"))[0];
                //    if (logoElement.files.length <= 0) {
                //        toastr.info('Please First Upload file ', 'Information!');
                //        return;
                //    }
                //} else if (buttonId == "proofImageButton") {
                //    var logoElement = angular.element(document.querySelector("#prooffile-upload"))[0];
                //    if (logoElement.files.length <= 0) {
                //        toastr.info('Please First Upload file ', 'Information!');
                //        return;
                //    }
                //} else if (buttonId == "finalImageButton") {
                //    var logoElement = angular.element(document.querySelector("#finalfile-upload"))[0];
                //    if (logoElement.files.length <= 0) {
                //        toastr.info('Please First Upload file ', 'Information!');
                //        return;
                //    }
                //}

                var data = angular.element(this).data();
                var args = data.arguments || [];
                if (data.method) {
                    if ($scope.originalFileImageSrc != "" && $scope.originalFileImageSrc != undefined && $scope.originalFileImageSrc != null) {
                        if (data.target) {
                            $images.viewer(data.method, $(data.target).val());
                        } else {
                            $images.viewer(data.method, args[0], args[1]);
                        }

                        switch (data.method) {
                            case 'scaleX':
                            case 'scaleY':
                                args[0] = -args[0];
                                break;

                            case 'destroy':
                                toggleButtons('none');
                                break;
                        }
                    }
                }
            });
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function showImageInViewerForProofFile(pictureClasssName, buttonClasssName) {
        try {
            var $images = angular.element(document.querySelector('.' + pictureClasssName));
            var $buttons = angular.element(document.querySelector('.' + buttonClasssName));
            var options = {
                // inline: true,
                url: 'data-original',
                toolbar: false,
                tooltip: false,
                title: false,
                navbar: false,
                ready: function (e) {
                },
                show: function (e) {
                },
                shown: function (e) {
                },
                hide: function (e) {
                },
                hidden: function (e) {
                },
                view: function (e) {
                },
                viewed: function (e) {
                    console.log(e.type);
                }
            };

            function toggleButtons(mode) {
                if (/modal|inline|none/.test(mode)) {
                    $buttons
                      .find('button[data-enable]')
                        .prop('disabled', true)
                      .filter('[data-enable*="' + mode + '"]')
                        .prop('disabled', false);
                }
            }

            $images.on({

                ready: function (e) {
                },
                show: function (e) {
                },
                shown: function (e) {
                },
                hide: function (e) {
                },
                hidden: function (e) {
                },
                view: function (e) {
                },
                viewed: function (e) {
                }
            }).viewer(options);

            toggleButtons(options.inline ? 'inline' : 'modal');

            $buttons.on('click', 'button', function () {

                if ($scope.isPdfTrueForProof == true && $scope.proofFileImageSrc != "" && $scope.proofFileImageSrc != undefined && $scope.proofFileImageSrc != null) {
                    window.open($scope.tempPdfPathForProof, '_blank');
                    return;
                }
                else if ($scope.isImgTrueForProof == false && $scope.proofFileImageSrc != "" && $scope.proofFileImageSrc != undefined && $scope.proofFileImageSrc != null) {
                    window.open($scope.proofFileImageSrc, '_blank');
                    return;
                }

                var buttonElement = angular.element(this)[0];
                var buttonId = buttonElement.id;
                //if (buttonId == "originalImageButton") {
                //    var logoElement = angular.element(document.querySelector("#originalfile-upload"))[0];
                //    if (logoElement.files.length <= 0) {
                //        toastr.info('Please First Upload file ', 'Information!');
                //        return;
                //    }
                //} else if (buttonId == "proofImageButton") {
                //    var logoElement = angular.element(document.querySelector("#prooffile-upload"))[0];
                //    if (logoElement.files.length <= 0) {
                //        toastr.info('Please First Upload file ', 'Information!');
                //        return;
                //    }
                //} else if (buttonId == "finalImageButton") {
                //    var logoElement = angular.element(document.querySelector("#finalfile-upload"))[0];
                //    if (logoElement.files.length <= 0) {
                //        toastr.info('Please First Upload file ', 'Information!');
                //        return;
                //    }
                //}

                var data = angular.element(this).data();
                var args = data.arguments || [];
                if (data.method) {
                    if ($scope.proofFileImageSrc != "" && $scope.proofFileImageSrc != undefined && $scope.proofFileImageSrc != null) {
                        if (data.target) {
                            $images.viewer(data.method, $(data.target).val());
                        } else {
                            $images.viewer(data.method, args[0], args[1]);
                        }

                        switch (data.method) {
                            case 'scaleX':
                            case 'scaleY':
                                args[0] = -args[0];
                                break;

                            case 'destroy':
                                toggleButtons('none');
                                break;
                        }
                    }
                }
            });
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function showImageInViewerForFinalFile(pictureClasssName, buttonClasssName) {
        try {
            var $images = angular.element(document.querySelector('.' + pictureClasssName));
            var $buttons = angular.element(document.querySelector('.' + buttonClasssName));
            var options = {
                // inline: true,
                url: 'data-original',
                toolbar: false,
                tooltip: false,
                title: false,
                navbar: false,
                ready: function (e) {
                },
                show: function (e) {
                },
                shown: function (e) {
                },
                hide: function (e) {
                },
                hidden: function (e) {
                },
                view: function (e) {
                },
                viewed: function (e) {
                }
            };

            function toggleButtons(mode) {
                if (/modal|inline|none/.test(mode)) {
                    $buttons
                      .find('button[data-enable]')
                        .prop('disabled', true)
                      .filter('[data-enable*="' + mode + '"]')
                        .prop('disabled', false);
                }
            }

            $images.on({

                ready: function (e) {
                },
                show: function (e) {
                },
                shown: function (e) {
                },
                hide: function (e) {
                },
                hidden: function (e) {
                },
                view: function (e) {
                },
                viewed: function (e) {
                }
            }).viewer(options);

            toggleButtons(options.inline ? 'inline' : 'modal');

            $buttons.on('click', 'button', function () {
                if ($scope.isPdfTrueForfinal == true && $scope.finalFileImageSrc != "" && $scope.finalFileImageSrc != undefined && $scope.finalFileImageSrc != null) {
                    window.open($scope.tempPdfPathForfinal, '_blank');
                    return;
                }
                else if ($scope.isImgTrueForfinal == false && $scope.finalFileImageSrc != "" && $scope.finalFileImageSrc != undefined && $scope.finalFileImageSrc != null) {
                    window.open($scope.finalFileImageSrc, '_blank');
                    return;
                }
                var buttonElement = angular.element(this)[0];
                var buttonId = buttonElement.id;
                //if (buttonId == "originalImageButton") {
                //    var logoElement = angular.element(document.querySelector("#originalfile-upload"))[0];
                //    if (logoElement.files.length <= 0) {
                //        toastr.info('Please First Upload file ', 'Information!');
                //        return;
                //    }
                //} else if (buttonId == "proofImageButton") {
                //    var logoElement = angular.element(document.querySelector("#prooffile-upload"))[0];
                //    if (logoElement.files.length <= 0) {
                //        toastr.info('Please First Upload file ', 'Information!');
                //        return;
                //    }
                //} else if (buttonId == "finalImageButton") {
                //    var logoElement = angular.element(document.querySelector("#finalfile-upload"))[0];
                //    if (logoElement.files.length <= 0) {
                //        toastr.info('Please First Upload file ', 'Information!');
                //        return;
                //    }
                //}

                var data = angular.element(this).data();
                var args = data.arguments || [];
                if (data.method) {
                    if ($scope.finalFileImageSrc != "" && $scope.finalFileImageSrc != undefined) {
                        if (data.target) {
                            $images.viewer(data.method, $(data.target).val());
                        } else {
                            $images.viewer(data.method, args[0], args[1]);
                        }

                        switch (data.method) {
                            case 'scaleX':
                            case 'scaleY':
                                args[0] = -args[0];
                                break;

                            case 'destroy':
                                toggleButtons('none');
                                break;
                        }
                    }
                }
            });
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    getNoOfIssueMediaOrder();
    getAllProductionStatus();
    getAllPosition();
    getAllSeparation();
    //getOrdersNotInBuyer($scope.$parent.tabOrderId);
    getAdvertiserThroughASI();
    commonInit();
})