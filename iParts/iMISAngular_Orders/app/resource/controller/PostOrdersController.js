﻿'use strict';

app.controller('PostOrdersController', function PostOrdersController($scope, $location, $rootScope, $compile, $stateParams, $exceptionHandler, adAdjustmentsFactory,
    mediaAssetFactory, AdTypeFactory, issueDateFactory, $filter, mediaOrderLinesFactory, ordersFactory, $timeout, mediaOrdersFactory, advertiserFactory, authKeyService, toastr) {

    // tab Setting
    $scope.$parent.setPostOrdersTabActive();
    var authKey = authKeyService.getConstantAuthKey();
    var baseUrl = authKey.baseUrl;
    var authToken = authKey.authToken;

    $scope.selectedPostOrder = [];
    $scope.selectedAllPostOrders = false;

    $scope.partyAndOrganizationData = [];
    $scope.advertisers = [];
    $scope.agencies = [];
    $scope.billingToContacts = [];

    $scope.PostOrder = {};
    $scope.PostOrders = [];
    $scope.MediaAssets = [];
    $scope.IssueDates = [];
    $scope.AdTypes = [];
    $scope.POadvertisers = [];

    //$scope.isShowDefaultTable = true;
    //$scope.PostOrder.chkIssueDate = "IssueDate";
    //$scope.isShowPagination = false;
    //$scope.dataLoading = false;

    $scope.PostOrder.chkIssueDate = "IssueDate";
    $scope.isShowDefaultTable = false;
    $scope.isShowPagination = false;
    $scope.dataLoading = true;
    $scope.isLoading = true;

    $scope.selectedMediaAsset = "";
    $scope.selectedIssueDate = "";
    $scope.selectedAdType = "";
    $scope.selectedAdvertiser = "";
    $scope.changeDropdown = 0;
    $scope.isShowInvoiceDate = false;

    $scope.mediaAssetSelect = {
        multiple: false,
        formatSearching: 'Searching the Media Assset...',
        formatNoMatches: 'No Media Assset found'
    };

    $scope.IssueDateSelect = {
        multiple: false,
        formatSearching: 'Searching the Issue Date...',
        formatNoMatches: 'No Issue Date found'
    };

    $scope.AdTypeSelect = {
        multiple: false,
        formatSearching: 'Searching the Ad Type...',
        formatNoMatches: 'No Ad Type found'
    };

    $scope.AdvertiserSelect = {
        multiple: false,
        formatSearching: 'Searching the Advertiser...',
        formatNoMatches: 'No Advertiser found'
    };

    function InitDateRange() {
        try {
            angular.element(document).ready(function () {
                console.log('page loading completed');
                var invoiceDateControl = angular.element(document.querySelector('#txtInvoiceDate'));
                invoiceDateControl.datepicker();
            });
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getAllMediaAssets() {
        try {
            mediaAssetFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            $scope.MediaAssets = result.data;
                        } else {
                            $scope.MediaAssets = [];
                        }
                    } else {
                        $scope.MediaAssets = [];
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.MediaAssets = [];
                }
                else {
                    $scope.MediaAssets = [];
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getIssueDatebyMediaAsset(Id) {
        try {
            issueDateFactory.getByMediaAsset(Id, function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            $scope.IssueDates = result.data[0].IssueDates;
                        } else {
                            $scope.IssueDates = [];
                        }
                    } else {
                        $scope.IssueDates = [];
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.IssueDates = [];
                }
                else {
                    $scope.IssueDates = [];
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getAllAdType() {
        try {
            AdTypeFactory.getMediaOrdersAdType(baseUrl, authToken, function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            $scope.AdTypes = result.data;
                        } else {
                            $scope.AdTypes = [];
                        }
                    } else {
                        $scope.AdTypes = [];
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.AdTypes = [];
                }
                else {
                    $scope.AdTypes = [];
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getAllIssueDates() {
        try {
            issueDateFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            $scope.IssueDates = result.data;
                        } else {
                            $scope.IssueDates = [];
                        }
                    } else {
                        $scope.IssueDates = [];
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.IssueDates = [];
                }
                else {
                    $scope.IssueDates = [];
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getAllAdvertiser() {
        try {
            if ($rootScope.mainPartyAndOrganizationData.length <= 0 && $rootScope.mainAdvertisers.length <= 0 && $rootScope.mainAgencies.length <= 0
                  && $rootScope.mainBillingToContacts.length <= 0) {
                getAllPartyDataThroughASI();
            } else {
                $scope.partyAndOrganizationData = $rootScope.mainPartyAndOrganizationData;
                $scope.advertisers = $rootScope.mainAdvertisers;
                $scope.agencies = $rootScope.mainAgencies;
                $scope.billingToContacts = $rootScope.mainBillingToContacts;
                $scope.POadvertisers = $scope.advertisers;
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getAllPartyDataThroughASI() {
        try {
            if ($rootScope.mainPartyAndOrganizationData.length <= 0 && $rootScope.mainAdvertisers.length <= 0 && $rootScope.mainAgencies.length <= 0
                && $rootScope.mainBillingToContacts.length <= 0) {
                var HasNext = true;
                var offset = 0;
                var advertisers = [];
                var contacts = [];
                function getPartyThroughASI() {
                    advertiserFactory.getAllAdvertiser1(baseUrl, authToken, offset, function (result) {
                        if (result != null && result != undefined && result != "") {
                            var ItemData = result.Items.$values
                            console.log(ItemData);
                            angular.forEach(ItemData, function (itemdatavalue, key) {
                                $scope.partyAndOrganizationData.push(itemdatavalue);
                                var type = itemdatavalue.$type;
                                if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                                    advertisers.push(itemdatavalue);
                                }
                                else if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                                    contacts.push(itemdatavalue);
                                }
                            });
                            $scope.advertisers = advertisers;
                            $scope.agencies = advertisers;
                            $scope.billingToContacts = contacts;
                            var TotalCount = result.TotalCount;
                            HasNext = result.HasNext;
                            offset = result.Offset;
                            var Limit = result.Limit;
                            var NextOffset = result.NextOffset;
                            if (HasNext == true) {
                                offset = NextOffset;
                                getPartyThroughASI();
                            } else {
                                $rootScope.mainPartyAndOrganizationData = $scope.partyAndOrganizationData;
                                $rootScope.mainAdvertisers = $scope.advertisers;
                                $rootScope.mainAgencies = $scope.agencies;
                                $rootScope.mainBillingToContacts = $scope.billingToContacts;
                                //$scope.POadvertisers = $scope.advertisers;
                                getAllAdvertisers();
                                $scope.searchOrdersbyMediaAsset($scope.PostOrder);
                            }
                        }
                        else {
                            $scope.partyAndOrganizationData = [];
                            $scope.advertisers = [];
                            $scope.agencies = [];
                            $scope.billingToContacts = [];
                            $scope.POadvertisers = [];
                        }
                    })
                }
                getPartyThroughASI();
            } else {
                $scope.partyAndOrganizationData = $rootScope.mainPartyAndOrganizationData;
                $scope.advertisers = $rootScope.mainAdvertisers;
                $scope.agencies = $rootScope.mainAgencies;
                $scope.billingToContacts = $rootScope.mainBillingToContacts;
                getAllAdvertisers();
                $scope.searchOrdersbyMediaAsset($scope.PostOrder);
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function createListforPostOrder(objOrders) {
        try {
            if (objOrders != null && objOrders != undefined && objOrders != "" && objOrders.length > 0) {
                if (objOrders.length > 0) {
                    if ($rootScope.mainPartyAndOrganizationData.length > 0) {
                        $scope.partyAndOrganizationData = $rootScope.mainPartyAndOrganizationData;
                        $scope.PostOrders = [];
                        angular.forEach(objOrders, function (data, key) {
                            var AdvertiserId = data.AdvertiserId;
                            var ST_ID = data.ST_ID;
                            var objAdvertiser = alasql('SELECT * FROM ? AS add WHERE Id = ?', [$scope.partyAndOrganizationData, AdvertiserId]);
                            var objContact = alasql('SELECT * FROM ? AS add WHERE Id = ?', [$scope.partyAndOrganizationData, ST_ID]);
                            if (objAdvertiser.length > 0) {
                                data.AdvertiserName = objAdvertiser[0].Name;
                            } else {
                                data.AdvertiserName = "";
                            }
                            if (objContact.length > 0) {
                                data.billToContactName = objContact[0].Name;
                            } else {
                                data.billToContactName = "";
                            }
                            $scope.PostOrders.push(data);
                        });
                        $scope.PostOrders = alasql('SELECT * FROM ? AS add ORDER BY AdvertiserName ASC', [$scope.PostOrders])
                        initPagination();
                        $scope.isShowDefaultTable = false;
                        $scope.dataLoading = false;
                        $scope.isShowPagination = true;

                    } else {
                        $scope.PostOrders = [];
                        $scope.isShowDefaultTable = true;
                        $scope.isShowPagination = false;
                        $scope.dataLoading = false;
                        getAllPartyDataThroughASI();
                    }
                } else {
                    $scope.PostOrders = [];
                    initPagination();
                    $scope.isShowDefaultTable = true;
                    $scope.isShowPagination = false;
                    $scope.dataLoading = false;
                }
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getAllAdvertisers() {
        try {
            mediaOrdersFactory.getMediaOrdersAdvertiser(baseUrl, authToken, function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            var data = result.data;
                            setAdvertiserList(data);
                        } else {
                            $scope.POadvertisers = [];
                        }
                    } else {
                        $scope.POadvertisers = [];
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.POadvertisers = [];
                }
                else {
                    $scope.POadvertisers = [];
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function setAdvertiserList(objAdvertisers) {
        try {
            if (objAdvertisers.length > 0) {
                if ($rootScope.mainAdvertisers.length > 0) {
                    $scope.advertisers = $rootScope.mainAdvertisers;
                    $scope.POadvertisers = [];
                    angular.forEach(objAdvertisers, function (data, key) {
                        var AdvertiserId = data.AdvertiserId;
                        var objAdvertiser = alasql('SELECT * FROM ? AS add WHERE Id = ?', [$scope.advertisers, AdvertiserId]);
                        $scope.POadvertisers.push(objAdvertiser[0]);
                    });
                    if ($scope.POadvertisers.length > 0) {
                        var advertiser = parseInt($scope.selectedAdvertiser);
                        if (advertiser > 0) {
                            $scope.selectedAdvertiser = "";
                        }
                        if ($scope.selectedAdvertiser != "" && $scope.selectedAdvertiser != undefined && $scope.selectedAdvertiser != 0) {
                            $scope.PostOrder.AdvertiserId = $scope.selectedAdvertiser;
                            setDroupDownValue('ddlAdvertiser', ($scope.PostOrder.AdvertiserId));
                            changeSelect2DropDownColor("s2id_ddlAdvertiser", $scope.PostOrder.AdvertiserId);
                        } else {
                            $scope.PostOrder.AdvertiserId = "";
                            setDroupDownValue('ddlAdvertiser', "");
                            changeSelect2DropDownColor("s2id_ddlAdvertiser", $scope.PostOrder.AdvertiserId);
                        }
                    }
                    //else {
                    //    $scope.PostOrder.AdvertiserId = $scope.selectedAdvertiser;
                    //    setDroupDownValue('ddlAdvertiser', ($scope.POadvertisers.Id));
                    //    //$scope.POadvertisers = [];
                    //}
                } else {
                    $scope.POadvertisers = [];
                    getAllPartyDataThroughASI();
                }
            } else {
                $scope.POadvertisers = [];
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getDataOnChangeofDropdown() {
        try {
            var data = $scope.PostOrder;
            mediaOrdersFactory.getDataForPostOrdersDropDown(data, baseUrl, authToken, function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            var apiData = result.data;
                            var mAssets = apiData[0].MediaAssets
                            var iDates = apiData[0].IssueDates
                            var Advertisers = apiData[0].Advertisers
                            var aTypes = apiData[0].AdTypes

                            // set Media Asset Dropdown
                            if (mAssets.length > 0) {
                                //$scope.MediaAssets = mAssets;
                                if ($scope.selectedMediaAsset != "" && $scope.selectedMediaAsset != undefined && $scope.selectedMediaAsset != 0) {
                                    $scope.PostOrder.MediaAssetId = $scope.selectedMediaAsset;
                                    setDroupDownValue('ddlMediaAsset', ($scope.PostOrder.MediaAssetId));
                                    changeSelect2DropDownColor("s2id_ddlMediaAsset", $scope.PostOrder.MediaAssetId);
                                } else {
                                    $scope.PostOrder.MediaAssetId = "";
                                    setDroupDownValue('ddlMediaAsset', "");
                                    changeSelect2DropDownColor("s2id_ddlMediaAsset", $scope.PostOrder.MediaAssetId);
                                }
                            } else {
                                $scope.PostOrder.MediaAssetId = $scope.selectedMediaAsset;
                                setDroupDownValue('ddlMediaAsset', ($scope.PostOrder.MediaAssetId));
                                changeSelect2DropDownColor("s2id_ddlMediaAsset", $scope.PostOrder.MediaAssetId);
                                // $scope.MediaAssets = [];
                            }

                            // set Issue Date Dropdown
                            if ($scope.changeDropdown == 0) {
                                if (iDates.length > 0) {
                                    $scope.IssueDates = iDates;
                                    var issueDate = parseInt($scope.selectedIssueDate);
                                    if (issueDate > 0) {
                                        $scope.selectedIssueDate = "";
                                    }

                                    if ($scope.selectedIssueDate != "" && $scope.selectedIssueDate != undefined && $scope.selectedIssueDate != 0) {
                                        $scope.PostOrder.IssueDateId = $scope.selectedIssueDate;
                                        setDroupDownValue('ddlIssueDate', ($scope.IssueDates.IssueDateId));
                                        changeSelect2DropDownColor("s2id_ddlIssueDate", $scope.IssueDates.IssueDateId);

                                    } else {
                                        $scope.PostOrder.IssueDateId = "";
                                        setDroupDownValue('ddlIssueDate', "");
                                        changeSelect2DropDownColor("s2id_ddlIssueDate", $scope.IssueDates.IssueDateId);
                                    }
                                } else {
                                    $scope.IssueDates = [];
                                    $scope.PostOrder.IssueDateId = "";
                                    // $scope.PostOrder.IssueDateId = $scope.selectedIssueDate;
                                    setDroupDownValue('ddlIssueDate', ($scope.IssueDates.IssueDateId));
                                    changeSelect2DropDownColor("s2id_ddlIssueDate", $scope.IssueDates.IssueDateId);
                                }
                            }

                            // set AdType Dropdown
                            if ($scope.changeDropdown == 0 || $scope.changeDropdown == 1) {
                                if (aTypes.length > 0) {
                                    $scope.AdTypes = aTypes;
                                    var adtype = parseInt($scope.selectedAdType);
                                    if (adtype > 0) {
                                        $scope.selectedAdType = "";
                                    }
                                    if ($scope.selectedAdType != "" && $scope.selectedAdType != undefined && $scope.selectedAdType != 0) {
                                        $scope.PostOrder.AdTypeId = $scope.selectedAdType;
                                        setDroupDownValue('ddlAdType', ($scope.AdTypes.AdTypeId));
                                        changeSelect2DropDownColor("s2id_ddlAdType", $scope.AdTypes.AdTypeId);

                                    } else {
                                        $scope.PostOrder.AdTypeId = "";
                                        setDroupDownValue('ddlAdType', "");
                                        changeSelect2DropDownColor("s2id_ddlAdType", $scope.AdTypes.AdTypeId);
                                    }
                                } else {
                                    $scope.AdTypes = [];
                                    $scope.PostOrder.AdTypeId = "";
                                    // $scope.PostOrder.AdTypeId = $scope.selectedAdType;
                                    setDroupDownValue('ddlAdType', ($scope.AdTypes.AdTypeId));
                                    changeSelect2DropDownColor("s2id_ddlAdType", $scope.AdTypes.AdTypeId);
                                }
                            }

                            // set Advertiser Dropdown
                            if ($scope.changeDropdown == 0 || $scope.changeDropdown == 1 || $scope.changeDropdown == 2) {
                                if (Advertisers.length > 0) {
                                    setAdvertiserList(Advertisers);
                                } else {
                                    $scope.POadvertisers = [];
                                    //$scope.PostOrder.AdvertiserId = $scope.selectedAdvertiser;
                                    $scope.PostOrder.AdvertiserId = "";
                                    setDroupDownValue('ddlAdvertiser', "");
                                    changeSelect2DropDownColor("s2id_ddlAdvertiser", "");
                                }
                            }

                        }
                    }
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function initPagination() {
        try {
            //pagination
            var sortingOrder = '';
            $scope.sortingOrder = sortingOrder;
            $scope.reverse = false;
            $scope.filteredItems = [];
            $scope.groupedItems = [];
            $scope.itemsPerPage = 10;
            $scope.pagedItems = [];
            $scope.currentPage = 0;
            if ($scope.PostOrders.length > 0) {
                $scope.items = $scope.PostOrders;
            } else {
                $scope.items = [];
            }

            var searchMatch = function (haystack, needle) {
                try {
                    if (!needle) {
                        return true;
                    }
                    return haystack.toString().toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                    // return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            // init the filtered items
            $scope.search = function () {
                try {
                    $scope.filteredItems = $filter('filter')($scope.items, function (item) {
                        for (var attr in item) {
                            if (searchMatch(item[attr], $scope.query))
                                return true;
                        }
                        return false;
                    });
                    // take care of the sorting order
                    if ($scope.sortingOrder !== '') {
                        $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                    }
                    $scope.currentPage = 0;
                    // now group by pages
                    $scope.groupToPages();
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            // calculate page in place
            $scope.groupToPages = function () {
                try {
                    $scope.pagedItems = [];

                    for (var i = 0; i < $scope.filteredItems.length; i++) {
                        if (i % $scope.itemsPerPage === 0) {
                            $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [$scope.filteredItems[i]];
                        } else {
                            $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                        }
                    }
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            $scope.range = function (start, end) {
                try {
                    var ret = [];
                    if (!end) {
                        end = start;
                        start = 0;
                    }
                    for (var i = start; i < end; i++) {
                        ret.push(i);
                    }
                    return ret;
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            $scope.prevPage = function () {
                $scope.selectedPostOrder = [];
                $scope.selectedAllPostOrders = false;

                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.nextPage = function () {
                $scope.selectedPostOrder = [];
                $scope.selectedAllPostOrders = false;
                if ($scope.currentPage < $scope.pagedItems.length - 1) {
                    $scope.currentPage++;
                }
            };

            $scope.setPage = function () {
                $scope.selectedPostOrder = [];
                $scope.selectedAllPostOrders = false;
                $scope.currentPage = this.n;
            };

            // functions have been describe process the data for display
            $scope.search();
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onChangeMediaAsset = function () {
        try {
            var DropDown = angular.element(document.querySelector('#ddlMediaAsset'));
            var getSelectedData = DropDown.select2("data");
            if (getSelectedData != null && getSelectedData != undefined) {
                var Id = getSelectedData.id;
                var Value = getSelectedData.text;
                $scope.selectedMediaAsset = Id;
                $scope.changeDropdown = 0;
                if (Id != undefined && Id != "" && Id != 0 && Id != null) {
                    changeSelect2DropDownColor("s2id_ddlMediaAsset", Id);
                    //getIssueDatebyMediaAsset(Id);
                    $scope.PostOrder.IssueDateId = "";
                    $scope.PostOrder.AdTypeId = "";
                    $scope.PostOrder.AdvertiserId = "";
                    getDataOnChangeofDropdown();
                } else if (Id == "") {
                    $scope.PostOrder.MediaAssetId = "";
                    $scope.PostOrder.IssueDateId = "";
                    $scope.PostOrder.AdTypeId = "";
                    $scope.PostOrder.AdvertiserId = "";
                    changeSelect2DropDownColor("s2id_ddlMediaAsset", "");
                    changeSelect2DropDownColor("s2id_ddlIssueDate", "");
                    changeSelect2DropDownColor("s2id_ddlAdType", "");
                    changeSelect2DropDownColor("s2id_ddlAdvertiser", "");
                    setDroupDownValue('ddlMediaAsset', "");
                    setDroupDownValue('ddlIssueDate', "");
                    setDroupDownValue('ddlAdType', "");
                    setDroupDownValue('ddlAdvertiser', "");
                    getAllIssueDates();
                    getAllAdType();
                    getAllAdvertisers();
                } else {
                    changeSelect2DropDownColor("s2id_ddlMediaAsset", "");
                }
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onChangeIssueDate = function () {
        try {
            var DropDown = angular.element(document.querySelector('#ddlIssueDate'));
            var getSelectedData = DropDown.select2("data");
            if (getSelectedData != null && getSelectedData != undefined) {
                var Id = getSelectedData.id;
                var Value = getSelectedData.text;
                $scope.selectedIssueDate = Id;
                $scope.changeDropdown = 1;
                if (Id != undefined && Id != "" && Id != 0 && Id != null) {
                    changeSelect2DropDownColor("s2id_ddlIssueDate", Id);
                    $scope.PostOrder.AdTypeId = "";
                    $scope.PostOrder.AdvertiserId = "";
                    getDataOnChangeofDropdown();
                } else {
                    changeSelect2DropDownColor("s2id_ddlIssueDate", "");
                }
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onChangeAdType = function () {
        try {
            var DropDown = angular.element(document.querySelector('#ddlAdType'));
            var getSelectedData = DropDown.select2("data");
            if (getSelectedData != null && getSelectedData != undefined) {
                var Id = getSelectedData.id;
                var Value = getSelectedData.text;
                $scope.selectedAdType = Id;
                $scope.changeDropdown = 2;
                if (Id != undefined && Id != "" && Id != 0 && Id != null) {
                    changeSelect2DropDownColor("s2id_ddlAdType", Id);
                    $scope.PostOrder.AdvertiserId = "";
                    getDataOnChangeofDropdown();
                    //getAllAdvertiser();
                } else {
                    changeSelect2DropDownColor("s2id_ddlAdType", "");
                }
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onChangeAdvertiser = function () {
        try {
            var DropDown = angular.element(document.querySelector('#ddlAdvertiser'));
            var getSelectedData = DropDown.select2("data");
            if (getSelectedData != null && getSelectedData != undefined) {
                var Id = getSelectedData.id;
                var Value = getSelectedData.text;
                $scope.selectedAdvertiser = Id;
                $scope.changeDropdown = 3;
                if (Id != undefined && Id != "" && Id != 0 && Id != null) {
                    changeSelect2DropDownColor("s2id_ddlAdvertiser", Id);
                    getDataOnChangeofDropdown();
                } else {
                    changeSelect2DropDownColor("s2id_ddlAdvertiser", "");
                }
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.searchOrdersbyMediaAsset = function (objPostOrder) {
        try {
            $scope.isShowDefaultTable = false;
            $scope.isShowPagination = false;
            // $scope.dataLoading = true;

            var data = objPostOrder;
            mediaOrdersFactory.getAllOrdersMediaAssetIdWise(data, function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            // $scope.isShowDefaultTable = false;
                            var data = result.data;
                            // $scope.PostOrders = result.data;
                            createListforPostOrder(data);

                        } else {
                            $scope.PostOrders = [];
                            $scope.isShowDefaultTable = true;
                            $scope.isShowPagination = false;
                            $scope.dataLoading = false;
                        }
                    } else {
                        $scope.PostOrders = [];
                        $scope.isShowDefaultTable = true;
                        $scope.isShowPagination = false;
                        $scope.dataLoading = false;
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.PostOrders = [];
                    $scope.isShowDefaultTable = true;
                    $scope.isShowPagination = false;
                    $scope.dataLoading = false;
                }
                else {
                    $scope.PostOrders = [];
                    $scope.isShowDefaultTable = true;
                    $scope.isShowPagination = false;
                    $scope.dataLoading = false;
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.setInvoiceDateShowOrHide = function (IsHideShow) {
        try {
            if (IsHideShow == true) {
                $scope.isShowInvoiceDate = true;
                InitDateRange();
            } else {
                $scope.PostOrder.InvoiceDate = "";
                $scope.isShowInvoiceDate = false;
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.toggleAll = function () {
        try {
            if ($scope.selectedAllPostOrders == false) {
                $scope.selectedAllPostOrders = false;
                var DropDown = angular.element(document.querySelector('#postOrderTable'));
                var allPostOrderTableTd = DropDown.find('tr');

                angular.forEach(allPostOrderTableTd, function (v, k) {
                    if (k != 0 && k != 11) {
                        var inputElement = v.firstElementChild.firstElementChild;
                        var inputElementId = inputElement.id;
                        $scope.selectedPostOrder[inputElementId] = false
                    }
                });

            } else {
                $scope.selectedAllPostOrders = true;
                var DropDown = angular.element(document.querySelector('#postOrderTable'));
                var allPostOrderTableTd = DropDown.find('tr');

                angular.forEach(allPostOrderTableTd, function (v, k) {
                    if (k != 0 && k != 11) {
                        var inputElement = v.firstElementChild.firstElementChild;
                        var inputElementId = inputElement.id;
                        $scope.selectedPostOrder[inputElementId] = true
                    }
                });
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    }

    $scope.PostOrderCheckboxChange = function (objMediaOrderId) {
        try {
            var isTrue = $scope.selectedPostOrder[objMediaOrderId];
            if (isTrue == true) {
                $scope.selectedPostOrder[objMediaOrderId] = true;
            } else {
                $scope.selectedPostOrder[objMediaOrderId] = false;
            }
            var cnt = 0;
            angular.forEach($scope.selectedPostOrder, function (data, key) {
                cnt++;
                if (cnt == parseInt($scope.itemsPerPage)) {
                    if ($scope.selectedPostOrder.indexOf(false) >= 0) {
                        $scope.selectedAllPostOrders = false;
                    } else {
                        $scope.selectedAllPostOrders = true;
                    }
                } else {
                    $scope.selectedAllPostOrders = false;
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    }

    getAllMediaAssets();
    getAllIssueDates();
    getAllAdType();
    getAllPartyDataThroughASI();
    getAllAdvertisers();
    initPagination();

})