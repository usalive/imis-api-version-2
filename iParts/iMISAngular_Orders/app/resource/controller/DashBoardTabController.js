﻿'use strict';

app.controller('DashBoardTabController', function DashBoardTabController($scope, $location, $rootScope, $stateParams, toastr) {
    console.log('called');

    $scope.setAdvertisersTabActive = function () {
        $scope.AdvertisersTab = true;
        $scope.OrdersTab = false;
        $scope.PostOrdersTab = false;
        $scope.MissingMaterialsTab = false;

        $scope.AdvertisersDisable = false;
        $scope.OrdersDisable = false;
        $scope.PostOrdersDisable = false;
        $scope.MissingMaterialsDisable = false;
    }

    $scope.setOrdersTabActive = function () {
        $scope.AdvertisersTab = false;
        $scope.OrdersTab = true;
        $scope.PostOrdersTab = false;
        $scope.MissingMaterialsTab = false;

        $scope.AdvertisersDisable = false;
        $scope.OrdersDisable = false;
        $scope.PostOrdersDisable = false;
        $scope.MissingMaterialsDisable = false;
    }

    $scope.setPostOrdersTabActive = function () {
        $scope.AdvertisersTab = false;
        $scope.OrdersTab = false;
        $scope.PostOrdersTab = true;
        $scope.MissingMaterialsTab = false;

        $scope.AdvertisersDisable = false;
        $scope.OrdersDisable = false;
        $scope.PostOrdersDisable = false;
        $scope.MissingMaterialsDisable = false;
    }

    $scope.setMissingMaterialsTabActive = function () {
        $scope.AdvertisersTab = false;
        $scope.OrdersTab = false;
        $scope.PostOrdersTab = false;
        $scope.MissingMaterialsTab = true;

        $scope.AdvertisersDisable = false;
        $scope.OrdersDisable = false;
        $scope.PostOrdersDisable = false;
        $scope.MissingMaterialsDisable = false;
    }

    $scope.setAdvertisersTabActive();

    $scope.redirectToMediaOrder = function () {
        $rootScope.fromDashboard = false;
        $location.path('ordersDetails/orders/');
    }

})