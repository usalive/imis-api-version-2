﻿'use strict';

app.controller('MissingMaterialsController', function MissingMaterialsController($scope, $location, $rootScope, $compile, $stateParams, $exceptionHandler, adAdjustmentsFactory,
    mediaAssetFactory, AdTypeFactory, issueDateFactory, $filter, mediaOrderLinesFactory, ordersFactory, $timeout, mediaOrdersFactory, advertiserFactory, authKeyService, toastr) {

    // tab Setting
    $scope.$parent.setMissingMaterialsTabActive();
    var authKey = authKeyService.getConstantAuthKey();
    var baseUrl = authKey.baseUrl;
    var authToken = authKey.authToken;

    $scope.partyAndOrganizationData = [];
    $scope.advertisers = [];
    $scope.agencies = [];
    $scope.billingToContacts = [];

    $scope.MissingMaterial = {};
    $scope.MissingMaterial.MediaAssetId = "";
    $scope.MissingMaterial.IssueDateId = "";
    $scope.MissingMaterials = [];
    $scope.MediaAssets = [];
    $scope.IssueDates = [];
    $scope.AdTypes = [];
    $scope.POadvertisers = [];

    //$scope.isShowDefaultTable = true;
    //$scope.MissingMaterial.chkIssueDate = "IssueDate";
    //$scope.isShowPagination = false;
    //$scope.dataLoading = false;

    $scope.MissingMaterial.chkIssueDate = "IssueDate";
    $scope.isShowDefaultTable = false;
    $scope.isShowPagination = false;
    $scope.dataLoading = true;
    $scope.isLoading = true;

    $scope.selectedMediaAsset = "";
    $scope.selectedIssueDate = "";
    $scope.selectedAdType = "";
    $scope.selectedAdvertiser = "";
    $scope.changeDropdown = 0;
    $scope.isShowInvoiceDate = false;
    var postData = {};

    $scope.mediaAssetSelect = {
        multiple: false,
        formatSearching: 'Searching the Media Assset...',
        formatNoMatches: 'No Media Assset found'
    };

    $scope.IssueDateSelect = {
        multiple: false,
        formatSearching: 'Searching the Issue Date...',
        formatNoMatches: 'No Issue Date found'
    };

    function InitDateRange() {
        try {
            angular.element(document).ready(function () {
                console.log('page loading completed');
                var invoiceDateControl = angular.element(document.querySelector('#txtInvoiceDate'));
                invoiceDateControl.datepicker();
            });
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getAllMediaAssets() {
        try {
            mediaAssetFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            $scope.MediaAssets = result.data;
                        } else {
                            $scope.MediaAssets = [];
                        }
                    } else {
                        $scope.MediaAssets = [];
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.MediaAssets = [];
                }
                else {
                    $scope.MediaAssets = [];
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getIssueDatebyMediaAsset(Id) {
        try {
            issueDateFactory.getByMediaAsset(Id, function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            $scope.IssueDates = result.data[0].IssueDates;
                        } else {
                            $scope.IssueDates = [];
                        }
                    } else {
                        $scope.IssueDates = [];
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.IssueDates = [];
                }
                else {
                    $scope.IssueDates = [];
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }


    function getAllIssueDates() {
        try {
            issueDateFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            $scope.IssueDates = result.data;
                        } else {
                            $scope.IssueDates = [];
                        }
                    } else {
                        $scope.IssueDates = [];
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.IssueDates = [];
                }
                else {
                    $scope.IssueDates = [];
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    //function getAllAdvertiser() {
    //    try {
    //        if ($rootScope.mainPartyAndOrganizationData.length <= 0 && $rootScope.mainAdvertisers.length <= 0 && $rootScope.mainAgencies.length <= 0
    //              && $rootScope.mainBillingToContacts.length <= 0) {
    //            getAllPartyDataThroughASI();
    //        } else {
    //            $scope.partyAndOrganizationData = $rootScope.mainPartyAndOrganizationData;
    //            $scope.advertisers = $rootScope.mainAdvertisers;
    //            $scope.agencies = $rootScope.mainAgencies;
    //            $scope.billingToContacts = $rootScope.mainBillingToContacts;
    //            $scope.POadvertisers = $scope.advertisers;
    //        }
    //    }
    //   catch (e) { $scope.isLoading = false; 
    //        $exceptionHandler(e);
    //    }
    //}

    function getAllPartyDataThroughASI() {
        try {
            if ($rootScope.mainPartyAndOrganizationData.length <= 0 && $rootScope.mainAdvertisers.length <= 0 && $rootScope.mainAgencies.length <= 0
                && $rootScope.mainBillingToContacts.length <= 0) {
                var HasNext = true;
                var offset = 0;
                var advertisers = [];
                var contacts = [];
                function getPartyThroughASI() {
                    advertiserFactory.getAllAdvertiser1(baseUrl, authToken, offset, function (result) {
                        if (result != null && result != undefined && result != "") {
                            var ItemData = result.Items.$values
                            console.log(ItemData);
                            angular.forEach(ItemData, function (itemdatavalue, key) {
                                $scope.partyAndOrganizationData.push(itemdatavalue);
                                var type = itemdatavalue.$type;
                                if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                                    advertisers.push(itemdatavalue);
                                }
                                else if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                                    contacts.push(itemdatavalue);
                                }
                            });
                            $scope.advertisers = advertisers;
                            $scope.agencies = advertisers;
                            $scope.billingToContacts = contacts;
                            var TotalCount = result.TotalCount;
                            HasNext = result.HasNext;
                            offset = result.Offset;
                            var Limit = result.Limit;
                            var NextOffset = result.NextOffset;
                            if (HasNext == true) {
                                offset = NextOffset;
                                getPartyThroughASI();
                            } else {
                                $rootScope.mainPartyAndOrganizationData = $scope.partyAndOrganizationData;
                                $rootScope.mainAdvertisers = $scope.advertisers;
                                $rootScope.mainAgencies = $scope.agencies;
                                $rootScope.mainBillingToContacts = $scope.billingToContacts;
                                //$scope.POadvertisers = $scope.advertisers;
                                getAllAdvertisers();
                                $scope.searchOrdersbyMediaAsset();
                            }
                        }
                        else {
                            $scope.partyAndOrganizationData = [];
                            $scope.advertisers = [];
                            $scope.agencies = [];
                            $scope.billingToContacts = [];
                            $scope.POadvertisers = [];
                        }
                    })
                }
                getPartyThroughASI();
            } else {
                $scope.partyAndOrganizationData = $rootScope.mainPartyAndOrganizationData;
                $scope.advertisers = $rootScope.mainAdvertisers;
                $scope.agencies = $rootScope.mainAgencies;
                $scope.billingToContacts = $rootScope.mainBillingToContacts;
                getAllAdvertisers();
                $scope.searchOrdersbyMediaAsset();
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function createListforMissingMaterial(objOrders) {
        try {
            if (objOrders != null && objOrders != undefined && objOrders != "" && objOrders.length > 0) {
                if (objOrders.length > 0) {
                    if ($rootScope.mainPartyAndOrganizationData.length > 0) {
                        $scope.partyAndOrganizationData = $rootScope.mainPartyAndOrganizationData;
                        $scope.MissingMaterials = [];
                        angular.forEach(objOrders, function (data, key) {
                            var AdvertiserId = data.AdvertiserId;
                            var ST_ID = data.ST_ID;
                            var objAdvertiser = alasql('SELECT * FROM ? AS add WHERE Id = ?', [$scope.partyAndOrganizationData, AdvertiserId]);
                            var objContact = alasql('SELECT * FROM ? AS add WHERE Id = ?', [$scope.partyAndOrganizationData, ST_ID]);
                            if (objAdvertiser.length > 0) {
                                data.AdvertiserName = objAdvertiser[0].Name;
                            } else {
                                data.AdvertiserName = "";
                            }
                            if (objContact.length > 0) {
                                data.billToContactName = objContact[0].Name;
                            } else {
                                data.billToContactName = "";
                            }
                            $scope.MissingMaterials.push(data);
                        });
                        $scope.MissingMaterials = alasql('SELECT * FROM ? AS add ORDER BY AdvertiserName ASC', [$scope.MissingMaterials])
                        initPagination();
                        $scope.isShowDefaultTable = false;
                        $scope.dataLoading = false;
                        $scope.isShowPagination = true;

                    } else {
                        $scope.MissingMaterials = [];
                        initPagination();
                        $scope.isShowDefaultTable = true;
                        $scope.isShowPagination = false;
                        $scope.dataLoading = false;
                        getAllPartyDataThroughASI();
                    }
                } else {
                    $scope.MissingMaterials = [];
                    initPagination();
                    $scope.isShowDefaultTable = true;
                    $scope.isShowPagination = false;
                    $scope.dataLoading = false;
                }
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getAllAdvertisers() {
        try {
            mediaOrdersFactory.getMediaOrdersAdvertiser(baseUrl, authToken, function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            var data = result.data;
                            setAdvertiserList(data);
                        } else {
                            $scope.POadvertisers = [];
                        }
                    } else {
                        $scope.POadvertisers = [];
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.POadvertisers = [];
                }
                else {
                    $scope.POadvertisers = [];
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function setAdvertiserList(objAdvertisers) {
        try {
            if (objAdvertisers.length > 0) {
                if ($rootScope.mainAdvertisers.length > 0) {
                    $scope.advertisers = $rootScope.mainAdvertisers;
                    $scope.POadvertisers = [];
                    angular.forEach(objAdvertisers, function (data, key) {
                        var AdvertiserId = data.AdvertiserId;
                        var objAdvertiser = alasql('SELECT * FROM ? AS add WHERE Id = ?', [$scope.advertisers, AdvertiserId]);
                        $scope.POadvertisers.push(objAdvertiser[0]);
                    });
                    if ($scope.POadvertisers.length > 0) {
                        var advertiser = parseInt($scope.selectedAdvertiser);
                        if (advertiser > 0) {
                            $scope.selectedAdvertiser = "";
                        }
                        if ($scope.selectedAdvertiser != "" && $scope.selectedAdvertiser != undefined && $scope.selectedAdvertiser != 0) {
                            $scope.MissingMaterial.AdvertiserId = $scope.selectedAdvertiser;
                            setDroupDownValue('ddlAdvertiser', ($scope.MissingMaterial.AdvertiserId));
                            changeSelect2DropDownColor("s2id_ddlAdvertiser", $scope.MissingMaterial.AdvertiserId);
                        } else {
                            $scope.MissingMaterial.AdvertiserId = "";
                            setDroupDownValue('ddlAdvertiser', "");
                            changeSelect2DropDownColor("s2id_ddlAdvertiser", $scope.MissingMaterial.AdvertiserId);
                        }
                    }
                    //else {
                    //    $scope.MissingMaterial.AdvertiserId = $scope.selectedAdvertiser;
                    //    setDroupDownValue('ddlAdvertiser', ($scope.POadvertisers.Id));
                    //    //$scope.POadvertisers = [];
                    //}
                } else {
                    $scope.POadvertisers = [];
                    getAllPartyDataThroughASI();
                }
            } else {
                $scope.POadvertisers = [];
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getDataOnChangeofDropdown() {
        try {
            var data = $scope.MissingMaterial;
            mediaOrdersFactory.getDataForPostOrdersDropDown(data, baseUrl, authToken, function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            var apiData = result.data;
                            var mAssets = apiData[0].MediaAssets
                            var iDates = apiData[0].IssueDates
                            var Advertisers = apiData[0].Advertisers
                            var aTypes = apiData[0].AdTypes

                            // set Media Asset Dropdown
                            if (mAssets.length > 0) {
                                //$scope.MediaAssets = mAssets;
                                if ($scope.selectedMediaAsset != "" && $scope.selectedMediaAsset != undefined && $scope.selectedMediaAsset != 0) {
                                    $scope.MissingMaterial.MediaAssetId = $scope.selectedMediaAsset;
                                    setDroupDownValue('ddlMediaAsset', ($scope.MissingMaterial.MediaAssetId));
                                    changeSelect2DropDownColor("s2id_ddlMediaAsset", $scope.MissingMaterial.MediaAssetId);
                                } else {
                                    $scope.MissingMaterial.MediaAssetId = "";
                                    setDroupDownValue('ddlMediaAsset', "");
                                    changeSelect2DropDownColor("s2id_ddlMediaAsset", $scope.MissingMaterial.MediaAssetId);
                                }
                            } else {
                                $scope.MissingMaterial.MediaAssetId = $scope.selectedMediaAsset;
                                setDroupDownValue('ddlMediaAsset', ($scope.MissingMaterial.MediaAssetId));
                                changeSelect2DropDownColor("s2id_ddlMediaAsset", $scope.MissingMaterial.MediaAssetId);
                                // $scope.MediaAssets = [];
                            }

                            // set Issue Date Dropdown
                            if ($scope.changeDropdown == 0) {
                                if (iDates.length > 0) {
                                    $scope.IssueDates = iDates;
                                    var issueDate = parseInt($scope.selectedIssueDate);
                                    if (issueDate > 0) {
                                        $scope.selectedIssueDate = "";
                                    }

                                    if ($scope.selectedIssueDate != "" && $scope.selectedIssueDate != undefined && $scope.selectedIssueDate != 0) {
                                        $scope.MissingMaterial.IssueDateId = $scope.selectedIssueDate;
                                        setDroupDownValue('ddlIssueDate', ($scope.IssueDates.IssueDateId));
                                        changeSelect2DropDownColor("s2id_ddlIssueDate", $scope.IssueDates.IssueDateId);

                                    } else {
                                        $scope.MissingMaterial.IssueDateId = "";
                                        setDroupDownValue('ddlIssueDate', "");
                                        changeSelect2DropDownColor("s2id_ddlIssueDate", $scope.IssueDates.IssueDateId);
                                    }
                                } else {
                                    $scope.IssueDates = [];
                                    $scope.MissingMaterial.IssueDateId = $scope.selectedIssueDate;
                                    setDroupDownValue('ddlIssueDate', ($scope.IssueDates.IssueDateId));
                                    changeSelect2DropDownColor("s2id_ddlIssueDate", $scope.IssueDates.IssueDateId);
                                }
                            }

                            // set AdType Dropdown
                            //if ($scope.changeDropdown == 0 || $scope.changeDropdown == 1) {
                            //    if (aTypes.length > 0) {
                            //        $scope.AdTypes = aTypes;
                            //        var adtype = parseInt($scope.selectedAdType);
                            //        if (adtype > 0) {
                            //            $scope.selectedAdType = "";
                            //        }
                            //        if ($scope.selectedAdType != "" && $scope.selectedAdType != undefined && $scope.selectedAdType != 0) {
                            //            $scope.MissingMaterial.AdTypeId = $scope.selectedAdType;
                            //            setDroupDownValue('ddlAdType', ($scope.AdTypes.AdTypeId));
                            //            changeSelect2DropDownColor("s2id_ddlAdType", $scope.AdTypes.AdTypeId);

                            //        } else {
                            //            $scope.MissingMaterial.AdTypeId = "";
                            //            setDroupDownValue('ddlAdType', "");
                            //            changeSelect2DropDownColor("s2id_ddlAdType", $scope.AdTypes.AdTypeId);
                            //        }
                            //    } else {
                            //        $scope.AdTypes = [];
                            //        $scope.MissingMaterial.AdTypeId = $scope.selectedAdType;
                            //        setDroupDownValue('ddlAdType', ($scope.AdTypes.AdTypeId));
                            //        changeSelect2DropDownColor("s2id_ddlAdType", $scope.AdTypes.AdTypeId);
                            //    }
                            //}

                            // set Advertiser Dropdown
                            //if ($scope.changeDropdown == 0 || $scope.changeDropdown == 1 || $scope.changeDropdown == 2) {
                            //    if (Advertisers.length > 0) {
                            //        setAdvertiserList(Advertisers);
                            //    } else {
                            //        $scope.POadvertisers = [];
                            //        $scope.MissingMaterial.AdvertiserId = $scope.selectedAdvertiser;
                            //        setDroupDownValue('ddlAdvertiser', "");
                            //        changeSelect2DropDownColor("s2id_ddlAdvertiser", "");
                            //    }
                            //}

                        }
                    }
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function initPagination() {
        try {
            //pagination
            var sortingOrder = '';
            $scope.sortingOrder = sortingOrder;
            $scope.reverse = false;
            $scope.filteredItems = [];
            $scope.groupedItems = [];
            $scope.itemsPerPage = 10;
            $scope.pagedItems = [];
            $scope.currentPage = 0;
            if ($scope.MissingMaterials.length > 0) {
                $scope.items = $scope.MissingMaterials;
            } else {
                $scope.items = [];
            }

            var searchMatch = function (haystack, needle) {
                try {
                    if (!needle) {
                        return true;
                    }
                    return haystack.toString().toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                    // return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            // init the filtered items
            $scope.search = function () {
                try {
                    $scope.filteredItems = $filter('filter')($scope.items, function (item) {
                        for (var attr in item) {
                            if (searchMatch(item[attr], $scope.query))
                                return true;
                        }
                        return false;
                    });
                    // take care of the sorting order
                    if ($scope.sortingOrder !== '') {
                        $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                    }
                    $scope.currentPage = 0;
                    // now group by pages
                    $scope.groupToPages();
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            // calculate page in place
            $scope.groupToPages = function () {
                try {
                    $scope.pagedItems = [];

                    for (var i = 0; i < $scope.filteredItems.length; i++) {
                        if (i % $scope.itemsPerPage === 0) {
                            $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [$scope.filteredItems[i]];
                        } else {
                            $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                        }
                    }
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            $scope.range = function (start, end) {
                try {
                    var ret = [];
                    if (!end) {
                        end = start;
                        start = 0;
                    }
                    for (var i = start; i < end; i++) {
                        ret.push(i);
                    }
                    return ret;
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pagedItems.length - 1) {
                    $scope.currentPage++;
                }
            };

            $scope.setPage = function () {
                $scope.currentPage = this.n;
            };

            // functions have been describe process the data for display
            $scope.search();
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onChangeMediaAsset = function () {
        try {
            var DropDown = angular.element(document.querySelector('#ddlMediaAsset'));
            var getSelectedData = DropDown.select2("data");
            if (getSelectedData != null && getSelectedData != undefined) {
                var Id = getSelectedData.id;
                var Value = getSelectedData.text;
                $scope.selectedMediaAsset = Id;
                $scope.changeDropdown = 0;
                if (Id != undefined && Id != "" && Id != 0 && Id != null) {
                    changeSelect2DropDownColor("s2id_ddlMediaAsset", Id);
                    //getIssueDatebyMediaAsset(Id);
                    $scope.MissingMaterial.IssueDateId = "";
                    $scope.MissingMaterial.AdTypeId = "";
                    $scope.MissingMaterial.AdvertiserId = "";
                    getDataOnChangeofDropdown();
                } else if (Id == "") {
                    getAllIssueDates();

                } else {
                    changeSelect2DropDownColor("s2id_ddlMediaAsset", "");
                }
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onChangeIssueDate = function () {
        try {
            var DropDown = angular.element(document.querySelector('#ddlIssueDate'));
            var getSelectedData = DropDown.select2("data");
            if (getSelectedData != null && getSelectedData != undefined) {
                var Id = getSelectedData.id;
                var Value = getSelectedData.text;
                $scope.selectedIssueDate = Id;
                $scope.changeDropdown = 1;
                if (Id != undefined && Id != "" && Id != 0 && Id != null) {
                    changeSelect2DropDownColor("s2id_ddlIssueDate", Id);
                    $scope.MissingMaterial.AdTypeId = "";
                    $scope.MissingMaterial.AdvertiserId = "";
                    getDataOnChangeofDropdown();
                } else {
                    changeSelect2DropDownColor("s2id_ddlIssueDate", "");
                }
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }



    $scope.searchOrdersbyMediaAsset = function () {
        try {
            debugger;
            $scope.isShowDefaultTable = false;
            $scope.isShowPagination = false;
            //$scope.dataLoading = true;

            //var data = objMissingMaterial; 
            var obj = {};
            obj.productionstatusId = 2;
            obj.MediaAssetId = $scope.MissingMaterial.MediaAssetId;
            obj.IssueDateId = $scope.MissingMaterial.IssueDateId;

            mediaOrdersFactory.getAllOrdersProductionStatusdWise(obj, function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            // $scope.isShowDefaultTable = false;
                            var data = result.data;
                            // $scope.MissingMaterials = result.data;
                            createListforMissingMaterial(data);

                        } else {
                            $scope.MissingMaterials = [];
                            initPagination();
                            $scope.isShowDefaultTable = true;
                            $scope.isShowPagination = false;
                            $scope.dataLoading = false;
                        }
                    } else {
                        $scope.MissingMaterials = [];
                        initPagination();
                        $scope.isShowDefaultTable = true;
                        $scope.isShowPagination = false;
                        $scope.dataLoading = false;
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.MissingMaterials = [];
                    initPagination();
                    $scope.isShowDefaultTable = true;
                    $scope.isShowPagination = false;
                    $scope.dataLoading = false;
                }
                else {
                    $scope.MissingMaterials = [];
                    initPagination();
                    $scope.isShowDefaultTable = true;
                    $scope.isShowPagination = false;
                    $scope.dataLoading = false;
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.setInvoiceDateShowOrHide = function (IsHideShow) {
        try {
            if (IsHideShow == true) {
                $scope.isShowInvoiceDate = true;
                InitDateRange();
            } else {
                $scope.MissingMaterial.InvoiceDate = "";
                $scope.isShowInvoiceDate = false;
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }
    $scope.editOrder = function (buyId) {
        $rootScope.fromDashboard = true;
        $location.path('ordersDetails/orders/' + buyId);
    }
    $scope.deleteMediaOrder = function (mediaOrderId) {
        try {
            //toastr["warning"]("Clear itself?<br /><br /><button type='button' class='btn clear'>Yes</button>")
            bootbox.confirm({
                message: "Are you sure you want to delete the selected object(s) and all of their children?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    // alert(result);
                    if (result) {
                        //var mediaOrderId = objAdjustment.MediaOrderId;
                        if (mediaOrderId != null && mediaOrderId != undefined && mediaOrderId != "") {
                            mediaOrderLinesFactory.deleteMediaOrderLinesbyMediaOrder(mediaOrderId, function (result) {
                                if (result.statusCode == 1) {

                                    if (result.data != null) {
                                        initPagination();
                                        $scope.searchOrdersbyMediaAsset();
                                        toastr.success('Deleted successfully.', 'Success!');

                                    }
                                }
                                else {
                                    toastr.error(result.message, 'Error!');
                                }
                            });
                        }
                        else {
                            console.log('media Order Id is null');
                        }
                    }
                }
            });

            //if (objAdjustment.IssueDateId > 0) {
            //    angular.forEach($scope.issueDateWiseAdjustment, function (objIssueDateWiseAdjustment, key) {
            //        if (objIssueDateWiseAdjustment.IssueDateId == objAdjustment.IssueDateId) {
            //            $scope.issueDateWiseAdjustment.splice(key, 1);
            //        }
            //    });
            //}
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    getAllMediaAssets();
    getAllIssueDates();

    getAllPartyDataThroughASI();
    getAllAdvertisers();
    initPagination();

})