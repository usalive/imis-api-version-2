﻿'use strict';

app.controller('AdjustmentController', function AdjustmentController($scope, $location, $rootScope, $compile, $stateParams, $exceptionHandler, adAdjustmentsFactory,
    mediaOrderLinesFactory, ordersFactory, $timeout, mediaOrdersFactory, advertiserFactory, authKeyService, toastr) {
    console.log('called');
    // tab Setting
    $scope.$parent.tabOrderId = $stateParams.AdOrderId;
    $scope.$parent.setAdjustmentTabActive();
    var authKey = authKeyService.getConstantAuthKey();
    var baseUrl = authKey.baseUrl;
    var authToken = authKey.authToken;

    $scope.BuyerId = $stateParams.AdOrderId;
    $scope.selectedMediaOrderId = 0;
    $scope.insertMode = "insertMode";
    //Main Data Array
    $scope.tempAdjustment = [];
    $scope.mediaIssueDateWiseadAdjustment = [];
    $scope.selectedAllGrossAdjustment = false;

    // adjustment form variable
    $scope.issueDateWiseOrderData = $rootScope.mainArray;
    $scope.adjustment = {};

    $scope.adjustment.AdOrderId = $stateParams.AdOrderId;
    $scope.isShowDefaultTable = true;
    $scope.isShowDefaultInnerTable = true;
    $scope.issueDateWiseAdjustment = [];
    // $scope.selectedIssueMediaOrder = false;
    $scope.selectedIssueMediaOrder = {};
    $scope.selectedCheckbox = [];
    $scope.selectedIssueMediaOrderData = [];
    $scope.selectedIssueMediaOrderDataDownload = [];
    $scope.adAdjustments = {};
    $scope.isMultiple = false;
    $scope.mediaAssetId = 0;
    $scope.selectedMediaAssetsIds = [];
    $scope.selectedMediaOrderIds = [];
    $scope.commonOrderDetail = {};
    $scope.MediaOrderReps = [];

    //Gross Adjustment Table 
    $scope.isShowDefaultGrossAdjustmentTable = true;
    $scope.GrossAdjustmentshowMsgs = false;
    $scope.grossAdAdjustment = {};
    $scope.grossAdAdjustments = [];
    $scope.amountType = "Amount";
    $scope.isPercentage = false;
    $scope.tempGrossAmount = parseFloat(0).toFixed(2);
    $scope.finalGrossAmount = parseFloat(0).toFixed(2);
    $scope.selectedGrossAdjustment = false;
    $scope.selectedGrossAdjustmentCheckbox = [];
    $scope.selectedGrossAdjustmentMediaOrderLines = [];

    //Net Adjustment Table 
    $scope.isShowDefaultNetAdjustmentTable = true;
    $scope.NetAdjustmentshowMsgs = false;
    $scope.netAdAdjustment = {};
    $scope.netAdAdjustments = [];
    $scope.netamountType = "Amount";
    $scope.netisPercentage = false;
    $scope.tempNetAmount = parseFloat(0).toFixed(2);
    $scope.finalNetAmount = parseFloat(0).toFixed(2);
    $scope.selectedNetAdjustment = false;
    $scope.selectedNetAdjustmentCheckbox = [];
    $scope.selectedNetAdjustmentMediaOrderLines = [];

    $scope.OrderType = "Proposal";

    // $scope.adAdjustmentsM = {};
    $scope.isShowDefaultGrossAdjustmentTableM = true;
    $scope.isShowDefaultNetAdjustmentTableM = true;
    $scope.multipleGrossAdjustmentshowMsgs = false;
    $scope.multipleNetAdjustmentshowMsgs = false;
    $scope.tempMediaOrderLinesforGrossMultiple = [];
    $scope.tempMediaOrderLinesforNetMultiple = [];
    //$scope.tempMediaOrderLinesforMultiple = [];

    $scope.loadTimeIssueDateWiseAdjustment = [];

    $scope.adAdjustmentSelect = {
        multiple: false,
        formatSearching: 'Searching the ad adjustment...',
        formatNoMatches: 'No ad adjustment found'
    };
    $scope.IsEditMode = "";
    var objGrossAndNet = {};

    function initPageControls() {
        $scope.isShowDefaultGrossAdjustmentTable = true;
        $scope.GrossAdjustmentshowMsgs = false;
        $scope.grossAdAdjustment = {};
        $scope.grossAdAdjustments = [];
        $scope.amountType = "Amount";
        $scope.isPercentage = false;
        $scope.tempGrossAmount = parseFloat(0).toFixed(2);
        $scope.finalGrossAmount = parseFloat(0).toFixed(2);
        $scope.selectedGrossAdjustment = false;

        $scope.isShowDefaultNetAdjustmentTable = true;
        $scope.NetAdjustmentshowMsgs = false;
        $scope.netAdAdjustment = {};
        $scope.netAdAdjustments = [];
        $scope.netamountType = "Amount";
        $scope.netisPercentage = false;
        $scope.tempNetAmount = parseFloat(0).toFixed(2);
        $scope.finalNetAmount = parseFloat(0).toFixed(2);
        $scope.selectedNetAdjustment = false;
    }

    function initGrossandNetCost() {
        var totalSum = alasql('SELECT sum(GrossCost),sum(NetCost) FROM ? AS add ', [$scope.issueDateWiseAdjustment]);
        var grossAmount = parseFloat(totalSum[0]["SUM(GrossCost)"]);
        var netAmount = parseFloat(totalSum[0]["SUM(NetCost)"]);
        $rootScope.GrossCost = (grossAmount).toFixed(2);
        $rootScope.NetCost = (netAmount).toFixed(2);
    }

    var firstTimeOnly = 1;

    function getNoOfIssueMediaOrder() {
        try {
            //  if ($scope.issueDateWiseAdjustment.length > 0)
            var BuyerId = $scope.BuyerId;
            if (BuyerId != null && BuyerId != undefined && BuyerId != "") {
                ordersFactory.getMediaOrderbyBuyId(BuyerId, function (result) {
                    if (result.statusCode == 1) {
                        console.log(result);
                        if (result.data != null) {
                            if (result.data.length > 0) {
                                $scope.isShowDefaultTable = false;
                                $scope.issueDateWiseAdjustment = result.data;
                                if (result.data.length > 0) {
                                    $scope.commonOrderDetail = result.data[0];
                                    $scope.MediaOrderReps = $scope.commonOrderDetail.MediaOrderReps;
                                    var commonDetaildv = angular.element(document.querySelector('#commonDetaildv'));
                                    var commonDetaildvProposal = angular.element(document.querySelector('#commonDetaildvProposal'));
                                    commonDetaildv.empty();
                                    commonDetaildvProposal.empty();
                                    var reps = ""; var htmlTemp = "";
                                    angular.forEach($scope.MediaOrderReps, function (data, key) {
                                        reps = data.RepsName + ", " + data.TerritoryName + " (" + data.Commission + "%)";
                                        if (htmlTemp != "") {
                                            htmlTemp = htmlTemp + '<div><b>Reps ' + parseInt(key + 1) + ':</b> <span>' + reps + '</span></div>';
                                        }
                                        else {
                                            htmlTemp = '<div><b>Reps ' + parseInt(key + 1) + ':</b> <span>' + reps + '</span></div>';
                                        }
                                    });
                                    var finalDiv = $compile(htmlTemp)($scope);
                                    var finalDiv1 = $compile(htmlTemp)($scope);
                                    commonDetaildv.append(finalDiv);
                                    commonDetaildvProposal.append(finalDiv1);

                                }
                                if (firstTimeOnly == 1) {
                                    //console.log('firstTimeOnly')
                                    angular.forEach($scope.issueDateWiseAdjustment, function (data, key) {
                                        var obj = {};
                                        obj.MediaOrderId = data.MediaOrderId;
                                        obj.GrossCost = data.GrossCost;
                                        obj.NetCost = data.NetCost;
                                        $scope.loadTimeIssueDateWiseAdjustment.push(obj);
                                    });
                                    firstTimeOnly++;
                                }
                                initGrossandNetCost();
                            }
                            else {
                                $scope.isShowDefaultTable = true;
                                $scope.issueDateWiseAdjustment = [];
                                $scope.loadTimeIssueDateWiseAdjustment = [];
                            }
                        } else {
                            $scope.isShowDefaultTable = true;
                            $scope.issueDateWiseAdjustment = [];
                            $scope.loadTimeIssueDateWiseAdjustment = [];
                        }
                    }
                    else if (result.statusCode == 3) {
                        $scope.isShowDefaultTable = true;
                        $scope.issueDateWiseAdjustment = [];
                        $scope.loadTimeIssueDateWiseAdjustment = [];
                    }
                    else {
                        toastr.error(result.message, 'Error!');
                    }
                });
            }
            else {
                console.log('Buyer Id is null');
            }

        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getAdAdjustmentByMediaAssetId() {
        try {
            if ($scope.issueDateWiseAdjustment.length > 0) {
                if ($scope.selectedMediaAssetsIds.length > 0) {

                    var MediaAssetIds = "";
                    angular.forEach($scope.selectedMediaAssetsIds, function (data, key) {
                        var id = data;
                        if (key == 0) {
                            MediaAssetIds += "id=" + id;
                        } else {
                            MediaAssetIds += "&id=" + id;
                        }
                    });
                    if (MediaAssetIds != null && MediaAssetIds != undefined && MediaAssetIds != "") {
                        adAdjustmentsFactory.getByMultipleMediaAsset(MediaAssetIds, function (result) {
                            if (result.statusCode == 1) {
                                console.log(result);
                                if (result.data != null) {
                                    if (result.data.length > 0) {
                                        $scope.adAdjustments = result.data;
                                    }
                                }
                            }
                            else {
                                toastr.error(result.message, 'Error!');
                            }
                        });
                    }
                    else {
                        console.log('media assets Id is null');
                    }
                }


                //$scope.mediaAssetId = MediaAssetId;
                //if (MediaAssetId != null && MediaAssetId != undefined && MediaAssetId != "") {
                //    adAdjustmentsFactory.getByMediaAsset(MediaAssetId, function (result) {
                //        if (result.statusCode == 1) {
                //            console.log(result);
                //            if (result.data != null) {
                //                if (result.data.length > 0) {
                //                    $scope.adAdjustments = result.data;
                //                }
                //            }
                //        }
                //        else {
                //            toastr.error(result.message, 'Error!');
                //        }
                //    });
                //}
                //else {
                //    console.log('media Order Id is null');
                //}
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getMediaOrderLinesByMediaOrder(mediaOrderId) {
        try {
            if (mediaOrderId != null && mediaOrderId != undefined && mediaOrderId != "") {
                mediaOrderLinesFactory.getMediaOrderLinesbyOrderId(mediaOrderId, function (result) {
                    if (result.statusCode == 1) {
                        console.log(result);
                        if (result.data != null) {
                            if (result.data.length > 0) {
                                $scope.mediaIssueDateWiseadAdjustment = result.data;
                                if ($scope.mediaIssueDateWiseadAdjustment.length > 0) {
                                    var objgrossAdAdjustments = alasql('SELECT * FROM ? AS add WHERE GrossInd  = ?', [$scope.mediaIssueDateWiseadAdjustment, true]);
                                    var objnetAdAdjustments = alasql('SELECT * FROM ? AS add WHERE GrossInd  = ?', [$scope.mediaIssueDateWiseadAdjustment, false]);
                                    // $scope.grossAdAdjustments = alasql('SELECT * FROM ? AS add WHERE GrossInd  = ?', [$scope.mediaIssueDateWiseadAdjustment, true]);
                                    // $scope.netAdAdjustments = alasql('SELECT * FROM ? AS add WHERE GrossInd  = ?', [$scope.mediaIssueDateWiseadAdjustment, false]);
                                    if (objgrossAdAdjustments.length > 0) {
                                        $scope.isShowDefaultGrossAdjustmentTable = false;
                                    } else {
                                        $scope.isShowDefaultGrossAdjustmentTable = true;
                                    }
                                    if (objnetAdAdjustments.length > 0) {
                                        $scope.isShowDefaultNetAdjustmentTable = false;
                                    } else {
                                        $scope.isShowDefaultNetAdjustmentTable = true;
                                    }
                                    angular.forEach(objgrossAdAdjustments, function (data, key) {
                                        if (key == 0) {
                                            $scope.isGrossChangedByMoveUp = true;
                                        } else {
                                            $scope.isGrossChangedByMoveUp = false;
                                        }
                                        $scope.AdMediaOrderdAdjustment(data, "editMode");
                                    });
                                    if (objgrossAdAdjustments.length == 1) {
                                        $scope.isGrossChangedByMoveUp = false;
                                    }

                                    angular.forEach(objnetAdAdjustments, function (data, key) {
                                        if (key == 0) {
                                            $scope.isNetChangedByMove = true;
                                        } else {
                                            $scope.isNetChangedByMove = false;
                                        }
                                        $scope.AdNetMediaOrderdAdjustment(data, "editMode");
                                    });
                                    if (objnetAdAdjustments.length == 1) {
                                        $scope.isNetChangedByMove = false;
                                    }
                                }
                            } else {
                                $scope.isShowDefaultGrossAdjustmentTable = true;
                                $scope.isShowDefaultNetAdjustmentTable = true;
                                $scope.grossAdAdjustments = [];
                                $scope.netAdAdjustments = [];

                            }
                        }
                    }
                    else if (result.statusCode == 3) {
                        $scope.isShowDefaultGrossAdjustmentTable = true;
                        $scope.isShowDefaultNetAdjustmentTable = true;
                        $scope.grossAdAdjustments = [];
                        $scope.netAdAdjustments = [];

                    }
                    else {
                        toastr.error(result.message, 'Error!');
                    }
                });
            }
            else {
                console.log('media Order Id is null');
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    //redirect to production page
    $scope.redirectToProductionPage = function () {
        $location.path('ordersDetails/production/' + $scope.BuyerId + '');
    }

    //Gross Amount Calculation
    $scope.AdMediaOrderdAdjustment = function (objGrossAdAdjustment, operationMode) {
        try {
            if (($scope.selectedCheckbox.length == 1) || ($scope.selectedCheckbox.length > 0 && $scope.isMultipleIssueDate == true)) {
                if ($scope.GrossAdAdjustmentModalForm.$valid) {

                    if ((objGrossAdAdjustment.AdAdjustmentId != null && objGrossAdAdjustment.AdAdjustmentId != undefined && objGrossAdAdjustment.AdAdjustmentId != "") ||
                        (objGrossAdAdjustment.AdAdjustmentName != null && objGrossAdAdjustment.AdAdjustmentName != undefined && objGrossAdAdjustment.AdAdjustmentName != "")) {
                        //objGrossAdAdjustment.issueDateOrderId = $scope.selectedCheckbox[0];
                        objGrossAdAdjustment.issueDateOrderId = $scope.selectedIssueMediaOrderData[0].IssueDateId;

                        if (objGrossAdAdjustment.AdAdjustmentId != null && objGrossAdAdjustment.AdAdjustmentId != undefined && objGrossAdAdjustment.AdAdjustmentId != "") {
                            var objAdAdjustmentName = alasql('SELECT * FROM ? AS add WHERE SrNo  = ?', [$scope.adAdjustments, parseInt(objGrossAdAdjustment.AdAdjustmentId)]);
                            if (objAdAdjustmentName.length > 0) {
                                objGrossAdAdjustment.AdAdjustmentName = objAdAdjustmentName[0].AdAdjustmentName;
                            }
                        } else {
                            objGrossAdAdjustment.AdAdjustmentName = objGrossAdAdjustment.AdAdjustmentName;
                        }

                        if ($scope.grossAdAdjustments.length >= 0) {
                            if ($scope.isGrossChangedByMoveUp == true) {
                                $scope.grossAdAdjustments = [];
                            }

                            var len = $scope.grossAdAdjustments.length;
                            if (len == 0 || len == undefined && len == "" && len == null) {
                                objGrossAdAdjustment.SequenceNumber = 1;
                                // Amount
                                if (objGrossAdAdjustment.AmountPercent == 0) {
                                    var tempAmount = parseFloat($scope.tempGrossAmount);
                                    var adjustmentValue = parseFloat(objGrossAdAdjustment.AdjustmentValue);
                                    var afterMinusAmount = 0;
                                    if (objGrossAdAdjustment.SurchargeDiscount == 0) {
                                        afterMinusAmount = parseFloat(tempAmount + adjustmentValue);
                                    } else {
                                        afterMinusAmount = parseFloat(tempAmount - adjustmentValue);
                                    }
                                    //afterMinusAmount = parseFloat(tempAmount - adjustmentValue);
                                    afterMinusAmount = afterMinusAmount.toFixed(2);
                                    objGrossAdAdjustment.tempRunningAmount = afterMinusAmount;
                                    objGrossAdAdjustment.tempAdjustmentAmount = adjustmentValue.toFixed(2);
                                    $scope.finalGrossAmount = objGrossAdAdjustment.tempRunningAmount;
                                    $scope.tempNetAmount = $scope.finalGrossAmount;
                                    setInitiallyFinalNetAdjustmentAmount($scope.finalGrossAmount);
                                    objGrossAdAdjustment.isPercentage = false;
                                }
                                    //Percentage
                                else if (objGrossAdAdjustment.AmountPercent == 1) {
                                    var tempAmount = parseFloat($scope.tempGrossAmount);
                                    var adjustmentValue = parseFloat(objGrossAdAdjustment.AdjustmentValue);
                                    var percentageValueInAmount = parseFloat(adjustmentValue * tempAmount / 100);
                                    var afterMinusAmount = 0;
                                    if (objGrossAdAdjustment.SurchargeDiscount == 0) {
                                        afterMinusAmount = parseFloat(tempAmount + percentageValueInAmount);
                                    } else {
                                        afterMinusAmount = parseFloat(tempAmount - percentageValueInAmount);
                                    }
                                    //var afterMinusAmount = parseFloat(tempAmount - percentageValueInAmount);
                                    afterMinusAmount = afterMinusAmount.toFixed(2);
                                    objGrossAdAdjustment.tempRunningAmount = afterMinusAmount;
                                    objGrossAdAdjustment.tempAdjustmentAmount = percentageValueInAmount.toFixed(2);
                                    $scope.finalGrossAmount = objGrossAdAdjustment.tempRunningAmount;
                                    $scope.tempNetAmount = $scope.finalGrossAmount;
                                    setInitiallyFinalNetAdjustmentAmount($scope.finalGrossAmount);
                                    objGrossAdAdjustment.isPercentage = true;
                                }

                            } else {
                                var PlusOneInlen = len + 1;
                                objGrossAdAdjustment.SequenceNumber = PlusOneInlen;
                                var dataContext = $scope.grossAdAdjustments;
                                var objSequenceNoWiseGrossAdAdjustment = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [dataContext, parseInt(len)]);

                                if (objGrossAdAdjustment.AmountPercent == 0) {
                                    var tempAmount = parseFloat(objSequenceNoWiseGrossAdAdjustment[0].tempRunningAmount);
                                    var adjustmentValue = parseFloat(objGrossAdAdjustment.AdjustmentValue);
                                    var afterMinusAmount = 0;
                                    if (objGrossAdAdjustment.SurchargeDiscount == 0) {
                                        afterMinusAmount = parseFloat(tempAmount + adjustmentValue);
                                    } else {
                                        afterMinusAmount = parseFloat(tempAmount - adjustmentValue);
                                    }
                                    //var afterMinusAmount = parseFloat(tempAmount - adjustmentValue);
                                    afterMinusAmount = afterMinusAmount.toFixed(2);
                                    objGrossAdAdjustment.tempRunningAmount = afterMinusAmount;
                                    objGrossAdAdjustment.tempAdjustmentAmount = adjustmentValue.toFixed(2);
                                    $scope.finalGrossAmount = objGrossAdAdjustment.tempRunningAmount;
                                    $scope.tempNetAmount = $scope.finalGrossAmount;
                                    setInitiallyFinalNetAdjustmentAmount($scope.finalGrossAmount);
                                    objGrossAdAdjustment.isPercentage = false;
                                }
                                    //Percentage
                                else if (objGrossAdAdjustment.AmountPercent == 1) {
                                    var tempAmount = parseFloat(objSequenceNoWiseGrossAdAdjustment[0].tempRunningAmount);
                                    var adjustmentValue = parseFloat(objGrossAdAdjustment.AdjustmentValue);
                                    var percentageValueInAmount = parseFloat(adjustmentValue * tempAmount / 100);
                                    var afterMinusAmount = 0;
                                    if (objGrossAdAdjustment.SurchargeDiscount == 0) {
                                        afterMinusAmount = parseFloat(tempAmount + percentageValueInAmount);
                                    } else {
                                        afterMinusAmount = parseFloat(tempAmount - percentageValueInAmount);
                                    }
                                    //var afterMinusAmount = parseFloat(tempAmount - percentageValueInAmount);
                                    afterMinusAmount = afterMinusAmount.toFixed(2);
                                    objGrossAdAdjustment.tempRunningAmount = afterMinusAmount;
                                    objGrossAdAdjustment.tempAdjustmentAmount = percentageValueInAmount.toFixed(2);
                                    $scope.finalGrossAmount = objGrossAdAdjustment.tempRunningAmount;
                                    $scope.tempNetAmount = $scope.finalGrossAmount;
                                    setInitiallyFinalNetAdjustmentAmount($scope.finalGrossAmount);
                                    objGrossAdAdjustment.isPercentage = true;
                                }
                            }
                        }
                        createObjectForGrossAmount(objGrossAdAdjustment, operationMode);

                        if ($scope.netAdAdjustments.length == 0) {
                            $scope.isShowDefaultNetAdjustmentTable = true;
                            $scope.isShowDefaultNetAdjustmentTableM = true;
                        }
                    }
                }
                else {
                    $scope.GrossAdjustmentshowMsgs = true;
                }
            }
            else {
                toastr.info('Please Select One Issue Media Order', 'Information!');
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function setInitiallyFinalNetAdjustmentAmount(finalAmount) {
        var netAdjustmentData = $scope.netAdAdjustments;
        if (netAdjustmentData.length <= 0) {
            $scope.finalNetAmount = finalAmount;
        }
    }

    function createObjectForGrossAmount(objGrossAdAdjustment, operationMode) {
        try {
            var obj = {};
            objGrossAndNet = {};
            obj.MediaOrderLineId = objGrossAdAdjustment.MediaOrderLineId;
            obj.AdAdjustmentId = objGrossAdAdjustment.AdAdjustmentId;
            obj.AdAdjustmentName = objGrossAdAdjustment.AdAdjustmentName;
            obj.AdjustmentValue = parseFloat(objGrossAdAdjustment.AdjustmentValue);
            obj.AmountPercent = objGrossAdAdjustment.AmountPercent;
            //obj.MediaOrderId = objGrossAdAdjustment.MediaOrderId;
            obj.MediaOrderId = $scope.selectedMediaOrderId;

            obj.ProductCode = objGrossAdAdjustment.ProductCode;
            obj.SequenceNumber = objGrossAdAdjustment.SequenceNumber;
            obj.SurchargeDiscount = objGrossAdAdjustment.SurchargeDiscount;
            obj.tempAdjustmentAmount = objGrossAdAdjustment.tempAdjustmentAmount;
            obj.tempRunningAmount = objGrossAdAdjustment.tempRunningAmount;
            obj.isPercentage = objGrossAdAdjustment.isPercentage;
            obj.GrossInd = true;


            //}, 5000);
            //if (operationMode == "editMode") {
            //    EditGrossMediaOrderLines(obj)
            //} else {
            //    saveGrossMediaOrderLines(obj);
            //}

            if (operationMode != "insertMode") {
                EditGrossMediaOrderLines(obj)
            } else {
                saveGrossMediaOrderLines(obj);
            }

        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function EditGrossMediaOrderLines(objMediaOrderLine) {
        try {
            var MediaOrderLineModel = {};
            MediaOrderLineModel.MediaOrderLineId = objMediaOrderLine.MediaOrderLineId;
            // MediaOrderLineModel.MediaOrderIds = $scope.selectedMediaOrderIds;
            MediaOrderLineModel.MediaOrderIds = objMediaOrderLine.MediaOrderId;
            MediaOrderLineModel.AdAdjustmentName = objMediaOrderLine.AdAdjustmentName;
            MediaOrderLineModel.AmountPercent = objMediaOrderLine.AmountPercent;
            MediaOrderLineModel.SurchargeDiscount = objMediaOrderLine.SurchargeDiscount;
            MediaOrderLineModel.AdjustmentValue = objMediaOrderLine.AdjustmentValue;
            MediaOrderLineModel.GrossInd = objMediaOrderLine.GrossInd;
            // need to save multiple times as no of checkbox is active
            console.log(MediaOrderLineModel);
            $scope.grossAdAdjustments.push(objMediaOrderLine);

            //update net amount
            if ($scope.netAdAdjustments.length > 0) {
                var netAdjustmentDaat = $scope.netAdAdjustments;
                angular.forEach(netAdjustmentDaat, function (data, key) {
                    var objAdAdjustmentName = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [netAdjustmentDaat, parseInt(key + 1)]);
                    if (key == 0) {
                        $scope.isNetChangedByMove = true;
                    } else {
                        $scope.isNetChangedByMove = false;
                    }
                    $scope.AdNetMediaOrderdAdjustment(objAdAdjustmentName[0], "editMode");
                });
                if (netAdjustmentDaat.length == 1) {
                    $scope.isNetChangedByMove = false;
                }
            }
            //end net amount
            if ($scope.isMultiple == true) {
                $scope.isShowDefaultGrossAdjustmentTable = true;
                $scope.isShowDefaultGrossAdjustmentTableM = false;
            } else {
                $scope.isShowDefaultGrossAdjustmentTable = false;
            }

            // $scope.isShowDefaultGrossAdjustmentTable = false;

            clearGrossAmountSection();
            $scope.GrossAdAdjustmentModalForm.$setPristine();
            $scope.GrossAdjustmentshowMsgs = false;

        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function saveGrossMediaOrderLines(objMediaOrderLine) {
        try {
            var MediaOrderLineModel = {};
            MediaOrderLineModel.MediaOrderLineId = objMediaOrderLine.MediaOrderLineId;
            //MediaOrderLineModel.MediaOrderIds = objMediaOrderLine.MediaOrderId;
            //MediaOrderLineModel.SequenceNumber = objMediaOrderLine.SequenceNumber;
            MediaOrderLineModel.MediaOrderIds = $scope.selectedMediaOrderIds;
            MediaOrderLineModel.AdAdjustmentName = objMediaOrderLine.AdAdjustmentName;
            MediaOrderLineModel.AmountPercent = objMediaOrderLine.AmountPercent;
            MediaOrderLineModel.SurchargeDiscount = objMediaOrderLine.SurchargeDiscount;
            MediaOrderLineModel.AdjustmentValue = objMediaOrderLine.AdjustmentValue;
            //MediaOrderLineModel.ProductCode = objMediaOrderLine.ProductCode;
            MediaOrderLineModel.GrossInd = objMediaOrderLine.GrossInd;
            // need to save multiple times as no of checkbox is active
            console.log(MediaOrderLineModel);
            if (MediaOrderLineModel.MediaOrderLineId <= 0 || MediaOrderLineModel.MediaOrderLineId == undefined) {
                mediaOrderLinesFactory.saveMediaOrderLines(MediaOrderLineModel, function (result) {
                    if (result.statusCode == 1) {
                        var responseData = result.data.MediaOrderLines;
                        var MOrderId = objMediaOrderLine.MediaOrderId;
                        var objCurrentAdAdjustments = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  = ?', [responseData, parseInt(MOrderId)]);
                        objMediaOrderLine.MediaOrderLineId = objCurrentAdAdjustments[0].MediaOrderLineId;
                        // objMediaOrderLine.MediaOrderLineId = result.data.MediaOrderLines[0].MediaOrderLineId;

                        // for multiple delete
                        if ($scope.isMultiple == true) {
                            var multipleMediaOrderLines = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  != ?', [responseData, parseInt(objMediaOrderLine.MediaOrderId)]);
                            angular.forEach(multipleMediaOrderLines, function (data, key) {
                                data.AdAdjustmentId = objMediaOrderLine.AdAdjustmentId;
                                data.tempAdjustmentAmount = objMediaOrderLine.tempAdjustmentAmount;
                                data.tempRunningAmount = objMediaOrderLine.tempRunningAmount;
                                $scope.tempMediaOrderLinesforGrossMultiple.push(data);
                            });
                        }

                        $scope.grossAdAdjustments.push(objMediaOrderLine);

                        var grossCost = result.data.Cost.GrossCost;
                        var netCost = result.data.Cost.NetCost;
                        $rootScope.GrossCost = (grossCost).toFixed(2);
                        $rootScope.NetCost = (netCost).toFixed(2);
                        getNoOfIssueMediaOrder();
                        //update net amount
                        if ($scope.netAdAdjustments.length > 0) {
                            var netAdjustmentDaat = $scope.netAdAdjustments;
                            angular.forEach(netAdjustmentDaat, function (data, key) {
                                var objAdAdjustmentName = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [netAdjustmentDaat, parseInt(key + 1)]);
                                if (key == 0) {
                                    $scope.isNetChangedByMove = true;
                                } else {
                                    $scope.isNetChangedByMove = false;
                                }
                                $scope.AdNetMediaOrderdAdjustment(objAdAdjustmentName[0], "editMode");
                            });
                            if (netAdjustmentDaat.length == 1) {
                                $scope.isNetChangedByMove = false;
                            }
                        }
                        //end net amount

                        if ($scope.isMultiple == true) {
                            $scope.isShowDefaultGrossAdjustmentTable = true;
                            $scope.isShowDefaultGrossAdjustmentTableM = false;
                            changeSelect2DropDownColor("s2id_ddlAdAdjustmentM", "");
                        } else {
                            changeSelect2DropDownColor("s2id_ddlAdAdjustment", "");
                            $scope.isShowDefaultGrossAdjustmentTable = false;
                        }
                        //$scope.isShowDefaultGrossAdjustmentTable = false;
                        clearGrossAmountSection();
                        $scope.GrossAdAdjustmentModalForm.$setPristine();
                        $scope.GrossAdjustmentshowMsgs = false;
                        toastr.success('Save successfully.', 'Success!');
                    }
                    else {
                        toastr.error(result.message, 'Error!');
                    }
                })
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function clearGrossAmountSection() {
        try {
            $scope.grossAdAdjustment.AdAdjustmentId = "";
            $scope.grossAdAdjustment.AdAdjustmentName = "";
            $scope.grossAdAdjustment.AdjustmentValue = "";
            $scope.grossAdAdjustment.SequenceNumber = 0;
            $scope.grossAdAdjustment.tempAdjustmentAmount = parseFloat(0).toFixed(2);
            $scope.grossAdAdjustment.tempRunningAmount = parseFloat(0).toFixed(2);
            $scope.grossAdAdjustment.isPercentage = false;
            initSelect2('s2id_ddlAdAdjustment', 'Select Ad Adjustment');
            initSelect2('s2id_ddlAdAdjustmentM', 'Select Ad Adjustment');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    //Net Amount Calculation
    $scope.AdNetMediaOrderdAdjustment = function (objNetAdAdjustment, operationMode) {
        try {
            if (($scope.selectedCheckbox.length == 1) || ($scope.selectedCheckbox.length > 0 && $scope.isMultipleIssueDate == true)) {
                if ($scope.NetAdAdjustmentModalForm.$valid) {

                    if ((objNetAdAdjustment.AdAdjustmentId != null && objNetAdAdjustment.AdAdjustmentId != undefined && objNetAdAdjustment.AdAdjustmentId != "") ||
                        (objNetAdAdjustment.AdAdjustmentName != null && objNetAdAdjustment.AdAdjustmentName != undefined && objNetAdAdjustment.AdAdjustmentName != "")) {
                        //objNetAdAdjustment.issueDateOrderId = $scope.selectedCheckbox[0];
                        objNetAdAdjustment.issueDateOrderId = $scope.selectedIssueMediaOrderData[0].IssueDateId;

                        if (objNetAdAdjustment.AdAdjustmentId != null && objNetAdAdjustment.AdAdjustmentId != undefined && objNetAdAdjustment.AdAdjustmentId != "") {
                            var objAdAdjustmentName = alasql('SELECT * FROM ? AS add WHERE SrNo  = ?', [$scope.adAdjustments, parseInt(objNetAdAdjustment.AdAdjustmentId)]);
                            if (objAdAdjustmentName.length > 0) {
                                objNetAdAdjustment.AdAdjustmentName = objAdAdjustmentName[0].AdAdjustmentName;
                            }
                        } else {
                            objNetAdAdjustment.AdAdjustmentName = objNetAdAdjustment.AdAdjustmentName;
                        }

                        if ($scope.netAdAdjustments.length >= 0) {
                            //if ($scope.isChangedByGross == true) {
                            //    $scope.netAdAdjustments = [];
                            //}
                            if ($scope.isNetChangedByMove == true) {
                                $scope.netAdAdjustments = [];
                            }
                            var len = $scope.netAdAdjustments.length;
                            if (len == 0 || len == undefined && len == "" && len == null) {
                                objNetAdAdjustment.SequenceNumber = 1;
                                // Amount
                                if (objNetAdAdjustment.AmountPercent == 0) {
                                    var tempAmount = parseFloat($scope.tempNetAmount);
                                    var adjustmentValue = parseFloat(objNetAdAdjustment.AdjustmentValue);
                                    var afterMinusAmount = 0;
                                    if (objNetAdAdjustment.SurchargeDiscount == 0) {
                                        afterMinusAmount = parseFloat(tempAmount + adjustmentValue);
                                    } else {
                                        afterMinusAmount = parseFloat(tempAmount - adjustmentValue);
                                    }
                                    //var afterMinusAmount = parseFloat(tempAmount - adjustmentValue);
                                    afterMinusAmount = afterMinusAmount.toFixed(2);
                                    objNetAdAdjustment.tempRunningAmount = afterMinusAmount;
                                    objNetAdAdjustment.tempAdjustmentAmount = adjustmentValue.toFixed(2);
                                    $scope.finalNetAmount = objNetAdAdjustment.tempRunningAmount;
                                    objNetAdAdjustment.netisPercentage = false;
                                }
                                    //Percentage
                                else if (objNetAdAdjustment.AmountPercent == 1) {
                                    var tempAmount = parseFloat($scope.tempNetAmount);
                                    var adjustmentValue = parseFloat(objNetAdAdjustment.AdjustmentValue);
                                    var percentageValueInAmount = parseFloat(adjustmentValue * tempAmount / 100);
                                    var afterMinusAmount = 0;
                                    if (objNetAdAdjustment.SurchargeDiscount == 0) {
                                        afterMinusAmount = parseFloat(tempAmount + percentageValueInAmount);
                                    } else {
                                        afterMinusAmount = parseFloat(tempAmount - percentageValueInAmount);
                                    }
                                    //var afterMinusAmount = parseFloat(tempAmount - percentageValueInAmount);
                                    afterMinusAmount = afterMinusAmount.toFixed(2);
                                    objNetAdAdjustment.tempRunningAmount = afterMinusAmount;
                                    objNetAdAdjustment.tempAdjustmentAmount = percentageValueInAmount.toFixed(2);
                                    $scope.finalNetAmount = objNetAdAdjustment.tempRunningAmount;
                                    objNetAdAdjustment.netisPercentage = true;
                                }

                            } else {
                                var PlusOneInlen = len + 1;
                                objNetAdAdjustment.SequenceNumber = PlusOneInlen;
                                var dataContext = $scope.netAdAdjustments;
                                var objSequenceNoWiseGrossAdAdjustment = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [dataContext, parseInt(len)]);

                                if (objNetAdAdjustment.AmountPercent == 0) {
                                    var tempAmount = parseFloat(objSequenceNoWiseGrossAdAdjustment[0].tempRunningAmount);
                                    var adjustmentValue = parseFloat(objNetAdAdjustment.AdjustmentValue);
                                    var afterMinusAmount = 0;
                                    if (objNetAdAdjustment.SurchargeDiscount == 0) {
                                        afterMinusAmount = parseFloat(tempAmount + adjustmentValue);
                                    } else {
                                        afterMinusAmount = parseFloat(tempAmount - adjustmentValue);
                                    }
                                    //var afterMinusAmount = parseFloat(tempAmount - adjustmentValue);
                                    afterMinusAmount = afterMinusAmount.toFixed(2);
                                    objNetAdAdjustment.tempRunningAmount = afterMinusAmount;
                                    objNetAdAdjustment.tempAdjustmentAmount = adjustmentValue.toFixed(2);
                                    $scope.finalNetAmount = objNetAdAdjustment.tempRunningAmount;
                                    objNetAdAdjustment.netisPercentage = false;
                                }
                                    //Percentage
                                else if (objNetAdAdjustment.AmountPercent == 1) {
                                    var tempAmount = parseFloat(objSequenceNoWiseGrossAdAdjustment[0].tempRunningAmount);
                                    var adjustmentValue = parseFloat(objNetAdAdjustment.AdjustmentValue);
                                    var percentageValueInAmount = parseFloat(adjustmentValue * tempAmount / 100);
                                    var afterMinusAmount = 0;
                                    if (objNetAdAdjustment.SurchargeDiscount == 0) {
                                        afterMinusAmount = parseFloat(tempAmount + percentageValueInAmount);
                                    } else {
                                        afterMinusAmount = parseFloat(tempAmount - percentageValueInAmount);
                                    }
                                    //var afterMinusAmount = parseFloat(tempAmount - percentageValueInAmount);
                                    afterMinusAmount = afterMinusAmount.toFixed(2);
                                    objNetAdAdjustment.tempRunningAmount = afterMinusAmount;
                                    objNetAdAdjustment.tempAdjustmentAmount = percentageValueInAmount.toFixed(2);
                                    $scope.finalNetAmount = objNetAdAdjustment.tempRunningAmount;
                                    objNetAdAdjustment.netisPercentage = true;
                                }
                            }
                        }
                        createObjectForNetAmount(objNetAdAdjustment, operationMode)
                        // clearNetAmountSection();
                    }
                    //}
                    //else {
                    //    toastr.info('First Create Ad Adjustment for Gross Amount', 'Information!');
                    //}
                }
                else {
                    $scope.NetAdjustmentshowMsgs = true;
                }
            }
            else {
                toastr.info('Please Select One Issue Media Order', 'Information!');

            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function createObjectForNetAmount(objNetAdAdjustment, operationMode) {
        try {
            var obj = {};
            obj.MediaOrderLineId = objNetAdAdjustment.MediaOrderLineId;
            obj.AdAdjustmentId = objNetAdAdjustment.AdAdjustmentId;
            obj.AdAdjustmentName = objNetAdAdjustment.AdAdjustmentName;
            obj.AdjustmentValue = parseFloat(objNetAdAdjustment.AdjustmentValue);
            obj.AmountPercent = objNetAdAdjustment.AmountPercent;
            //obj.MediaOrderId = objNetAdAdjustment.MediaOrderId;
            obj.MediaOrderId = $scope.selectedMediaOrderId;
            obj.ProductCode = objNetAdAdjustment.ProductCode;
            obj.SequenceNumber = objNetAdAdjustment.SequenceNumber;
            obj.SurchargeDiscount = objNetAdAdjustment.SurchargeDiscount;
            obj.tempAdjustmentAmount = objNetAdAdjustment.tempAdjustmentAmount;
            obj.tempRunningAmount = objNetAdAdjustment.tempRunningAmount;
            obj.isPercentage = objNetAdAdjustment.netisPercentage;
            obj.GrossInd = false;

            //angular.forEach($scope.issueDateWiseAdjustment, function (data, key) {
            //    angular.forEach($scope.selectedMediaOrderIds, function (data1, key1) {
            //        if (data.MediaOrderId == data1) {
            //            var cost = parseFloat($scope.issueDateWiseAdjustment[key].NetCost);
            //            if (objNetAdAdjustment.SurchargeDiscount == 0) {
            //                //var changecost = parseFloat(obj.tempAdjustmentAmount);
            //                //var finalcost = parseFloat(cost + changecost)
            //                var amount = $scope.finalNetAmount;
            //                $scope.issueDateWiseAdjustment[key].NetCost = amount.toFixed(2);
            //            } else {
            //                //var changecost = parseFloat(obj.tempAdjustmentAmount);
            //                //var finalcost = parseFloat(cost - changecost)
            //                var amount = $scope.finalNetAmount;
            //                $scope.issueDateWiseAdjustment[key].NetCost = amount.toFixed(2);
            //            }
            //        }
            //    });
            //});

            //angular.forEach($scope.issueDateWiseAdjustment, function (data, key) {
            //    if (data.MediaOrderId == obj.MediaOrderId) {
            //        $scope.issueDateWiseAdjustment[key] == obj.tempAdjustmentAmount;
            //    }
            //});

            //if (operationMode == "editMode") {
            //    EditNetMediaOrderLines(obj)
            //} else {
            //    saveNetMediaOrderLines(obj);
            //}

            if (operationMode != "insertMode") {
                EditNetMediaOrderLines(obj)
            } else {
                saveNetMediaOrderLines(obj);
            }

        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function EditNetMediaOrderLines(objMediaOrderLine) {
        try {
            var MediaOrderLineModel = {};
            MediaOrderLineModel.MediaOrderLineId = objMediaOrderLine.MediaOrderLineId;
            // MediaOrderLineModel.MediaOrderIds = $scope.selectedMediaOrderIds;
            MediaOrderLineModel.MediaOrderIds = objMediaOrderLine.MediaOrderId;
            MediaOrderLineModel.AdAdjustmentName = objMediaOrderLine.AdAdjustmentName;
            MediaOrderLineModel.AmountPercent = objMediaOrderLine.AmountPercent;
            MediaOrderLineModel.SurchargeDiscount = objMediaOrderLine.SurchargeDiscount;
            MediaOrderLineModel.AdjustmentValue = objMediaOrderLine.AdjustmentValue;
            MediaOrderLineModel.GrossInd = objMediaOrderLine.GrossInd;
            console.log(MediaOrderLineModel);
            // need to save multiple times as no of checkbox is active
            $scope.netAdAdjustments.push(objMediaOrderLine);

            if ($scope.isMultiple == true) {
                $scope.isShowDefaultNetAdjustmentTable = true;
                $scope.isShowDefaultNetAdjustmentTableM = false;
            } else {
                $scope.isShowDefaultNetAdjustmentTable = false;
            }
            //$scope.isShowDefaultNetAdjustmentTable = false;
            clearNetAmountSection();
            $scope.NetAdAdjustmentModalForm.$setPristine();
            $scope.NetAdjustmentshowMsgs = false;

        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function saveNetMediaOrderLines(objMediaOrderLine) {
        try {
            var MediaOrderLineModel = {};
            MediaOrderLineModel.MediaOrderLineId = objMediaOrderLine.MediaOrderLineId;
            // MediaOrderLineModel.MediaOrderId = objMediaOrderLine.MediaOrderId;
            // MediaOrderLineModel.SequenceNumber = objMediaOrderLine.SequenceNumber;

            MediaOrderLineModel.MediaOrderIds = $scope.selectedMediaOrderIds;
            MediaOrderLineModel.AdAdjustmentName = objMediaOrderLine.AdAdjustmentName;
            MediaOrderLineModel.AmountPercent = objMediaOrderLine.AmountPercent;
            MediaOrderLineModel.SurchargeDiscount = objMediaOrderLine.SurchargeDiscount;
            MediaOrderLineModel.AdjustmentValue = objMediaOrderLine.AdjustmentValue;
            // MediaOrderLineModel.ProductCode = objMediaOrderLine.ProductCode;
            MediaOrderLineModel.GrossInd = objMediaOrderLine.GrossInd;
            console.log(MediaOrderLineModel);
            // need to save multiple times as no of checkbox is active
            if (MediaOrderLineModel.MediaOrderLineId <= 0 || MediaOrderLineModel.MediaOrderLineId == undefined) {
                mediaOrderLinesFactory.saveMediaOrderLines(MediaOrderLineModel, function (result) {
                    if (result.statusCode == 1) {
                        var responseData = result.data.MediaOrderLines;
                        var MOrderId = objMediaOrderLine.MediaOrderId;
                        var objCurrentAdAdjustments = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  = ?', [responseData, parseInt(MOrderId)]);
                        objMediaOrderLine.MediaOrderLineId = objCurrentAdAdjustments[0].MediaOrderLineId;

                        // objMediaOrderLine.MediaOrderLineId = result.data.MediaOrderLines[0].MediaOrderLineId;
                        // for multiple delete
                        if ($scope.isMultiple == true) {
                            var multipleMediaOrderLines = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  != ?', [responseData, parseInt(objMediaOrderLine.MediaOrderId)]);
                            angular.forEach(multipleMediaOrderLines, function (data, key) {
                                data.AdAdjustmentId = objMediaOrderLine.AdAdjustmentId;
                                data.tempAdjustmentAmount = objMediaOrderLine.tempAdjustmentAmount;
                                data.tempRunningAmount = objMediaOrderLine.tempRunningAmount;
                                $scope.tempMediaOrderLinesforNetMultiple.push(data);
                            });
                        }

                        $scope.netAdAdjustments.push(objMediaOrderLine);
                        // $scope.selectedIssueMediaOrderDataDownload.mediaIssueDateWiseAdjustments = objMediaOrderLine;
                        var grossCost = result.data.Cost.GrossCost;
                        var netCost = result.data.Cost.NetCost;
                        $rootScope.GrossCost = (grossCost).toFixed(2);
                        $rootScope.NetCost = (netCost).toFixed(2);
                        getNoOfIssueMediaOrder();

                        if ($scope.isMultiple == true) {
                            $scope.isShowDefaultNetAdjustmentTable = true;
                            $scope.isShowDefaultNetAdjustmentTableM = false;
                            changeSelect2DropDownColor("s2id_ddlNetAdAdjustmentM", "");
                        } else {
                            changeSelect2DropDownColor("s2id_ddlNetAdAdjustment", "");
                            $scope.isShowDefaultNetAdjustmentTable = false;
                        }

                        //$scope.isShowDefaultNetAdjustmentTable = false;
                        clearNetAmountSection();
                        $scope.NetAdAdjustmentModalForm.$setPristine();
                        $scope.NetAdjustmentshowMsgs = false;
                        toastr.success('Save successfully.', 'Success!');
                    }
                    else {
                        toastr.error(result.message, 'Error!');
                    }
                })
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function clearNetAmountSection() {
        try {
            $scope.netAdAdjustment.AdAdjustmentId = "";
            $scope.netAdAdjustment.AdAdjustmentName = "";
            $scope.netAdAdjustment.AdjustmentValue = "";
            $scope.netAdAdjustment.SequenceNumber = 0;
            $scope.netAdAdjustment.tempAdjustmentAmount = parseFloat(0).toFixed(2);
            $scope.netAdAdjustment.tempRunningAmount = parseFloat(0).toFixed(2);
            $scope.netAdAdjustment.isPercentage = false;
            initSelect2('s2id_ddlNetAdAdjustment', 'Select Ad Adjustment');
            initSelect2('s2id_ddlNetAdAdjustmentM', 'Select Ad Adjustment');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    //checkbox change event
    $scope.chechboxChange = function (objAdAdjustment) {
        try {

            var grossAndNetDiv = angular.element(document.querySelector('#grossAndNetDiv'))
            grossAndNetDiv.addClass('no-click');

            var qId = objAdAdjustment.IssueDateId;
            var mId = objAdAdjustment.MediaAssetId;
            var mOrderId = objAdAdjustment.MediaOrderId;

            //var idx = $scope.selectedCheckbox.indexOf(qId);
            var idx = $scope.selectedCheckbox.indexOf(mOrderId);
            if (idx > -1) {
                $scope.selectedCheckbox.splice(idx, 1);
                angular.forEach($scope.selectedIssueMediaOrderData, function (data, key) {
                    // if (data.IssueDateId == qId) {
                    if (data.MediaOrderId == mOrderId) {
                        if ($scope.selectedIssueMediaOrderData.length == 1) {
                            $scope.tempGrossAmount = parseFloat(0).toFixed(2);
                        }
                        $scope.selectedIssueMediaOrderData.splice(key, 1);
                        $scope.selectedMediaAssetsIds.splice(key, 1);
                        $scope.selectedMediaOrderIds.splice(key, 1);
                    }
                });
                angular.forEach($scope.selectedIssueMediaOrderDataDownload, function (data, key) {
                    // if (data.IssueDateId == qId) {
                    if (data.MediaOrderId == mOrderId) {
                        $scope.selectedIssueMediaOrderDataDownload.splice(key, 1);
                    }
                });
                if ($scope.selectedCheckbox.length < 1) {
                    $scope.tempGrossAmount = parseFloat(0).toFixed(2);

                    $scope.finalGrossAmount = parseFloat(0).toFixed(2);
                    $scope.tempNetAmount = parseFloat(0).toFixed(2);
                    $scope.finalNetAmount = parseFloat(0).toFixed(2);

                    initPageControls();
                    $scope.isShowDefaultGrossAdjustmentTable = true;
                    $scope.isShowDefaultNetAdjustmentTable = true;
                    $scope.grossAdAdjustments = [];
                    $scope.netAdAdjustments = [];
                };
                if ($scope.selectedCheckbox.length == 1) {
                    //called get by api
                    getAdAdjustmentByMediaAssetId();
                    var grossAndNetDiv = angular.element(document.querySelector('#grossAndNetDiv'))
                    grossAndNetDiv.removeClass('no-click');

                    $scope.selectedMediaOrderId = $scope.selectedMediaOrderIds[0];
                    var objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  = ?', [$scope.issueDateWiseAdjustment, parseInt($scope.selectedMediaOrderId)]);
                    //var grossAmount = parseFloat(objSelectedAdAdjustments[0].GrossCost);
                    var grossAmount = parseFloat(objSelectedAdAdjustments[0].RateCardCost);
                    $scope.tempGrossAmount = parseFloat(0);
                    $scope.tempGrossAmount += grossAmount;
                    var initialAmount = ($scope.tempGrossAmount).toFixed(2);
                    $scope.tempGrossAmount = initialAmount;
                    $scope.finalGrossAmount = initialAmount;
                    $scope.tempNetAmount = initialAmount;

                    var objSelectedAdAdjustmentsforNet = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  = ?', [$scope.issueDateWiseAdjustment, parseInt($scope.selectedMediaOrderId)]);
                    var netAmount = parseFloat(objSelectedAdAdjustmentsforNet[0].NetCost);
                    //$scope.tempNetAmount = parseFloat(0);
                    //$scope.tempNetAmount += netAmount;
                    // var initialNetAmount = ($scope.tempNetAmount).toFixed(2);
                    var zeroAmount = 0;
                    var netAmountinDecimal = parseFloat(netAmount + zeroAmount);
                    netAmountinDecimal = netAmountinDecimal.toFixed(2);
                    $scope.finalNetAmount = netAmountinDecimal;

                    getMediaOrderLinesByMediaOrder(parseInt($scope.selectedMediaOrderIds[0]));
                }
            }
            else {
                //$scope.selectedCheckbox.push(qId);
                $scope.selectedCheckbox.push(mOrderId);
                $scope.selectedMediaAssetsIds.push(mId);
                $scope.selectedMediaOrderIds.push(mOrderId);
                $scope.selectedIssueMediaOrderData.push(objAdAdjustment);
                // $scope.selectedIssueMediaOrderDataDownload = $scope.selectedIssueMediaOrderData;
                mediaOrderLinesFactory.getMediaOrderLinesbyOrderId(mOrderId, function (result) {
                    if (result.statusCode == 1) {
                        console.log(result);
                        if (result.data != null) {
                            if (result.data.length > 0) {
                                $scope.mediaIssueDateWiseAdjustment = result.data;

                                angular.forEach($scope.mediaIssueDateWiseAdjustment, function (value, key) {
                                    if (value.AdAdjustmentName != null) {
                                        if (key == 0) {
                                            $scope.runningCost = objAdAdjustment.RateCardCost;
                                        }
                                        value.tempAdjustmentAmount = (value.AmountPercent ? ($scope.runningCost * value.AdjustmentValue) / 100 : value.AdjustmentValue);
                                        if (value.SurchargeDiscount == 0) {
                                            value.runningCost = $scope.runningCost + value.tempAdjustmentAmount;
                                        } else {
                                            value.runningCost = $scope.runningCost - value.tempAdjustmentAmount;
                                        }

                                        $scope.runningCost = value.runningCost;

                                    }
                                });
                                //  $scope.tempAdjustment.push($scope.mediaIssueDateWiseAdjustment);
                                objAdAdjustment.mediaIssueDateWiseAdjustments = $scope.mediaIssueDateWiseAdjustment;

                            }
                        }
                    }
                    $scope.selectedIssueMediaOrderDataDownload.push(objAdAdjustment);
                });




                // for API called now called static
                //var orderWiseMediaAssetId = objAdAdjustment.MediaAssetId;
                //getAdAdjustmentByMediaAssetId(orderWiseMediaAssetId);
                //getAdAdjustmentByMediaAssetId();

                if ($scope.selectedCheckbox.length > 1) {
                    $scope.tempGrossAmount = parseFloat(0).toFixed(2);
                    $scope.finalGrossAmount = parseFloat(0).toFixed(2);
                    $scope.tempNetAmount = parseFloat(0).toFixed(2);
                    $scope.finalNetAmount = parseFloat(0).toFixed(2);

                    initPageControls();
                    $scope.isShowDefaultGrossAdjustmentTable = true;
                    $scope.isShowDefaultNetAdjustmentTable = true;
                    $scope.grossAdAdjustments = [];
                    $scope.netAdAdjustments = [];
                }

                if ($scope.selectedCheckbox.length == 1) {
                    //called get api
                    getAdAdjustmentByMediaAssetId();
                    var grossAndNetDiv = angular.element(document.querySelector('#grossAndNetDiv'))
                    grossAndNetDiv.removeClass('no-click');

                    $scope.selectedMediaOrderId = $scope.selectedMediaOrderIds[0];

                    var objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  = ?', [$scope.issueDateWiseAdjustment, parseInt($scope.selectedMediaOrderId)]);
                    //var grossAmount = parseFloat(objSelectedAdAdjustments[0].GrossCost);
                    var grossAmount = parseFloat(objSelectedAdAdjustments[0].RateCardCost);
                    $scope.tempGrossAmount = parseFloat(0);
                    $scope.tempGrossAmount += grossAmount;
                    var initialGrossAmount = ($scope.tempGrossAmount).toFixed(2);
                    $scope.tempGrossAmount = initialGrossAmount;
                    $scope.finalGrossAmount = initialGrossAmount;
                    $scope.tempNetAmount = initialGrossAmount;

                    var objSelectedAdAdjustmentsforNet = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  = ?', [$scope.issueDateWiseAdjustment, parseInt($scope.selectedMediaOrderId)]);
                    var netAmount = parseFloat(objSelectedAdAdjustmentsforNet[0].NetCost);
                    //$scope.tempNetAmount = parseFloat(0);
                    //$scope.tempNetAmount += netAmount;
                    //var initialNetAmount = ($scope.tempNetAmount).toFixed(2);
                    var zeroAmount = 0;
                    var netAmountinDecimal = parseFloat(netAmount + zeroAmount);
                    netAmountinDecimal = netAmountinDecimal.toFixed(2);
                    $scope.finalNetAmount = netAmountinDecimal;

                    getMediaOrderLinesByMediaOrder(parseInt($scope.selectedMediaOrderIds[0]));
                }
            }
            if ($scope.selectedCheckbox.length > 1) {
                $scope.isMultiple = true;
            } else {
                $scope.isMultiple = false;
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    };

    //deleteIssueOrderAdjustment
    $scope.deleteMediaOrderMediaLinesbyMediaOrderId = function (objAdjustment) {
        try {
            //toastr["warning"]("Clear itself?<br /><br /><button type='button' class='btn clear'>Yes</button>")
            bootbox.confirm({
                message: "Are you sure you want to delete the selected object(s) and all of their children?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    // alert(result);
                    if (result) {
                        debugger;
                        var mediaOrderId = objAdjustment.MediaOrderId;
                        if (mediaOrderId != null && mediaOrderId != undefined && mediaOrderId != "") {
                            mediaOrderLinesFactory.deleteMediaOrderLinesbyMediaOrder(mediaOrderId, function (result) {
                                if (result.statusCode == 1) {

                                    if (result.data != null) {
                                        var data = result.data;
                                        var grossCost = data.GrossCost;
                                        var netCost = data.NetCost;
                                        $rootScope.GrossCost = (grossCost).toFixed(2);
                                        $rootScope.NetCost = (netCost).toFixed(2);
                                        getNoOfIssueMediaOrder();

                                        console.log(result);
                                        $scope.isShowDefaultGrossAdjustmentTable = true;
                                        $scope.isShowDefaultNetAdjustmentTable = true;
                                        $scope.grossAdAdjustments = [];
                                        $scope.netAdAdjustments = [];
                                        toastr.success('Deleted successfully.', 'Success!');
                                    }
                                }
                                else {
                                    toastr.error(result.message, 'Error!');
                                }
                            });
                        }
                        else {
                            console.log('media Order Id is null');
                        }
                    }
                }
            });

            //if (objAdjustment.IssueDateId > 0) {
            //    angular.forEach($scope.issueDateWiseAdjustment, function (objIssueDateWiseAdjustment, key) {
            //        if (objIssueDateWiseAdjustment.IssueDateId == objAdjustment.IssueDateId) {
            //            $scope.issueDateWiseAdjustment.splice(key, 1);
            //        }
            //    });
            //}
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    //editIssueOrderAdjustment
    $scope.editMediaOrderMediaLinesbyMediaOrderId = function (MediaOrderId) {
        //$location.path('ordersDetails/mediaSchedule/' + $stateParams.AdOrderId);
        $location.path('ordersDetails/mediaSchedule/' + $stateParams.AdOrderId + '/' + MediaOrderId);
    }

    $scope.onAdjustmentChange = function () {
        try {
            var DropDown = angular.element(document.querySelector('#ddlAdAdjustment'));
            var getSelectedData = DropDown.select2("data");
            var Id = getSelectedData.id;
            var Value = getSelectedData.text;
            var isPopUp = false;
            //if (Id == undefined || Id == "" || Id == 0 || Id == null) {
            //    DropDown = angular.element(document.querySelector('#ddlAdAdjustmentM'));
            //    getSelectedData = DropDown.select2("data");
            //    Id = getSelectedData.id;
            //    Value = getSelectedData.text;
            //    isPopUp = true;
            //}

            if (Id != undefined && Id != "" && Id != 0 && Id != null) {
                if (isPopUp == false) {
                    changeSelect2DropDownColor("s2id_ddlAdAdjustment", Id);
                } else {
                    changeSelect2DropDownColor("s2id_ddlAdAdjustmentM", Id);
                }

                var dataContext = $scope.adAdjustments;
                var objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SrNo  = ?', [dataContext, parseInt(Id)]);
                //var objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE AdAdjustmentName  = ?', [dataContext, parseInt(Id)]);
                var IsAmountPercent = objSelectedAdAdjustments[0].AmountPercent;

                $scope.grossAdAdjustment.AdjustmentValue = objSelectedAdAdjustments[0].AdjustmentValue;
                //if ($scope.isMultiple == true) {
                //    $scope.grossAdjustmentValueM = objSelectedAdAdjustments[0].AdjustmentValue;
                //}
                //else {
                //    $scope.grossAdjustmentValue = objSelectedAdAdjustments[0].AdjustmentValue;
                //}
                if (IsAmountPercent == 0) {
                    $scope.amountType = "Amount";
                    $scope.isPercentage = false;
                    $scope.grossAdAdjustment.AmountPercent = objSelectedAdAdjustments[0].AmountPercent;
                    $scope.grossAdAdjustment.ProductCode = objSelectedAdAdjustments[0].ProductCode;
                    $scope.grossAdAdjustment.SurchargeDiscount = objSelectedAdAdjustments[0].SurchargeDiscount;
                } else if (IsAmountPercent == 1) {
                    $scope.amountType = "Percent";
                    $scope.isPercentage = true;
                    $scope.grossAdAdjustment.AmountPercent = objSelectedAdAdjustments[0].AmountPercent;
                    $scope.grossAdAdjustment.ProductCode = objSelectedAdAdjustments[0].ProductCode;
                    $scope.grossAdAdjustment.SurchargeDiscount = objSelectedAdAdjustments[0].SurchargeDiscount;
                }
            }
            else {
                $scope.grossAdAdjustment.AdjustmentValue = "";
                if (isPopUp == false) {
                    changeSelect2DropDownColor("s2id_ddlAdAdjustment", "");
                } else {
                    changeSelect2DropDownColor("s2id_ddlAdAdjustmentM", "");
                }

            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onAdjustmentChangePopUp = function () {
        try {
            //var DropDown = angular.element(document.querySelector('#ddlAdAdjustment'));
            //var getSelectedData = DropDown.select2("data");
            //var Id = getSelectedData.id;
            //var Value = getSelectedData.text;
            //var isPopUp = false;
            //if (Id == undefined || Id == "" || Id == 0 || Id == null) {
            var DropDown = angular.element(document.querySelector('#ddlAdAdjustmentM'));
            var getSelectedData = DropDown.select2("data");
            var Id = getSelectedData.id;
            var Value = getSelectedData.text;
            var isPopUp = true;
            // }

            if (Id != undefined && Id != "" && Id != 0 && Id != null) {
                if (isPopUp == false) {
                    changeSelect2DropDownColor("s2id_ddlAdAdjustment", Id);
                } else {
                    changeSelect2DropDownColor("s2id_ddlAdAdjustmentM", Id);
                }

                var dataContext = $scope.adAdjustments;
                var objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SrNo  = ?', [dataContext, parseInt(Id)]);
                //var objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE AdAdjustmentName  = ?', [dataContext, parseInt(Id)]);
                var IsAmountPercent = objSelectedAdAdjustments[0].AmountPercent;

                $scope.grossAdAdjustment.AdjustmentValue = objSelectedAdAdjustments[0].AdjustmentValue;
                //if ($scope.isMultiple == true) {
                //    $scope.grossAdjustmentValueM = objSelectedAdAdjustments[0].AdjustmentValue;
                //}
                //else {
                //    $scope.grossAdjustmentValue = objSelectedAdAdjustments[0].AdjustmentValue;
                //}
                if (IsAmountPercent == 0) {
                    $scope.amountType = "Amount";
                    $scope.isPercentage = false;
                    $scope.grossAdAdjustment.AmountPercent = objSelectedAdAdjustments[0].AmountPercent;
                    $scope.grossAdAdjustment.ProductCode = objSelectedAdAdjustments[0].ProductCode;
                    $scope.grossAdAdjustment.SurchargeDiscount = objSelectedAdAdjustments[0].SurchargeDiscount;
                } else if (IsAmountPercent == 1) {
                    $scope.amountType = "Percent";
                    $scope.isPercentage = true;
                    $scope.grossAdAdjustment.AmountPercent = objSelectedAdAdjustments[0].AmountPercent;
                    $scope.grossAdAdjustment.ProductCode = objSelectedAdAdjustments[0].ProductCode;
                    $scope.grossAdAdjustment.SurchargeDiscount = objSelectedAdAdjustments[0].SurchargeDiscount;
                }
            }
            else {
                $scope.grossAdAdjustment.AdjustmentValue = "";
                if (isPopUp == false) {
                    changeSelect2DropDownColor("s2id_ddlAdAdjustment", "");
                } else {
                    changeSelect2DropDownColor("s2id_ddlAdAdjustmentM", "");
                }

            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onNetAdjustmentChangePopUp = function () {
        try {
            //var DropDown = angular.element(document.querySelector('#ddlNetAdAdjustment'));
            //var getSelectedData = DropDown.select2("data");
            //var Id = getSelectedData.id;
            //var Value = getSelectedData.text;
            //var isPopUp = false;
            //if (Id == undefined || Id == "" || Id == 0 || Id == null) {
            var DropDown = angular.element(document.querySelector('#ddlNetAdAdjustmentM'));
            var getSelectedData = DropDown.select2("data");
            var Id = getSelectedData.id;
            var Value = getSelectedData.text;
            var isPopUp = true;
            // }

            if (Id != undefined && Id != "" && Id != 0 && Id != null) {
                if (isPopUp == false) {
                    changeSelect2DropDownColor("s2id_ddlNetAdAdjustment", Id);
                } else {
                    changeSelect2DropDownColor("s2id_ddlNetAdAdjustmentM", Id);
                }

                var dataContext = $scope.adAdjustments;
                var objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SrNo  = ?', [dataContext, parseInt(Id)]);
                // var objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE AdAdjustmentName  = ?', [dataContext, parseInt(Id)]);
                var IsAmountPercent = objSelectedAdAdjustments[0].AmountPercent;
                $scope.netAdAdjustment.AdjustmentValue = objSelectedAdAdjustments[0].AdjustmentValue;
                if (IsAmountPercent == 0) {
                    $scope.netamountType = "Amount";
                    $scope.netisPercentage = false;
                    $scope.netAdAdjustment.AmountPercent = objSelectedAdAdjustments[0].AmountPercent;
                    $scope.netAdAdjustment.ProductCode = objSelectedAdAdjustments[0].ProductCode;
                    $scope.netAdAdjustment.SurchargeDiscount = objSelectedAdAdjustments[0].SurchargeDiscount;
                } else if (IsAmountPercent == 1) {
                    $scope.netamountType = "Percent";
                    $scope.netisPercentage = true;
                    $scope.netAdAdjustment.AmountPercent = objSelectedAdAdjustments[0].AmountPercent;
                    $scope.netAdAdjustment.ProductCode = objSelectedAdAdjustments[0].ProductCode;
                    $scope.netAdAdjustment.SurchargeDiscount = objSelectedAdAdjustments[0].SurchargeDiscount;
                }
            }
            else {
                $scope.netAdAdjustment.AdjustmentValue = "";
                if (isPopUp == false) {
                    changeSelect2DropDownColor("s2id_ddlNetAdAdjustment", "");
                } else {
                    changeSelect2DropDownColor("s2id_ddlNetAdAdjustmentM", "");
                }
                //changeSelect2DropDownColor("s2id_ddlAdAdjustment", "");
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onNetAdjustmentChange = function () {
        try {
            var DropDown = angular.element(document.querySelector('#ddlNetAdAdjustment'));
            var getSelectedData = DropDown.select2("data");
            var Id = getSelectedData.id;
            var Value = getSelectedData.text;
            var isPopUp = false;
            //if (Id == undefined || Id == "" || Id == 0 || Id == null) {
            //    DropDown = angular.element(document.querySelector('#ddlNetAdAdjustmentM'));
            //    getSelectedData = DropDown.select2("data");
            //    Id = getSelectedData.id;
            //    Value = getSelectedData.text;
            //    isPopUp = true;
            //}

            if (Id != undefined && Id != "" && Id != 0 && Id != null) {
                if (isPopUp == false) {
                    changeSelect2DropDownColor("s2id_ddlNetAdAdjustment", Id);
                } else {
                    changeSelect2DropDownColor("s2id_ddlNetAdAdjustmentM", Id);
                }

                var dataContext = $scope.adAdjustments;
                var objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SrNo  = ?', [dataContext, parseInt(Id)]);
                // var objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE AdAdjustmentName  = ?', [dataContext, parseInt(Id)]);
                var IsAmountPercent = objSelectedAdAdjustments[0].AmountPercent;
                $scope.netAdAdjustment.AdjustmentValue = objSelectedAdAdjustments[0].AdjustmentValue;
                if (IsAmountPercent == 0) {
                    $scope.netamountType = "Amount";
                    $scope.netisPercentage = false;
                    $scope.netAdAdjustment.AmountPercent = objSelectedAdAdjustments[0].AmountPercent;
                    $scope.netAdAdjustment.ProductCode = objSelectedAdAdjustments[0].ProductCode;
                    $scope.netAdAdjustment.SurchargeDiscount = objSelectedAdAdjustments[0].SurchargeDiscount;
                } else if (IsAmountPercent == 1) {
                    $scope.netamountType = "Percent";
                    $scope.netisPercentage = true;
                    $scope.netAdAdjustment.AmountPercent = objSelectedAdAdjustments[0].AmountPercent;
                    $scope.netAdAdjustment.ProductCode = objSelectedAdAdjustments[0].ProductCode;
                    $scope.netAdAdjustment.SurchargeDiscount = objSelectedAdAdjustments[0].SurchargeDiscount;
                }
            }
            else {
                $scope.netAdAdjustment.AdjustmentValue = "";
                if (isPopUp == false) {
                    changeSelect2DropDownColor("s2id_ddlNetAdAdjustment", "");
                } else {
                    changeSelect2DropDownColor("s2id_ddlNetAdAdjustmentM", "");
                }
                //changeSelect2DropDownColor("s2id_ddlAdAdjustment", "");
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onGrossChechboxChange = function (objGrossAdjustment, isCheckTrue) {
        try {
            if (isCheckTrue == true) {
                var sNo = objGrossAdjustment.SequenceNumber;
                var molNo = objGrossAdjustment.MediaOrderLineId;
                $scope.selectedGrossAdjustmentCheckbox.push(sNo);
                $scope.selectedGrossAdjustmentMediaOrderLines.push(molNo);

            } else {
                var sNo = objGrossAdjustment.SequenceNumber;
                var molNo = objGrossAdjustment.MediaOrderLineId;
                var idx = $scope.selectedGrossAdjustmentCheckbox.indexOf(sNo);
                var idx1 = $scope.selectedGrossAdjustmentMediaOrderLines.indexOf(molNo);
                $scope.selectedGrossAdjustmentCheckbox.splice(idx, 1);
                $scope.selectedGrossAdjustmentMediaOrderLines.splice(idx1, 1);
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    };

    $scope.onNetChechboxChange = function (objNetAdjustment, isCheckTrue) {
        try {
            if (isCheckTrue == true) {
                var sNo = objNetAdjustment.SequenceNumber;
                var molNo = objNetAdjustment.MediaOrderLineId;
                $scope.selectedNetAdjustmentCheckbox.push(sNo);
                $scope.selectedNetAdjustmentMediaOrderLines.push(molNo);
            } else {
                var sNo = objNetAdjustment.SequenceNumber;
                var molNo = objNetAdjustment.MediaOrderLineId;
                var idx = $scope.selectedNetAdjustmentCheckbox.indexOf(sNo);
                var idx1 = $scope.selectedNetAdjustmentMediaOrderLines.indexOf(molNo);
                $scope.selectedNetAdjustmentCheckbox.splice(idx, 1);
                $scope.selectedNetAdjustmentMediaOrderLines.splice(idx1, 1);
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    };

    // Gross Move and  Delete event
    function createSequenceOrderWiseGrossTable() {
        try {
            var objAllDeletedAdAdjustment = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [$scope.grossAdAdjustments, parseInt(0)]);
            if (objAllDeletedAdAdjustment.length == $scope.grossAdAdjustments.length) {
                $scope.grossAdAdjustments = [];
                $scope.isShowDefaultGrossAdjustmentTable = true;

                var objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  = ?', [$scope.loadTimeIssueDateWiseAdjustment, parseInt($scope.selectedMediaOrderId)]);
                var grossAmount = parseFloat(objSelectedAdAdjustments[0].GrossCost);
                $scope.tempGrossAmount = parseFloat(0);
                $scope.tempGrossAmount += grossAmount;
                var initialGrossAmount = ($scope.tempGrossAmount).toFixed(2);
                $scope.tempGrossAmount = initialGrossAmount;
                $scope.finalGrossAmount = initialGrossAmount;
                $scope.tempNetAmount = initialGrossAmount;
                $scope.finalNetAmount = initialGrossAmount;

                //var objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  = ?', [$scope.issueDateWiseAdjustment, parseInt($scope.selectedMediaOrderId)]);
                //var grossAmount = parseFloat(objSelectedAdAdjustments[0].GrossCost);
                //$scope.tempGrossAmount = parseFloat(0);
                //$scope.tempGrossAmount += grossAmount;
                //var initialGrossAmount = ($scope.tempGrossAmount).toFixed(2);
                //$scope.tempGrossAmount = initialGrossAmount;
                //$scope.finalGrossAmount = initialGrossAmount;
                //$scope.tempNetAmount = initialGrossAmount;

                var netAdjustmentData = $scope.netAdAdjustments;
                angular.forEach(netAdjustmentData, function (data, key) {
                    var objAdAdjustmentName = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [netAdjustmentData, parseInt(key + 1)]);
                    if (key == 0) {
                        $scope.isNetChangedByMove = true;
                    } else {
                        $scope.isNetChangedByMove = false;
                    }
                    if (objAdAdjustmentName.length > 0) {
                        $scope.AdNetMediaOrderdAdjustment(objAdAdjustmentName[0], "editMode");
                    }
                    if (netAdjustmentData.length == 1) {
                        $scope.isNetChangedByMove = false;

                        //$scope.tempNetAmount = initialGrossAmount;
                        //var objSelectedAdAdjustmentsforNet = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  = ?', [$scope.issueDateWiseAdjustment, parseInt($scope.selectedMediaOrderId)]);
                        //var netAmount = parseFloat(objSelectedAdAdjustmentsforNet[0].NetCost);
                        //$scope.tempNetAmount = parseFloat(0);
                        //$scope.tempNetAmount += netAmount;
                        //var initialNetAmount = ($scope.tempNetAmount).toFixed(2);
                        //$scope.finalNetAmount = initialNetAmount;

                        //$scope.isShowDefaultNetAdjustmentTable = true;
                        //$scope.netAdAdjustments = [];
                    }
                    $scope.selectedNetAdjustmentCheckbox = [];
                    $scope.selectedNetAdjustmentMediaOrderLines = [];
                });


                ////$scope.tempGrossAmount = parseFloat(0).toFixed(2);
                //$scope.finalGrossAmount = parseFloat(0).toFixed(2);
                //$scope.tempNetAmount = parseFloat(0).toFixed(2);
                //$scope.finalNetAmount = parseFloat(0).toFixed(2);

                $scope.selectedGrossAdjustmentCheckbox = [];
                $scope.selectedGrossAdjustmentMediaOrderLines = [];
            }
            else {
                var grossAdjustmentData = $scope.grossAdAdjustments;
                angular.forEach(grossAdjustmentData, function (data, key) {
                    var objAdAdjustmentName = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [grossAdjustmentData, parseInt(key + 1)]);
                    if (key == 0) {
                        $scope.isGrossChangedByMoveUp = true;
                    } else {
                        $scope.isGrossChangedByMoveUp = false;
                    }
                    if (objAdAdjustmentName.length > 0) {
                        $scope.AdMediaOrderdAdjustment(objAdAdjustmentName[0], "deleteMode");
                    }

                    if (grossAdjustmentData.length == 1) {
                        $scope.isGrossChangedByMoveUp = false;
                        $scope.isShowDefaultGrossAdjustmentTable = true;

                        //$scope.tempGrossAmount = parseFloat(0).toFixed(2);
                        $scope.finalGrossAmount = parseFloat(0).toFixed(2);
                        $scope.tempNetAmount = parseFloat(0).toFixed(2);
                        $scope.finalNetAmount = parseFloat(0).toFixed(2);

                        $scope.grossAdAdjustments = [];
                    }
                    $scope.selectedGrossAdjustmentCheckbox = [];
                    $scope.selectedGrossAdjustmentMediaOrderLines = [];
                });
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.moveRecordOneRowAbove = function () {
        try {
            if ($scope.selectedGrossAdjustmentCheckbox.length == 1) {
                var currentSequenceNo = parseInt($scope.selectedGrossAdjustmentCheckbox[0]);
                if ($scope.grossAdAdjustments[0].SequenceNumber != currentSequenceNo) {
                    var objCurrentAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [$scope.grossAdAdjustments, parseInt(currentSequenceNo)]);

                    if (objCurrentAdAdjustments.length > 0) {
                        objCurrentAdAdjustments = objCurrentAdAdjustments[0];
                        var obj = {};
                        obj.MediaOrderLineId = objCurrentAdAdjustments.MediaOrderLineId;
                        obj.MediaOrderId = objCurrentAdAdjustments.MediaOrderId;
                        obj.MovedUp = true;

                        mediaOrderLinesFactory.moveUpOrDownMediaOrderLines(obj, function (result) {
                            if (result.statusCode == 1) {
                                console.log(result);
                                if (result.data != null) {
                                    var data = result.data;
                                    var grossCost = data.GrossCost;
                                    var netCost = data.NetCost;
                                    $rootScope.GrossCost = (grossCost).toFixed(2);
                                    $rootScope.NetCost = (netCost).toFixed(2);
                                    getNoOfIssueMediaOrder();
                                    moveOneStepAboveforGross();
                                }
                            } else {
                                toastr.error(result.message, 'Error!');
                            }
                        });
                    }
                }
            }
            else {
                toastr.info('Please Select One Ad Adjustment', 'Information!');
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.moveRecordOneRowBelow = function () {
        try {
            if ($scope.selectedGrossAdjustmentCheckbox.length == 1) {
                var currentSequenceNo = parseInt($scope.selectedGrossAdjustmentCheckbox[0]);
                var grossAdAdjustmentsLength = $scope.grossAdAdjustments.length - 1;
                if ($scope.grossAdAdjustments[grossAdAdjustmentsLength].SequenceNumber != currentSequenceNo) {
                    var objCurrentAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [$scope.grossAdAdjustments, parseInt(currentSequenceNo)]);

                    if (objCurrentAdAdjustments.length > 0) {
                        objCurrentAdAdjustments = objCurrentAdAdjustments[0];
                        var obj = {};
                        obj.MediaOrderLineId = objCurrentAdAdjustments.MediaOrderLineId;
                        obj.MediaOrderId = objCurrentAdAdjustments.MediaOrderId;
                        obj.MovedUp = false;

                        mediaOrderLinesFactory.moveUpOrDownMediaOrderLines(obj, function (result) {
                            if (result.statusCode == 1) {
                                console.log(result);
                                if (result.data != null) {
                                    var data = result.data;
                                    var grossCost = data.GrossCost;
                                    var netCost = data.NetCost;
                                    $rootScope.GrossCost = (grossCost).toFixed(2);
                                    $rootScope.NetCost = (netCost).toFixed(2);
                                    getNoOfIssueMediaOrder();
                                    moveOneStepBelowforGross();
                                }
                            } else {
                                toastr.error(result.message, 'Error!');
                            }
                        });
                    }
                }
            }
            else {
                toastr.info('Please Select One Ad Adjustment', 'Information!');
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.deleteSelectedRecord = function () {
        try {


            if ($scope.selectedGrossAdjustmentCheckbox.length > 0) {
                bootbox.confirm({
                    message: "Are you sure you want to delete the selected object(s) and all of their children?",
                    buttons: {
                        confirm: {
                            label: 'Yes',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: 'No',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            if ($scope.selectedGrossAdjustmentMediaOrderLines.length > 0) {

                                //if ($scope.selectedGrossAdjustmentMediaOrderLines.length == $scope.grossAdAdjustments.length) {
                                //    var id = $scope.selectedGrossAdjustmentMediaOrderLines[0];
                                //    var objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE MediaOrderLineId  = ?', [$scope.grossAdAdjustments, parseInt(id)]);
                                //    $scope.deleteMediaOrderMediaLinesbyMediaOrderId(objSelectedAdAdjustments[0]);
                                //    getNoOfIssueMediaOrder();
                                //    deleteMediaOrderLinesForGrost();
                                //}
                                //else {
                                // multiple
                                var multipleOrderLinesIds = [];
                                var extraOrderlinesToRemove = [];
                                angular.forEach($scope.selectedGrossAdjustmentMediaOrderLines, function (data, key) {
                                    multipleOrderLinesIds.push(data);
                                    //if ($scope.isMultiple == true) {
                                    //    var selectedAdjustmentName = alasql('SELECT * FROM ? AS add WHERE MediaOrderLineId  = ?', [$scope.grossAdAdjustments, parseInt($scope.selectedGrossAdjustmentMediaOrderLines[key])]);
                                    //    if (selectedAdjustmentName.length > 0) {
                                    //        var name = selectedAdjustmentName[0].AdAdjustmentName;
                                    //        var id = selectedAdjustmentName[0].AdAdjustmentId;
                                    //        var tempAdAmount = selectedAdjustmentName[0].tempAdjustmentAmount;
                                    //        var temprunAmount = selectedAdjustmentName[0].tempRunningAmount;

                                    //        var OrderlinesToRemove = alasql('SELECT * FROM ? AS add WHERE AdAdjustmentName  = ? AND  AdAdjustmentId = ?' +
                                    //            'AND GrossInd = ? AND tempAdjustmentAmount = ? AND tempRunningAmount = ?',
                                    //            [$scope.tempMediaOrderLinesforGrossMultiple, name, id, true, tempAdAmount, temprunAmount]);;
                                    //        if (OrderlinesToRemove.length > 0) {
                                    //            angular.forEach(OrderlinesToRemove, function (data1, key1) {
                                    //                extraOrderlinesToRemove.push(data1);
                                    //            });
                                    //        }
                                    //    }
                                    //}
                                });
                                //if ($scope.isMultiple == true) {
                                //    angular.forEach(extraOrderlinesToRemove, function (data, key) {
                                //        multipleOrderLinesIds.push(data.MediaOrderLineId);
                                //    });
                                //}
                                var obj = {};
                                obj.MediaOrderLineIds = multipleOrderLinesIds;

                                // single
                                //var obj = {};
                                //obj.MediaOrderLineIds = $scope.selectedGrossAdjustmentMediaOrderLines;
                                mediaOrderLinesFactory.deleteMediaOrderLinesbyMediaOrderLineId(obj, function (result) {
                                    if (result.statusCode == 1) {
                                        console.log(result);
                                        if (result.data != null) {
                                            var data = result.data;
                                            var grossCost = data.GrossCost;
                                            var netCost = data.NetCost;
                                            //if ($scope.selectedGrossAdjustmentMediaOrderLines.length == $scope.grossAdAdjustments.length) {
                                            //    $scope.allGrossRecordsDeleted = true;
                                            //}
                                            getNoOfIssueMediaOrder();
                                            deleteMediaOrderLinesForGrost();
                                            //angular.forEach($scope.tempMediaOrderLinesforGrossMultiple, function (data, key) {
                                            //    angular.forEach(extraOrderlinesToRemove, function (data1, key1) {
                                            //        if (data.MediaOrderLineId == data1.MediaOrderLineId) {
                                            //            $scope.tempMediaOrderLinesforGrossMultiple.splice(key, 1);
                                            //            extraOrderlinesToRemove.splice(key1, 1);
                                            //        }
                                            //    });
                                            //});
                                            $rootScope.GrossCost = (grossCost).toFixed(2);
                                            $rootScope.NetCost = (netCost).toFixed(2);
                                            toastr.success('delete successfully.', 'Success!');
                                        }
                                    } else {
                                        toastr.error(result.message, 'Error!');
                                    }
                                });
                                // }
                            }
                        }
                    }
                });


            }
            else {
                toastr.info('Please Select One Ad Adjustment', 'Information!');
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function deleteMediaOrderLinesForGrost() {
        try {
            var objCurrentAdAdjustments = {};
            var sequenceMinus = 0;
            angular.forEach($scope.grossAdAdjustments, function (data, key) {
                var isExits = false;
                var currentSequenceNo = parseInt(key + 1);
                objCurrentAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [$scope.grossAdAdjustments, parseInt(currentSequenceNo)]);
                if (objCurrentAdAdjustments.length > 0) {
                    var sNo = parseInt(objCurrentAdAdjustments[0].SequenceNumber);
                    angular.forEach($scope.selectedGrossAdjustmentCheckbox, function (data, key) {
                        var cNo = parseInt(data);
                        if (cNo == sNo) {
                            sequenceMinus++;
                            isExits = true;
                        }
                    });
                    if (isExits == false) {
                        angular.forEach($scope.grossAdAdjustments, function (data1, key1) {
                            var currentNo = currentSequenceNo;
                            var LessNo = parseInt(currentNo - sequenceMinus);
                            //if (sequenceMinus > 0) {
                            if (data1.SequenceNumber == currentNo) {
                                $scope.grossAdAdjustments[key1].SequenceNumber = LessNo;
                            }
                            //}
                        });
                    } else {
                        angular.forEach($scope.grossAdAdjustments, function (data1, key1) {
                            var currentNo = currentSequenceNo;
                            if (sequenceMinus > 0) {
                                if (data1.SequenceNumber == currentNo) {
                                    $scope.grossAdAdjustments[key1].SequenceNumber = 0;
                                }
                            }
                        });
                    }
                }
            });
            createSequenceOrderWiseGrossTable();
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function moveOneStepAboveforGross() {
        try {
            var currentSequenceNo = parseInt($scope.selectedGrossAdjustmentCheckbox[0]);
            if ($scope.grossAdAdjustments[0].SequenceNumber != currentSequenceNo) {
                var previousSequenceNo = parseInt(currentSequenceNo - 1);

                var objCurrentAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [$scope.grossAdAdjustments, parseInt(currentSequenceNo)]);
                var objPreviousAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [$scope.grossAdAdjustments, parseInt(previousSequenceNo)]);

                angular.forEach($scope.grossAdAdjustments, function (data, key) {
                    var onceUpdateed = false;
                    if (data.SequenceNumber == currentSequenceNo) {
                        $scope.grossAdAdjustments[key].SequenceNumber = previousSequenceNo;
                        onceUpdateed = true;
                    }
                    if (onceUpdateed != true) {
                        if (data.SequenceNumber == previousSequenceNo) {
                            $scope.grossAdAdjustments[key].SequenceNumber = currentSequenceNo;
                        }
                    }
                });
                createSequenceOrderWiseGrossTable();
            }
            else {
                toastr.info('You can not move this records one step above', 'Information!');
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function moveOneStepBelowforGross() {
        try {
            var currentSequenceNo = parseInt($scope.selectedGrossAdjustmentCheckbox[0]);
            var grossAdAdjustmentsLength = $scope.grossAdAdjustments.length - 1;
            if ($scope.grossAdAdjustments[grossAdAdjustmentsLength].SequenceNumber != currentSequenceNo) {
                var nextSequenceNo = parseInt(currentSequenceNo + 1);

                var objCurrentAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [$scope.grossAdAdjustments, parseInt(currentSequenceNo)]);
                var objPreviousAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [$scope.grossAdAdjustments, parseInt(nextSequenceNo)]);

                angular.forEach($scope.grossAdAdjustments, function (data, key) {
                    var onceUpdateed = false;
                    if (data.SequenceNumber == currentSequenceNo) {
                        $scope.grossAdAdjustments[key].SequenceNumber = nextSequenceNo;
                        onceUpdateed = true;
                    }
                    if (onceUpdateed != true) {
                        if (data.SequenceNumber == nextSequenceNo) {
                            $scope.grossAdAdjustments[key].SequenceNumber = currentSequenceNo;
                        }
                    }
                });

                createSequenceOrderWiseGrossTable();

            }
            else {
                toastr.info('You can not move this records one step below', 'Information!');
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    // Net Move and Gross event
    function createSequenceOrderWiseNetTable() {
        try {
            var objAllDeletedAdAdjustment = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [$scope.netAdAdjustments, parseInt(0)]);
            if (objAllDeletedAdAdjustment.length == $scope.netAdAdjustments.length) {
                $scope.netAdAdjustments = [];
                $scope.isShowDefaultNetAdjustmentTable = true;
                ////$scope.tempNetAmount = parseFloat(0).toFixed(2);
                //$scope.finalNetAmount = parseFloat(0).toFixed(2);

                $scope.selectedMediaOrderId = $scope.selectedMediaOrderIds[0];
                var grossAdjustmentData = $scope.grossAdAdjustments;
                if (grossAdjustmentData.length == 0) {
                    var objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  = ?', [$scope.issueDateWiseAdjustment, parseInt($scope.selectedMediaOrderId)]);
                    var grossAmount = parseFloat(objSelectedAdAdjustments[0].GrossCost);
                    $scope.tempGrossAmount = parseFloat(0);
                    $scope.tempGrossAmount += grossAmount;
                    var initialAmount = ($scope.tempGrossAmount).toFixed(2);
                    $scope.tempGrossAmount = initialAmount;
                    $scope.finalGrossAmount = initialAmount;
                    $scope.tempNetAmount = initialAmount;
                }

                //var objSelectedAdAdjustmentsforNet = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  = ?', [$scope.issueDateWiseAdjustment, parseInt($scope.selectedMediaOrderId)]);
                //var netAmount = parseFloat(objSelectedAdAdjustmentsforNet[0].NetCost);
                //$scope.tempNetAmount = parseFloat(0);
                //$scope.tempNetAmount += netAmount;
                //var initialNetAmount = ($scope.tempNetAmount).toFixed(2);
                $scope.tempNetAmount = $scope.finalGrossAmount;
                $scope.finalNetAmount = $scope.finalGrossAmount;

                $scope.selectedNetAdjustmentCheckbox = [];
                $scope.selectedNetAdjustmentMediaOrderLines = [];
            }
            else {
                var netAdjustmentData = $scope.netAdAdjustments;
                angular.forEach(netAdjustmentData, function (data, key) {
                    var objAdAdjustmentName = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [netAdjustmentData, parseInt(key + 1)]);
                    if (key == 0) {
                        $scope.isNetChangedByMove = true;
                    } else {
                        $scope.isNetChangedByMove = false;
                    }
                    if (objAdAdjustmentName.length > 0) {
                        $scope.AdNetMediaOrderdAdjustment(objAdAdjustmentName[0], "editMode");
                    }
                    if (netAdjustmentData.length == 1) {
                        $scope.isNetChangedByMove = false;
                        //$scope.tempNetAmount = parseFloat(0).toFixed(2);
                        $scope.finalNetAmount = parseFloat(0).toFixed(2);
                        $scope.isShowDefaultNetAdjustmentTable = true;
                        $scope.netAdAdjustments = [];
                    }
                    $scope.selectedNetAdjustmentCheckbox = [];
                    $scope.selectedNetAdjustmentMediaOrderLines = [];
                });
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.moveOneNetRecordAbove = function () {
        try {
            if ($scope.selectedNetAdjustmentCheckbox.length == 1) {
                var currentSequenceNo = parseInt($scope.selectedNetAdjustmentCheckbox[0]);
                if ($scope.netAdAdjustments[0].SequenceNumber != currentSequenceNo) {
                    var objCurrentAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [$scope.netAdAdjustments, parseInt(currentSequenceNo)]);

                    if (objCurrentAdAdjustments.length > 0) {
                        objCurrentAdAdjustments = objCurrentAdAdjustments[0];
                        var obj = {};
                        obj.MediaOrderLineId = objCurrentAdAdjustments.MediaOrderLineId;
                        obj.MediaOrderId = objCurrentAdAdjustments.MediaOrderId;
                        obj.MovedUp = true;

                        mediaOrderLinesFactory.moveUpOrDownMediaOrderLines(obj, function (result) {
                            if (result.statusCode == 1) {
                                console.log(result);
                                if (result.data != null) {
                                    var data = result.data;
                                    var grossCost = data.GrossCost;
                                    var netCost = data.NetCost;
                                    $rootScope.GrossCost = (grossCost).toFixed(2);
                                    $rootScope.NetCost = (netCost).toFixed(2);
                                    getNoOfIssueMediaOrder();
                                    moveOneStepAboveforNet();
                                }
                            } else {
                                toastr.error(result.message, 'Error!');
                            }
                        });
                    }
                }
            }
            else {
                toastr.info('Please Select One Ad Adjustment', 'Information!');
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.moveOneNetRecordBelow = function () {
        try {
            if ($scope.selectedNetAdjustmentCheckbox.length == 1) {
                var currentSequenceNo = parseInt($scope.selectedNetAdjustmentCheckbox[0]);
                var netAdAdjustmentsLength = $scope.netAdAdjustments.length - 1;
                if ($scope.netAdAdjustments[netAdAdjustmentsLength].SequenceNumber != currentSequenceNo) {
                    var objCurrentAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [$scope.netAdAdjustments, parseInt(currentSequenceNo)]);

                    if (objCurrentAdAdjustments.length > 0) {
                        objCurrentAdAdjustments = objCurrentAdAdjustments[0];
                        var obj = {};
                        obj.MediaOrderLineId = objCurrentAdAdjustments.MediaOrderLineId;
                        obj.MediaOrderId = objCurrentAdAdjustments.MediaOrderId;
                        obj.MovedUp = false;

                        mediaOrderLinesFactory.moveUpOrDownMediaOrderLines(obj, function (result) {
                            if (result.statusCode == 1) {
                                console.log(result);
                                if (result.data != null) {
                                    var data = result.data;
                                    var grossCost = data.GrossCost;
                                    var netCost = data.NetCost;
                                    $rootScope.GrossCost = (grossCost).toFixed(2);
                                    $rootScope.NetCost = (netCost).toFixed(2);
                                    getNoOfIssueMediaOrder();
                                    moveOneStepBelowforNet();
                                }
                            } else {
                                toastr.error(result.message, 'Error!');
                            }
                        });
                    }
                }
            }
            else {
                toastr.info('Please Select One Ad Adjustment', 'Information!');
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.deleteSelectedNetRecord = function () {
        try {
            if ($scope.selectedNetAdjustmentCheckbox.length > 0) {
                bootbox.confirm({
                    message: "Are you sure you want to delete the selected object(s) and all of their children?",
                    buttons: {
                        confirm: {
                            label: 'Yes',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: 'No',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            if ($scope.selectedNetAdjustmentMediaOrderLines.length > 0) {
                                //if ($scope.selectedNetAdjustmentMediaOrderLines.length == $scope.netAdAdjustments.length) {
                                //    var id = $scope.selectedNetAdjustmentMediaOrderLines[0];
                                //    var objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE MediaOrderLineId  = ?', [$scope.netAdAdjustments, parseInt(id)]);
                                //    $scope.deleteMediaOrderMediaLinesbyMediaOrderId(objSelectedAdAdjustments);
                                //    getNoOfIssueMediaOrder();
                                //    deleteMediaOrderLinesForGrost();
                                //}
                                //else {
                                // multiple
                                var multipleOrderLinesIds = [];
                                var extraOrderlinesToRemove = [];
                                angular.forEach($scope.selectedNetAdjustmentMediaOrderLines, function (data, key) {
                                    multipleOrderLinesIds.push(data);

                                    //if ($scope.isMultiple == true) {
                                    //    var selectedAdjustmentName = alasql('SELECT * FROM ? AS add WHERE MediaOrderLineId  = ?', [$scope.netAdAdjustments, parseInt($scope.selectedNetAdjustmentMediaOrderLines[key])]);
                                    //    if (selectedAdjustmentName.length > 0) {
                                    //        var name = selectedAdjustmentName[0].AdAdjustmentName;
                                    //        var id = selectedAdjustmentName[0].AdAdjustmentId;
                                    //        var tempAdAmount = selectedAdjustmentName[0].tempAdjustmentAmount;
                                    //        var temprunAmount = selectedAdjustmentName[0].tempRunningAmount;

                                    //        var OrderlinesToRemove = alasql('SELECT * FROM ? AS add WHERE AdAdjustmentName  = ? AND  AdAdjustmentId = ?' +
                                    //            'AND GrossInd = ? AND tempAdjustmentAmount = ? AND tempRunningAmount = ?',
                                    //            [$scope.tempMediaOrderLinesforNetMultiple, name, id, true, tempAdAmount, temprunAmount]);;
                                    //        if (OrderlinesToRemove.length > 0) {
                                    //            angular.forEach(OrderlinesToRemove, function (data1, key1) {
                                    //                extraOrderlinesToRemove.push(data1);
                                    //            });
                                    //        }
                                    //    }
                                    //}
                                });
                                //if ($scope.isMultiple == true) {
                                //    angular.forEach(extraOrderlinesToRemove, function (data, key) {
                                //        multipleOrderLinesIds.push(data.MediaOrderLineId);
                                //    });
                                //}
                                var obj = {};
                                obj.MediaOrderLineIds = multipleOrderLinesIds;


                                //var obj = {};
                                //obj.MediaOrderLineIds = $scope.selectedNetAdjustmentMediaOrderLines;
                                mediaOrderLinesFactory.deleteMediaOrderLinesbyMediaOrderLineId(obj, function (result) {
                                    if (result.statusCode == 1) {
                                        console.log(result);
                                        if (result.data != null) {
                                            var data = result.data;
                                            var grossCost = data.GrossCost;
                                            var netCost = data.NetCost;
                                            $rootScope.GrossCost = (grossCost).toFixed(2);
                                            $rootScope.NetCost = (netCost).toFixed(2);

                                            //angular.forEach($scope.tempMediaOrderLinesforNetMultiple, function (data, key) {
                                            //    angular.forEach(extraOrderlinesToRemove, function (data1, key1) {
                                            //        if (data.MediaOrderLineId == data1.MediaOrderLineId) {
                                            //            $scope.tempMediaOrderLinesforNetMultiple.splice(key, 1);
                                            //            extraOrderlinesToRemove.splice(key1, 1);
                                            //        }
                                            //    });
                                            //});

                                            getNoOfIssueMediaOrder();
                                            deleteMediaOrderLinesForNet();
                                            toastr.success('delete successfully.', 'Success!');
                                        }
                                    } else {
                                        toastr.error(result.message, 'Error!');
                                    }
                                });
                                //}
                            }
                        }
                    }
                });

            }
            else {
                toastr.info('Please Select  Ad Adjustment', 'Information!');
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function deleteMediaOrderLinesForNet() {
        try {
            var objCurrentAdAdjustments = {};
            var sequenceMinus = 0;
            angular.forEach($scope.netAdAdjustments, function (data, key) {
                var isExits = false;
                var currentSequenceNo = parseInt(key + 1);
                objCurrentAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [$scope.netAdAdjustments, parseInt(currentSequenceNo)]);
                if (objCurrentAdAdjustments.length > 0) {
                    var sNo = parseInt(objCurrentAdAdjustments[0].SequenceNumber);
                    angular.forEach($scope.selectedNetAdjustmentCheckbox, function (data, key) {
                        var cNo = parseInt(data);
                        if (cNo == sNo) {
                            sequenceMinus++;
                            isExits = true;
                        }
                    });
                    if (isExits == false) {
                        angular.forEach($scope.netAdAdjustments, function (data1, key1) {
                            var currentNo = currentSequenceNo;
                            var LessNo = parseInt(currentNo - sequenceMinus);
                            //if (sequenceMinus > 0) {
                            if (data1.SequenceNumber == currentNo) {
                                $scope.netAdAdjustments[key1].SequenceNumber = LessNo;
                            }
                            //}
                        });
                    } else {
                        angular.forEach($scope.netAdAdjustments, function (data1, key1) {
                            var currentNo = currentSequenceNo;
                            if (sequenceMinus > 0) {
                                if (data1.SequenceNumber == currentNo) {
                                    $scope.netAdAdjustments[key1].SequenceNumber = 0;
                                }
                            }
                        });
                    }
                }
            });

            createSequenceOrderWiseNetTable();
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function moveOneStepAboveforNet() {
        try {
            var currentSequenceNo = parseInt($scope.selectedNetAdjustmentCheckbox[0]);
            if ($scope.netAdAdjustments[0].SequenceNumber != currentSequenceNo) {

                var previousSequenceNo = parseInt(currentSequenceNo - 1);
                var objCurrentAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [$scope.netAdAdjustments, parseInt(currentSequenceNo)]);
                var objPreviousAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [$scope.netAdAdjustments, parseInt(previousSequenceNo)]);

                angular.forEach($scope.netAdAdjustments, function (data, key) {
                    var onceUpdateed = false;
                    if (data.SequenceNumber == currentSequenceNo) {
                        $scope.netAdAdjustments[key].SequenceNumber = previousSequenceNo;
                        onceUpdateed = true;
                    }
                    if (onceUpdateed != true) {
                        if (data.SequenceNumber == previousSequenceNo) {
                            $scope.netAdAdjustments[key].SequenceNumber = currentSequenceNo;
                        }
                    }
                });

                createSequenceOrderWiseNetTable();

            }
            else {
                toastr.info('You can not move this records one step above', 'Information!');
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function moveOneStepBelowforNet() {
        try {
            var currentSequenceNo = parseInt($scope.selectedNetAdjustmentCheckbox[0]);
            var netAdAdjustmentsLength = $scope.netAdAdjustments.length - 1;
            if ($scope.netAdAdjustments[netAdAdjustmentsLength].SequenceNumber != currentSequenceNo) {
                var nextSequenceNo = parseInt(currentSequenceNo + 1);

                var objCurrentAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [$scope.netAdAdjustments, parseInt(currentSequenceNo)]);
                var objPreviousAdAdjustments = alasql('SELECT * FROM ? AS add WHERE SequenceNumber  = ?', [$scope.netAdAdjustments, parseInt(nextSequenceNo)]);

                angular.forEach($scope.netAdAdjustments, function (data, key) {
                    var onceUpdateed = false;
                    if (data.SequenceNumber == currentSequenceNo) {
                        $scope.netAdAdjustments[key].SequenceNumber = nextSequenceNo;
                        onceUpdateed = true;
                    }
                    if (onceUpdateed != true) {
                        if (data.SequenceNumber == nextSequenceNo) {
                            $scope.netAdAdjustments[key].SequenceNumber = currentSequenceNo;
                        }
                    }
                });

                createSequenceOrderWiseNetTable();

            }
            else {
                toastr.info('You can not move this records one step below', 'Information!');
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    //init Pop up
    $scope.InitAdAdjustmentPopUpforMultiple = function () {
        try {
            // now it is static
            //var id = $scope.selectedIssueMediaOrderData[0].MediaAssetId;
            //getAdAdjustmentByMediaAssetIdforMultiple(id);
            getAdAdjustmentByMediaAssetId();
            $scope.isMultiple = true;
            $scope.isShowDefaultGrossAdjustmentTable = true;
            $scope.isShowDefaultNetAdjustmentTable = true;
            initSelect2('s2id_ddlAdAdjustmentM', 'Select Ad Adjustment');
            initSelect2('s2id_ddlNetAdAdjustmentM', 'Select Ad Adjustment');

            $scope.multipleGrossAdAdjustmentModalForm.$setPristine();
            $scope.multipleGrossAdjustmentshowMsgs = false;

            $scope.multipleNetAdAdjustmentModalForm.$setPristine();
            $scope.multipleNetAdjustmentshowMsgs = false;

            $scope.isShowDefaultGrossAdjustmentTableM = true;
            $scope.isShowDefaultNetAdjustmentTableM = true;

            initPageControls();
            //
            //$scope.selectedMediaOrderId = $scope.selectedMediaOrderIds[0];
            //var objSelectedAdAdjustments = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  = ?', [$scope.issueDateWiseAdjustment, parseInt($scope.selectedMediaOrderId)]);
            //var grossAmount = parseFloat(objSelectedAdAdjustments[0].GrossCost);
            //$scope.tempGrossAmount = parseFloat(0);
            //$scope.tempGrossAmount += grossAmount;
            //var initialAmount = ($scope.tempGrossAmount).toFixed(2);
            //$scope.tempGrossAmount = initialAmount;
            //$scope.finalGrossAmount = initialAmount;
            //$scope.tempNetAmount = initialGrossAmount;

            //var objSelectedAdAdjustmentsforNet = alasql('SELECT * FROM ? AS add WHERE MediaOrderId  = ?', [$scope.issueDateWiseAdjustment, parseInt($scope.selectedMediaOrderId)]);
            //var netAmount = parseFloat(objSelectedAdAdjustmentsforNet[0].NetCost);
            //$scope.tempNetAmount = parseFloat(0);
            //$scope.tempNetAmount += netAmount;
            //var initialNetAmount = ($scope.tempNetAmount).toFixed(2);
            //$scope.finalNetAmount = initialNetAmount;
            //

            var totalSum = alasql('SELECT sum(GrossCost) FROM ? AS add ', [$scope.selectedIssueMediaOrderData]);
            var grossAmount = parseFloat(totalSum[0]["SUM(GrossCost)"]);
            //if ($scope.tempGrossAmount == "0.00") {
            $scope.tempGrossAmount = parseFloat(0);
            $scope.tempGrossAmount += grossAmount;
            var initialAmount = ($scope.tempGrossAmount).toFixed(2);
            $scope.tempGrossAmount = initialAmount;
            $scope.finalGrossAmount = initialAmount;
            $scope.tempNetAmount = initialAmount;

            var objSelectedAdAdjustmentsforNet = alasql('SELECT sum(NetCost) FROM ? AS add ', [$scope.selectedIssueMediaOrderData]);
            var netAmount = parseFloat(objSelectedAdAdjustmentsforNet[0]["SUM(NetCost)"]);
            var netAmountinDecimal = (netAmount).toFixed(2);
            $scope.finalNetAmount = netAmountinDecimal;

            $scope.isMultipleIssueDate = true;
            //}
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.CancelMultipleAdAdjustment = function () {
        try {
            //   $scope.isMultiple = false;
            $scope.isMultiple = true
            $scope.tempGrossAmount = (0).toFixed(2);
            $scope.finalGrossAmount = parseFloat(0).toFixed(2);
            $scope.tempNetAmount = parseFloat(0).toFixed(2);
            $scope.finalNetAmount = parseFloat(0).toFixed(2);
            $scope.tempMediaOrderLinesforGrossMultiple = [];
            $scope.multipleGrossAdAdjustmentModalForm.$setPristine();
            $scope.multipleGrossAdjustmentshowMsgs = false;

            $scope.multipleNetAdAdjustmentModalForm.$setPristine();
            $scope.multipleNetAdjustmentshowMsgs = false;

            $scope.isShowDefaultGrossAdjustmentTable = true;
            $scope.isShowDefaultNetAdjustmentTable = true;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.UpdateOrderType = function (objOrderType) {
        try {
            var issueDateWiseAdjustment = $scope.issueDateWiseAdjustment.length;
            if (issueDateWiseAdjustment > 0) {
                if ($scope.selectedCheckbox.length > 0) {
                    var ids = $scope.selectedMediaOrderIds;
                    if (ids.length > 0) {
                        var obj = {};
                        obj.MediaOrderIds = $scope.selectedMediaOrderIds;
                        obj.OrderStatus = objOrderType;

                        mediaOrdersFactory.updateMediaOrderStatus(obj, function (result) {
                            if (result.statusCode == 1) {
                                console.log(result);
                                getNoOfIssueMediaOrder();

                            } else {
                                toastr.error(result.message, 'Error!');
                            }
                        });
                    }
                }
                else {
                    toastr.info('Please select at least one media order', 'Information!');
                }
            }
            //var issueDateWiseAdjustment = $scope.issueDateWiseAdjustment.length;
            //if (issueDateWiseAdjustment > 0) {

            //    angular.forEach($scope.issueDateWiseAdjustment, function (data, key) {
            //        $scope.issueDateWiseAdjustment[key].OrderStatus = objOrderType;
            //    });
            //}

        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    var grossAndNetModal = angular.element(document.querySelector('#multipleAdAdjustmentModal'))
    grossAndNetModal.on('hide.bs.modal', function (event) {
        $scope.isMultiple = true
        $scope.tempGrossAmount = (0).toFixed(2);
        $scope.finalGrossAmount = parseFloat(0).toFixed(2);
        $scope.tempNetAmount = parseFloat(0).toFixed(2);
        $scope.finalNetAmount = parseFloat(0).toFixed(2);
        $scope.tempMediaOrderLinesforGrossMultiple = [];
        $scope.multipleGrossAdAdjustmentModalForm.$setPristine();
        $scope.multipleGrossAdjustmentshowMsgs = false;

        $scope.multipleNetAdAdjustmentModalForm.$setPristine();
        $scope.multipleNetAdjustmentshowMsgs = false;

        $scope.isShowDefaultGrossAdjustmentTable = true;
        $scope.isShowDefaultNetAdjustmentTable = true;
    });
    function getDateInFormat(date) {
        var dateObj = new Date(date);
        var month = dateObj.getMonth() + 1;
        if (month.toString().length == 1) {
            month = "0" + month;
        }
        var day = dateObj.getDate();
        if (day.toString().length == 1) {
            day = "0" + day;
        }
        var year = dateObj.getFullYear();
        var newdate = month + "/" + day + "/" + year;
        return newdate;
    }
    $scope.partyAndOrganizationData = [];
    $scope.isAgency = false;
    function InitOrdersDetailPopup() {
        try {
            $scope.OrdersDetail = {};
            $scope.OrdersDetailModalForm.$setPristine();
            $scope.ordersDetailModalMsgs = false;
            var responseData = $scope.personData;
            var Contacts = alasql('SELECT * FROM ? AS add WHERE Id  = ?', [responseData, $scope.commonOrderDetail.ST_ID.toString()]);
            $scope.commonOrderDetail.ContactName = Contacts[0].PersonName.FullName;
            var streetArray = Contacts[0].Addresses.$values[0].Address.AddressLines.$values;
            var street = "";
            var addStreet = angular.element(document.querySelector('#addStreet'));
            var addStreetProposal = angular.element(document.querySelector('#addStreetProposal'));
            addStreet.empty();
            addStreetProposal.empty();
            angular.forEach(streetArray, function (itemdatavalue, key) {
                if (key == 0) {
                    street = '<span> ' + itemdatavalue + '</span>,<br />';
                }
                else {
                    //street ='<br/>'+street+ '<span>' + itemdatavalue + '</span>,<br />';
                    street = street + '<span style="margin-left:65px"> ' + itemdatavalue + '</span>,<br />';
                }
            });
            var finalDiv = $compile(street)($scope);
            var finalDiv1 = $compile(street)($scope);
            addStreet.append(finalDiv);
            addStreetProposal.append(finalDiv1);
            //$scope.commonOrderDetail.Addressstreet = finalDiv;
            $scope.commonOrderDetail.City = Contacts[0].Addresses.$values[0].Address.CityName;
            $scope.commonOrderDetail.PostalCode = Contacts[0].Addresses.$values[0].Address.PostalCode;
            $scope.commonOrderDetail.Country = Contacts[0].Addresses.$values[0].Address.CountryName;
            var responseData = $scope.advertisers;
            var Advertisers = alasql('SELECT Name FROM ? AS add WHERE Id  = ?', [responseData, $scope.commonOrderDetail.AdvertiserId.toString()]);
            $scope.commonOrderDetail.AdvertiserName = Advertisers[0].Name;
            var responseData = $scope.advertisers;
            if ($scope.commonOrderDetail.AgencyId > 0 && $scope.commonOrderDetail.AgencyId != undefined && $scope.commonOrderDetail.AgencyId != null && $scope.commonOrderDetail.AgencyId != "") {
                var Agencies = alasql('SELECT Name FROM ? AS add WHERE Id  = ?', [responseData, $scope.commonOrderDetail.AgencyId.toString()]);
                $scope.commonOrderDetail.AgencyName = Agencies[0].Name;
            }
            if ($scope.commonOrderDetail.BillToInd == false) {
                $scope.isAgency = false;
                $scope.commonOrderDetail.BillTo = "Advertiser";
            }
            else {
                $scope.isAgency = true;
                $scope.commonOrderDetail.BillTo = "Agency";
            }
            $scope.commonOrderDetail.CreatedDate = getDateInFormat($scope.commonOrderDetail.CreatedDate);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
        //getAdvertiserThroughASI();

    }

    $scope.getAdvertiserThroughASI = function () {
        try {
            if ($rootScope.mainPartyAndOrganizationData.length <= 0 && $rootScope.mainAdvertisers.length <= 0 && $rootScope.mainAgencies.length <= 0 && $rootScope.mainBillingToContacts.length <= 0) {
                var HasNext = true;
                var offset = 0;
                var advertisers = [];
                var contacts = [];
                function getAdvertiserData() {
                    advertiserFactory.getAllAdvertiser1(baseUrl, authToken, offset, function (result) {
                        if (result != null && result != undefined && result != "") {
                            var ItemData = result.Items.$values
                            console.log(ItemData);
                            angular.forEach(ItemData, function (itemdatavalue, key) {
                                $scope.partyAndOrganizationData.push(itemdatavalue);
                                var type = itemdatavalue.$type;
                                if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                                    advertisers.push(itemdatavalue);
                                }
                                else if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                                    contacts.push(itemdatavalue);
                                }
                            });

                            $scope.advertisers = advertisers;
                            $scope.agencies = advertisers;
                            //$scope.billingToContacts = contacts;
                            $scope.personData = contacts;
                            var TotalCount = result.TotalCount;
                            HasNext = result.HasNext;
                            offset = result.Offset;
                            var Limit = result.Limit;
                            var NextOffset = result.NextOffset;
                            if (HasNext == true) {
                                offset = NextOffset;
                                getAdvertiserData();
                            } else {
                                InitOrdersDetailPopup();
                                // fillPartyComingDashBoard();
                                changeCalledManually();
                                //changeCalledManually_Agency();
                                // $scope.changeAdvertiser();
                                // $scope.changeAgency();
                            }
                        }
                        else {
                            $scope.advertisers = [];
                        }
                    })
                }
                getAdvertiserData();

            }
            else {
                $scope.partyAndOrganizationData = $rootScope.mainPartyAndOrganizationData;
                $scope.advertisers = $rootScope.mainAdvertisers;
                $scope.agencies = $rootScope.mainAgencies;
                $scope.personData = $rootScope.mainBillingToContacts;
                // $scope.billingToContacts = $rootScope.mainBillingToContacts;
                // $scope.$on('$viewContentLoaded', function () {
                advertiserFactory.getAllAdvertiser1(baseUrl, authToken, offset, function (result) {
                    if (result != null && result != undefined && result != "") {
                        //console.log(result.Items.$values);
                        $scope.isEditMode = true;
                        // fillPartyComingDashBoard();
                        InitOrdersDetailPopup();
                        changeCalledManually();
                        //  changeCalledManually_Agency();
                        //  $scope.changeAdvertiser();
                        //  $scope.changeAgency();
                        $scope.isEditMode = false;
                    }
                });
                // });
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }


    }

    function changeCalledManually() {
        try {
            if ($scope.isEditMode == true) {
                var advertiserSelected = $scope.commonOrderDetail.BillToInd;
                if (parseInt(advertiserSelected) == 0) {


                    var advertiserId = $scope.commonOrderDetail.AdvertiserId;
                    // var aadvertiserValue = getSelectedData.text;
                    var partyData = $scope.partyAndOrganizationData;
                    $scope.organizationWiseParty = [];

                    angular.forEach(partyData, function (person, key) {
                        if (person.PrimaryOrganization != undefined) {
                            if (person.PrimaryOrganization.OrganizationPartyId != undefined) {
                                var organizationI = person.PrimaryOrganization.OrganizationPartyId;
                                if (advertiserId == organizationI) {
                                    $scope.organizationWiseParty.push(person);
                                    $scope.test = "secound";

                                    if ($scope.organizationWiseParty.length > 0) {
                                        $scope.personData = $scope.organizationWiseParty;
                                    }
                                }
                            }
                        }
                    });

                    if ($scope.test = "first") {

                        var billingDetailsData = $rootScope.billingDetails;
                        var IsAdvertiser = parseInt(billingDetailsData.BillTo);
                        if (IsAdvertiser == 0) {
                            var billingDetailsData = $rootScope.billingDetails;
                            $scope.personData = parseInt(billingDetailsData.BillToContactId);
                        }
                    }
                }
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    }

    $scope.generateDynamicImageURL = function () {
        $scope.editImageIcon = "";
        $scope.sampleLogoImagePath = "https://localhost" + baseUrl + "areas/iMISAngular_Orders/image/sample-logo.png";
        $scope.checkboxImagePath = "https://localhost" + baseUrl + "areas/iMISAngular_Orders/image/check-box-empty.png";
    }

    // Order Export
    $scope.exportDataExcel = function () {
        try {
            var BuyerId = $scope.BuyerId;
            BuyerId = BuyerId + "IO";
            var HTMLtoExport = document.getElementById('exportable').innerHTML;

            var blob = new Blob([HTMLtoExport], {
                type: "application/vnd.ms-excel charset=utf-8"
            });
            saveAs(blob, BuyerId + ".xlsx");
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    };

    // Order Export
    $scope.exportDataPdf = function () {
        try {
            var BuyerId = $scope.BuyerId;
            BuyerId = BuyerId + "IO";
            kendo.drawing.drawDOM("#exportable", {
                paperSize: "A4",
                margin: { top: "1cm", bottom: "1cm", left: "1cm", right: "1cm" },
                scale: 0.8,
                height: 500
            })
          .then(function (group) {
              kendo.drawing.pdf.saveAs(group, BuyerId)
          });
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    }

    // Order Export
    $scope.exportDataWord = function () {
        try {
            var BuyerId = $scope.BuyerId;
            BuyerId = BuyerId + "IO";
            var HTMLtoExport = document.getElementById('exportable').innerHTML;
            var blob = new Blob([HTMLtoExport], {
                type: "application/msword;charset=utf-8"
            });
            saveAs(blob, BuyerId + ".doc");

        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    };
    // Order Export
    $scope.exportDataWord1 = function () {
        try {
            var BuyerId = $scope.BuyerId;
            BuyerId = BuyerId + "IO";
            var HTMLdata = angular.element(document.querySelector('#exporProposal'))
            HTMLdata.wordExport(BuyerId);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    };



    // Proposal Export
    $scope.exportProposalExcel = function () {
        try {
            var BuyerId = $scope.BuyerId;
            BuyerId = BuyerId + "Proposal";
            var HTMLtoExport = document.getElementById('exporProposal').innerHTML;

            var blob = new Blob([HTMLtoExport], {
                type: "application/ms-excel charset=utf-8"
            });
            saveAs(blob, BuyerId + ".xls");
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    };

    // Proposal Export
    $scope.exportProposalPdf = function () {
        try {
            var BuyerId = $scope.BuyerId;
            BuyerId = BuyerId + "Proposal";
            kendo.drawing.drawDOM("#exporProposal", {
                paperSize: "A4",
                margin: { top: "1cm", bottom: "1cm", left: "1cm", right: "1cm" },
                scale: 0.8,
                height: 500
            })
            .then(function (group) {
                kendo.drawing.pdf.saveAs(group, BuyerId)
            });
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    }

    // Proposal Export
    $scope.exportProposalWord = function () {
        try {
            var BuyerId = $scope.BuyerId;
            BuyerId = BuyerId + "Proposal";
            var HTMLtoExport = document.getElementById('exporProposal').innerHTML;

            var blob = new Blob([HTMLtoExport], {
                type: "application/msword;charset=utf-8"
            });
            saveAs(blob, BuyerId + ".doc");
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    };

    getNoOfIssueMediaOrder();
})