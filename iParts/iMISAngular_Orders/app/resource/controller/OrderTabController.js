﻿'use strict';

app.controller('OrderTabController', function OrderTabController($scope, $location, $stateParams, toastr) {
    console.log('called');
    $scope.OrderTab = true;
    $scope.MediaScheduleTab = false;
    $scope.AdjustmentTab = false;
    $scope.ProductionTab = false;

    $scope.OrderDisable = false;
    $scope.MediaScheduleDisable = true;
    $scope.AdjustmentDisable = true;
    $scope.ProductionDisable = true;
    //$scope.tabOrderId = $stateParams.OrderId;
    $scope.tabOrderId = 0;

    $scope.setOrdersTabActive = function () {
        //console.log('tab called');
        $scope.OrderTab = true;
        $scope.MediaScheduleTab = false;
        $scope.AdjustmentTab = false;
        $scope.ProductionTab = false;

        $scope.OrderDisable = false;
        $scope.MediaScheduleDisable = true;
        $scope.AdjustmentDisable = true;
        $scope.ProductionDisable = true;
        $scope.isFrozen = false;

    }

    //$scope.setMediaScheduleTabActive = function () {
    //    console.log('tab called');
    //    // $scope.tabOrderId = $stateParams.OrderId;
    //    $scope.OrderTab = false;
    //    $scope.MediaScheduleTab = true;
    //    $scope.AdjustmentTab = false;
    //    $scope.ProductionTab = false;

    //    $scope.OrderDisable = false;
    //    $scope.MediaScheduleDisable = false;
    //    $scope.AdjustmentDisable = true;
    //    $scope.ProductionDisable = true;
    //}

    //$scope.setAdjustmentTabActive = function () {
    //    console.log('tab called');
    //    // $scope.tabOrderId = $stateParams.OrderId;
    //    $scope.OrderTab = false;
    //    $scope.MediaScheduleTab = false;
    //    $scope.AdjustmentTab = true;
    //    $scope.ProductionTab = false;

    //    $scope.OrderDisable = false;
    //    $scope.MediaScheduleDisable = false;
    //    $scope.AdjustmentDisable = false;
    //    $scope.ProductionDisable = true;
    //}

    //$scope.setProductionTabActive = function () {
    //    console.log('tab called');
    //    // $scope.tabOrderId = $stateParams.OrderId;
    //    $scope.OrderTab = false;
    //    $scope.MediaScheduleTab = false;
    //    $scope.AdjustmentTab = false;
    //    $scope.ProductionTab = true;

    //    $scope.OrderDisable = false;
    //    $scope.MediaScheduleDisable = false;
    //    $scope.AdjustmentDisable = false;
    //    $scope.ProductionDisable = false;
    //}


    $scope.setMediaScheduleTabActive = function () {
            console.log('tab called');
            // $scope.tabOrderId = $stateParams.OrderId;
            $scope.OrderTab = false;
            $scope.MediaScheduleTab = true;
            $scope.AdjustmentTab = false;
            $scope.ProductionTab = false;

            if ($scope.isFrozen == true)
            {
                $scope.OrderDisable = true;
            }
            else
            {
                $scope.OrderDisable = false;
            }
            $scope.MediaScheduleDisable = false;
            $scope.AdjustmentDisable = false;
            $scope.ProductionDisable = false;
        }

        $scope.setAdjustmentTabActive = function () {
            console.log('tab called');
            // $scope.tabOrderId = $stateParams.OrderId;
            $scope.OrderTab = false;
            $scope.MediaScheduleTab = false;
            $scope.AdjustmentTab = true;
            $scope.ProductionTab = false;

            $scope.OrderDisable = false;
            $scope.MediaScheduleDisable = false;
            $scope.AdjustmentDisable = false;
            $scope.ProductionDisable = false;
        }

        $scope.setProductionTabActive = function () {
            console.log('tab called');
            // $scope.tabOrderId = $stateParams.OrderId;
            $scope.OrderTab = false;
            $scope.MediaScheduleTab = false;
            $scope.AdjustmentTab = false;
            $scope.ProductionTab = true;

            $scope.OrderDisable = false;
            $scope.MediaScheduleDisable = false;
            $scope.AdjustmentDisable = false;
            $scope.ProductionDisable = false;
        }
})