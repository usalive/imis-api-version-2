﻿'use strict';

app.controller('MediaScheduleController', function MediaScheduleController($scope, $stateParams, $compile, $sce, $rootScope, $parse, $location, mediaAssetFactory,
    frequencyFactory, adColorFactory, adSizeFactory, rateCardFactory, issueDateFactory, rateCardCostFactory, mediaOrdersFactory, authKeyService, toastr) {
    console.log("controller called");
    var authKey = authKeyService.getConstantAuthKey();
    $scope.baseUrl = authKey.baseUrl;
    $scope.mediaSchedule = {};
    $scope.mediaSchedule.AdOrderId = $stateParams.AdOrderId;
    $scope.IssueDates = {};
    $scope.IssueDatesArray = [];
    $scope.IssueDate = [];
    $scope.showMsgs = false;
    $scope.rateCardDetail = "";
    $scope.rateCardMaster = [];
    $scope.tempArray = [];
    $scope.$parent.tabOrderId = $stateParams.AdOrderId;
    $scope.$parent.setMediaScheduleTabActive();
    $scope.MediaScheduleTab = [];
    $scope.flag = true;
    $scope.isRequired = false;
    $scope.mediaScheduleData = {};
    $scope.dvIssueDate = false;
    $scope.dvselectedIssueDate = false;
    var GrossCost = $rootScope.GrossCost;
    var NetCost = $rootScope.NetCost;
    $rootScope.GrossCost = parseFloat(GrossCost).toFixed(2);
    $rootScope.NetCost = parseFloat(NetCost).toFixed(2);
    commonInit();

    function commonInit() {
        $scope.mediaSchedule.AdColorId = "";
        $scope.mediaSchedule.BaseCost = "";
        $scope.mediaSchedule.AdCost = "";
        $scope.mediaSchedule.AdSizeId = "";
        $scope.mediaSchedule.IssueDates = "";
        $scope.mediaSchedule.MediaAssetId = "";
        $scope.mediaSchedule.RateCardCost = "";
        $scope.mediaSchedule.FrequencyId = "";
        $scope.mediaSchedule.RateCardId = "";
        $scope.mediaSchedule.RateCardDetailId = "";
        $scope.mediaSchedule.ProductCode = "";
        $scope.mediaSchedule.CampaignName = "";
        $scope.rateCardDetail = "";
        $scope.dvAdColor = true;
        $scope.dvAdSize = true;
        $scope.dvAdSizeTextbox = false;
        $scope.dvFrequency = true;
        $scope.dvColumn = false;
        $scope.dvInches = false;
        $scope.mediaSchedule.billingMethodId = "";
        $scope.mediaSchedule.billingMethodName = "";
        $scope.mediaSchedule.AdSize = "";
        $scope.mediaSchedule.mediaAssetName = "";
        $scope.mediaSchedule.rateCardDetail = "";
        $scope.mediaSchedule.AdColorName = "";
        $scope.mediaSchedule.AdSizeName = "";
        $scope.mediaSchedule.FrequencyName = "";
        $scope.defaultAdColorId = "";
        $scope.defaultAdColorName = "";
        $scope.defaultAdSizeId = "";
        $scope.defaultAdSizeName = "";
        $scope.defaultFrequencyId = "";
        $scope.defaultFrequencyName = "";
        $scope.mediaSchedule.ClassifiedText = "";
        $scope.ClassifiedTextNote = "";
        $scope.TotalWords = 0;
        $scope.mediaSchedule.PerWordCost = "";
        $scope.NoOfWord = "";
        $scope.AdditionalWord = "";
        $scope.mediaSchedule.Column = "";
        $scope.mediaSchedule.Inches = "";
        $scope.mediaSchedule.NoOfInserts = "";
        $scope.mediaSchedule.Impression = "";
        $scope.lblAdSize = "Ad Size";
        $scope.mediaSchedule.AdColorId = $scope.defaultAdColorId;
    }
    function InitDateRange() {

        var fromDateControl = angular.element(document.querySelector('#txtFromDate'));
        var toDateControl = angular.element(document.querySelector('#txtToDate'));
        var currDate = new Date();
        currDate.setMonth(currDate.getMonth() + 1);
        currDate.setDate(1);
        $scope.mediaSchedule.FromDate = getDateInFormat(currDate);
        var todate = currDate;
        todate.setMonth(currDate.getMonth() + 12);
        $scope.mediaSchedule.ToDate = getDateInFormat(todate);


        fromDateControl.datepicker({
            onSelect: function (date) {
                $scope.mediaSchedule.FromDate = getDateInFormat(date);
                var toDate = new Date(date);
                toDate.setMonth(toDate.getMonth() + 12);
                $scope.mediaSchedule.ToDate = getDateInFormat(toDate);
                var MediaAssetDropDown = angular.element(document.querySelector('#ddlMediaAsset'));
                var Id = 0;
                if ($scope.mediaSchedule.MediaAssetId > 0) {
                    Id = $scope.mediaSchedule.MediaAssetId;
                }
                else {
                    var getSelected = MediaAssetDropDown.select2("data").id;
                    Id = getSelected.split(':')[1];
                }
                if (($scope.mediaSchedule.ToDate != null || $scope.mediaSchedule.ToDate == undefined) && (Id != "" || Id == undefined)) {
                    var issueDates = { "MediaAssetId": Id, "FromDate": $scope.mediaSchedule.FromDate, "ToDate": $scope.mediaSchedule.ToDate }
                    GetIssueDates(issueDates);
                }
            }
        });
        toDateControl.datepicker({
            onSelect: function (date) {
                $scope.mediaSchedule.ToDate = getDateInFormat(date);
                var MediaAssetDropDown = angular.element(document.querySelector('#ddlMediaAsset'));
                var Id = 0;
                if ($scope.mediaSchedule.MediaAssetId > 0) {
                    Id = $scope.mediaSchedule.MediaAssetId;
                }
                else {
                    var getSelected = MediaAssetDropDown.select2("data").id;
                    Id = getSelected.split(':')[1];
                }
                if (($scope.mediaSchedule.FromDate != null || $scope.mediaSchedule.FromDate == undefined) && ($scope.mediaSchedule.ToDate != null || $scope.mediaSchedule.ToDate == undefined) && (Id != "" || Id == undefined)) {
                    var issueDates = { "MediaAssetId": Id, "FromDate": $scope.mediaSchedule.FromDate, "ToDate": $scope.mediaSchedule.ToDate }
                    GetIssueDates(issueDates);
                }
            }
        }
        );
        fromDateControl.datepicker("option", "maxDate", null);
        toDateControl.datepicker("option", "maxDate", null);
    }
    function clearData(id, text) {
        $scope.flag = false;
        $scope.changeMediaAsset();
        changeDropDownColor('s2id_ddlMediaAsset', "");
        MaintainMediaBillingMethod("Flat Rate");
        commonInit();
        InitDateRange();
    }
    function getDateInFormat(date) {
        var dateObj = new Date(date);
        var month = dateObj.getMonth() + 1;
        if (month.toString().length == 1) {
            month = "0" + month;
        }
        var day = dateObj.getDate();
        if (day.toString().length == 1) {
            day = "0" + day;
        }
        var year = dateObj.getFullYear();
        var newdate = month + "/" + day + "/" + year;
        return newdate;
    }

    $scope.MediaAssetSelect = {
        multiple: false,
        formatSearching: 'Searching the MediaAsset...',
        formatNoMatches: 'No advertiser found'
    };

    $scope.RateCardSelect = {
        multiple: false,
        formatSearching: 'Searching the RateCard...',
        formatNoMatches: 'No RateCard found'
    };

    $scope.AdColorSelect = {
        multiple: false,
        formatSearching: 'Searching the AdColor...',
        formatNoMatches: 'No AdColor found'
    };

    $scope.AdSizeSelect = {
        multiple: false,
        formatSearching: 'Searching the AdSize...',
        formatNoMatches: 'No AdSize found'
    };

    $scope.FrequencySelect = {
        multiple: false,
        formatSearching: 'Searching the Frequency...',
        formatNoMatches: 'No Frequency found'
    };


    var authKey = authKeyService.getConstantAuthKey();
    var baseUrl = authKey.baseUrl;
    var authToken = authKey.authToken;
    function getData() {

        var mediaOrderId = $stateParams.MediaOrderId;
        var adOrderId = $stateParams.AdOrderId;
        if (adOrderId > 0 && mediaOrderId > 0) {
            fillMediaSchedule(mediaOrderId);
            if ($rootScope.fromDashboard == false) {
                $scope.dvIssueDate = true;
                $scope.dvselectedIssueDate = true;
            }
            else {
                $scope.dvIssueDate = true;
                $scope.dvselectedIssueDate = true;
            }

        }
        else {
            $scope.dvIssueDate = false;
            $scope.dvselectedIssueDate = false;
        }
    }
    function fillMediaSchedule(mediaOrderId) {
        try {
            mediaOrdersFactory.getOrdersByMediaOrderId(mediaOrderId, function (result) {
                $scope.mediaScheduleData = result.data;
                if (result.data.IsFrozen == true) {
                    $scope.$parent.isFrozen = true
                    $scope.$parent.setMediaScheduleTabActive();
                }
                else {
                    $scope.$parent.isFrozen = false;
                    $scope.$parent.setMediaScheduleTabActive();
                }
                if ($rootScope.billingDetails.CampaignName == "" || $rootScope.billingDetails.CampaignName == null || $rootScope.billingDetails.CampaignName == undefined) {
                    $rootScope.billingDetails.CampaignName = result.data.CampaignName;
                }
                $rootScope.billingDetails.AdOrderId = result.data.BuyId;
                if ($rootScope.billingDetails.AdvertiserId == "" || $rootScope.billingDetails.AdvertiserId == null || $rootScope.billingDetails.AdvertiserId == undefined) {
                    $rootScope.billingDetails.AdvertiserId = result.data.AdvertiserId;
                }
                if ($rootScope.billingDetails.AgencyId == "" || $rootScope.billingDetails.AgencyId == null || $rootScope.billingDetails.AgencyId == undefined) {
                    $rootScope.billingDetails.AgencyId = result.data.AgencyId;
                }
                if ($rootScope.billingDetails.BillTo == "" || $rootScope.billingDetails.BillTo == null || $rootScope.billingDetails.BillTo == undefined) {
                    $rootScope.billingDetails.BillTo = result.data.BillToInd;
                }
                if ($rootScope.billingDetails.AdCommissions == "" || $rootScope.billingDetails.AdCommissions == null || $rootScope.billingDetails.AdCommissions == undefined) {
                    $rootScope.billingDetails.AdCommissions = result.data.MediaOrderReps;
                }
                if ($rootScope.billingDetails.BillToContactId == "" || $rootScope.billingDetails.BillToContactId == null || $rootScope.billingDetails.BillToContactId == undefined) {
                    $rootScope.billingDetails.BillToContactId = result.data.ST_ID;
                }
                if ($rootScope.billingDetails.MediaOrderId == "" || $rootScope.billingDetails.MediaOrderId == null || $rootScope.billingDetails.MediaOrderId == undefined) {
                    $rootScope.billingDetails.MediaOrderId = mediaOrderId;
                }
                if ($scope.mediaSchedule.MediaAssetId > 0 && $scope.mediaSchedule.MediaAssetId != undefined && $scope.mediaSchedule.MediaAssetId != null && $scope.mediaSchedule.MediaAssetId != "") {

                }
                else if ($scope.mediaScheduleData.MediaAssetId > 0 && $scope.mediaScheduleData.MediaAssetId != undefined && $scope.mediaScheduleData.MediaAssetId != null && $scope.mediaScheduleData.MediaAssetId != "") {
                    $scope.mediaSchedule.MediaAssetId = $scope.mediaScheduleData.MediaAssetId;
                    GetAllMediaAsset();


                }
            });
            //if (MediaScheduleData.AdOrderId > 0) {
            //    $scope.isEditMode = true;
            //   // $scope.mediaSchedule.AdOrderId = MediaScheduleData.AdOrderId;            
            //    $scope.mediaSchedule.MediaAssetId = MediaScheduleData.MediaAssetId;
            //    $scope.mediaSchedule.Adverdtiser = parseInt(billingDetailsData.AdvertiserId);
            //    if (billingDetailsData.AgencyId != "") {
            //        $scope.order.Agency = parseInt(billingDetailsData.AgencyId);
            //    }
            //    else {
            //        $scope.order.Agency = 0;
            //    }
            //    $scope.order.BillTo = parseInt(billingDetailsData.BillTo);
            //    setDroupDownValue('ddlAdverdtiser', $scope.order.Adverdtiser);
            //    setDroupDownValue('ddlAgency', $scope.order.Agency);
            //    if (billingDetailsData.AdCommissions.length > 0) {
            //        createRepsandTerritoryWiseCommissionHTMLWithData(billingDetailsData.AdCommissions);
            //    }
            //}
        }
        catch (e) {
            $scope.isLoading = false;
          
            $exceptionHandler(e);
        }

    }
    function getUnits() {
        try {
            var MediaBillingMathodeName = $scope.mediaScheduleData.MediaBillingMathodeName;
            if (MediaBillingMathodeName != undefined && MediaBillingMathodeName != "" && MediaBillingMathodeName != null) {
                if (MediaBillingMathodeName == "Per Word") {
                    $scope.TotalWords = $scope.mediaScheduleData.Units;
                }
                else if (MediaBillingMathodeName == "CPM") {
                    $scope.mediaSchedule.NoOfInserts = $scope.mediaScheduleData.Units;
                }
                else if (MediaBillingMathodeName == "Web CPM") {
                    $scope.mediaSchedule.Impression = $scope.mediaScheduleData.Units;
                }
            }
            $scope.mediaSchedule.Column = $scope.mediaScheduleData.Column;
            $scope.mediaSchedule.Inches = $scope.mediaScheduleData.Inches;
            $scope.mediaSchedule.AdSize = ($scope.mediaScheduleData.Column * $scope.mediaScheduleData.Inches);
            $scope.mediaSchedule.ClassifiedText = $scope.mediaScheduleData.ClassifiedText;
            $scope.mediaSchedule.RateCardCost = $scope.mediaScheduleData.BaseCalculationCost;
            $scope.mediaSchedule.BaseCost = $scope.mediaScheduleData.BaseRateCardCost;
            $scope.mediaSchedule.AdCost = $scope.mediaScheduleData.RateCardCost;
            $scope.mediaSchedule.FlightStartDate = $scope.mediaScheduleData.FlightStartDate;
            $scope.mediaSchedule.FlightEndDate = $scope.mediaScheduleData.FlightEndDate;
            $scope.selected = [];
            $scope.selected[$scope.mediaScheduleData.IssueDateId] = true;
        }
        catch (e) {
            $scope.isLoading = false;
           
            $exceptionHandler(e);
        }

    }
    function GetAllMediaAsset() {
        try {
            mediaAssetFactory.getAll(function (result) {
                // $scope.mediaSchedule.MediaAssetId = 0;
                $scope.mediaAssets = result.data;
                if ($scope.mediaSchedule.MediaAssetId > 0) {
                    setDroupDownValue('ddlMediaAsset', $scope.mediaSchedule.MediaAssetId);
                    var responseData = $scope.mediaAssets;
                    var MediaAssets = alasql('SELECT mediaAssetName FROM ? AS add WHERE mediaAssetId  = ?', [responseData, parseInt($scope.mediaSchedule.MediaAssetId)]);
                    if (MediaAssets != undefined && MediaAssets != null && MediaAssets != "") {
                        initSelect2('s2id_ddlMediaAsset', MediaAssets[0].mediaAssetName);
                    }
                    changeDropDownColor('s2id_ddlMediaAsset', $scope.mediaSchedule.MediaAssetId);
                    if ($scope.mediaSchedule.RateCardId > 0 && $scope.mediaSchedule.RateCardId != undefined && $scope.mediaSchedule.RateCardId != null && $scope.mediaSchedule.RateCardId != "") {

                    }
                    else if ($scope.mediaScheduleData.RateCardId > 0 && $scope.mediaScheduleData.RateCardId != undefined && $scope.mediaScheduleData.RateCardId != null && $scope.mediaScheduleData.RateCardId != "") {
                        $scope.mediaSchedule.RateCardId = $scope.mediaScheduleData.RateCardId;
                        if ($scope.mediaScheduleData.AdSizeId > 0 && $scope.mediaScheduleData.AdSizeId != undefined && $scope.mediaScheduleData.AdSizeId != null && $scope.mediaScheduleData.AdSizeId != "") {
                            $scope.mediaSchedule.AdSizeId = $scope.mediaScheduleData.AdSizeId;
                            $scope.changeMediaAsset();
                        }

                    }
                }

            })
        }
        catch (e) {
            $scope.isLoading = false;
           
            $exceptionHandler(e);
        }

    }


    function GetRateCardsbyMediaAsset(mediaAssetId) {
        try {
            rateCardFactory.getByMediaAsset(mediaAssetId, function (result) {
                $scope.tempArray = [];
                angular.forEach(result.data[0].RateCards, function (value, key) {
                    //alert(value.MediaBillingMethodId);
                    $scope.rateCardMaster.rateCardDetail = value.RateCardName + ' | ' + (value.MediaBillingMethodName).trim() + ' | ' + value.AdTypeName;
                    $scope.rateCardMaster.RateCardId = value.RateCardId;
                    $scope.rateCardMaster.MediaBillingMethodId = value.MediaBillingMethodId;
                    $scope.rateCardMaster.MediaBillingMethodName = value.MediaBillingMethodName;
                    $scope.rateCardMaster.ProductCode = value.ProductCode;
                    $scope.tempArray.push($scope.rateCardMaster);
                    $scope.rateCardMaster = [];
                })
                $scope.rateCards = $scope.tempArray;
                if (mediaAssetId == $scope.mediaScheduleData.MediaAssetId && $scope.mediaScheduleData.RateCardId > 0) {
                    $scope.mediaSchedule.RateCardId = $scope.mediaScheduleData.RateCardId;
                    setDroupDownValue('ddlRateCard', $scope.mediaSchedule.RateCardId);
                    var responseData = $scope.rateCards;
                    var RateCards = alasql('SELECT rateCardDetail FROM ? AS add WHERE RateCardId  = ?', [responseData, parseInt($scope.mediaSchedule.RateCardId)]);
                    if (RateCards != undefined && RateCards != "" && RateCards != null && RateCards.length > 0) {
                        initSelect2('s2id_ddlRateCard', RateCards[0].rateCardDetail);
                    }
                    changeDropDownColor('s2id_ddlRateCard', $scope.mediaSchedule.RateCardId);
                    $scope.changeRateCard();
                }
                else {
                    initSelect2('s2id_ddlRateCard', "Select Rate Card");
                    changeDropDownColor('s2id_ddlRateCard', "");
                }

            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }


    }
    function GetAdSizeByRateCard(rateCardId) {
        try {
            adSizeFactory.getByRateCard(rateCardId, function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            $scope.adSizes = result.data;

                            angular.forEach(result.data, function (itemdatavalue, key) {
                                if (key == 0) {
                                    $scope.defaultAdSizeId = itemdatavalue.AdSizeId;
                                    $scope.defaultAdSizeName = itemdatavalue.AdSizeName;
                                }
                                $scope.PCIAdSizeId = itemdatavalue.AdSizeId;
                                $scope.PCIAdSizeName = itemdatavalue.AdSizeName;
                                if ($scope.mediaSchedule.billingMethodName == "PCI") {
                                    $scope.mediaSchedule.AdSizeId = $scope.PCIAdSizeId;
                                    $scope.mediaSchedule.AdSizeName = $scope.PCIAdSizeName;
                                    GetAdColor(rateCardId, $scope.mediaSchedule.AdSizeId, $scope.mediaSchedule.billingMethodName);
                                }
                                if ($scope.mediaSchedule.billingMethodName == "Per Word" || $scope.mediaSchedule.billingMethodName == "Sponsorship") {
                                    GetAdColor(rateCardId, $scope.defaultAdSizeId, $scope.mediaSchedule.billingMethodName);
                                }
                            });
                            if ($scope.mediaSchedule.MediaAssetId == $scope.mediaScheduleData.MediaAssetId
                                && $scope.mediaSchedule.RateCardId == $scope.mediaScheduleData.RateCardId
                                && $scope.mediaScheduleData.AdSizeId > 0) {
                                $scope.mediaSchedule.AdSizeId = $scope.mediaScheduleData.AdSizeId;
                                setDroupDownValue('ddlAdSize', $scope.mediaSchedule.AdSizeId);
                                var responseData = $scope.adSizes;
                                var AdSizes = alasql('SELECT AdSizeName FROM ? AS add WHERE AdSizeId  = ?', [responseData, parseInt($scope.mediaSchedule.AdSizeId)]);
                                if (AdSizes != undefined && AdSizes != null && AdSizes != null) {
                                    initSelect2('s2id_ddlAdSize', AdSizes[0].AdSizeName);
                                }
                                changeDropDownColor('s2id_ddlAdSize', $scope.mediaSchedule.AdSizeId);
                                $scope.changeAdSize();
                            }

                        }
                    }
                }
                // else
                // toastr.error(result.message, 'Error!');
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    }
    function GetAdColor(rateCardId, adSizeId, billingMethod) {
        try {
            adColorFactory.getbyRatecardandAdsize(rateCardId, adSizeId, function (result) {
                if (result.data != null) {
                    if (result.data.length > 0) {
                        $scope.adColors = result.data;

                        angular.forEach(result.data, function (value, key) {
                            if (value.AdColorName == "4 Color") {
                                $scope.defaultAdColorId = value.AdColorId;
                                $scope.defaultAdColorName = value.AdColorName;
                            } else if (key == 0) {
                                $scope.defaultAdColorId = value.AdColorId;
                                $scope.defaultAdColorName = value.AdColorName;
                            }
                        })
                        $scope.mediaSchedule.AdColorId = $scope.defaultAdColorId;
                        $scope.mediaSchedule.AdColorName = $scope.defaultAdColorName;
                        var adDropDown = angular.element(document.querySelector('#ddlAdColor'));
                        adDropDown.select2('val', $scope.mediaSchedule.AdColorId);
                        initSelect2("s2id_ddlAdColor", $scope.mediaSchedule.AdColorName);
                        changeDropDownColor('s2id_ddlAdColor', $scope.mediaSchedule.AdColorId);
                        GetFrequency(rateCardId, $scope.defaultAdColorId, adSizeId);

                        // GetAdSizeByRateCard(rateCardId);
                        if ($scope.mediaSchedule.billingMethodName == "Per Word" || $scope.mediaSchedule.billingMethodName == "CPM" || $scope.mediaSchedule.billingMethodName == "Sponsorship") {
                            GetFrequency(rateCardId, $scope.defaultAdColorId, adSizeId);
                        }
                        if ($scope.mediaSchedule.MediaAssetId == $scope.mediaScheduleData.MediaAssetId
                            && $scope.mediaSchedule.RateCardId == $scope.mediaScheduleData.RateCardId
                             && $scope.mediaSchedule.AdSizeId == $scope.mediaScheduleData.AdSizeId
                          && $scope.mediaScheduleData.AdColorId > 0) {
                            $scope.mediaSchedule.AdColorId = $scope.mediaScheduleData.AdColorId;
                            setDroupDownValue('ddlAdColor', $scope.mediaSchedule.AdColorId);
                            var responseData = $scope.adColors;
                            var AdColors = alasql('SELECT AdColorName FROM ? AS add WHERE AdColorId  = ?', [responseData, parseInt($scope.mediaSchedule.AdColorId)]);
                            if (AdColors != undefined && AdColors != null && AdColors != "") {
                                initSelect2('s2id_ddlAdColor', AdColors[0].AdColorName);
                            }
                            changeDropDownColor('s2id_ddlAdColor', $scope.mediaSchedule.AdColorId);
                            $scope.changeAdColor();
                        }
                        //var res = alasql('SELECT * FROM ? AS data ' + 'WHERE AdColorName > ?', [data, '4 Color']);
                    }
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function GetFrequency(rateCardId, adColorId, adSizeId) {
        try {
            frequencyFactory.getbyRatecardColorandAdsize(rateCardId, adColorId, adSizeId, function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            $scope.frequencies = result.data;

                            angular.forEach(result.data, function (itemdatavalue, key) {
                                if (itemdatavalue.FrequencyName == "1" || itemdatavalue.FrequencyName == "1X") {
                                    $scope.sponsorFrequencyId = itemdatavalue.FrequencyId;
                                    $scope.sponsorFrequencyName = itemdatavalue.FrequencyName;
                                    $scope.defaultFrequencyId = itemdatavalue.FrequencyId;
                                    $scope.defaultFrequencyName = itemdatavalue.FrequencyName;
                                }
                                else if (key == 0) {
                                    $scope.defaultFrequencyId = itemdatavalue.FrequencyId;
                                    $scope.defaultFrequencyName = itemdatavalue.FrequencyName;
                                }
                            });
                            if ($scope.mediaSchedule.billingMethodName == "Sponsorship") {
                                $scope.mediaSchedule.FrequencyId = $scope.sponsorFrequencyId;
                                $scope.mediaSchedule.FrequencyName = $scope.sponsorFrequencyName;
                            }
                            if ($scope.mediaSchedule.billingMethodName == "Per Word") {
                                $scope.mediaSchedule.AdSizeId = $scope.defaultAdSizeId;
                                $scope.mediaSchedule.AdSizeName = $scope.defaultAdSizeName;
                                $scope.mediaSchedule.FrequencyId = $scope.defaultFrequencyId;
                                $scope.mediaSchedule.FrequencyName = $scope.defaultFrequencyName;


                                GetRateCardCostForPerWord(rateCardId, adColorId);

                            }
                            if ($scope.mediaSchedule.MediaAssetId == $scope.mediaScheduleData.MediaAssetId
                            && $scope.mediaSchedule.RateCardId == $scope.mediaScheduleData.RateCardId
                             && $scope.mediaSchedule.AdSizeId == $scope.mediaScheduleData.AdSizeId
                                && $scope.mediaScheduleData.FrequencyId > 0) {
                                $scope.mediaSchedule.FrequencyId = $scope.mediaScheduleData.FrequencyId;
                                setDroupDownValue('ddlfrequency', $scope.mediaSchedule.FrequencyId);
                                var responseData = $scope.frequencies;
                                var Frequencies = alasql('SELECT FrequencyName FROM ? AS add WHERE FrequencyId  = ?', [responseData, parseInt($scope.mediaSchedule.FrequencyId)]);
                                if (Frequencies != undefined && Frequencies != null && Frequencies != "") {
                                    initSelect2('s2id_ddlfrequency', Frequencies[0].FrequencyName);
                                }
                                changeDropDownColor('s2id_ddlfrequency', $scope.mediaSchedule.FrequencyId);
                                $scope.changeFrequency();
                            }
                        }
                    }
                }
                //else
                //  toastr.error(result.message, 'Error!');
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    }
    function GetRateCardCostForPerWord(rateCardId, adColorId) {
        try {
            rateCardCostFactory.getRateCardCostForPerWord(rateCardId, adColorId, function (result) {
                if (result.statusCode == 1) {
                    if ($scope.mediaSchedule.MediaAssetId == $scope.mediaScheduleData.MediaAssetId
                                  && $scope.mediaSchedule.RateCardId == $scope.mediaScheduleData.RateCardId) {
                        getUnits();
                        $scope.mediaSchedule.RateCardDetailId = result.data.RateCardDetailId;
                    }
                    else {
                        $scope.mediaSchedule.RateCardCost = result.data.RateCardCost;
                        $scope.mediaSchedule.AdCost = result.data.RateCardCost;
                        $scope.mediaSchedule.BaseCost = result.data.RateCardCost;
                        $scope.mediaSchedule.PerWordCost = result.data.PerWordCost;

                        $scope.NoOfWord = result.data.NoOfWord;
                        $scope.AdditionalWord = result.data.AdditionalWord;
                        $scope.ClassifiedTextNote = "Fixed cost $" + $scope.mediaSchedule.RateCardCost + " for " + $scope.NoOfWord + " words. Additional cost $" + $scope.mediaSchedule.PerWordCost + " per " + $scope.AdditionalWord + " word(s).";

                    }
                }
                //    else
                //        toastr.error(result.message, 'Error!');
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    }
    function GetRateCardCost(rateCardId, adColorId, adSizeId, frequencyId) {
        try {
            if ($scope.mediaSchedule.billingMethodName == "CPM" && adColorId == "") {
                adColorId = $scope.defaultAdColorId;
            }
            if ($scope.mediaSchedule.billingMethodName == "PCI" && adSizeId == "") {
                adSizeId = $scope.PCIAdSizeId;
            }
            if ($scope.mediaSchedule.billingMethodName == "Sponsorship") {
                adColorId = $scope.defaultAdColorId;
                frequencyId = $scope.sponsorFrequencyId;
            }

            if (rateCardId != "" && adColorId != "" && adSizeId != "" && frequencyId != "") {
                rateCardCostFactory.getCost(parseInt(rateCardId), parseInt(adColorId), parseInt(adSizeId), parseInt(frequencyId), function (result) {
                    if (result.statusCode == 1) {
                        if ($scope.mediaScheduleData.MediaOrderId > 0 && $scope.mediaSchedule.MediaAssetId == $scope.mediaScheduleData.MediaAssetId
                             && $scope.mediaSchedule.RateCardId == $scope.mediaScheduleData.RateCardId) {
                            getUnits();
                            $scope.mediaSchedule.RateCardDetailId = result.data.RateCardDetailId;
                        }
                        else {
                            if ($scope.mediaSchedule.billingMethodName == "PCI") {
                                $scope.mediaSchedule.RateCardCost = result.data.RateCardCost;
                                $scope.mediaSchedule.BaseCost = result.data.RateCardCost;
                                if ($scope.mediaSchedule.Inches > 0 && $scope.mediaSchedule.Column > 0) {

                                    $scope.mediaSchedule.AdCost = $scope.mediaSchedule.Column * $scope.mediaSchedule.Inches * result.data.RateCardCost;
                                }
                            }
                            else if ($scope.mediaSchedule.billingMethodName == "CPM") {
                                $scope.mediaSchedule.RateCardCost = result.data.RateCardCost;
                                //$scope.mediaSchedule.AdCost = result.data;
                                $scope.mediaSchedule.BaseCost = result.data.RateCardCost;
                            }
                            else {
                                $scope.mediaSchedule.RateCardCost = result.data.RateCardCost;
                                $scope.mediaSchedule.AdCost = result.data.RateCardCost;
                                $scope.mediaSchedule.BaseCost = result.data.RateCardCost;
                            }
                            $scope.mediaSchedule.RateCardDetailId = result.data.RateCardDetailId;
                        }
                    }
                    // else
                    // toastr.error(result.message, 'Error!');
                })

            }
            else {
                $scope.mediaSchedule.RateCardCost = "";
                $scope.mediaSchedule.AdCost = "";
                $scope.mediaSchedule.BaseCost = "";
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    }
    function GetIssueDates(Dates) {
        try {
            issueDateFactory.getIssueDate(Dates, function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    var mainDiv = angular.element(document.querySelector('#dvGroup'));
                    mainDiv.empty();
                    if (result.data != null) {
                        if (result.data.length > 0) {
                            $scope.IssueDates = result.data;
                            var count = 1;
                            angular.forEach($scope.IssueDates, function (itemdatavalue, key) {
                                var date = new Date(itemdatavalue.CoverDate);
                                var formatedDate = getDateInFormat(date);
                                var month = date, locale = "en-us", month = date.toLocaleString(locale, { month: "long" });
                                var fullDate = formatedDate + ", " + month + " " + date.getFullYear();
                                //id="' + itemdatavalue.IssueDateId + '" value="' + fullDate + '"
                                if ($scope.mediaScheduleData != undefined && $scope.mediaScheduleData.IssueDateId == itemdatavalue.IssueDateId) {
                                    var htmlTemp = '<div class="col-md-6"><label class="checkbox-inline"><input type="checkbox" class="checkthis" ng-model="selected[' + itemdatavalue.IssueDateId + ']"  ng-disabled="dvselectedIssueDate"> ' + fullDate + '</label></div>'
                                }
                                else {
                                    var htmlTemp = '<div class="col-md-6"><label class="checkbox-inline"><input type="checkbox" class="checkthis" ng-model="selected[' + itemdatavalue.IssueDateId + ']" ng-disabled="dvIssueDate"> ' + fullDate + '</label></div>'
                                }
                                var finalDiv = $compile(htmlTemp)($scope);
                                mainDiv.append(finalDiv);
                                count++;
                            });
                        }
                    }
                    console.log($scope.IssueDatesArray);
                }
                else
                    console.log(result.message);
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    }


    function MaintainMediaBillingMethod(billingMethod) {
        // var temp = billingMethod.trim();
        if (billingMethod == "Flat Rate") {
            $scope.dvAdColor = true;
            $scope.dvAdSize = true;
            $scope.dvAdSizeTextbox = false;
            $scope.dvFrequency = true;
            $scope.dvClassifiedText = false;
            $scope.dvNoOfInsert = false;
            $scope.dvColumn = false;
            $scope.dvInches = false;
            $scope.dvImpression = false;
            $scope.lblAdSize = "Ad Size";
            initSelect2("s2id_ddlAdSize", "Select " + $scope.lblAdSize);
            $scope.isRequired = false;
        }
        else if (billingMethod == "Web CPM") {
            $scope.dvAdColor = true;
            $scope.dvAdSize = true;
            $scope.dvAdSizeTextbox = false;
            $scope.dvFrequency = true;
            $scope.dvClassifiedText = false;
            $scope.dvNoOfInsert = false;
            $scope.dvColumn = false;
            $scope.dvInches = false;
            $scope.dvImpression = true;
            $scope.lblAdSize = "Ad Size";
            initSelect2("s2id_ddlAdSize", "Select " + $scope.lblAdSize);
            $scope.isRequired = false;
        }
        else if (billingMethod == "Per Word") {
            $scope.dvAdColor = false;
            $scope.dvAdSize = false;
            $scope.dvAdSizeTextbox = false;
            $scope.dvFrequency = false;
            $scope.dvClassifiedText = true;
            $scope.dvNoOfInsert = false;
            $scope.dvColumn = false;
            $scope.dvInches = false;
            $scope.dvImpression = false;
            $scope.lblAdSize = "Ad Size";
            initSelect2("s2id_ddlAdSize", "Select " + $scope.lblAdSize);
            $scope.isRequired = true;
        }
        else if (billingMethod == "CPM") {
            $scope.dvAdColor = false;
            $scope.dvAdSize = true;
            $scope.dvAdSizeTextbox = false;
            $scope.dvFrequency = true;
            $scope.dvClassifiedText = false;
            $scope.dvNoOfInsert = true;
            $scope.dvColumn = false;
            $scope.dvInches = false;
            $scope.dvImpression = false;
            // $scope.lblAdSize = "Ad Size";
            $scope.lblAdSize = "Ad Size";
            initSelect2("s2id_ddlAdSize", "Select " + $scope.lblAdSize);
            $scope.isRequired = false;
        }
        else if (billingMethod == "PCI") {
            $scope.dvAdColor = true;
            $scope.dvAdSize = false;
            $scope.dvAdSizeTextbox = true;
            $scope.dvFrequency = true;
            $scope.dvClassifiedText = false;
            $scope.dvNoOfInsert = false;
            $scope.dvColumn = true;
            $scope.dvInches = true;
            $scope.dvImpression = false;
            $scope.lblAdSize = "Ad Size";
            initSelect2("s2id_ddlAdSize", "Select " + $scope.lblAdSize);
            $scope.isRequired = false;
        }
        else if (billingMethod == "Sponsorship") {
            $scope.dvAdColor = false;
            $scope.dvAdSize = true;
            $scope.dvAdSizeTextbox = false;
            $scope.dvFrequency = false;
            $scope.dvClassifiedText = false;
            $scope.dvNoOfInsert = false;
            $scope.dvColumn = false;
            $scope.dvInches = false;
            $scope.dvImpression = false;
            $scope.lblAdSize = "Booth";
            initSelect2("s2id_ddlAdSize", "Select " + $scope.lblAdSize);
            $scope.isRequired = false;
        }
        else {
            $scope.dvAdColor = true;
            $scope.dvAdSize = true;
            $scope.dvAdSizeTextbox = false;
            $scope.dvFrequency = true;
            $scope.dvClassifiedText = false;
            $scope.dvNoOfInsert = false;
            $scope.dvColumn = false;
            $scope.dvInches = false;
            $scope.dvImpression = false;
            $scope.lblAdSize = "Ad Size";
            initSelect2("s2id_ddlAdSize", "Select " + $scope.lblAdSize);
            $scope.isRequired = false;
        }
    }
    $scope.SaveMediaSchedule = function (mediaAsset) {
        try {
            if ($scope.mediaScheduleForm.$valid && mediaAsset.MediaAssetId > 0 && mediaAsset.RateCardCost > 0) {
                if ($scope.selected != undefined) {
                    var temparray = [];
                    var issueDates = [];
                    angular.forEach($scope.selected, function (itemdatavalue, key) {
                        if (itemdatavalue) {
                            var id = parseInt(key)
                            if (id != $scope.mediaScheduleData.IssueDateId) {
                                issueDates.push(id);
                            }
                        }
                    });
                    var temp = {};
                    temp.BuyId = mediaAsset.AdOrderId;
                    temp.MediaAssetId = mediaAsset.MediaAssetId;
                    temp.MediaAssetName = mediaAsset.mediaAssetName;
                    temp.RateCardId = mediaAsset.RateCardId;
                    temp.RateCardName = mediaAsset.rateCardDetail;
                    temp.RateCardDetailId = mediaAsset.RateCardDetailId;;
                    temp.ProductCode = mediaAsset.ProductCode;
                    temp.billingMethodId = mediaAsset.billingMethodId;
                    temp.billingMethodName = mediaAsset.billingMethodName;
                    temp.AdColorId = mediaAsset.AdColorId;
                    temp.AdColorName = mediaAsset.AdColorName;
                    temp.AdSize = mediaAsset.AdSize;
                    temp.AdSizeId = mediaAsset.AdSizeId;
                    temp.AdSizeName = mediaAsset.AdSizeName;
                    temp.FrequencyId = mediaAsset.FrequencyId;
                    temp.FrequencyName = mediaAsset.FrequencyName;
                    temp.Column = mediaAsset.Column;
                    temp.Inches = mediaAsset.Inches;
                    if (mediaAsset.NoOfInserts != "") {
                        temp.Units = mediaAsset.NoOfInserts;
                    }
                    else if (mediaAsset.Impression != "") {
                        temp.Units = mediaAsset.Impression;
                    }
                    else if ($scope.TotalWords != "") {
                        temp.Units = $scope.TotalWords;
                    }
                    else {
                        temp.Units = "";
                    }


                    //temp.NoOfInserts = mediaAsset.NoOfInserts;
                    //temp.Impressions = mediaAsset.Impression;
                    temp.ClassifiedText = mediaAsset.ClassifiedText;
                    temp.PerWordCost = mediaAsset.PerWordCost;
                    temp.BaseRateCardCost = mediaAsset.BaseCost;
                    temp.BaseCalculationCost = mediaAsset.RateCardCost;
                    temp.RateCardCost = mediaAsset.AdCost;
                    //temp.GrossCost = $rootScope.GrossCost;
                    // temp.NetCost = $rootScope.GrossCost;
                    temp.FlightStartDate = mediaAsset.FromDate;
                    temp.FlightEndDate = mediaAsset.ToDate;
                    temp.MediaOrderIssueDates = issueDates;
                    // temp.IssueDateId = id;
                    temp.CampaignName = $rootScope.billingDetails.CampaignName != undefined ? $rootScope.billingDetails.CampaignName : $scope.mediaScheduleData.CampaignName;
                    temp.AdvertiserId = $rootScope.billingDetails.AdvertiserId != undefined ? $rootScope.billingDetails.AdvertiserId : $scope.mediaScheduleData.AdvertiserId;
                    temp.MediaOrderReps = $rootScope.billingDetails.AdCommissions != undefined ? $rootScope.billingDetails.AdCommissions : $scope.mediaScheduleData.MediaOrderReps;
                    temp.AdvertiserName = $rootScope.billingDetails.AdvertiserName != undefined ? $rootScope.billingDetails.AdvertiserName : "";
                    temp.AgencyId = $rootScope.billingDetails.AgencyId != undefined ? $rootScope.billingDetails.AgencyId : $scope.mediaScheduleData.AgencyId;
                    temp.AgencyName = $rootScope.billingDetails.AgencyName != undefined ? $rootScope.billingDetails.AgencyName : "";
                    if ($rootScope.billingDetails.BillTo == 0 || $scope.mediaScheduleData.BT_ID) {
                        temp.BT_ID = $rootScope.billingDetails.AdvertiserId != undefined ? $rootScope.billingDetails.AdvertiserId : $scope.mediaScheduleData.BT_ID;
                    }
                    else if ($rootScope.billingDetails.BillTo == 1 || $scope.mediaScheduleData.BT_ID) {
                        temp.BT_ID = $rootScope.billingDetails.AgencyId != undefined ? $rootScope.billingDetails.AdvertiserId : $scope.mediaScheduleData.BT_ID;

                    }
                    temp.ST_ID = $rootScope.billingDetails.BillToContactId != undefined ? $rootScope.billingDetails.BillToContactId : $scope.mediaScheduleData.ST_ID;
                    temp.BillToInd = $rootScope.billingDetails.BillTo != undefined ? $rootScope.billingDetails.BillTo : $scope.mediaScheduleData.BillToInd;
                    temp.BillToContactId = $rootScope.billingDetails.BillToContactId != undefined ? $rootScope.billingDetails.BillToContactId : $scope.mediaScheduleData.ST_ID;
                    temp.BillToContactName = $rootScope.billingDetails.BillToContactName != undefined ? $rootScope.billingDetails.BillToContactName : "";
                    temp.OrderStatus = "Proposal";
                    temp.Headline = "";
                    temparray.push(temp);
                    //   }
                    //   });
                    $rootScope.mainArray.push(temparray);
                    if ($scope.mediaScheduleData != undefined && $scope.mediaScheduleData.MediaOrderId > 0) {
                        temp.MediaOrderId = $scope.mediaScheduleData.MediaOrderId;
                        mediaAssetFactory.update(temp, function (result) {
                            if (result.statusCode == 1) {
                                //$rootScope.GrossCost = result.data.GrossCost;
                                //$rootScope.NetCost = result.data.NetCost;
                                var GrossCost = result.data.GrossCost;
                                var NetCost = result.data.NetCost;
                                $rootScope.GrossCost = parseFloat(GrossCost).toFixed(2);
                                $rootScope.NetCost = parseFloat(NetCost).toFixed(2);
                                var id = "s2id_ddlMediaAsset";
                                var text = "Select Media Asset";
                                temparray = [];
                                mediaAsset = {};
                                clearData(id, text);
                                $scope.flag = true;
                                $scope.selected = {};
                                $scope.tempArray = [];
                                issueDates = [];
                                $scope.mediaScheduleForm.$setPristine();
                                $scope.showMsgs = false;
                                toastr.success('Edit to the Buy ID ' + $scope.mediaSchedule.AdOrderId, 'Success!');
                            }
                            else
                                toastr.error(result.message, 'Error!');
                        })
                    }
                    else {
                        mediaAssetFactory.save(temp, function (result) {
                            if (result.statusCode == 1) {
                                //$rootScope.GrossCost = result.data.GrossCost;
                                //$rootScope.NetCost = result.data.NetCost;
                                var GrossCost = result.data.GrossCost;
                                var NetCost = result.data.NetCost;
                                $rootScope.GrossCost = parseFloat(GrossCost).toFixed(2);
                                $rootScope.NetCost = parseFloat(NetCost).toFixed(2);
                                var id = "s2id_ddlMediaAsset";
                                var text = "Select Media Asset";
                                temparray = [];
                                mediaAsset = {};
                                clearData(id, text);
                                $scope.flag = true;
                                $scope.selected = {};
                                $scope.tempArray = [];
                                issueDates = [];
                                $scope.mediaScheduleForm.$setPristine();
                                $scope.showMsgs = false;
                                toastr.success('Add to the Buy ID ' + $scope.mediaSchedule.AdOrderId, 'Success!');
                            }
                            else
                                toastr.error(result.message, 'Error!');
                        })
                    }


                }
                else {
                    toastr.error("Please select issuedates...", 'Error!');
                }
                //toastr.error("Please enter classified text ...", 'Error!');
                //}
            } else {
                $scope.showMsgs = true;
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    //Dropdown change event
    $scope.changeMediaAsset = function () {
        try {
            angular.element(document.querySelector('#ddlRateCard')).select2("val", "");
            angular.element(document.querySelector('#ddlAdColor')).select2("val", "");
            angular.element(document.querySelector('#ddlAdSize')).select2("val", "");
            angular.element(document.querySelector('#ddlfrequency')).select2("val", "");
            $scope.mediaSchedule.AdCost = 0;
            $scope.mediaSchedule.RateCardCost = 0;
            $scope.rateCards = "";
            $scope.adColors = "";
            $scope.adSizes = "";
            $scope.frequencies = "";
            $scope.mediaSchedule.ClassifiedText = "";
            $scope.mediaSchedule.PerWordCost = "";
            $scope.ClassifiedTextNote = "";
            $scope.TotalWords = 0;
            $scope.mediaSchedule.Column = "";
            $scope.mediaSchedule.Inches = "";
            $scope.mediaSchedule.NoOfInserts = "";
            $scope.mediaSchedule.Impression = "";
            $scope.mediaSchedule.NoOfInserts = "";
            $scope.mediaSchedule.billingMethodName = "";
            $scope.mediaSchedule.RateCardId = 0;
            changeDropDownColor('s2id_ddlRateCard', "");
            changeDropDownColor('s2id_ddlAdColor', "");
            changeDropDownColor('s2id_ddlAdSize', "");
            changeDropDownColor('s2id_ddlfrequency', "");
            //commonInit();
            if ($scope.flag) {
                //var MediaAssetDropDown = angular.element(document.querySelector('#ddlMediaAsset'));
                // var getSelected = MediaAssetDropDown.select2("data").id;
                // var Id = getSelected.split(':')[1];
                var responseData = $scope.mediaAssets;
                var MediaAssets = alasql('SELECT mediaAssetName FROM ? AS add WHERE mediaAssetId  = ?', [responseData, parseInt($scope.mediaSchedule.MediaAssetId)]);
                if (MediaAssets != undefined && MediaAssets != "" && MediaAssets != null) {
                    $scope.mediaSchedule.mediaAssetName = MediaAssets[0].mediaAssetName;
                }
                //if ($scope.mediaSchedule.RateCardId > 0 && $scope.mediaSchedule.RateCardId != null && $scope.mediaSchedule.RateCardId != "" && $scope.mediaSchedule.RateCardId!=undefined)
                //{
                //    $scope.mediaSchedule.RateCardId = $scope.mediaSchedule.RateCardId;
                //}
                //else
                //{
                //    $scope.mediaSchedule.RateCardId = 0;
                //}

                GetRateCardsbyMediaAsset($scope.mediaSchedule.MediaAssetId);
                changeDropDownColor('s2id_ddlMediaAsset', $scope.mediaSchedule.MediaAssetId);

            }
            else {
                angular.element(document.querySelector('#ddlMediaAsset')).select2("val", "");
                GetAllMediaAsset();
            }
            if (($scope.mediaSchedule.FromDate != null || $scope.mediaSchedule.FromDate == undefined) && ($scope.mediaSchedule.ToDate != null || $scope.mediaSchedule.ToDate == undefined) && ($scope.mediaSchedule.MediaAssetId != "" || $scope.mediaSchedule.MediaAssetId == undefined)) {
                var issueDates = { "MediaAssetId": $scope.mediaSchedule.MediaAssetId, "FromDate": $scope.mediaSchedule.FromDate, "ToDate": $scope.mediaSchedule.ToDate }
                GetIssueDates(issueDates);
            }
            MaintainMediaBillingMethod($scope.mediaSchedule.billingMethodName);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    }
    $scope.changeRateCard = function () {
        try {
            angular.element(document.querySelector('#ddlAdColor')).select2("val", "");
            angular.element(document.querySelector('#ddlAdSize')).select2("val", "");
            angular.element(document.querySelector('#ddlfrequency')).select2("val", "");
            $scope.mediaSchedule.AdSizeId = "";
            $scope.mediaSchedule.FrequencyId = "";
            $scope.mediaSchedule.AdSizeName = "";
            $scope.mediaSchedule.FrequencyName = "";
            $scope.mediaSchedule.AdCost = 0;
            $scope.mediaSchedule.RateCardCost = 0;
            $scope.adColors = "";
            $scope.adSizes = "";
            $scope.frequencies = "";
            $scope.mediaSchedule.ClassifiedText = "";
            $scope.mediaSchedule.PerWordCost = "";
            $scope.ClassifiedTextNote = "";
            $scope.TotalWords = 0;
            $scope.mediaSchedule.Column = "";
            $scope.mediaSchedule.Inches = "";
            $scope.mediaSchedule.NoOfInserts = "";
            $scope.mediaSchedule.Impression = "";
            $scope.mediaSchedule.NoOfInserts = "";
            changeDropDownColor('s2id_ddlAdColor', "");
            changeDropDownColor('s2id_ddlAdSize', "");
            changeDropDownColor('s2id_ddlfrequency', "");
            //var RateCardDropDown = angular.element(document.querySelector('#ddlRateCard'));
            //var getSelectedRateCard = RateCardDropDown.select2("data").id;
            //var getSelectedRateCardText = RateCardDropDown.select2("data").text;
            var rateCardId = "";
            if ($scope.mediaSchedule.MediaAssetId == $scope.mediaScheduleData.MediaAssetId && $scope.mediaSchedule.RateCardId == $scope.mediaScheduleData.RateCardId
                && $scope.mediaScheduleData.RateCardId > 0 && $scope.mediaScheduleData.RateCardId != "" && $scope.mediaScheduleData.RateCardId != null && $scope.mediaScheduleData.RateCardId != undefined) {
                rateCardId = $scope.mediaScheduleData.RateCardId
            }
            else {
                rateCardId = $scope.mediaSchedule.RateCardId;
            }
            var responseData = $scope.tempArray;
            var billingMethod = "";
            var RateCards = alasql('SELECT * FROM ? AS add WHERE RateCardId  = ?', [responseData, parseInt(rateCardId)]);
            if (RateCards != undefined && RateCards != null && RateCards != "" && RateCards.length > 0) {
                initSelect2('s2id_ddlRateCard', RateCards[0].rateCardDetail);
                $scope.mediaSchedule.rateCardDetail = RateCards[0].rateCardDetail;
                billingMethod = (RateCards[0].MediaBillingMethodName).trim();
                changeDropDownColor('s2id_ddlRateCard', $scope.mediaSchedule.RateCardId);
            }
            else {
                initSelect2('s2id_ddlRateCard', "Select Rate Card");
                changeDropDownColor('s2id_ddlRateCard', "");
            }

            // alert(getSelectedRateCard);


            // changeDropDownColor('s2id_ddlRateCard', rateCardId);
            //var temp = getSelectedRateCardText.split(' | ')[1];

            $scope.mediaSchedule.billingMethodName = billingMethod;
            angular.forEach($scope.tempArray, function (itemdatavalue, key) {
                var billingMethodName = itemdatavalue.MediaBillingMethodName.trim();
                if (billingMethodName == $scope.mediaSchedule.billingMethodName) {
                    $scope.mediaSchedule.billingMethodId = itemdatavalue.MediaBillingMethodId;
                    $scope.mediaSchedule.ProductCode = itemdatavalue.ProductCode;
                }
            });
            //$scope.tempArray = [];
            var adColorId = "";
            if ($scope.mediaSchedule.billingMethodName == "Flat Rate" || $scope.mediaSchedule.billingMethodName == "Web CPM" || $scope.mediaSchedule.billingMethodName == "PCI") {
                //var AdColorDropDown = angular.element(document.querySelector('#ddlAdColor'));
                //var getSelectedAdColor = AdColorDropDown.select2("data").id;
                //var adColorId = getSelectedAdColor.split(':')[1];
                if ($scope.mediaSchedule.MediaAssetId == $scope.mediaScheduleData.MediaAssetId && $scope.mediaSchedule.RateCardId == $scope.mediaScheduleData.RateCardId
                    && $scope.mediaSchedule.AdColorId == $scope.mediaScheduleData.AdColorId
                    && $scope.mediaScheduleData.AdColorId > 0 && $scope.mediaScheduleData.AdColorId != "" && $scope.mediaScheduleData.AdColorId != null && $scope.mediaScheduleData.AdColorId != undefined) {
                    adColorId = $scope.mediaScheduleData.AdColorId
                }
                else {
                    adColorId = $scope.mediaSchedule.AdColorId;
                }


            }
            else {
                var adColorId = $scope.defaultAdColorId;
            }
            // var adSizeDropDown = angular.element(document.querySelector('#ddlAdSize'));
            // var getSelectedSize = adSizeDropDown.select2("data").id;
            var adSizeId = "";
            if ($scope.mediaSchedule.MediaAssetId == $scope.mediaScheduleData.MediaAssetId && $scope.mediaSchedule.RateCardId == $scope.mediaScheduleData.RateCardId
                 && $scope.mediaSchedule.AdSizeId == $scope.mediaScheduleData.AdSizeId
                && $scope.mediaScheduleData.AdSizeId > 0 && $scope.mediaScheduleData.AdSizeId != "" && $scope.mediaScheduleData.AdSizeId != null && $scope.mediaScheduleData.AdSizeId != undefined) {
                adSizeId = $scope.mediaScheduleData.AdSizeId
            }
            else {
                adSizeId = $scope.mediaSchedule.AdSizeId;
            }
            // var adFrequencyDropDown = angular.element(document.querySelector('#ddlfrequency'));
            //var getSelectedFrequency = adFrequencyDropDown.select2("data").id;
            var frequencyId = $scope.mediaSchedule.FrequencyId;
            if ($scope.mediaSchedule.MediaAssetId == $scope.mediaScheduleData.MediaAssetId && $scope.mediaSchedule.RateCardId == $scope.mediaScheduleData.RateCardId
                && $scope.mediaSchedule.FrequencyId == $scope.mediaScheduleData.FrequencyId
                && $scope.mediaScheduleData.FrequencyId > 0 && $scope.mediaScheduleData.FrequencyId != "" && $scope.mediaScheduleData.FrequencyId != null && $scope.mediaScheduleData.FrequencyId != undefined) {
                frequencyId = $scope.mediaScheduleData.FrequencyId
            }
            else {
                frequencyId = $scope.mediaSchedule.FrequencyId;
            }

            MaintainMediaBillingMethod(billingMethod);

            //if ($scope.mediaSchedule.AdSizeId > 0 && $scope.mediaSchedule.AdSizeId != null && $scope.mediaSchedule.AdSizeId != "" && $scope.mediaSchedule.AdSizeId != undefined) {
            //    $scope.mediaSchedule.AdSizeId = $scope.mediaSchedule.AdSizeId;
            //}
            //else {
            //    $scope.mediaSchedule.AdSizeId = 0;
            //}
            GetAdSizeByRateCard(rateCardId);
            // GetAdColor(rateCardId, adSizeId, billingMethod);
            GetRateCardCost(rateCardId, adColorId, adSizeId, frequencyId);

        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    }

    $scope.changeAdSize = function () {
        try {
            $scope.mediaSchedule.AdCost = 0;
            $scope.mediaSchedule.RateCardCost = 0;
            //var RateCardDropDown = angular.element(document.querySelector('#ddlRateCard'));
            //var getSelectedRateCard = RateCardDropDown.select2("data").id;
            var rateCardId = "";
            if ($scope.mediaSchedule.MediaAssetId == $scope.mediaScheduleData.MediaAssetId && $scope.mediaSchedule.RateCardId == $scope.mediaScheduleData.RateCardId
                && $scope.mediaScheduleData.RateCardId > 0 && $scope.mediaScheduleData.RateCardId != "" && $scope.mediaScheduleData.RateCardId != null && $scope.mediaScheduleData.RateCardId != undefined) {
                rateCardId = $scope.mediaScheduleData.RateCardId
            }
            else {
                rateCardId = $scope.mediaSchedule.RateCardId;
            }
            $scope.mediaSchedule.NoOfInserts = "";
            $scope.adColors = "";
            $scope.frequencies = "";
            $scope.mediaSchedule.NoOfInserts = "";
            changeDropDownColor('s2id_ddlAdColor', "");
            changeDropDownColor('s2id_ddlfrequency', "");
            //if ($scope.mediaSchedule.billingMethodName == "Flat Rate" || $scope.mediaSchedule.billingMethodName == "Web CPM" || $scope.mediaSchedule.billingMethodName == "PCI") {
            //    var AdColorDropDown = angular.element(document.querySelector('#ddlAdColor'));
            //    var getSelectedAdColor = AdColorDropDown.select2("data").id;
            //    var adColorId = getSelectedAdColor.split(':')[1];
            //}
            // else {
            var adColorId = $scope.defaultAdColorId;

            // }
            //var adSizeDropDown = angular.element(document.querySelector('#ddlAdSize'));
            //var getSelectedSize = adSizeDropDown.select2("data").id;
            var adSizeId = "";
            if ($scope.mediaSchedule.MediaAssetId == $scope.mediaScheduleData.MediaAssetId && $scope.mediaSchedule.RateCardId == $scope.mediaScheduleData.RateCardId
                 && $scope.mediaSchedule.AdSizeId == $scope.mediaScheduleData.AdSizeId
                && $scope.mediaScheduleData.AdSizeId > 0 && $scope.mediaScheduleData.AdSizeId != "" && $scope.mediaScheduleData.AdSizeId != null && $scope.mediaScheduleData.AdSizeId != undefined) {
                adSizeId = $scope.mediaScheduleData.AdSizeId
            }
            else {
                adSizeId = $scope.mediaSchedule.AdSizeId;
            }
            var responseData = $scope.adSizes;
            var AdSizes = alasql('SELECT AdSizeName FROM ? AS add WHERE AdSizeId  = ?', [responseData, parseInt(adSizeId)]);
            if (AdSizes != undefined && AdSizes != null && AdSizes != "") {
                initSelect2('s2id_ddlAdSize', AdSizes[0].AdSizeName);
            }
            changeDropDownColor('s2id_ddlAdSize', adSizeId);
            $scope.mediaSchedule.AdSizeName = AdSizes[0].AdSizeName;
            //var adFrequencyDropDown = angular.element(document.querySelector('#ddlfrequency'));
            //var getSelectedFrequency = adFrequencyDropDown.select2("data").id;
            //var responseData = $scope.frequencies;
            //var Frequencies = alasql('SELECT FrequencyName FROM ? AS add WHERE FrequencyId  = ?', [responseData, parseInt($scope.mediaSchedule.FrequencyId)]);
            // initSelect2('s2id_ddfrequency', Frequencies[0].FrequencyName);
            var frequencyId = $scope.mediaSchedule.FrequencyId;
            if ($scope.mediaSchedule.MediaAssetId == $scope.mediaScheduleData.MediaAssetId && $scope.mediaSchedule.RateCardId == $scope.mediaScheduleData.RateCardId
                && $scope.mediaSchedule.FrequencyId == $scope.mediaScheduleData.FrequencyId
                && $scope.mediaScheduleData.FrequencyId > 0 && $scope.mediaScheduleData.FrequencyId != "" && $scope.mediaScheduleData.FrequencyId != null && $scope.mediaScheduleData.FrequencyId != undefined) {
                frequencyId = $scope.mediaScheduleData.FrequencyId
            }
            else {
                frequencyId = $scope.mediaSchedule.FrequencyId;
            }

            GetAdColor(rateCardId, adSizeId, $scope.mediaSchedule.billingMethodName);
            GetRateCardCost(rateCardId, adColorId, adSizeId, frequencyId);

            if ($scope.mediaSchedule.billingMethodName == "Per Word") {
                GetAdColor(rateCardId, $scope.defaultAdSizeId, $scope.mediaSchedule.billingMethodName);
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    }
    $scope.changeAdColor = function () {
        try {
            //  angular.element(document.querySelector('#ddlAdSize')).select2("val", "");
            angular.element(document.querySelector('#ddlfrequency')).select2("val", "");
            $scope.mediaSchedule.AdCost = 0;
            $scope.mediaSchedule.RateCardCost = 0;
            // $scope.adSizes = "";
            $scope.frequencies = "";
            $scope.mediaSchedule.NoOfInserts = "";
            //changeDropDownColor('s2id_ddlAdSize', "");
            changeDropDownColor('s2id_ddlfrequency', "");
            //var RateCardDropDown = angular.element(document.querySelector('#ddlRateCard'));
            // var getSelectedRateCard = RateCardDropDown.select2("data").id;
            var rateCardId = "";
            if ($scope.mediaSchedule.MediaAssetId == $scope.mediaScheduleData.MediaAssetId && $scope.mediaSchedule.RateCardId == $scope.mediaScheduleData.RateCardId
                && $scope.mediaScheduleData.RateCardId > 0 && $scope.mediaScheduleData.RateCardId != "" && $scope.mediaScheduleData.RateCardId != null && $scope.mediaScheduleData.RateCardId != undefined) {
                rateCardId = $scope.mediaScheduleData.RateCardId
            }
            else {
                rateCardId = $scope.mediaSchedule.RateCardId;
            }
            var adColorId = "";
            if ($scope.mediaSchedule.billingMethodName == "Flat Rate" || $scope.mediaSchedule.billingMethodName == "Web CPM" || $scope.mediaSchedule.billingMethodName == "PCI") {
                //var AdColorDropDown = angular.element(document.querySelector('#ddlAdColor'));
                //var getSelectedAdColor = AdColorDropDown.select2("data").id;
                //var adColorId = getSelectedAdColor.split(':')[1];
                if ($scope.mediaSchedule.MediaAssetId == $scope.mediaScheduleData.MediaAssetId && $scope.mediaSchedule.RateCardId == $scope.mediaScheduleData.RateCardId
                    && $scope.mediaSchedule.AdColorId == $scope.mediaScheduleData.AdColorId
                    && $scope.mediaScheduleData.AdColorId > 0 && $scope.mediaScheduleData.AdColorId != "" && $scope.mediaScheduleData.AdColorId != null && $scope.mediaScheduleData.AdColorId != undefined) {
                    adColorId = $scope.mediaScheduleData.AdColorId
                }
                else {
                    adColorId = $scope.mediaSchedule.AdColorId;
                }
            }
            else {
                adColorId = $scope.defaultAdColorId;

            }
            var adSizeId = "";
            if ($scope.mediaSchedule.billingMethodName == "PCI") {
                adSizeId = $scope.defaultAdSizeId;
            }
            else {
                //var adSizeDropDown = angular.element(document.querySelector('#ddlAdSize'));
                //var getSelectedSize = adSizeDropDown.select2("data").id;

                if ($scope.mediaSchedule.MediaAssetId == $scope.mediaScheduleData.MediaAssetId && $scope.mediaSchedule.RateCardId == $scope.mediaScheduleData.RateCardId
                    && $scope.mediaSchedule.AdSizeId == $scope.mediaScheduleData.AdSizeId
                    && $scope.mediaScheduleData.AdSizeId > 0 && $scope.mediaScheduleData.AdSizeId != "" && $scope.mediaScheduleData.AdSizeId != null && $scope.mediaScheduleData.AdSizeId != undefined) {
                    adSizeId = $scope.mediaScheduleData.AdSizeId
                }
                else {
                    adSizeId = $scope.mediaSchedule.AdSizeId;
                }
            }

            changeDropDownColor('s2id_ddlAdColor', adColorId);
            // var AdColorDropDown = angular.element(document.querySelector('#ddlAdColor'));
            var responseData = $scope.adColors;
            var AdColors = alasql('SELECT AdColorName FROM ? AS add WHERE AdColorId  = ?', [responseData, parseInt($scope.mediaSchedule.AdColorId)]);
            if (AdColors != undefined && AdColors != null && AdColors != "") {
                initSelect2('s2id_ddlAdColor', AdColors[0].AdColorName);
            }
            $scope.mediaSchedule.AdColorName = AdColors[0].AdColorName;

            //var adFrequencyDropDown = angular.element(document.querySelector('#ddlfrequency'));
            //  var getSelectedFrequency = adFrequencyDropDown.select2("data").id;
            var frequencyId = $scope.mediaSchedule.FrequencyId;
            if ($scope.mediaSchedule.MediaAssetId == $scope.mediaScheduleData.MediaAssetId && $scope.mediaSchedule.RateCardId == $scope.mediaScheduleData.RateCardId
                 && $scope.mediaSchedule.FrequencyId == $scope.mediaScheduleData.FrequencyId
                && $scope.mediaScheduleData.FrequencyId > 0 && $scope.mediaScheduleData.FrequencyId != "" && $scope.mediaScheduleData.FrequencyId != null && $scope.mediaScheduleData.FrequencyId != undefined) {
                frequencyId = $scope.mediaScheduleData.FrequencyId
            }
            else {
                frequencyId = $scope.mediaSchedule.FrequencyId;
            }

            // GetAdSizeByRateCard(rateCardId);

            GetFrequency(rateCardId, adColorId, adSizeId);
            GetRateCardCost(rateCardId, adColorId, adSizeId, frequencyId);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.changeFrequency = function () {
        try {
            $scope.mediaSchedule.AdCost = 0;
            $scope.mediaSchedule.RateCardCost = 0;
            //var RateCardDropDown = angular.element(document.querySelector('#ddlRateCard'));
            // var getSelectedRateCard = RateCardDropDown.select2("data").id;
            var rateCardId = $scope.mediaSchedule.RateCardId;
            $scope.mediaSchedule.NoOfInserts = "";
            var adColorId = "";
            if ($scope.mediaSchedule.billingMethodName == "Flat Rate" || $scope.mediaSchedule.billingMethodName == "Web CPM" || $scope.mediaSchedule.billingMethodName == "PCI") {
                //var AdColorDropDown = angular.element(document.querySelector('#ddlAdColor'));
                //var getSelectedAdColor = AdColorDropDown.select2("data").id;
                //var adColorId = getSelectedAdColor.split(':')[1];
                if ($scope.mediaSchedule.MediaAssetId == $scope.mediaScheduleData.MediaAssetId && $scope.mediaSchedule.RateCardId == $scope.mediaScheduleData.RateCardId
                    && $scope.mediaSchedule.AdColorId == $scope.mediaScheduleData.AdColorId
                    && $scope.mediaScheduleData.AdColorId > 0 && $scope.mediaScheduleData.AdColorId != "" && $scope.mediaScheduleData.AdColorId != null && $scope.mediaScheduleData.AdColorId != undefined) {
                    adColorId = $scope.mediaScheduleData.AdColorId
                }
                else {
                    adColorId = $scope.mediaSchedule.AdColorId;
                }
            }
            else {
                adColorId = $scope.defaultAdColorId;
            }
            // var adSizeDropDown = angular.element(document.querySelector('#ddlAdSize'));
            // var getSelectedSize = adSizeDropDown.select2("data").id;
            var adSizeId = "";
            if ($scope.mediaSchedule.MediaAssetId == $scope.mediaScheduleData.MediaAssetId && $scope.mediaSchedule.RateCardId == $scope.mediaScheduleData.RateCardId
                && $scope.mediaSchedule.AdSizeId == $scope.mediaScheduleData.AdSizeId
                && $scope.mediaScheduleData.AdSizeId > 0 && $scope.mediaScheduleData.AdSizeId != "" && $scope.mediaScheduleData.AdSizeId != null && $scope.mediaScheduleData.AdSizeId != undefined) {
                adSizeId = $scope.mediaScheduleData.AdSizeId
            }
            else {
                adSizeId = $scope.mediaSchedule.AdSizeId;
            }
            // var adFrequencyDropDown = angular.element(document.querySelector('#ddlfrequency'));
            // var getSelectedFrequency = adFrequencyDropDown.select2("data").id;
            var frequencyId = $scope.mediaSchedule.FrequencyId;
            if ($scope.mediaSchedule.MediaAssetId == $scope.mediaScheduleData.MediaAssetId && $scope.mediaSchedule.RateCardId == $scope.mediaScheduleData.RateCardId
                && $scope.mediaSchedule.FrequencyId == $scope.mediaScheduleData.FrequencyId
                && $scope.mediaScheduleData.FrequencyId > 0 && $scope.mediaScheduleData.FrequencyId != "" && $scope.mediaScheduleData.FrequencyId != null && $scope.mediaScheduleData.FrequencyId != undefined) {
                frequencyId = $scope.mediaScheduleData.FrequencyId
            }
            else {
                frequencyId = $scope.mediaSchedule.FrequencyId;
            }
            changeDropDownColor('s2id_ddlfrequency', frequencyId);
            var responseData = $scope.frequencies;
            var Frequencies = alasql('SELECT FrequencyName FROM ? AS add WHERE FrequencyId  = ?', [responseData, parseInt($scope.mediaSchedule.FrequencyId)]);
            if (Frequencies != undefined && Frequencies != null && Frequencies != "") {
                initSelect2('s2id_ddfrequency', Frequencies[0].FrequencyName);
            }
            $scope.mediaSchedule.FrequencyName = Frequencies[0].FrequencyName;

            GetRateCardCost(rateCardId, adColorId, adSizeId, frequencyId);

            if ($scope.mediaSchedule.MediaAssetId == $scope.mediaScheduleData.MediaAssetId && $scope.mediaSchedule.RateCardId == $scope.mediaScheduleData.RateCardId
                && $scope.mediaSchedule.AdSizeId == $scope.mediaScheduleData.AdSizeId && $scope.mediaSchedule.AdColorId == $scope.mediaScheduleData.AdColorId
                && $scope.mediaSchedule.FrequencyId == $scope.mediaScheduleData.FrequencyId
                && $scope.mediaScheduleData.MediaAssetId > 0 && $scope.mediaScheduleData.MediaAssetId != undefined && $scope.mediaScheduleData.MediaAssetId != null && $scope.mediaScheduleData.MediaAssetId != "") {
                getUnits();
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    }
    $scope.changeRateCardCost = function () {
        try {
            if ($scope.mediaSchedule.billingMethodName == "CPM") {
                $scope.mediaSchedule.AdCost = ($scope.mediaSchedule.NoOfInserts * $scope.mediaSchedule.RateCardCost / 1000);
            } else if ($scope.mediaSchedule.billingMethodName == "PCI") {
                if ($scope.mediaSchedule.Inches > 0 && $scope.mediaSchedule.Column > 0 && $scope.mediaSchedule.RateCardCost > 0) {
                    $scope.mediaSchedule.AdCost = $scope.mediaSchedule.Column * $scope.mediaSchedule.Inches * $scope.mediaSchedule.RateCardCost;
                }
            }
            else {
                $scope.mediaSchedule.AdCost = $scope.mediaSchedule.RateCardCost;
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }
    $scope.changeNoOfInserts = function () {
        try {
            $scope.mediaSchedule.AdCost = ($scope.mediaSchedule.NoOfInserts * $scope.mediaSchedule.RateCardCost / 1000);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }
    $scope.changeClassifiedText = function () {
        try {
            if ($scope.mediaSchedule.ClassifiedText != undefined) {
                var temp = $scope.mediaSchedule.ClassifiedText.split(' ');
                if (temp.length > 0 && temp != "") {
                    $scope.TotalWords = temp.length;
                    if (temp.length > $scope.NoOfWord) {
                        $scope.mediaSchedule.RateCardCost = (($scope.mediaSchedule.BaseCost) + (((temp.length - $scope.NoOfWord) / $scope.AdditionalWord) * ($scope.mediaSchedule.PerWordCost)));
                        $scope.mediaSchedule.AdCost = $scope.mediaSchedule.RateCardCost;
                    }
                    else {
                        $scope.mediaSchedule.RateCardCost = $scope.mediaSchedule.BaseCost;
                        $scope.mediaSchedule.AdCost = $scope.mediaSchedule.RateCardCost;
                    }
                }
                else {
                    $scope.TotalWords = 0;
                }
            }
            else {
                $scope.TotalWords = 0;
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }
    function changeDropDownColor(controlId, controlValue) {
        try {
            if (controlValue != "" && controlValue != undefined && controlValue != null) {
                // id would be dynamic generated by select2
                var DropDown = angular.element(document.querySelector('#' + controlId));
                var select2ChosenDiv = DropDown.find('.select2-chosen');
                select2ChosenDiv.css('color', '#323232');
            } else {
                var DropDown = angular.element(document.querySelector('#' + controlId));
                var select2ChosenDiv = DropDown.find('.select2-chosen');
                select2ChosenDiv.css('color', '#999999');
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    }

    $scope.redirectToMediaOrderAdjustmentTab = function () {
        $location.path('ordersDetails/adjustment/' + $scope.mediaSchedule.AdOrderId + '');
    }
    function initSelect2(id, text) {
        var stateSelect2Button = angular.element(document.querySelector('#' + id))
        var selectedState = stateSelect2Button.find('span.select2-chosen');
        selectedState.text(text);
    }

    $scope.changeColInch = function () {
        try {
            $scope.mediaSchedule.AdSize = $scope.mediaSchedule.Column * $scope.mediaSchedule.Inches;
            if ($scope.mediaSchedule.Inches > 0 && $scope.mediaSchedule.Column > 0 && $scope.mediaSchedule.RateCardCost > 0) {
                $scope.mediaSchedule.AdCost = $scope.mediaSchedule.Column * $scope.mediaSchedule.Inches * $scope.mediaSchedule.RateCardCost;
            }
            //$scope.mediaSchedule.AdCost = $scope.mediaSchedule.RateCardCost;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    InitDateRange();
    GetAllMediaAsset();
    getData();

})