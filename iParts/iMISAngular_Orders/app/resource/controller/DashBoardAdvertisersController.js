﻿'use strict';

app.controller('DashBoardAdvertisersController', function DashBoardAdvertisersController($scope, $stateParams, $compile, $sce, $rootScope, $parse, $location,
    $exceptionHandler, $filter, authKeyService, mediaOrdersFactory, advertiserFactory, toastr) {

    $scope.$parent.setAdvertisersTabActive();

    var authKey = authKeyService.getConstantAuthKey();
    $scope.baseUrl = authKey.baseUrl;
    var authKey = authKeyService.getConstantAuthKey();
    var baseUrl = authKey.baseUrl;
    var authToken = authKey.authToken;
    $scope.dataLoading = true;
    $scope.isLoading = true;

    $scope.partyAndOrganizationData = [];
    $scope.advertisers = [];
    $scope.agencies = [];
    $scope.billingToContacts = [];
    $scope.organizationsIds = [];

    // Cards Inits
    $scope.searchText = "";
    $scope.createdOrders = [];
    $scope.organizationWiseCards = [];
    $scope.isShowDefaultTable = false;


    function getAllPartyDataThroughASI() {
        try {
            if ($rootScope.mainPartyAndOrganizationData.length <= 0 && $rootScope.mainAdvertisers.length <= 0 && $rootScope.mainAgencies.length <= 0
                && $rootScope.mainBillingToContacts.length <= 0) {
                var HasNext = true;
                var offset = 0;
                var advertisers = [];
                var contacts = [];
                function getPartyThroughASI() {
                    advertiserFactory.getAllAdvertiser1(baseUrl, authToken, offset, function (result) {
                        if (result != null && result != undefined && result != "") {
                            var ItemData = result.Items.$values
                            console.log(ItemData);
                            angular.forEach(ItemData, function (itemdatavalue, key) {
                                $scope.partyAndOrganizationData.push(itemdatavalue);
                                var type = itemdatavalue.$type;
                                if (type.indexOf('Asi.Soa.Membership.DataContracts.OrganizationData,') >= 0) {
                                    advertisers.push(itemdatavalue);
                                }
                                else if (type.indexOf('Asi.Soa.Membership.DataContracts.PersonData,') >= 0) {
                                    contacts.push(itemdatavalue);
                                }
                            });
                            $scope.advertisers = advertisers;
                            $scope.agencies = advertisers;
                            $scope.billingToContacts = contacts;
                            //$scope.personData = contacts;
                            var TotalCount = result.TotalCount;
                            HasNext = result.HasNext;
                            offset = result.Offset;
                            var Limit = result.Limit;
                            var NextOffset = result.NextOffset;
                            if (HasNext == true) {
                                offset = NextOffset;
                                getPartyThroughASI();
                            } else {
                                $rootScope.mainPartyAndOrganizationData = $scope.partyAndOrganizationData;
                                $rootScope.mainAdvertisers = $scope.advertisers;
                                $rootScope.mainAgencies = $scope.agencies;
                                $rootScope.mainBillingToContacts = $scope.billingToContacts;
                                getAllPartyStartWithEnglishAlphabet('A');
                                // getOrdersByAdvertisersIds();
                            }
                        }
                        else {
                            $scope.advertisers = [];
                            $scope.agencies = [];
                            $scope.billingToContacts = [];
                        }
                    })
                }
                getPartyThroughASI();
            } else {

                $scope.partyAndOrganizationData = $rootScope.mainPartyAndOrganizationData;
                $scope.advertisers = $rootScope.mainAdvertisers;
                $scope.agencies = $rootScope.mainAgencies;
                $scope.billingToContacts = $rootScope.mainBillingToContacts;
                getAllPartyStartWithEnglishAlphabet('A');
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getAllPartyStartWithEnglishAlphabet(firstWord) {
        try {
            if (firstWord != null && firstWord != "" && firstWord != 0 && firstWord != undefined) {
                var filterWord = firstWord + '%';
                var allPartyData = $scope.partyAndOrganizationData;
                var alphabetWiseFilterdata = alasql('SELECT * FROM ? AS add WHERE OrganizationName LIKE ?', [$scope.partyAndOrganizationData, filterWord]);
                if (alphabetWiseFilterdata.length > 0) {
                    var organizationIds = alasql('SELECT Id FROM ? AS add', [alphabetWiseFilterdata]);
                    var idsString = ""
                    angular.forEach(organizationIds, function (data, key) {
                        if (key == 0) {
                            idsString += data.Id;
                        }
                        else {
                            idsString += '/' + data.Id;
                        }
                    });
                    if (idsString != null && idsString != "" && idsString != 0 && idsString != undefined) {
                        getOrdersByAdvertisersIds(idsString);
                    }
                } else {
                    $scope.isShowDefaultTable = true;
                    $scope.dataLoading = false;
                }
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getAllPartyContainsEnglishAlphabet(firstWord) {
        try {
            if (firstWord != null && firstWord != "" && firstWord != 0 && firstWord != undefined) {
                var filterWord = '%' + firstWord + '%';
                var allPartyData = $scope.partyAndOrganizationData;
                var alphabetWiseFilterdata = alasql('SELECT * FROM ? AS add WHERE OrganizationName LIKE ?', [$scope.partyAndOrganizationData, filterWord]);
                if (alphabetWiseFilterdata.length > 0) {
                    var organizationIds = alasql('SELECT Id FROM ? AS add', [alphabetWiseFilterdata]);
                    var idsString = ""
                    angular.forEach(organizationIds, function (data, key) {
                        if (key == 0) {
                            idsString += data.Id;
                        }
                        else {
                            idsString += '/' + data.Id;
                        }
                    });
                    if (idsString != null && idsString != "" && idsString != 0 && idsString != undefined) {
                        getOrdersByAdvertisersIds(idsString);
                    }
                } else {
                    $scope.isShowDefaultTable = true;
                }
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getOrdersByAdvertisersIds(idsString) {
        try {
            if (idsString != null && idsString != "" && idsString != 0 && idsString != undefined) {
                mediaOrdersFactory.getOrdersByAdvertiser(idsString, function (result) {
                    if (result.statusCode == 1) {
                        if (result.data.length > 0) {
                            console.log(result.data);
                            $scope.createdOrders = result.data;
                            if ($scope.createdOrders.length > 0) {
                                getOrganizationforCreatedOrder();
                            }
                        } else {
                            $scope.createdOrders = [];
                            $scope.isShowDefaultTable = true;
                            $scope.dataLoading = false;
                        }
                    } else if (result.statusCode == 3) {
                        $scope.createdOrders = [];
                        $scope.isShowDefaultTable = true;
                        $scope.dataLoading = false;
                        console.log(result.message);
                    }
                    else {
                        $scope.createdOrders = [];
                        $scope.isShowDefaultTable = true;
                        $scope.dataLoading = false;
                        toastr.error(result.message, 'Error');
                    }
                })
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getOrganizationforCreatedOrder() {
        try {
            if ($scope.createdOrders.length > 0) {
                var allPartyData = $scope.partyAndOrganizationData;
                // var alphabetWiseFilterdata = alasql('SELECT * FROM ? AS add WHERE OrganizationName LIKE ?', [$scope.partyAndOrganizationData]);
                $scope.organizationWiseCards = [];
                angular.forEach($scope.createdOrders, function (data, key) {
                    var AdvertiserId = data.AdvertiserId;
                    var organizationData = alasql('SELECT * FROM ? AS add WHERE Id = ?', [$scope.partyAndOrganizationData, AdvertiserId]);
                    var contactId = data.ST_ID;
                    var contactData = alasql('SELECT * FROM ? AS add WHERE Id = ?', [$scope.partyAndOrganizationData, contactId]);
                    var GrossCost = data.GrossCost;
                    var NetCost = data.NetCost;
                    var NoOfOrders = data.NoOfOrders;
                    if (organizationData.length > 0 && contactData.length > 0) {
                        var obj = {};
                        obj.OrganizationId = AdvertiserId;
                        obj.OrganizationName = organizationData[0].OrganizationName;
                        obj.contactId = contactId;
                        obj.contactName = contactData[0].Name;
                        obj.GrossCost = GrossCost;
                        obj.NetCost = NetCost;
                        obj.NoOfOrders = NoOfOrders;
                        $scope.organizationWiseCards.push(obj);
                    }
                    $scope.isShowDefaultTable = false;
                    $scope.dataLoading = false;
                });
                initPagination();
            } else {
                $scope.isShowDefaultTable = true;
                $scope.dataLoading = false;
            }
            if ($scope.organizationWiseCards.length == 0) {
                $scope.isShowDefaultTable = true;
                $scope.dataLoading = false;
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.filterDataAlphabetsWise = function (startWith) {
        try {
            getAllPartyStartWithEnglishAlphabet(startWith);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onEnterGetAllPartySearchWise = function (words, keyEvent) {
        try {
            if (keyEvent.which === 13) {
                keyEvent.preventDefault();
                if ($scope.partyAndOrganizationData.length != 0) {
                    if (words != "" && words != undefined && words != null) {
                        getAllPartyContainsEnglishAlphabet(words);
                    } else {
                        getAllPartyStartWithEnglishAlphabet('A');
                    }
                } else {
                    getAllPartyDataThroughASI();
                }
                return false;
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onClickGetAllPartySearchWise = function (words) {
        try {
            if ($scope.partyAndOrganizationData.length != 0) {
                if (words != "" && words != undefined && words != null) {
                    getAllPartyContainsEnglishAlphabet(words);
                } else {
                    getAllPartyStartWithEnglishAlphabet('A');
                }
            } else {
                getAllPartyDataThroughASI();
            }
            return false;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.CreateNewOrder = function (objOrder) {
        try {
            $rootScope.dashboardOrganizationId = objOrder.OrganizationId;
            $rootScope.dashboardContactId = objOrder.contactId;
            $rootScope.GrossCost = (0).toFixed(2);
            $rootScope.NetCost = (0).toFixed(2);
            $rootScope.fromDashboard = false;
            $location.path('ordersDetails/orders/');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.redirectToOrderTabByAdvertiser = function (objfilter) {
        var id = objfilter.OrganizationId;
        if (id != null && id != undefined && id != "") {
            var aID = id;
            aID = parseInt(aID);
            if (parseInt(aID) > 0) {
                $location.path('dashBoard/Orders/' + aID + '');
            } else {
                $location.path('dashBoard/Orders/' + aID + '');
            }
        } else {
            $location.path('dashBoard/Orders/' + aID + '');
        }
    }

    function initPagination() {
        try {
            //pagination
            var sortingOrder = 'OrganizationName';
            $scope.sortingOrder = sortingOrder;
            $scope.reverse = false;
            $scope.filteredItems = [];
            $scope.groupedItems = [];
            $scope.itemsPerPage = 9;
            $scope.pagedItems = [];
            $scope.currentPage = 0;
            if ($scope.organizationWiseCards.length > 0) {
                $scope.items = $scope.organizationWiseCards;
            } else {
                $scope.items = [];
            }

            var searchMatch = function (haystack, needle) {
                try {
                    if (!needle) {
                        return true;
                    }
                    return haystack.toString().toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                    // return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            // init the filtered items
            $scope.search = function () {
                try {
                    $scope.filteredItems = $filter('filter')($scope.items, function (item) {
                        for (var attr in item) {
                            if (searchMatch(item[attr], $scope.query))
                                return true;
                        }
                        return false;
                    });
                    // take care of the sorting order
                    if ($scope.sortingOrder !== '') {
                        $scope.filteredItems = $filter('orderBy')($scope.filteredItems, $scope.sortingOrder, $scope.reverse);
                    }
                    $scope.currentPage = 0;
                    // now group by pages
                    $scope.groupToPages();
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            // calculate page in place
            $scope.groupToPages = function () {
                try {
                    $scope.pagedItems = [];

                    for (var i = 0; i < $scope.filteredItems.length; i++) {
                        if (i % $scope.itemsPerPage === 0) {
                            $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [$scope.filteredItems[i]];
                        } else {
                            $scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.filteredItems[i]);
                        }
                    }
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            $scope.range = function (start, end) {
                try {
                    var ret = [];
                    if (!end) {
                        end = start;
                        start = 0;
                    }
                    for (var i = start; i < end; i++) {
                        ret.push(i);
                    }
                    return ret;
                }
                catch (e) {
                    $scope.isLoading = false;
                    $exceptionHandler(e);
                }
            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pagedItems.length - 1) {
                    $scope.currentPage++;
                }
            };

            $scope.setPage = function () {
                $scope.currentPage = this.n;
            };

            // functions have been describe process the data for display
            $scope.search();


            //// change sorting order
            //$scope.sort_by = function (newSortingOrder) {
            //    if ($scope.sortingOrder == newSortingOrder)
            //        $scope.reverse = !$scope.reverse;

            //    $scope.sortingOrder = newSortingOrder;

            //    // icon setup
            //    $('th i').each(function () {
            //        // icon reset
            //        $(this).removeClass().addClass('icon-sort');
            //    });
            //    if ($scope.reverse)
            //        $('th.' + new_sorting_order + ' i').removeClass().addClass('icon-chevron-up');
            //    else
            //        $('th.' + new_sorting_order + ' i').removeClass().addClass('icon-chevron-down');
            //};
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    getAllPartyDataThroughASI();
    initPagination();

})