﻿'use strict';

app.factory('mediaOrderLinesFactory', function ($http, $rootScope) {

    var url = "https://localhost:444/api/mediaorderlines";//sf.getServiceRoot('ue') + "api/MediaAsset";
    var url1 = "https://localhost:444/api";
    var cacheData = {};
    var individualAreaDetailCache = {};
    var cacheDataDistinct = null;

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    var returnadvertiserFactory = {
        saveMediaOrderLines: saveMediaOrderLines,
        deleteMediaOrderLinesbyMediaOrder: deleteMediaOrderLinesbyMediaOrder,
        getMediaOrderLinesbyOrderId: getMediaOrderLinesbyOrderId,
        deleteMediaOrderLinesbyMediaOrderLineId: deleteMediaOrderLinesbyMediaOrderLineId,
        moveUpOrDownMediaOrderLines: moveUpOrDownMediaOrderLines
    }
    return returnadvertiserFactory;


    function saveMediaOrderLines(data, callback) {
        $http({
            url: url + '',
            // url: asi_URL,
            dataType: 'json',
            method: 'POST',
            data: data,
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function deleteMediaOrderLinesbyMediaOrder(morderId, callback) {
        $http({
            //url: url + "/bymediaorderid/" + morderId,
            url: url1 + "/mediaorder/" + morderId,
            dataType: 'json',
            method: 'DELETE',
            data: '',
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function getMediaOrderLinesbyOrderId(orderId, callback) {
        $http({
            url: url + "/getbymediaorder/" + orderId,
            dataType: 'json',
            method: 'GET',
            data: orderId,
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function deleteMediaOrderLinesbyMediaOrderLineId(mediaOrderLineIds, callback) {
        $http({
            url: url + "/removebymedialine/",
            // url: 'https://localhost:444/api/mediaorderlines/removebymedialine',
            dataType: 'json',
            method: 'POST',
            data: mediaOrderLineIds,
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function moveUpOrDownMediaOrderLines(mediaOrderLines, callback) {
        $http({
            url: url + "/updatemediaorderlines/",
            dataType: 'json',
            method: 'PUT',
            data: mediaOrderLines,
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

});

//update: function (adAdjustments, callback) {

//    return $http({
//        url: url + '',
//        method: 'PUT',
//        data: adAdjustments,
//        headers: sfHeaders,
//        cache: false
//    }).success(function (result) {
//        callback(result);
//    });
//},
//        delete: function (AdAdjustmentId, callback) {

//            return $http({
//                url: url + '/' + AdAdjustmentId,
//                method: 'DELETE',
//                headers: sfHeaders,
//                cache: false
//            }).success(function (result) {
//                callback(result);
//            });
//        },