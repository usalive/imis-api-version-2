﻿'use strict';

app.factory('adColorFactory', function ($http, $rootScope) {


    var url = "https://localhost:444/api/AdColor";//sf.getServiceRoot('ue') + "api/MediaAsset";
    var cacheData = {};
    var individualAreaDetailCache = {};
    var cacheDataDistinct = null;

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getAll: function (callback) {
            $http({
                url: url,
                dataType: 'json',
                method: 'GET',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            }).success(function (result) {
                callback(result);
            }).error(function (error) {
                alert(error);
            });

        },
        getbyRatecardandAdsize: function (rateCardId, adSizeId, callback) {

            $http({
                url: url + '/getbyratecardandadsize/' + rateCardId + '/' + adSizeId,
                method: 'GET',
                headers: sfHeaders,
                params: {},
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        save: function (adColor, callback) {

            return $http({
                url: url + '',
                method: 'POST',
                data: adColor,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        update: function (adColor, callback) {

            return $http({
                url: url + '',
                method: 'PUT',
                data: adColor,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        delete: function (AdColorId, callback) {
            return $http({
                url: url + '/' + AdColorId,
                method: 'DELETE',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
    };
});