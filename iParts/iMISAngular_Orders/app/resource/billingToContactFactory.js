﻿'use strict';

app.factory('billingToContactFactory', function ($http, $rootScope) {

    var url = "https://localhost:444/api/MediaAsset";//sf.getServiceRoot('ue') + "api/MediaAsset";
    var cacheData = {};
    var individualAreaDetailCache = {};
    var cacheDataDistinct = null;

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getAll: getAll,

        //getAll: function (callback) {
        //    $http({
        //        url: url,
        //        dataType: 'json',
        //        method: 'GET',
        //        data: '',
        //        headers: {
        //            "Content-Type": "application/json"
        //        }
        //    }).success(function (result) {
        //        callback(result);
        //    }).error(function (error) {
        //        alert(error);
        //    });
        //},

        save: function (baseUrl, authToken, contact, callback) {
            return $http({
                url: baseUrl + "api/party",
                dataType: 'json',
                method: 'POST',
                data: contact,
                headers: {
                    "Content-Type": "application/json",
                    'RequestVerificationToken': authToken
                }
            }).success(function (result) {
                callback(result);
            }).error(function (error) {
                alert(error);
            });
        }
        //update: function (order, callback) {

        //    return $http({
        //        url: url + '',
        //        method: 'PUT',
        //        data: order,
        //        headers: sfHeaders,
        //        cache: false
        //    }).success(function (result) {
        //        callback(result);
        //    });
        //},

        //delete: function (orderId, callback) {
        //    return $http({
        //        url: url + '/' + orderId,
        //        method: 'DELETE',
        //        headers: sfHeaders,
        //        cache: false
        //    }).success(function (result) {
        //        callback(result);
        //    });
        //},

        //getAllItemsummary: function (baseUrl, authToken, callback) {
        //    $http({
        //        //url: "/Scripts/itemCode.txt",
        //        url: baseUrl + "api/Itemsummary?limit=1000000",
        //        dataType: 'json',
        //        method: 'GET',
        //        data: '',
        //        headers: {
        //            "Content-Type": "application/json",
        //            'RequestVerificationToken': authToken
        //        }
        //    }).success(function (result) {
        //        callback(result);
        //    }).error(function (error) {
        //        console.log(error);
        //    });
        //},
    };

    function getAll() {
        var objdata = {};
        var Arraydata = [];
        objdata.id = 1;
        objdata.Name = "Billing To Contact1";
        Arraydata.push(objdata);
        objdata = {};
        objdata.id = 2;
        objdata.Name = "Billing To Contact2";
        Arraydata.push(objdata);
        objdata = {};
        objdata.id = 3;
        objdata.Name = "Billing To Contact3";
        Arraydata.push(objdata);
        objdata = {};
        objdata.id = 4;
        objdata.Name = "Billing To Contact4";
        Arraydata.push(objdata);
        objdata = {};
        objdata.id = 5;
        objdata.Name = "Billing To Contact5";
        Arraydata.push(objdata);
        return Arraydata;
    }

});