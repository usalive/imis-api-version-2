﻿'use strict';

app.factory('mediaOrdersFactory', function ($http, $rootScope) {

    var url = "https://localhost:444/api/mediaorder";//sf.getServiceRoot('ue') + "api/MediaAsset";
    var cacheData = {};
    var individualAreaDetailCache = {};
    var cacheDataDistinct = null;

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        updateMediaOrderStatus: updateMediaOrderStatus,
        getOrdersByAdvertiser: getOrdersByAdvertiser,
        getAllOrdersbuyIdWise: getAllOrdersbuyIdWise,
        getOrdersByMediaOrderId: getOrdersByMediaOrderId,
        getOrdersByBuyId: getOrdersByBuyId,
        getAllOrdersMediaAssetIdWise: getAllOrdersMediaAssetIdWise,
        getMediaOrdersAdvertiser: getMediaOrdersAdvertiser,
        getMediaOrdersAdType: getMediaOrdersAdType,
        getDataForPostOrdersDropDown: getDataForPostOrdersDropDown,
        postorderforcopy: postorderforcopy,
        getAllOrdersProductionStatusdWise: getAllOrdersProductionStatusdWise,
    };

    function updateMediaOrderStatus(mediaOrderIds, callback) {
        $http({
            url: url + "/changestatus/",
            dataType: 'json',
            method: 'PUT',
            data: mediaOrderIds,
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function getOrdersByAdvertiser(ids, callback) {
        $http({
            url: url + "/GetOrdersByAdvertisor?advertiserId=" + ids,
            dataType: 'json',
            method: 'GET',
            data: '',
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function getAllOrdersbuyIdWise(data, callback) {
        $http({
            url: url + "/getordersbuyidwise",
            dataType: 'json',
            method: 'POST',
            data: data,
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function getOrdersByMediaOrderId(mediaOrderId, callback) {
        $http({
            url: url + "/" + mediaOrderId,
            dataType: 'json',
            method: 'GET',
            data: '',
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function getOrdersByBuyId(buyId, callback) {
        $http({
            url: url + "/getbybuyid/" + buyId,
            dataType: 'json',
            method: 'GET',
            data: '',
            async:false,
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function getAllOrdersMediaAssetIdWise(data, callback) {
        $http({
            url: url + "/getordersforpostorder",
            dataType: 'json',
            method: 'POST',
            data: data,
            async: false,
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function getAllOrdersProductionStatusdWise(data, callback) {
        $http({
            url: url + "/getordersbyproductionstatus",
            dataType: 'json',
            method: 'POST',
            data: data,
            async: false,
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': $rootScope.authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function getMediaOrdersAdvertiser(baseUrl, authToken, callback) {
        $http({
            url: url + "/getalladvertiserforpostorder",
            dataType: 'json',
            method: 'GET',
            data: '',
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function getMediaOrdersAdType(baseUrl, authToken, callback) {
        $http({
            url: url + "/getalladtypeforpostorder",
            dataType: 'json',
            method: 'GET',
            data: '',
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }

    function getDataForPostOrdersDropDown(data, baseUrl, authToken, callback) {
        $http({
            url: url + "/filldropdownforpostorder",
            dataType: 'json',
            method: 'POST',
            data: data,
            headers: {
                "Content-Type": "application/json",
                'RequestVerificationToken': authToken
            }
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }
    function postorderforcopy(MediaOrderData, callback) {
        $http({
            url: url + "/postorderforcopy",
            method: 'POST',
            data: MediaOrderData,
            headers: sfHeaders,
            cache: false
        }).success(function (result) {
            callback(result);
        }).error(function (error) {
            console.log(error);
        });
    }  

});