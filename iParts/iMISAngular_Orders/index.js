"use strict";

var app = angular.module("ordersApp", ["ui.router", "angular-toArrayFilter", "ngMessages", "ui.select2", "toastr", "ngAnimate", "nsPopover"])

.run(function ($rootScope, $timeout) {
    $rootScope.flag = true;
    var Url = JSON.parse(angular.element(document.querySelector("#__ClientContext")).val()).baseUrl;
    var auth = angular.element(document.querySelector("#__RequestVerificationToken")).val();
    $rootScope.iMISbaseUrl = Url;
    $rootScope.authToken = auth;
    $rootScope.billingDetails = {};
    $rootScope.mainArray = [];
    $rootScope.MediaScheduleTab = [];
    $rootScope.GrossCost = (0).toFixed(2);
    $rootScope.NetCost = (0).toFixed(2);

    $rootScope.mainPartyAndOrganizationData = [];
    $rootScope.mainAdvertisers = [];
    $rootScope.mainAgencies = [];
    $rootScope.mainBillingToContacts = [];

    $rootScope.dashboardOrganizationId = 0;
    $rootScope.dashboardContactId = 0;

});

app.provider("authKeyService", function () {
    var privateUserList = [];
    var appConstant = {};

    var Url = JSON.parse(angular.element(document.querySelector("#__ClientContext")).val()).baseUrl;
    var auth = angular.element(document.querySelector("#__RequestVerificationToken")).val();
    appConstant.baseUrl = Url;
    appConstant.authToken = auth;

    this.getAuthKey = function (username, email) {
        return appConstant;
    };

    this.$get = function () {
        return {
            getConstantAuthKey: function () {
                return appConstant;
            },
        };
    };
});

app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', 'authKeyServiceProvider', 'toastrConfig', function ($stateProvider, $urlRouterProvider,
    $locationProvider, authKeyServiceProvider, toastrConfig) {
    //$locationProvider.hashPrefix('');

    //$locationProvider.html5Mode({
    //    enabled: true,
    //    requireBase: false,
    //    rewriteLinks: false
    //});
    //$urlMatcherFactoryProvider.type('SlashFix', {
    //    raw: true,
    //});

    var curUrl = window.location.pathname + window.location.search;
    var authKey = authKeyServiceProvider.getAuthKey();
    var baseUrl = authKey.baseUrl;
    var authToken = authKey.authToken;

    $stateProvider
        //dashboard tab router
        .state('dashBoard', {
            url: '/dashBoard',
            templateUrl: baseUrl + 'Areas/iMISAngular_Orders/app/views/dashboardTab.html',
            controller: 'DashBoardTabController',
        })
         .state('dashBoard.Advertisers', {
             url: '/Advertisers',
             templateUrl: baseUrl + 'Areas/iMISAngular_Orders/app/views/dashboardAdvertisers.html',
             controller: 'DashBoardAdvertisersController',
         })
        //.state('dashBoard.Orders', {
        //    url: '/Orders',
        //    templateUrl: baseUrl + 'Areas/iMISAngular_Orders/app/views/dashboardOrders.html',
        //    controller: 'DashBoardOrdersController',
        //})
         .state('dashBoard.Orders', {
             url: '/Orders/:AdvertiserId',
             templateUrl: baseUrl + 'Areas/iMISAngular_Orders/app/views/dashboardOrders.html',
             controller: 'DashBoardOrdersController',
         })

        .state('dashBoard.PostOrders', {
            url: '/PostOrders',
            templateUrl: baseUrl + 'Areas/iMISAngular_Orders/app/views/PostOrders.html',
            controller: 'PostOrdersController',
        })
        .state('dashBoard.MissingMaterials', {
            url: '/MissingMaterials',
            templateUrl: baseUrl + 'Areas/iMISAngular_Orders/app/views/MissingMaterials.html',
            controller: 'MissingMaterialsController',
        })

        //orders tab router
         .state('ordersDetails', {
             url: '/ordersDetails',
             templateUrl: baseUrl + 'Areas/iMISAngular_Orders/app/views/orderTab.html',
             controller: 'OrderTabController',
         })
      .state('ordersDetails.orders', {
          url: '/orders/:AdOrderId',
          templateUrl: baseUrl + 'Areas/iMISAngular_Orders/app/views/orders.html',
          controller: 'OrdersController',
          controllerAs: "vm",
      })
    .state('ordersDetails.mediaSchedule', {
        url: '/mediaSchedule/:AdOrderId/:MediaOrderId',
        templateUrl: baseUrl + 'Areas/iMISAngular_Orders/app/views/mediaSchedule.html',
        controller: 'MediaScheduleController',
        controllerAs: "vm",
    })
       .state('ordersDetails.adjustment', {
           url: '/adjustment/:AdOrderId',
           templateUrl: baseUrl + 'Areas/iMISAngular_Orders/app/views/adjustment.html',
           controller: 'AdjustmentController',
           controllerAs: "vm",
       })
     .state('ordersDetails.production', {
         url: '/production/:AdOrderId',
         templateUrl: baseUrl + 'Areas/iMISAngular_Orders/app/views/production.html',
         controller: 'ProductionController',
         controllerAs: "vm",
     })

    //.state('dashBoardChart', {
    //    url: '/dashBoardchart',
    //    templateUrl: baseUrl + 'Areas/iMISAngular_Orders/app/views/dashboardchart.html',
    //    controller: 'DashBoardTabController',
    //})

    //$urlRouterProvider.otherwise('ordersDetails/orders');
    $urlRouterProvider.otherwise('dashBoard/Advertisers');

    angular.extend(toastrConfig, {
        autoDismiss: false,
        containerId: 'toast-container',
        maxOpened: 0,
        newestOnTop: true,
        positionClass: 'toast-top-right',
        preventDuplicates: false,
        preventOpenDuplicates: false,
        target: 'body',
        closeButton: true,
        iconClasses: {
            error: 'toast-error',
            info: 'toast-info',
            success: 'toast-success',
            warning: 'toast-warning'
        },
        progressBar: true
    });
}])
