"use strict";

var app = angular.module("mediaAssetsApp", ["ui.router", "angular-toArrayFilter", "ngMessages", "ui.select2", "toastr"])

app.run(function ($rootScope, $timeout, $window, $location) {
    $rootScope.flag = true;
    var Url = JSON.parse(angular.element(document.querySelector("#__ClientContext")).val()).baseUrl;
    var auth = angular.element(document.querySelector("#__RequestVerificationToken")).val();
    $rootScope.iMISbaseUrl = Url;
  
});

app.provider("authKeyService", function () {
    var privateUserList = [];
    var appConstant = {};

    var Url = JSON.parse(angular.element(document.querySelector("#__ClientContext")).val()).baseUrl;
    var auth = angular.element(document.querySelector("#__RequestVerificationToken")).val();
    appConstant.baseUrl = Url;
    appConstant.authToken = auth;

    this.getAuthKey = function (username, email) {
        return appConstant;
    };

    this.$get = function () {
        return {
            getConstantAuthKey: function () {
                return appConstant;
            },
        };
    };
});

app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', 'authKeyServiceProvider', 'toastrConfig', '$urlMatcherFactoryProvider',
function ($stateProvider, $urlRouterProvider, $locationProvider, authKeyServiceProvider, toastrConfig, $urlMatcherFactoryProvider) {
    //$locationProvider.hashPrefix('');

    //$urlMatcherFactoryProvider.type('SlashFix', {
    //    raw: true,
    //});

    //function valToString(val) { return val != null ? val.toString() : val; }
    //function regexpMatches(val) { /*jshint validthis:true */ return this.pattern.test(val); }
    //$urlMatcherFactory.type("uriType", {
    //    encode: valToString, decode: valToString, is: regexpMatches, pattern: /.*/
    //});
    //$urlMatcherFactoryProvider.strictMode(true);
    //$locationProvider.html5Mode({
    //    enabled: true,
    //    requireBase: false,
    //    rewriteLinks: false
    //});
    //$urlMatcherFactoryProvider.type('SlashFix', {
    //    raw: true,
    //});

    var curUrl = window.location.pathname + window.location.search;
    var authKey = authKeyServiceProvider.getAuthKey();
    var baseUrl = authKey.baseUrl;

    $stateProvider
        .state('mediaAssetsDetails', {
            url: '/mediaAssetsDetails',
            templateUrl: baseUrl + 'Areas/iMISAngular/app/views/mediaAssetTab.html',
            controller: 'MediaAssetsTabController',
        })
      .state('mediaAssetsDetails.mediaAssets', {
          url: '/mediaAssets',
          templateUrl: baseUrl + 'Areas/iMISAngular/app/views/mediaAssets.html',
          controller: 'MediaAssetsController',
          controllerAs: "vm"
      })
        .state('mediaAssetsDetails.issueDate', {
            url: '/issueDate/:mediaAssetId',
            templateUrl: baseUrl + 'Areas/iMISAngular/app/views/issueDates.html',
            controller: 'IssueDatesController',
            controllerAs: "vm"
        })
        .state('mediaAssetsDetails.AdAdjustment', {
            url: '/AdAdjustment/:mediaAssetId',
            templateUrl: baseUrl + 'Areas/iMISAngular/app/views/AdAdjustment.html',
            controller: 'AdAdjustmentsController',
            controllerAs: "vm"
        })
      .state('mediaAssetsDetails.rateCard', {
          url: '/rateCard/:mediaAssetId',
          templateUrl: baseUrl + 'Areas/iMISAngular/app/views/rateCard.html',
          controller: 'RateCardController',
          controllerAs: "vm"
      })
       .state('mediaAssetsDetails.rateCardsDetails', {
           url: '/rateCardsDetails/:RateCardId',
           templateUrl: baseUrl + 'Areas/iMISAngular/app/views/rateCardsDetails.html',
           controller: 'RateCardsDetailsController',
           controllerAs: "vm"
       })
      .state('productionManagement', {
          url: '/productionManagement',
          templateUrl: baseUrl + 'Areas/iMISAngular/app/views/productionManagement.html',
          controller: 'ProductionManagementController',
          controllerAs: "vm"
      })

      .state('repsTerritory', {
          url: '/repsTerritory',
          templateUrl: baseUrl + 'Areas/iMISAngular/app/views/repsTerritory.html',
          controller: 'RepTerritoriesController',
          controllerAs: "vm"
      })
    $urlRouterProvider.otherwise('mediaAssetsDetails/mediaAssets');

    angular.extend(toastrConfig, {
        autoDismiss: false,
        containerId: 'toast-container',
        maxOpened: 0,
        newestOnTop: true,
        positionClass: 'toast-top-right',
        preventDuplicates: false,
        preventOpenDuplicates: false,
        target: 'body',
        closeButton: true,
        iconClasses: {
            error: 'toast-error',
            info: 'toast-info',
            success: 'toast-success',
            warning: 'toast-warning'
        },
        progressBar: true
    });
}])

