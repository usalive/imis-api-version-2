﻿'use strict';

app.controller('AdAdjustmentsController', function AdAdjustmentsController($scope, $stateParams, mediaAssetsFactory, adAdjustmentsFactory,
    $exceptionHandler, adAdjustmentsTargetFactory, authKeyService, toastr) {

    $scope.AddOREdit = "ADD";
    $scope.adAdjustments = {};
    $scope.adAdjustment = {};
    $scope.adAdjustment.AdAdjustmentId = 0;
    $scope.adAdjustment.AmountPercent = 0;
    $scope.adAdjustment.SurchargeDiscount = 0;
    $scope.adAdjustment.MediaAssetId = parseInt($stateParams.mediaAssetId);
    $scope.mediaAssetItemCode = [];
    $scope.adAdjustmentsTargets = {};
    $scope.MediaAssetName = "";
    $scope.showMsgs = false;
    $scope.isShowDefaultTable = false;
    var authKey = authKeyService.getConstantAuthKey();
    $scope.baseUrl = authKey.baseUrl;
    var baseUrl = authKey.baseUrl;
    var authToken = authKey.authToken;
    $scope.isLoading = true;
    $scope.dataLoading = true;

    $scope.productCodeSelect2 = {
        multiple: false,
        formatSearching: 'Searching the product Code...',
        formatNoMatches: 'No product code found'
    };
    $scope.adjustmentTargetSelect2 = {
        multiple: false,
        formatSearching: 'Searching the Adjustment Target...',
        formatNoMatches: 'No Adjustment Target found'
    };

    $scope.$parent.tabMediaAssetId = $stateParams.mediaAssetId;
    $scope.$parent.setAdAdjustmentTabActive();
    initSelect2("s2id_productcode", "Select Product Code");
    initSelect2("s2id_adjustmentstarget", "Select Adjustments Target");
    changeSelect2DropDownColor("s2id_productcode", "");
    changeSelect2DropDownColor("s2id_adjustmentstarget", "");

    function GetMediaAssetItemData() {
        try {
            var HasNext = true;
            var offset = 0;
            var advertisers = [];
            var contacts = [];
            function getItemDataASI() {
                mediaAssetsFactory.getAllItemsummary(baseUrl, authToken, offset, function (result) {
                    if (result != null && result != undefined && result != "") {
                        var ItemData = result.Items.$values
                        console.log(ItemData);
                        angular.forEach(ItemData, function (itemdatavalue, key) {
                            $scope.mediaAssetItemCode.push(itemdatavalue);
                        });
                        var TotalCount = result.TotalCount;
                        HasNext = result.HasNext;
                        offset = result.Offset;
                        var Limit = result.Limit;
                        var NextOffset = result.NextOffset;
                        if (HasNext == true) {
                            offset = NextOffset;
                            getItemDataASI();
                        } else {

                        }
                    }
                    else {
                        $scope.mediaAssetItemCode = [];
                    }
                })
            }
            getItemDataASI();
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function GetAdAdjustmentsData(mediaAssetId) {
        try {
            adAdjustmentsFactory.getByMediaAsset(mediaAssetId, function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data.length > 0) {

                        $scope.adAdjustments.MediaAssetId = result.data[0].MediaAssetId;
                        $scope.MediaAssetName = result.data[0].MediaAssetName;
                        if (result.data[0].AdAdjustments.length > 0) {
                            $scope.adAdjustments = result.data[0].AdAdjustments;
                            $scope.isShowDefaultTable = false;
                            $scope.dataLoading = false;
                        }
                        else {
                            $scope.adAdjustments = [];
                            $scope.isShowDefaultTable = true;
                            $scope.dataLoading = false;
                        }
                        //$scope.adAdjustments = result.data;
                        //$scope.MediaAssetName = result.data[0].MediaAssetName;
                        //$scope.isShowDefaultTable = false;
                    } else {
                        $scope.adAdjustments = result.data;
                        $scope.isShowDefaultTable = true;
                        $scope.dataLoading = false;
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.adAdjustments = result.data;
                    $scope.isShowDefaultTable = true;
                    $scope.dataLoading = false;
                }
                else {
                    $scope.adAdjustments = [];
                    $scope.isShowDefaultTable = true;
                    $scope.dataLoading = false;
                    //  alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function GetAdAdjustmentsTargetData() {
        try {
            adAdjustmentsTargetFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    $scope.adAdjustmentsTargets = result.data;
                }
                else {
                    //alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function UpdateAdAdjustmentButtonName() {
        try {
            var adAdjustmentButton = angular.element(document.querySelector('#btnAdAdjustment'));
            adAdjustmentButton.text('Update');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function CreateAdAdjustmentButtonName() {
        try {
            var adAdjustmentButton = angular.element(document.querySelector('#btnAdAdjustment'));
            adAdjustmentButton.text('Create');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function itemDataSelect() {
        try {
            var mediaAssetButton = angular.element(document.querySelector('#select2-chosen-2'));
            mediaAssetButton.text("Select Product Code");
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.SaveAdjustments = function (adAdjustment) {
        try {
            if ($scope.adAdjustmentsForm.$valid) {
                console.log($scope.adAdjustment);
                if (adAdjustment.AdAdjustmentId == 0) {
                    adAdjustmentsFactory.save(adAdjustment, function (result) {
                        if (result.statusCode == 1) {
                            GetAdAdjustmentsData($scope.adAdjustment.MediaAssetId);
                            $scope.AddOREdit = "ADD";
                            $scope.adAdjustment = {};
                            $scope.adAdjustment.AdAdjustmentId = 0;
                            $scope.adAdjustment.AmountPercent = 0;
                            $scope.adAdjustment.SurchargeDiscount = 0;
                            initSelect2("s2id_productcode", "Select Product Code");
                            initSelect2("s2id_adjustmentstarget", "Select Adjustments Target");
                            changeSelect2DropDownColor("s2id_productcode", "");
                            changeSelect2DropDownColor("s2id_adjustmentstarget", "");
                            // itemDataSelect();
                            $scope.adAdjustment.MediaAssetId = parseInt($stateParams.mediaAssetId);
                            $scope.adAdjustmentsForm.$setPristine();
                            $scope.showMsgs = false;
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else {
                            toastr.error(result.message, 'Error');
                        }
                    })
                } else {
                    adAdjustmentsFactory.update(adAdjustment, function (result) {
                        if (result.statusCode == 1) {
                            GetAdAdjustmentsData($scope.adAdjustment.MediaAssetId);
                            $scope.AddOREdit = "ADD";
                            $scope.adAdjustment = {};
                            $scope.adAdjustment.AdAdjustmentId = 0;
                            $scope.adAdjustment.AmountPercent = 0;
                            $scope.adAdjustment.SurchargeDiscount = 0;
                            initSelect2("s2id_productcode", "Select Product Code");
                            initSelect2("s2id_adjustmentstarget", "Select Adjustments Target");
                            changeSelect2DropDownColor("s2id_productcode", "");
                            changeSelect2DropDownColor("s2id_adjustmentstarget", "");
                            // itemDataSelect();
                            $scope.adAdjustment.MediaAssetId = parseInt($stateParams.mediaAssetId);
                            CreateAdAdjustmentButtonName();
                            $scope.adAdjustmentsForm.$setPristine();
                            $scope.showMsgs = false;
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else {
                            toastr.error(result.message, 'Error');
                        }
                    })
                }
            }
            else {
                $scope.showMsgs = true;
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.EditadAdjustments = function (adAdjustment) {
        try {
            adAdjustment.AdjustmentTarget = adAdjustment.AdjustmentTarget;
            UpdateAdAdjustmentButtonName();
            $scope.adAdjustment = adAdjustment;

            var productitem = adAdjustment.ProductCode;
            var itemData = $scope.mediaAssetItemCode;
            var itemcode = '';
            var itemName = '';
            var mergeName = '';
            angular.forEach(itemData, function (items, key2) {
                if (items.ItemId == productitem) {
                    itemcode = items.ItemCode;
                    itemName = items.Name;
                    mergeName = itemcode + " " + "(" + itemName + ")";
                }
            });
            //var mediaAssetButton = angular.element(document.querySelector('#select2-chosen-2'));
            // mediaAssetButton.text(mergeName);
            initSelect2("s2id_productcode", mergeName);
            changeSelect2DropDownColor("s2id_productcode", itemcode);
            initSelect2("s2id_adjustmentstarget", adAdjustment.AdjustmentTarget);
            changeSelect2DropDownColor("s2id_adjustmentstarget", adAdjustment.AdjustmentTarget);
            $scope.AddOREdit = "EDIT";
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function initSelect2(id, text) {
        try {
            var stateSelect2Button = angular.element(document.querySelector('#' + id))
            var selectedState = stateSelect2Button.find('span.select2-chosen');
            selectedState.text(text);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.DeleteadAdjustments = function (objadAdjustment) {
        try {
            console.log($scope.objadAdjustment);
            var selectedadAdjustmentDate = $scope.adAdjustment;
            bootbox.confirm({
                message: "Are you sure you want to delete this Media Asset?",
                //message: "Are you sure you want to delete the selected object(s) and all of their children?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        adAdjustmentsFactory.delete(objadAdjustment.AdAdjustmentId, function (result) {
                            if (result.statusCode == 1) {
                                var UpdateRecord = selectedadAdjustmentDate.AdAdjustmentId;
                                var deletedRecord = objadAdjustment.AdAdjustmentId;
                                if (UpdateRecord == deletedRecord) {
                                    $scope.adAdjustment = {};
                                    $scope.adAdjustment.AdAdjustmentId = 0;
                                    $scope.adAdjustment.AmountPercent = 0;
                                    $scope.adAdjustment.SurchargeDiscount = 0;
                                    initSelect2("s2id_productcode", "Select Product Code");
                                    initSelect2("s2id_adjustmentstarget", "Select Adjustments Target");
                                    changeSelect2DropDownColor("s2id_productcode", "");
                                    changeSelect2DropDownColor("s2id_adjustmentstarget", "");
                                    $scope.adAdjustment.MediaAssetId = parseInt($stateParams.mediaAssetId);
                                    CreateAdAdjustmentButtonName();
                                    $scope.adAdjustmentsForm.$setPristine();
                                    $scope.showMsgs = false;
                                }
                                GetAdAdjustmentsData($scope.adAdjustment.MediaAssetId);
                                toastr.success('Deleted successfully.', 'Success!');
                            }
                            else {
                                toastr.error(result.message, 'Error');
                            }
                        })
                    }
                }
            });

            //if (confirm("Are you sure you want to delete the selected object(s) and all of their children?")) {
            //    adAdjustmentsFactory.delete(adAdjustment.AdAdjustmentId, function (result) {
            //        if (result.statusCode == 1) {
            //            initSelect2("s2id_productcode", "Select Product Code");
            //            initSelect2("s2id_adjustmentstarget", "Select Adjustments Target");
            //            changeSelect2DropDownColor("s2id_productcode", "");
            //            changeSelect2DropDownColor("s2id_adjustmentstarget", "");
            //            GetAdAdjustmentsData($scope.adAdjustment.MediaAssetId);
            //            toastr.success('Deleted successfully.', 'Success!');
            //        } else {
            //            toastr.error(result.message, 'Error');
            //        }
            //    })
            //}
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onProductCodeChange = function () {
        try {
            var DropDown = angular.element(document.querySelector('#productcode'));
            var getSelectedData = DropDown.select2("data");
            var Id = getSelectedData.id;
            var Value = getSelectedData.text;
            if (Id != undefined && Id != "" && Id != 0 && Id != null) {
                changeSelect2DropDownColor("s2id_productcode", Id);
            } else {
                changeSelect2DropDownColor("s2id_productcode", "");
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onAdjustmentTargetChange = function () {
        try {
            var DropDown = angular.element(document.querySelector('#adjustmentstarget'));
            var getSelectedData = DropDown.select2("data");
            var Id = getSelectedData.id;
            var Value = getSelectedData.text;
            if (Id != undefined && Id != "" && Id != 0 && Id != null) {
                changeSelect2DropDownColor("s2id_adjustmentstarget", Id);
            } else {
                changeSelect2DropDownColor("s2id_adjustmentstarget", "");
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    GetMediaAssetItemData();
    GetAdAdjustmentsData($stateParams.mediaAssetId);
    GetAdAdjustmentsTargetData();

})