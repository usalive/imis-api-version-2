﻿'use strict';

app.controller('MediaAssetsTabController', function MediaAssetsTabController($scope, $location, $stateParams, toastr) {
    console.log('called');
    $scope.MediaAssetTab = true;
    $scope.MediaAssetDisable = false;
    $scope.issueDateTabDisable = true;
    $scope.AdAdjustmenTabDisable = true;
    $scope.rateCardTabDisable = true;
    $scope.rateCardsDetailsTabDisable = true;
    $scope.tabMediaAssetId = $stateParams.mediaAssetId;
    $scope.tabRateCardId = $stateParams.RateCardId

    $scope.setMediaAssetsTabActive = function () {
        $scope.MediaAssetTab = true;
        $scope.issueDateTab = false;
        $scope.AdAdjustmentTab = false;
        $scope.rateCardTab = false;
        $scope.rateCardsDetailsTab = false;

        $scope.MediaAssetDisable = false;
        $scope.issueDateTabDisable = true;
        $scope.AdAdjustmenTabDisable = true;
        $scope.rateCardTabDisable = true;
        $scope.rateCardsDetailsTabDisable = true;
    }
    $scope.setIssueDateTabActive = function () {
       // $scope.tabMediaAssetId = $stateParams.mediaAssetId;
        $scope.MediaAssetTab = false;
        $scope.issueDateTab = true;
        $scope.AdAdjustmentTab = false;
        $scope.rateCardTab = false;
        $scope.rateCardsDetailsTab = false;

        $scope.MediaAssetDisable = false;
        $scope.issueDateTabDisable = false;
        $scope.AdAdjustmenTabDisable = true;
        $scope.rateCardTabDisable = true;
        $scope.rateCardsDetailsTabDisable = true;
    }
    $scope.setAdAdjustmentTabActive = function () {
       // $scope.tabMediaAssetId = $stateParams.mediaAssetId;
        $scope.MediaAssetTab = false;
        $scope.issueDateTab = false;
        $scope.AdAdjustmentTab = true;
        $scope.rateCardTab = false;
        $scope.rateCardsDetailsTab = false;

        $scope.MediaAssetDisable = false;
        $scope.issueDateTabDisable = false;
        $scope.AdAdjustmenTabDisable = false;
        $scope.rateCardTabDisable = true;
        $scope.rateCardsDetailsTabDisable = true;
    }
    $scope.setRateCardTabActive = function () {
      //  $scope.tabMediaAssetId = $stateParams.mediaAssetId;
        $scope.MediaAssetTab = false;
        $scope.issueDateTab = false;
        $scope.AdAdjustmentTab = false;
        $scope.rateCardTab = true;
        $scope.rateCardsDetailsTab = false;

        $scope.MediaAssetDisable = false;
        $scope.issueDateTabDisable = false;
        $scope.AdAdjustmenTabDisable = false;
        $scope.rateCardTabDisable = false;
        $scope.rateCardsDetailsTabDisable = true;
    }
    $scope.setRateCardsDetailsTabActive = function () {

        $scope.MediaAssetTab = false;
        $scope.issueDateTab = false;
        $scope.AdAdjustmentTab = false;
        $scope.rateCardTab = false;
        $scope.rateCardsDetailsTab = true;

        $scope.MediaAssetDisable = false;
        $scope.issueDateTabDisable = false;
        $scope.AdAdjustmenTabDisable = false;
        $scope.rateCardTabDisable = false;
        $scope.rateCardsDetailsTabDisable = false;
    }
})