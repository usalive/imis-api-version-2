﻿'use strict';

app.controller('RateCardsDetailsController', function RateCardsDetailsController($scope, $stateParams, $compile, $sce, $parse, adColorFactory, adSizeFactory, frequencyFactory,
    rateCardFactory, rateCardDetailsFactory, authKeyService, toastr) {

    var authKey = authKeyService.getConstantAuthKey();
    $scope.baseUrl = authKey.baseUrl;

    //Init Rate Cards Details 
    $scope.rateCardDetails = {};
    $scope.rateCardsDetails = {};
    $scope.rateCardshowMsgs = false;
    $scope.isShowDefaultTable = false;
    $scope.selectedAdColorValue = [];
    $scope.selectedAdSizeValue = [];
    $scope.selectedFrequencyValue = [];
    $scope.ColorName = "Select Ad Color";
    $scope.isShowDefaultTable = true;
    var rateCardId = $stateParams.RateCardId;
    $scope.dynamicRowData = {};
    $scope.RateCardName = "";
    $scope.AdTypeName = "";
    $scope.MediaAssetName = "";
    var globalCostValue = {};
    var PselectedAdSizeValue = [];
    var PselectedFrequencyValue = [];
    var mediaAssetId = 0;
    $scope.AddOrEdit = "Add";

    $scope.isMultipleRateCardsDetails = true;

    //Init Ad Color 
    $scope.adColor = {};
    $scope.adColor.AdColorId = 0;
    $scope.adColors = {};
    $scope.adColorModalMsgs = false;

    //Init Ad Size 
    $scope.adSize = {};
    $scope.adSize.AdSizeId = 0;
    $scope.adSizes = {};
    $scope.adSizeModalMsgs = false;

    //Init Ad Frequency 
    $scope.frequency = {};
    $scope.frequency.FrequencyId = 0;
    $scope.frequencies = {};
    $scope.adFrequencyModalMsgs = false;
    var frequencies = [];
    var isRateCardDetailsAlreadyExits = false;

    $scope.isLoading = true;
    $scope.dataLoading = true;

    $scope.groupSetup = {
        multiple: true,

        formatSearching: 'Searching the Ad Size...',
        formatNoMatches: 'No Ad Size found'
    };

    $scope.groupSetup1 = {
        multiple: true,
        formatSearching: 'Searching the Frequency...',
        formatNoMatches: 'No Frequency found'
    };

    $scope.adColorSelect2 = {
        multiple: false,
        formatSearching: 'Searching the ad color...',
        formatNoMatches: 'No ad color found'
    };

    //$scope.$parent.tabMediaAssetId = result.data[0].MediaAssetId;
    $scope.$parent.tabRateCardId = $stateParams.RateCardId;
    $scope.$parent.setRateCardsDetailsTabActive();


    function getAllAdColor() {
        try {
            adColorFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data.length > 0) {
                        $scope.adColors = result.data;
                    }
                }
                else {
                    //alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getAllAdSize() {
        try {
            adSizeFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data.length > 0) {
                        $scope.adSizes = result.data;
                    }
                }
                else {
                    //alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getAllFrequency() {
        try {
            frequencyFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data.length > 0) {
                        $scope.frequencies = result.data;
                    }
                }
                else {
                    //alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getAllFrequencyWithFilter() {
        try {
            frequencyFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data.length > 0) {
                        var frequency = result.data;
                        var selectedFrequency = $scope.selectedFrequencyValue;

                        angular.forEach(selectedFrequency, function (fId, key2) {
                            var Id = fId;
                            angular.forEach(frequency, function (fItem, key2) {
                                if (Id == fItem.FrequencyId) {
                                    frequency.pop(fItem);
                                }
                            });
                        });
                        $scope.frequencies = frequency;
                    }
                }
                else {
                    //alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getAllAdSizeWithFilter() {

    }

    function getRateCardDetailsByRateCardId(rateCardId) {
        try {
            rateCardDetailsFactory.getByRateCardId(rateCardId, function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data.length > 0) {
                        $scope.isShowDefaultTable = true;
                        $scope.dataLoading = false;
                        $scope.rateCardsDetails.RateCardId = result.data[0].RateCardId;
                        $scope.rateCardsDetails.AdTypeId = result.data[0].AdTypeId;
                        mediaAssetId = result.data[0].MediaAssetId;
                        $scope.rateCardsDetails.MediaAssetId = mediaAssetId;

                        $scope.RateCardName = result.data[0].RateCardName;
                        $scope.AdTypeName = result.data[0].AdTypeName;
                        $scope.MediaAssetName = result.data[0].MediaAssetName;

                        // for multiple
                        if (result.data[0].RateCardDetails.length > 0) {
                            var mRateCardsDetails = result.data[0].RateCardDetails;
                            angular.forEach(mRateCardsDetails, function (value, key) {
                                makeDynamicArraytoCreateRateCardsDetailsTable(value);
                            });
                        } else {
                            $scope.isMultipleRateCardsDetails = false;
                        }
                        //end for multiple
                        //  $scope.rateCardDetails.AdColorId = (selectedAdColorId);
                        $scope.$parent.tabMediaAssetId = result.data[0].MediaAssetId;
                        $scope.$parent.tabRateCardId = result.data[0].RateCardId;
                        $scope.$parent.setRateCardsDetailsTabActive();
                    } else {
                        $scope.isShowDefaultTable = true;
                        $scope.dataLoading = false;
                        $scope.rateCardsDetails = {};
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.isShowDefaultTable = true;
                    $scope.dataLoading = false;
                    $scope.rateCardsDetails = {};
                }
                else {
                    $scope.dataLoading = false;
                    //alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function makeDynamicArraytoCreateRateCardsDetailsTable(objMRateCardsDetails) {
        try {
            //$scope.rateCardDetails = objMRateCardsDetails;
            //var colorId = parseInt($scope.rateCardDetails.AdColorId);
            //var colorName = $scope.rateCardDetails.AdColorName;
            //var objRateCardDetailCost = $scope.rateCardDetails.RateCardDetailCost;

            var rateCardDetails = objMRateCardsDetails;
            var colorId = rateCardDetails.AdColorId.toString();
            var colorName = rateCardDetails.AdColorName;
            var objRateCardDetailCost = rateCardDetails.RateCardDetailCost;

            if (objRateCardDetailCost.length > 0) {
                isRateCardDetailsAlreadyExits = true;
                //var objRateCardDetailCost = $scope.rateCardDetails.RateCardDetailCost;
                var objRateCardDetailCost = rateCardDetails.RateCardDetailCost;
                var AdSizeIdArray = [];
                var FrequencyIdArray = [];
                var AdSizeArray = [];
                var FrequencyArray = [];
                var AdColorIdArray = [];
                var RowDataArray = [];
                angular.forEach(objRateCardDetailCost, function (costValue, key2) {
                    var AdSizeId = 0;
                    var FrequencyId = 0;
                    var AdColorId = 0;
                    AdSizeId = costValue.AdSizeId;
                    FrequencyId = costValue.FrequencyId;
                    AdColorId = costValue.AdColorId;

                    if (AdSizeIdArray.indexOf(AdSizeId) == -1) {
                        var tempobj = {};
                        tempobj.AdSizeId = costValue.AdSizeId;
                        tempobj.AdSizeName = costValue.AdSizeName;
                        //tempobj.RateCardCost = costValue.RateCardCost;
                        tempobj.RateCardDetailId = costValue.RateCardDetailId;
                        AdSizeArray.push(tempobj);
                        AdSizeIdArray.push(tempobj.AdSizeId);

                    }
                    if (FrequencyIdArray.indexOf(FrequencyId) == -1) {
                        var tempobj = {};
                        tempobj.FrequencyId = costValue.FrequencyId;
                        tempobj.FrequencyName = costValue.FrequencyName;
                        // tempobj.RateCardCost = costValue.RateCardCost;
                        tempobj.RateCardDetailId = costValue.RateCardDetailId;
                        FrequencyArray.push(tempobj);
                        FrequencyIdArray.push(tempobj.FrequencyId);

                    }
                    if (AdColorIdArray.indexOf(AdColorId) == -1) {
                        AdColorIdArray.push(AdColorId);
                    }
                    var dynamicModelName = "sizeFrequency_" + costValue.AdSizeId + "_" + costValue.FrequencyId + "_" + colorId + "";
                    RowDataArray[dynamicModelName] = costValue.RateCardCost;
                });
                var FIdArray = [];
                angular.forEach(FrequencyIdArray, function (fvalue, key2) {
                    var id = String(fvalue);
                    FIdArray.push(id);
                });
                var SIdArray = [];
                angular.forEach(AdSizeIdArray, function (svalue, key2) {
                    var id = String(svalue);
                    SIdArray.push(id);
                });

                PselectedAdSizeValue = SIdArray;
                PselectedFrequencyValue = FIdArray;

                createRateCardDetailsTableWithDataForMultiple(colorId, colorName, AdSizeIdArray, FrequencyIdArray, AdSizeArray, FrequencyArray, objRateCardDetailCost, RowDataArray, $scope);
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function GetRateCardsDetailsByRateCardAndColor(rateCardId, AdColorId) {
        try {
            var colorId = AdColorId;
            rateCardDetailsFactory.GetByRateCardAndColor(rateCardId, AdColorId, function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data.length > 0) {

                        $scope.isShowDefaultTable = false;
                        $scope.rateCardDetails = result.data[0];
                        // $scope.rateCardDetails.AdColorId = parseInt(selectedAdColorId);

                        var objRateCardDetailCost = result.data[0].RateCardDetailCost;
                        if (objRateCardDetailCost.length > 0) {
                            colorId = result.data[0].RateCardDetailCost[0].AdColorId;
                            var objRateCardDetailCost = result.data[0].RateCardDetailCost;
                            var AdSizeIdArray = [];
                            var FrequencyIdArray = [];
                            var AdSizeArray = [];
                            var FrequencyArray = [];
                            var AdColorIdArray = [];
                            var RowDataArray = [];
                            angular.forEach(objRateCardDetailCost, function (costValue, key2) {
                                var AdSizeId = 0;
                                var FrequencyId = 0;
                                var AdColorId = 0;
                                AdSizeId = costValue.AdSizeId;
                                FrequencyId = costValue.FrequencyId;
                                AdColorId = costValue.AdColorId;

                                if (AdSizeIdArray.indexOf(AdSizeId) == -1) {
                                    var tempobj = {};
                                    tempobj.AdSizeId = costValue.AdSizeId;
                                    tempobj.AdSizeName = costValue.AdSizeName;
                                    //tempobj.RateCardCost = costValue.RateCardCost;
                                    tempobj.RateCardDetailId = costValue.RateCardDetailId;
                                    AdSizeArray.push(tempobj);
                                    AdSizeIdArray.push(tempobj.AdSizeId);

                                }
                                if (FrequencyIdArray.indexOf(FrequencyId) == -1) {
                                    var tempobj = {};
                                    tempobj.FrequencyId = costValue.FrequencyId;
                                    tempobj.FrequencyName = costValue.FrequencyName;
                                    // tempobj.RateCardCost = costValue.RateCardCost;
                                    tempobj.RateCardDetailId = costValue.RateCardDetailId;
                                    FrequencyArray.push(tempobj);
                                    FrequencyIdArray.push(tempobj.FrequencyId);

                                }
                                if (AdColorIdArray.indexOf(AdColorId) == -1) {
                                    AdColorIdArray.push(AdColorId);
                                }
                                var dynamicModelName = "sizeFrequency_" + costValue.AdSizeId + "_" + costValue.FrequencyId + "_" + colorId + "";
                                RowDataArray[dynamicModelName] = costValue.RateCardCost;
                            });
                            var cId = AdColorIdArray[0].toString();
                            $scope.rateCardDetails.AdColorId = cId;

                            //$scope.selectedAdSizeValue = AdSizeIdArray;
                            //$scope.selectedFrequencyValue = FrequencyIdArray;

                            var FIdArray = [];
                            angular.forEach(FrequencyIdArray, function (fvalue, key2) {
                                var id = String(fvalue);
                                FIdArray.push(id);
                            });
                            var SIdArray = [];
                            angular.forEach(AdSizeIdArray, function (svalue, key2) {
                                var id = String(svalue);
                                SIdArray.push(id);
                            });

                            $scope.selectedFrequencyValue = FIdArray;
                            $scope.selectedAdSizeValue = SIdArray;

                            //var adSizeDropDown = angular.element(document.querySelector('#adjtarget1'));
                            //adSizeDropDown.select2('val', $scope.selectedAdSizeValue);
                            //var adFrequencyDropDown = angular.element(document.querySelector('#adjtarget2'));
                            //adFrequencyDropDown.select2('val', $scope.selectedFrequencyValue);

                            var adSizeDropDown = angular.element(document.querySelector('#adjtarget1'));
                            adSizeDropDown.select2({
                                //  dropdownCssClass: 'no-search'
                            });
                            var objSizeArray = [];
                            angular.forEach(objRateCardDetailCost, function (objRateCardDetail, key) {
                                var obj = {};
                                obj.id = objRateCardDetail.AdSizeId;
                                obj.text = objRateCardDetail.AdSizeName;
                                objSizeArray.push(obj);
                            });

                            adSizeDropDown.data().select2.updateSelection(objSizeArray);
                            //adSizeDropDown.trigger("change.select2");

                            var adFrequencyDropDown = angular.element(document.querySelector('#adjtarget2'));
                            adFrequencyDropDown.select2({
                                // dropdownCssClass: 'no-search'
                            });
                            var objFrequencyArray = [];
                            angular.forEach(objRateCardDetailCost, function (objRateCardDetail, key) {
                                var obj = {};
                                obj.id = objRateCardDetail.FrequencyId;
                                obj.text = objRateCardDetail.FrequencyName;
                                objFrequencyArray.push(obj);
                            });

                            adFrequencyDropDown.data().select2.updateSelection(objFrequencyArray);
                            //adFrequencyDropDown.trigger("change.select2");

                            createRateCardDetailsTableWithData(AdSizeIdArray, FrequencyIdArray, AdSizeArray, FrequencyArray, objRateCardDetailCost, RowDataArray, $scope);
                        }
                        else {
                            var theadElementTR = angular.element(document.querySelector('#rateCardsDetailsTableTH'));
                            if (theadElementTR.children().length != -1) {
                                theadElementTR.empty();
                            }
                            var TbodyElementTr = angular.element(document.querySelector('#rateCardsDetailsTableTbody'));
                            if (TbodyElementTr.children().length != -1) {
                                TbodyElementTr.empty();
                            }
                            $scope.isShowDefaultTable = true;
                        }
                    }
                    else {
                        $scope.isShowDefaultTable = true;
                        $scope.rateCardsDetails = {};
                    }
                }
                else if (result.statusCode == 3) {
                    $scope.isShowDefaultTable = true;
                    $scope.rateCardsDetails = {};
                }
                else {
                    //alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function createRateCardDetailsTableWithDataForMultiple(colorId, colorName, AdSizeIdArray, FrequencyIdArray, AdSizeArray, FrequencyArray, objRateCardDetailCost, RowDataArray, $scope) {
        try {
            var getAllSelectedFrequency = FrequencyIdArray;
            var getAllSelectedSize = AdSizeIdArray;
            var multipleRateCardsDiv = angular.element(document.querySelector('#rateCardsDetailsTableforMultiple'));
            //var tempHTML = '<div class="rateCardTitle">{{colorName' + colorId + '}}</div> <div class="table-responsive custom-table rateCard-table">' +
            //               '<table class="table" id="rateCardsDetailsTable"> <thead> <tr id="rateCardsDetailsTableTH' + colorId + '"></tr> </thead>' +
            //               '<tbody id="rateCardsDetailsTableTbody' + colorId + '"></tbody> ' +
            //               //'<tbody id="rateCardsDetailsTableTbody' + colorId + '" ng-if="isShowDefaultTable == true">' +
            //               //'<tr> <td colspan="8" class="setTextCenter">No Record is Found</td> </tr></tbody>' +
            //               '</table>  </div>';

            var tempHTML = '<div class="rateCardTitle">{{colorName' + colorId + '}}</div> <div class="table-responsive custom-table rateCard-table">' +
                    '<table class="table" id="rateCardsDetailsTable"> <thead> <tr id="rateCardsDetailsTableTH' + colorId + '"></tr> </thead>' +
                    '<tbody id="rateCardsDetailsTableTbody' + colorId + '"></tbody> ' +
                    //'<tbody id="rateCardsDetailsTableTbody' + colorId + '" ng-if="isShowDefaultTable == true">' +
                    //'<tr> <td colspan="8" class="setTextCenter">No Record is Found</td> </tr></tbody>' +
                    '<tr ng-if="isLoading == true"> <td>&nbsp;</td> <td>&nbsp;</td> </tr>' +
                    '</table> <div ng-if="isLoading == dataLoading"> <div class="dataLoad"> <div data-loading></div> </div> </div> </div>';

            var staticHTML = $compile(tempHTML)($scope);
            multipleRateCardsDiv.append(staticHTML);

            var text = colorName;
            var the_string = "colorName" + colorId;
            var model = $parse(the_string);
            model.assign($scope, text);

            if (getAllSelectedFrequency.length > 0) {
                var theadElementTR = angular.element(document.querySelector('#rateCardsDetailsTableTH' + colorId));
                theadElementTR.empty();
                var tableHeader = new Array();
                var thElement = "<th>SPACE</th>";
                angular.forEach(getAllSelectedFrequency, function (objFrequency, key) {
                    thElement = thElement + '<th style="text-align: center;">' + FrequencyArray[key].FrequencyName + '</th>';
                });
                var Thtemp = $compile(thElement)($scope);
                theadElementTR.append(Thtemp);
            }

            if (getAllSelectedFrequency.length > 0) {
                //var colorId = objRateCardDetailCost[0].AdColorId;
                var TbodyElementTr = angular.element(document.querySelector('#rateCardsDetailsTableTbody' + colorId));
                TbodyElementTr.empty();
                if (getAllSelectedSize.length > 0) {
                    // $scope.dynamicRowData = new Array();
                    angular.forEach(getAllSelectedSize, function (objSize, key1) {
                        var tdElement = '<td style="vertical-align: middle">' + AdSizeArray[key1].AdSizeName + '</td>';
                        var Sizeid = objSize;
                        globalCostValue = RowDataArray;
                        angular.forEach(getAllSelectedFrequency, function (objFrequency, key2) {
                            //var text = objFrequency.RateCardCost;
                            var dynamicModelName = "sizeFrequency_" + Sizeid + "_" + objFrequency + "_" + colorId + "";
                            var text = globalCostValue[dynamicModelName];
                            var the_string = "dynamicRowData.sizeFrequency_" + Sizeid + "_" + objFrequency + "_" + colorId + "";
                            var model = $parse(the_string);
                            model.assign($scope, text);

                            tdElement = tdElement + '<td style="text-align: center;"> {{dynamicRowData.sizeFrequency_' + Sizeid + '_' + objFrequency + '_' + colorId + '}}' +
                            '</td>';

                            //tdElement = tdElement + '<td style="text-align: center;"><input type="text" allow-decimal-numbers ' +
                            //'ng-model="dynamicRowData.sizeFrequency_' + Sizeid + '_' + objFrequency + '_' + colorId + '" name="sizeFrequency_' + Sizeid + '_' + objFrequency + '_' + colorId + '"' +
                            //'id="sizeFrequency_' + Sizeid + '_' + objFrequency + '_' + colorId + '" ng-value="' + text + '" placeholder="Rate" /></td>';
                        });
                        tdElement = '<tr>' + tdElement + ' </tr>';
                        var temp = $compile(tdElement)($scope);
                        TbodyElementTr.append(temp);
                        $scope.isShowDefaultTable = false;
                        $scope.dataLoading = false;
                    });
                }
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function createRateCardDetailsTableWithData(AdSizeIdArray, FrequencyIdArray, AdSizeArray, FrequencyArray, objRateCardDetailCost, RowDataArray, $scope) {
        try {
            var getAllSelectedFrequency = FrequencyIdArray;
            var getAllSelectedSize = AdSizeIdArray;

            if (getAllSelectedFrequency.length > 0) {
                var theadElementTR = angular.element(document.querySelector('#rateCardsDetailsTableTH'));
                theadElementTR.empty();
                var tableHeader = new Array();
                var thElement = "<th>SPACE</th>";
                angular.forEach(getAllSelectedFrequency, function (objFrequency, key) {
                    thElement = thElement + '<th style="text-align: center;">' + FrequencyArray[key].FrequencyName + '</th>';
                });
                var Thtemp = $compile(thElement)($scope);
                theadElementTR.append(Thtemp);
            }

            if (getAllSelectedFrequency.length > 0) {
                var colorId = objRateCardDetailCost[0].AdColorId;
                var TbodyElementTr = angular.element(document.querySelector('#rateCardsDetailsTableTbody'));
                TbodyElementTr.empty();
                if (getAllSelectedSize.length > 0) {
                    $scope.dynamicRowData = new Array();
                    angular.forEach(getAllSelectedSize, function (objSize, key1) {
                        var tdElement = '<td style="vertical-align: middle">' + AdSizeArray[key1].AdSizeName + '</td>';
                        var Sizeid = objSize;
                        globalCostValue = RowDataArray;
                        angular.forEach(getAllSelectedFrequency, function (objFrequency, key2) {
                            //var text = objFrequency.RateCardCost;
                            var dynamicModelName = "sizeFrequency_" + Sizeid + "_" + objFrequency + "_" + colorId + "";
                            var text = globalCostValue[dynamicModelName];
                            var the_string = "dynamicRowData.sizeFrequency_" + Sizeid + "_" + objFrequency + "_" + colorId + "";
                            var model = $parse(the_string);
                            model.assign($scope, text);

                            tdElement = tdElement + '<td style="text-align: center;"><input type="text" allow-decimal-numbers ' +
                            'ng-model="dynamicRowData.sizeFrequency_' + Sizeid + '_' + objFrequency + '_' + colorId + '" name="sizeFrequency_' + Sizeid + '_' + objFrequency + '_' + colorId + '"' +
                            'id="sizeFrequency_' + Sizeid + '_' + objFrequency + '_' + colorId + '" ng-value="' + text + '" placeholder="Rate" /></td>';
                        });
                        tdElement = '<tr>' + tdElement + ' </tr>';
                        var temp = $compile(tdElement)($scope);
                        TbodyElementTr.append(temp);
                        $scope.isShowDefaultTable = false;
                        $scope.MatrixCreated = true;
                        $scope.dataLoading = false;
                    });
                }
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    //Init Rate Cards Details code 
    var selectedAdColorId = [];
    $scope.MatrixCreated = false;

    $scope.changeAdColor = function (rateCardsDetails) {
        try {
            $scope.isMultipleRateCardsDetails = false;
            $scope.AddOrEdit = "Add";
            var adColorDropDown = angular.element(document.querySelector('#ddlAdColor'));
            var selectedText = adColorDropDown.find(":selected").text();
            $scope.ColorName = selectedText;
            var selectedValue = adColorDropDown.find(":selected").val();
            var selectedAdColorName = selectedText;
            selectedAdColorId = selectedValue.replace("number:", "");
            $scope.rateCardsDetails.AdColorId = selectedAdColorId;
            var rateCardId = $stateParams.RateCardId;

        if (selectedAdColorId != undefined && selectedAdColorId != "" && selectedAdColorId != 0 && selectedAdColorId != null) {
            changeSelect2DropDownColor("s2id_ddlAdColor", selectedAdColorId);
        } else {
            changeSelect2DropDownColor("s2id_ddlAdColor", "");
        }

        $scope.selectedFrequencyValue = [];
        $scope.selectedAdSizeValue = [];

        var adSizeDropDown = angular.element(document.querySelector('#adjtarget1'));
        adSizeDropDown.select2('val', $scope.selectedAdSizeValue);

        var adFrequencyDropDown = angular.element(document.querySelector('#adjtarget2'));
        adFrequencyDropDown.select2('val', $scope.selectedFrequencyValue);


        isRateCardDetailsAlreadyExits = false;
        $scope.MatrixCreated = false;
        $scope.rateCardDetailsForm.$setPristine();
        $scope.rateCardshowMsgs = false;

            if (rateCardId != null && rateCardId != undefined && rateCardId != "" && selectedAdColorId != null && selectedAdColorId != undefined && selectedAdColorId != "") {
                rateCardDetailsFactory.GetByRateCardAndColor(rateCardId, selectedAdColorId, function (result) {
                    if (result.statusCode == 1) {
                        console.log(result);
                        if (result.data.length > 0) {
                            $scope.isShowDefaultTable = false;
                            $scope.dataLoading = false;
                            $scope.rateCardDetails = result.data[0];
                            $scope.rateCardDetails.AdColorId = selectedAdColorId.toString();
                            var colorId = parseInt(selectedAdColorId);

                        var objRateCardDetailCost = result.data[0].RateCardDetailCost;
                        if (objRateCardDetailCost.length > 0) {
                            isRateCardDetailsAlreadyExits = true;
                            var objRateCardDetailCost = result.data[0].RateCardDetailCost;
                            var AdSizeIdArray = [];
                            var FrequencyIdArray = [];
                            var AdSizeArray = [];
                            var FrequencyArray = [];
                            var AdColorIdArray = [];
                            var RowDataArray = [];
                            angular.forEach(objRateCardDetailCost, function (costValue, key2) {
                                var AdSizeId = 0;
                                var FrequencyId = 0;
                                var AdColorId = 0;
                                AdSizeId = costValue.AdSizeId;
                                FrequencyId = costValue.FrequencyId;
                                AdColorId = costValue.AdColorId;

                                if (AdSizeIdArray.indexOf(AdSizeId) == -1) {
                                    var tempobj = {};
                                    tempobj.AdSizeId = costValue.AdSizeId;
                                    tempobj.AdSizeName = costValue.AdSizeName;
                                    //tempobj.RateCardCost = costValue.RateCardCost;
                                    tempobj.RateCardDetailId = costValue.RateCardDetailId;
                                    AdSizeArray.push(tempobj);
                                    AdSizeIdArray.push(tempobj.AdSizeId);

                                }
                                if (FrequencyIdArray.indexOf(FrequencyId) == -1) {
                                    var tempobj = {};
                                    tempobj.FrequencyId = costValue.FrequencyId;
                                    tempobj.FrequencyName = costValue.FrequencyName;
                                    // tempobj.RateCardCost = costValue.RateCardCost;
                                    tempobj.RateCardDetailId = costValue.RateCardDetailId;
                                    FrequencyArray.push(tempobj);
                                    FrequencyIdArray.push(tempobj.FrequencyId);

                                }
                                if (AdColorIdArray.indexOf(AdColorId) == -1) {
                                    AdColorIdArray.push(AdColorId);
                                }
                                var dynamicModelName = "sizeFrequency_" + costValue.AdSizeId + "_" + costValue.FrequencyId + "_" + colorId + "";
                                RowDataArray[dynamicModelName] = costValue.RateCardCost;
                            });
                            var cId = AdColorIdArray[0].toString();
                            $scope.rateCardDetails.AdColorId = cId;

                            //$scope.selectedAdSizeValue = AdSizeIdArray;
                            //$scope.selectedFrequencyValue = FrequencyIdArray;

                            var FIdArray = [];
                            angular.forEach(FrequencyIdArray, function (fvalue, key2) {
                                var id = String(fvalue);
                                FIdArray.push(id);
                            });
                            var SIdArray = [];
                            angular.forEach(AdSizeIdArray, function (svalue, key2) {
                                var id = String(svalue);
                                SIdArray.push(id);
                            });

                            $scope.selectedFrequencyValue = FIdArray;
                            $scope.selectedAdSizeValue = SIdArray;

                            PselectedAdSizeValue = SIdArray;
                            PselectedFrequencyValue = FIdArray;
                            //var adSizeDropDown = angular.element(document.querySelector('#adjtarget1'));
                            //adSizeDropDown.select2('data', $scope.selectedAdSizeValue);
                            //var adFrequencyDropDown = angular.element(document.querySelector('#adjtarget2'));
                            //adFrequencyDropDown.select2('data', $scope.selectedFrequencyValue);

                            var adSizeDropDown = angular.element(document.querySelector('#adjtarget1'));
                            adSizeDropDown.select2({
                                //dropdownCssClass: 'no-search'
                            });
                            var objSizeArray = [];
                            angular.forEach(objRateCardDetailCost, function (objRateCardDetail, key) {
                                var obj = {};
                                obj.id = objRateCardDetail.AdSizeId;
                                obj.text = objRateCardDetail.AdSizeName;
                                objSizeArray.push(obj);
                            });

                            adSizeDropDown.data().select2.updateSelection(objSizeArray);
                            //adSizeDropDown.trigger("change.select2");

                            var adFrequencyDropDown = angular.element(document.querySelector('#adjtarget2'));
                            adFrequencyDropDown.select2({
                                //dropdownCssClass: 'no-search'
                            });
                            var objFrequencyArray = [];
                            angular.forEach(objRateCardDetailCost, function (objRateCardDetail, key) {
                                var obj = {};
                                obj.id = objRateCardDetail.FrequencyId;
                                obj.text = objRateCardDetail.FrequencyName;
                                objFrequencyArray.push(obj);
                            });

                                adFrequencyDropDown.data().select2.updateSelection(objFrequencyArray);
                                //adFrequencyDropDown.trigger("change.select2");
                                $scope.AddOrEdit = "Edit";
                                createRateCardDetailsTableWithData(AdSizeIdArray, FrequencyIdArray, AdSizeArray, FrequencyArray, objRateCardDetailCost, RowDataArray, $scope);
                            }
                            else {
                                var theadElementTR = angular.element(document.querySelector('#rateCardsDetailsTableTH'));
                                if (theadElementTR.children().length != -1) {
                                    theadElementTR.empty();
                                }
                                var TbodyElementTr = angular.element(document.querySelector('#rateCardsDetailsTableTbody'));
                                if (TbodyElementTr.children().length != -1) {
                                    TbodyElementTr.empty();
                                }
                                $scope.isShowDefaultTable = true;
                                $scope.dataLoading = false;
                            }
                        }
                        else {
                            $scope.isShowDefaultTable = true;
                            $scope.dataLoading = false;
                            $scope.rateCardsDetails = {};
                        }
                    }
                    else if (result.statusCode == 3) {
                        $scope.isShowDefaultTable = true;
                        $scope.dataLoading = false;
                        $scope.rateCardsDetails = {};
                    }
                    else {
                        $scope.dataLoading = false;
                        //   alert(result.message);
                    }
                })
            }
            else {
                var theadElementTR = angular.element(document.querySelector('#rateCardsDetailsTableTH'));
                theadElementTR.empty();
                var TbodyElementTr = angular.element(document.querySelector('#rateCardsDetailsTableTbody'));
                TbodyElementTr.empty();
                $scope.isShowDefaultTable = true;
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    var sizeChangeEvent = angular.element(document.querySelector('#adjtarget1'));
    sizeChangeEvent.on('change', function () {
        try {
            $scope.$emit('select2:change', sizeChangeEvent);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    });
    $scope.$on('select2:change', function (event, element) {
        try {
            //  console.log('change fired by' + element.get(0).id);
            $scope.MatrixCreated = false;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    });

    var frequencyChangeEvent = angular.element(document.querySelector('#adjtarget2'));
    frequencyChangeEvent.on('change', function () {
        try {
            $scope.$emit('select2:change', frequencyChangeEvent);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    });
    $scope.$on('select2:change', function (event, element) {
        try {
            //  console.log('change fired by' + element.get(0).id);
            $scope.MatrixCreated = false;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    });

    // function getRateCardsDetails
    $scope.SaveRateCardDetails = function (rateCardDetails, selectedAdSizeValue, selectedFrequencyValue, dynamicRowData) {
        try {
            // rateCardDetails.RateCardDetailCost = new Array();
            if (selectedAdSizeValue != undefined && selectedAdSizeValue != "" && selectedAdSizeValue != null) {
                if (selectedAdSizeValue.length > 0) {
                    var adSizeDropDown = angular.element(document.querySelector('#adjtarget1'));
                    var adSizeIds = adSizeDropDown.select2("data");
                    $scope.selectedAdSizeValue = [];
                    angular.forEach(adSizeIds, function (adSize, key) {
                        $scope.selectedAdSizeValue.push(adSize.id);
                    });
                    selectedAdSizeValue = $scope.selectedAdSizeValue;
                }
            }

        if (selectedFrequencyValue != undefined && selectedFrequencyValue != "" && selectedFrequencyValue != null) {
            if (selectedFrequencyValue.length > 0) {
                var adFrequencyDropDown = angular.element(document.querySelector('#adjtarget2'));
                var adFrequencyIds = adFrequencyDropDown.select2("data");
                $scope.selectedFrequencyValue = [];
                angular.forEach(adFrequencyIds, function (adFrequency, key) {
                    $scope.selectedFrequencyValue.push(adFrequency.id);
                });
                selectedFrequencyValue = $scope.selectedFrequencyValue;
            }
        }

        var objRateCardDetailCost = {};
        var colorId = rateCardDetails.AdColorId;
        var ArrayRateCardDetailCost = new Array();
        if ($scope.rateCardDetailsForm.$valid) {
            if (selectedAdSizeValue.length > 0) {
                var objrateCardDetails = rateCardDetails.RateCardDetailCost;
                angular.forEach(selectedAdSizeValue, function (objSizeValue, key1) {
                    angular.forEach(selectedFrequencyValue, function (objFrequencyValue, key2) {
                        var rateCardDetailsId = 0;
                        for (var i = 0, len = objrateCardDetails.length - 1; i <= len; i++) {
                            var tempRateCardDetails = objrateCardDetails[i];
                            if (tempRateCardDetails.AdSizeId == objSizeValue && tempRateCardDetails.FrequencyId == objFrequencyValue) {
                                rateCardDetailsId = tempRateCardDetails.RateCardDetailId;
                                break
                            }
                        };

                        var tempRateCardDetailCost = {};
                        if (rateCardDetailsId == 0) {
                            tempRateCardDetailCost.RateCardDetailId = 0;
                        } else {
                            tempRateCardDetailCost.RateCardDetailId = rateCardDetailsId;
                        }
                        tempRateCardDetailCost.AdSizeId = objSizeValue;
                        tempRateCardDetailCost.FrequencyId = objFrequencyValue;
                        var dynamicModelName = "sizeFrequency_" + objSizeValue + "_" + objFrequencyValue + "_" + colorId + "";
                        var indexof = Object.keys(dynamicRowData).indexOf(dynamicModelName);
                        if (indexof != -1) {
                            var value = dynamicRowData[dynamicModelName];
                            if (value != "" && value != undefined && value != null) {
                                tempRateCardDetailCost.RateCardCost = dynamicRowData[dynamicModelName];
                            }
                            else {
                                tempRateCardDetailCost.RateCardCost = 0;
                                //var model = $parse(dynamicModelName);
                                //model.assign($scope, 0);
                            }
                        }
                        ArrayRateCardDetailCost.push(tempRateCardDetailCost);
                    });


                });
                rateCardDetails.RateCardDetailCost = ArrayRateCardDetailCost;
            }
        }
        else {
            $scope.rateCardshowMsgs = true;
        }

        if ($scope.rateCardDetailsForm.$valid) {
            if (rateCardDetails.RateCardDetailCost.length > 0) {
                if ($scope.MatrixCreated == true) {
                    console.log(rateCardDetails);
                    rateCardDetailsFactory.save(rateCardDetails, function (result) {
                        if (result.statusCode == 1) {
                            //$scope.rateCardDetails = {};
                            isRateCardDetailsAlreadyExits = true;
                            $scope.rateCardDetails.AdColorId = selectedAdColorId.toString();
                            GetRateCardsDetailsByRateCardAndColor($stateParams.RateCardId, selectedAdColorId);
                            $scope.AddOrEdit = "Edit";
                            //getRateCardDetailsByRateCardId($stateParams.RateCardId);
                            //getAllFrequencyWithFilter();
                            //getAllAdSizeWithFilter();
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else {
                            toastr.error(result.message, 'Error');
                        }
                    })
                }
                else {
                    toastr.info('Please first create matrix', 'Information');
                }
            }

            }
            else {
                $scope.rateCardshowMsgs = true;
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.createRateCardDetailsTable = function (rateCardDetails, selectedAdSizeValue, selectedFrequencyValue) {
        try {
            var adSizeDropDown = angular.element(document.querySelector('#adjtarget1'));
            var getAllSelectedSize = adSizeDropDown.select2("data");
            var adFrequencyDropDown = angular.element(document.querySelector('#adjtarget2'));
            var getAllSelectedFrequency = adFrequencyDropDown.select2("data");
            var colorId = rateCardDetails.AdColorId;
            var color = $scope.ColorName;
            var success = false;
            if (isRateCardDetailsAlreadyExits == true) {
                var success = confirm("Matrix already exist for " + color + ".  Are you sure you want to generate new matrix?");
                //success = true;
            }
            if (isRateCardDetailsAlreadyExits == false) {
                success = true;
            }
            // var success = confirm("Matrix already exist for " + color + ".  Are you sure you want to generate new matrix?");
            if (success) {
                if ($scope.rateCardDetailsForm.$valid) {
                    if (getAllSelectedFrequency.length > 0) {
                        var theadElementTR = angular.element(document.querySelector('#rateCardsDetailsTableTH'));
                        if (theadElementTR.children().length != -1) {
                            theadElementTR.empty();
                            var tableHeader = new Array();
                            var thElement = "<th>SPACE</th>";
                            angular.forEach(getAllSelectedFrequency, function (objFrequency, key) {
                                thElement = thElement + '<th style="text-align: center;">' + objFrequency.text + '</th>';

                        });
                        var temp = $compile(thElement)($scope);
                        theadElementTR.append(temp);
                    }
                }

                if (getAllSelectedSize.length > 0) {
                    var TbodyElementTr = angular.element(document.querySelector('#rateCardsDetailsTableTbody'));
                    if (TbodyElementTr.children().length != -1) {
                        TbodyElementTr.empty();
                        if (getAllSelectedSize.length > 0) {
                            $scope.dynamicRowData = new Array();
                            //PselectedAdSizeValue = [];
                            //PselectedFrequencyValue = [];
                            angular.forEach(getAllSelectedSize, function (objSize, key1) {
                                var tdElement = '<td style="vertical-align: middle">' + objSize.text + '</td>';
                                var sizeId = objSize.id;
                                angular.forEach(getAllSelectedFrequency, function (objFrequency, key2) {
                                    var FrequencyId = objFrequency.id;

                                    var dynamicModelName = "sizeFrequency_" + sizeId + "_" + FrequencyId + "_" + colorId + "";
                                    var text = globalCostValue[dynamicModelName];
                                    var the_string = "dynamicRowData.sizeFrequency_" + sizeId + "_" + FrequencyId + "_" + colorId + "";
                                    var model = $parse(the_string);
                                    model.assign($scope, text);

                                    if (text == undefined) {
                                        if (PselectedAdSizeValue.indexOf(sizeId) == -1) {
                                            PselectedAdSizeValue.push(sizeId);
                                        }

                                        if (PselectedFrequencyValue.indexOf(FrequencyId) == -1) {
                                            PselectedFrequencyValue.push(FrequencyId);
                                        }
                                    }

                                    var text = "";
                                    tdElement = tdElement + '<td style="text-align: center;"><input type="text" allow-decimal-numbers ' +
                                    'ng-model="dynamicRowData.sizeFrequency_' + sizeId + '_' + FrequencyId + '_' + colorId + '" name="sizeFrequency_' + sizeId + '_' + FrequencyId + '_' + colorId + '"' +
                                    'id="sizeFrequency_' + sizeId + '_' + FrequencyId + '_' + colorId + '" value="" placeholder="Rate" /></td>';
                                });
                                tdElement = '<tr>' + tdElement + ' </tr>';
                                var temp = $compile(tdElement)($scope);
                                TbodyElementTr.append(temp);
                                $scope.isShowDefaultTable = false;
                                $scope.MatrixCreated = true;
                            });
                        }
                    }
                }
                $scope.rateCardDetailsForm.$setPristine();
                $scope.rateCardshowMsgs = false;
            }
            else {
                $scope.rateCardshowMsgs = true;
            }
        }
        else {
            $scope.isShowDefaultTable = false;

            //var ss = PselectedAdSizeValue;
            //var ff = PselectedFrequencyValue;
            //var PselectedFrequencyValueArray = [];
            //var PselectedAdSizeValueArray = [];

            //angular.forEach(getAllSelectedFrequency, function (objFrequency, key1) {
            //    angular.forEach(ff, function (pObjFrequency, key2) {
            //        var pFId = pObjFrequency;
            //        var fId = objFrequency.id;
            //        if (pFId == fId) {
            //            var objFrequencyValue = getAllSelectedFrequency[key1];
            //            PselectedFrequencyValueArray.push(objFrequencyValue);
            //        }
            //    });
            //});

            //angular.forEach(getAllSelectedSize, function (objSize, key1) {
            //    angular.forEach(ss, function (pObjSize, key2) {
            //        var pSId = pObjSize;
            //        var SId = objSize.id;
            //        if (pSId == SId) {
            //            var objSizeValue = getAllSelectedSize[key1];
            //            PselectedAdSizeValueArray.push(objSizeValue);
            //        }
            //    });
            //});

            //if ($scope.rateCardDetailsForm.$valid) {
            //    if (PselectedFrequencyValueArray.length > 0) {
            //        var theadElementTR = angular.element(document.querySelector('#rateCardsDetailsTableTH'));
            //        if (theadElementTR.children().length != -1) {
            //            theadElementTR.empty();
            //            var tableHeader = new Array();
            //            var thElement = "<th>SPACE</th>";
            //            angular.forEach(PselectedFrequencyValueArray, function (objFrequency, key) {
            //                thElement = thElement + '<th style="text-align: center;">' + objFrequency.text + '</th>';
            //            });
            //            var temp = $compile(thElement)($scope);
            //           // theadElementTR.append(temp);
            //        }
            //    }

            //    if (PselectedAdSizeValueArray.length > 0) {
            //        var TbodyElementTr = angular.element(document.querySelector('#rateCardsDetailsTableTbody'));
            //        if (TbodyElementTr.children().length != -1) {
            //            TbodyElementTr.empty();
            //            if (PselectedAdSizeValueArray.length > 0) {
            //                $scope.dynamicRowData = new Array();
            //                angular.forEach(PselectedAdSizeValueArray, function (objSize, key1) {
            //                    var tdElement = '<td style="vertical-align: middle">' + objSize.text + '</td>';
            //                    // var sizeId = selectedAdSizeValue[key1];
            //                    var sizeId = objSize.id;
            //                    angular.forEach(PselectedFrequencyValueArray, function (objFrequency, key2) {
            //                        //var FrequencyId = selectedFrequencyValue[key2];
            //                        var FrequencyId = objFrequency.id;

            //                        var dynamicModelName = "sizeFrequency_" + sizeId + "_" + FrequencyId + "";
            //                        var text = globalCostValue[dynamicModelName];
            //                        var the_string = "dynamicRowData.sizeFrequency_" + sizeId + "_" + FrequencyId + "";
            //                        var model = $parse(the_string);
            //                        model.assign($scope, text);

            //                        var text = "";
            //                        tdElement = tdElement + '<td style="text-align: center;"><input type="text" allow-decimal-numbers ' +
            //                        'ng-model="dynamicRowData.sizeFrequency_' + sizeId + '_' + FrequencyId + '" name="sizeFrequency_' + sizeId + '_' + FrequencyId + '"' +
            //                        'id="sizeFrequency_' + sizeId + '_' + FrequencyId + '" value="" placeholder="Rate" /></td>';
            //                    });
            //                    tdElement = '<tr>' + tdElement + ' </tr>';
            //                    var temp = $compile(tdElement)($scope);
            //                   // TbodyElementTr.append(temp);
            //                    $scope.isShowDefaultTable = false;
            //                });
            //            }
            //        }
            //    }
            //    $scope.rateCardDetailsForm.$setPristine();
            //    $scope.rateCardshowMsgs = false;
            //}
            //else {
            //    $scope.rateCardshowMsgs = true;
            //}

            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    //Init Frequency Modal Pop up and code 
    $scope.InitFrequencyPopup = function () {
        try {
            $scope.frequency = {};
            $scope.frequency.FrequencyId = 0;
            $scope.FrequencyModalForm.$setPristine();
            $scope.adFrequencyModalMsgs = false;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function CreateFrequencyButtonName() {
        try {
            var frequencyButton = angular.element(document.querySelector('#btnFrequency'));
            frequencyButton.text('Add Frequency');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.SaveFrequency = function (frequency) {
        try {
            if ($scope.FrequencyModalForm.$valid) {
                console.log(frequency);
                if (frequency.FrequencyId == 0) {
                    frequencyFactory.save(frequency, function (result) {
                        if (result.statusCode == 1) {
                            $scope.frequency = {};
                            $scope.frequency.FrequencyId = 0;
                            getAllFrequency();
                            CreateFrequencyButtonName();
                            var Modal = angular.element(document.querySelector('#FrequencyModal'))
                            Modal.modal('hide');
                            $scope.FrequencyModalForm.$setPristine();
                            $scope.adFrequencyModalMsgs = false;
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else {
                            toastr.error(result.message, 'Error');
                        }
                    })
                } else {

                }
            }
            else {
                $scope.adFrequencyModalMsgs = true;
                var FrequencyModal = angular.element(document.querySelector('#FrequencyModal'))
                FrequencyModal.show();
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.CancelFrequency = function (frequency) {
        try {
            // console.log('Cancel called');
            $scope.FrequencyModalForm.$setPristine();
            $scope.adFrequencyModalMsgs = false;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    //Init Ad Color Modal Pop up and code 
    $scope.InitAdColorPopup = function () {
        try {
            $scope.adColor = {};
            $scope.adColor.AdColorId = 0;
            $scope.AdColorModalForm.$setPristine();
            $scope.adColorModalMsgs = false;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function CreateAdColorButtonName() {
        try {
            var adColorButton = angular.element(document.querySelector('#btnadColor'));
            adColorButton.text('Add Ad Color');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.SaveAdColor = function (adColor) {
        try {
            if ($scope.AdColorModalForm.$valid) {
                console.log(adColor);
                if (adColor.AdColorId == 0) {
                    adColorFactory.save(adColor, function (result) {
                        if (result.statusCode == 1) {
                            $scope.adColor = {};
                            $scope.adColor.AdColorId = 0;
                            getAllAdColor();
                            CreateAdColorButtonName();
                            var repModal = angular.element(document.querySelector('#adColorModal'))
                            repModal.modal('hide');
                            $scope.AdColorModalForm.$setPristine();
                            $scope.adColorModalMsgs = false;
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else {
                            toastr.error(result.message, 'Error');
                        }
                    })
                } else {

                }
            }
            else {
                $scope.adColorModalMsgs = true;
                var adColorModal = angular.element(document.querySelector('#adColorModal'))
                adColorModal.show();
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.CancelAdColor = function (mediaAsset) {
        try {
            // console.log('Cancel called');
            $scope.AdColorModalForm.$setPristine();
            $scope.adColorModalMsgs = false;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    //Init Ad Size Modal Pop up and code
    $scope.InitAdSizePopup = function () {
        try {
            $scope.adSize = {};
            $scope.adSize.AdSizeId = 0;
            $scope.AdSizeModalForm.$setPristine();
            $scope.adSizeModalMsgs = false;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function CreateAdSizeButtonName() {
        try {
            var adSizeButton = angular.element(document.querySelector('#btnadSize'));
            adSizeButton.text('Add Ad Size');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.SaveAdSize = function (adSize) {
        try {
            $scope.adSize.MediaAssetId = mediaAssetId;
            $scope.adSize.PageFraction = parseFloat(adSize.PageFraction);
            if ($scope.AdSizeModalForm.$valid) {
                console.log(adSize);
                if (adSize.AdSizeId == 0) {
                    adSizeFactory.save(adSize, function (result) {
                        if (result.statusCode == 1) {
                            $scope.adSize = {};
                            $scope.adSize.AdSizeId = 0;
                            getAllAdSize();
                            CreateAdSizeButtonName();
                            var Modal = angular.element(document.querySelector('#adSizeModal'))
                            Modal.modal('hide');
                            $scope.AdSizeModalForm.$setPristine();
                            $scope.adSizeModalMsgs = false;
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else {
                            toastr.error(result.message, 'Error');
                        }
                    })
                } else {

                }
            }
            else {
                $scope.adSizeModalMsgs = true;
                var adSizeModal = angular.element(document.querySelector('#adSizeModal'))
                adSizeModal.show();
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.CancelAdSize = function (adSize) {
        try {
            // console.log('Cancel called');
            $scope.AdSizeModalForm.$setPristine();
            $scope.adSizeModalMsgs = false;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    //Dropdown change event

    //  var latestSizeSelected = [];
    $scope.changeAdSize = function () {
        try {
            var adSizeDropDown = angular.element(document.querySelector('#adjtarget1'));
            var getAllSelectedSize = adSizeDropDown.select2("data");
            var sizeIds = adSizeDropDown.select2("val");
            var lastSizeLength = sizeIds.length - 1;
            var id = sizeIds[lastSizeLength];
            // latestSizeSelected.push(id);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    //  var latestFrequencySelected = [];
    $scope.changeFrequency = function () {
        try {
            var adFrequencyDropDown = angular.element(document.querySelector('#adjtarget2'));
            var getAllSelectedFrequency = adFrequencyDropDown.select2("data");
            var frequencyIds = adFrequencyDropDown.select2("val");
            var lastFrequencyLength = frequencyIds.length - 1;
            var id = frequencyIds[lastFrequencyLength];
            //  latestFrequencySelected.push(id);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    getAllAdColor();
    getAllAdSize();
    getAllFrequency();
    getRateCardDetailsByRateCardId($stateParams.RateCardId);
    //initCloseEvent();

})