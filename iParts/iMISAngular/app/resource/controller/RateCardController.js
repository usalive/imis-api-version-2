﻿'use strict';

app.controller('RateCardController', function RateCardController($scope, $location, $stateParams, mediaTypeFactory, mediaBillingMethodFactory, AdTypeFactory, rateCardFactory, authKeyService, toastr) {

    $scope.AddOREdit = "ADD";
    $scope.rateCard = {};
    $scope.rateCards = {};
    $scope.rateCard.RateCardId = 0;
    $scope.rateCard.MediaAssetId = $stateParams.mediaAssetId;
    $scope.adTypes = {};
    $scope.mediaTypes = {};
    $scope.mediaBillingMethods = {};
    $scope.showMsgs = false;
    $scope.mediaAssetItemCode = {};
    $scope.isShowDefaultTable = false;

    // Res Pop up Initiallize
    $scope.adType = {};
    $scope.adType.adTypeId = 0;
    $scope.adTypeModalMsgs = false;

    // Global variable for controller
    var authKey = authKeyService.getConstantAuthKey();
    $scope.baseUrl = authKey.baseUrl;

    $scope.rateCard.IsCopy = true;
    $scope.rateCard.SourceRateCardId = 0;

    var InitialImagePath = authKey.baseUrl + 'areas/imisangular/image/upload-icon.png';
    $scope.ImageSrc = InitialImagePath;
    $scope.isLoading = true;
    $scope.dataLoading = true;

    $scope.adTypesSelect2 = {
        multiple: false,
        formatSearching: 'Searching the ad types Method...',
        formatNoMatches: 'No ad types found'
    };

    $scope.adMediaTypesSelect2 = {
        multiple: false,
        formatSearching: 'Searching the ad media types...',
        formatNoMatches: 'No ad media types found'
    };

    $scope.adMediaBillingSelect2 = {
        multiple: false,
        formatSearching: 'Searching the media billing method...',
        formatNoMatches: 'No media billing method found'
    };

    $scope.RateCardName = "";

    //init Select2
    clearSelect2();

    $scope.$parent.tabMediaAssetId = $stateParams.mediaAssetId;
    $scope.$parent.setRateCardTabActive();

    function initDate() {
        try {
            var ValidFromDate = angular.element(document.querySelector('#ValidFromDate'));
            var ValidToDate = angular.element(document.querySelector('#ValidToDate'));
            ValidFromDate.datepicker();
            ValidToDate.datepicker();
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function initPopover() {
        try {
            var copyPopover = angular.element(document.querySelector('.tip'));
            copyPopover.popover({
                placement: 'top',
                trigger: 'hover',
                html: true
            });
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function GetMediaTypeData() {
        try {
            mediaTypeFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data.length > 0) {
                        $scope.mediaTypes = result.data;
                    }
                }
                else {
                    //alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function GetAdTypeData() {
        try {
            AdTypeFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data.length > 0) {
                        $scope.adTypes = result.data;
                    }
                }
                else {
                    //alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function GetMediaBillingMethodData() {
        try {
            mediaBillingMethodFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data.length > 0) {
                        $scope.mediaBillingMethods = result.data;
                    }
                }
                else {
                    //alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function GetRateCardsbyMediaAsset(mediaAssetId) {
        try {
            rateCardFactory.getByMediaAsset(mediaAssetId, function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data.length > 0) {

                        $scope.rateCards.MediaAssetId = result.data[0].MediaAssetId;
                        $scope.MediaAssetName = result.data[0].MediaAssetName;
                        if (result.data[0].RateCards.length > 0) {
                            $scope.rateCards = result.data[0].RateCards;
                            $scope.isShowDefaultTable = false;
                            $scope.dataLoading = false;
                        }
                        else {
                            $scope.rateCards = [];
                            $scope.isShowDefaultTable = true;
                            $scope.dataLoading = false;
                        }

                        //$scope.rateCards = result.data;
                        //$scope.MediaAssetName = result.data[0].MediaAssetName;
                        //$scope.isShowDefaultTable = false;
                    } else {
                        $scope.rateCards = result.data;
                        $scope.isShowDefaultTable = true;
                        $scope.dataLoading = false;
                    }
                } else if (result.statusCode == 3) {
                    $scope.rateCards = result.data;
                    $scope.isShowDefaultTable = true;
                    $scope.dataLoading = false;
                }
                else {
                    $scope.rateCards = [];
                    $scope.isShowDefaultTable = true;
                    $scope.dataLoading = false;
                    //alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getDateInFormat(date) {
        try {
            var dateObj = new Date(date);
            var month = dateObj.getMonth() + 1;
            if (month.toString().length == 1) {
                month = "0" + month;
            }
            var day = dateObj.getDate();
            if (day.toString().length == 1) {
                day = "0" + day;
            }
            var year = dateObj.getFullYear();
            var newdate = month + "/" + day + "/" + year;
            return newdate;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function UpdaterateCardButtonName() {
        try {
            var rateCardButton = angular.element(document.querySelector('#btnRateCard'));
            rateCardButton.text('Update');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function CreaterateCardButtonName() {
        try {
            var rateCardButton = angular.element(document.querySelector('#btnRateCard'));
            rateCardButton.text('Create');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.SaveRateCard = function (ojbrateCard) {
        try {
            //$scope.rateCard.RateCardName = $scope.RateCardName;
            ojbrateCard.ProductCode = "code3";
            ojbrateCard.RateCardName = $scope.RateCardName;
            if ($scope.rateCardForm.$valid) {
                console.log(ojbrateCard);
                if (ojbrateCard.RateCardId == 0) {
                    rateCardFactory.save(ojbrateCard, function (result) {
                        if (result.statusCode == 1) {
                            GetRateCardsbyMediaAsset($stateParams.mediaAssetId);
                            $scope.AddOREdit = "ADD";
                            $scope.rateCard = {};
                            $scope.RateCardName = "";
                            $scope.rateCard.RateCardId = 0;
                            $scope.rateCard.MediaAssetId = $stateParams.mediaAssetId;
                            $scope.rateCard.IsCopy = true;
                            $scope.rateCard.SourceRateCardId = 0;
                            clearSelect2();
                            $scope.rateCardForm.$setPristine();
                            $scope.showMsgs = false;
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else {
                            toastr.error(result.message, 'Error');
                        }
                    })
                } else {
                    rateCardFactory.update(ojbrateCard, function (result) {
                        if (result.statusCode == 1) {
                            GetRateCardsbyMediaAsset($stateParams.mediaAssetId);
                            $scope.AddOREdit = "ADD";
                            $scope.rateCard = {};
                            $scope.RateCardName = "";
                            $scope.rateCard.RateCardId = 0;
                            $scope.rateCard.MediaAssetId = $stateParams.mediaAssetId;
                            $scope.rateCard.IsCopy = true;
                            $scope.rateCard.SourceRateCardId = 0;
                            clearSelect2();
                            CreaterateCardButtonName();
                            $scope.rateCardForm.$setPristine();
                            $scope.showMsgs = false;
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else {
                            toastr.error(result.message, 'Error');
                        }
                    })
                }
            }
            else {
                $scope.showMsgs = true;
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.EditRateCard = function (rateCard) {
        try {
            UpdaterateCardButtonName();
            rateCard.ValidFromDate = getDateInFormat(rateCard.ValidFromDate);
            rateCard.ValidToDate = getDateInFormat(rateCard.ValidToDate);
            $scope.rateCard = rateCard;
            $scope.RateCardName = $scope.rateCard.RateCardName;
            setSelect2(rateCard);
            $scope.AddOREdit = "EDIT";
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function setSelect2(objRateCard) {
        try {
            $scope.rateCard.AdTypeId = objRateCard.AdTypeId.toString();
            $scope.rateCard.MediaTypeId = objRateCard.MediaTypeId.toString();
            $scope.rateCard.MediaBillingMethodId = objRateCard.MediaBillingMethodId.toString();
            initSelect2("s2id_AdType", objRateCard.AdTypeName);
            initSelect2("s2id_MediaType", objRateCard.MediaTypeName);
            initSelect2("s2id_MediaBillingMethod", objRateCard.MediaBillingMethodName);
            changeSelect2DropDownColor("s2id_AdType", objRateCard.AdTypeId);
            changeSelect2DropDownColor("s2id_MediaType", objRateCard.MediaTypeId);
            changeSelect2DropDownColor("s2id_MediaBillingMethod", objRateCard.MediaBillingMethodId);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function clearSelect2() {
        try {
            changeSelect2DropDownColor("s2id_AdType", "");
            changeSelect2DropDownColor("s2id_MediaType", "");
            changeSelect2DropDownColor("s2id_MediaBillingMethod", "");
            initSelect2("s2id_AdType", "Select Ad Type");
            initSelect2("s2id_MediaType", "Select Media Type");
            initSelect2("s2id_MediaBillingMethod", "Select Media Billing Method");
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.copyRateCard = function (rateCard) {
        try {
            var SourceRateCardId = rateCard.RateCardId;
            $scope.rateCard = rateCard;
            rateCard.ValidFromDate = getDateInFormat(rateCard.ValidFromDate);
            rateCard.ValidToDate = getDateInFormat(rateCard.ValidToDate);
            $scope.rateCard.RateCardId = 0;
            $scope.rateCard.MediaAssetId = $stateParams.mediaAssetId;
            $scope.rateCard.IsCopy = true;
            $scope.rateCard.SourceRateCardId = SourceRateCardId;
            $scope.RateCardName = $scope.rateCard.RateCardName;
            setSelect2(rateCard);
            CreaterateCardButtonName();
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.DeleteRateCard = function (objrateCard) {
        try {
            console.log(objrateCard);
            var selectedrateCard = $scope.rateCard;
            bootbox.confirm({
                message: "Are you sure you want to delete this Media Asset?",
                //message: "Are you sure you want to delete the selected object(s) and all of their children?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        rateCardFactory.delete(objrateCard.RateCardId, function (result) {
                            if (result.statusCode == 1) {

                                var UpdateRecord = selectedrateCard.RateCardId;
                                var deletedRecord = objrateCard.RateCardId;
                                if (UpdateRecord == deletedRecord) {
                                    $scope.rateCard = {};
                                    $scope.rateCard.RateCardId = 0;
                                    $scope.RateCardName = "";
                                    $scope.rateCard.MediaAssetId = $stateParams.mediaAssetId;
                                    $scope.rateCard.IsCopy = true;
                                    $scope.rateCard.SourceRateCardId = 0;
                                    CreaterateCardButtonName();
                                    clearSelect2();
                                    $scope.rateCardForm.$setPristine();
                                }
                                GetRateCardsbyMediaAsset($stateParams.mediaAssetId)
                                toastr.success('Deleted successfully.', 'Success!');
                            } else {
                                toastr.error(result.message, 'Error');
                            }
                        })
                    }
                }
            });
            //if (confirm("Are you sure you want to delete the selected object(s) and all of their children?")) {

            //}
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.updateAdType = function () {
        //var adTypeDropDown = angular.element(document.querySelector('#AdType'));
        //var selectedValue = adTypeDropDown.find(":selected").text();
        //$scope.rateCard.AdTypeName = selectedValue;
        try {
            var DropDown = angular.element(document.querySelector('#AdType'));
            var getSelectedData = DropDown.select2("data");
            var Id = getSelectedData.id;
            var Value = getSelectedData.text;
            $scope.rateCard.AdTypeName = Value;
            if (Id != undefined && Id != "" && Id != 0 && Id != null) {
                changeSelect2DropDownColor("s2id_AdType", Id);
            } else {
                changeSelect2DropDownColor("s2id_AdType", "");
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    }

    $scope.updateMediaType = function () {
        //var MediaTypeDropDown = angular.element(document.querySelector('#MediaType'));
        //var selectedValue = MediaTypeDropDown.find(":selected").text();
        //$scope.rateCard.MediaTypeName = selectedValue;

        try {
            var DropDown = angular.element(document.querySelector('#MediaType'));
            var getSelectedData = DropDown.select2("data");
            var Id = getSelectedData.id;
            var Value = getSelectedData.text;
            $scope.rateCard.MediaTypeName = Value;
            if (Id != undefined && Id != "" && Id != 0 && Id != null) {
                changeSelect2DropDownColor("s2id_MediaType", Id);
            } else {
                changeSelect2DropDownColor("s2id_MediaType", "");
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }

    }

    $scope.updateMediaBillingMethod = function () {
        //var MBMethodDropDown = angular.element(document.querySelector('#MediaBillingMethod'));
        //var selectedValue = MBMethodDropDown.find(":selected").text();
        //$scope.rateCard.MediaBillingMethodName = selectedValue;

        try {
            var DropDown = angular.element(document.querySelector('#MediaBillingMethod'));
            var getSelectedData = DropDown.select2("data");
            var Id = getSelectedData.id;
            var Value = getSelectedData.text;
            $scope.rateCard.MediaBillingMethodName = Value;
            if (Id != undefined && Id != "" && Id != 0 && Id != null) {
                changeSelect2DropDownColor("s2id_MediaBillingMethod", Id);
            } else {
                changeSelect2DropDownColor("s2id_MediaBillingMethod", "");
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.redirectToRateCardsDetails = function (rateCard) {
        try {
            $scope.$parent.tabRateCardId = rateCard.RateCardId;
            $scope.$parent.setRateCardsDetailsTabActive();
            $location.path('mediaAssetsDetails/rateCardsDetails/' + rateCard.RateCardId + '');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    // Init Pop up 
    $scope.InitAdTypePopup = function () {
        try {
            $scope.adType = {};
            $scope.adType.adTypeId = 0;
            $scope.AdTypeModalForm.$setPristine();
            $scope.adTypeModalMsgs = false;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function CreateAdTypeButtonName() {
        try {
            var adTypeButton = angular.element(document.querySelector('#btnAdType'));
            adTypeButton.text('Create');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.SaveAdType = function (adType) {
        try {
            if ($scope.AdTypeModalForm.$valid) {
                console.log(adType);
                if (adType.adTypeId == 0) {
                    AdTypeFactory.save(adType, function (result) {
                        if (result.statusCode == 1) {
                            $scope.adType = {};
                            $scope.adType.adTypeId = 0;
                            GetAdTypeData();
                            CreateAdTypeButtonName();
                            var repModal = angular.element(document.querySelector('#adTypeModal'))
                            repModal.modal('hide');
                            $scope.AdTypeModalForm.$setPristine();
                            $scope.adTypeModalMsgs = false;
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else {
                            toastr.error(result.message, 'Error');
                        }
                    })
                } else {

                }
            }
            else {
                $scope.adTypeModalMsgs = true;
                var adTypeModal = angular.element(document.querySelector('#adTypeModal'))
                adTypeModal.show();
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.CancelAdType = function (mediaAsset) {
        try {
            $scope.AdTypeModalForm.$setPristine();
            $scope.adTypeModalMsgs = false;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    initDate();
    initPopover();
    GetMediaTypeData();
    GetAdTypeData();
    GetMediaBillingMethodData();
    GetRateCardsbyMediaAsset($stateParams.mediaAssetId)

})