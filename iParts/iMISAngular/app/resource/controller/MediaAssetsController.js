'use strict';

app.controller('MediaAssetsController', function MediaAssetsController($scope, $location, mediaTypeFactory, mediaAssetGroupFactory, mediaAssetsFactory,
    $exceptionHandler, authKeyService, toastr) {
    $scope.mediaTypes = {};
    $scope.mediaAssetGroups = {};
    $scope.mediaAssets = {};
    $scope.mediaAsset = {};
    $scope.mediaAsset.mediaAssetId = 0;
    $scope.mediaAssetItemCode = [];
    var authKey = authKeyService.getConstantAuthKey();
    $scope.baseUrl = authKey.baseUrl;
    $scope.isShowDefaultTable = false;
    $scope.showMsgs = false;
    var baseUrl = authKey.baseUrl;
    var authToken = authKey.authToken;
    $scope.isLoading = true;
    $scope.dataLoading = true;

    // Media Asset Group Name Pop up Initiallize
    $scope.MediaAssetGroup = {};
    $scope.MediaAssetGroup.MediaAssetGroupId = 0;
    $scope.MediaAssetGroupNameModalMsgs = false;

    //  $scope.selectedAdColorValue = [];
    $scope.$parent.setMediaAssetsTabActive();

    $scope.groupSetup = {
        multiple: false,
        formatSearching: 'Searching the product Code...',
        formatNoMatches: 'No product code found'
    };

    $scope.mediaGroupSelect2 = {
        multiple: false,
        formatSearching: 'Searching the media group...',
        formatNoMatches: 'No media group found'
    };

    $scope.mediaTypeSelect2 = {
        multiple: false,
        formatSearching: 'Searching the media type...',
        formatNoMatches: 'No media type found'
    };
    var IsImageChange = false;
    var InitialImagePath = authKey.baseUrl + 'areas/imisangular/image/upload-icon.png';
    $scope.ImageSrc = InitialImagePath;

    clearSelect2();

    function GetMediaTypeData() {
        try {
            mediaTypeFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    $scope.mediaTypes = result.data;
                }
                else {
                    //alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function GetMediaAssetGroupData() {
        try {
            mediaAssetGroupFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    $scope.mediaAssetGroups = result.data;
                }
                else {
                    //alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function GetMediaAssetsData() {
        try {
            mediaAssetsFactory.getAll(function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data.length > 0) {
                        $scope.mediaAssets = result.data;
                        $scope.isShowDefaultTable = false;
                        $scope.dataLoading = false;
                    }
                    else {
                        $scope.mediaAssets = result.data;
                        $scope.isShowDefaultTable = true;
                        $scope.dataLoading = false;
                    }
                } else if (result.statusCode == 3) {
                    $scope.isShowDefaultTable = true;
                    $scope.dataLoading = false;
                }
                else {
                    $scope.isShowDefaultTable = true;
                    $scope.dataLoading = false;
                    //alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function GetMediaAssets(maid) {
        try {
            mediaAssetsFactory.get(maid, function (result) {
                if (result.statusCode == 1)
                    console.log(result);
                else {
                    //alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function CancelMediaAsset() {
        try {
            var mediaAssetButton = angular.element(document.querySelector('#btnMediaAsset'));
            mediaAssetButton.text('Create Media Asset');
            //(angular.element(document.querySelector('#MediaAssetsImage'))).attr('src', InitialImagePath);
            (angular.element(document.querySelector('#MediaAssetsImage'))).attr('src', $scope.ImageSrc);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function UpdateMediaAssetsButtonName() {
        try {
            var mediaAssetButton = angular.element(document.querySelector('#btnMediaAsset'));
            mediaAssetButton.text('Update Media Asset');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function CreateMediaAssetsButtonName() {
        try {
            var mediaAssetButton = angular.element(document.querySelector('#btnMediaAsset'));
            mediaAssetButton.text('Create Media Asset');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function InitMediaAssetPopup(mediaAsset) {
        try {
            $scope.mediaAsset = {};
            $scope.mediaAsset.mediaAssetId = 0;
            $scope.ImageSrc = InitialImagePath;
            //itemDataSelect();
            clearSelect2();
            var mediaAssetButton = angular.element(document.querySelector('#btnMediaAsset'));
            mediaAssetButton.text('Create Media Asset');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function itemDataSelect() {
        try {
            var mediaAssetButton = angular.element(document.querySelector('#select2-chosen-1'));
            mediaAssetButton.text("Select Item Code");
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function GetMediaAssetItemData() {
        try {
            var HasNext = true;
            var offset = 0;
            var advertisers = [];
            var contacts = [];
            function getItemDataASI() {
                mediaAssetsFactory.getAllItemsummary(baseUrl, authToken, offset, function (result) {
                    if (result != null && result != undefined && result != "") {
                        var ItemData = result.Items.$values
                        console.log(ItemData);
                        angular.forEach(ItemData, function (itemdatavalue, key) {
                            $scope.mediaAssetItemCode.push(itemdatavalue);
                        });
                        var TotalCount = result.TotalCount;
                        HasNext = result.HasNext;
                        offset = result.Offset;
                        var Limit = result.Limit;
                        var NextOffset = result.NextOffset;
                        if (HasNext == true) {
                            offset = NextOffset;
                            getItemDataASI();
                        } else {

                        }
                    }
                    else {
                        $scope.mediaAssetItemCode = [];
                    }
                })
            }
            getItemDataASI();
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.redirectToIssueDate = function (mediaAsset) {
        try {
            $scope.$parent.tabMediaAssetId = mediaAsset.mediaAssetId;
            $scope.$parent.setIssueDateTabActive();
            $location.path('mediaAssetsDetails/issueDate/' + mediaAsset.mediaAssetId + '');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.redirectToAdjustment = function (mediaAsset) {
        try {
            $scope.$parent.tabMediaAssetId = mediaAsset.mediaAssetId;
            $scope.$parent.setAdAdjustmentTabActive();
            $location.path('mediaAssetsDetails/AdAdjustment/' + mediaAsset.mediaAssetId + '');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.redirectToRateCard = function (mediaAsset) {
        try {
            $scope.$parent.tabMediaAssetId = mediaAsset.mediaAssetId;
            $scope.$parent.setRateCardTabActive();
            $location.path('mediaAssetsDetails/rateCard/' + mediaAsset.mediaAssetId + '');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.uploadImage = function (files) {
        try {
            var logoElement = angular.element(document.querySelector("#file-upload"))[0];
            console.log();
            if (logoElement.files && logoElement.files[0]) {
                var FR = new FileReader();
                FR.onload = function (e) {
                    var strImage = e.target.result.replace(/^data:image\/[a-z]+;base64,/, "");
                    $scope.mediaAsset.logo = strImage;
                    $scope.$apply(function () {
                        $scope.ImageSrc = e.target.result;
                        IsImageChange = true;
                    });
                };
                FR.readAsDataURL(logoElement.files[0]);
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.SaveMediaAsset = function (mediaAsset) {
        try {
            if ($scope.MediaAssetsForm.$valid) {
                if (IsImageChange == false) {
                    $scope.mediaAsset.logo = "";
                }
                console.log($scope.mediaAsset);
                if (mediaAsset.mediaAssetId == 0) {
                    mediaAssetsFactory.save(mediaAsset, function (result) {
                        if (result.statusCode == 1) {
                            GetMediaAssetsData();
                            $scope.mediaAsset = {};
                            $scope.mediaAsset.mediaAssetId = 0;
                            $scope.ImageSrc = InitialImagePath;
                            //itemDataSelect();
                            //changeSelect2DropDownColor("s2id_productcode", "");
                            clearSelect2();
                            var Modal = angular.element(document.querySelector('#mediaAssetModal'))
                            Modal.modal('hide');
                            $scope.MediaAssetsForm.$setPristine();
                            $scope.showMsgs = false;
                            IsImageChange = false
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else {
                            toastr.error(result.message, 'Error!');
                            // alert(result.message);
                        }
                    })
                } else {
                    CancelMediaAsset();
                    mediaAssetsFactory.update(mediaAsset, function (result) {
                        if (result.statusCode == 1) {
                            GetMediaAssetsData();
                            $scope.mediaAsset = {};
                            $scope.mediaAsset.mediaAssetId = 0;
                            CreateMediaAssetsButtonName();
                            $scope.ImageSrc = InitialImagePath;
                            clearSelect2();
                            //changeSelect2DropDownColor("s2id_productcode", "");
                            //itemDataSelect();
                            var Modal = angular.element(document.querySelector('#mediaAssetModal'))
                            Modal.modal('hide');
                            $scope.MediaAssetsForm.$setPristine();
                            $scope.showMsgs = false;
                            IsImageChange = false
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else {
                            toastr.error(result.message, 'Error!');
                            //alert(result.message);
                        }
                    })
                }
            } else {
                $scope.showMsgs = true;
                var adTypeModal = angular.element(document.querySelector('#mediaAssetModal'))
                adTypeModal.show();
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.EditMediaAsset = function (mediaAsset) {
        try {
            UpdateMediaAssetsButtonName();
            $scope.mediaAsset = mediaAsset;
            var ImagePath = mediaAsset.logo;
            if (ImagePath != null && ImagePath != undefined && ImagePath != "") {
                $scope.ImageSrc = ImagePath;
            }
            else {
                $scope.ImageSrc = InitialImagePath;
            }
            //var productitem = mediaAsset.productCode;
            //var itemData = $scope.mediaAssetItemCode;
            //var itemcode = '';
            //var itemName = '';
            //var mergeName = '';
            //angular.forEach(itemData, function (items, key2) {
            //    if (items.ItemId == productitem) {
            //        itemcode = items.ItemCode;
            //        itemName = items.Name;
            //        mergeName = itemcode + " " + "(" + itemName + ")";
            //    }
            //});
            //var mediaAssetButton = angular.element(document.querySelector('#select2-chosen-1'));
            //mediaAssetButton.text(mergeName);
            //changeSelect2DropDownColor("s2id_productcode", itemcode);
            setSelect2(mediaAsset);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.CancelMediaAsset = function (mediaAsset) {
        try {
            CancelMediaAsset();
            clearSelect2();
            //changeSelect2DropDownColor("s2id_productcode", "");
            $scope.MediaAssetsForm.$setPristine();
            $scope.showMsgs = false;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function setSelect2(objMediaAsset) {
        try {
            $scope.mediaAsset.productCode = objMediaAsset.productCode.toString();
            $scope.mediaAsset.mediaAssetGroupId = objMediaAsset.mediaAssetGroupId.toString();
            $scope.mediaAsset.mediaTypeId = objMediaAsset.mediaTypeId.toString();

            var productitem = objMediaAsset.productCode;
            var itemData = $scope.mediaAssetItemCode;
            var itemcode = '';
            var itemName = '';
            var mergeName = '';
            angular.forEach(itemData, function (items, key2) {
                if (items.ItemId == productitem) {
                    itemcode = items.ItemCode;
                    itemName = items.Name;
                    mergeName = itemcode + " " + "(" + itemName + ")";
                }
            });

            initSelect2("s2id_productcode", mergeName);
            initSelect2("s2id_mediatype", objMediaAsset.mediaTypeName);
            initSelect2("s2id_mediagroup", objMediaAsset.mediaAssetGroupName);
            changeSelect2DropDownColor("s2id_productcode", objMediaAsset.productCode);
            changeSelect2DropDownColor("s2id_mediagroup", objMediaAsset.mediaAssetGroupId);
            changeSelect2DropDownColor("s2id_mediatype", objMediaAsset.mediaTypeId);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function clearSelect2() {
        try {
            initSelect2("s2id_productcode", "Select Item Code");
            initSelect2("s2id_mediagroup", "Select Media Group");
            initSelect2("s2id_mediatype", "Select Media Type");
            changeSelect2DropDownColor("s2id_productcode", "");
            changeSelect2DropDownColor("s2id_mediagroup", "");
            changeSelect2DropDownColor("s2id_mediatype", "");
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.InitMediaAssetPopup = function (mediaAsset) {
        try {
            InitMediaAssetPopup(mediaAsset);
            //changeSelect2DropDownColor("s2id_productcode", "");
            $scope.MediaAssetsForm.$setPristine();
            $scope.showMsgs = false;
            //changeMediaAssetsButtonCreate();
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.DeleteMediaAsset = function (objmediaAsset) {
        try {
            console.log(objmediaAsset);
            var selectedmediaAsset = $scope.mediaAsset;
            bootbox.confirm({
                message: "Are you sure you want to delete this Media Asset?",
                // message: "Are you sure you want to delete the selected object(s) and all of their children?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        mediaAssetsFactory.delete(objmediaAsset.mediaAssetId, function (result) {
                            if (result.statusCode == 1) {

                                var UpdateRecord = selectedmediaAsset.mediaAssetId;
                                var deletedRecord = objmediaAsset.mediaAssetId;
                                if (UpdateRecord == deletedRecord) {
                                    $scope.mediaAsset = {};
                                    $scope.mediaAsset.mediaAssetId = 0;
                                    CreateMediaAssetsButtonName();
                                    $scope.ImageSrc = InitialImagePath;
                                    clearSelect2();
                                    //itemDataSelect();
                                    IsImageChange = false;
                                    $scope.MediaAssetsForm.$setPristine();
                                }

                                GetMediaAssetsData();
                                toastr.success('Deleted successfully.', 'Success!');
                            }
                            else {
                                toastr.error(result.message, 'Error!');
                            }
                        })
                    }
                }
            });
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    // Init Pop up 
    $scope.InitMediaAssetGroupModalPopup = function () {
        try {
            $scope.MediaAssetGroup = {};
            $scope.MediaAssetGroup.MediaAssetGroupId = 0;
            $scope.MediaAssetGroupNameModalMsgs = false;
            $scope.mediaAssetGroupForm.$setPristine();
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }
    // Media Asset Group Pop up Code
    function CreateMediaAssetGroupButtonName() {
        try {
            var MAGroupButton = angular.element(document.querySelector('#btnMediaAssetGroup'));
            MAGroupButton.text('Create');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.SaveMediaAssetGroupForm = function (MediaAssetGroup) {
        try {
            MediaAssetGroup.MediaAssetGroupParentGroupId = 1;
            if ($scope.mediaAssetGroupForm.$valid) {
                console.log(MediaAssetGroup);
                if (MediaAssetGroup.MediaAssetGroupId == 0) {
                    mediaAssetGroupFactory.save(MediaAssetGroup, function (result) {
                        if (result.statusCode == 1) {
                            $scope.MediaAssetGroup = {};
                            $scope.MediaAssetGroup.MediaAssetGroupId = 0;
                            GetMediaAssetGroupData();
                            CreateMediaAssetGroupButtonName();
                            var Modal = angular.element(document.querySelector('#mediaAssetGroupModal'))
                            Modal.modal('hide');
                            $scope.mediaAssetGroupForm.$setPristine();
                            $scope.MediaAssetGroupNameModalMsgs = false;
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else
                            toastr.error(result.message, 'Error');
                    })
                } else {

                }
            }
            else {
                $scope.MediaAssetGroupNameModalMsgs = true;
                var repModal = angular.element(document.querySelector('#repsModal'))
                repModal.show();
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.CancelMediaAssetGroupForm = function (MediaAssetGroup) {
        try {
            $scope.mediaAssetGroupForm.$setPristine();
            $scope.MediaAssetGroupNameModalMsgs = false;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onProductCodeChange = function () {
        try {
            var DropDown = angular.element(document.querySelector('#productcode'));
            var getSelectedData = DropDown.select2("data");
            var Id = getSelectedData.id;
            var Value = getSelectedData.text;
            if (Id != undefined && Id != "" && Id != 0 && Id != null) {
                changeSelect2DropDownColor("s2id_productcode", Id);
            } else {
                changeSelect2DropDownColor("s2id_productcode", "");
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onMediaGroupChange = function () {
        try {
            var DropDown = angular.element(document.querySelector('#mediagroup'));
            var getSelectedData = DropDown.select2("data");
            var Id = getSelectedData.id;
            var Value = getSelectedData.text;
            if (Id != undefined && Id != "" && Id != 0 && Id != null) {
                changeSelect2DropDownColor("s2id_mediagroup", Id);
            } else {
                changeSelect2DropDownColor("s2id_mediagroup", "");
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.onMediaTypeChange = function () {
        try {
            var DropDown = angular.element(document.querySelector('#mediatype'));
            var getSelectedData = DropDown.select2("data");
            var Id = getSelectedData.id;
            var Value = getSelectedData.text;
            if (Id != undefined && Id != "" && Id != 0 && Id != null) {
                changeSelect2DropDownColor("s2id_mediatype", Id);
            } else {
                changeSelect2DropDownColor("s2id_mediatype", "");
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    //GetMediaAssets(1);
    GetMediaAssetsData();
    GetMediaTypeData();
    GetMediaAssetGroupData();
    GetMediaAssetItemData();
})