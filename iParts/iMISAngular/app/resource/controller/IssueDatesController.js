'use strict';

app.controller('IssueDatesController', function IssueDatesController($scope, $stateParams, mediaTypeFactory, mediaAssetGroupFactory, mediaAssetsFactory, issueDatesFactory,
    $exceptionHandler, authKeyService, toastr) {

    $scope.AddOREdit = "ADD";
    $scope.issueDates = {};
    $scope.issueDate = {};
    $scope.mediaAsset = {};
    $scope.mediaAsset.mediaAssetId = $stateParams.mediaAssetId;
    $scope.issueDate.IssueDateId = 0;
    $scope.issueDate.MediaAssetId = $stateParams.mediaAssetId;
    var authKey = authKeyService.getConstantAuthKey();
    $scope.baseUrl = authKey.baseUrl;
    $scope.showMsgs = false;
    $scope.isShowDefaultTable = false;
    $scope.isLoading = true;
    $scope.dataLoading = true;

    $scope.$parent.tabMediaAssetId = $stateParams.mediaAssetId;
    $scope.$parent.setIssueDateTabActive();

    function GetIssueDates(mediaAssetId) {
        try {
            issueDatesFactory.getByMediaAsset(mediaAssetId, function (result) {
                if (result.statusCode == 1) {
                    console.log(result);
                    if (result.data.length > 0) {

                        $scope.issueDates.MediaAssetId = result.data[0].MediaAssetId;
                        $scope.mediaAsset.MediaAssetName = result.data[0].MediaAssetName;
                        if (result.data[0].IssueDates.length > 0) {
                            $scope.issueDates = result.data[0].IssueDates;
                            $scope.isShowDefaultTable = false;
                            $scope.dataLoading = false;
                        }
                        else {
                            $scope.issueDates = [];
                            $scope.isShowDefaultTable = true;
                            $scope.dataLoading = false;
                        }

                        //$scope.issueDates = result.data;
                        //$scope.mediaAsset.MediaAssetName = result.data[0].MediaAssetName;
                        //$scope.isShowDefaultTable = false;
                    }
                    else {
                        $scope.issueDates = [];
                        $scope.isShowDefaultTable = true;
                        $scope.dataLoading = false;
                    }
                } else if (result.statusCode == 3) {
                    $scope.issueDates = [];
                    $scope.isShowDefaultTable = true;
                    $scope.dataLoading = false;
                }
                else {
                    //alert(result.message);
                }
            })
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function getDateInFormat(date) {
        try {
            var dateObj = new Date(date);
            var month = dateObj.getMonth() + 1;
            if (month.toString().length == 1) {
                month = "0" + month;
            }
            var day = dateObj.getDate();
            if (day.toString().length == 1) {
                day = "0" + day;
            }
            var year = dateObj.getFullYear();
            var newdate = month + "/" + day + "/" + year;
            return newdate;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function InitDateRange() {
        try {
            var coverDateControl = angular.element(document.querySelector('#txtCoverDate'));
            var adClosingDateControl = angular.element(document.querySelector('#txtAdClosingDate'));
            var materialDueDateControl = angular.element(document.querySelector('#txtMaterialDueDate'));
            // coverDateControl.datepicker();
            adClosingDateControl.datepicker();
            materialDueDateControl.datepicker();
            coverDateControl.datepicker({
                onSelect: function (date) {
                    setDateRange(date);
                    $scope.issueDate.CoverDate = getDateInFormat(date);
                    // $scope.IssueDateForm.CoverDate.$valid = true;
                    // $scope.IssueDateForm.CoverDate.$error.required = false;
                }
            });
            adClosingDateControl.datepicker("option", "maxDate", null);
            materialDueDateControl.datepicker("option", "maxDate", null);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function setDateRange(date) {
        try {
            var selectedCoverDate = new Date(date);
            var msecsInADay = 86400000;
            var maxDateRange = new Date(selectedCoverDate.getTime() - msecsInADay);
            //Set Maximum Date of AdCloaseDate and MaterialDate After Selected Date of CoverDate
            var adClosingDateControl = angular.element(document.querySelector('#txtAdClosingDate'))
            var materialDueDateControl = angular.element(document.querySelector('#txtMaterialDueDate'))
            adClosingDateControl.datepicker("option", "maxDate", maxDateRange);
            materialDueDateControl.datepicker("option", "maxDate", maxDateRange);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function setDateRangeforAdClosingDate(date) {
        try {
            var selectedCoverDate = new Date(date);
            var msecsInADay = 86400000;
            var maxDateRange = new Date(selectedCoverDate.getTime() - msecsInADay);
            var adClosingDateControl = angular.element(document.querySelector('#txtAdClosingDate'))
            adClosingDateControl.datepicker("option", "maxDate", maxDateRange);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function setDateRangeforMaterialDate(date) {
        try {
            var selectedCoverDate = new Date(date);
            var msecsInADay = 86400000;
            var maxDateRange = new Date(selectedCoverDate.getTime() - msecsInADay);
            var materialDueDateControl = angular.element(document.querySelector('#txtMaterialDueDate'))
            materialDueDateControl.datepicker("option", "maxDate", maxDateRange);
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function UpdateIssueDateButtonName() {
        try {
            var mediaAssetButton = angular.element(document.querySelector('#btnIssueDate'));
            mediaAssetButton.text('Update');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function CreateIssueDateButtonName() {
        try {
            var mediaAssetButton = angular.element(document.querySelector('#btnIssueDate'));
            mediaAssetButton.text('Create');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    function ClearForm() {
        try {
            (angular.element(document.querySelector('#txtCoverDate'))).val('');
            (angular.element(document.querySelector('#txtAdClosingDate'))).val('');
            (angular.element(document.querySelector('#txtMaterialDueDate'))).val('');
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.SaveIssueDate = function (issueDate) {
        try {
            if ($scope.IssueDateForm.$valid) {
                if (issueDate.IssueDateId == 0) {
                    issueDatesFactory.save(issueDate, function (result) {
                        if (result.statusCode == 1) {
                            GetIssueDates($stateParams.mediaAssetId);
                            $scope.AddOREdit = "ADD";
                            $scope.issueDate = {};
                            $scope.issueDate.IssueDateId = 0;
                            $scope.issueDate.MediaAssetId = $stateParams.mediaAssetId;
                            InitDateRange();
                            $scope.IssueDateForm.$setPristine();
                            $scope.showMsgs = false;
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else {
                            toastr.error(result.message, 'Error');
                        }
                    })
                } else {
                    issueDatesFactory.update(issueDate, function (result) {
                        if (result.statusCode == 1) {
                            GetIssueDates($stateParams.mediaAssetId);
                            $scope.AddOREdit = "ADD";
                            $scope.issueDate = {};
                            //ClearForm();
                            $scope.issueDate.IssueDateId = 0;
                            $scope.issueDate.MediaAssetId = $stateParams.mediaAssetId;
                            InitDateRange();
                            CreateIssueDateButtonName();
                            $scope.IssueDateForm.$setPristine();
                            $scope.showMsgs = false;
                            toastr.success('Save successfully.', 'Success!');
                        }
                        else {
                            toastr.error(result.message, 'Error');
                        }
                    })
                }
            }
            else {
                $scope.showMsgs = true;
            }
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.AddIssueDate = function () {
        try {
            $scope.issueDate = {};
            $scope.issueDate.IssueDateId = 0;
            $scope.issueDate.MediaAssetId = $stateParams.mediaAssetId;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.EditIssueDate = function (issueDate) {
        try {
            UpdateIssueDateButtonName();
            setDateRange(issueDate.CoverDate);

            //if ((issueDate.AdClosingDate != null && issueDate.AdClosingDate != undefined && issueDate.AdClosingDate != "") &&
            //    (issueDate.MaterialDueDate != null && issueDate.MaterialDueDate != undefined && issueDate.MaterialDueDate != "")) {
            //    setDateRange(issueDate.CoverDate);
            //} else if ((issueDate.AdClosingDate != null && issueDate.AdClosingDate != undefined && issueDate.AdClosingDate != "")) {
            //    setDateRangeforAdClosingDate(issueDate.CoverDate);
            //}
            //else if ((issueDate.MaterialDueDate != null && issueDate.MaterialDueDate != undefined && issueDate.MaterialDueDate != "")) {
            //    setDateRangeforMaterialDate(issueDate.CoverDate);
            //}

            if (issueDate.AdClosingDate != null && issueDate.AdClosingDate != undefined && issueDate.AdClosingDate != "") {
                issueDate.AdClosingDate = getDateInFormat(issueDate.AdClosingDate);
            }
            if (issueDate.CoverDate != null && issueDate.CoverDate != undefined && issueDate.CoverDate != "") {
                issueDate.CoverDate = getDateInFormat(issueDate.CoverDate);
            }
            if (issueDate.MaterialDueDate != null && issueDate.MaterialDueDate != undefined && issueDate.MaterialDueDate != "") {
                issueDate.MaterialDueDate = getDateInFormat(issueDate.MaterialDueDate);
            }
            $scope.issueDate = issueDate;
            $scope.issueDate.MediaAssetId = $stateParams.mediaAssetId;
            $scope.AddOREdit = "EDIT";
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.DeleteIssueDate = function (objissueDate) {
        try {
            console.log(objissueDate);
            var selectedissueDate = $scope.issueDate;
            bootbox.confirm({
                message: "Are you sure you want to delete this Media Asset?",
                //message: "Are you sure you want to delete the selected object(s) and all of their children?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result) {
                        issueDatesFactory.delete(objissueDate.IssueDateId, function (result) {
                            if (result.statusCode == 1) {

                                var UpdateRecord = selectedissueDate.IssueDateId;
                                var deletedRecord = objissueDate.IssueDateId;
                                if (UpdateRecord == deletedRecord) {
                                    $scope.issueDate = {};
                                    $scope.issueDate.IssueDateId = 0;
                                    $scope.issueDate.MediaAssetId = $stateParams.mediaAssetId;
                                    InitDateRange();
                                    CreateIssueDateButtonName();
                                    $scope.IssueDateForm.$setPristine();
                                }
                                GetIssueDates($stateParams.mediaAssetId);
                                toastr.success('Deleted successfully.', 'Success!');
                            }
                            else {
                                toastr.error(result.message, 'Error');
                            }
                        })
                    }
                }
            });
            //if (confirm("Are you sure you want to delete the selected object(s) and all of their children?")) {

            //}
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    $scope.getDateInFormat = function (date) {
        try {
            var dateObj = new Date(date);
            var month = dateObj.getUTCMonth() + 1;
            var day = dateObj.getUTCDate();
            var year = dateObj.getUTCFullYear();
            var newdate = month + "/" + day + "/" + year;
            return newdate;
        }
        catch (e) {
            $scope.isLoading = false;
            $exceptionHandler(e);
        }
    }

    GetIssueDates($stateParams.mediaAssetId);
    InitDateRange();
})