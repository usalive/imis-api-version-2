﻿'use strict';

app.factory('rateCardDetailsFactory', function ($http, $rootScope) {

    var url = "https://localhost:444/api/RateCardDetail";//sf.getServiceRoot('ue') + "api/MediaAsset";

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getAll: function (callback) {

            $http({
                url: url + '',
                method: 'GET',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

        getByRateCardId: function (rateCardId, callback) {
            $http({
                url: url + '/GetByRateCard/' + rateCardId,
                method: 'GET',
                headers: sfHeaders,
                params: {},
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

        GetByRateCardAndColor: function (rateCardId, colorid, callback) {
            $http({
                url: url + '/GetByRateCardAndColor/' + rateCardId + '/' + colorid,
                method: 'GET',
                headers: sfHeaders,
                params: {},
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

        save: function (rateCardDetails, callback) {

            return $http({
                url: url + '',
                method: 'POST',
                data: rateCardDetails,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        update: function (rateCard, callback) {

            return $http({
                url: url + '',
                method: 'PUT',
                data: rateCard,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        delete: function (rateCardId, callback) {

            return $http({
                url: url + '/' + rateCardId,
                method: 'DELETE',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
    };
});