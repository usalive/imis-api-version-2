﻿'use strict';

app.factory('adSizeFactory', function ($http, $rootScope) {


    var url = "https://localhost:444/api/AdSize";//sf.getServiceRoot('ue') + "api/MediaAsset";
    var cacheData = {};
    var individualAreaDetailCache = {};
    var cacheDataDistinct = null;

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getAll: function (callback) {
            $http({
                url: url,
                dataType: 'json',
                method: 'GET',
                data: '',
                headers: {
                    "Content-Type": "application/json"
                }
            }).success(function (result) {
                callback(result);
            }).error(function (error) {
                alert(error);
            });

        },

        save: function (adSize, callback) {

            return $http({
                url: url + '',
                method: 'POST',
                data: adSize,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

        update: function (adSize, callback) {

            return $http({
                url: url + '',
                method: 'PUT',
                data: adSize,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

        delete: function (AdSizeId, callback) {
            return $http({
                url: url + '/' + AdSizeId,
                method: 'DELETE',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
    };
});