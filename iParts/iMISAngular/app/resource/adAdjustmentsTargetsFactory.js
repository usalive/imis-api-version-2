﻿'use strict';

app.factory('adAdjustmentsTargetFactory', function ($http, $rootScope) {

    var url = "https://localhost:444/api/AdAdjustmentTarget";//sf.getServiceRoot('ue') + "api/MediaAsset";

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getAll: function (callback) {
            $http({
                url: url + '',
                method: 'GET',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
      
    };
});