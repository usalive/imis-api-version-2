﻿'use strict';

app.factory('mediaBillingMethodFactory', function ($http, $rootScope) {

    var url = "https://localhost:444/api/MediaBillingMethod";//sf.getServiceRoot('ue') + "api/MediaAsset";

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getAll: function (callback) {
            $http({
                url: url + '',
                method: 'GET',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },


        save: function (adAdjustments, callback) {

            return $http({
                url: url + '',
                method: 'POST',
                data: adAdjustments,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        delete: function (AdAdjustmentId, callback) {

            return $http({
                url: url + '/' + AdAdjustmentId,
                method: 'DELETE',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
    };
});