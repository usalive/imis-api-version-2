﻿'use strict';

app.factory('frequencyFactory', function ($http, $rootScope) {


    var url = "https://localhost:444/api/Frequency";//sf.getServiceRoot('ue') + "api/MediaAsset";

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getAll: function (callback) {
            $http({
                url: url + '',
                method: 'GET',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        getFrequencyById: function (FrequencyId, callback) {
            $http({
                url: url + '/' + FrequencyId,
                method: 'GET',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        save: function (frequency, callback) {

            return $http({
                url: url + '',
                method: 'POST',
                data: frequency,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        update: function (frequency, callback) {

            return $http({
                url: url + '',
                method: 'PUT',
                data: frequency,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        delete: function (FrequencyId, callback) {

            return $http({
                url: url + '/' + FrequencyId,
                method: 'DELETE',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

    };
});