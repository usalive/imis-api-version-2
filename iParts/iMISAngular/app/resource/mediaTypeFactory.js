'use strict';

app.factory('mediaTypeFactory', function ($http, $rootScope) {


    var url = "https://localhost:444/api/MediaType";//sf.getServiceRoot('ue') + "api/MediaAsset";
	var cacheData = {};
	var individualAreaDetailCache = {};
	var cacheDataDistinct = null;

	var sfHeaders = {
		RequestVerificationToken: "token"
	};

	return {
		getAll: function(callback) {

			$http({
				url: url + '',
				method: 'GET',
				headers: sfHeaders,
				cache: false
			}).success(function(result) {
				callback(result);
			});
		},
		get: function(mediaTypeId, callback) {

			$http({
			    url: url + '/' + mediaTypeId,
				method: 'GET',
				headers: sfHeaders,
				params: { },
				cache: false
			}).success(function(result) {
				callback(result);
			});
		},
		save: function (mediaType, callback) {

		    return $http({
		        url: url + '',
		        method: 'POST',
		        data: mediaType,
		        headers: sfHeaders,
		        cache: false
		    }).success(function (result) {
		        callback(result);
		    });
		},
	};
});