'use strict';

app.factory('mediaAssetsFactory', function ($http, $rootScope) {


    var url = "https://localhost:444/api/MediaAsset";//sf.getServiceRoot('ue') + "api/MediaAsset";
    // https://localhost:444/api/MediaAsset
    var cacheData = {};
    var individualAreaDetailCache = {};
    var cacheDataDistinct = null;

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getAll: function (callback) {

            $http({
                url: url + '',
                method: 'GET',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        get: function (mediaAssetId, callback) {

            $http({
                url: url + '/' + mediaAssetId,
                method: 'GET',
                headers: sfHeaders,
                params: {},
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        save: function (mediaAsset, callback) {

            return $http({
                url: url + '',
                method: 'POST',
                data: mediaAsset,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        update: function (mediaAsset, callback) {

            return $http({
                url: url + '',
                method: 'PUT',
                data: mediaAsset,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        delete: function (mediaAssetId, callback) {

            return $http({
                url: url + '/' + mediaAssetId,
                method: 'DELETE',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        getAllItemsummary: function (baseUrl, authToken, Offset, callback) {
            $http({
                //url: "/Scripts/itemCode.txt",
                // url: baseUrl + "api/Itemsummary?limit=1000000",
                url: baseUrl + "api/Itemsummary?limit=500&Offset=" + Offset,
                dataType: 'json',
                method: 'GET',
                data: '',
                headers: {
                    "Content-Type": "application/json",
                    'RequestVerificationToken': authToken
                }
            }).success(function (result) {
                callback(result);
            }).error(function (error) {
                console.log(error);
            });
        },
    };
});