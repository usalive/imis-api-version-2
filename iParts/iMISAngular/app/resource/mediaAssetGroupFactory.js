'use strict';

app.factory('mediaAssetGroupFactory', function ($http, $rootScope) {


    var url = "https://localhost:444/api/MediaAssetGroup";//sf.getServiceRoot('ue') + "api/MediaAsset";
	
	var sfHeaders = {
		RequestVerificationToken: "token"
	};

	return {
		getAll: function(callback) {

			$http({
				url: url + '',
				method: 'GET',
				headers: sfHeaders,
				cache: false
			}).success(function(result) {
				callback(result);
			});
		},
		get: function(mediaAssetGroupId, callback) {

			$http({
			    url: url + '/' + mediaAssetGroupId,
				method: 'GET',
				headers: sfHeaders,
				params: { },
				cache: false
			}).success(function(result) {
				callback(result);
			});
		},
		save: function (mediaAssetGroup, callback) {

		    return $http({
		        url: url + '',
		        method: 'POST',
		        data: mediaAssetGroup,
		        headers: sfHeaders,
		        cache: false
		    }).success(function (result) {
		        callback(result);
		    });
		},
	};
});