﻿'use strict';

app.factory('adAdjustmentsFactory', function ($http, $rootScope) {

    var url = "https://localhost:444/api/adadjustment";//sf.getServiceRoot('ue') + "api/MediaAsset";

    var sfHeaders = {
        RequestVerificationToken: "token"
    };

    return {
        getAll: function (callback) {

            $http({
                url: url + '',
                method: 'GET',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

        getByMediaAsset: function (mediaAssetId, callback) {

            $http({
                url: url + '/getbymediaasset/' + mediaAssetId,
                method: 'GET',
                headers: sfHeaders,
                params: {},
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },

        save: function (adAdjustments, callback) {

            return $http({
                url: url + '',
                method: 'POST',
                data: adAdjustments,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        update: function (adAdjustments, callback) {

            return $http({
                url: url + '',
                method: 'PUT',
                data: adAdjustments,
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
        delete: function (AdAdjustmentId, callback) {

            return $http({
                url: url + '/' + AdAdjustmentId,
                method: 'DELETE',
                headers: sfHeaders,
                cache: false
            }).success(function (result) {
                callback(result);
            });
        },
    };
});