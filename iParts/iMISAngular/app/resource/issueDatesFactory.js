'use strict';

app.factory('issueDatesFactory', function ($http, $rootScope) {


    var url = "https://localhost:444/api/issuedate";//sf.getServiceRoot('ue') + "api/MediaAsset";
	
	var sfHeaders = {
		RequestVerificationToken: "token"
	};

	return {
		getAll: function(callback) {

			$http({
				url: url + '',
				method: 'GET',
				headers: sfHeaders,
				cache: false
			}).success(function(result) {
				callback(result);
			});
		},
		getByMediaAsset: function (mediaAssetId, callback) {

			$http({
			    url: url + '/getbymediaasset/' + mediaAssetId,
				method: 'GET',
				headers: sfHeaders,
				params: { },
				cache: false
			}).success(function(result) {
				callback(result);
			});
		},
		save: function (issueDate, callback) {

		    return $http({
		        url: url + '',
		        method: 'POST',
		        data: issueDate,
		        headers: sfHeaders,
		        cache: false
		    }).success(function (result) {
		        callback(result);
		    });
		},
		update: function (issueDate, callback) {

		    return $http({
		        url: url + '',
		        method: 'PUT',
		        data: issueDate,
		        headers: sfHeaders,
		        cache: false
		    }).success(function (result) {
		        callback(result);
		    });
		},
		delete: function (issueDateId, callback) {

		    return $http({
		        url: url + '/' + issueDateId,
		        method: 'DELETE',
		        headers: sfHeaders,
		        cache: false
		    }).success(function (result) {
		        callback(result);
		    });
		},
	};
});